package com.capgemini.psd2.account.standingorder.boi.fs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.aisp.domain.StandingOrdersGETResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.AccountStandingOrderFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.domain.StandingOrder;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.domain.StandingOrders;

@SpringBootApplication
@ComponentScan("com.capgemini.psd2")
public class AccountStandingOrderFoundationServiceTestProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccountStandingOrderFoundationServiceTestProjectApplication.class, args);
	}
}

@RestController
@ResponseBody
class TestAccountStandingOrderFSController{
	
	@Autowired
	AccountStandingOrderFoundationServiceAdapter adapter;
	
	@RequestMapping("/testAccountStandingOrder")
	public StandingOrdersGETResponse getResponse(){
		StandingOrders standingOrders = new StandingOrders();
		StandingOrder standingOrder = new StandingOrder();
		
		standingOrders.getStandingOrders().add(standingOrder);
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		
		AccountDetails accDet1 = new AccountDetails();
		accDet1.setAccountId("56789");
		accDet1.setAccountNSC("901541");
		accDet1.setAccountNumber("23682879");
		accDetList.add(accDet1);

		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("BOI999");
		accountMapping.setCorrelationId("23456778");
		Map<String, String> params = new HashMap<>();
		params.put("channelId", "BOL");
	    params.put("consentFlowType", "AISP");
	    params.put("x-channel-id", "BOL");
	    params.put("x-user-id", "BOI999");
	    params.put("X-BOI-PLATFORM", "platform");
	    params.put("x-fapi-interaction-id", "12345678"); 
		return adapter.retrieveAccountStandingOrders(accountMapping, params);
		
	}
}
