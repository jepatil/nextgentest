/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.product.mock.foundationservice.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.account.product.mock.foundationservice.domain.Product;
import com.capgemini.psd2.account.product.mock.foundationservice.repository.AccountProductRepository;
import com.capgemini.psd2.account.product.mock.foundationservice.service.AccountProductService;
import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;

/**
 * The Class AccountProductServiceImpl.
 */
@Service
public class AccountProductServiceImpl implements AccountProductService {

	/** The repository. */
	@Autowired
	private AccountProductRepository repository;

	@Override
	public Product retrieveAccountProduct(String sortCode, String accountNumber) throws Exception {
		Product product = repository.findBySortCodeAndAccountNumber(sortCode, accountNumber);
		if (product == null) {

			throw MockFoundationServiceException
					.populateMockFoundationServiceException(ErrorCodeEnum.NO_PRODUCTDETAILS_FOUND_ESPR);
		}

		return product;
	}

}
