package com.capgemini.psd2.customer.account.list.mongo.db.adapter.test.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.aisp.domain.Data2;
import com.capgemini.psd2.customer.account.list.mongo.db.adapter.impl.CustomerAccountListMongoDBAdapterImpl;
import com.capgemini.psd2.customer.account.list.mongo.db.adapter.repository.CustomerAccountListMongoDBAdapterRepository;
import com.capgemini.psd2.customer.account.list.mongo.db.adapter.test.mock.data.CustomerAccountListMockData;
import com.capgemini.psd2.exceptions.PSD2Exception;

@RunWith(SpringJUnit4ClassRunner.class)
public class CustomerAccountListMongoDBAdapterImplTest {

	@InjectMocks
	private CustomerAccountListMongoDBAdapterImpl customerAccountListMongoDBAdapterImpl = new CustomerAccountListMongoDBAdapterImpl();

	@Mock
	private CustomerAccountListMongoDBAdapterRepository customerAccountListMongoDBAdapterRepository;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testRetrieveCustomerAccountListSuccessFlow() {
		Data2 data2 = CustomerAccountListMockData.getCustomerAccountInfoList();
		when(customerAccountListMongoDBAdapterRepository.findByPsuId(anyString())).thenReturn(data2.getAccount());
		assertEquals(CustomerAccountListMockData.getCustomerAccountInfoList(),
				customerAccountListMongoDBAdapterImpl.retrieveCustomerAccountList("1234",null).getData());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testRetrieveCustomerAccountListDataAccessException() {
		when(customerAccountListMongoDBAdapterRepository.findByPsuId("1234"))
				.thenThrow(DataAccessResourceFailureException.class);
		customerAccountListMongoDBAdapterImpl.retrieveCustomerAccountList("1234",null);
	}

}
