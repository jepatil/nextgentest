package com.capgemini.psd2.customer.account.list.mongo.db.adapter.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.aisp.domain.Account;
import com.capgemini.psd2.aisp.mock.domain.MockAccount;

public interface CustomerAccountListMongoDBAdapterRepository extends MongoRepository<MockAccount, String> {
	public List<Account> findByPsuId(String userId);
}
