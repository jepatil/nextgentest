package com.capgemini.psd2.customer.account.list.routing.adapter.test.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.aisp.adapter.CustomerAccountListAdapter;
import com.capgemini.psd2.aisp.domain.AccountGETResponse;
import com.capgemini.psd2.customer.account.list.routing.adapter.impl.CustomerAccountListRoutingAdapter;
import com.capgemini.psd2.customer.account.list.routing.adapter.routing.CustomerListAdapterFactory;
import com.capgemini.psd2.customer.account.list.routing.adapter.test.adapter.CustomerAccountListRoutingTestAdapter;
import com.capgemini.psd2.customer.account.list.routing.adapter.test.mock.data.CustomerAccountListRoutingAdapterMockData;

@RunWith(SpringJUnit4ClassRunner.class)
public class CustomerAccountListRoutingAdapterTest {

	@Mock
	private CustomerListAdapterFactory customerListAdapterFactory;

	@InjectMocks
	private CustomerAccountListRoutingAdapter customerAccountListRoutingAdapter = new CustomerAccountListRoutingAdapter();

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testRetrieveCustomerAccountListSuccessFlow() {
		AccountGETResponse acctResponse = CustomerAccountListRoutingAdapterMockData
				.getCustomerAccountInfo();
		CustomerAccountListAdapter customerAccountListAdapter = new CustomerAccountListRoutingTestAdapter();
		when(customerListAdapterFactory.getAdapterInstance(anyString())).thenReturn(customerAccountListAdapter);
		Map<String, String> params = null;
		AccountGETResponse accountGETResponse = customerAccountListRoutingAdapter.retrieveCustomerAccountList("1234",params);
		assertEquals(acctResponse.getData().getAccount().get(0).getAccount(),
				accountGETResponse.getData().getAccount().get(0).getAccount());
	}
}