package com.capgemini.psd2.customer.account.list.routing.adapter.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.Account;
import com.capgemini.psd2.aisp.domain.AccountGETResponse;
import com.capgemini.psd2.aisp.domain.Data2;
import com.capgemini.psd2.aisp.domain.Data2Account;
import com.capgemini.psd2.aisp.domain.Data2Servicer;
import com.capgemini.psd2.consent.domain.CustomerAccountInfo;

public class CustomerAccountListRoutingAdapterMockData {


	public static List<CustomerAccountInfo> customerAccountInfoList;
	
	public static List<CustomerAccountInfo> getCustomerAccountInfoList(){
		customerAccountInfoList = new ArrayList();
		CustomerAccountInfo customerAccountInfo1 = new CustomerAccountInfo();
		customerAccountInfo1.setAccountName("John Doe");
		customerAccountInfo1.setUserId("1234");
		customerAccountInfo1.setAccountNumber("10203345");
		customerAccountInfo1.setAccountNSC("SC802001");
		customerAccountInfo1.setCurrency("EUR");
		customerAccountInfo1.setNickname("John");
		customerAccountInfo1.setAccountType("checking");
		
		CustomerAccountInfo customerAccountInfo2 = new CustomerAccountInfo();
		customerAccountInfo2.setAccountName("Tiffany Doe");
		customerAccountInfo2.setUserId("1234");
		customerAccountInfo2.setAccountNumber("10203346");
		customerAccountInfo2.setAccountNSC("SC802002");
		customerAccountInfo2.setCurrency("GRP");
		customerAccountInfo2.setNickname("Tiffany");
		customerAccountInfo2.setAccountType("savings");
		
		customerAccountInfoList.add(customerAccountInfo1);
		customerAccountInfoList.add(customerAccountInfo2);
		return customerAccountInfoList;
	}
	
	public static AccountGETResponse getCustomerAccountInfo(){
		AccountGETResponse accountGETResponse = new AccountGETResponse();
		List<Account> accountList =  new ArrayList<>();
	    Account account = new Account();
	    account.setAccountId("12435455");
	    account.setCurrency("EUR");
	    Data2Servicer servicer =  new Data2Servicer();
	    servicer.setIdentification("123445");
		account.setServicer(servicer);
		Data2Account acct = new Data2Account();
		acct.setIdentification("243543");
		account.setAccount(acct);
		accountList.add(account );
		Data2 data2 = new Data2();
		data2.setAccount(accountList);
		accountGETResponse.setData(data2);
		return accountGETResponse;
	}


}
