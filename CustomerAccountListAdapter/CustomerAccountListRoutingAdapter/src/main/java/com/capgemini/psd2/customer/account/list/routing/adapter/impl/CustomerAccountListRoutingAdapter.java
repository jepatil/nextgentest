package com.capgemini.psd2.customer.account.list.routing.adapter.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.capgemini.psd2.aisp.adapter.CustomerAccountListAdapter;
import com.capgemini.psd2.aisp.domain.AccountGETResponse;
import com.capgemini.psd2.customer.account.list.routing.adapter.routing.CustomerListAdapterFactory;
import com.capgemini.psd2.fraudsystem.domain.PSD2CustomerInfo;

public class CustomerAccountListRoutingAdapter implements CustomerAccountListAdapter {

	@Autowired
	private CustomerListAdapterFactory customerListAdapterFactory;

	@Value("${app.defaultCustomerListAdapter}")
	private String defaultAdapter;

	@Override
	public AccountGETResponse retrieveCustomerAccountList(String userId, Map<String, String> params) {
		CustomerAccountListAdapter customerAccountListAdapter = customerListAdapterFactory
				.getAdapterInstance(defaultAdapter);
		return customerAccountListAdapter.retrieveCustomerAccountList(userId, params);
	}

	@Override
	public PSD2CustomerInfo retrieveCustomerInfo(String userId, Map<String, String> params) {
		CustomerAccountListAdapter customerAccountListAdapter = customerListAdapterFactory
				.getAdapterInstance(defaultAdapter);
		return customerAccountListAdapter.retrieveCustomerInfo(userId, params);
	}
}