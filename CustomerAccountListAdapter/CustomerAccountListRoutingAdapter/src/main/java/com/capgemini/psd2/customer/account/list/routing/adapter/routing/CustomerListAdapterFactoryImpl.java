package com.capgemini.psd2.customer.account.list.routing.adapter.routing;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.adapter.CustomerAccountListAdapter;

@Component
public class CustomerListAdapterFactoryImpl implements ApplicationContextAware, CustomerListAdapterFactory {

	private ApplicationContext applicationContext;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}

	@Override
	public CustomerAccountListAdapter getAdapterInstance(String coreSystemName) {
		return (CustomerAccountListAdapter) applicationContext.getBean(coreSystemName);
	}
}