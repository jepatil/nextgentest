package com.capgemini.psd2.scaconsenthelper.config.helpers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.capgemini.psd2.aisp.domain.Account;
import com.capgemini.psd2.aisp.domain.AccountGETResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.utilities.NullCheckUtils;

public abstract class ConsentAuthorizationHelper {

	
	public List<Account> matchAccountFromConsentAccount(AccountGETResponse accountGETResponse,AispConsent aispConsent){
		List<Account> consentAccounts = null;
		Map<String,Account> mapOfAccounts = null;
		Account account = null;
		if (accountGETResponse == null || accountGETResponse.getData().getAccount().isEmpty()) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.SELECTED_ACCOUNTS_NOT_OWNED_BY_CUSTOMER);
		}
		if(aispConsent.getAccountDetails() == null || aispConsent.getAccountDetails().isEmpty()){
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_ACCOUNTS_SELECTED_BY_CUSTOMER);
		}
		mapOfAccounts = new HashMap<String,Account>();
		consentAccounts = new ArrayList<Account>();
		for (Account currAccount : accountGETResponse.getData().getAccount()) {
			mapOfAccounts.put(((PSD2Account)currAccount).getHashedValue(), currAccount);
		}
		for(AccountDetails accountDetail : aispConsent.getAccountDetails()){
			account = mapOfAccounts.get(accountDetail.getHashValue());
			consentAccounts.add(account);
		}
		return consentAccounts;
	}
	
	public List<Account> populateAccountListFromAccountDetails(AispConsent aispConsent){
		List<AccountDetails> accountDetails =aispConsent.getAccountDetails();
		List<Account> accountList = new ArrayList<Account>();
		PSD2Account account = null;
		for(AccountDetails accountDetail : accountDetails){
			account = new PSD2Account();
			account.setCurrency(accountDetail.getCurrency());
			account.setNickname(accountDetail.getNickname());
			account.setAccountId(accountDetail.getAccountId());
			account.setAccountType(accountDetail.getAccountType());
			account.setAccount(accountDetail.getAccount());
			account.setServicer(accountDetail.getServicer());
			account.setHashedValue();
			accountList.add(account);
		}
		return accountList;
	}
	
	public Account matchAccountFromList(AccountGETResponse accountGETResponse, PSD2Account selectedAccount) {
		PSD2Account customerAccount = null;
		if (accountGETResponse == null || accountGETResponse.getData().getAccount().isEmpty()) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.SELECTED_ACCOUNTS_NOT_OWNED_BY_CUSTOMER);
		}
		if(selectedAccount != null)
		{
			for (Account account : accountGETResponse.getData().getAccount()) {
				customerAccount = matchHash(selectedAccount, (PSD2Account)account);
				if (customerAccount != null) {
					break;
				}
			}			
			if(customerAccount == null){
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.SELECTED_ACCOUNTS_NOT_OWNED_BY_CUSTOMER);
			}
		}
		else {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_ACCOUNTS_SELECTED_BY_CUSTOMER);
		}
		customerAccount = populateAccountwithUnmaskedValues(selectedAccount,(PSD2Account)customerAccount);
		return customerAccount;		
	}
	
	private PSD2Account matchHash(PSD2Account submittedAccount,PSD2Account customerAccount){

		String acctHashedValue = submittedAccount.getHashedValue();
		if (NullCheckUtils.isNullOrEmpty(acctHashedValue)) {
			throw PSD2Exception
					.populatePSD2Exception(ErrorCodeEnum.SELECTED_ACCOUNTS_NOT_OWNED_BY_CUSTOMER);
		}
			
		boolean accountMatch = acctHashedValue.equals(customerAccount.getHashedValue());
		if(accountMatch) {
			return customerAccount;
		}
		return null;
	}
	
	public abstract PSD2Account populateAccountwithUnmaskedValues(PSD2Account accountwithMask, PSD2Account accountwithoutMask);
}
