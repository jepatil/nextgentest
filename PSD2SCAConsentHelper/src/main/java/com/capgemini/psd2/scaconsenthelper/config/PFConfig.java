package com.capgemini.psd2.scaconsenthelper.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "pf")
public class PFConfig {

	private String resumePathBaseURL;

	private String dropOffURL;

	private String scainstanceusername;

	private String scainstancepassword;

	private String aispinstanceusername;

	private String aispinstancepassword;

	private String pispinstanceusername;

	private String pispinstancepassword;
	
	private String scainstanceId;

	private String aispinstanceId;

	private String pispinstanceId;

	private String pickupURL;
	
	private String grantsrevocationURL;
	
	private String grantsrevocationapiuser;
	
	private String grantsrevocationapipwd;

	public String getResumePathBaseURL() {
		return resumePathBaseURL;
	}

	public void setResumePathBaseURL(String resumePathBaseURL) {
		this.resumePathBaseURL = resumePathBaseURL;
	}

	public String getDropOffURL() {
		return dropOffURL;
	}

	public void setDropOffURL(String dropOffURL) {
		this.dropOffURL = dropOffURL;
	}

	public String getPickupURL() {
		return pickupURL;
	}

	public void setPickupURL(String pickupURL) {
		this.pickupURL = pickupURL;
	}

	public String getScainstanceId() {
		return scainstanceId;
	}

	public void setScainstanceId(String scainstanceId) {
		this.scainstanceId = scainstanceId;
	}

	public String getAispinstanceId() {
		return aispinstanceId;
	}

	public void setAispinstanceId(String aispinstanceId) {
		this.aispinstanceId = aispinstanceId;
	}

	public String getPispinstanceId() {
		return pispinstanceId;
	}

	public void setPispinstanceId(String pispinstanceId) {
		this.pispinstanceId = pispinstanceId;
	}

	public String getScainstanceusername() {
		return scainstanceusername;
	}

	public void setScainstanceusername(String scainstanceusername) {
		this.scainstanceusername = scainstanceusername;
	}

	public String getScainstancepassword() {
		return scainstancepassword;
	}

	public void setScainstancepassword(String scainstancepassword) {
		this.scainstancepassword = scainstancepassword;
	}

	public String getAispinstanceusername() {
		return aispinstanceusername;
	}

	public void setAispinstanceusername(String aispinstanceusername) {
		this.aispinstanceusername = aispinstanceusername;
	}

	public String getAispinstancepassword() {
		return aispinstancepassword;
	}

	public void setAispinstancepassword(String aispinstancepassword) {
		this.aispinstancepassword = aispinstancepassword;
	}

	public String getPispinstanceusername() {
		return pispinstanceusername;
	}

	public void setPispinstanceusername(String pispinstanceusername) {
		this.pispinstanceusername = pispinstanceusername;
	}

	public String getPispinstancepassword() {
		return pispinstancepassword;
	}

	public void setPispinstancepassword(String pispinstancepassword) {
		this.pispinstancepassword = pispinstancepassword;
	}

	public String getGrantsrevocationapiuser() {
		return grantsrevocationapiuser;
	}

	public void setGrantsrevocationapiuser(String grantsrevocationapiuser) {
		this.grantsrevocationapiuser = grantsrevocationapiuser;
	}

	public String getGrantsrevocationapipwd() {
		return grantsrevocationapipwd;
	}

	public void setGrantsrevocationapipwd(String grantsrevocationapipwd) {
		this.grantsrevocationapipwd = grantsrevocationapipwd;
	}

	public String getGrantsrevocationURL() {
		return grantsrevocationURL;
	}

	public void setGrantsrevocationURL(String grantsrevocationURL) {
		this.grantsrevocationURL = grantsrevocationURL;
	}	
}