package com.capgemini.psd2.scaconsenthelper.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DropOffResponse {
	
	@JsonProperty("REF")
	private String ref;
/*
	public String getREF() {
		return REF;
	}

	public void setREF(String REF) {
		this.REF = REF;
	}
*/

	public String getRef() {
		return ref;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}	
	
/*	public static void main(String[] args){
		String json = "{\"REF\":\"3432432423423\"}";
		DropOffResponse response = JSONUtilities.getObjectFromJSONString(json, DropOffResponse.class);
		System.out.println(response.getRef());
	}
*/
	

}
