package com.capgemini.psd2.scaconsenthelper.filters;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.filter.OncePerRequestFilter;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.scaconsenthelper.constants.PFConstants;
import com.capgemini.psd2.scaconsenthelper.constants.SCAConsentHelperConstants;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.scaconsenthelper.models.RawAccessJwtToken;
import com.capgemini.psd2.scaconsenthelper.models.SecurityRequestAttributes;
import com.capgemini.psd2.security.exceptions.PSD2SecurityException;
import com.capgemini.psd2.security.exceptions.SCAConsentErrorCodeEnum;
import com.capgemini.psd2.utilities.DateUtilites;
import com.capgemini.psd2.utilities.JSONUtilities;

public class ConsentBlockBackActionFilter extends OncePerRequestFilter {

	@Value("${app.cookietoken.tokenSigningKey:#{null}}")
	private String tokenSigningKey;

	@Autowired
	private LoggerUtils loggerUtils;
	
	
	@Value("${spring.application.name}")
	private String applicationName;
	
	@Autowired
	private RequestHeaderAttributes requestHeaderAttributes;

	@Autowired
	private SecurityRequestAttributes securityRequestAttributes; 
	
	@Value("${app.jweDecryptionKey:#{null}}")
	private String jweDecryptionKey;
	
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(ConsentBlockBackActionFilter.class);


	public ConsentBlockBackActionFilter() {
	}
	
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException 
	{
		LOG.info(
				"{\"Enter\":\"{}\",\"preCorrelationId\":\" \",\"timestamp\":\"{}\",\"remoteAddress\":\"{}\",\"hostName\":\"{}\",\"correlationId\":\"{}\"}",
				"com.capgemini.psd2.security.consent.aisp.filters.BlockRefreshActionsFilter.doFilterInternal()",
				DateUtilites.generateCurrentTimeStamp(), request.getRemoteAddr(), request.getRemoteHost(), null);

		RawAccessJwtToken token = null;		
		
		try {
			PickupDataModel intentData = null;		
			String refId = null;
			request.setAttribute(PSD2Constants.APPLICATION_NAME, applicationName);
			if(request.getParameter(PFConstants.REF) == null || request.getParameter(PFConstants.REF).trim().isEmpty()) {
				refId = securityRequestAttributes.getParamMap().get(PFConstants.REF) != null ? (String)securityRequestAttributes.getParamMap().get(PFConstants.REF)[0] : null;
			}
			
			else {
				refId = request.getParameter(PFConstants.REF);
			}
			
			if(refId == null || refId.trim().isEmpty()){
				throw PSD2Exception.populatePSD2Exception("Reference Id missing in the request",ErrorCodeEnum.VALIDATION_ERROR);
			}
			
			Cookie customSessionControllCookie = null;		
			Cookie[] cookies = request.getCookies();
			if (cookies != null) {
				for (Cookie cookie : cookies) {
					if (cookie.getName().equalsIgnoreCase(SCAConsentHelperConstants.CUSTOM_SESSION_CONTROLL_COOKIE)) {
						customSessionControllCookie = cookie;
						LOG.info("customSessionControllCookie found...");
						break;
					}
				}
			}
			
			if(customSessionControllCookie != null){
				String tokenPayload =  customSessionControllCookie.getValue();
				
				token = new RawAccessJwtToken(tokenPayload, requestHeaderAttributes);
				
				String intentDataStr = token.parseAndReturnClaims(jweDecryptionKey);
				
/*				String claims = JwtHelper.decode(tokenPayload).getClaims();
				JSONParser parser = new JSONParser();
				JSONObject jsonObjClaims = (JSONObject) parser.parse(claims);
				String intentDataStr = (String) jsonObjClaims.get(SCAConsentHelperConstants.INTENT_DATA);
*/				intentData = JSONUtilities.getObjectFromJSONString(intentDataStr, PickupDataModel.class);
				LOG.info("Token Found .... ");
				if(intentData.getOriginatorRefId().equalsIgnoreCase(refId)){
					throw PSD2SecurityException.populatePSD2SecurityException("Refresh or back button event..",SCAConsentErrorCodeEnum.VALIDATION_ERROR);				
				}
			}
			doFilter(request, response, filterChain);			
		}		
		catch (PSD2Exception psd2Exception) {
			LOG.error("{\"Exception\":\"{}\",\"{}\",\"ErrorDetails\":{}}",
					"com.capgemini.psd2.security.consent.aisp.filters.BlockRefreshActionsFilter.doFilterInternal()",
					loggerUtils.populateLoggerData("doFilterInternal"), psd2Exception.getErrorInfo());
			if (LOG.isDebugEnabled()) {
				LOG.error("{\"Exception\":\"{}\",\"{}\",\"ErrorDetails\":\"{}\"}",
						"com.capgemini.psd2.security.consent.aisp.filters.BlockRefreshActionsFilter.doFilterInternal()",
						loggerUtils.populateLoggerData("doFilterInternal"), psd2Exception.getStackTrace(), psd2Exception);
			}
			response.setContentType("application/json");
			response.setStatus(Integer.parseInt(psd2Exception.getErrorInfo().getStatusCode()));
			request.setAttribute(SCAConsentHelperConstants.EXCEPTION, psd2Exception.getErrorInfo());
			request.getRequestDispatcher("/sessionerrors").forward(request, response);
		}
		catch (Exception e) {
			PSD2Exception psd2Exception = PSD2Exception.populatePSD2Exception(e.getMessage(),
					ErrorCodeEnum.TECHNICAL_ERROR);
			LOG.error("{\"Exception\":\"{}\",\"{}\",\"ErrorDetails\":{}}",
					"com.capgemini.psd2.security.consent.aisp.filters.BlockRefreshActionsFilter.doFilterInternal()",
					loggerUtils.populateLoggerData("doFilterInternal"), psd2Exception.getErrorInfo());
			if (LOG.isDebugEnabled()) {
				LOG.error("{\"Exception\":\"{}\",\"{}\",\"ErrorDetails\":\"{}\"}",
						"com.capgemini.psd2.security.consent.aisp.filters.BlockRefreshActionsFilter.doFilterInternal()",
						loggerUtils.populateLoggerData("doFilterInternal"), e.getStackTrace(), e);
			}
			response.setContentType("application/json");
			response.setStatus(Integer.parseInt(psd2Exception.getErrorInfo().getStatusCode()));
			request.setAttribute(SCAConsentHelperConstants.EXCEPTION, psd2Exception.getErrorInfo());
			request.getRequestDispatcher("/sessionerrors").forward(request, response);
		}
	}
}