package com.capgemini.psd2.scaconsenthelper.filters;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.scaconsenthelper.config.PFConfig;
import com.capgemini.psd2.scaconsenthelper.config.helpers.JwtTokenUtility;
import com.capgemini.psd2.scaconsenthelper.constants.PFConstants;
import com.capgemini.psd2.scaconsenthelper.constants.SCAConsentHelperConstants;
import com.capgemini.psd2.scaconsenthelper.models.IntentTypeEnum;
import com.capgemini.psd2.scaconsenthelper.models.JwtToken;
import com.capgemini.psd2.scaconsenthelper.models.PFInstanceData;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.scaconsenthelper.models.RawAccessJwtToken;
import com.capgemini.psd2.scaconsenthelper.services.SCAConsentHelperService;
import com.capgemini.psd2.security.constants.PSD2SecurityConstants;
import com.capgemini.psd2.security.exceptions.PSD2SecurityException;
import com.capgemini.psd2.security.exceptions.SCAConsentErrorCodeEnum;
import com.capgemini.psd2.utilities.DateUtilites;
import com.capgemini.psd2.utilities.JSONUtilities;

public abstract class CookieHandlerFilter extends OncePerRequestFilter {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(CookieHandlerFilter.class);

	@Value("${app.protectedEndPoints:#{null}}")
	private String protectedEndPoints;

	@Value("${app.tokenSigningKey:#{null}}")
	private String tokenSigningKey;
	
	@Value("${app.jweDecryptionKey:#{null}}")
	private String jweDecryptionKey;
	
	@Value("${app.cookietoken.tokenIssuer:#{null}}")
	private String tokenIssuer;

	@Value("${app.cookievalidity:#{null}}")
	private Integer expiry;
	
	@Value("${app.sessiontimeout:#{null}}")
	private Integer sessiontimeout;
		
	

	/** The logger utils. */
	@Autowired
	private LoggerUtils loggerUtils;

	@Autowired
	private RequestHeaderAttributes requestHeaderAttributes;

	@Autowired
	private SCAConsentHelperService helperService;

	@Autowired
	private PFConfig pfConfig;

	private String flowType;

	public CookieHandlerFilter(String flowType) {
		this.flowType = flowType;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		LOG.info(
				"{\"Enter\":\"{}\",\"preCorrelationId\":\" \",\"timestamp\":\"{}\",\"remoteAddress\":\"{}\",\"hostName\":\"{}\",\"correlationId\":\"{}\"}",
				"com.capgemini.psd2.scaconsenthelper.filters.CookieHandlerFilter.doFilterInternal()",
				DateUtilites.generateCurrentTimeStamp(), request.getRemoteAddr(), request.getRemoteHost(), null);
		populateSecurityHeaders(response);
		// boolean refreshCase = Boolean.FALSE;
		PickupDataModel intentData = null;
		PFInstanceData pfInstanceData = populatePFInstanceData();
		try {
			Cookie[] cookies = request.getCookies();
			Cookie customSessionControllCookie = null;
			String[] endPoints = protectedEndPoints.split(",");

			if (cookies != null) {
				for (Cookie cookie : cookies) {
					if (cookie.getName().equalsIgnoreCase(SCAConsentHelperConstants.CUSTOM_SESSION_CONTROLL_COOKIE)) {
						customSessionControllCookie = cookie;
						customSessionControllCookie.setHttpOnly(true);
						customSessionControllCookie.setSecure(true);
						LOG.info("customSessionControllCookie found...");
						break;
					}
				}
			}

			LOG.info("End Point.... " + endPoints);
			LOG.info("End Point match .... " + request.getServletPath().lastIndexOf("/"));
			if (!Arrays.asList(endPoints)
					.contains(request.getServletPath().substring(request.getServletPath().lastIndexOf("/")))) {

				if (customSessionControllCookie != null && !customSessionControllCookie.getValue().trim().isEmpty()) {
					// validate
					String tokenPayload = customSessionControllCookie.getValue();
					try {
						RawAccessJwtToken token = new RawAccessJwtToken(tokenPayload, requestHeaderAttributes);
						/*String claims = JwtHelper.decode(tokenPayload).getClaims();
						JSONParser parser = new JSONParser();
						JSONObject jsonObjClaims = (JSONObject) parser.parse(claims);
						String intentDataStr = (String) jsonObjClaims.get(SCAConsentHelperConstants.INTENT_DATA);*/
						
						String intentDataStr = token.parseAndReturnClaims(jweDecryptionKey);
						String requestUri = request.getRequestURI();
						intentData = JSONUtilities.getObjectFromJSONString(intentDataStr, PickupDataModel.class);
						request.setAttribute(SCAConsentHelperConstants.INTENT_DATA, intentData);

						requestHeaderAttributes.setIntentId(intentData.getIntentId());
						requestHeaderAttributes.setTppCID(intentData.getClientId());
						requestHeaderAttributes.setPsuId(intentData.getUserId() != null && !intentData.getUserId().trim().isEmpty() ? intentData.getUserId() : request.getParameter("username"));
						requestHeaderAttributes.setChannelId(intentData.getChannelId() != null && !intentData.getChannelId().trim().isEmpty() ? intentData.getChannelId() : request.getParameter(PSD2SecurityConstants.CHANNEL_ID));
						requestHeaderAttributes.setCorrelationId(intentData.getCorrelationId());
						requestHeaderAttributes.setScopes(intentData.getScope());
						requestHeaderAttributes.setSelfUrl(
								requestUri.endsWith("/") ? requestUri.substring(0, requestUri.length() - 1) : requestUri);
						requestHeaderAttributes.setMethodType(request.getMethod());
						
						
						token.parseClaims(tokenSigningKey,jweDecryptionKey);
						LOG.info("Token Found .... ");
					} catch (Exception e) {
						LOG.info("Exception in toekn decrypt .... ");
						throw PSD2SecurityException.populatePSD2SecurityException(e.getMessage(),
								SCAConsentErrorCodeEnum.SESSION_EXPIRED);
					}
				} else {
					LOG.info("In else condition .... ");
					throw PSD2SecurityException.populatePSD2SecurityException(SCAConsentErrorCodeEnum.SESSION_EXPIRED);
				}
			} else {
				validateRefreshBackScenarios(customSessionControllCookie, pfInstanceData,request,response);
			}
			doFilter(request, response, filterChain);				
		} catch (PSD2Exception e) {
			LOG.error("{\"Exception\":\"{}\",\"{}\",\"ErrorDetails\":{}}",
					"com.capgemini.psd2.scaconsenthelper.filters.CookieHandlerFilter.doFilterInternal()",
					loggerUtils.populateLoggerData("doFilterInternal"), e.getErrorInfo());
			if (LOG.isDebugEnabled()) {
				LOG.error("{\"Exception\":\"{}\",\"{}\",\"ErrorDetails\":\"{}\"}",
						"com.capgemini.psd2.scaconsenthelper.filters.CookieHandlerFilter.doFilterInternal()",
						loggerUtils.populateLoggerData("doFilterInternal"), e.getStackTrace(), e);
			}
			request.setAttribute(PSD2Constants.CONSENT_FLOW_TYPE, flowType);			
			request.setAttribute(SCAConsentHelperConstants.EXCEPTION, e.getErrorInfo());			
			response.setContentType("application/json");
			response.setStatus(Integer.parseInt(e.getErrorInfo().getStatusCode()));
			if(request.getAttribute(SCAConsentHelperConstants.UNAUTHORIZED_ERROR) != null && ((Boolean)request.getAttribute(SCAConsentHelperConstants.UNAUTHORIZED_ERROR))){				
				request.getRequestDispatcher("/sessionerrors").forward(request, response);				
			}
			else {
				dropOff(request, response, pfInstanceData);
				request.getRequestDispatcher("/errors").forward(request, response);
			}
		} catch (Exception e) {
			PSD2Exception psd2Exception = PSD2Exception.populatePSD2Exception(e.getMessage(),
					ErrorCodeEnum.TECHNICAL_ERROR);
			LOG.error("{\"Exception\":\"{}\",\"{}\",\"ErrorDetails\":{}}",
					"com.capgemini.psd2.scaconsenthelper.filters.CookieHandlerFilter.doFilterInternal()",
					loggerUtils.populateLoggerData("doFilterInternal"), psd2Exception.getErrorInfo());
			if (LOG.isDebugEnabled()) {
				LOG.error("{\"Exception\":\"{}\",\"{}\",\"ErrorDetails\":\"{}\"}",
						"com.capgemini.psd2.scaconsenthelper.filters.CookieHandlerFilter.doFilterInternal()",
						loggerUtils.populateLoggerData("doFilterInternal"), e.getStackTrace(), e);
			}
			response.setContentType("application/json");
			response.setStatus(Integer.parseInt(psd2Exception.getErrorInfo().getStatusCode()));
			request.setAttribute(SCAConsentHelperConstants.EXCEPTION, psd2Exception.getErrorInfo());
			dropOff(request, response, pfInstanceData);
			request.setAttribute(PSD2Constants.CONSENT_FLOW_TYPE, flowType);
			request.getRequestDispatcher("/errors").forward(request, response);
		}
	}

	public abstract void validateRefreshBackScenarios(Cookie customSessionControllCookie, PFInstanceData pfInstanceData,HttpServletRequest request,HttpServletResponse response)
			throws ParseException, ServletException, IOException;

	private void dropOff(HttpServletRequest request, HttpServletResponse response, PFInstanceData pfInstanceData)
			throws ServletException, IOException {
		
		String resumePath = pfConfig.getResumePathBaseURL();
		String resumePathParam = request.getParameter(PFConstants.RESUME_PATH);
		String OAuthUrl = request.getParameter(SCAConsentHelperConstants.OAUTH_URL_PARAM);
		
		if (!(resumePathParam == null || resumePathParam.isEmpty())) {
			resumePath = resumePath.concat(resumePathParam);
		}
		else if(!(OAuthUrl == null || OAuthUrl.isEmpty())){
			resumePath = resumePath.concat(OAuthUrl);
		}
		
		resumePath = helperService.cancelJourney(resumePath, pfInstanceData);
		request.setAttribute(PFConstants.RESUME_PATH, resumePath);
	}

	private PFInstanceData populatePFInstanceData() {
		String pfInstanceId = null;
		String pfInstanceUserName = null;
		String pfInstanceUserPwd = null;
		PFInstanceData pfInstanceData = new PFInstanceData();
		if (flowType.equalsIgnoreCase("SCA")) {
			pfInstanceId = pfConfig.getScainstanceId();
			pfInstanceUserName = pfConfig.getScainstanceusername();
			pfInstanceUserPwd = pfConfig.getScainstancepassword();
		} else if (flowType.equalsIgnoreCase(IntentTypeEnum.AISP_INTENT_TYPE.getIntentType())) {
			pfInstanceId = pfConfig.getAispinstanceId();
			pfInstanceUserName = pfConfig.getAispinstanceusername();
			pfInstanceUserPwd = pfConfig.getAispinstancepassword();
		} else if (flowType.equalsIgnoreCase(IntentTypeEnum.PISP_INTENT_TYPE.getIntentType())) {
			pfInstanceId = pfConfig.getPispinstanceId();
			pfInstanceUserName = pfConfig.getPispinstanceusername();
			pfInstanceUserPwd = pfConfig.getAispinstancepassword();
		}
		pfInstanceData.setPfInstanceId(pfInstanceId);
		pfInstanceData.setPfInstanceUserName(pfInstanceUserName);
		pfInstanceData.setPfInstanceUserPwd(pfInstanceUserPwd);
		return pfInstanceData;
	}
	
	public String getFlowType(){
		return this.flowType;
	}

	@Override
	protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
		List<String> excludeUrlPatterns = new ArrayList<>();
		excludeUrlPatterns.add("/**/build/**");
		excludeUrlPatterns.add("/**/sca-ui/**");
		excludeUrlPatterns.add("/js/**");
		excludeUrlPatterns.add("/css/**");
		excludeUrlPatterns.add("/ui/staticContentGet");
		excludeUrlPatterns.add("/ui/staticContentGet");
		excludeUrlPatterns.add("/**/fonts/**");
		excludeUrlPatterns.add("/**/img/**");
		excludeUrlPatterns.add("/AISP.**");
		excludeUrlPatterns.add("/ext-libs/fraudnet/**");
		AntPathMatcher matcher = new AntPathMatcher();
		return excludeUrlPatterns.stream().anyMatch(p -> matcher.match(p, request.getServletPath()));
	}
	
	public Cookie createNewSessionCookie(PickupDataModel intentData) {
		String intentDataJSON = JSONUtilities.getJSONOutPutFromObject(intentData);
		JwtToken sessionAccessToken = JwtTokenUtility.createSessionJwtToken(tokenIssuer, sessiontimeout, tokenSigningKey,
				intentDataJSON,jweDecryptionKey);
		String sessionTokenStr = sessionAccessToken.getToken();
		Cookie cookie = new Cookie(SCAConsentHelperConstants.CUSTOM_SESSION_CONTROLL_COOKIE, null);
		cookie.setSecure(Boolean.TRUE);
		cookie.setHttpOnly(Boolean.TRUE);
		cookie.setValue(sessionTokenStr);
		cookie.setMaxAge(expiry);
		return cookie;
	}	
	
	public abstract void populateSecurityHeaders(HttpServletResponse response);
}
