package com.capgemini.psd2.tpp.block.routing.adapter.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.capgemini.psd2.aisp.adapter.TppBlockAdapter;
import com.capgemini.psd2.internal.apis.domain.ActionEnum;
import com.capgemini.psd2.internal.apis.domain.TppStatusDetails;
import com.capgemini.psd2.tpp.block.routing.adapter.routing.TppBlockAdapterFactory;

public class TppBlockRoutingAdapter implements TppBlockAdapter {

	@Autowired
	private TppBlockAdapterFactory adapters;

	@Value("${app.defaultAdapter}")
	private String defaultAdapter;

	@Override
	public TppStatusDetails fetchTppStatusDetails(String tppId) {
		TppBlockAdapter tppBlockAdapter = adapters.getAdapterInstance(defaultAdapter);
		return tppBlockAdapter.fetchTppStatusDetails(tppId);
	}

	@Override
	public void updateTppStatus(String tppId, ActionEnum action) {
		TppBlockAdapter tppBlockAdapter = adapters.getAdapterInstance(defaultAdapter);
		tppBlockAdapter.updateTppStatus(tppId, action);
	}

}
