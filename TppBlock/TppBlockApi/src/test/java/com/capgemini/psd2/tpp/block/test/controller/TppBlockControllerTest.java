package com.capgemini.psd2.tpp.block.test.controller;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.internal.apis.domain.ActionEnum;
import com.capgemini.psd2.internal.apis.domain.TppBlockInput;
import com.capgemini.psd2.tpp.block.controller.TppBlockController;
import com.capgemini.psd2.tpp.block.domain.TppBlockPathVariables;
import com.capgemini.psd2.tpp.block.service.TppBlockService;

public class TppBlockControllerTest {

	@Mock
	private TppBlockService tppBlockService;

	@InjectMocks
	TppBlockController obj = new TppBlockController();

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testUpdateTppStatus() {
		TppBlockInput tppBlockInput = new TppBlockInput();
		tppBlockInput.setAction(ActionEnum.BLOCK);
		tppBlockInput.setDescription("Dummy Description.");
		String tppId = "123456";

		// TppBlockController obj=new TppBlockController();
		doNothing().when(tppBlockService).updateTppStatus(anyString(), anyObject(), anyString());
		TppBlockPathVariables pathVariables = new TppBlockPathVariables();
		pathVariables.setTppId(tppId);
		obj.updateTppStatus(pathVariables, tppBlockInput);
	}
}
