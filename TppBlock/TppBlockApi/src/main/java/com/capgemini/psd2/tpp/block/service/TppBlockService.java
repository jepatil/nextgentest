package com.capgemini.psd2.tpp.block.service;

import com.capgemini.psd2.internal.apis.domain.ActionEnum;

@FunctionalInterface
public interface TppBlockService {
	public void updateTppStatus(String tppId, ActionEnum action, String description);
}
