package com.capgemini.psd2.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.PaymentSubmitPOST201Response;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;
import com.capgemini.psd2.test.config.PaymentSubmissionRetrieveITConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = PaymentSubmissionRetrieveITConfig.class)
@WebIntegrationTest
public class PaymentSubmissionRetrieveIT {
	
	@Value("${app.accessToken}")
	private String accessToken;

	@Value("${app.expiredToken}")
	private String expiredToken;
	
	@Value("${app.revokedToken}")
	private String revokedToken;
	
	@Value("${app.edgeserverURL}")
	private String edgeserverURL;
	
	@Value("${app.paymentSubmissionId}")
	private String paymentSubmissionId;
	
	@Value("${app.invalidPaymentSubmissionId}")
	private String invalidPaymentSubmissionId;
		
	@Autowired
	protected RestTemplate restTemplate;
	
	@Autowired
	protected RestClientSyncImpl restClientSync;

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testPaymentSubmissionRetrievePositive() {
		String url = edgeserverURL + "payment-submissions/" + paymentSubmissionId;
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", accessToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("client_id", "6443e15975554bce8099e35b88b40465");
		headers.add("client_secret", "7ca382cf4dff4d2dBCF034DBBA34A653");
		
		HttpEntity<?> requestEntity = new HttpEntity<>(headers);		
		ResponseEntity<PaymentSubmitPOST201Response> response = restTemplate.exchange(url, HttpMethod.GET,
				requestEntity, PaymentSubmitPOST201Response.class);

		assertNotNull(response);
	}

	@Test(expected = PSD2Exception.class)
	public void testPaymentSubmissionRetrieveWithoutAccessToken() {
		String url = edgeserverURL + "payment-submissions/" + paymentSubmissionId;
		HttpHeaders headers = new HttpHeaders();
		headers.add("x-fapi-financial-id", "1234");
		headers.add("client_id", "6443e15975554bce8099e35b88b40465");
		headers.add("client_secret", "7ca382cf4dff4d2dBCF034DBBA34A653");
		
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			restClientSync.callForGet(requestInfo, PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e){
			assertEquals(HttpStatus.UNAUTHORIZED.value(), Integer.parseInt(e.getErrorInfo().getStatusCode()));
			throw e;
		}
	}
	
	@Test(expected = PSD2Exception.class)
	public void testPaymentSubmissionRetrieveExpiredAccessToken() {
		String url = edgeserverURL + "payment-submissions/" + paymentSubmissionId;
		HttpHeaders headers = new HttpHeaders();
		headers.add("x-fapi-financial-id", "1234");
		headers.add("Authorization", expiredToken);
		headers.add("client_id", "6443e15975554bce8099e35b88b40465");
		headers.add("client_secret", "7ca382cf4dff4d2dBCF034DBBA34A653");
		
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			restClientSync.callForGet(requestInfo, PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e){
			assertEquals(HttpStatus.UNAUTHORIZED.value(), Integer.parseInt(e.getErrorInfo().getStatusCode()));
			throw e;
		}
	}
	
	@Test(expected = PSD2Exception.class)
	public void testPaymentSubmissionRetrieveRevokedAccessToken() {
		String url = edgeserverURL + "payment-submissions/" + paymentSubmissionId;
		HttpHeaders headers = new HttpHeaders();
		headers.add("x-fapi-financial-id", "1234");
		headers.add("Authorization", revokedToken);
		headers.add("client_id", "6443e15975554bce8099e35b88b40465");
		headers.add("client_secret", "7ca382cf4dff4d2dBCF034DBBA34A653");
		
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			restClientSync.callForGet(requestInfo, PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e){
			assertEquals(HttpStatus.UNAUTHORIZED.value(), Integer.parseInt(e.getErrorInfo().getStatusCode()));
			throw e;
		}
	}
	
	@Test(expected = PSD2Exception.class)
	public void testPaymentSubmissionRetrieveInvalidSubmissionId() {
		String url = edgeserverURL + "payment-submissions/" + invalidPaymentSubmissionId;
		HttpHeaders headers = new HttpHeaders();
		headers.add("x-fapi-financial-id", "1234");
		headers.add("Authorization", accessToken);
		headers.add("client_id", "6443e15975554bce8099e35b88b40465");
		headers.add("client_secret", "7ca382cf4dff4d2dBCF034DBBA34A653");
		
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			restClientSync.callForGet(requestInfo, PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e){
			assertEquals(HttpStatus.BAD_REQUEST.value(), Integer.parseInt(e.getErrorInfo().getStatusCode()));
			throw e;
		}
	}
	
	@Test(expected = PSD2Exception.class)
	public void testPaymentSubmissionRetrieveWithoutFinancialId() {
		String url = edgeserverURL + "payment-submissions/" + paymentSubmissionId;
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", accessToken);
		headers.add("client_id", "6443e15975554bce8099e35b88b40465");
		headers.add("client_secret", "7ca382cf4dff4d2dBCF034DBBA34A653");
		
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			restClientSync.callForGet(requestInfo, PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e){
			assertEquals(HttpStatus.BAD_REQUEST.value(), Integer.parseInt(e.getErrorInfo().getStatusCode()));
			throw e;
		}
	}
	
	

}
