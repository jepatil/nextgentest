
package com.capgemini.psd2.foundationservice.payment.submission.boi.adapter.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.foundationservice.payment.submission.boi.adapter.domain.PaymentInstruction;
import com.capgemini.psd2.foundationservice.payment.submission.boi.adapter.domain.ValidationPassed;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;

@Service
public class PaymentSubmissionFoundationServiceClientImpl implements PaymentSubmissionFoundationServiceClient {

	/** The rest client. */
	@Autowired
	@Qualifier("restClientFoundation")
	private RestClientSync restClient;

	@Override
	public ValidationPassed executePaymentSubmission(RequestInfo requestInfo, PaymentInstruction paymentInstruction, Class<ValidationPassed> response, HttpHeaders httpHeaders) {

		return restClient.callForPost(requestInfo, paymentInstruction, response, httpHeaders);

	}
}
