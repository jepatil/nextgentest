package com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.test;

import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.adapter.security.constants.AdapterSecurityConstants;
import com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.delegate.AuthenticationApplicationFoundationServiceDelegate;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.rest.client.model.RequestInfo;

@RunWith(SpringJUnit4ClassRunner.class)
public class AuthenticationApplicationFoundationServiceDelegateTest {

	@InjectMocks
	private AuthenticationApplicationFoundationServiceDelegate delegate;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	
	@Test
	public void testCreateRequestHeadersActual() {

		Map<String, String> params = new HashMap<String, String>();

		params.put(AdapterSecurityConstants.USER_HEADER, "test");
		params.put(AdapterSecurityConstants.CHANNELID_HEADER, "test");
		params.put(PSD2Constants.CORRELATION_ID, "test");
		
		HttpHeaders httpHeaders = new HttpHeaders();
		ReflectionTestUtils.setField(delegate, "userInReqHeader", "X-BOI-USER");
		ReflectionTestUtils.setField(delegate, "channelInReqHeader", "X-BOI-CHANNEL");
		ReflectionTestUtils.setField(delegate, "platformInReqHeader", "X-BOI-PLATFORM");
		ReflectionTestUtils.setField(delegate, "correlationIdInReqHeader", "X-CORRELATION-ID");
		ReflectionTestUtils.setField(delegate, "platform", "test");

		httpHeaders = delegate.createRequestHeaders(new RequestInfo(), params);

		assertNotNull(httpHeaders);
	}

}
