package com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.Authentication;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.adapter.exceptions.security.AdapterAuthenticationException;
import com.capgemini.psd2.adapter.security.constants.AdapterSecurityConstants;
import com.capgemini.psd2.adapter.utility.AdapterUtility;
import com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.AuthenticationApplicationFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.client.AuthenticationApplicationFoundationServiceClient;
import com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.delegate.AuthenticationApplicationFoundationServiceDelegate;


@RunWith(SpringJUnit4ClassRunner.class)
public class AuthenticationApplicationFoundationServiceAdapterTest{

	@InjectMocks
	private AuthenticationApplicationFoundationServiceAdapter authenticationApplicationFoundationServiceAdapter;

	@Mock
	private AuthenticationApplicationFoundationServiceClient authenticationApplicationFoundationServiceClient;

	@Mock
	private AuthenticationApplicationFoundationServiceDelegate authenticationApplicationFoundationServiceDelegate;
	
	@Mock
	private AdapterUtility adapterUtility;
	
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testAuthenticationApplicationFS() {
		
		PrincipalImpl principal = new PrincipalImpl("boi123");
		CredentialsImpl credentials = new CredentialsImpl("1234");
		AuthenticationImpl authentication = new AuthenticationImpl(principal, credentials);
		
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("X-BOI-USER", "header user");
		httpHeaders.add("X-BOI-CHANNEL", "BOL");
		httpHeaders.add("X-BOI-PLATFORM", "header platform");
		httpHeaders.add("X-CORRELATION-ID", "header correlation Id");
		
		Map<String, String> params = new HashMap<String, String>();
		params.put(AdapterSecurityConstants.CHANNELID_HEADER, "BOL");
		
	
		Mockito.when(authenticationApplicationFoundationServiceDelegate.createRequestHeaders(any(),any())).thenReturn(httpHeaders);
		
		Mockito.when(adapterUtility.retriveFoundationServiceURL(any())).thenReturn("http://localhost:8081/fs-login-business-service/services/loginServiceBusiness/channel/business/login");
		    
		Mockito.when(authenticationApplicationFoundationServiceClient.restTransportForAuthenticationApplication(any(), any(), any(), any())).thenReturn("Successfully authenticated");
	    
	    Authentication authenticationTest = authenticationApplicationFoundationServiceAdapter.authenticate(authentication, params);
	    
	    assertNotNull(authenticationTest);
	}
	
	@Test(expected = AdapterAuthenticationException.class)
	public void testAuthenticationApplicationFinalURLAsNull() {
				
		PrincipalImpl principal = new PrincipalImpl("boi123");
		CredentialsImpl credentials = new CredentialsImpl("1234");
		AuthenticationImpl authentication = new AuthenticationImpl(principal, credentials);
		
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("X-BOI-USER", "header user");
		httpHeaders.add("X-BOI-CHANNEL", "BOL");
		httpHeaders.add("X-BOI-PLATFORM", "header platform");
		httpHeaders.add("X-CORRELATION-ID", "header correlation Id");
		
		Map<String, String> params = new HashMap<String, String>();
		params.put(AdapterSecurityConstants.CHANNELID_HEADER, "BOL");
		
		Mockito.when(authenticationApplicationFoundationServiceDelegate.createRequestHeaders(any(),any())).thenReturn(httpHeaders);
		
		Mockito.when(adapterUtility.retriveFoundationServiceURL(any())).thenReturn(null);
		
		authenticationApplicationFoundationServiceAdapter.authenticate(authentication, params);
	}
	
	
	
	
}
