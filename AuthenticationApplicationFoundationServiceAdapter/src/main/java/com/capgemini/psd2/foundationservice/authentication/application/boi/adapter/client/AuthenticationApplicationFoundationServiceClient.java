
package com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.domain.AuthenticationRequest;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;

@Component
public class AuthenticationApplicationFoundationServiceClient {

	@Autowired
	@Qualifier("restClientSecurityFoundation")
	private RestClientSync restClient;

	public String restTransportForAuthenticationApplication(RequestInfo reqInfo, AuthenticationRequest authenticationRequest, Class<String> response, HttpHeaders headers) {
		return restClient.callForPost(reqInfo, authenticationRequest, response, headers);

	}

}
