
package com.capgemini.psd2.foundationservice.authentication.application.boi.adapter;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.security.AdapterAuthenticationException;
import com.capgemini.psd2.adapter.exceptions.security.SecurityAdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.security.constants.AdapterSecurityConstants;
import com.capgemini.psd2.adapter.utility.AdapterUtility;
import com.capgemini.psd2.authentication.adapter.AuthenticationAdapter;
import com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.client.AuthenticationApplicationFoundationServiceClient;
import com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.delegate.AuthenticationApplicationFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.domain.AuthenticationRequest;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class AuthenticationApplicationFoundationServiceAdapter implements AuthenticationAdapter {

	@Autowired
	private AdapterUtility adapterUtility;
	

	@Autowired
	private AuthenticationApplicationFoundationServiceDelegate authenticationApplicationFoundationServiceDelegate;

	@Autowired
	private AuthenticationApplicationFoundationServiceClient authenticationApplicationFoundationServiceClient;

	
	@Override
	public <T> T authenticate(T authentication, Map<String, String> params) {
		
		AuthenticationRequest authenticationRequest = new AuthenticationRequest();
		Authentication authenticationObject = (Authentication)authentication;
		authenticationRequest.setUserName(authenticationObject.getPrincipal().toString());
		authenticationRequest.setPassword(authenticationObject.getCredentials().toString());
		
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders httpHeaders = authenticationApplicationFoundationServiceDelegate.createRequestHeaders(requestInfo, params);
		
		String channelId = params.get(AdapterSecurityConstants.CHANNELID_HEADER);
		String finalURL = adapterUtility.retriveFoundationServiceURL(channelId);
		if(NullCheckUtils.isNullOrEmpty(finalURL)){
			throw AdapterAuthenticationException.populateAuthenticationFailedException("No valid foundation url found.",SecurityAdapterErrorCodeEnum.TECHNICAL_ERROR);
		}
		requestInfo.setUrl(finalURL);
		authenticationApplicationFoundationServiceClient.restTransportForAuthenticationApplication(requestInfo, authenticationRequest, String.class, httpHeaders);		  
		return authentication;
	}

}
