/*************************************************************************
 * 
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 */
package com.capgemini.psd2.security.consent.pisp.view.controllers;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;
import javax.naming.directory.BasicAttributes;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriUtils;

import com.capgemini.psd2.aisp.domain.Account;
import com.capgemini.psd2.aisp.domain.AccountGETResponse;
import com.capgemini.psd2.aisp.domain.Data2;
import com.capgemini.psd2.aisp.domain.Data2Account;
import com.capgemini.psd2.aisp.domain.Data2Servicer;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.fraudsystem.request.handler.impl.FraudSystemRequestMapping;
import com.capgemini.psd2.fraudsystem.utilities.FraudSystemUtilities;
import com.capgemini.psd2.integration.adapter.TPPInformationAdaptor;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.mask.DataMask;
import com.capgemini.psd2.pisp.domain.CreditorAccount;
import com.capgemini.psd2.pisp.domain.DebtorAccount;
import com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum;
import com.capgemini.psd2.pisp.domain.DebtorAgent;
import com.capgemini.psd2.pisp.domain.PaymentSetupInitiationInstructedAmount;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponseInitiation;
import com.capgemini.psd2.pisp.domain.RemittanceInformation;
import com.capgemini.psd2.scaconsenthelper.config.CdnConfig;
import com.capgemini.psd2.scaconsenthelper.config.helpers.SCAConsentHelper;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.security.consent.pisp.helpers.PispConsentCreationDataHelper;
import com.capgemini.psd2.security.constants.PSD2SecurityConstants;
import com.capgemini.psd2.security.exceptions.PSD2SecurityException;
import com.capgemini.psd2.security.exceptions.SCAConsentErrorCodeEnum;
import com.capgemini.psd2.tppinformation.adaptor.ldap.constants.TPPInformationConstants;
import com.capgemini.psd2.ui.content.utility.controller.UIStaticContentUtilityController;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;

/**
 * ConsentView Controller
 * 
 * @author Capgemini
 *
 */
@Controller
public class PispConsentViewController {

	@Value("${app.edgeserverhost}")
	private String edgeserverhost;
	
	@Value("${spring.application.name}")
	private String applicationName;

	@Value("${app.fraudSystem.resUrl}")
	private String resUrl;

	@Value("${app.fraudSystem.hdmUrl}")
	private String hdmUrl;

	@Value("${app.fraudSystem.hdmInputName}")
	private String hdmInputName;

	@Value("${app.fraudSystem.jsc-file-path}")
	private String jscFilePath;
	@Autowired
	private TPPInformationAdaptor tppInformationAdaptor;
	
	@Autowired
	private UIStaticContentUtilityController uiController;
	
	@Autowired
	private RequestHeaderAttributes requestHeaders;

	@Autowired
	private PispConsentCreationDataHelper consentCreationDataHelper;
	

	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private HttpServletResponse response;
	
	@Autowired
	private DataMask dataMask;
	
	@Value("${cdn.baseURL}")
	private String cdnBaseURL;

	@RequestMapping(value="/pisp/home")
	public ModelAndView homePage(Map<String, Object> model){
		SCAConsentHelper.populateSecurityHeaders(response);
		
		String homeScreen = "/customerconsentview";
		String requestUrl  = request.getRequestURI();
		requestUrl = requestUrl.replace("/home", homeScreen);
		model.put("redirectUrl",edgeserverhost.concat("/").concat(applicationName).concat(requestUrl).concat("?").concat(request.getQueryString()));
		return new ModelAndView("homePage", model);
    }

	/**
	 * customerconsentview end-point of consent application gets invoked by
	 * OAuth2 server with some params like - call back URI of OAuth2 server & ID
	 * token which contains the identity information of the customer. It
	 * collects the data for allowed scopes of the caller AISP & fetches
	 * accounts list of the current customer by using the user-id information
	 * taken from the ID token (SaaS Server issued ID token) which gets relayed
	 * by the OAuth server to consent app.
	 * 
	 * @param model
	 * @param oAuthUrl
	 * @param idToken
	 * @return
	 * @throws NamingException 
	 */
	@RequestMapping(value = "/pisp/customerconsentview")
	public ModelAndView consentView(Map<String, Object> model) throws NamingException {
		boolean isAccounSelected = false;
		
		PickupDataModel pickupDataModel = SCAConsentHelper.populatePickupDataModel(request);
		
		AccountGETResponse customerAccountList = consentCreationDataHelper
				.retrieveCustomerAccountListInfo(pickupDataModel.getUserId(),pickupDataModel.getClientId(),
						pickupDataModel.getIntentTypeEnum().getIntentType(),requestHeaders.getCorrelationId(),pickupDataModel.getChannelId());

		Object tppInformationObj = tppInformationAdaptor.fetchTPPInformation(pickupDataModel.getClientId());
		String tppApplicationName = tppInformationAdaptor.fetchApplicationName(pickupDataModel.getClientId());
		
		PaymentSetupPOSTResponse setUpResponseData = consentCreationDataHelper.retrievePaymentSetupData(pickupDataModel.getIntentId());
		
		if (setUpResponseData.getData() != null && setUpResponseData.getData().getInitiation() != null
				&& !NullCheckUtils.isNullOrEmpty(setUpResponseData.getData().getInitiation().getDebtorAccount())
				&& !NullCheckUtils.isNullOrEmpty(setUpResponseData.getData().getInitiation().getDebtorAccount().getIdentification())) {

			isAccounSelected = true;
			customerAccountList = validateAccountWithAccountList(setUpResponseData, customerAccountList);
		}
		
		PaymentSetupPOSTResponse setUpResponseDataforUI = populateLimitedSetUpDataforUI(setUpResponseData);
		
        String customerAccount = dataMask.maskResponseGenerateString(customerAccountList, "account");
		model.put(PSD2SecurityConstants.CUSTOMER_ACCOUNT_LIST, customerAccount);
		model.put(PSD2SecurityConstants.CONSENT_SETUP_DATA, JSONUtilities.getJSONOutPutFromObject(setUpResponseDataforUI));
		model.put(PSD2Constants.CONSENT_FLOW_TYPE, pickupDataModel.getIntentTypeEnum().getIntentType());
		model.put(PSD2Constants.JS_MSG, uiController.retrieveUiParamValue(PSD2Constants.ERROR_MESSAGE,PSD2Constants.JS_MSG));
		model.put(PSD2Constants.CO_RELATION_ID, requestHeaders.getCorrelationId());

		JSONObject tppInfoJson = new JSONObject();

		if(tppInformationObj != null && tppInformationObj instanceof BasicAttributes){
			BasicAttributes tppInfoAttributes = (BasicAttributes)tppInformationObj;
			String legalEntityName = returnLegalEntityName(tppInfoAttributes);
			model.put(PSD2SecurityConstants.TPP_NAME, legalEntityName);
			tppInfoJson.put(PSD2SecurityConstants.TPP_NAME, legalEntityName);
			
		}
		tppInfoJson.put(PSD2SecurityConstants.APP_NAME, tppApplicationName);
		model.put(PSD2SecurityConstants.TPP_INFO, tppInfoJson.toString());

		model.put(PSD2SecurityConstants.ACCOUNT_SELECTED, isAccounSelected);
		model.put(PSD2SecurityConstants.USER_ID, pickupDataModel.getUserId());
		model.put(PSD2Constants.JS_MSG,
				uiController.retrieveUiParamValue(PSD2Constants.ERROR_MESSAGE, PSD2Constants.JS_MSG));
		model.put(PSD2Constants.CO_RELATION_ID, requestHeaders.getCorrelationId());
		model.put(PSD2Constants.APPLICATION_NAME, applicationName);

		JSONObject fraudHdmInfo = new JSONObject();
		try{
		fraudHdmInfo.put(PSD2SecurityConstants.RES_URL, UriUtils.encode(resUrl, StandardCharsets.UTF_8.toString()));
		fraudHdmInfo.put(PSD2SecurityConstants.HDM_URL, UriUtils.encode(hdmUrl, StandardCharsets.UTF_8.toString()));
		} catch (UnsupportedEncodingException e) {
			throw PSD2SecurityException.populatePSD2SecurityException(e.getMessage(), SCAConsentErrorCodeEnum.TECHNICAL_ERROR);
		}		
		fraudHdmInfo.put(PSD2SecurityConstants.HDM_INPUT_NAME, hdmInputName);
		model.put(PSD2SecurityConstants.FRAUD_HDM_INFO, fraudHdmInfo.toString());
		model.put(PSD2SecurityConstants.JSC_FILE_PATH, jscFilePath);
		model.put(FraudSystemRequestMapping.FS_HEADERS, FraudSystemUtilities.populateFraudSystemHeaders(request));
		model.put(PSD2Constants.UICONTENT, uiController.getConfigVariable());
		model.putAll(CdnConfig.populateCdnAttributes());
		return new ModelAndView("index", model);
	}

	private String returnLegalEntityName(BasicAttributes basicAttributes) throws NamingException{
		String legalEntityName = getAttributeValue(basicAttributes, TPPInformationConstants.LEGAL_ENTITY_NAME);
		return legalEntityName;
	}
	
	private String getAttributeValue(BasicAttributes tppApplication, String ldapAttr) throws NamingException {
		String attributeValue = null;
		if (tppApplication.get(ldapAttr) != null && tppApplication.get(ldapAttr).get() != null) {
			attributeValue = tppApplication.get(ldapAttr).get().toString();
		}
		return attributeValue;
	}
	private AccountGETResponse validateAccountWithAccountList(PaymentSetupPOSTResponse setUpResponseData,
			AccountGETResponse accountGETResponse) {
		boolean isAccountMatch = false;
		DebtorAccount debtorAcccount = setUpResponseData.getData().getInitiation().getDebtorAccount();
		DebtorAgent debtorAgent = setUpResponseData.getData().getInitiation().getDebtorAgent();
		if(accountGETResponse != null && ! accountGETResponse.getData().getAccount().isEmpty() ){
			for(Account account:accountGETResponse.getData().getAccount()){
				if(account instanceof PSD2Account){
					if(!(debtorAcccount.getSchemeName().equals(SchemeNameEnum.IBAN))){
						if (account.getAccount().getIdentification().equals(debtorAcccount.getIdentification())) {
							isAccountMatch = true;
							accountGETResponse = populateAccountData(account,debtorAcccount,debtorAgent);
							break;
						}
					} else {
						Map<String, String> additionalInfo = ((PSD2Account) account).getAdditionalInformation();
						if (debtorAgent != null && debtorAgent.getIdentification() != null && additionalInfo != null
								&& !additionalInfo.isEmpty()
								&& additionalInfo.get(SchemeNameEnum.IBAN.name()) != null) {
							if (additionalInfo.get(SchemeNameEnum.IBAN.name())
									.equals(debtorAcccount.getIdentification())
									&& additionalInfo.get(Data2Servicer.SchemeNameEnum.BICFI.name())
											.equals(debtorAgent.getIdentification())) {
								isAccountMatch = true;
								accountGETResponse = populateAccountData(account,debtorAcccount,debtorAgent);
								break;
							}
						}
					}
				}
			}
			if(!isAccountMatch){
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.SELECTED_ACCOUNTS_NOT_OWNED_BY_CUSTOMER);
			}
		} else {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.SELECTED_ACCOUNTS_NOT_OWNED_BY_CUSTOMER);
		}
		return accountGETResponse;
	}

	private AccountGETResponse populateAccountData(Account account, DebtorAccount debtorAcccount,
			DebtorAgent debtorAgent) {
		List<Account> accountData = new ArrayList<>();
		account.getAccount().setName(debtorAcccount.getName());
		account.getAccount().setSecondaryIdentification(debtorAcccount.getSecondaryIdentification());
		account.getAccount().setIdentification(debtorAcccount.getIdentification());
		account.getAccount().setSchemeName(Data2Account.SchemeNameEnum.valueOf(debtorAcccount.getSchemeName().name()));
		if(debtorAgent !=null){
			Data2Servicer servicer = new Data2Servicer();
			account.setServicer(servicer);
			account.getServicer().setIdentification(debtorAgent.getIdentification());
			account.getServicer()
					.setSchemeName(Data2Servicer.SchemeNameEnum.valueOf(debtorAgent.getSchemeName().name()));
		}
		accountData.add(account);
		Data2 data2 = new Data2();
		data2.setAccount(accountData);
		AccountGETResponse accountGETResponse = new AccountGETResponse();
		accountGETResponse .setData(data2);
		return accountGETResponse ;
	}
	
private PaymentSetupPOSTResponse populateLimitedSetUpDataforUI(PaymentSetupPOSTResponse setUpResponseData) {
		
		PaymentSetupPOSTResponse pmtPostResp = new PaymentSetupPOSTResponse();
		PaymentSetupResponse pmtSetupResp = new PaymentSetupResponse();
		PaymentSetupResponseInitiation pmtSetupRepsInit = new PaymentSetupResponseInitiation();
		PaymentSetupInitiationInstructedAmount resultInstAmount = new PaymentSetupInitiationInstructedAmount();
		CreditorAccount resultCreditorAccount = new CreditorAccount();
		RemittanceInformation resultRemittanceInfo = new RemittanceInformation();
		
		if(setUpResponseData != null && setUpResponseData.getData() != null){
			PaymentSetupResponseInitiation initiation = setUpResponseData.getData().getInitiation();
			if(initiation != null){
				PaymentSetupInitiationInstructedAmount instructedAmount = initiation.getInstructedAmount();
				if(instructedAmount != null){
					resultInstAmount.setAmount(instructedAmount.getAmount());
					resultInstAmount.setCurrency(instructedAmount.getCurrency());
				}
				
				CreditorAccount creditorAccount = initiation.getCreditorAccount();
				if(creditorAccount != null){
					resultCreditorAccount.setName(creditorAccount.getName());
					resultCreditorAccount.setSecondaryIdentification(creditorAccount.getSecondaryIdentification());
				}
				
				RemittanceInformation remittanceInformation = initiation.getRemittanceInformation();
				if(remittanceInformation != null){
					resultRemittanceInfo.setReference(remittanceInformation.getReference());
					resultRemittanceInfo.setUnstructured(remittanceInformation.getUnstructured());
				}
			}
		}
		
		pmtSetupRepsInit.setInstructedAmount(resultInstAmount);
		pmtSetupRepsInit.setCreditorAccount(resultCreditorAccount);
		pmtSetupRepsInit.setRemittanceInformation(resultRemittanceInfo);
		pmtSetupResp.setInitiation(pmtSetupRepsInit);
		pmtPostResp.setData(pmtSetupResp);
		
		return pmtPostResp;
	}
}
