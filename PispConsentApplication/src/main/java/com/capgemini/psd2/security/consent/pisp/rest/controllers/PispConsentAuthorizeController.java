/*************************************************************************
 * 
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 */
package com.capgemini.psd2.security.consent.pisp.rest.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;

import com.capgemini.psd2.aisp.domain.AccountGETResponse;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.consent.domain.PSD2AccountsAdditionalInfo;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.integration.adapter.TPPInformationAdaptor;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.PaymentSetupValidationResponse;
import com.capgemini.psd2.scaconsenthelper.config.PFConfig;
import com.capgemini.psd2.scaconsenthelper.config.helpers.ConsentAuthorizationHelper;
import com.capgemini.psd2.scaconsenthelper.config.helpers.SCAConsentHelper;
import com.capgemini.psd2.scaconsenthelper.constants.PFConstants;
import com.capgemini.psd2.scaconsenthelper.models.DropOffResponse;
import com.capgemini.psd2.scaconsenthelper.models.IntentTypeEnum;
import com.capgemini.psd2.scaconsenthelper.models.PFInstanceData;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.scaconsenthelper.services.SCAConsentHelperService;
import com.capgemini.psd2.security.consent.pisp.helpers.PispConsentCreationDataHelper;
import com.capgemini.psd2.security.constants.PSD2SecurityConstants;
import com.capgemini.psd2.utilities.JSONUtilities;

/**
 * ConsentAuthorize Controller
 * 
 * @author Capgemini
 */

@RestController
public class PispConsentAuthorizeController {


	@Autowired
	private PispConsentCreationDataHelper consentCreationDataHelper;

	@Autowired
	private RequestHeaderAttributes requestHeaderAttributes;
	
	@Autowired
	@Qualifier("pispConsentAuthorizationHelper")
	private ConsentAuthorizationHelper consentAuthorizationHelper;
	
	@Autowired
	private TPPInformationAdaptor tppInformationAdaptor;
	
	@Autowired
	private SCAConsentHelperService helperService;

	@Autowired
	private PFConfig pfConfig;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private HttpServletResponse response;
	

	/**
	 * This method is used for creating the consent
	 * 
	 * @param contract-
	 *            ConsentTemplate
	 * @return ConsentResponse - Response after creating consent
	 * @throws NamingException 
	 */
	@RequestMapping(value = "/pisp/consent", method = RequestMethod.POST)
	public String createConsent(ModelAndView model, @RequestBody PSD2AccountsAdditionalInfo accountAdditionalInfo,@RequestParam String resumePath) throws NamingException {

		PSD2Account customerAccount = null;
		String headers = accountAdditionalInfo.getHeaders();

		if (accountAdditionalInfo != null) {
			customerAccount = accountAdditionalInfo.getAccountdetails().get(0);
		}
		if (customerAccount == null) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_ACCOUNTS_SELECTED_BY_CUSTOMER);
		}

		PickupDataModel pickupDataModel = SCAConsentHelper.populatePickupDataModel(request);
		
		AccountGETResponse accountGETResponse = consentCreationDataHelper.retrieveCustomerAccountListInfo(pickupDataModel.getUserId(),pickupDataModel.getClientId(),IntentTypeEnum.PISP_INTENT_TYPE.getIntentType(),requestHeaderAttributes.getCorrelationId(),pickupDataModel.getChannelId());
		PSD2Account unmaskedAccount = (PSD2Account)consentAuthorizationHelper.matchAccountFromList(accountGETResponse, customerAccount);
		
		Object tppInformationObj = tppInformationAdaptor.fetchTPPInformation(pickupDataModel.getClientId());
		String tppApplicationName = tppInformationAdaptor.fetchApplicationName(pickupDataModel.getClientId());
		
		consentCreationDataHelper.createConsent(unmaskedAccount, pickupDataModel.getUserId(), pickupDataModel.getClientId(), pickupDataModel.getIntentId(), pickupDataModel.getChannelId(), headers, tppApplicationName, tppInformationObj);
				
		DropOffResponse dropOffResponse = helperService.dropOffOnConsentSubmission(pickupDataModel,pfConfig.getPispinstanceId(),pfConfig.getPispinstanceusername(),pfConfig.getPispinstancepassword());
		
		resumePath = pfConfig.getResumePathBaseURL().concat(resumePath);

		String redirectURI = UriComponentsBuilder.fromHttpUrl(resumePath).queryParam(PFConstants.REF, dropOffResponse.getRef()).toUriString();
				
		model.addObject(PSD2SecurityConstants.REDIRECT_URI_MODEL_ATTRIBUTE,redirectURI);
		SCAConsentHelper.invalidateCookie(response);		
		return JSONUtilities.getJSONOutPutFromObject(model);
	}
	
	@RequestMapping(value = "/pisp/cancelConsent", method = RequestMethod.PUT)
	public String cancelConsent(ModelAndView model,
			@RequestParam Map<String, String> paramsMap){
		
		String serverErrorFlag = paramsMap.get(PSD2Constants.SERVER_ERROR_FLAG_ATTR);
		
		
		PickupDataModel pickupDataModel = SCAConsentHelper.populatePickupDataModel(request);
		
		paramsMap.put(PSD2Constants.USER_IN_REQ_HEADER, pickupDataModel.getUserId());
		paramsMap.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, pickupDataModel.getChannelId());
		
		consentCreationDataHelper.cancelPaymentSetup(pickupDataModel.getIntentId(),paramsMap);
		
		String resumePath  = paramsMap.get(PFConstants.RESUME_PATH);
		
		resumePath = pfConfig.getResumePathBaseURL().concat(resumePath);
		
		if (serverErrorFlag != null && !serverErrorFlag.isEmpty() && Boolean.valueOf(serverErrorFlag)) {
			resumePath = UriComponentsBuilder.fromHttpUrl(resumePath).queryParam(PFConstants.REF, null).toUriString();
		}else{
			PFInstanceData pfInstanceData = new PFInstanceData();
			pfInstanceData.setPfInstanceId(pfConfig.getPispinstanceId());
			pfInstanceData.setPfInstanceUserName(pfConfig.getPispinstanceusername());
			pfInstanceData.setPfInstanceUserPwd(pfConfig.getPispinstancepassword());
			resumePath = helperService.cancelJourney(resumePath,pfInstanceData);
		}
		model.addObject(PSD2SecurityConstants.REDIRECT_URI_MODEL_ATTRIBUTE,resumePath);
		SCAConsentHelper.invalidateCookie(response);				
		return JSONUtilities.getJSONOutPutFromObject(model);
	}
	
	@RequestMapping(value ="/pisp/preAuthorisation",method =RequestMethod.POST)
	public String preAuthorisation(ModelAndView model,@RequestBody PSD2Account account,@RequestParam Map<String,String> paramMap){

		if(account == null) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_ACCOUNTS_SELECTED_BY_CUSTOMER);
		}
		PickupDataModel pickupDataModel = SCAConsentHelper.populatePickupDataModel(request);

		AccountGETResponse accountGETResponse = consentCreationDataHelper.retrieveCustomerAccountListInfo(pickupDataModel.getUserId(),pickupDataModel.getClientId(),IntentTypeEnum.PISP_INTENT_TYPE.getIntentType(),requestHeaderAttributes.getCorrelationId(),pickupDataModel.getChannelId());

		PSD2Account unmaskedAccount = (PSD2Account)consentAuthorizationHelper.matchAccountFromList(accountGETResponse, account);
		Map<String,String> paramsMap = new HashMap<String, String>();
	    paramsMap.put(PSD2Constants.CONSENT_FLOW_TYPE, pickupDataModel.getIntentTypeEnum().getIntentType());
        paramsMap.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, pickupDataModel.getChannelId());
        paramsMap.put(PSD2Constants.USER_IN_REQ_HEADER,pickupDataModel.getUserId());

		PaymentSetupValidationResponse preAuthorisationResponse =consentCreationDataHelper.validatePreAuthorisation(unmaskedAccount,pickupDataModel.getIntentId(),paramsMap);
		if(preAuthorisationResponse == null || PSD2SecurityConstants.REJECTED_STATUS.equals(preAuthorisationResponse.getPaymentSetupValidationStatus())){
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PRE_AUTHORISATION_FAILED);
		}
		model.addObject(PSD2SecurityConstants.PRE_AUTHORISATION_RESPONSE, preAuthorisationResponse.getPaymentSetupValidationStatus());
		return JSONUtilities.getJSONOutPutFromObject(model);
	}
}