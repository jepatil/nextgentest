package com.capgemini.psd2.security.consent.pisp.test.helpers;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.capgemini.psd2.aisp.domain.Data2Account;
import com.capgemini.psd2.aisp.domain.Data2Servicer;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.security.consent.pisp.helpers.PispConsentAuthorizationHelper;

public class PispConsentAuthorizationHelperTest {
	
	private PispConsentAuthorizationHelper pispConsentAutorizationHelper = new PispConsentAuthorizationHelper();
	
	@Test
	public void populateAccountWithUnmaskedValuesTest() {
		Map<String,String> map=new HashMap<>();
		
		PSD2Account accountwithoutmask = new PSD2Account();
		Data2Account acct = new Data2Account();
		acct.setIdentification("12345");
		acct.setSchemeName(com.capgemini.psd2.aisp.domain.Data2Account.SchemeNameEnum.IBAN);
		Data2Servicer servicer = new Data2Servicer();
		servicer.setIdentification("12345");
		accountwithoutmask.setServicer(servicer);
		accountwithoutmask.setAccount(acct);
		accountwithoutmask.setCurrency("EUR");
		accountwithoutmask.setNickname("John");
		accountwithoutmask.setAdditionalInformation(map);
		
		
		PSD2Account accountwithmask = new PSD2Account();
		Data2Account acct1 = new Data2Account();
		acct1.setIdentification("12345");
		acct1.setSchemeName(com.capgemini.psd2.aisp.domain.Data2Account.SchemeNameEnum.IBAN);
		Data2Servicer servicer1 = new Data2Servicer();
		servicer1.setIdentification("12345");
		accountwithmask.setServicer(servicer);
		accountwithmask.setAccount(acct);
		accountwithmask.setCurrency("EUR");
		accountwithmask.setNickname("John");
		accountwithmask.setAdditionalInformation(map);
		
		pispConsentAutorizationHelper.populateAccountwithUnmaskedValues(accountwithmask, accountwithoutmask);
		
		acct.setSchemeName(com.capgemini.psd2.aisp.domain.Data2Account.SchemeNameEnum.SORTCODEACCOUNTNUMBER);
		acct1.setSchemeName(com.capgemini.psd2.aisp.domain.Data2Account.SchemeNameEnum.SORTCODEACCOUNTNUMBER);
		accountwithoutmask.setServicer(servicer);
		accountwithoutmask.setAccount(acct);
		accountwithoutmask.setCurrency("EUR");
		accountwithoutmask.setNickname("John");
		accountwithoutmask.setAdditionalInformation(map);
		servicer1.setIdentification("12345");
		accountwithmask.setServicer(servicer);
		accountwithmask.setAccount(acct);
		accountwithmask.setCurrency("EUR");
		accountwithmask.setNickname("John");
		accountwithmask.setAdditionalInformation(map);
		pispConsentAutorizationHelper.populateAccountwithUnmaskedValues(accountwithmask, accountwithoutmask);
		
	}

}
