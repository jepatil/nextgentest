package com.capgemini.psd2.security.consent.pisp.test.mock.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.capgemini.psd2.aisp.domain.Account;
import com.capgemini.psd2.aisp.domain.AccountGETResponse;
import com.capgemini.psd2.aisp.domain.Data2;
import com.capgemini.psd2.aisp.domain.Data2Account;
import com.capgemini.psd2.aisp.domain.Data2Servicer;
import com.capgemini.psd2.consent.domain.CustomerAccountInfo;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.DebtorAccount;
import com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum;
import com.capgemini.psd2.pisp.domain.DebtorAgent;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse.StatusEnum;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponseInitiation;
import com.capgemini.psd2.pisp.validation.PispUtilities;

public class PispConsentApplicationMockdata {

	public static CustomPaymentSetupPOSTResponse mockPaymentSetupPOSTResponse;
	public static List<CustomerAccountInfo> mockCustomerAccountInfoList;
	
	public static CustomPaymentSetupPOSTResponse getPaymentSetupPOSTResponseInvalidStatus() {
		mockPaymentSetupPOSTResponse = new CustomPaymentSetupPOSTResponse();
        PaymentSetupResponse data =  new PaymentSetupResponse();
		data.setStatus(StatusEnum.ACCEPTEDCUSTOMERPROFILE);
		data.setCreationDateTime(PispUtilities.getCurrentDateInISOFormat());
		PaymentSetupResponseInitiation initiation = new PaymentSetupResponseInitiation();
		initiation.setEndToEndIdentification("232445646");
		data.setInitiation(initiation );
		data.setPaymentId("345654");
		mockPaymentSetupPOSTResponse.setData(data);
		mockPaymentSetupPOSTResponse.setRisk(null);
		return mockPaymentSetupPOSTResponse;
	}
	
	public static CustomPaymentSetupPOSTResponse getPaymentSetupPOSTResponse() {
		mockPaymentSetupPOSTResponse = new CustomPaymentSetupPOSTResponse();
        PaymentSetupResponse data =  new PaymentSetupResponse();
		data.setStatus(StatusEnum.ACCEPTEDTECHNICALVALIDATION);
		data.setCreationDateTime(PispUtilities.getCurrentDateInISOFormat());
		PaymentSetupResponseInitiation initiation = new PaymentSetupResponseInitiation();
		initiation.setEndToEndIdentification("232445646");
		data.setInitiation(initiation );
		data.setPaymentId("345654");
		mockPaymentSetupPOSTResponse.setData(data);
		mockPaymentSetupPOSTResponse.setRisk(null);
		return mockPaymentSetupPOSTResponse;
	}
	
	public static CustomPaymentSetupPOSTResponse getPaymentSetupPOSTResponsewithAccounts() {
		mockPaymentSetupPOSTResponse = new CustomPaymentSetupPOSTResponse();
        PaymentSetupResponse data =  new PaymentSetupResponse();
		data.setStatus(StatusEnum.ACCEPTEDTECHNICALVALIDATION);
		data.setCreationDateTime(PispUtilities.getCurrentDateInISOFormat());
		PaymentSetupResponseInitiation initiation = new PaymentSetupResponseInitiation();
		initiation.setEndToEndIdentification("1234566");
		DebtorAccount debtorAccount =  new DebtorAccount();
		debtorAccount.setIdentification("1234566");
		debtorAccount.setSchemeName(DebtorAccount.SchemeNameEnum.IBAN);
		initiation.setDebtorAccount(debtorAccount);
		DebtorAgent debtorAgent = new DebtorAgent();
		debtorAgent.setIdentification("1234566");
		debtorAgent.setSchemeName(DebtorAgent.SchemeNameEnum.BICFI);
		initiation.setDebtorAgent(debtorAgent );
		data.setInitiation(initiation );
		data.setPaymentId("1234566");
		mockPaymentSetupPOSTResponse.setData(data);
		mockPaymentSetupPOSTResponse.setRisk(null);
		return mockPaymentSetupPOSTResponse;
	}

	public static Account getAccountData(){
		PSD2Account account=  new PSD2Account();
		account.setAccountId("2343253464");
		account.setCurrency("EUR");
		account.setNickname("John");
		Data2Servicer servicer = new Data2Servicer();
		servicer.setIdentification("35475687");
		Data2Account acct = new Data2Account();
		acct.setIdentification("354645");
		acct.setSchemeName(Data2Account.SchemeNameEnum.IBAN);
		account.setAccount(acct);
		account.setServicer(servicer);
		return account;	
	}
	
	public static Account getAccountDataNull(){
	//	PSD2Account account=  new PSD2Account();
		return null;
	}
	
	public static List<CustomerAccountInfo> getCustomerAccountInfoList(){
		mockCustomerAccountInfoList = new ArrayList<>();
		CustomerAccountInfo customerAccountInfo1 = new CustomerAccountInfo();
		customerAccountInfo1.setAccountName("John Doe");
		customerAccountInfo1.setUserId("1234");
		customerAccountInfo1.setAccountNumber("10203345");
		customerAccountInfo1.setAccountNSC("SC802001");
		customerAccountInfo1.setCurrency("EUR");
		customerAccountInfo1.setNickname("John");
		customerAccountInfo1.setAccountType("checking");

		CustomerAccountInfo customerAccountInfo2 = new CustomerAccountInfo();
		customerAccountInfo2.setAccountName("Tiffany Doe");
		customerAccountInfo2.setUserId("1234");
		customerAccountInfo2.setAccountNumber("10203346");
		customerAccountInfo2.setAccountNSC("SC802002");
		customerAccountInfo2.setCurrency("GRP");
		customerAccountInfo2.setNickname("Tiffany");
		customerAccountInfo2.setAccountType("savings");

		mockCustomerAccountInfoList.add(customerAccountInfo1);
		mockCustomerAccountInfoList.add(customerAccountInfo2);

		return mockCustomerAccountInfoList;
	}
	
	
	public static AccountGETResponse getCustomerAccountInfo(){
		AccountGETResponse mockAccountGETResponse =new AccountGETResponse();
		List<Account> accountData = new ArrayList<>();
		PSD2Account acct = new PSD2Account();
		acct.setAccountId("14556236");
		acct.setCurrency("EUR");
		acct.setNickname("John");
		acct.setAccountType("savings");
		Data2Servicer servicer = new Data2Servicer();
		servicer.setIdentification("12345");
		Data2Account account = new Data2Account();
		account.setIdentification("1245676");
		acct.setAccount(account );
		acct.setServicer(servicer );
		
		PSD2Account accnt = new PSD2Account();
		accnt.setAccountId("14556236");
		accnt.setCurrency("EUR");
		accnt.setNickname("John");
		accnt.setServicer(servicer);
		accnt.setAccount(account);
		accnt.setAccountType("savings");
		accountData.add(acct );
		accountData.add(accnt);
		Data2 data2 = new Data2();
		data2.setAccount(accountData);
		mockAccountGETResponse.setData(data2);
	
		return mockAccountGETResponse;
	}
	
	public static AccountGETResponse getCustomerAccountList(){
		AccountGETResponse mockAccountGETResponse =new AccountGETResponse();
		List<Account> accountData = new ArrayList<>();
		PSD2Account acct = new PSD2Account();
		acct.setAccountId("1234566");
		acct.setCurrency("EUR");
		acct.setNickname("John");
		acct.setAccountType("savings");
		Map<String, String> additionalInformation = new HashMap<>();
		additionalInformation.put(SchemeNameEnum.IBAN.name(), "1234566");
		additionalInformation.put(Data2Servicer.SchemeNameEnum.BICFI.name(), "1234566");
		acct.setAdditionalInformation(additionalInformation );
		Data2Servicer servicer = new Data2Servicer();
		servicer.setIdentification("1234566");
		Data2Account account = new Data2Account();
		account.setIdentification("1234566");
		account.setSchemeName(Data2Account.SchemeNameEnum.IBAN);
		acct.setAccount(account );
		acct.setServicer(servicer );
		
		PSD2Account accnt = new PSD2Account();
		accnt.setAccountId("1234566");
		accnt.setCurrency("EUR");
		accnt.setNickname("John");
		accnt.setServicer(servicer);
		accnt.setAccount(account);
		accnt.setAccountType("savings");
		accountData.add(acct );
		accountData.add(accnt);
		Data2 data2 = new Data2();
		data2.setAccount(accountData);
		mockAccountGETResponse.setData(data2);
	
		return mockAccountGETResponse;
	}
	
	public static List<PSD2Account> getPSD2AcountList(){
		List<PSD2Account> accountData = new ArrayList<>();
		PSD2Account acct = new PSD2Account();
		acct.setAccountId("14556236");
		acct.setCurrency("EUR");
		acct.setNickname("John");
		acct.setAccountType("savings");
		Data2Servicer servicer = new Data2Servicer();
		servicer.setIdentification("12345");
		Data2Account account = new Data2Account();
		account.setIdentification("1245676");
		acct.setAccount(account );
		acct.setServicer(servicer );
		
		PSD2Account accnt = new PSD2Account();
		accnt.setAccountId("14556236");
		accnt.setCurrency("EUR");
		accnt.setNickname("John");
		accnt.setServicer(servicer);
		accnt.setAccount(account);
		accnt.setAccountType("savings");
		accountData.add(acct );
		accountData.add(accnt);
		
		return  accountData;
	}

}
