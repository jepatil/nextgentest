package com.capgemini.psd2.security.consent.pisp.test.controllers;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.consent.domain.PSD2AccountsAdditionalInfo;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.integration.adapter.TPPInformationAdaptor;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.PaymentSetupValidationResponse;
import com.capgemini.psd2.scaconsenthelper.config.PFConfig;
import com.capgemini.psd2.scaconsenthelper.config.helpers.ConsentAuthorizationHelper;
import com.capgemini.psd2.scaconsenthelper.constants.PFConstants;
import com.capgemini.psd2.scaconsenthelper.models.DropOffResponse;
import com.capgemini.psd2.scaconsenthelper.models.IntentTypeEnum;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.scaconsenthelper.services.SCAConsentHelperService;
import com.capgemini.psd2.security.consent.pisp.helpers.PispConsentCreationDataHelper;
import com.capgemini.psd2.security.consent.pisp.rest.controllers.PispConsentAuthorizeController;
import com.capgemini.psd2.security.consent.pisp.test.mock.data.PispConsentApplicationMockdata;



@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class PispConsentAuthorizeControllerTest {

	@Mock
	private PispConsentCreationDataHelper consentCreationDataHelper;

	@InjectMocks
	private PispConsentAuthorizeController consentAuthorizeController;

	@Mock
	private RequestHeaderAttributes requestHeaderAttributes;

	@Mock
	private HttpServletRequest request;

	@Mock
	private HttpServletResponse response;
	
	@Mock
	private SCAConsentHelperService helperService;
	
	@Mock
	private TPPInformationAdaptor tppInformationAdaptor;
	
	@Mock
	@Qualifier("pispConsentAuthorizationHelper")
	private ConsentAuthorizationHelper consentAuthorizationHelper;
	
	@Mock
	private PFConfig pfConfig;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setPrefix("/");
		viewResolver.setSuffix(".jsp");
		
	}

	@Test
	public void testCreateConsentModelAndView() throws Exception {
		ModelAndView model = new ModelAndView();
		PSD2AccountsAdditionalInfo obj=new PSD2AccountsAdditionalInfo();
		obj.setAccountdetails(PispConsentApplicationMockdata.getPSD2AcountList());
		String resumePath="0";
		when(consentCreationDataHelper.retrieveCustomerAccountListInfo(anyString(),anyString(),anyString(),anyString(),anyString()))
		.thenReturn(PispConsentApplicationMockdata.getCustomerAccountInfo());
		when(consentAuthorizationHelper.matchAccountFromList(anyObject(), anyObject())).thenReturn(new PSD2Account());
		doNothing().when(consentCreationDataHelper).createConsent(anyObject(), anyString(), anyString(), anyString(), anyString(), anyString(),anyString(),anyObject());
		
		DropOffResponse dropresponse=new DropOffResponse();
		dropresponse.setRef("abcd");
		when(helperService.dropOffOnConsentSubmission(anyObject(), anyString(), anyString(), anyString())).thenReturn(dropresponse);
		when(pfConfig.getResumePathBaseURL()).thenReturn("http://localhost:8");
		when(request.getAttribute(anyString())).thenReturn(new PickupDataModel());
		when(tppInformationAdaptor.fetchApplicationName(anyString())).thenReturn("Moneywise");
		when(tppInformationAdaptor.fetchTPPInformation(anyString())).thenReturn(new Object());
		
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put("redirect_uri", "/test");
		consentAuthorizeController.createConsent(model, obj, resumePath);	

	}

	@SuppressWarnings("unchecked")
	@Test
	public void testCancelConsent() throws UnsupportedEncodingException {
		Map<String, String> paramsMap=new HashMap<>();
		paramsMap.put(PSD2Constants.SERVER_ERROR_FLAG_ATTR, "true");
		when(request.getAttribute(anyString())).thenReturn(new PickupDataModel());
		paramsMap.put(PFConstants.RESUME_PATH,"80");
		when(pfConfig.getResumePathBaseURL()).thenReturn("http://localhost:");
		ModelAndView model=new ModelAndView();
		doNothing().when(consentCreationDataHelper).cancelPaymentSetup(anyString(),anyMap());
		assertNotNull(consentAuthorizeController.cancelConsent(model,paramsMap));
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testCancelConsentElseBranch() {
		Map<String, String> paramsMap=new HashMap<>();
		paramsMap.put(PSD2Constants.SERVER_ERROR_FLAG_ATTR, null);
		when(request.getAttribute(anyString())).thenReturn(new PickupDataModel());
		doNothing().when(consentCreationDataHelper).cancelPaymentSetup(anyString(),anyMap());
		paramsMap.put(PFConstants.RESUME_PATH,"");
		when(pfConfig.getResumePathBaseURL()).thenReturn("");
		when(helperService.cancelJourney(anyString(), anyObject())).thenReturn("http://localhost:80");		
		ModelAndView model=new ModelAndView();
		consentAuthorizeController.cancelConsent(model, paramsMap);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testPreAuthorisation(){
		Map<String, String> paramsMap=new HashMap<>();
		PSD2Account account = mock(PSD2Account.class);
		PickupDataModel pickUpModel = new PickupDataModel();
		pickUpModel.setIntentId("1234");
		pickUpModel.setIntentTypeEnum(IntentTypeEnum.AISP_INTENT_TYPE);
		when(request.getAttribute(anyString())).thenReturn(pickUpModel);
		when(consentCreationDataHelper.retrieveCustomerAccountListInfo(anyString(),anyString(),anyString(),anyString(),anyString()))
		.thenReturn(PispConsentApplicationMockdata.getCustomerAccountInfo());
		when(consentAuthorizationHelper.matchAccountFromList(anyObject(), anyObject())).thenReturn(new PSD2Account());
		PaymentSetupValidationResponse validResponse = new PaymentSetupValidationResponse();
		validResponse.setPaymentSetupValidationStatus("valid");
		when(consentCreationDataHelper.validatePreAuthorisation(anyObject(),anyString(),anyMap())).thenReturn(validResponse);
		ModelAndView model=new ModelAndView();
		
		consentAuthorizeController.preAuthorisation(model, account, paramsMap);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testPreAuthorisationAccountNUll(){
		Map<String, String> paramsMap=new HashMap<>();
		ModelAndView model=new ModelAndView();
		consentAuthorizeController.preAuthorisation(model, null, paramsMap);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected=PSD2Exception.class)
	public void testPreAuthorisationValidAuthorisationNull(){
		Map<String, String> paramsMap=new HashMap<>();
		PSD2Account account = mock(PSD2Account.class);
		PickupDataModel pickUpModel = new PickupDataModel();
		pickUpModel.setIntentId("1234");
		pickUpModel.setIntentTypeEnum(IntentTypeEnum.AISP_INTENT_TYPE);
		when(request.getAttribute(anyString())).thenReturn(pickUpModel);
		when(consentCreationDataHelper.retrieveCustomerAccountListInfo(anyString(),anyString(),anyString(),anyString(),anyString()))
		.thenReturn(PispConsentApplicationMockdata.getCustomerAccountInfo());
		when(consentAuthorizationHelper.matchAccountFromList(anyObject(), anyObject())).thenReturn(new PSD2Account());
		when(consentCreationDataHelper.validatePreAuthorisation(anyObject(),anyString(),anyMap())).thenReturn(null);
		ModelAndView model=new ModelAndView();
		consentAuthorizeController.preAuthorisation(model, account, paramsMap);
	}

}


