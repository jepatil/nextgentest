package com.capgemini.psd2.pisp.payment.submission.platform.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.pisp.domain.PaymentSubmissionPlatformResource;

public interface PaymentSubmissionPlatformRepository extends MongoRepository<PaymentSubmissionPlatformResource, String> {
	public PaymentSubmissionPlatformResource findOneByTppCIDAndIdempotencyKeyAndIdempotencyRequestAndCreatedAtGreaterThan(String tppCID,String idempotencyKey,String idempotencyRequest,String date);
	public PaymentSubmissionPlatformResource findOneByPaymentSubmissionId(String paymentSubmissionId);
	public PaymentSubmissionPlatformResource findOneByPaymentId(String paymentId);
}

