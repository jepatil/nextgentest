<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<!DOCTYPE html>
<html lang="en" ng-app="loginApp" ng-cloak>
<head>
<meta charset="utf-8">
<base href="<%=request.getContextPath() %>/">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Login - Bank of Ireland</title>
<link rel="stylesheet" type="text/css" href="build/css/common.min.css" />
<link rel="stylesheet" type="text/css" href="build/css/bank-of-ireland.css" />

<link rel="shortcut icon" type="image/x-icon" href="build/img/favicon.ico?rev=16" />
<noscript>${JAVASCRIPT_ENABLE_MSG}</noscript>
<script src="build/js/libs.min.js"></script>
<script src="build/js/app.min.js"></script>
<script src="build/js/templates.js"></script>
</head>

<body class="next-gen-home" block-ui="main" >
 <div class="page-container">
	 
		<div ui-view autoscroll="false"></div>

		<div class="content" ui-view="modalContainer" autoscroll="false"></div>
		<!-- <div ui-view="main" class="main-container" block-ui="main" autoscroll="false"></div> -->
		</div>
		<c:if test="${not empty SPRING_SECURITY_LAST_EXCEPTION}">
			<input type="hidden" id="sec-error" name="secError" value="${fn:escapeXml(SPRING_SECURITY_LAST_EXCEPTION.errorInfo)}">
		</c:if>
		<input type="hidden" id="csrf" name="${_csrf.parameterName}" value="${_csrf.token}" />
		<input type="hidden" id="error" name="error" value="${fn:escapeXml(exception)}">
        <input type="hidden" id="oAuthUrl" name="oAuthUrl" value="${param.oAuthUrl}"/>
		<input type="hidden" id="correlationId" name="correlationId" value="${param.correlationId}"/>
         <input type="hidden" id="redirectUri" name="redirectUri" value="${redirectUri}"/>	   
		<c:if test="${not empty param.oAuthUrl}">
			<input type="hidden" name="saasurl" id="saasurl" value="<%=javax.servlet.http.HttpUtils.getRequestURL(request).append("?oAuthUrl=").append(request.getParameter("oAuthUrl"))%>"/>
		</c:if>
		<input type="hidden" id="brandId" name="brandId" value=""/>
		<input type="hidden" id="channelId" name="channelId" value=""/>
	 


</body>
		
</html>