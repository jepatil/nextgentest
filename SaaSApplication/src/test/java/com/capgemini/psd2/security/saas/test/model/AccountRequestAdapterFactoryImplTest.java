package com.capgemini.psd2.security.saas.test.model;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.security.saas.factories.AccountRequestAdapterFactoryImpl;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountRequestAdapterFactoryImplTest {
	
	@Mock
	private ApplicationContext context;
	
	@InjectMocks
	private AccountRequestAdapterFactoryImpl factoryImpl;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void getAccountRequestAdapterTest() {
		AccountRequestAdapter adapter = mock(AccountRequestAdapter.class);
		when(context.getBean(anyString())).thenReturn(adapter);
		factoryImpl.getAccountRequestAdapter("xyz");
	}

	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
		factoryImpl = null;
	}
}
