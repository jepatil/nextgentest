package com.capgemini.psd2.security.saas.test.model;

import static org.junit.Assert.*;

import org.junit.Test;

import com.capgemini.psd2.security.saas.model.JwtSettings;

public class JwtSettingsTest {

	@Test
	public void SetterGetterTest() {
		String signingKey = "0123456789abcdef";
		int tokenExpirationTime = 15;
		int refreshTokenExpTime = 60;
		String tokenIssuer = "http://SaaS.com";
		JwtSettings settings = new JwtSettings();
		settings.setTokenSigningKey(signingKey);
		settings.setTokenIssuer(tokenIssuer);
		settings.setTokenExpirationTime(tokenExpirationTime);
		settings.setRefreshTokenExpTime(refreshTokenExpTime);
		
		String resultSigningKey = settings.getTokenSigningKey();
		int resultTokenExpTime = settings.getTokenExpirationTime();
		int resultRefreshTokenExpTime = settings.getRefreshTokenExpTime();
		String resultTokenIssuer = settings.getTokenIssuer();
		
		assertEquals(resultSigningKey, signingKey);
		assertEquals(resultTokenIssuer, tokenIssuer);
		assertEquals(resultTokenExpTime, tokenExpirationTime);
		assertEquals(resultRefreshTokenExpTime, refreshTokenExpTime);
		
		
	}

}
