/*************************************************************************
* 
 * CAPGEMINI CONFIDENTIAL
* __________________
* 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
* 
 * NOTICE:  All information contained herein is, and remains
* the property of CAPGEMINI GROUP.
* The intellectual and technical concepts contained herein
* are proprietary to CAPGEMINI GROUP and may be covered
* by patents, patents in process, and are protected by trade secret
* or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from CAPGEMINI GROUP.
*//*

package com.capgemini.psd2.security.saas.factories;

import java.util.UUID;
import java.util.stream.Collectors;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.security.models.UserContext;
import com.capgemini.psd2.security.saas.model.AccessJwtToken;
import com.capgemini.psd2.security.saas.model.JwtSettings;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtTokenFactory {

	private final JwtSettings settings;

	@Autowired
	public JwtTokenFactory(JwtSettings settings) {
		this.settings = settings;
	}

	*//**
	 * Factory method for issuing new JWT Tokens.
	 * 
	 * @param username
	 * @param roles
	 * @return
	 *//*
	public AccessJwtToken createAccessJwtToken(UserContext userContext) {
		Claims claims = Jwts.claims().setSubject(userContext.getUsername());
		claims.put("scopes", userContext.getAuthorities().stream().map(s -> s.toString()).collect(Collectors.toList()));

		DateTime currentTime = new DateTime();

		String token = Jwts.builder().setClaims(claims).setIssuer(settings.getTokenIssuer())
		         .setId(UUID.randomUUID().toString())
				.setIssuedAt(currentTime.toDate())
				.setExpiration(currentTime.plusMinutes(settings.getTokenExpirationTime()).toDate())
				.signWith(SignatureAlgorithm.HS512, settings.getTokenSigningKey()).compact();

		return new AccessJwtToken(token, claims);
	}

	public JwtToken createRefreshToken(UserContext userContext) {
        DateTime currentTime = new DateTime();
        Claims claims = Jwts.claims().setSubject(userContext.getUsername());
        claims.put(PSD2SecurityConstants.SCOPES, Arrays.asList(Scopes.REFRESH_TOKEN.authority()));
        
        String token = Jwts.builder()
          .setClaims(claims)
          .setIssuer(settings.getTokenIssuer())
          .setId(UUID.randomUUID().toString())
          .setIssuedAt(currentTime.toDate())
          .setExpiration(currentTime.plusMinutes(settings.getRefreshTokenExpTime()).toDate())
          .signWith(SignatureAlgorithm.HS512, settings.getTokenSigningKey())
        .compact();

        return new AccessJwtToken(token, claims);
    }
}*/