/*************************************************************************
 * 
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 *//*

package com.capgemini.psd2.security.saas.handlers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.identitymanagement.models.UserAuthorities;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.security.constants.PSD2SecurityConstants;
import com.capgemini.psd2.security.helpers.SessionLogoutHelper;
import com.capgemini.psd2.security.models.JwtToken;
import com.capgemini.psd2.security.models.UserContext;
import com.capgemini.psd2.security.saas.factories.JwtTokenFactory;
import com.capgemini.psd2.security.saas.model.JwtSettings;
import com.capgemini.psd2.security.utilities.CryptoUtilities;
import com.capgemini.psd2.security.utilities.URLUtilities;
import com.capgemini.psd2.utilities.DateUtilites;

@Component
public class SimpleUrlAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(SimpleUrlAuthenticationSuccessHandler.class);

	private final JwtTokenFactory tokenFactory;

	private final JwtSettings settings;

	@Autowired
	private RequestHeaderAttributes requestHeaderAttribute;
	
	@Autowired
	public SimpleUrlAuthenticationSuccessHandler(final JwtTokenFactory factory, final JwtSettings settings) {
		this.tokenFactory = factory;
		this.settings = settings;
	}

	*//**
	 * This is a helper method of onAuthenticationSuccess method.
	 * It takes the ID token as a param and sends this param at the time of redirecting back to
	 * the OAuth server's end-point , through this way its injecting the ID token to OAuth server to 
	 * prove the successful authentication of the customer.
	 * @param accessToken
	 * @param request
	 * @param response
	 * @param authentication
	 * @throws IOException
	 *//*
	protected void handle(JwtToken accessToken, HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException {
		LOGGER.info("{\"Enter\":\"com.capgemini.psd2.security.alpha.saas.handlers.SimpleUrlAuthenticationSuccessHandler.handle()\",\"timestamp\":\"{}\",\"correlationId\": \"{}\"}",DateUtilites.generateCurrentTimeStamp(),requestHeaderAttribute.getCorrelationId());
		String targetEncUrl = request.getParameter(PSD2SecurityConstants.OAUTH_URL_PARAM);
		if (response.isCommitted()) {
			return;
		}
		String encrypted = CryptoUtilities.encPayLoad(accessToken.getToken(), settings.getTokenSigningKey());
		String targetUrl = CryptoUtilities.decryptPayLoad(targetEncUrl, settings.getTokenSigningKey());
		
		LOGGER.info("{\"Exit\":\"com.capgemini.psd2.security.alpha.saas.handlers.SimpleUrlAuthenticationSuccessHandler.handle()\",\"timestamp\":\"{}\",\"correlationId\": \"{}\"}",DateUtilites.generateCurrentTimeStamp(),requestHeaderAttribute.getCorrelationId());
		String returnURL = targetUrl + PSD2Constants.AMPERSAND+PSD2SecurityConstants.ID_TOKEN_PARAM+PSD2Constants.EQUAS + encrypted;
		response.sendRedirect(URLUtilities.createURLEncString(returnURL,PSD2SecurityConstants.OAUTH_ENC_URL,settings.getTokenSigningKey()));			
		
		SessionLogoutHelper.logout(request,response);
	}

	*//**
	 * onAuthenticationSuccess is a overridden method of
	 * AuthenticationSuccessHandler class, it will get it invoked on the
	 * successful authentication of the customer. It generates the signed ID token in
	 * the form of JWT to contain the user's identity information with the help of one tokenFactory.
	 * It invokes the handle method to redirect back to OAuth server's end-point with ID token. 
	 *//*
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		LOGGER.info("{\"Enter\":\"com.capgemini.psd2.security.alpha.saas.handlers.SimpleUrlAuthenticationSuccessHandler.onAuthenticationSuccess()\",\"timestamp\":\"{}\",\"correlationId\": \"{}\"}",DateUtilites.generateCurrentTimeStamp(),requestHeaderAttribute.getCorrelationId());
		String userName = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new UserAuthorities(PSD2SecurityConstants.USER_AUTHORITY));
		UserContext userContext = UserContext.create(userName, authorities);
		JwtToken accessToken = tokenFactory.createAccessJwtToken(userContext);
		LOGGER.info("{\"Exit\":\"com.capgemini.psd2.security.alpha.saas.handlers.SimpleUrlAuthenticationSuccessHandler.onAuthenticationSuccess()\",\"timestamp\":\"{}\",\"correlationId\": \"{}\"}",DateUtilites.generateCurrentTimeStamp(),requestHeaderAttribute.getCorrelationId());
		handle(accessToken, request, response, authentication);
	}
}*/