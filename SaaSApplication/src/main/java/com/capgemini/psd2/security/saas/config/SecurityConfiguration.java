/*************************************************************************
* 
 * CAPGEMINI CONFIDENTIAL
* __________________
* 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
* 
 * NOTICE:  All information contained herein is, and remains
* the property of CAPGEMINI GROUP.
* The intellectual and technical concepts contained herein
* are proprietary to CAPGEMINI GROUP and may be covered
* by patents, patents in process, and are protected by trade secret
* or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from CAPGEMINI GROUP.
*//*

*//*************************************************************************
* 
 * CAPGEMINI CONFIDENTIAL
* __________________
* 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
* 
 * NOTICE:  All information contained herein is, and remains
* the property of CAPGEMINI GROUP.
* The intellectual and technical concepts contained herein
* are proprietary to CAPGEMINI GROUP and may be covered
* by patents, patents in process, and are protected by trade secret
* or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from CAPGEMINI GROUP.
*//*

package com.capgemini.psd2.security.saas.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

import com.capgemini.psd2.security.saas.handlers.CustomAuthenticationFailureHandler;
import com.capgemini.psd2.security.saas.handlers.CustomAuthenticationProvider;
import com.capgemini.psd2.security.saas.handlers.SimpleUrlAuthenticationSuccessHandler;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	private CustomAuthenticationProvider authenticationProvider;

	@Autowired
	private SimpleUrlAuthenticationSuccessHandler successHandler;

	@Autowired
	private CustomAuthenticationFailureHandler failureHandler;
	
	@Autowired
	private PSD2SecurityFilter psd2SecurityFilter;
	
	@Override
	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	*//**
	 * Configured the CustomAuthenticationProvider to perform Multi Factor
	 * Authentication with the help of Ping Directory (LDAP) & Ping ID
	 *//*
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(authenticationProvider);
	}

	@Bean
	public ContentNegotiatingViewResolver contentViewResolver() throws Exception {
		ContentNegotiationManagerFactoryBean contentNegotiationManager = new ContentNegotiationManagerFactoryBean();
		contentNegotiationManager.addMediaType("json", MediaType.APPLICATION_JSON);


		MappingJackson2JsonView defaultView = new MappingJackson2JsonView();
		defaultView.setExtractValueFromSingleKeyModel(true);

		ContentNegotiatingViewResolver contentViewResolver = new ContentNegotiatingViewResolver();
		contentViewResolver.setContentNegotiationManager(contentNegotiationManager.getObject());
		contentViewResolver.setDefaultViews(Arrays.<View>asList(defaultView));
		return contentViewResolver;
	}
	
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/static/**","/build/**","/errors","/securityLogout","index.jsp");
	}

	*//**
	 * Security configuration done.. to protect some resources and to define
	 * login action & configure success & failure handler.
	 *//*
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers("/restart").permitAll();
		http.authorizeRequests().anyRequest()
		.permitAll()
		.antMatchers("/console/**").permitAll()
		.and()
		.csrf().disable()
		.logout().logoutUrl("/logout").logoutSuccessUrl("/login")
		.and()
		.formLogin().loginProcessingUrl("/login")
		.successHandler(successHandler).failureHandler(failureHandler).loginPage("/login")
		.and()
		//.addFilterBefore(psd2SecurityFilter, AbstractPreAuthenticatedProcessingFilter.class)
		.sessionManagement()
		.enableSessionUrlRewriting(Boolean.FALSE)
        .sessionFixation().migrateSession()
        .sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
        .maximumSessions(1);

		http.headers().frameOptions().disable();
	}
}*/