package com.capgemini.psd2.security.saas.factories;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;

@Component
public class AccountRequestAdapterFactoryImpl implements AccountRequestAdapterFactory,ApplicationContextAware{
	
	private ApplicationContext applicationContext;
		
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
		
	}
	@Override
		public AccountRequestAdapter getAccountRequestAdapter(String requestAdapter) {
			return (AccountRequestAdapter)applicationContext.getBean(requestAdapter);
		}
}
