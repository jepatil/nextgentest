package com.capgemini.psd2.account.standingorder.routing.adapter.routing;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;



import com.capgemini.psd2.aisp.adapter.AccountStandingOrdersAdapter;

	@Component
	public class AccountStandingOrderCoreSystemAdapterFactory implements ApplicationContextAware, AccountStandingOrderAdapterFactory{

		
		/** The application context. */
		private ApplicationContext applicationContext;

		@Override
		public AccountStandingOrdersAdapter getAdapterInstance(String adapterName) {
			return (AccountStandingOrdersAdapter) applicationContext.getBean(adapterName);
			
		}

		@Override
		public void setApplicationContext(ApplicationContext context) {
			this.applicationContext = context;
			
		}
		
		/* (non-Javadoc)
		 * @see com.capgemini.psd2.account.information.routing.adapter.routing.AccountInformationAdapterFactory#getAdapterInstance(java.lang.String)
		 */
}

