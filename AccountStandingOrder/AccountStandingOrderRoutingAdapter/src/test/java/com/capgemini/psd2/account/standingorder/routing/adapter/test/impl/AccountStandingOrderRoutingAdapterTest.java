package com.capgemini.psd2.account.standingorder.routing.adapter.test.impl;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


import com.capgemini.psd2.account.standingorder.routing.adapter.impl.AccountStandingOrderRoutingAdapter;
import com.capgemini.psd2.account.standingorder.routing.adapter.routing.AccountStandingOrderAdapterFactory;
import com.capgemini.psd2.account.standingorder.routing.adapter.test.adapter.AccountStandingOrderTestRoutingAdapter;
import com.capgemini.psd2.aisp.adapter.AccountStandingOrdersAdapter;

import com.capgemini.psd2.aisp.domain.StandingOrdersGETResponse;


public class AccountStandingOrderRoutingAdapterTest 
{
	
	/** The account standing order adapter factory. */
	@Mock
	private AccountStandingOrderAdapterFactory accountStandingOrderAdapterFactory;

	/** The account standing order routing adapter. */
	@InjectMocks
	private AccountStandingOrdersAdapter accountStandingOrderRoutingAdapter = new AccountStandingOrderRoutingAdapter();

	/**
	 * Sets the up.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Retrieve account balancetest.
	 */
	@Test
	public void retrieveAccountStandingOrdertest() {

		AccountStandingOrdersAdapter accountStandingOrdersAdapter = new AccountStandingOrderTestRoutingAdapter();
		Mockito.when(accountStandingOrderAdapterFactory.getAdapterInstance(anyString()))
				.thenReturn(accountStandingOrdersAdapter);

		StandingOrdersGETResponse standingOrdersGETResponse = accountStandingOrderRoutingAdapter.retrieveAccountStandingOrders(null,
				null);

		assertTrue(standingOrdersGETResponse.getData().getStandingOrder().isEmpty());

	}

}
