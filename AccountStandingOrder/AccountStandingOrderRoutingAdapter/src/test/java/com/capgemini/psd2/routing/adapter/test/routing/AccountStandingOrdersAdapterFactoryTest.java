package com.capgemini.psd2.routing.adapter.test.routing;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;



import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;


import com.capgemini.psd2.account.standingorder.routing.adapter.impl.AccountStandingOrderRoutingAdapter;
import com.capgemini.psd2.account.standingorder.routing.adapter.routing.AccountStandingOrderCoreSystemAdapterFactory;

import com.capgemini.psd2.aisp.adapter.AccountStandingOrdersAdapter;


public class AccountStandingOrdersAdapterFactoryTest 
{
	/** The application context. */
	@Mock
	private ApplicationContext applicationContext;
	
	/** The account standing order core system adapter factory. */
	@InjectMocks
	private AccountStandingOrderCoreSystemAdapterFactory accountStandingOrderCoreSystemAdapterFactory;
	
	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Test account standing order adapter.
	 */
	@Test
	public void testAccountDirectDebitsAdapter() {
		AccountStandingOrdersAdapter accountStandingOrderAdapter = new AccountStandingOrderRoutingAdapter();
		when(applicationContext.getBean(anyString())).thenReturn(accountStandingOrderAdapter);
		AccountStandingOrdersAdapter accountStandingOrderAdapterResult = (AccountStandingOrderRoutingAdapter) accountStandingOrderCoreSystemAdapterFactory.getAdapterInstance("accountStandingOrdersAdapter");
		assertEquals(accountStandingOrderAdapter, accountStandingOrderAdapterResult);
	}
	
	

}
