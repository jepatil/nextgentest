package com.capgemini.psd2.mongo.db.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.capgemini.psd2.account.standingorder.mongo.db.adapter.impl.AccountStandingOrderMongoDbAdapterImpl;
import com.capgemini.psd2.aisp.adapter.AccountStandingOrdersAdapter;

@Configuration
public class AccountStandingOrderMongoDbAdapterConfig {
	
	@Bean(name = "accountStandingOrderMongoDbAdapter")
	public AccountStandingOrdersAdapter mongoDBAdapter() {
		return new AccountStandingOrderMongoDbAdapterImpl();
	}
}
