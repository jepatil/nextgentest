package com.capgemini.psd2.account.standingorder.test.controller;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.capgemini.psd2.account.standingorder.controller.AccountStandingOrderController;
import com.capgemini.psd2.account.standingorder.service.AccountStandingOrderService;

import com.capgemini.psd2.aisp.domain.StandingOrdersGETResponse;
import com.capgemini.psd2.exceptions.PSD2Exception;


@RunWith(SpringJUnit4ClassRunner.class)
public class AccountStandingOrderControllerTest 
{

	/** The service. */
	@Mock
	private AccountStandingOrderService service;

	/** The mock mvc. */
	private MockMvc mockMvc;

	/** The controller. */
	@InjectMocks
	private AccountStandingOrderController controller;

	/** The standing order response. */
	StandingOrdersGETResponse standingOrdersGETResponse = null;
	
	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(controller).dispatchOptions(true).build();
	}

	/**
	 * Test null account id. Failure test
	 */
	@Test(expected = PSD2Exception.class)
	public void testNullAccountId() {
		controller.retrieveAccountStandingOrders(null);
	}
	
	/**
	 * Test invalid account id length.
	 */
	@Test(expected = PSD2Exception.class)
	public void testInvalidAccountIdLength() {
		controller.retrieveAccountStandingOrders("269c3ff5-d7f8-419b-a3b9-7136c5b4611a-qwer23454c51326fdr34-12345123smcge36dg337b");
	}

	/**
	 * Test.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void successTest() throws Exception {
		when(service.retrieveAccountStandingOrders(anyString())).thenReturn(standingOrdersGETResponse);
		this.mockMvc.perform(get("/accounts/{accountId}/standing-orders", "269c3ff5-d7f8-419b-a3b9-7136c5b4611a")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}
	

	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
		controller = null;
	}
}
