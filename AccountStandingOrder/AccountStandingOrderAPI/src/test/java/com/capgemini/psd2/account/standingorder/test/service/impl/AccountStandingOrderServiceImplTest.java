package com.capgemini.psd2.account.standingorder.test.service.impl;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.account.standingorder.service.impl.AccountStandingOrderServiceImpl;
import com.capgemini.psd2.account.standingorder.test.mockdata.AccountStandingOrderMockData;
import com.capgemini.psd2.aisp.adapter.AccountStandingOrdersAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.StandingOrdersGETResponse;
import com.capgemini.psd2.logger.RequestHeaderAttributes;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountStandingOrderServiceImplTest 

{

	/** The adapter. */
	@Mock
	private AccountStandingOrdersAdapter adapter;

	/** The consent mapping. */
	@Mock
	private AispConsentAdapter aispConsentAdapter;

	/** The header atttributes. */
	@Mock
	private RequestHeaderAttributes headerAtttributes;

	/** The service. */
	@InjectMocks
	private AccountStandingOrderServiceImpl service = new AccountStandingOrderServiceImpl();

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		when(headerAtttributes.getToken()).thenReturn(AccountStandingOrderMockData.getToken());
		when(aispConsentAdapter.retrieveAccountMappingByAccountId(anyString(), anyString()))
		.thenReturn(AccountStandingOrderMockData.getMockAccountMapping());
		
	}

	/**
	 * Retrieve account standing order test (empty list).
	 */
	@Test
	public void retrieveAccountStandingOrderTest() {
		when(adapter.retrieveAccountStandingOrders(anyObject(), anyObject()))
				.thenReturn(AccountStandingOrderMockData.getMockBlankResponse());
		StandingOrdersGETResponse standingOrdersGETResponse = service.retrieveAccountStandingOrders(
				"123");
		assertTrue(standingOrdersGETResponse.getData().getStandingOrder().isEmpty());
	}


	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
		service = null;
	}

}
