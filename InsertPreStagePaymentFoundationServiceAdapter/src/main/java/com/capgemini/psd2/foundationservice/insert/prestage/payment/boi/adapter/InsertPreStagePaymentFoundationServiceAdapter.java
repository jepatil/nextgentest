/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.client.InsertPreStagePaymentFoundationClient;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.domain.PaymentInstruction;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.domain.ValidationPassed;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.utility.InsertPreStagePaymentFoundationServiceUtility;
import com.capgemini.psd2.pisp.adapter.PaymentSetupStagingAdapter;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupStagingResponse;
import com.capgemini.psd2.rest.client.model.RequestInfo;

@Component
public class InsertPreStagePaymentFoundationServiceAdapter implements PaymentSetupStagingAdapter {

	@Value("${foundationService.insertPreStagePaymentBaseURL:#{null}}")
	private String insertPreStagePaymentBaseURL;
	
	@Value("${foundationService.updatePreStagePaymentBaseURL:#{null}}")
	private String updatePreStagePaymentBaseURL;
	
	/** The Payment Retrieval base URL. */
	@Value("${foundationService.paymentRetrievalBaseURL:#{null}}")
	private String paymentRetrievalBaseURL;
	
	/** The Payment Retrieval end URL. */
	@Value("${foundationService.paymentRetrievalEndURL:#{null}}")
	private String paymentRetrievalEndURL;
	
	
	@Autowired
	private InsertPreStagePaymentFoundationServiceUtility insertPreStagePaymentFoundationServiceUtility;
	
	@Autowired
	private InsertPreStagePaymentFoundationClient insertPreStagePaymentFoundationClient;
	
	
	@Override
	public PaymentSetupStagingResponse createStagingPaymentSetup(CustomPaymentSetupPOSTRequest paymentSetupRequest, Map<String, String> params){
		RequestInfo requestInfo = new RequestInfo();
		
		HttpHeaders httpHeaders = insertPreStagePaymentFoundationServiceUtility.createRequestHeaders(requestInfo, params);		
			
		requestInfo.setUrl(insertPreStagePaymentBaseURL);
		
		PaymentInstruction paymentInstruction = insertPreStagePaymentFoundationServiceUtility.transformResponseFromAPIToFDForInsert(paymentSetupRequest, params);
		
 		ValidationPassed validationPassed = null;
				
		try {
			validationPassed = insertPreStagePaymentFoundationClient.restTransportForInsertPreStagePayment(requestInfo, paymentInstruction, ValidationPassed.class, httpHeaders);
		} catch (AdapterException e) {
				throw e;
		} catch(Exception e){
			throw AdapterException.populatePSD2Exception(e.getMessage(), AdapterErrorCodeEnum.TECHNICAL_ERROR);
		}
		return insertPreStagePaymentFoundationServiceUtility.transformResponseFromFDToAPIForInsert(validationPassed);
	}

	@Override
	public CustomPaymentSetupPOSTResponse retrieveStagedPaymentSetup(String paymentId, Map<String, String> params) {
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders httpHeaders = insertPreStagePaymentFoundationServiceUtility.createRequestHeaders(requestInfo, params);
		
		
		String finalURL = insertPreStagePaymentFoundationServiceUtility.getFoundationServiceURL(
				paymentId,  paymentRetrievalBaseURL, paymentRetrievalEndURL);
		requestInfo.setUrl(finalURL);
		PaymentInstruction paymentInstruction = null;
		
		try {
			paymentInstruction = insertPreStagePaymentFoundationClient.restTransportForPaymentRetrieval(requestInfo,
					PaymentInstruction.class, httpHeaders);
		} catch (AdapterException e) {
			throw e;
		}catch(Exception e){
				throw AdapterException.populatePSD2Exception(e.getMessage(), AdapterErrorCodeEnum.TECHNICAL_ERROR);
		} 

		return insertPreStagePaymentFoundationServiceUtility.transformResponseFromFDToAPI(paymentInstruction);
	}

	@Override
	public PaymentSetupStagingResponse updateStagedPaymentSetup(CustomPaymentSetupPOSTResponse paymentSetupBankResource, Map<String, String> params) {
		RequestInfo requestInfo = new RequestInfo();
		
		HttpHeaders httpHeaders = insertPreStagePaymentFoundationServiceUtility.createRequestHeaders(requestInfo, params);		
			
		requestInfo.setUrl(updatePreStagePaymentBaseURL);
		
		PaymentInstruction paymentInstruction = insertPreStagePaymentFoundationServiceUtility.transformResponseFromAPIToFDForUpdate(paymentSetupBankResource, params);
		
		ValidationPassed validationPassed = null;
		
		try {
			validationPassed = insertPreStagePaymentFoundationClient.restTransportForUpdatePreStagePayment(requestInfo, paymentInstruction, ValidationPassed.class, httpHeaders);
		} catch (AdapterException e) {
			throw e;
		}catch(Exception e){
			throw AdapterException.populatePSD2Exception(e.getMessage(), AdapterErrorCodeEnum.TECHNICAL_ERROR);
		}
		
		return insertPreStagePaymentFoundationServiceUtility.transformResponseFromFDToAPIForUpdate(validationPassed);
	}

}
