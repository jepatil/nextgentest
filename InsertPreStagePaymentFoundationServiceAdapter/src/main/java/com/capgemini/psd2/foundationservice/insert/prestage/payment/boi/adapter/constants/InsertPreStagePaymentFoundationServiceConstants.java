/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.constants;

/**
 * The Class AccountInformationFoundationServiceConstants.
 */
public class InsertPreStagePaymentFoundationServiceConstants {
	
	public static final String CHANNEL_ID = "X-BOI-CHANNEL";
	public static final String USER_ID = "X-BOI-USER";
	public static final String PLATFORM_ID = "X-BOI-PLATFORM";
	public static final String CORRELATION_ID = "X-CORRELATION-ID";
	
	public static final String BENEFICIARY_COUNTRY = "GB";
	
	public static final String VALIDATION_STATUS = "PAYMENT_SETUP_VALIDATION_STATUS";
	
	public static final String PAYMENT_ID = "paymentId";
	
	public static final String YOU_ARE_NOT_AUTHORISED = "You are not authorised";
	public static final String GENERAL_ERROR = "General error";
	public static final String PLEASE_TRY_AGAIN_LATER = "Please try again later";
	public static final String CONNECTION_REFUSED_CONNECT = "Connection refused: connect";
	
}
