
package com.capgemini.psd2.foundationservice.validate.payment.boi.adapter;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.exceptions.ValidationViolations;
import com.capgemini.psd2.adapter.utility.AdapterUtility;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.CustomerAccountsFilterFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.constants.CustomerAccountsFilterFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.validate.payment.boi.adapter.client.ValidatePaymentFoundationServiceClient;
import com.capgemini.psd2.foundationservice.validate.payment.boi.adapter.constants.ValidatePaymentFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.validate.payment.boi.adapter.domain.PaymentInstruction;
import com.capgemini.psd2.foundationservice.validate.payment.boi.adapter.domain.ValidationPassed;
import com.capgemini.psd2.foundationservice.validate.payment.boi.adapter.utility.ValidatePaymentFoundationServiceUtility;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.adapter.PaymentAuthorizationValidationAdapter;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponseInitiation;
import com.capgemini.psd2.pisp.domain.PaymentSetupValidationResponse;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class ValidatePaymentFoundationServiceAdapter implements PaymentAuthorizationValidationAdapter {

	@Value("${foundationService.validatePreAuthorizationPaymentBaseURL}")
	private String validatePreAuthorizationPaymentBaseURL;

	@Value("${foundationService.validatePreSubmissionPaymentBaseURL}")
	private String validatePreSubmissionPaymentBaseURL;

	@Autowired
	private ValidatePaymentFoundationServiceUtility validatePaymentFoundationServiceUtility;

	@Autowired
	private ValidatePaymentFoundationServiceClient validatePaymentFoundationServiceClient;
	
	@Autowired
	private CustomerAccountsFilterFoundationServiceAdapter commonFilterUtility;
	
	@Autowired
	private AdapterUtility adapterUtility;

	@Override
	public PaymentSetupValidationResponse preAuthorizationPaymentValidation(CustomPaymentSetupPOSTResponse paymentSetupResponse, Map<String, String> params) {

		ValidationPassed validationPassed = null;
		ValidationViolations validationViolations = null;
		RequestInfo requestInfo = new RequestInfo();
		PaymentInstruction paymentInstruction = null;
		String submissionID = null;
		HttpHeaders httpHeaders = validatePaymentFoundationServiceUtility.createRequestHeaders(requestInfo, paymentSetupResponse, params);
		requestInfo.setUrl(validatePreAuthorizationPaymentBaseURL);

		if (params.get(PSD2Constants.CONSENT_FLOW_TYPE).equalsIgnoreCase(PSD2Constants.PISP_SUBMISSION_AUTHORIZATION_FLOW)) {
			params.put(PSD2Constants.CONSENT_FLOW_TYPE, CustomerAccountsFilterFoundationServiceConstants.PISP);
			PaymentSetupResponseInitiation initiation = paymentSetupResponse.getData().getInitiation();
			if (!NullCheckUtils.isNullOrEmpty(initiation.getDebtorAccount())) {
				String accNo = paymentSetupResponse.getAccountNumber();
				String accNsc = paymentSetupResponse.getAccountNSC();
				params.put(CustomerAccountsFilterFoundationServiceConstants.ACCOUNTNSCNUMBER, accNsc);
				params.put(CustomerAccountsFilterFoundationServiceConstants.ACCOUNTNUMBER, accNo);
				String userId = params.get(PSD2Constants.USER_IN_REQ_HEADER);
				if (null == userId) {
					userId = params.get(PSD2Constants.USER_ID);
				}
				com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts filteredAccounts = commonFilterUtility.retrieveCustomerAccountList(userId, params);
				if (filteredAccounts == null || filteredAccounts.getAccount() == null
						|| filteredAccounts.getAccount().isEmpty()) {
					throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.ACCOUNT_NOT_FOUND);
				}
				requestInfo.setUrl(validatePreSubmissionPaymentBaseURL);

			}
		}
		paymentInstruction = validatePaymentFoundationServiceUtility.transformRequestFromAPIToFS(paymentSetupResponse, params);

		try {
			validationPassed = validatePaymentFoundationServiceClient.validatePayment(requestInfo, paymentInstruction, ValidationPassed.class, httpHeaders);
		} catch (AdapterException e) {
			validationViolations = (ValidationViolations) e.getFoundationError();
			if (NullCheckUtils.isNullOrEmpty(validationViolations)) {
				throw e;
			}			
			String errorCode = validationViolations.getValidationViolation().get(0).getErrorCode();
			String errorText = validationViolations.getValidationViolation().get(0).getErrorText();
			String throwableError = adapterUtility.getThrowableError().get(errorCode);
			
			if (!com.capgemini.psd2.utilities.NullCheckUtils.isNullOrEmpty(throwableError)) {
				e.getErrorInfo().setDetailErrorMessage(e.getErrorInfo().getErrorMessage());
				throw e;
			}
			if (errorText.contains(ValidatePaymentFoundationServiceConstants.DELIMITER)) {
				String[] splitResult = errorText.split(ValidatePaymentFoundationServiceConstants.SPLIT_DELIMITER);
				submissionID = splitResult[0];
			}
			if (!NullCheckUtils.isNullOrEmpty(submissionID)) {
				PaymentSetupValidationResponse paymentSetupValidationResponse = new PaymentSetupValidationResponse();
				paymentSetupValidationResponse.setPaymentSubmissionId(submissionID);
				paymentSetupValidationResponse.setPaymentSetupValidationStatus(ValidatePaymentFoundationServiceConstants.VALIDATION_STATUS_REJECTED);
				return paymentSetupValidationResponse;
			} else {
				throw AdapterException.populatePSD2Exception("Submission ID is not received from FS for Error code: "+ errorCode, AdapterErrorCodeEnum.TECHNICAL_ERROR);
			}
		} catch (Exception e) {
			throw AdapterException.populatePSD2Exception(e.getMessage(),AdapterErrorCodeEnum.TECHNICAL_ERROR);
		}
		return validatePaymentFoundationServiceUtility.transformResponseFromFSToAPI(validationPassed);
	}

}
