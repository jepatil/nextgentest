package com.capgemini.psd2.foundationservice.validate.payment.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.exceptions.ValidationViolation;
import com.capgemini.psd2.adapter.exceptions.ValidationViolations;
import com.capgemini.psd2.adapter.utility.AdapterUtility;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.CustomerAccountsFilterFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt;
import com.capgemini.psd2.foundationservice.validate.payment.boi.adapter.ValidatePaymentFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.validate.payment.boi.adapter.client.ValidatePaymentFoundationServiceClient;
import com.capgemini.psd2.foundationservice.validate.payment.boi.adapter.domain.PaymentInstruction;
import com.capgemini.psd2.foundationservice.validate.payment.boi.adapter.domain.ValidationPassed;
import com.capgemini.psd2.foundationservice.validate.payment.boi.adapter.utility.ValidatePaymentFoundationServiceUtility;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.DebtorAccount;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponseInitiation;
import com.capgemini.psd2.pisp.domain.PaymentSetupValidationResponse;
import com.capgemini.psd2.rest.client.sync.RestClientSync;

@RunWith(SpringJUnit4ClassRunner.class)
public class ValidatePaymentFoundationServiceAdapterTest {

	@InjectMocks
	private ValidatePaymentFoundationServiceAdapter adapter;
	
	@Mock
	private ValidatePaymentFoundationServiceUtility validatePaymentFoundationServiceUtility;

	@Mock
	private ValidatePaymentFoundationServiceClient validatePaymentFoundationServiceClient;

	@Mock
	private AdapterUtility adapterUtility;
	
	@Mock
	private CustomerAccountsFilterFoundationServiceAdapter commonFilterUtility;
	
	@Mock
	private CustomPaymentSetupPOSTResponse customPaymentSetupPOSTResponse;
	
	
	
	@Mock
	private RestClientSync restClient;
	
	/**
	 * Sets the up.
	 */
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	
	
	/**
	 * Test payment setup validation response.
	 */
	@Test
	public void paymentSetupValidationResponseTest(){
		
		
		Map<String, String> map = new HashMap<>();
		map.put("test", "test");
		HttpHeaders headers = new HttpHeaders();
		HashMap<String, String> params = new HashMap<>();
		ValidationPassed validationPassed = new ValidationPassed();
		PaymentInstruction paymentInstruction = new PaymentInstruction();
		PaymentSetupValidationResponse paymentSetupValidationResponse = new PaymentSetupValidationResponse();
		ReflectionTestUtils.setField(adapter, "validatePreAuthorizationPaymentBaseURL", "test");
		params.put("consentFlowType", "PISP-SUBMISSION-AUTHORIZATION");
		
		Mockito.when(validatePaymentFoundationServiceUtility.createRequestHeaders(anyObject(), anyObject(), anyObject())).thenReturn(headers);
		Mockito.when(validatePaymentFoundationServiceUtility.transformRequestFromAPIToFS(anyObject(), anyObject())).thenReturn(paymentInstruction);
		Mockito.when(validatePaymentFoundationServiceClient.validatePayment(anyObject(), anyObject(), anyObject(), anyObject())).thenReturn(validationPassed);
		Mockito.when(validatePaymentFoundationServiceUtility.transformResponseFromFSToAPI(anyObject())).thenReturn(paymentSetupValidationResponse);
		CustomPaymentSetupPOSTResponse paymentSetupPOSTResponse = new CustomPaymentSetupPOSTResponse();
		
		
		paymentSetupValidationResponse = adapter.preAuthorizationPaymentValidation(paymentSetupPOSTResponse, params);
		assertNotNull(paymentSetupValidationResponse);
	}

	
	/**
	 * Test payment setup validation response.
	 */
	@Test
	public void validatePaymentSetupTest(){
		
	
		HttpHeaders headers = new HttpHeaders();
		ErrorInfo errorInfo = new ErrorInfo();
		errorInfo.setErrorCode("test");
		errorInfo.setErrorMessage("test");
		HashMap<String, String> params = new HashMap<>();
		HashMap<String, String> map = new HashMap<>();
		map.put(errorInfo.getErrorCode(), "test");
		PaymentInstruction paymentInstruction = new PaymentInstruction();
		PaymentSetupValidationResponse paymentSetupValidationResponse = new PaymentSetupValidationResponse();
		CustomPaymentSetupPOSTResponse paymentValidationResponse = new CustomPaymentSetupPOSTResponse();
		params.put("consentFlowType", "PISP-SUBMISSION-AUTHORIZATION-FLOW");
		PaymentSetupResponse data1 = new PaymentSetupResponse();
		PaymentSetupResponseInitiation initiation = new PaymentSetupResponseInitiation();
		paymentValidationResponse.data(data1);
		data1.initiation(initiation);
		DebtorAccount debtorAccount = new DebtorAccount();
		initiation.setDebtorAccount(debtorAccount);
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts filteredAccounts =new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts();
		filteredAccounts.getAccount().add(new Accnt());
		
		
		Mockito.when(commonFilterUtility.retrieveCustomerAccountList(anyString(), anyMap())).thenReturn(filteredAccounts);
		Mockito.when(validatePaymentFoundationServiceUtility.createRequestHeaders(anyObject(), anyObject(), anyObject())).thenReturn(headers);
		Mockito.when(validatePaymentFoundationServiceUtility.transformRequestFromAPIToFS(anyObject(), anyObject())).thenReturn(paymentInstruction);
		Mockito.when(validatePaymentFoundationServiceClient.validatePayment(anyObject(), anyObject(), anyObject(), anyObject())).thenReturn(new ValidationPassed());
		Mockito.when(validatePaymentFoundationServiceUtility.transformResponseFromFSToAPI(anyObject())).thenReturn(paymentSetupValidationResponse);
		paymentSetupValidationResponse = adapter.preAuthorizationPaymentValidation(paymentValidationResponse, params);
		assertNotNull(paymentSetupValidationResponse);
	}
	
	@Test(expected = AdapterException.class)
	public void validatePaymentSetupTestAccountNotFoundException(){
		
	
		HttpHeaders headers = new HttpHeaders();
		ErrorInfo errorInfo = new ErrorInfo();
		errorInfo.setErrorCode("test");
		errorInfo.setErrorMessage("test");
		HashMap<String, String> params = new HashMap<>();
		HashMap<String, String> map = new HashMap<>();
		map.put(errorInfo.getErrorCode(), "test");
		PaymentInstruction paymentInstruction = new PaymentInstruction();
		PaymentSetupValidationResponse paymentSetupValidationResponse = new PaymentSetupValidationResponse();
		CustomPaymentSetupPOSTResponse paymentValidationResponse = new CustomPaymentSetupPOSTResponse();
		params.put("consentFlowType", "PISP-SUBMISSION-AUTHORIZATION-FLOW");
		PaymentSetupResponse data1 = new PaymentSetupResponse();
		PaymentSetupResponseInitiation initiation = new PaymentSetupResponseInitiation();
		paymentValidationResponse.data(data1);
		data1.initiation(initiation);
		DebtorAccount debtorAccount = new DebtorAccount();
		initiation.setDebtorAccount(debtorAccount);
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts filteredAccounts =new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts();
		
		
		Mockito.when(commonFilterUtility.retrieveCustomerAccountList(anyString(), anyMap())).thenReturn(filteredAccounts);
		Mockito.when(validatePaymentFoundationServiceUtility.createRequestHeaders(anyObject(), anyObject(), anyObject())).thenReturn(headers);
		Mockito.when(validatePaymentFoundationServiceUtility.transformRequestFromAPIToFS(anyObject(), anyObject())).thenReturn(paymentInstruction);
		Mockito.when(validatePaymentFoundationServiceClient.validatePayment(anyObject(), anyObject(), anyObject(), anyObject())).thenReturn(new ValidationPassed());
		Mockito.when(validatePaymentFoundationServiceUtility.transformResponseFromFSToAPI(anyObject())).thenReturn(paymentSetupValidationResponse);
		paymentSetupValidationResponse = adapter.preAuthorizationPaymentValidation(paymentValidationResponse, params);
		assertNotNull(paymentSetupValidationResponse);
	}
	
	
	/**
	 * Test payment setup validation internal server error.
	 */
	@Test(expected = AdapterException.class)
	public void validatePaymentSetupInternalServerErrorAdapterExceptionTest(){
		
		HttpHeaders headers = new HttpHeaders();
		ErrorInfo errorInfo = new ErrorInfo();
		errorInfo.setErrorCode("test");
		errorInfo.setErrorMessage("test");
		HashMap<String, String> params = new HashMap<>();
		HashMap<String, String> map = new HashMap<>();
		map.put(errorInfo.getErrorCode(), "test");
		PaymentInstruction paymentInstruction = new PaymentInstruction();
		PaymentSetupValidationResponse paymentSetupValidationResponse = new PaymentSetupValidationResponse();
		CustomPaymentSetupPOSTResponse paymentValidationResponse = new CustomPaymentSetupPOSTResponse();
		params.put("consentFlowType", "PISP_SUBMISSION_AUTHORIZATION_FLOW");
		ValidationViolations validationViolations = new ValidationViolations();
		ValidationViolation validationViolation = new ValidationViolation();
		validationViolation.setErrorCode("test");
		validationViolation.setErrorText("test^test");
		validationViolations.getValidationViolation().add(validationViolation);
		
		Mockito.when(validatePaymentFoundationServiceUtility.createRequestHeaders(anyObject(), anyObject(), anyObject())).thenReturn(headers);
		Mockito.when(validatePaymentFoundationServiceUtility.transformRequestFromAPIToFS(anyObject(), anyObject())).thenReturn(paymentInstruction);
		Mockito.when(validatePaymentFoundationServiceClient.validatePayment(anyObject(), anyObject(), anyObject(), anyObject())).thenThrow(new AdapterException("Adapter Exception", errorInfo, validationViolations));
		Mockito.when(adapterUtility.getThrowableError()).thenReturn(map);
		Mockito.when(validatePaymentFoundationServiceUtility.transformResponseFromFSToAPI(anyObject())).thenReturn(paymentSetupValidationResponse);	
		paymentSetupValidationResponse = adapter.preAuthorizationPaymentValidation(paymentValidationResponse, params);
	}
	
	@Test(expected = AdapterException.class)
	public void validatePaymentSetupInternalServerErrorAdapterExceptionTest2(){
		
		HttpHeaders headers = new HttpHeaders();
		ErrorInfo errorInfo = new ErrorInfo();
		errorInfo.setErrorCode("test");
		errorInfo.setErrorMessage("test");
		HashMap<String, String> params = new HashMap<>();
		HashMap<String, String> map = new HashMap<>();
		map.put(errorInfo.getErrorCode(), "test");
		PaymentInstruction paymentInstruction = new PaymentInstruction();
		PaymentSetupValidationResponse paymentSetupValidationResponse = new PaymentSetupValidationResponse();
		CustomPaymentSetupPOSTResponse paymentValidationResponse = new CustomPaymentSetupPOSTResponse();
		params.put("consentFlowType", "PISP_SUBMISSION_AUTHORIZATION_FLOW");
		ValidationViolations validationViolations = new ValidationViolations();
		ValidationViolation validationViolation = new ValidationViolation();
		validationViolation.setErrorCode("123");
		validationViolation.setErrorText("test^test");
		validationViolations.getValidationViolation().add(validationViolation);
		
		Mockito.when(validatePaymentFoundationServiceUtility.createRequestHeaders(anyObject(), anyObject(), anyObject())).thenReturn(headers);
		Mockito.when(validatePaymentFoundationServiceUtility.transformRequestFromAPIToFS(anyObject(), anyObject())).thenReturn(paymentInstruction);
		Mockito.when(validatePaymentFoundationServiceClient.validatePayment(anyObject(), anyObject(), anyObject(), anyObject())).thenThrow(new AdapterException("Adapter Exception", errorInfo, validationViolations));
		Mockito.when(adapterUtility.getThrowableError()).thenReturn(map);
		Mockito.when(validatePaymentFoundationServiceUtility.transformResponseFromFSToAPI(anyObject())).thenReturn(paymentSetupValidationResponse);	
		paymentSetupValidationResponse = adapter.preAuthorizationPaymentValidation(paymentValidationResponse, params);
		validationViolation.setErrorText("test");
		adapter.preAuthorizationPaymentValidation(paymentValidationResponse, params);
	}
	
	@Test(expected = AdapterException.class)
	public void validatePaymentSetupInternalServerErrorTestException(){
		
		HttpHeaders headers = new HttpHeaders();
		ErrorInfo errorInfo = new ErrorInfo();
		errorInfo.setErrorCode("test");
		errorInfo.setErrorMessage("test");
		HashMap<String, String> params = new HashMap<>();
		HashMap<String, String> map = new HashMap<>();
		map.put(errorInfo.getErrorCode(), "test");
		PaymentInstruction paymentInstruction = new PaymentInstruction();
		PaymentSetupValidationResponse paymentSetupValidationResponse = new PaymentSetupValidationResponse();
		CustomPaymentSetupPOSTResponse paymentValidationResponse = new CustomPaymentSetupPOSTResponse();
		params.put("consentFlowType", "PISP_SUBMISSION_AUTHORIZATION_FLOW");
		ValidationViolations validationViolations = new ValidationViolations();
		ValidationViolation validationViolation = new ValidationViolation();
		validationViolation.setErrorCode("123");
		validationViolation.setErrorText("test^test");
		validationViolations.getValidationViolation().add(validationViolation);
		
		Mockito.when(validatePaymentFoundationServiceUtility.createRequestHeaders(anyObject(), anyObject(), anyObject())).thenReturn(headers);
		Mockito.when(validatePaymentFoundationServiceUtility.transformRequestFromAPIToFS(anyObject(), anyObject())).thenReturn(paymentInstruction);
		Mockito.when(validatePaymentFoundationServiceClient.validatePayment(anyObject(), anyObject(), anyObject(), anyObject())).thenThrow(new PSD2Exception("test", errorInfo));
		Mockito.when(adapterUtility.getThrowableError()).thenReturn(map);
		Mockito.when(validatePaymentFoundationServiceUtility.transformResponseFromFSToAPI(anyObject())).thenReturn(paymentSetupValidationResponse);	
		paymentSetupValidationResponse = adapter.preAuthorizationPaymentValidation(paymentValidationResponse, params);
	}
	
	
	/**
	 * Test payment setup validation exception.
	 */
	@Test(expected = PSD2Exception.class)
	public void validatePaymentSetupPSD2ExceptionTest(){
		
		HttpHeaders headers = new HttpHeaders();
		HashMap<String, String> params = new HashMap<>();
		PaymentInstruction paymentInstruction = new PaymentInstruction();
		PaymentSetupValidationResponse paymentSetupValidationResponse = new PaymentSetupValidationResponse();
		CustomPaymentSetupPOSTResponse paymentValidationResponse = new CustomPaymentSetupPOSTResponse();
		params.put("consentFlowType", "PISP-SUBMISSION-AUTHORIZATION_FLOW");
		Mockito.when(validatePaymentFoundationServiceUtility.createRequestHeaders(anyObject(), anyObject(), anyObject())).thenReturn(headers);
		Mockito.when(validatePaymentFoundationServiceUtility.transformRequestFromAPIToFS(anyObject(), anyObject())).thenReturn(paymentInstruction);
		Mockito.when(validatePaymentFoundationServiceClient.validatePayment(anyObject(), anyObject(), anyObject(), anyObject())).thenThrow(PSD2Exception.populatePSD2Exception("Url cannot be null",ErrorCodeEnum.REST_TRANSPORT_ADAPTER_TECHNICAL_ERROR));
		Mockito.when(validatePaymentFoundationServiceUtility.transformResponseFromFSToAPI(anyObject())).thenReturn(paymentSetupValidationResponse);
		paymentSetupValidationResponse = adapter.preAuthorizationPaymentValidation(paymentValidationResponse, params);
	}
	
	

	}
