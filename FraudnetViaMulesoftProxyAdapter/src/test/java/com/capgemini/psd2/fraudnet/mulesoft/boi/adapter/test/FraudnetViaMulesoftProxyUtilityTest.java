package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.adapter.fraudnet.domain.FraudServiceResponse;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.constants.FraudnetViaMulesoftProxyConstants;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.FraudServiceRequest;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.transformer.FraudnetViaMulesoftProxyTransformer;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.utility.FraudnetViaMulesoftProxyUtility;
import com.capgemini.psd2.rest.client.model.RequestInfo;

@RunWith(SpringJUnit4ClassRunner.class)
public class FraudnetViaMulesoftProxyUtilityTest {
	
	@InjectMocks
	private FraudnetViaMulesoftProxyUtility fraudnetViaMulesoftProxyUtility;
	
	@Mock
	private FraudnetViaMulesoftProxyTransformer fraudnetViaMulesoftProxyTransformer;
	
	/**
	 * Sets the up.
	 */
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}

	@Test
	public void transformRequestFromAPIToFNTest(){
		
		Map<String, Map<String, Object>> fraudSystemRequest = new HashMap<String, Map<String, Object>>();
		FraudServiceRequest fraudServiceRequest = new FraudServiceRequest();
		
		Mockito.when(fraudnetViaMulesoftProxyTransformer.transformFraudnetRequest(anyObject())).thenReturn(fraudServiceRequest);
		
		FraudServiceRequest response = fraudnetViaMulesoftProxyUtility.transformRequestFromAPIToFN(fraudSystemRequest);		
		assertNotNull(response);
	}
	
	@Test
	public void transformResponseFromFNToAPITest(){
		
		FraudServiceResponse fraudServiceResponse = new FraudServiceResponse();
		
		Mockito.when(fraudnetViaMulesoftProxyTransformer.transformFraudnetResponse(anyObject())).thenReturn(fraudServiceResponse);
		
		FraudServiceResponse response = fraudnetViaMulesoftProxyUtility.transformResponseFromFNToAPI(fraudServiceResponse);		
		assertNotNull(response);
	}
	
	@Test
	public void createRequestHeadersTest(){
		
		RequestInfo requestInfo = new RequestInfo();
		Map<String, Map<String, Object>> fraudSystemRequest = new HashMap<String, Map<String, Object>>();
		ReflectionTestUtils.setField(fraudnetViaMulesoftProxyUtility, "interactionIdReqHeader", "x-api-transaction-id");
		ReflectionTestUtils.setField(fraudnetViaMulesoftProxyUtility, "clientIdReqHeader", "client_id");
		ReflectionTestUtils.setField(fraudnetViaMulesoftProxyUtility, "clientSecretReqHeader", "client_secret");
		ReflectionTestUtils.setField(fraudnetViaMulesoftProxyUtility, "clientIdReqHeaderValue", "test");
		ReflectionTestUtils.setField(fraudnetViaMulesoftProxyUtility, "clientSecretReqHeaderValue", "test");
		
		HttpHeaders response = fraudnetViaMulesoftProxyUtility.createRequestHeaders(requestInfo, fraudSystemRequest);
		assertNotNull(response);
		/*
		 * 
		 */
		Map<String, Object> eventMap = new HashMap<>();
		eventMap.put(FraudnetViaMulesoftProxyConstants.EVENT_ID, "test");
		fraudSystemRequest.put(FraudnetViaMulesoftProxyConstants.FRAUDREQUEST_EVENT_MAP, eventMap);
		response = fraudnetViaMulesoftProxyUtility.createRequestHeaders(requestInfo, fraudSystemRequest);
		assertNotNull(response);
		/*
		 * 
		 */
		eventMap.put(FraudnetViaMulesoftProxyConstants.EVENT_ID, null);
		response = fraudnetViaMulesoftProxyUtility.createRequestHeaders(requestInfo, fraudSystemRequest);
		assertNotNull(response);
	}
	
}
