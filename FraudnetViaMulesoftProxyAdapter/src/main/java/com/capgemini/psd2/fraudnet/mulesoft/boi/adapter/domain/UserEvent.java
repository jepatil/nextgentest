package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "source", "type", "time", "channelClassificationCode", "electronicDeviceSession",
		"onlineUserSession", "financialEventAmount", "person" })
public class UserEvent {

	@JsonProperty("source")
	private Source source;
	@JsonProperty("type")
	private String type;
	@JsonProperty("time")
	private String time;
	@JsonProperty("channelClassificationCode")
	private String channelClassificationCode;
	@JsonProperty("electronicDeviceSession")
	private ElectronicDeviceSession electronicDeviceSession;
	@JsonProperty("onlineUserSession")
    private OnlineUserSession onlineUserSession;
	@JsonProperty("financialEventAmount")
	private FinancialEventAmount financialEventAmount;
	@JsonProperty("person")
	private Person person;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("source")
	public Source getSource() {
		return source;
	}

	@JsonProperty("source")
	public void setSource(Source source) {
		this.source = source;
	}

	@JsonProperty("type")
	public String getType() {
		return type;
	}

	@JsonProperty("type")
	public void setType(String type) {
		this.type = type;
	}

	@JsonProperty("time")
	public String getTime() {
		return time;
	}

	@JsonProperty("time")
	public void setTime(String time) {
		this.time = time;
	}

	@JsonProperty("channelClassificationCode")
	public String getChannelClassificationCode() {
		return channelClassificationCode;
	}

	@JsonProperty("channelClassificationCode")
	public void setChannelClassificationCode(String channelClassificationCode) {
		this.channelClassificationCode = channelClassificationCode;
	}

	@JsonProperty("electronicDeviceSession")
	public ElectronicDeviceSession getElectronicDeviceSession() {
		return electronicDeviceSession;
	}

	@JsonProperty("electronicDeviceSession")
	public void setElectronicDeviceSession(ElectronicDeviceSession electronicDeviceSession) {
		this.electronicDeviceSession = electronicDeviceSession;
	}

	@JsonProperty("onlineUserSession")
    public OnlineUserSession getOnlineUserSession() {
		return onlineUserSession;
	}

	@JsonProperty("onlineUserSession")
    public void setOnlineUserSession(OnlineUserSession onlineUserSession) {
		this.onlineUserSession = onlineUserSession;
	}

	@JsonProperty("financialEventAmount")
	public FinancialEventAmount getFinancialEventAmount() {
		return financialEventAmount;
	}

	@JsonProperty("financialEventAmount")
	public void setFinancialEventAmount(FinancialEventAmount financialEventAmount) {
		this.financialEventAmount = financialEventAmount;
	}

	@JsonProperty("person")
	public Person getPerson() {
		return person;
	}

	@JsonProperty("person")
	public void setPerson(Person person) {
		this.person = person;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
