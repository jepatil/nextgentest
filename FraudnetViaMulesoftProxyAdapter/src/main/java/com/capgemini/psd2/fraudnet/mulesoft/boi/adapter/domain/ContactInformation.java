package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "emailAddress", "faxNumber", "homePhoneNumber", "mobilePhoneNumber", "workPhoneNumber", "otherPhoneNumber" })
public class ContactInformation {

	@JsonProperty("emailAddress")
	private String emailAddress;
	@JsonProperty("faxNumber")
	private String faxNumber;
	@JsonProperty("homePhoneNumber")
	private String homePhoneNumber;
	@JsonProperty("mobilePhoneNumber")
	private String mobilePhoneNumber;
	@JsonProperty("workPhoneNumber")
	private String workPhoneNumber;
	@JsonProperty("otherPhoneNumber")
	private String otherPhoneNumber;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("emailAddress")
	public String getEmailAddress() {
		return emailAddress;
	}

	@JsonProperty("emailAddress")
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	@JsonProperty("faxNumber")
	public String getFaxNumber() {
		return faxNumber;
	}

	@JsonProperty("faxNumber")
	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	@JsonProperty("homePhoneNumber")
	public String getHomePhoneNumber() {
		return homePhoneNumber;
	}

	@JsonProperty("homePhoneNumber")
	public void setHomePhoneNumber(String homePhoneNumber) {
		this.homePhoneNumber = homePhoneNumber;
	}

	@JsonProperty("mobilePhoneNumber")
	public String getMobilePhoneNumber() {
		return mobilePhoneNumber;
	}

	@JsonProperty("mobilePhoneNumber")
	public void setMobilePhoneNumber(String mobilePhoneNumber) {
		this.mobilePhoneNumber = mobilePhoneNumber;
	}

	@JsonProperty("workPhoneNumber")
	public String getWorkPhoneNumber() {
		return workPhoneNumber;
	}

	@JsonProperty("workPhoneNumber")
	public void setWorkPhoneNumber(String workPhoneNumber) {
		this.workPhoneNumber = workPhoneNumber;
	}
	
	@JsonProperty("otherPhoneNumber")
	public String getOtherPhoneNumber() {
		return otherPhoneNumber;
	}

	@JsonProperty("otherPhoneNumber")
	public void setOtherPhoneNumber(String otherPhoneNumber) {
		this.otherPhoneNumber = otherPhoneNumber;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
