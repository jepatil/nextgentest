
package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.utility;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.fraudnet.domain.FraudServiceResponse;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.constants.FraudnetViaMulesoftProxyConstants;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.FraudServiceRequest;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.transformer.FraudnetViaMulesoftProxyTransformer;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class FraudnetViaMulesoftProxyUtility {

	@Autowired
	private FraudnetViaMulesoftProxyTransformer fraudnetViaMulesoftProxyTransformer;
	
	@Value("${foundationService.interactionIdReqHeader:#{x-api-transaction-id}}")
	private String interactionIdReqHeader;

	@Value("${foundationService.clientIdReqHeader:#{client_id}}")
	private String clientIdReqHeader;

	@Value("${foundationService.clientSecretReqHeader:#{client_secret}}")
	private String clientSecretReqHeader;

	@Value("${foundationService.clientId}")
	private String clientIdReqHeaderValue;

	@Value("${foundationService.clientSecret}")
	private String clientSecretReqHeaderValue;
	
	
	public FraudServiceRequest transformRequestFromAPIToFN(Map<String, Map<String, Object>> fraudSystemRequest) {
		return fraudnetViaMulesoftProxyTransformer.transformFraudnetRequest(fraudSystemRequest);
	}

	public <T> T transformResponseFromFNToAPI(FraudServiceResponse fraudServiceResponse) {
		return fraudnetViaMulesoftProxyTransformer.transformFraudnetResponse(fraudServiceResponse);
	}

	public HttpHeaders createRequestHeaders(RequestInfo requestInfo, Map<String, Map<String, Object>> fraudnetRequest) {

		HttpHeaders httpHeaders = new HttpHeaders();
		
		Map<String, Object> eventMap = fraudnetRequest.get(FraudnetViaMulesoftProxyConstants.FRAUDREQUEST_EVENT_MAP);
		
		if(eventMap != null){
		    String interactionIdReqHeaderValue = (String) eventMap.get(FraudnetViaMulesoftProxyConstants.EVENT_ID);
			String eventTime = (String) eventMap.get(FraudnetViaMulesoftProxyConstants.EVENT_TIME);
			
			if(!NullCheckUtils.isNullOrEmpty(interactionIdReqHeaderValue)){				
				if(!NullCheckUtils.isNullOrEmpty(eventTime))
					interactionIdReqHeaderValue  = interactionIdReqHeaderValue + "_" + eventTime;
				
				httpHeaders.add(interactionIdReqHeader, interactionIdReqHeaderValue);
			}
		}
		
		httpHeaders.add(clientIdReqHeader, clientIdReqHeaderValue);
		httpHeaders.add(clientSecretReqHeader, clientSecretReqHeaderValue);
		return httpHeaders;
	}

}
