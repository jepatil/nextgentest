package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "groupReportingCurrency", "localReportingCurrency", "transactionCurrency" })
public class TransactionEventAmount {

	@JsonProperty("groupReportingCurrency")
	private Integer groupReportingCurrency;
	@JsonProperty("localReportingCurrency")
	private Integer localReportingCurrency;
	@JsonProperty("transactionCurrency")
	private String transactionCurrency;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("groupReportingCurrency")
	public Integer getGroupReportingCurrency() {
		return groupReportingCurrency;
	}

	@JsonProperty("groupReportingCurrency")
	public void setGroupReportingCurrency(Integer groupReportingCurrency) {
		this.groupReportingCurrency = groupReportingCurrency;
	}

	@JsonProperty("localReportingCurrency")
	public Integer getLocalReportingCurrency() {
		return localReportingCurrency;
	}

	@JsonProperty("localReportingCurrency")
	public void setLocalReportingCurrency(Integer localReportingCurrency) {
		this.localReportingCurrency = localReportingCurrency;
	}

	@JsonProperty("transactionCurrency")
	public String getTransactionCurrency() {
		return transactionCurrency;
	}

	@JsonProperty("transactionCurrency")
	public void setTransactionCurrency(String transactionCurrency) {
		this.transactionCurrency = transactionCurrency;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
