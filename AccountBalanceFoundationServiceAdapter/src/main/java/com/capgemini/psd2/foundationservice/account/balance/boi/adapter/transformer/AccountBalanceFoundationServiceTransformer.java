/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.balance.boi.adapter.transformer;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.adapter.enumerator.CurrencyEnum;
import com.capgemini.psd2.aisp.domain.Balance;
import com.capgemini.psd2.aisp.domain.Balance.CreditDebitIndicatorEnum;
import com.capgemini.psd2.aisp.domain.Balance.TypeEnum;
import com.capgemini.psd2.aisp.domain.BalancesGETResponse;
import com.capgemini.psd2.aisp.domain.Data5;
import com.capgemini.psd2.aisp.domain.Data5Amount;
import com.capgemini.psd2.aisp.transformer.AccountBalanceTransformer;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.constants.AccountBalanceFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.domain.Accnt;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.domain.Accounts;
import com.capgemini.psd2.validator.PSD2Validator;

/**
 * The Class AccountBalanceFoundationServiceTransformer.
 */
@Component
public class AccountBalanceFoundationServiceTransformer implements AccountBalanceTransformer {

	/** The psd 2 validator. */
	@Autowired
	@Qualifier("PSD2ResponseValidator")
	private PSD2Validator psd2Validator;
	
	@Autowired
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;


	/*
	 * (non-Javadoc)
	 * 
	 * @see com.capgemini.psd2.aisp.transformer.AcountBalanceTransformer#
	 * transformAccountBalance(java.lang.Object, java.util.Map)
	 */
	@Override
	public <T> BalancesGETResponse transformAccountBalance(T inputBalanceObj, Map<String, String> params) {
		Accounts accounts = (Accounts) inputBalanceObj;
		Data5 data5 = new Data5();
		List<Balance> balanceList = new ArrayList<>();
		data5.setBalance(balanceList);
		BalancesGETResponse balancesGETResponse = new BalancesGETResponse();
		balancesGETResponse.setData(data5);
		for (Accnt accnt : accounts.getAccount()) {
			Balance responseDataObj = new Balance();
			Data5Amount amount = new Data5Amount();
			String currency = accnt.getBalance().getCurrency();
			psd2Validator.validateEnum(CurrencyEnum.class, currency);
			String resAmount = null;
			if (accnt.getBalance().getAvailableBalance() != null) {
				resAmount = accnt.getBalance().getAvailableBalance().toString();
				if (!resAmount.contains("."))
					resAmount = resAmount + ".0";
			}
			amount.setCurrency(currency);
			if (accnt.getBalance().getAvailableBalance().compareTo(BigDecimal.ZERO) == 0
					|| accnt.getBalance().getAvailableBalance().compareTo(BigDecimal.ZERO) > 0) {
				responseDataObj.setCreditDebitIndicator(CreditDebitIndicatorEnum.CREDIT);
				amount.setAmount(resAmount);
			}
			else {
				responseDataObj.setCreditDebitIndicator(CreditDebitIndicatorEnum.DEBIT);
				if(resAmount != null) {
					resAmount = resAmount.replaceFirst("-", "");
					amount.setAmount(resAmount);
				}
			}
			responseDataObj.setAccountId(params.get(AccountBalanceFoundationServiceConstants.ACCOUNT_ID));
			responseDataObj.type(TypeEnum.EXPECTED);
			psd2Validator.validate(amount);
			responseDataObj.setAmount(amount);
			responseDataObj.setDateTime(timeZoneDateTimeAdapter.parseDateTimeCMA(accnt.getBalance().getAvailableBalanceTimestamp()));
			psd2Validator.validate(responseDataObj);
			/*
			 * here
			 */
			balancesGETResponse.getData().getBalance().add(responseDataObj);
		}
		psd2Validator.validate(balancesGETResponse);
		return balancesGETResponse;
	}

}
