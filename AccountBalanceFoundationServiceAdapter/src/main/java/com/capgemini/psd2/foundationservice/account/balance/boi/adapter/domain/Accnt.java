//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.10.11 at 12:56:04 PM IST 
//


package com.capgemini.psd2.foundationservice.account.balance.boi.adapter.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * Account for which the details are displayed
 * 			
 * 
 * <p>Java class for accnt complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="accnt">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="transactions" type="{http://bankofireland.com/channels/abt/}transactions" minOccurs="0"/>
 *         &lt;element name="balance" type="{http://bankofireland.com/channels/abt/}balance" minOccurs="0"/>
 *         &lt;element name="creditCardBalance" type="{http://bankofireland.com/channels/abt/}creditCardBalance" minOccurs="0"/>
 *         &lt;element name="creditCardTransactions" type="{http://bankofireland.com/channels/abt/}creditCardTransactions" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="nsc" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="accountNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="creditCardNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "accnt", propOrder = {
    "transactions",
    "balance",
    "creditCardBalance",
    "creditCardTransactions"
})
public class Accnt {

    protected Transactions transactions;
    protected Balance balance;
    protected CreditCardBalance creditCardBalance;
    protected CreditCardTransactions creditCardTransactions;
    @XmlAttribute(name = "nsc")
    protected String nsc;
    @XmlAttribute(name = "accountNumber")
    protected String accountNumber;
    @XmlAttribute(name = "creditCardNumber")
    protected String creditCardNumber;

    /**
     * Gets the value of the transactions property.
     * 
     * @return
     *     possible object is
     *     {@link Transactions }
     *     
     */
    public Transactions getTransactions() {
        return transactions;
    }

    /**
     * Sets the value of the transactions property.
     * 
     * @param value
     *     allowed object is
     *     {@link Transactions }
     *     
     */
    public void setTransactions(Transactions value) {
        this.transactions = value;
    }

    /**
     * Gets the value of the balance property.
     * 
     * @return
     *     possible object is
     *     {@link Balance }
     *     
     */
    public Balance getBalance() {
        return balance;
    }

    /**
     * Sets the value of the balance property.
     * 
     * @param value
     *     allowed object is
     *     {@link Balance }
     *     
     */
    public void setBalance(Balance value) {
        this.balance = value;
    }

    /**
     * Gets the value of the creditCardBalance property.
     * 
     * @return
     *     possible object is
     *     {@link CreditCardBalance }
     *     
     */
    public CreditCardBalance getCreditCardBalance() {
        return creditCardBalance;
    }

    /**
     * Sets the value of the creditCardBalance property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreditCardBalance }
     *     
     */
    public void setCreditCardBalance(CreditCardBalance value) {
        this.creditCardBalance = value;
    }

    /**
     * Gets the value of the creditCardTransactions property.
     * 
     * @return
     *     possible object is
     *     {@link CreditCardTransactions }
     *     
     */
    public CreditCardTransactions getCreditCardTransactions() {
        return creditCardTransactions;
    }

    /**
     * Sets the value of the creditCardTransactions property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreditCardTransactions }
     *     
     */
    public void setCreditCardTransactions(CreditCardTransactions value) {
        this.creditCardTransactions = value;
    }

    /**
     * Gets the value of the nsc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNsc() {
        return nsc;
    }

    /**
     * Sets the value of the nsc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNsc(String value) {
        this.nsc = value;
    }

    /**
     * Gets the value of the accountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * Sets the value of the accountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountNumber(String value) {
        this.accountNumber = value;
    }

    /**
     * Gets the value of the creditCardNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditCardNumber() {
        return creditCardNumber;
    }

    /**
     * Sets the value of the creditCardNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditCardNumber(String value) {
        this.creditCardNumber = value;
    }

}
