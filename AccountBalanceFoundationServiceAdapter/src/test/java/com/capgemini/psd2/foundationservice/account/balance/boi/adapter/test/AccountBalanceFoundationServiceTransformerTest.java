/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.balance.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.domain.BalancesGETResponse;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.domain.Accnt;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.domain.Accounts;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.domain.Balance;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.transformer.AccountBalanceFoundationServiceTransformer;
import com.capgemini.psd2.validator.impl.PSD2ResponseValidatorImpl;


/**
 * The Class AccountBalanceFoundationServiceTransformerTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountBalanceFoundationServiceTransformerTest {
	
	/** The psd 2 validator. */
	@Mock
	private PSD2ResponseValidatorImpl psd2Validator;

	/** The account balance FS transformer. */
	@InjectMocks
	private AccountBalanceFoundationServiceTransformer accountBalanceFSTransformer;
	
	@Mock
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;
	
	
	
	/**
	 * Sets the up.
	 */
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Test transform account balance.
	 */
	@Test
	public void testTransformAccountBalance() {
		
		Balance bal = new Balance();
		bal.setAvailableBalance(new BigDecimal(5000.00d));
		bal.setCurrency("GBP");
		bal.setPostedBalance(new BigDecimal(5000.00d));
		Accounts accounts = new Accounts();
		Accnt accnt = new Accnt();
		accnt.setBalance(bal);
		accounts.getAccount().add(accnt);
		
		Map<String, String> params = new HashMap<>();
		params.put("accountId", "12345");
		
		BalancesGETResponse res = accountBalanceFSTransformer.transformAccountBalance(accounts, params);
		assertNotNull(res);
	}
	
	/**
	 * Test transform account balance account id null.
	 */
	@Test(expected=AdapterException.class)
	public void testTransformAccountBalanceAccountIdNull() {
		
		Balance bal = new Balance();
		bal.setAvailableBalance(new BigDecimal(5000.00d));
		bal.setCurrency("GBP");
		bal.setPostedBalance(new BigDecimal(5000.00d));
		Accounts accounts = new Accounts();
		Accnt accnt = new Accnt();
		accnt.setBalance(bal);
		accounts.getAccount().add(accnt);	
		Map<String, String> params = new HashMap<>();
		params.put("accountId", null);
		doThrow(AdapterException.class).when(psd2Validator).validate(any(com.capgemini.psd2.aisp.domain.Balance.class));
		BalancesGETResponse res = accountBalanceFSTransformer.transformAccountBalance(accounts, params);
		assertNotNull(res);
	}
	
	/**
	 * Test transform account balance currency failure.
	 */
	@Test(expected=AdapterException.class)
	public void testTransformAccountBalanceCurrencyFailure() {	
		
		Balance bal = new Balance();
		bal.setAvailableBalance(new BigDecimal(5000.00d));
		bal.setCurrency("GBPDS");
		bal.setPostedBalance(new BigDecimal(5000.00d));
		Accounts accounts = new Accounts();
		Accnt accnt = new Accnt();
		accnt.setBalance(bal);
		accounts.getAccount().add(accnt);	
		Map<String, String> params = new HashMap<>();
		params.put("accountId", "1234");
		doThrow(AdapterException.class).when(psd2Validator).validate(any(com.capgemini.psd2.aisp.domain.Data5Amount.class));
//		doThrow(AdapterException.class).when(psd2Validator).validate(any(com.capgemini.psd2.aisp.domain.BalancesGETResponseAmount.class));
		BalancesGETResponse res = accountBalanceFSTransformer.transformAccountBalance(accounts, params);
		assertNotNull(res);
	}
	
	/**
	 * Test transform account balance amount failure.
	 */
	@Test(expected=AdapterException.class)
	public void testTransformAccountBalanceAmountFailure() {
		
		Balance bal = new Balance();
		bal.setAvailableBalance(new BigDecimal(670.029239934));
		bal.setCurrency("GBPDS");
		bal.setPostedBalance(new BigDecimal(670.029239934));
		Accounts accounts = new Accounts();
		Accnt accnt = new Accnt();
		accnt.setBalance(bal);
		accounts.getAccount().add(accnt);	
		Map<String, String> params = new HashMap<>();
		params.put("accountId", "1234");
		doThrow(AdapterException.class).when(psd2Validator).validate(any(com.capgemini.psd2.aisp.domain.Data5Amount.class));
//		doThrow(AdapterException.class).when(psd2Validator).validate(any(com.capgemini.psd2.aisp.domain.BalancesGETResponseAmount.class));
		BalancesGETResponse res = accountBalanceFSTransformer.transformAccountBalance(accounts, params);
		assertNotNull(res);
	}
	
	@Test(expected=AdapterException.class)
	public void testTransformAccountBalanceCurrencyDebit() {	
		
		Balance bal = new Balance();
		bal.setAvailableBalance(new BigDecimal(-5000.00d));
		bal.setCurrency("GBPDS");
		bal.setPostedBalance(new BigDecimal(5000.00d));
		Accounts accounts = new Accounts();
		Accnt accnt = new Accnt();
		accnt.setBalance(bal);
		accounts.getAccount().add(accnt);	
		Map<String, String> params = new HashMap<>();
		params.put("accountId", "1234");
		doThrow(AdapterException.class).when(psd2Validator).validate(any(com.capgemini.psd2.aisp.domain.Data5Amount.class));
//		doThrow(AdapterException.class).when(psd2Validator).validate(any(com.capgemini.psd2.aisp.domain.BalancesGETResponseAmount.class));
		BalancesGETResponse res = accountBalanceFSTransformer.transformAccountBalance(accounts, params);
		assertNotNull(res);
	}
	
}
