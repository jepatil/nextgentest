package com.capgemini.psd2.account.beneficiaries.mock.foundationservice.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.account.beneficiaries.mock.foundationservice.domain.Beneficiaries;

public interface AccountBeneficiariesRepository extends MongoRepository<Beneficiaries, String>{

	public Beneficiaries findByNscAndAccountNumber(String nsc, String accountNumber);
}
