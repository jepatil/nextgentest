package com.capgemini.psd2.account.beneficiaries.mock.foundationservice.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.account.beneficiaries.mock.foundationservice.domain.Beneficiaries;
import com.capgemini.psd2.account.beneficiaries.mock.foundationservice.repository.AccountBeneficiariesRepository;
import com.capgemini.psd2.account.beneficiaries.mock.foundationservice.service.AccountBeneficiariesService;
import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;

@Service
public class AccountBeneficiariesServiceImpl implements AccountBeneficiariesService{

	
	@Autowired
	AccountBeneficiariesRepository repository;

	@Override
	public Beneficiaries getBeneficiaries(String userId, String nsc, String accountNumber) {
		Beneficiaries beneficiaries = repository.findByNscAndAccountNumber(nsc, accountNumber);
		if(null == beneficiaries){
				throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.NO_BENEFICIARY_FOUND_ESBE);
		} else{
			return beneficiaries;
		}
	}
}
