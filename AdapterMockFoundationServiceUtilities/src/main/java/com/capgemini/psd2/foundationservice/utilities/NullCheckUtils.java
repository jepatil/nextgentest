
package com.capgemini.psd2.foundationservice.utilities;

import org.springframework.util.StringUtils;

public final class NullCheckUtils {

	private NullCheckUtils() {

	}

	public static boolean isNullOrEmpty(Object obj) {
		boolean isEmpty = Boolean.FALSE;
		if (obj == null || (obj instanceof String && (!StringUtils.hasText((String) obj)))) {
			isEmpty = Boolean.TRUE;
		}
		return isEmpty;
	}
}
