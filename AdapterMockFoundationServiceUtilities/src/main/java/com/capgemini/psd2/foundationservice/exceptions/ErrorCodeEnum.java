package com.capgemini.psd2.foundationservice.exceptions;

import org.springframework.http.HttpStatus;

public enum ErrorCodeEnum {
	
	/*
	 * For PreStageValidatePayment
	 */
	BAD_REQUEST_PMPSV("FS_PMPSV_001", "Bad request. Please check your request", "The request parameters passed are invalid.", HttpStatus.BAD_REQUEST),
	
	AUTHENTICATION_FAILURE_PMPSV("FS_PMPSV_002", "You are not authorised to use this service.", "Absence of BOI Authentication Headers.", HttpStatus.UNAUTHORIZED),
	
	GENERAL_ERROR_PMPSV("FS_PMPSV_003", "General error", "General error", HttpStatus.BAD_REQUEST),
	
	CONNECTION_ERROR_PMPSV("FS_PMPSV_004", "Connection Error", "Connection Error", HttpStatus.INTERNAL_SERVER_ERROR),
	
	IBAN_PROVIDED_IS_INVALID_PMPSV("FS_PMPSV_006", "IBAN provided is invalid", "IBAN provided is invalid", HttpStatus.BAD_REQUEST),
	
	BIC_PROVIDED_IS_INVALID_PMPSV("FS_PMPSV_007", "BIC provided is invalid", "BIC provided is invalid", HttpStatus.BAD_REQUEST),
	
	INVALID_IBAN_CHECKSUM_PMPSV("FS_PMPSV_008", "Invalid IBAN checksum", "Invalid IBAN checksum", HttpStatus.BAD_REQUEST),
	
	BIC_IS_NOT_REACHABLE_PMPSV("FS_PMPSV_009", "BIC is not reachable", "BIC is not reachable", HttpStatus.BAD_REQUEST),
	
	ACCOUNT_NOT_FOUND_OR_CLOSED_PMPSV("FS_PMPSV_010", "Account not found or closed", "Account not found or closed", HttpStatus.BAD_REQUEST),
	
	BIC_AND_IBAN_DO_NOT_MATCH_PMPSV("FS_PMPSV_011", "BIC and IBAN do not match", "BIC and IBAN do not match", HttpStatus.BAD_REQUEST),
	
	NSC_PROVIDED_IS_INVALID_PMPSV("FS_PMPSV_012", "NSC provided is invalid", "NSC provided is invalid", HttpStatus.BAD_REQUEST),
	
	ACCOUNT_NUMBER_PROVIDED_IS_INVALID_PMPSV("FS_PMPSV_013", "Account number provided is invalid", "Account number provided is invalid", HttpStatus.BAD_REQUEST),
	
	TRANSACTION_CURRENCY_CODE_IS_INVALID_PMPSV("FS_PMPSV_014", "Transaction currency code is invalid", "Transaction currency code is invalid", HttpStatus.BAD_REQUEST),	
	
	BANK_DOES_NOT_PROCESS_COUNTRY_PMPSV("FS_PMPSV_015", "Bank does not process payments to the requested beneficiary country", "Bank does not process payments to the requested beneficiary country", HttpStatus.BAD_REQUEST),	
	
	BANK_DOES_NOT_PROCESS_CURRENCY_PMPSV("FS_PMPSV_016", "Bank does not deal in the requested transaction currency", "Bank does not deal in the requested transaction currency", HttpStatus.BAD_REQUEST),
	
	COUNTRY_CODE_IS_INVALID_PMPSV("FS_PMPSV_017", "Country code is invalid", "Country code is invalid", HttpStatus.BAD_REQUEST),

	ACCOUNT_IS_BLOCKED_TO_DEBITS_PMPSV("FS_PMPSV_018", "Account is blocked to debits", "Account is blocked to debits", HttpStatus.BAD_REQUEST),

	ACCOUNT_IS_BLOCKED_TO_CREDITS_PMPSV("FS_PMPSV_019", "Account is blocked to credits", "Account is blocked to credits", HttpStatus.BAD_REQUEST),

	ACCOUNT_IS_BLOCKED_TO_ALL_PMPSV("FS_PMPSV_020", "Account is blocked to All", "Account is blocked to All", HttpStatus.BAD_REQUEST),

	CREDIT_GRADE_NOT_6_OR_7_PMPSV("FS_PMPSV_021","Credit grade cannot be 6 or 7", "Credit grade cannot be 6 or 7", HttpStatus.BAD_REQUEST),

	ACCOUNT_CANNOT_BE_DORMANT_PMPSV("FS_PMPSV_022","Account cannot be dormant", "Account cannot be dormant", HttpStatus.BAD_REQUEST),

	ACCOUNT_CANNOT_BE_LIEN_PMPSV("FS_PMPSV_023","Account cannot be lien", "Account cannot be lien", HttpStatus.BAD_REQUEST),

	JOINT_ACCOUNT_PERMITTED_ALL_CUSTOMER_PMPSV("FS_PMPSV_024","Joint account must be permitted by all the customer associated", "Joint account must be permitted by all the customer associated", HttpStatus.BAD_REQUEST),

	INVALID_PAYMENT_REQUEST_PMPSV("FS_PMPSV_029","Invalid payment request", "Invalid payment request", HttpStatus.BAD_REQUEST),

	SWIFT_ADDRESS_IS_INVALID_PMPSV("FS_PMPSV_030","Swift address is invalid", "Swift address is invalid", HttpStatus.BAD_REQUEST),

	ABA_CODE_IS_INVALID_PMPSV("FS_PMPSV_031","ABA code is invalid", "ABA code is invalid", HttpStatus.BAD_REQUEST),
	
	
	/*
	 * For ValidatePayment
	 */
	
	BAD_REQUEST_PMV("FS_PMV_001", "Bad request. Please check your request", "The request parameters passed are invalid.", HttpStatus.BAD_REQUEST),
	
	AUTHENTICATION_FAILURE_PMV("FS_PMV_002", "You are not authorised to use this service.", "Absence of BOI Authentication Headers.", HttpStatus.UNAUTHORIZED),
	
	NOT_FOUND_PMV("FS_PMV_003", "No payment instruction found for the requested information", "No payment instruction found for the requested information", HttpStatus.NOT_FOUND),
	
	GENERAL_ERROR_PMV("FS_PMV_004", "General error", "General error", HttpStatus.BAD_REQUEST),
	
	INVALID_IBAN_LENGTH_PMV("FS_PMV_006","IBAN length is not valid", "IBAN length is not valid", HttpStatus.BAD_REQUEST),
	
	NOT_SEPA_IBAN_PMV("FS_PMV_007","IBAN does not belong to SEPA zone", "IBAN does not belong to SEPA zone", HttpStatus.BAD_REQUEST),
	
	INVALID_IBAN_CHECKSUM_PMV("FS_PMV_008", "IBAN checksum is not valid", "IBAN checksum is not valid", HttpStatus.BAD_REQUEST),
	
	NO_SUCH_SEPA_BIC_PMV("FS_PMV_009", "BIC does not belong to SEPA zone", "BIC does not belong to SEPA zone", HttpStatus.BAD_REQUEST),
	
	BIC_IS_NOT_REACHABLE_PMV("FS_PMV_010", "BIC is not reachable", "BIC is not reachable", HttpStatus.BAD_REQUEST),
	
	BIC_AND_IBAN_DO_NOT_MATCH_PMV("FS_PMV_011", "BIC and IBAN do not match", "BIC and IBAN do not match", HttpStatus.BAD_REQUEST),
	
	NOT_A_VALID_NSC_PMV("FS_PMV_012", "NSC is not valid", "NSC is not valid", HttpStatus.BAD_REQUEST),
	
	NOT_A_VALID_ACCOUNT_NUMBER_PMV("FS_PMV_013", "Account Number is not valid", "Account Number is not valid", HttpStatus.BAD_REQUEST),
	
	NOT_A_VALID_CURRENCY_PMV("FS_PMV_014", "Currency is not valid", "Currency is not valid", HttpStatus.BAD_REQUEST),	
	
	ACCOUNT_DOES_NOT_EXISTS_PMV("FS_PMV_015", "Account does not exist", "Account does not exist", HttpStatus.BAD_REQUEST),	
	
	ACCOUNT_STATUS_MUST_BE_ACTIVE_PMV("FS_PMV_016", "Account status must be active", "Account status must be active", HttpStatus.BAD_REQUEST),
	
	INVALID_ACCOUNT_TYPE_PMV("FS_PMV_017","Account Type is not valid", "Account Type is not valid", HttpStatus.BAD_REQUEST),
	
	ACCOUNT_IS_BLOCKED_TO_DEBITS_PMV("FS_PMV_018", "Account is blocked to debits", "Account is blocked to debits", HttpStatus.BAD_REQUEST),

	ACCOUNT_IS_BLOCKED_TO_CREDITS_PMV("FS_PMV_019", "Account is blocked to credits", "Account is blocked to credits", HttpStatus.BAD_REQUEST),

	ACCOUNT_IS_BLOCKED_TO_ALL_PMV("FS_PMV_020", "Account is blocked to All", "Account is blocked to All", HttpStatus.BAD_REQUEST),
	
	CREDIT_GRADE_NOT_6_OR_7_PMV("FS_PMV_021","Credit grade cannot be 6 or 7", "Credit grade cannot be 6 or 7", HttpStatus.BAD_REQUEST),
	
	ACCOUNT_CANNOT_BE_DORMANT_PMV("FS_PMV_022","Account cannot be dormant", "Account cannot be dormant", HttpStatus.BAD_REQUEST),
	
	ACCOUNT_CANNOT_BE_LIEN_PMV("FS_PMV_023","Account cannot be lien", "Account cannot be lien", HttpStatus.BAD_REQUEST),
	
	JOINT_ACCOUNT_PERMITTED_ALL_CUSTOMER_PMV("FS_PMV_024","Joint account must be permitted by all the customer associated", "Joint account must be permitted by all the customer associated", HttpStatus.BAD_REQUEST),
	
	FUNDS_NOT_AVAILABLE_PMV("FS_PMV_025","Account does not have enough funds", "Account does not have enough funds", HttpStatus.BAD_REQUEST),
	
	LIMIT_EXCEEDED_FOR_CUSTOMER_PMV("FS_PMV_026","Limit exceeded for the customer", "Limit exceeded for the customer", HttpStatus.BAD_REQUEST),
	
	LIMIT_EXCEEDED_FOR_PAYMENT_TYPE_CUSTOMER("FS_PMV_027","Limit exceeded on this payment type for the customer", "Limit exceeded on this payment type for the customer", HttpStatus.BAD_REQUEST),
	
	LIMIT_EXCEEDED_FOR_AGENT_PMV("FS_PMV_028","Limit exceeded for this agent", "Limit exceeded for this agent", HttpStatus.BAD_REQUEST),
	
	INVALID_PAYMENT_REQUEST_PMV("FS_PMV_029","Invalid payment request", "Invalid payment request", HttpStatus.BAD_REQUEST),
	
	SWIFT_ADDRESS_IS_INVALID_PMV("FS_PMV_030","Swift address is invalid", "Swift address is invalid", HttpStatus.BAD_REQUEST),

	ABA_CODE_IS_INVALID_PMV("FS_PMV_031","ABA code is invalid", "ABA code is invalid", HttpStatus.BAD_REQUEST),
	
	COUNTRY_IS_INVALID_PMV("FS_PMV_032","Country is invalid", "Country is invalid", HttpStatus.BAD_REQUEST),

	COUNTRY_RULES_LIMIT_EXCEEDED_PMV("FS_PMV_033","Country rules limit exceeded", "Country rules limit exceeded", HttpStatus.BAD_REQUEST),

	USER_ID_INVALID_PMV("FS_PMV_034","User id is invalid", "User id is invalid", HttpStatus.BAD_REQUEST),

	JURISDICTION_CODE_INVALID_PMV("FS_PMV_035","Jurisdiction code is invalid", "Jurisdiction code is invalid", HttpStatus.BAD_REQUEST),

	CIS_ID_INVALID_PMV("FS_PMV_036","CIS id is invalid", "CIS id is invalid", HttpStatus.BAD_REQUEST),

	PAYER_PAYEE_ACCOUNT_SAME_PMV("FS_PMV_037","The payer and the payee account cannot be the same", "The payer and the payee account cannot be the same", HttpStatus.BAD_REQUEST),

	BIC_PROVIDER_IS_INVALID_PMV("FS_PMV_038","BIC provided is invalid", "BIC provided is invalid", HttpStatus.BAD_REQUEST),
	
	/*
	 *Start For stagePaymentInsert
	 */
	BAD_REQUEST_PMI("FS_PMI_001","Bad Request","Please check your request",HttpStatus.BAD_REQUEST),
	AUTHENTICATION_FAILURE_PMI("FS_PMI_002", "You are not authorised to use this service.", "Absence of BOI Authentication Headers.", HttpStatus.UNAUTHORIZED),
	NO_TXN_DETAILS_FOUND_PMI("FS_PMI_003", "No transaction details found for requested account(s)","Not Found",HttpStatus.BAD_REQUEST),
	GENERAL_ERROR_PMI("FS_PMI_004", "General error","General error", HttpStatus.BAD_REQUEST),
	
	/*
	 *Start For stagePaymentUpdate
	 */
	BAD_REQUEST_PMU("FS_PMU_001","Bad Request","Please check your request",HttpStatus.BAD_REQUEST),
	AUTHENTICATION_FAILURE_PMU("FS_PMU_002", "You are not authorised to use this service.", "Absence of BOI Authentication Headers.", HttpStatus.UNAUTHORIZED),
	NO_TXN_DETAILS_FOUND_PMU("FS_PMU_003", "No transaction details found for requested account(s)","Not Found",HttpStatus.BAD_REQUEST),
	GENERAL_ERROR_PMU("FS_PMU_004", "General error","General error", HttpStatus.BAD_REQUEST),
	
	
	/*
	 * For RetrivevePayment
	 */
	
	BAD_REQUEST_PMR("FS_PMR_001","Bad Request","Please check your request",HttpStatus.NOT_FOUND),
	AUTHENTICATION_FAILURE_PMR("FS_PMR_002","You are not authorised to use this service.", "Absence of BOI Authentication Headers.", HttpStatus.UNAUTHORIZED),
	NO_PAYMENT_DETAILS_FOUND_PMR("FS_PMR_003","No transaction details found for the requested account(s)","Not Found",HttpStatus.NOT_FOUND),
	GENERAL_ERROR_PMR("FS_PMR_004", "General error","General error", HttpStatus.NOT_FOUND),

	/*
	 *Start For PaymentSubmissionCreate
	 */
	
	BAD_REQUEST_PME("FS_PME_001", "Bad request.Please check your request", "Bad request", HttpStatus.BAD_REQUEST),
	
	AUTHENTICATION_FAILURE_PME("FS_PME_002", "You are not authorised to use this service.", "Absence of BOI Authentication Headers.", HttpStatus.UNAUTHORIZED),
	
	NOT_FOUND_PME("FS_PME_003", "No payment instruction found for the requested information", "No payment instruction found for the requested information", HttpStatus.NOT_FOUND),
	
	GENERAL_ERROR_PME("FS_PME_004", "General Error", "General Error", HttpStatus.BAD_REQUEST),
	
	INVALID_IBAN_LENGTH_PME("FS_PME_006", "IBAN length is not valid", "IBAN length is not valid", HttpStatus.BAD_REQUEST),
	
	NOT_SEPA_IBAN_PME("FS_PME_007", "IBAN does not belong to SEPA zone", "IBAN does not belong to SEPA zone", HttpStatus.BAD_REQUEST),
	
	INVALID_IBAN_CHEKSUM_PME("FS_PME_008", "IBAN checksum is not valid", "IBAN checksum is not valid", HttpStatus.BAD_REQUEST),
	
	NO_SUCH_SEPA_BIC_PME("FS_PME_009", "BIC does not belong to SEPA zone", "BIC does not belong to SEPA zone", HttpStatus.BAD_REQUEST),
	
	BIC_NOT_REACHABLE_PME("FS_PME_010", "BIC is not reachable", "BIC is not reachable", HttpStatus.BAD_REQUEST),
	
	BIC_IBAN_MISMATCH_PME("FS_PME_011", "BIC and IBAN do not match", "BIC and IBAN do not match", HttpStatus.BAD_REQUEST),
	
	NSC_INVALID_PME("FS_PME_012", "NSC is not valid", "NSC is not valid", HttpStatus.BAD_REQUEST),
	
	ACCOUNT_INVALID_PME("FS_PME_013", "Account Number is not valid", "Account Number is not valid", HttpStatus.BAD_REQUEST),
	
	CURRENCY_INVALID_PME("FS_PME_014", "Currency is not valid", "Currency is not valid", HttpStatus.BAD_REQUEST),
	
	NO_ACCOUNT_EXISTS_PME("FS_PME_015", "Account does not exist", "Account does not exist", HttpStatus.BAD_REQUEST),
	
	ACCOUNT_STATUS_PME("FS_PME_016", "Account status must be active", "Account status must be active", HttpStatus.BAD_REQUEST),
	
	ACCOUNT_TYPE_INVALID_PME("FS_PME_017", "Account Type is not valid", "Account Type is not valid", HttpStatus.BAD_REQUEST),
	
	ACCOUNT_BLOCKED_TO_DEBITS_PME("FS_PME_018", "Account is blocked to debits", "Account is blocked to debits", HttpStatus.BAD_REQUEST),
	
	ACCOUNT_BLOCKED_TO_CREDITS_PME("FS_PME_019", "Account is blocked to credits", "Account is blocked to credits", HttpStatus.BAD_REQUEST),
	
	ACCOUNT_BLOCKED_TO_ALL_PME("FS_PME_020", "Account is blocked to All", "Account is blocked to All", HttpStatus.BAD_REQUEST),
	
	CREDIT_GRADE_PME("FS_PME_021", "Credit grade cannot be 6 or 7", "Credit grade cannot be 6 or 7", HttpStatus.BAD_REQUEST),
	
	ACCOUNT_DORMANT_PME("FS_PME_022", "Account cannot be dormant", "Account cannot be dormant", HttpStatus.BAD_REQUEST),
	
	ACCOUNT_LIEN_PME("FS_PME_023", "Account cannot be lien", "Account cannot be lien", HttpStatus.BAD_REQUEST),
	
	JOINT_AMOUNT_PME("FS_PME_024", "Joint account must be permitted by all the customer associated", "Joint account must be permitted by all the customer associated", HttpStatus.BAD_REQUEST),
	
	FUNDS_UNAVAILABLE_PME("FS_PME_025", "Account does not have enough funds", "Account does not have enough funds", HttpStatus.BAD_REQUEST),
	
	CUSTOMER_LIMITS_EXCEEDED_PME("FS_PME_026", "Limit exceeded for the customer", "Limit exceeded for the customer", HttpStatus.BAD_REQUEST),
	
	TRANSACTION_LIMITS_EXCEEDED_PME("FS_PME_027", "Limit exceeded on this payment type for the customer", "Limit exceeded on this payment type for the customer", HttpStatus.BAD_REQUEST),
	
	AGENT_LIMITS_EXCEEDED_PME("FS_PME_028", "Limits exceeded for this agent", "Limits exceeded for this agent", HttpStatus.BAD_REQUEST),
	
	INVALID_PAYMENT_REQUEST_PME("FS_PME_029","Invalid Payment Request", "Invalid Payment Request", HttpStatus.BAD_REQUEST),
	
	SWIFT_ADDRESS_IS_INVALID_PME("FS_PME_030","Swift address is invalid", "Swift address is invalid", HttpStatus.BAD_REQUEST),
	
	ABA_CODE_IS_INVALID_PME("FS_PME_031","ABA code is invalid", "ABA code is invalid", HttpStatus.BAD_REQUEST),
	
	COUNTRY_IS_INVALID_PME("FS_PME_032","Country is invalid", "Country is invalid", HttpStatus.BAD_REQUEST),
	
	COUNTRY_RULES_LIMIT_EXCEEDED_PME("FS_PME_033","Country rules limit exceeded", "Country rules limit exceeded", HttpStatus.BAD_REQUEST),
	
	USER_ID_INVALID_PME("FS_PME_034","User id is invalid", "User id is invalid", HttpStatus.BAD_REQUEST),
	
	JURISDICTION_CODE_INVALID_PME("FS_PME_035","Jurisdiction code is invalid", "Jurisdiction code is invalid", HttpStatus.BAD_REQUEST),
	
	CIS_ID_INVALID_PME("FS_PME_036","CIS id is invalid", "CIS id is invalid", HttpStatus.BAD_REQUEST),
	
	PAYER_PAYEE_ACCOUNT_SAME_PME("FS_PME_037","The payer and the payee account cannot be the same", "The payer and the payee account cannot be the same", HttpStatus.BAD_REQUEST),
	
	PAYMENT_REJECTED_PME("FS_PME_038","Payment rejected due to the suspicion of fraud", "Payment rejected due to the suspicion of fraud", HttpStatus.BAD_REQUEST),
	
	/*
	 *Start For B365 and BOL Authentication 
	 */
	
	AUTHENTICATION_FAILURE_LOGIN_AUTH("FS_AUTH_001", "Authentication failed.", "Authentication failed.", HttpStatus.BAD_REQUEST),
	USER_TEMPORARILY_BLOCKED_AUTH("FS_AUTH_002", "User temporarily blocked.", "User temporarily blocked.", HttpStatus.BAD_REQUEST),
	USER_BLOCKED_ON_B365_OR_BOL_AUTH("FS_AUTH_003", "User blocked on B365 or BOL", "User blocked on B365 or BOL", HttpStatus.BAD_REQUEST),
	USER_BLOCKED_ON_HID_AUTH("FS_AUTH_004", "User blocked on HID.", "User blocked on HID.", HttpStatus.BAD_REQUEST),
	USER_IS_NOT_REGISTERED_FOR_HID_AUTH("FS_AUTH_005", "User is not registered for HID.", "User is not registered for HID.", HttpStatus.BAD_REQUEST),
	USER_DOES_NOT_EXIT_ON_HID_AUTH("FS_AUTH_006", "User does not exist on HID.", "User does not exist on HID.", HttpStatus.BAD_REQUEST),
	USER_NAME_DOES_NOT_MATCH_HEADER_AUTH("FS_AUTH_007", "username does not match header.", "username does not match header.", HttpStatus.BAD_REQUEST),
	AUTHENTICATION_FAILURE_HEADER_AUTH("FS_AUTH_008", "You are not authorised to use this service.", "You are not authorised to use this service.", HttpStatus.UNAUTHORIZED),
	GENERAL_ERROR_AUTH("FS_AUTH_010", "General Error", "General Error", HttpStatus.BAD_REQUEST),
	INTERNAL_SERVER_ERROR("FS_AUTH_009", "Technical Error. Please try again later", "Technical Error. Please try again later", HttpStatus.INTERNAL_SERVER_ERROR),
	
	/*
	 * For AccountBeneficiary
	 */
	AUTHENTICATION_FAILURE_ESBE("FS_ESBE_002", "You are not authorised to use this service.", "You are not authorised to use this service.", HttpStatus.UNAUTHORIZED), 
	NO_BENEFICIARY_FOUND_ESBE("FS_ESBE_003", "No Beneficiary found.", "No Beneficiary found.", HttpStatus.NOT_FOUND),
	INTERNAL_SERVER_ERROR_ESBE("FS_ESBE_004", "Technical Error. Please try again later.", "Technical Error. Please try again later.", HttpStatus.INTERNAL_SERVER_ERROR),
	
	/*
	 *Start For StandingOrders 
	 */
	AUTHENTICATION_FAILURE_ESSO("FS_ESSO_002", "You are not authorised to use this service.", "You are not authorised to use this service.", HttpStatus.UNAUTHORIZED),
    NO_STANDINGORDER_FOUND_ESSO("FS_ESSO_003","No StandingOrders Found","No StandingOrders Found",HttpStatus.NOT_FOUND),
    INTERNAL_SERVER_ERROR_ESSO("FS_ESSO_004", "Technical Error. Please try again later", "Technical Error. Please try again later", HttpStatus.INTERNAL_SERVER_ERROR),
	
    /*
	 *Start For Product Details
	 */
    AUTHENTICATION_FAILURE_ESPR("FS_ESPR_002", "You are not authorised to use this service.", "You are not authorised to use this service.", HttpStatus.UNAUTHORIZED),
    NO_PRODUCTDETAILS_FOUND_ESPR("FS_ESPR_003","No Product Details Found","No Product Details Found",HttpStatus.NOT_FOUND),
    INTERNAL_SERVER_ERROR_ESPR("FS_ESPR_004", "Technical Error. Please try again later", "Technical Error. Please try again later", HttpStatus.INTERNAL_SERVER_ERROR);

	
	private String errorCode;

	private String errorText;

	private String errorField;

	private HttpStatus status;

	private ErrorCodeEnum(String errorCode, String errorText, String errorField, HttpStatus status) {
		this.errorCode = errorCode;
		this.errorText = errorText;
		this.errorField = errorField;
		this.status = status;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public String getErrorText() {
		return errorText;
	}

	public String getErrorField() {
		return errorField;
	}

	public HttpStatus getStatus() {
		return status;
	}

}