package com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.domain.StandingOrdersGETResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.delegate.AccountStandingOrderFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.domain.StandingOrder;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.domain.StandingOrders;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.transformer.AccountStandingOrderFoundationServiceTransformer;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;
import com.capgemini.psd2.validator.PSD2Validator;



@RunWith(SpringJUnit4ClassRunner.class)
public class AccountStandingOrdersFoundationServiceDelegateRestCallTest {
	
	/** The account information FS transformer. */
	@Mock
	private AccountStandingOrderFoundationServiceTransformer accountInformationFSTransformer = new AccountStandingOrderFoundationServiceTransformer();
	
	@InjectMocks
	private AccountStandingOrderFoundationServiceDelegate delegate;
	
	@Mock
	private PSD2Validator psd2Validator;
	
	/** The rest client. */
	@Mock
	private RestClientSyncImpl restClient;
	
	/**
	 * Sets the up.
	 */
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	
	/**
	 * Test transform response from FD to API.
	 */
	@Test
	public void testTransformResponseFromFDToAPI() {
		
		Map<String, String> params = new HashMap<>();
		params.put("accountId", "12345");
		StandingOrders standingOrders = new StandingOrders();
		StandingOrder standingOrder = new StandingOrder();
		standingOrder.setPayeeAccountNumber("12345");
		standingOrder.setPayeeSortCode("123456");
		standingOrder.setCurrency("GBP");
		StandingOrdersGETResponse standingOrdersGETResponse = new StandingOrdersGETResponse();
		standingOrders.getStandingOrders().add(standingOrder);
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		accountMapping.setCorrelationId("test");
		Mockito.when(delegate.transformResponseFromFDToAPI(any(), any())).thenReturn(new StandingOrdersGETResponse());
		StandingOrdersGETResponse res = accountInformationFSTransformer.transformAccountStandingOrders(standingOrders, params);
		assertThat(standingOrdersGETResponse).isEqualTo(res);
	}
	
	@Test
	public void testGetFoundationServiceURL(){
		
		StandingOrder standingOrder = new StandingOrder();
		standingOrder.setPayeeAccountNumber("12345");
		standingOrder.setPayeeSortCode("123456");
		String finalURL = "http://localhost:9086/fs-abt-service/services/account/123456/12345/standingorders";
		String response = delegate.getFoundationServiceURL("http://localhost:9086/fs-abt-service/services/account", "123456", "12345", "standingorders");
		assertThat(response).isEqualTo(finalURL);
	}
	
		
	@Test(expected = AdapterException.class)
	public void testGetFoundationServiceURLNSCNull(){
	delegate.getFoundationServiceURL("http://localhost:9086/fs-abt-service/services/account", null, "12345", "standingorders")	;
	}
	@Test(expected = AdapterException.class)
	public void testGetFoundationServiceURLAccountNumberNull(){
	delegate.getFoundationServiceURL("http://localhost:9086/fs-abt-service/services/account", "123456", null, "standingorders")	;
	}
	
	@Test
	public void testCreateRequestHeaders(){
		ReflectionTestUtils.setField(delegate, "userInReqHeader", "X-BOI-USER");
		ReflectionTestUtils.setField(delegate, "channelInReqHeader", "X-BOI-CHANNEL");
		ReflectionTestUtils.setField(delegate, "platformInReqHeader", "X-BOI-PLATFORM");
		ReflectionTestUtils.setField(delegate, "correlationReqHeader", "X-CORRELATION-ID");
		ReflectionTestUtils.setField(delegate, "platform", "PSD2API");
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("12345678");
		accountMapping.setCorrelationId("123");
		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.CHANNEL_NAME, "Channel");
		params.put(PSD2Constants.PLATFORM_IN_REQ_HEADER, "PSD2API");
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("X-BOI-USER", "12345678");
		httpHeaders.add("X-BOI-CHANNEL", "Channel");
		httpHeaders.add("X-BOI-PLATFORM", "PSD2API");
		httpHeaders.add("X-CORRELATION-ID", "123");
		HttpHeaders response = delegate.createRequestHeaders(new RequestInfo(), accountMapping, params);
		assertThat(response).isEqualTo(httpHeaders);
	}
}
