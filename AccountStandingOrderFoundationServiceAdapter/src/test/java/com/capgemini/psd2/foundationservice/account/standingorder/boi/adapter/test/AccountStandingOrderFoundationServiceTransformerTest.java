package com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.test;
/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.domain.StandingOrdersGETResponse;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.domain.StandingOrder;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.domain.StandingOrderFrequency;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.domain.StandingOrderStatus;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.domain.StandingOrders;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.transformer.AccountStandingOrderFoundationServiceTransformer;
import com.capgemini.psd2.validator.PSD2Validator;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountStandingOrderFoundationServiceTransformerTest {
	@InjectMocks
	private AccountStandingOrderFoundationServiceTransformer accountStandingOrderFoundationServiceTransformer;
	@Mock
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;
	@Mock
	@Qualifier("PSD2ResponseValidator")
	PSD2Validator validator;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);

	}

	@Test
	public void contextLoads() {

	}

	@Test
	public void testTransformAccountStandingOrders() throws ParseException {
		StandingOrders standingOrders = new StandingOrders();
		StandingOrder standingOrder = new StandingOrder();
		standingOrder.setPayeeAccountNumber("123456");
		standingOrder.setPayeeSortCode("654321");
		standingOrder.setCurrency("GBP");
		standingOrder.setStandingOrderId(123);
		standingOrder.setPayeeReference("12");
		standingOrder.setPayerReference("123");
		standingOrder.setFrequency(StandingOrderFrequency.DAILY);

		// standingOrder.setDueDate(new Date(1220227200L *1000));
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String dateString = "2014-02-11";
		Date dateObject = sdf.parse(dateString);
		standingOrder.setDueDate(dateObject);
		standingOrder.setDueAmount(new BigDecimal(1.50));
		standingOrder.setStatus(StandingOrderStatus.ACTIVE);
		standingOrder.setFrequency(StandingOrderFrequency.WEEKLY);
		standingOrders.getStandingOrders().add(standingOrder);
		Map<String, String> params = new HashMap<String, String>();
		params.put("accountId", "12345678");
		Mockito.doNothing().when(validator).validate(anyObject());

		StandingOrdersGETResponse response = accountStandingOrderFoundationServiceTransformer
				.transformAccountStandingOrders(standingOrders, params);
		assertNotNull(response);
		// Passing standingOrderId null.
		standingOrder.setStandingOrderId(null);
		response = accountStandingOrderFoundationServiceTransformer.transformAccountStandingOrders(standingOrders,
				params);
		assertNotNull(response);
		// Frequency options
		standingOrder.setFrequency(StandingOrderFrequency.DAILY);
		standingOrder.setFrequencySpan(7);
		response = accountStandingOrderFoundationServiceTransformer.transformAccountStandingOrders(standingOrders,
				params);
		assertNotNull(response);
		standingOrder.setFrequency(StandingOrderFrequency.DAILY);
		standingOrder.setFrequencySpan(14);
		response = accountStandingOrderFoundationServiceTransformer.transformAccountStandingOrders(standingOrders,
				params);
		assertNotNull(response);
		standingOrder.setFrequency(StandingOrderFrequency.WEEKLY);
		response = accountStandingOrderFoundationServiceTransformer.transformAccountStandingOrders(standingOrders,
				params);
		assertNotNull(response);
		standingOrder.setFrequency(StandingOrderFrequency.FORTNIGHTLY);
		response = accountStandingOrderFoundationServiceTransformer.transformAccountStandingOrders(standingOrders,
				params);
		assertNotNull(response);
		standingOrder.setFrequency(StandingOrderFrequency.MONTHLY);
		standingOrder.setFrequencySpan(1);
		response = accountStandingOrderFoundationServiceTransformer.transformAccountStandingOrders(standingOrders,
				params);
		assertNotNull(response);
		dateString = "2014-02-09";
		dateObject = sdf.parse(dateString);
		standingOrder.setDueDate(dateObject);
		response = accountStandingOrderFoundationServiceTransformer.transformAccountStandingOrders(standingOrders,
				params);
		assertNotNull(response);
		dateString = "2014-02-11";
		dateObject = sdf.parse(dateString);
		standingOrder.setDueDate(dateObject);
		standingOrder.setFrequency(StandingOrderFrequency.MONTHLY);
		standingOrder.setFrequencySpan(12);
		response = accountStandingOrderFoundationServiceTransformer.transformAccountStandingOrders(standingOrders,
				params);
		assertNotNull(response);
		dateString = "2014-02-09";
		dateObject = sdf.parse(dateString);
		standingOrder.setDueDate(dateObject);
		response = accountStandingOrderFoundationServiceTransformer.transformAccountStandingOrders(standingOrders,
				params);
		assertNotNull(response);
		dateString = "2014-02-11";
		dateObject = sdf.parse(dateString);
		standingOrder.setDueDate(dateObject);
		standingOrder.setFrequency(StandingOrderFrequency.YEARLY);
		response = accountStandingOrderFoundationServiceTransformer.transformAccountStandingOrders(standingOrders,
				params);
		assertNotNull(response);
		dateString = "2014-02-09";
		dateObject = sdf.parse(dateString);
		standingOrder.setStartDate(dateObject);
		standingOrder.setDueDate(null);
		response = accountStandingOrderFoundationServiceTransformer.transformAccountStandingOrders(standingOrders,
				params);
		assertNotNull(response);

		// null frequency
		standingOrder.setFrequency(null);
		response = accountStandingOrderFoundationServiceTransformer.transformAccountStandingOrders(standingOrders,
				params);
		assertNotNull(response);
		// Due date as null
		standingOrder.setDueDate(null);
		response = accountStandingOrderFoundationServiceTransformer.transformAccountStandingOrders(standingOrders,
				params);
		assertNotNull(response);
		// DueAmoint null
		standingOrder.setDueAmount(null);
		response = accountStandingOrderFoundationServiceTransformer.transformAccountStandingOrders(standingOrders,
				params);
		assertNotNull(response);
		// PayeeSortCode null
		standingOrder.setPayeeSortCode(null);
		response = accountStandingOrderFoundationServiceTransformer.transformAccountStandingOrders(standingOrders,
				params);
		assertNotNull(response);

		// StandingOrderStatus Active
		standingOrder.setStatus(StandingOrderStatus.ACTIVE);
		response = accountStandingOrderFoundationServiceTransformer.transformAccountStandingOrders(standingOrders,
				params);

		// StandingOrderStatus On_hold
		standingOrder.setStatus(StandingOrderStatus.ON_HOLD);
		response = accountStandingOrderFoundationServiceTransformer.transformAccountStandingOrders(standingOrders,
				params);

		// StandingOrderStatus Inactive
		standingOrder.setStatus(StandingOrderStatus.INACTIVE);
		response = accountStandingOrderFoundationServiceTransformer.transformAccountStandingOrders(standingOrders,
				params);

		// StandingOrderStatus Code_Issued
		standingOrder.setStatus(StandingOrderStatus.CODE_ISSUED);
		response = accountStandingOrderFoundationServiceTransformer.transformAccountStandingOrders(standingOrders,
				params);

		// No StandingOrders found scenario
		StandingOrders emptyStandingOrders = new StandingOrders();
		response = accountStandingOrderFoundationServiceTransformer.transformAccountStandingOrders(emptyStandingOrders,
				params);
		assertNotNull(emptyStandingOrders);

		// Frequency options
		standingOrder.setFrequency(StandingOrderFrequency.WEEKLY);
		response = accountStandingOrderFoundationServiceTransformer.transformAccountStandingOrders(standingOrders,
				params);
		assertNotNull(response);
		standingOrder.setFrequency(StandingOrderFrequency.FORTNIGHTLY);
		response = accountStandingOrderFoundationServiceTransformer.transformAccountStandingOrders(standingOrders,
				params);
		assertNotNull(response);
		standingOrder.setFrequency(StandingOrderFrequency.MONTHLY);
		response = accountStandingOrderFoundationServiceTransformer.transformAccountStandingOrders(standingOrders,
				params);
		assertNotNull(response);
		standingOrder.setFrequency(StandingOrderFrequency.YEARLY);
		response = accountStandingOrderFoundationServiceTransformer.transformAccountStandingOrders(standingOrders,
				params);
		assertNotNull(response);

	}

	@Test(expected = AdapterException.class)
	public void testTransformAccountStandingOrdersNull() throws ParseException {
		StandingOrders standingOrders = new StandingOrders();
		StandingOrder standingOrder = new StandingOrder();
		standingOrder.setPayeeAccountNumber("123456");
		standingOrder.setPayeeSortCode("654321");
		standingOrder.setCurrency("GBP");
		standingOrder.setStandingOrderId(123);
		standingOrder.setPayeeReference("12");
		standingOrder.setPayerReference("123");
		standingOrder.setFrequency(StandingOrderFrequency.DAILY);

		// standingOrder.setDueDate(new Date(1220227200L *1000));
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String dateString = "2014-02-11";
		Date dateObject = sdf.parse(dateString);
		standingOrder.setDueDate(dateObject);
		standingOrder.setDueAmount(new BigDecimal(1.50));
		standingOrder.setStatus(StandingOrderStatus.ACTIVE);
		standingOrder.setFrequency(StandingOrderFrequency.WEEKLY);
		standingOrders.getStandingOrders().add(standingOrder);
		Map<String, String> params = new HashMap<String, String>();
		params.put("accountId", "12345678");
		Mockito.doNothing().when(validator).validate(anyObject());
		standingOrder.setFrequency(StandingOrderFrequency.DAILY);
		standingOrder.setFrequencySpan(12);
		StandingOrdersGETResponse response = accountStandingOrderFoundationServiceTransformer
				.transformAccountStandingOrders(standingOrders, params);
	}

	@Test(expected = AdapterException.class)
	public void testTransformAccountStandingOrdersMonthlyNull() throws ParseException {
		StandingOrders standingOrders = new StandingOrders();
		StandingOrder standingOrder = new StandingOrder();
		standingOrder.setPayeeAccountNumber("123456");
		standingOrder.setPayeeSortCode("654321");
		standingOrder.setCurrency("GBP");
		standingOrder.setStandingOrderId(123);
		standingOrder.setPayeeReference("12");
		standingOrder.setPayerReference("123");
		standingOrder.setFrequency(StandingOrderFrequency.DAILY);

		// standingOrder.setDueDate(new Date(1220227200L *1000));
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String dateString = "2014-02-11";
		Date dateObject = sdf.parse(dateString);
		standingOrder.setDueDate(dateObject);
		standingOrder.setDueAmount(new BigDecimal(1.50));
		standingOrder.setStatus(StandingOrderStatus.ACTIVE);
		standingOrder.setFrequency(StandingOrderFrequency.WEEKLY);
		standingOrders.getStandingOrders().add(standingOrder);
		Map<String, String> params = new HashMap<String, String>();
		params.put("accountId", "12345678");
		Mockito.doNothing().when(validator).validate(anyObject());
		standingOrder.setFrequency(StandingOrderFrequency.MONTHLY);
		standingOrder.setFrequencySpan(10);
		StandingOrdersGETResponse response = accountStandingOrderFoundationServiceTransformer
				.transformAccountStandingOrders(standingOrders, params);
	}

}
