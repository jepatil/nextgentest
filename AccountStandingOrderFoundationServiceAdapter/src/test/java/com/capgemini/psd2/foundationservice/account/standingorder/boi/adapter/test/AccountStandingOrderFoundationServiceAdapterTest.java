package com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.domain.StandingOrdersGETResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.AccountStandingOrderFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.client.AccountStandingOrderFoundationServiceClient;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.delegate.AccountStandingOrderFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.domain.StandingOrder;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.domain.StandingOrders;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.CustomerAccountsFilterFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion.AdapterFilterUtility;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;



@RunWith(SpringJUnit4ClassRunner.class)
public class AccountStandingOrderFoundationServiceAdapterTest {

	@InjectMocks
	private AccountStandingOrderFoundationServiceAdapter accountStandingOrderFoundationServiceAdapter;

	@Mock
	private AccountStandingOrderFoundationServiceDelegate accountStandingOrderFoundationServiceDelegate;
	
	@Mock
	private AccountStandingOrderFoundationServiceClient accountStandingOrderFoundationServiceClient;
	
	@Mock
	private RestClientSyncImpl restClient;
	
	@Mock
	AdapterFilterUtility adapterFilterUtility;
	
	@Mock
	CustomerAccountsFilterFoundationServiceAdapter commonFilterUtility;
	
	
	
	private Map<String, String> params = new HashMap<String, String>();
	private AccountMapping accountMapping = new AccountMapping();
	private List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
	private AccountDetails accDet = new AccountDetails();
	private HttpHeaders httpHeaders = new HttpHeaders();
	private StandingOrders standingOrders = new StandingOrders();
	private StandingOrdersGETResponse standingOrderGETResponse = new StandingOrdersGETResponse();
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
		StandingOrders standingOrders = new StandingOrders();
		StandingOrder standingOrder = new StandingOrder();
		standingOrder.setPayeeAccountNumber("123456");
		standingOrder.setPayeeSortCode("654321");
		
		standingOrders.getStandingOrders().add(standingOrder);
		
		AccountDetails accDet1 = new AccountDetails();
		accDet.setAccountId("56789");
		accDet.setAccountNSC("654321");
		accDet.setAccountNumber("123456");
		accDetList.add(accDet1);

		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("BOI999");
		accountMapping.setCorrelationId("23456778");
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts filteredAccounts = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt accntFilter = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt();
		accntFilter.setAccountNumber("US2345");
		accntFilter.setAccountNSC("1234");
		filteredAccounts.getAccount().add(accntFilter);
		
		Map<String, String> params = new HashMap<>();
		params.put("channelId", "BOL");
	    params.put("consentFlowType", "AISP");
	    params.put("x-channel-id", "BOL");
	    params.put("x-user-id", "BOI999");
	    params.put("X-BOI-PLATFORM", "platform");
	    params.put("x-fapi-interaction-id", "12345678"); 
	    Mockito.when(accountStandingOrderFoundationServiceDelegate.getFoundationServiceURL(any(),any(), any(), any())).thenReturn("http://localhost:9086/fs-abt-service/services/account/903779/25369621/standingorders");
	    Mockito.when(accountStandingOrderFoundationServiceDelegate.createRequestHeaders(anyObject(), anyObject(), anyObject())).thenReturn(httpHeaders);
	    Mockito.when(commonFilterUtility.retrieveCustomerAccountList(any(),any())).thenReturn(filteredAccounts);
		Mockito.when(adapterFilterUtility.prevalidateAccounts(any(), any())).thenReturn(accountMapping);
		
		
	}
	
	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	
	
	@Test
	public void testAccountStandingOrderFS(){
	
		Mockito.when(accountStandingOrderFoundationServiceClient.restTransportForAccountStandingOrder(any(), any(), any())).thenReturn(standingOrders);
		Mockito.when(accountStandingOrderFoundationServiceDelegate.transformResponseFromFDToAPI(any(),any())).thenReturn(standingOrderGETResponse);
 		StandingOrdersGETResponse response = accountStandingOrderFoundationServiceAdapter.retrieveAccountStandingOrders(accountMapping, params);
		assertEquals(standingOrderGETResponse, response);
	}
	
	@Test(expected = Exception.class)
	public void testRestTransportForAccountStandingOrder(){
		Mockito.when(accountStandingOrderFoundationServiceClient.restTransportForAccountStandingOrder(any(), any(), any())).thenThrow(PSD2Exception.populatePSD2Exception("",ErrorCodeEnum.TECHNICAL_ERROR));
		Mockito.when(accountStandingOrderFoundationServiceDelegate.transformResponseFromFDToAPI(any(),any())).thenReturn(standingOrderGETResponse);
 		accountStandingOrderFoundationServiceAdapter.retrieveAccountStandingOrders(accountMapping, params);
		//assertEquals(standingOrderGETResponse, response);
	}
	
	
	@Test
	public void testRestTransportForAccountStandingOrders(){
		ErrorInfo errorInfo = new ErrorInfo();
		StandingOrders standingOrders = null;
		errorInfo.setErrorCode("570");
		errorInfo.setErrorMessage("This request cannot be processed. No StandingOrder found");
		Mockito.when(accountStandingOrderFoundationServiceClient.restTransportForAccountStandingOrder(any(), any(), any())).thenThrow((new AdapterException("Adapter Exception", errorInfo )));
		StandingOrdersGETResponse getresponse  = accountStandingOrderFoundationServiceAdapter.retrieveAccountStandingOrders(accountMapping, params);
		assertEquals(standingOrders, getresponse);
	}
	
	@Test
	public void noStandingOrderTest(){
		StandingOrders So = null;
		Mockito.when(accountStandingOrderFoundationServiceClient.restTransportForAccountStandingOrder(anyObject(), anyObject(), anyObject())).thenReturn(null);
		StandingOrdersGETResponse getresponse =accountStandingOrderFoundationServiceAdapter.retrieveAccountStandingOrders(accountMapping, params);
		assertEquals(So, getresponse);
	}
	@Test(expected = AdapterException.class)
	public void accountMappingNullTest(){
		accountStandingOrderFoundationServiceAdapter.retrieveAccountStandingOrders(null, params);
	}
	@Test(expected = AdapterException.class)
	public void paramsNullTest(){
		accountStandingOrderFoundationServiceAdapter.retrieveAccountStandingOrders(accountMapping, null);
	}
	@Test(expected = AdapterException.class)
	public void accountMappingEmptyTest(){
		AccountMapping accountMapping = new AccountMapping();
		accountStandingOrderFoundationServiceAdapter.retrieveAccountStandingOrders(accountMapping, params);
	}
	@Test(expected = AdapterException.class)
	public void psuIdNullTest(){
		accountMapping.setPsuId(null);
		accountStandingOrderFoundationServiceAdapter.retrieveAccountStandingOrders(accountMapping, params);
	}
	@Test(expected = AdapterException.class)
	public void accountDetailsEmptyTest(){
		accountMapping.setAccountDetails(new ArrayList<AccountDetails>());
		accountMapping.setPsuId("12345678");
		accountStandingOrderFoundationServiceAdapter.retrieveAccountStandingOrders(accountMapping, params);
	}
	@Test(expected = AdapterException.class)
	public void accountDetailsNullTest(){
		accountMapping.setAccountDetails(null);
		accountMapping.setPsuId("12345678");
		accountStandingOrderFoundationServiceAdapter.retrieveAccountStandingOrders(accountMapping, params);
	}
	
	/**
	 * Test account accnt details as null.
	 */
	@Test(expected = AdapterException.class)
	public void testAccountDetailsAsNull() {
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("test");
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts filteredAccounts = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt accntFilter = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt();
		accntFilter.setAccountNumber("US2345");
		accntFilter.setAccountNSC("1234");
		filteredAccounts.getAccount().add(accntFilter);
		
		Mockito.when(commonFilterUtility.retrieveCustomerAccountList(any(),any())).thenReturn(filteredAccounts);
		Mockito.when(adapterFilterUtility.prevalidateAccounts(any(), any())).thenReturn(accountMapping);
		accountStandingOrderFoundationServiceAdapter.retrieveAccountStandingOrders(accountMapping, params);

	
	}
	
	/**
	 * Test account Product accnt mapping psu ID as null.
	 */
	@Test(expected = AdapterException.class)
	public void testAccountStandingOrdersAccntMappingPsuIdAsNull() {
		AccountMapping accountMapping = new AccountMapping();		
		Mockito.when(accountStandingOrderFoundationServiceDelegate.createRequestHeaders(anyObject(),anyObject(),anyObject())).thenReturn(new HttpHeaders());
		accountStandingOrderFoundationServiceAdapter.retrieveAccountStandingOrders(accountMapping, params);
	}
}