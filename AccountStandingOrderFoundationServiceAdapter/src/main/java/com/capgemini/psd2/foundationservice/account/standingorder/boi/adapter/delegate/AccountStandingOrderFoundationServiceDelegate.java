/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.delegate;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;

import com.capgemini.psd2.aisp.domain.StandingOrdersGETResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.domain.StandingOrders;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.transformer.AccountStandingOrderFoundationServiceTransformer;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;

/**
 * The Class AccountStandingOrderFoundationServiceDelegate.
 */
@EnableConfigurationProperties
@ConfigurationProperties("foundationService")
@Component
public class AccountStandingOrderFoundationServiceDelegate {

	/** The adapterUtility. */

	/** The account standingOrder FS transformer. */
	@Autowired
	AccountStandingOrderFoundationServiceTransformer accountStandingOrderFSTransformer;

	@Value("${foundationService.userInReqHeader:#{X-BOI-USER}}")
	private String userInReqHeader;

	@Value("${foundationService.channelInReqHeader:#{X-BOI-CHANNEL}}")
	private String channelInReqHeader;

	@Value("${foundationService.platformInReqHeader:#{X-BOI-PLATFORM}}")
	private String platformInReqHeader;

	@Value("${foundationService.correlationReqHeader:#{X-CORRELATION-ID}}")
	private String correlationReqHeader;

	/** The platform. */
	@Value("${foundationService.platform:#{PSD2API}}")
	private String platform; 

	/**
	 * Gets the foundation service URL.
	 *
	 * @param channelId
	 *            the channel id
	 * @param accountNSC
	 *            the account NSC
	 * @param accountNumber
	 *            the account number
	 * @return the foundation service URL
	 */

	public String getFoundationServiceURL(String baseURL, String payeeSortCode, String payeeAccountNumber,
			String endURL) {

		if (NullCheckUtils.isNullOrEmpty(payeeSortCode)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_NSC_IN_REQUEST);
		}
		if (NullCheckUtils.isNullOrEmpty(payeeAccountNumber)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}

		return baseURL + "/" + payeeSortCode + "/" + payeeAccountNumber + "/" + endURL;
	}

	/**
	 * Transform response from FD to API.
	 *
	 * @param accnt
	 *            the accnt
	 * @param params
	 *            the params
	 * @return the account GET response
	 */
	public StandingOrdersGETResponse transformResponseFromFDToAPI(StandingOrders standingOrders,
			Map<String, String> params) {
		return accountStandingOrderFSTransformer.transformAccountStandingOrders(standingOrders, params);
	}

	/**
	 * Creates the request headers.
	 *
	 * @param requestInfo
	 *            the request info
	 * @param accountMapping
	 *            the account mapping
	 * @return the http headers
	 */
	public HttpHeaders createRequestHeaders(RequestInfo requestInfo, AccountMapping accountMapping,
			Map<String, String> params) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(userInReqHeader, accountMapping.getPsuId());
		httpHeaders.add(channelInReqHeader, params.get(PSD2Constants.CHANNEL_NAME));
		String platformId = params.get(PSD2Constants.PLATFORM_IN_REQ_HEADER);
		httpHeaders.add(platformInReqHeader, NullCheckUtils.isNullOrEmpty(platformId) ? platform : platformId);
		httpHeaders.add(correlationReqHeader, accountMapping.getCorrelationId());
		return httpHeaders;
	}

}
