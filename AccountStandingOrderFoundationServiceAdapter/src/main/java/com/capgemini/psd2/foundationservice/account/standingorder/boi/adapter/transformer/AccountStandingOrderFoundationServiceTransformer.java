/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.transformer;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.adapter.enumerator.CurrencyEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.domain.Data4CreditorAccount;
import com.capgemini.psd2.aisp.domain.Data7;
import com.capgemini.psd2.aisp.domain.Data7NextPaymentAmount;
import com.capgemini.psd2.aisp.domain.StandingOrder;
import com.capgemini.psd2.aisp.domain.StandingOrdersGETResponse;
import com.capgemini.psd2.aisp.transformer.AccountStandingOrdersTransformer;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.constants.AccountStandingOrderFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.domain.StandingOrderFrequency;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.domain.StandingOrderStatus;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;

/**
 * The Class AccountStandingOrderFoundationServiceTransformer.
 */
@Component
@ConfigurationProperties(prefix = "foundationService")
@Configuration
@EnableAutoConfiguration
public class AccountStandingOrderFoundationServiceTransformer implements AccountStandingOrdersTransformer {


	@Autowired
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;

	@Autowired
	@Qualifier("PSD2ResponseValidator")
	private PSD2Validator validator;

	@Override
	public <T> StandingOrdersGETResponse transformAccountStandingOrders(T source, Map<String, String> params) {

		Data7 data7 = new Data7();

		StandingOrdersGETResponse standingOrdersGETResponse = new StandingOrdersGETResponse();
		com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.domain.StandingOrders standingOrders = (com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.domain.StandingOrders) source;


		for (com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.domain.StandingOrder standingOrder : standingOrders
				.getStandingOrders()) {
			if (!NullCheckUtils.isNullOrEmpty(standingOrder.getStatus())) {
				if (StandingOrderStatus.ACTIVE.toString().equals(standingOrder.getStatus().value())
						|| StandingOrderStatus.ON_HOLD.toString().equals(standingOrder.getStatus().value())) {

					StandingOrder responseStandingOrder = new StandingOrder();
					responseStandingOrder
							.setAccountId(params.get(AccountStandingOrderFoundationServiceConstants.ACCOUNT_ID));
					if (!NullCheckUtils.isNullOrEmpty(standingOrder.getStandingOrderId())) {
						responseStandingOrder.setStandingOrderId(standingOrder.getStandingOrderId().toString());
					}
					if (!NullCheckUtils.isNullOrEmpty(standingOrder.getFrequency())) {
						// Mapping Foundation Service Frequency
						String freq = null;
						if(!NullCheckUtils.isNullOrEmpty(standingOrder.getDueDate())){
							 freq = getFrequency(standingOrder.getFrequency(),
									standingOrder.getDueDate(),standingOrder.getFrequencySpan());
						}
						else {
							 freq = getFrequency(standingOrder.getFrequency(),
									standingOrder.getStartDate(),standingOrder.getFrequencySpan());
						}
						responseStandingOrder.setFrequency(freq);
					}
					responseStandingOrder.setReference(standingOrder.getPayerReference());

					// NextPaymentDateTime
					if (!NullCheckUtils.isNullOrEmpty(standingOrder.getDueDate())) {
						responseStandingOrder.setNextPaymentDateTime(
								timeZoneDateTimeAdapter.parseDateTimeCMA(standingOrder.getDueDate()));
					}

					// NextpaymentAmount
					Data7NextPaymentAmount data7NextPaymentAmount = new Data7NextPaymentAmount();
					if (standingOrder.getDueAmount() != null) {
						data7NextPaymentAmount.setAmount(standingOrder.getDueAmount().toString());
						validator.validateEnum(CurrencyEnum.class, standingOrder.getCurrency());
						data7NextPaymentAmount.setCurrency(standingOrder.getCurrency());
						validator.validate(data7NextPaymentAmount);
					}
					responseStandingOrder.setNextPaymentAmount(data7NextPaymentAmount);

					// Creditor Account

					if ((!NullCheckUtils.isNullOrEmpty(standingOrder.getPayeeSortCode()))
							&& (!NullCheckUtils.isNullOrEmpty(standingOrder.getPayeeAccountNumber()))) {
						Data4CreditorAccount data4CreditorAccount = new Data4CreditorAccount();
						data4CreditorAccount.setSchemeName(
								com.capgemini.psd2.aisp.domain.Data4CreditorAccount.SchemeNameEnum.SORTCODEACCOUNTNUMBER);
						data4CreditorAccount.setIdentification(
								standingOrder.getPayeeSortCode().trim() + standingOrder.getPayeeAccountNumber().trim());
						responseStandingOrder.setCreditorAccount(data4CreditorAccount);
						validator.validate(data4CreditorAccount);
						responseStandingOrder.setCreditorAccount(data4CreditorAccount);
					}

					data7.addStandingOrderItem(responseStandingOrder);
					validator.validate(responseStandingOrder);

					validator.validate(data7);
					standingOrdersGETResponse.setData(data7);

				}

			}

		}
		if (standingOrders.getStandingOrders().isEmpty() || null == standingOrders.getStandingOrders()
				|| null == data7.getStandingOrder()) {
			data7.setStandingOrder(new ArrayList<>());
			standingOrdersGETResponse.data(data7);
			return standingOrdersGETResponse;
		}

		return standingOrdersGETResponse;
	}

	public String getFrequency(StandingOrderFrequency frequencyFS, Date date, int frequencySpan ) {
		String frequency = "";
		int dayofweek=0;
		int dayOfMonth=0;
		Calendar calendar=Calendar.getInstance();
		calendar.setTime(date);
		if(frequencyFS.value().equalsIgnoreCase("Monthly")||frequencyFS.value().equalsIgnoreCase("Yearly"))
		{
		 dayOfMonth=calendar.get(Calendar.DAY_OF_MONTH);
		}
		else if(frequencyFS.value().equalsIgnoreCase("Daily")||frequencyFS.value().equalsIgnoreCase("Weekly")||frequencyFS.value().equalsIgnoreCase("Fortnightly"))
		{
		Instant instant = date.toInstant();
		ZoneId defaultZoneId = ZoneId.systemDefault();
		LocalDate localDate = instant.atZone(defaultZoneId).toLocalDate();
		dayofweek=localDate.getDayOfWeek().getValue();
		}
		switch (frequencyFS) {
		case DAILY :
			if(frequencySpan==7)
				/*WEEKLY*/
				frequency = "IntrvlWkDay:01:0" + dayofweek;
			else if(frequencySpan==14)
				/*FORTNIGHTLY*/
				frequency = "IntrvlWkDay:02:0" + dayofweek;
			else
				throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.TECHNICAL_ERROR);
			break;
		case WEEKLY  :
			frequency = "IntrvlWkDay:01:0" + dayofweek;
			break;
		case FORTNIGHTLY:
			frequency = "IntrvlWkDay:02:0" + dayofweek;
			break;
		case MONTHLY:
			if (frequencySpan == 1) {
				/*Monthly*/
				if (dayOfMonth <= 9)
					frequency = "IntrvlMnthDay:01:0" + dayOfMonth;
				else
					frequency = "IntrvlMnthDay:01:" + dayOfMonth;
			}
		    else if(frequencySpan==12)
		    	{
		    	/*Yearly*/
		    	if (dayOfMonth <= 9)
					frequency = "IntrvlMnthDay:12:0" + dayOfMonth;
				else
					frequency = "IntrvlMnthDay:12:" + dayOfMonth;
		    	}
		    else
		    {
		    	throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.TECHNICAL_ERROR);
		    }
			break;
		case YEARLY:
			if (dayOfMonth <= 9)
				frequency = "IntrvlMnthDay:12:0" + dayOfMonth;
			else
				frequency = "IntrvlMnthDay:12:" + dayOfMonth;
			break;
		default:
			frequency = "EvryDay";
			break;
		}
		return frequency;
	}
}
