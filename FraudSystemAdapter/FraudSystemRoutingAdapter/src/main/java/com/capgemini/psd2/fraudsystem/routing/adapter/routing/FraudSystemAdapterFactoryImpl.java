package com.capgemini.psd2.fraudsystem.routing.adapter.routing;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.fraudsystem.adapter.FraudSystemAdapter;

@Component
public class FraudSystemAdapterFactoryImpl implements ApplicationContextAware, FraudSystemAdapterFactory {

	private ApplicationContext applicationContext;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}

	@Override
	public FraudSystemAdapter getAdapterInstance(String coreSystemName) {
		return (FraudSystemAdapter) applicationContext.getBean(coreSystemName);
	}
}