/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.information.routing.adapter.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.capgemini.psd2.account.information.routing.adapter.routing.AccountInformationAdapterFactory;
import com.capgemini.psd2.aisp.adapter.AccountInformationAdapter;
import com.capgemini.psd2.aisp.domain.AccountGETResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;

/**
 * The Class AccountInformationRoutingAdapter.
 */
public class AccountInformationRoutingAdapter implements AccountInformationAdapter {

	/** The account information adapter factory. */
	@Autowired
	private AccountInformationAdapterFactory accountInformationAdapterFactory;

	/** The default adapter. */
	@Value("${app.defaultAdapterAccountReq}")
	private String defaultAdapter;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.capgemini.psd2.aisp.adapter.AccountInformationAdapter#
	 * retrieveAccountInformation(com.capgemini.psd2.aisp.domain.AccountMapping,
	 * java.util.Map)
	 */
	@Override
	public AccountGETResponse retrieveAccountInformation(AccountMapping accountMapping, Map<String, String> params) {
		AccountInformationAdapter accountInformationAdapter = accountInformationAdapterFactory
				.getAdapterInstance(defaultAdapter);
		return accountInformationAdapter.retrieveAccountInformation(accountMapping, params);
	}

	@Override
	public AccountGETResponse retrieveMultipleAccountsInformation(AccountMapping accountMapping,
			Map<String, String> params) {
		AccountInformationAdapter accountInformationAdapter = accountInformationAdapterFactory
				.getAdapterInstance(defaultAdapter);
		return accountInformationAdapter.retrieveMultipleAccountsInformation(accountMapping, params);
	}
}