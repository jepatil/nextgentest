/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.information.routing.adapter.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.Account;
import com.capgemini.psd2.aisp.domain.AccountGETResponse;
import com.capgemini.psd2.aisp.domain.Data2;
import com.capgemini.psd2.aisp.domain.Data2Account;
import com.capgemini.psd2.aisp.domain.Data2Servicer;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;

/**
 * The Class AccountInformationRoutingAdapterTestMockData.
 */
public class AccountInformationRoutingAdapterTestMockData {
	
	/** The mock accounts GET response. */
	public static AccountGETResponse mockAccountsGETResponse;
	
	/** The mock accounts GET response accounts. */
	public static Account mockAccountsGETResponseAccounts;
	
	/** The mock account mapping. */
	public static AccountMapping mockAccountMapping;

	/**
	 * Gets the account GET response.
	 *
	 * @return the account GET response
	 */
	public static AccountGETResponse getAccountGETResponse() {
		mockAccountsGETResponse = new AccountGETResponse();
		List<Account> accountsGETResponseAccountsList = new ArrayList<>();
		accountsGETResponseAccountsList.add(getAccountGETResponseData());
		Data2 mockData= new Data2();
		mockData.setAccount(accountsGETResponseAccountsList);
		mockAccountsGETResponse.setData(mockData);
		return mockAccountsGETResponse;
	}
	

	/**
	 * Gets the account GET response data.
	 *
	 * @return the account GET response data
	 */
	public static Account getAccountGETResponseData() {
		mockAccountsGETResponseAccounts = new Account();
		Data2Account mockData2Account = new Data2Account();
		mockData2Account.setIdentification("10203345");
		mockData2Account
				.setSchemeName(Data2Account.SchemeNameEnum.IBAN);
		mockData2Account.setName("Lorem");
		mockAccountsGETResponseAccounts.setAccount(mockData2Account);

		Data2Servicer mockData2Servicer = new Data2Servicer();
		mockData2Servicer.setIdentification("SC802001");
		mockData2Servicer
				.setSchemeName(Data2Servicer.SchemeNameEnum.BICFI);
		mockAccountsGETResponseAccounts.setServicer(mockData2Servicer);
		return mockAccountsGETResponseAccounts;
	}

	/**
	 * Gets the account mapping.
	 *
	 * @return the account mapping
	 */
	public static AccountMapping getAccountMapping() {
		mockAccountMapping = new AccountMapping();
		mockAccountMapping.setTppCID("tpp123");
		mockAccountMapping.setPsuId("user123");
		mockAccountMapping.setCorrelationId("95212678-4d0c-450f-8268-25dcfc95bfa1");
		List<AccountDetails> selectedAccounts = new ArrayList<>();

		AccountDetails accountRequest = new AccountDetails();

		selectedAccounts.add(accountRequest);

		mockAccountMapping.setAccountDetails(selectedAccounts);
		return mockAccountMapping;
	}


	public static AccountGETResponse getMultipleAccountGETResponse() {
		mockAccountsGETResponse = new AccountGETResponse();
		List<Account> accountsGETResponseAccountsList = new ArrayList<>();
		accountsGETResponseAccountsList.add(getAccountGETResponseData());
		accountsGETResponseAccountsList.add(getAccountGETResponseData2());
		Data2 mockData= new Data2();
		mockData.setAccount(accountsGETResponseAccountsList);
		mockAccountsGETResponse.setData(mockData);
		return mockAccountsGETResponse;
	}


	private static Account getAccountGETResponseData2() {
		mockAccountsGETResponseAccounts = new Account();
		Data2Account mockData2Account = new Data2Account();
		mockData2Account.setIdentification("76528776");
		mockData2Account
				.setSchemeName(Data2Account.SchemeNameEnum.IBAN);
		mockData2Account.setName("Mr Kevin");
		mockAccountsGETResponseAccounts.setAccount(mockData2Account);

		Data2Servicer mockData2Servicer = new Data2Servicer();
		mockData2Servicer.setIdentification("SC802002");
		mockData2Servicer
				.setSchemeName(Data2Servicer.SchemeNameEnum.BICFI);
		mockAccountsGETResponseAccounts.setServicer(mockData2Servicer);
		return mockAccountsGETResponseAccounts;
	}
}
