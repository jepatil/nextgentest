/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.information.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.Account;
import com.capgemini.psd2.aisp.domain.AccountGETResponse;
import com.capgemini.psd2.aisp.domain.Data2;
import com.capgemini.psd2.aisp.domain.Data2Account;
import com.capgemini.psd2.aisp.domain.Data2Servicer;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.token.ConsentTokenData;
import com.capgemini.psd2.token.Token;

/**
 * The Class AccountInformationTestMockData.
 */
public class AccountInformationTestMockData {

	/** The mock account GET response. */
	public static AccountGETResponse mockAccountGETResponse;

	/** The mock account GET response data. */
	public static Account mockAccount;

	/** The mock account mapping. */
	public static AccountMapping mockAccountMapping;

	/** The mock consent. */
	public static AispConsent mockConsent;

	/** The mock token. */
	public static Token mockToken;

	/**
	 * Gets the token.
	 *
	 * @return the token
	 */
	public static Token getToken() {
		mockToken = new Token();
		ConsentTokenData consentTokenData = new ConsentTokenData();
		consentTokenData.setConsentExpiry("1509348259877L");
		consentTokenData.setConsentId("12345");
		mockToken.setConsentTokenData(consentTokenData);
		return mockToken;
	}

	/**
	 * Gets the account GET response.
	 *
	 * @return the account GET response
	 */
	public static AccountGETResponse getAccountGETResponse() {
		mockAccountGETResponse = new AccountGETResponse();
		List<Account> accountsGETResponseAccountsList = new ArrayList<>();
		accountsGETResponseAccountsList.add(getAccount1());
		Data2 mockData =  new Data2();
		mockData.setAccount(accountsGETResponseAccountsList);
		mockAccountGETResponse.setData(mockData);
		return mockAccountGETResponse;
	}

	/**
	 * Gets the consent mock data.
	 *
	 * @return the consent mock data
	 */
	public static AispConsent getConsentMockData() {
		mockConsent = new AispConsent();
		mockConsent.setTppCId("tpp123");
		mockConsent.setPsuId("user123");
		mockConsent.setConsentId("a1e590ad-73dc-453a-841e-2a2ef055e878");

		List<AccountDetails> selectedAccounts = new ArrayList<>();

		AccountDetails accountRequest = new AccountDetails();
		accountRequest.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		accountRequest.setAccountNSC("SC802001");
		accountRequest.setAccountNumber("10203345");
		selectedAccounts.add(accountRequest);
		mockConsent.setAccountDetails(selectedAccounts);

		return mockConsent;
	}

	/**
	 * Gets the account GET response data.
	 *
	 * @return the account GET response data
	 */
	public static Account getAccount1() {
		mockAccount = new Account();
		Data2Account mockData2Account = new Data2Account();
		mockData2Account.setIdentification("10203345");
		mockData2Account
				.setSchemeName(Data2Account.SchemeNameEnum.IBAN);
		mockData2Account.setName("Lorem");
		mockAccount.setAccount(mockData2Account);

		Data2Servicer mockData2Servicer = new Data2Servicer();
		mockData2Servicer.setIdentification("SC802001");
		mockData2Servicer
				.setSchemeName(Data2Servicer.SchemeNameEnum.BICFI);
		mockAccount.setServicer(mockData2Servicer);
		return mockAccount;
	}

	private static Account getAccount2() {
		mockAccount = new Account();
		Data2Account mockData2Account = new Data2Account();
		mockData2Account.setIdentification("76528776");
		mockData2Account
				.setSchemeName(Data2Account.SchemeNameEnum.IBAN);
		mockData2Account.setName("Mr Kevin");
		mockAccount.setAccount(mockData2Account);

		Data2Servicer mockData2Servicer = new Data2Servicer();
		mockData2Servicer.setIdentification("SC802001");
		mockData2Servicer
				.setSchemeName(Data2Servicer.SchemeNameEnum.BICFI);
		mockAccount.setServicer(mockData2Servicer);
		return mockAccount;
	}

	private static Account getAccount3() {
		mockAccount = new Account();
		Data2Account mockData2Account = new Data2Account();
		mockData2Account.setIdentification("25369621");
		mockData2Account
				.setSchemeName(Data2Account.SchemeNameEnum.IBAN);
		mockData2Account.setName("Mr Jack");
		mockAccount.setAccount(mockData2Account);

		Data2Servicer mockData2Servicer = new Data2Servicer();
		mockData2Servicer.setIdentification("SC802001");
		mockData2Servicer
				.setSchemeName(Data2Servicer.SchemeNameEnum.BICFI);
		mockAccount.setServicer(mockData2Servicer);
		return mockAccount;
	}

	private static Account getAccount4() {
		mockAccount = new Account();
		Data2Account mockData2Account = new Data2Account();
		mockData2Account.setIdentification("9999");
		mockData2Account
				.setSchemeName(Data2Account.SchemeNameEnum.IBAN);
		mockData2Account.setName("Mr Sam");
		mockAccount.setAccount(mockData2Account);

		Data2Servicer mockData2Servicer = new Data2Servicer();
		mockData2Servicer.setIdentification("SC802001");
		mockData2Servicer
				.setSchemeName(Data2Servicer.SchemeNameEnum.BICFI);
		mockAccount.setServicer(mockData2Servicer);
		return mockAccount;
	}

	private static Account getAccount5() {
		mockAccount = new Account();
		Data2Account mockData2Account = new Data2Account();
		mockData2Account.setIdentification("888");
		mockData2Account
				.setSchemeName(Data2Account.SchemeNameEnum.IBAN);
		mockData2Account.setName("Mr Ham");
		mockAccount.setAccount(mockData2Account);

		Data2Servicer mockData2Servicer = new Data2Servicer();
		mockData2Servicer.setIdentification("SC802001");
		mockData2Servicer
				.setSchemeName(Data2Servicer.SchemeNameEnum.BICFI);
		mockAccount.setServicer(mockData2Servicer);
		return mockAccount;
	}

	/**
	 * Gets the account mapping.
	 *
	 * @return the account mapping
	 */
	public static AccountMapping getAccountMapping() {
		mockAccountMapping = new AccountMapping();
		mockAccountMapping.setTppCID("tpp123");
		mockAccountMapping.setPsuId("user123");
		mockAccountMapping.setCorrelationId("95212678-4d0c-450f-8268-25dcfc95bfa1");
		List<AccountDetails> selectedAccounts = new ArrayList<>();

		AccountDetails accountRequest1 = new AccountDetails();
		accountRequest1.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		accountRequest1.setAccountNSC("SC802001");
		accountRequest1.setAccountNumber("10203345");

		AccountDetails accountRequest2 = new AccountDetails();
		accountRequest2.setAccountId("bf3e700e-25c9-475c-ab37-f2069af8b79a");
		accountRequest2.setAccountNSC("SC802001");
		accountRequest2.setAccountNumber("25369621");

		selectedAccounts.add(accountRequest1);
		selectedAccounts.add(accountRequest2);

		mockAccountMapping.setAccountDetails(selectedAccounts);
		return mockAccountMapping;
	}

	public static AccountGETResponse getMultipleAccountGETResponse() {
		mockAccountGETResponse = new AccountGETResponse();
		List<Account> accountsGETResponseAccountsList = new ArrayList<>();
		accountsGETResponseAccountsList.add(getAccount1());
		accountsGETResponseAccountsList.add(getAccount2());
		accountsGETResponseAccountsList.add(getAccount3());
		Data2 data2 = new Data2();
		data2.setAccount(accountsGETResponseAccountsList);
		mockAccountGETResponse.setData(data2);
		return mockAccountGETResponse;
	}

	public static AccountGETResponse getMultipleAccountGETResponseWithNoConsentAccounts() {
		mockAccountGETResponse = new AccountGETResponse();
		List<Account> accountsGETResponseAccountsList = new ArrayList<>();
		accountsGETResponseAccountsList.add(getAccount4());
		accountsGETResponseAccountsList.add(getAccount5());
		Data2 data2 = new Data2();
		data2.setAccount(accountsGETResponseAccountsList);
		mockAccountGETResponse.setData(data2);
		return mockAccountGETResponse;
	}

}
