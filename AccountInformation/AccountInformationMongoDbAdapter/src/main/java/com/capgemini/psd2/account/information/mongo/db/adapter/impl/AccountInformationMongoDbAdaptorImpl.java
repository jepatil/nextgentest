/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.information.mongo.db.adapter.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.account.information.mongo.db.adapter.repository.AccountInformationRepository;
import com.capgemini.psd2.aisp.adapter.AccountInformationAdapter;
import com.capgemini.psd2.aisp.domain.Account;
import com.capgemini.psd2.aisp.domain.AccountGETResponse;
import com.capgemini.psd2.aisp.domain.Data2;
import com.capgemini.psd2.aisp.mock.domain.MockAccount;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.validator.PSD2Validator;

/**
 * The Class AccountInformationMongoDbAdaptorImpl.
 */
@Component
public class AccountInformationMongoDbAdaptorImpl implements AccountInformationAdapter {

	/** The account info repo. */
	@Autowired
	private AccountInformationRepository accountInfoRepo;

	@Autowired
	@Qualifier("PSD2ResponseValidator")
	private PSD2Validator validator;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.capgemini.psd2.aisp.adapter.AccountInformationAdapter#
	 * retrieveAccountInformation(com.capgemini.psd2.aisp.domain.AccountMapping,
	 * java.util.Map)
	 */
	@Override
	public AccountGETResponse retrieveAccountInformation(AccountMapping accountMapping, Map<String, String> params) {

		String accountNumber = accountMapping.getAccountDetails().get(0).getAccountNumber();
		String accountNsc = accountMapping.getAccountDetails().get(0).getAccountNSC();
		String accountId = accountMapping.getAccountDetails().get(0).getAccountId();
		AccountGETResponse accountsGetResponse = new AccountGETResponse();
		List<MockAccount> mockAccountList = new ArrayList<>();
		try {
			mockAccountList = accountInfoRepo.findByAccountIdentificationAndServicerIdentification(accountNumber,
					accountNsc);

		} catch (DataAccessResourceFailureException e) {
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.CONNECTION_ERROR);
		}
		if (mockAccountList.isEmpty())
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_RECORD_FOUND_FOR_REQUESTED_ACCT);

		mockAccountList.get(0).setAccountId(accountId);
		validator.validate(mockAccountList.get(0));
		validator.validate(mockAccountList.get(0).getAccount());
		validator.validate(mockAccountList.get(0).getServicer());
		
		List<Account> accountList = new ArrayList<>();
		accountList.addAll(mockAccountList);
		Data2 data = new Data2();
		data.setAccount(accountList);
		accountsGetResponse.setData(data);
		validator.validate(accountsGetResponse);
		return accountsGetResponse;
	}

	@Override
	public AccountGETResponse retrieveMultipleAccountsInformation(AccountMapping accountMapping,
			Map<String, String> seviceParams) {
		List<MockAccount> multiAccountsInfoList = null;
		try {
			multiAccountsInfoList = accountInfoRepo.findByPsuId(accountMapping.getPsuId());
		} catch (DataAccessResourceFailureException e) {
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.CONNECTION_ERROR);
		}

		if (multiAccountsInfoList.isEmpty())
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_RECORD_FOUND_FOR_REQUESTED_ACCT);

		Map<String, String> accountNumberIdMap = new HashMap<>();
		accountMapping.getAccountDetails()
				.forEach(x -> accountNumberIdMap.put(x.getAccountNumber() + "|" + x.getAccountNSC(), x.getAccountId()));

		Iterator<MockAccount> itr = multiAccountsInfoList.iterator();
		List<Account> invalidAccounts=new ArrayList<>();
		List<Account> accountList = new ArrayList<>();
		while (itr.hasNext()) {
			MockAccount mockAccount = itr.next();
			if (accountNumberIdMap.get(mockAccount.getAccount().getIdentification() + "|"
					+ mockAccount.getServicer().getIdentification()) != null) {
				mockAccount.setAccountId(accountNumberIdMap.get(mockAccount.getAccount().getIdentification() + "|"
						+ mockAccount.getServicer().getIdentification()));
			} else {
				invalidAccounts.add(mockAccount);
				itr.remove();
			}
		}
		
		/*Invalid accounts is a list which wont be returned to TPP,but still we are
		  checking for any violations except accountId in that accounts*/
		invalidAccounts.forEach(invalidObj -> {
			// some value is set as accountId to check for violations in invalidAccounts 
			invalidObj.setAccountId("98765432");
			validator.validate(invalidObj);
			validator.validate(invalidObj.getAccount());
			validator.validate(invalidObj.getServicer());
		});
		
		/*checking for all possible violations which are being returned to TPP*/
		multiAccountsInfoList.forEach(obj -> {
			validator.validate(obj);
			validator.validate(obj.getAccount());
			validator.validate(obj.getServicer());
		});

		accountList.addAll(multiAccountsInfoList);
		AccountGETResponse accountsGetResponse = new AccountGETResponse();
		Data2 data = new Data2();
		data.setAccount(accountList);
		accountsGetResponse.setData(data);
		validator.validate(accountsGetResponse);
		return accountsGetResponse;
	}
}
