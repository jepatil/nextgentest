/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.information.mongo.db.adapter.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.Account;
import com.capgemini.psd2.aisp.domain.AccountGETResponse;
import com.capgemini.psd2.aisp.domain.Data2;
import com.capgemini.psd2.aisp.domain.Data2Account;
import com.capgemini.psd2.aisp.domain.Data2Account.SchemeNameEnum;
import com.capgemini.psd2.aisp.domain.Data2Servicer;
import com.capgemini.psd2.aisp.mock.domain.MockAccount;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;

/**
 * The Class AccountInformationAdapterTestMockData.
 */
public class AccountInformationAdapterTestMockData {

	/** The mock accounts GET response accounts. */
	public static Account mockAccountsGETResponseAccounts;

	/** The mock account mapping. */
	public static AccountMapping mockAccountMapping;

	/** The mock accounts GET response. */
	public static AccountGETResponse mockAccountsGETResponse;

	public static Account Account;
	public static MockAccount mockAccount; 

	/**
	 * Gets the account GET response.
	 *
	 * @return the account GET response
	 */
	public static AccountGETResponse getAccountGETResponse() {
		mockAccountsGETResponse = new AccountGETResponse();
		List<Account> accountsGETResponseAccountsList = new ArrayList<>();
		accountsGETResponseAccountsList.add(getAccountGETResponseData());
		Data2 mockData= new Data2();
		mockData.setAccount(accountsGETResponseAccountsList);
		mockAccountsGETResponse.setData(mockData);
		return mockAccountsGETResponse;
	}

	/**
	 * Gets the account GET response data.
	 *
	 * @return the account GET response data
	 */
	public static Account getAccountGETResponseData() {
		mockAccountsGETResponseAccounts = new MockAccount();
		Data2Account mockData2Account = new Data2Account();
		mockData2Account.setIdentification("47550843");
		mockData2Account
				.setSchemeName(Data2Account.SchemeNameEnum.IBAN);
		mockData2Account.setName("Lorem");
		mockAccountsGETResponseAccounts.setAccount(mockData2Account);

		Data2Servicer mockData2Servicer = new Data2Servicer();
		mockData2Servicer.setIdentification("901343");
		mockData2Servicer
				.setSchemeName(Data2Servicer.SchemeNameEnum.BICFI);
		mockAccountsGETResponseAccounts.setServicer(mockData2Servicer);
		return mockAccountsGETResponseAccounts;
	}

	/**
	 * Gets the account mapping.
	 *
	 * @return the account mapping
	 */
	public static AccountMapping getAccountMapping() {
		mockAccountMapping = new AccountMapping();
		mockAccountMapping.setTppCID("tpp123");
		mockAccountMapping.setPsuId("user123");
		mockAccountMapping.setCorrelationId("95212678-4d0c-450f-8268-25dcfc95bfa1");
		List<AccountDetails> selectedAccounts = new ArrayList<>();

		AccountDetails accountRequest1 = new AccountDetails();
		accountRequest1.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		accountRequest1.setAccountNSC("SC802001");
		accountRequest1.setAccountNumber("10203345");

		AccountDetails accountRequest2 = new AccountDetails();
		accountRequest2.setAccountId("bf3e700e-25c9-475c-ab37-f2069af8b79a");
		accountRequest2.setAccountNSC("SC802001");
		accountRequest2.setAccountNumber("25369621");

		selectedAccounts.add(accountRequest1);
		selectedAccounts.add(accountRequest2);

		mockAccountMapping.setAccountDetails(selectedAccounts);
		return mockAccountMapping;
	}

	public static MockAccount getAccount1() {
		mockAccount = new MockAccount();
		mockAccount.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		Data2Account mockData2Account = new Data2Account();
		mockData2Account.setIdentification("10203345");
		mockData2Account
				.setSchemeName(Data2Account.SchemeNameEnum.IBAN);
		mockData2Account.setName("Lorem");
		mockAccount.setAccount(mockData2Account);

		Data2Servicer mockData2Servicer = new Data2Servicer();
		mockData2Servicer.setIdentification("SC802001");
		mockData2Servicer
				.setSchemeName(Data2Servicer.SchemeNameEnum.BICFI);
		mockAccount.setServicer(mockData2Servicer);
		return mockAccount;
	}

	private static MockAccount getAccount2() {
		mockAccount=new MockAccount();
		Data2Account mockData2Account = new Data2Account();
		mockData2Account.setIdentification("76528776");
		mockData2Account
				.setSchemeName(Data2Account.SchemeNameEnum.IBAN);
		mockData2Account.setName("Mr Kevin");
		mockAccount.setAccount(mockData2Account);

		Data2Servicer mockData2Servicer = new Data2Servicer();
		mockData2Servicer.setIdentification("SC802001");
		mockData2Servicer
				.setSchemeName(Data2Servicer.SchemeNameEnum.BICFI);
		mockAccount.setServicer(mockData2Servicer);
		return mockAccount;
	}

	private static MockAccount getAccount3() {
		mockAccount=new MockAccount();
		Data2Account mockData2Account = new Data2Account();
		mockData2Account.setIdentification("25369621");
		mockData2Account
				.setSchemeName(Data2Account.SchemeNameEnum.IBAN);
		mockData2Account.setName("Mr Jack");
		mockAccount.setAccount(mockData2Account);

		Data2Servicer mockData2Servicer = new Data2Servicer();
		mockData2Servicer.setIdentification("SC802001");
		mockData2Servicer
				.setSchemeName(Data2Servicer.SchemeNameEnum.BICFI);
		mockAccount.setServicer(mockData2Servicer);
		return mockAccount;
	}

	private static MockAccount getAccount4() {
		mockAccount=new MockAccount();
		Data2Account mockData2Account = new Data2Account();
		mockData2Account.setIdentification("9999");
		mockData2Account
				.setSchemeName(Data2Account.SchemeNameEnum.IBAN);
		mockData2Account.setName("Mr Sam");
		mockAccount.setAccount(mockData2Account);

		Data2Servicer mockData2Servicer = new Data2Servicer();
		mockData2Servicer.setIdentification("SC802001");
		mockData2Servicer
				.setSchemeName(Data2Servicer.SchemeNameEnum.BICFI);
		mockAccount.setServicer(mockData2Servicer);
		return mockAccount;
	}

	private static MockAccount getAccount5() {
		mockAccount=new MockAccount();
		Data2Account mockData2Account = new Data2Account();
		mockData2Account.setIdentification("888");
		mockData2Account
				.setSchemeName(Data2Account.SchemeNameEnum.IBAN);
		mockData2Account.setName("Mr Ham");
		mockAccount.setAccount(mockData2Account);

		Data2Servicer mockData2Servicer = new Data2Servicer();
		mockData2Servicer.setIdentification("SC802001");
		mockData2Servicer
				.setSchemeName(Data2Servicer.SchemeNameEnum.BICFI);
		mockAccount.setServicer(mockData2Servicer);
		return mockAccount;
	}

	public static List<Account> getSingleAccountGETResponseDataList() {
		List<Account> mockAccountList = new ArrayList<>();
		mockAccountList.add(getAccount1());
		return mockAccountList;
	}

	public static List<MockAccount> getAccountGETResponseDataList() {
		List<MockAccount> mockAccountList = new ArrayList<>();
		mockAccountList.add(getAccount1());
		mockAccountList.add(getAccount2());
		mockAccountList.add(getAccount3());
		mockAccountList.add(getAccount4());
		mockAccountList.add(getAccount5());
		return mockAccountList;
	}
	
	public static AccountMapping getAccountMapping1() {
		AccountMapping accountMapping=new AccountMapping();
		accountMapping.setPsuId("88888888");
		List<AccountDetails> accountDetails=new ArrayList<>();
		AccountDetails obj1=new AccountDetails();
		obj1.setAccountId("d1c3d9cb-1725-45c2-8c99-f061ec53b37e");
		obj1.setAccountNSC("901343");
		obj1.setAccountNumber("47550843");
		
		AccountDetails obj2=new AccountDetails();
		obj2.setAccountId("d2192679-c26f-4af6-9102-2c9e8b8c207b");
		obj2.setAccountNSC("903779");
		obj2.setAccountNumber("25369621");
		
		accountDetails.add(obj1);
		accountDetails.add(obj2);
		accountMapping.setAccountDetails(accountDetails);
		accountMapping.setTppCID("123");
		return accountMapping;
	}

	public static List<MockAccount> getMockAccount() {
		List<MockAccount> mockAccountList=new ArrayList<>();
		
		MockAccount mockAccount=new MockAccount();
		mockAccount.setAccountType("savings");
		mockAccount.setCurrency("GBP");
		mockAccount.setNickname("james");
		mockAccount.setAccountId("d1c3d9cb-1725-45c2-8c99-f061ec53b37e");
		Data2Account data2account=new Data2Account();
		data2account.setIdentification("47550843");
		data2account.setName("franklin");
		data2account.setSchemeName(SchemeNameEnum.SORTCODEACCOUNTNUMBER);
		data2account.setSecondaryIdentification("0044");
		mockAccount.setAccount(data2account);
		
		Data2Servicer data2servicer=new Data2Servicer();
		data2servicer.setIdentification("901343");
		mockAccount.setServicer(data2servicer);
		mockAccountList.add(mockAccount);
		return mockAccountList;
	}
}
