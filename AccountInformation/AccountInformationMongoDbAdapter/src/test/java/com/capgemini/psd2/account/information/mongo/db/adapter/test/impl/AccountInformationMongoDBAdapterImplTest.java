/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.information.mongo.db.adapter.test.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.account.information.mongo.db.adapter.impl.AccountInformationMongoDbAdaptorImpl;
import com.capgemini.psd2.account.information.mongo.db.adapter.repository.AccountInformationRepository;
import com.capgemini.psd2.account.information.mongo.db.adapter.test.mock.data.AccountInformationAdapterTestMockData;
import com.capgemini.psd2.aisp.domain.Account;
import com.capgemini.psd2.aisp.domain.AccountGETResponse;
import com.capgemini.psd2.aisp.mock.domain.MockAccount;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.validator.PSD2Validator;

/**
 * The Class AccountInformationMongoDBAdapterImplTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountInformationMongoDBAdapterImplTest {

	/** The account info repo. */
	@Mock
	private AccountInformationRepository accountInfoRepo;
	
	@Mock
	private PSD2Validator validator;

	/**
	 * Sets the up.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	/** The account information mongo DB adapter impl. */
	@InjectMocks
	private AccountInformationMongoDbAdaptorImpl accountInformationMongoDBAdapterImpl;

	/**
	 * Test retrieve Single account information success flow.
	 */
	@Test
	public void testRetrieveAccountInformationSuccessFlow() {
		AccountMapping accountMapping = AccountInformationAdapterTestMockData.getAccountMapping1();
	
		List<MockAccount> mockAccountList = AccountInformationAdapterTestMockData.getMockAccount();
		
		Mockito.when(accountInfoRepo.findByAccountIdentificationAndServicerIdentification(anyString(), anyString()))
				.thenReturn(mockAccountList);
		AccountGETResponse accountGETResponse = accountInformationMongoDBAdapterImpl
				.retrieveAccountInformation(accountMapping, null);

		assertEquals("d1c3d9cb-1725-45c2-8c99-f061ec53b37e", accountGETResponse.getData().getAccount().get(0).getAccountId());

		assertEquals(AccountInformationAdapterTestMockData.getAccountGETResponse().getData().getAccount().get(0).getAccount()
				.getIdentification(), accountGETResponse.getData().getAccount().get(0).getAccount().getIdentification());

		assertEquals(AccountInformationAdapterTestMockData.getAccountGETResponse().getData().getAccount().get(0).getServicer()
				.getIdentification(), accountGETResponse.getData().getAccount().get(0).getServicer().getIdentification());
	}

	/**
	 * Test retrieve Single account information data access resource failure
	 * exception.
	 */
	@Test(expected = PSD2Exception.class)
	public void testRetrieveAccountInformationDataAccessResourceFailureException() {
		Mockito.when(accountInfoRepo.findByAccountIdentificationAndServicerIdentification(anyString(), anyString()))
				.thenThrow(new DataAccessResourceFailureException("Test"));
		assertEquals(AccountInformationAdapterTestMockData.getAccountGETResponse(), accountInformationMongoDBAdapterImpl
				.retrieveAccountInformation(AccountInformationAdapterTestMockData.getAccountMapping(), null));
	}

	/**
	 * Test retrieve Single account information null exception.
	 */
	@Test(expected = PSD2Exception.class)
	public void testRetrieveAccountInformationNullException() {

		Mockito.when(accountInfoRepo.findByAccountIdentificationAndServicerIdentification(anyString(), anyString()))
				.thenReturn(new ArrayList<>());

		accountInformationMongoDBAdapterImpl
				.retrieveAccountInformation(AccountInformationAdapterTestMockData.getAccountMapping(), null);
	}

	/**
	 * Test retrieve Multiple accounts information success flow.
	 */
	@Test
	public void testRetrieveMultipleAccountInformationSuccessFlow() {
		AccountMapping accountMapping = AccountInformationAdapterTestMockData.getAccountMapping1();
		List<MockAccount> mockAccountList=AccountInformationAdapterTestMockData.getMockAccount();
		//List<MockAccount> accountList = AccountInformationAdapterTestMockData.getAccountGETResponseDataList();
		//mockAccountList = accountList;
		Mockito.when(accountInfoRepo.findByPsuId(anyString()))
				.thenReturn(mockAccountList);
		AccountGETResponse accountGETResponse = accountInformationMongoDBAdapterImpl
				.retrieveMultipleAccountsInformation(accountMapping, null);

		assertEquals("d1c3d9cb-1725-45c2-8c99-f061ec53b37e", accountGETResponse.getData().getAccount().get(0).getAccountId());

		assertEquals(AccountInformationAdapterTestMockData.getAccountGETResponse().getData().getAccount().get(0).getAccount()
				.getIdentification(), accountGETResponse.getData().getAccount().get(0).getAccount().getIdentification());

		assertEquals(AccountInformationAdapterTestMockData.getAccountGETResponse().getData().getAccount().get(0).getServicer()
				.getIdentification(), accountGETResponse.getData().getAccount().get(0).getServicer().getIdentification());
	}

	/**
	 * Test retrieve Multiple accounts information data access resource failure
	 * exception.
	 */
	@Test(expected = PSD2Exception.class)
	public void testRetrieveMultipleAccountInformationDataAccessResourceFailureException() {
		Mockito.when(accountInfoRepo.findByPsuId(anyString()))
				.thenThrow(new DataAccessResourceFailureException("Test"));
		try {
			accountInformationMongoDBAdapterImpl.retrieveMultipleAccountsInformation(
					AccountInformationAdapterTestMockData.getAccountMapping(), null);
		} catch (PSD2Exception e) {
			assertEquals("Error while establishing connection,please try again..", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	/**
	 * Test retrieve Multiple accounts information empty List.
	 */
	@Test(expected = PSD2Exception.class)
	public void testRetrieveMultipleAccountInformationEmptyList() {
		List<MockAccount> list = new ArrayList<>();
		Mockito.when(accountInfoRepo.findAll()).thenReturn(list);
		try {
			accountInformationMongoDBAdapterImpl.retrieveMultipleAccountsInformation(
					AccountInformationAdapterTestMockData.getAccountMapping(), null);
		} catch (PSD2Exception e) {
			assertEquals("No data found for the requested account", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

}
