package com.capgemini.psd2.payment.insert.update.boi.fs;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.InsertPreStagePaymentFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.constants.InsertPreStagePaymentFoundationServiceConstants;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupStagingResponse;

@SpringBootApplication
@ComponentScan("com.capgemini.psd2")
public class PaymentInsertUpdateFoundationServiceTestProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaymentInsertUpdateFoundationServiceTestProjectApplication.class, args);
	}
	
}

@RestController
@ResponseBody
class TestPaymentInsertUpdateFSAdapter {

	@Autowired
	InsertPreStagePaymentFoundationServiceAdapter adapter;
	
	@RequestMapping(value="/testPaymentInsertUpdateAdapter", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public PaymentSetupStagingResponse getResponse(
			@RequestBody CustomPaymentSetupPOSTRequest pr,
			@RequestHeader(required = false,value="X-BOI-USER") String boiUser,
			@RequestHeader(required = false,value="X-BOI-CHANNEL") String boiChannel,
			@RequestHeader(required = false,value="X-BOI-PLATFORM") String boiPlatform,
			@RequestHeader(required = false,value="X-CORRELATION-ID") String correlationID) {
		
		Map<String, String> params = new HashMap<>();
		params.put(InsertPreStagePaymentFoundationServiceConstants.USER_ID, boiUser);
		params.put(InsertPreStagePaymentFoundationServiceConstants.CHANNEL_ID, boiChannel);
		params.put(InsertPreStagePaymentFoundationServiceConstants.CORRELATION_ID, correlationID);
		params.put(InsertPreStagePaymentFoundationServiceConstants.PLATFORM_ID, boiPlatform);
		params.put(InsertPreStagePaymentFoundationServiceConstants.VALIDATION_STATUS, "Pending");
		//params.put(PSD2Constants.PAYMENT_SUBMISSION_STATUS, "AcceptedSubmissionStatus");
		//pr.getData().setStatus(StatusEnum.ACCEPTEDCUSTOMERPROFILE);
		//pr.getData().setCreationDateTime(new DateTime());
		return adapter.createStagingPaymentSetup(pr, params);
		//return adapter.updateStagedPaymentSetup(pr, params);

	}
	
	@RequestMapping(value="/testPaymentRetrieval/{Payment-Id}", method = RequestMethod.GET, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public PaymentSetupPOSTResponse getResponse(
			@PathVariable("Payment-Id") String paymentId,
			@RequestHeader(required = false,value="X-BOI-USER") String boiUser,
			@RequestHeader(required = false,value="X-BOI-CHANNEL") String boiChannel,
			@RequestHeader(required = false,value="X-BOI-PLATFORM") String boiPlatform,
			@RequestHeader(required = false,value="X-CORRELATION-ID") String correlationID){

		Map<String, String> params = new HashMap<>();
		params.put(InsertPreStagePaymentFoundationServiceConstants.USER_ID, boiUser);
		params.put(InsertPreStagePaymentFoundationServiceConstants.CHANNEL_ID, boiChannel);
		params.put(InsertPreStagePaymentFoundationServiceConstants.CORRELATION_ID, correlationID);
		params.put(InsertPreStagePaymentFoundationServiceConstants.PLATFORM_ID, boiPlatform);
		params.put(InsertPreStagePaymentFoundationServiceConstants.VALIDATION_STATUS, "Pending");

		return adapter.retrieveStagedPaymentSetup(paymentId, params);
		
	}
	
}