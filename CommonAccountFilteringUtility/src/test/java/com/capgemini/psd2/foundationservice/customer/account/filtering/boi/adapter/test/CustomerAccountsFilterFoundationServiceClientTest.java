package com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.client.CustomerAccountsFilterFoundationServiceClient;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accounts;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.ChannelProfile;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
@RunWith(SpringJUnit4ClassRunner.class)
public class CustomerAccountsFilterFoundationServiceClientTest {

	@InjectMocks
	CustomerAccountsFilterFoundationServiceClient customerAccountsFilterFoundationServiceClient;
	
	@Mock
	RestClientSync restClient;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testRestTransportForCustomerAccountProfile(){
		
		ChannelProfile channelProfile=new ChannelProfile();
		Accnts accounts = new Accnts();
		channelProfile.setAccounts(accounts);
		when(restClient.callForGet(anyObject(), anyObject(), anyObject())).thenReturn(channelProfile);
		Accnts accnts = customerAccountsFilterFoundationServiceClient.restTransportForCustomerAccountProfile(new RequestInfo(), ChannelProfile.class, new HttpHeaders());
		assertNotNull(accnts);
	}
	@Test
	public void testRestTransportForSingleAccountProfile(){
		Accounts accounts = new Accounts();
		Accnt accnt = new Accnt();
		accnt.setAccountNSC("123");
		accounts.setAccount(accnt);
		when(restClient.callForGet(anyObject(), anyObject(), anyObject())).thenReturn(accounts);
		Accnts accnts = customerAccountsFilterFoundationServiceClient.restTransportForSingleAccountProfile(new RequestInfo(), Accounts.class, new HttpHeaders());
		assertNotNull(accnts);
	}
}
