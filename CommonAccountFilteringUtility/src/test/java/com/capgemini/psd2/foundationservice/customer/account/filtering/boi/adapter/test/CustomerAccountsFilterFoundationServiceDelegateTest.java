package com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.utility.AdapterUtility;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.delegate.CustomerAccountsFilterFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accounts;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion.CustomerAccountsFilter;
import com.capgemini.psd2.logger.PSD2Constants;
@RunWith(SpringJUnit4ClassRunner.class)
public class CustomerAccountsFilterFoundationServiceDelegateTest {

	@InjectMocks
	CustomerAccountsFilterFoundationServiceDelegate customerAccountsFilterFoundationServiceDelegate;
	
	@Mock
	AdapterUtility adapterUtility;
	
	@Mock
	CustomerAccountsFilter CustAccntProfileSerTrans;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testGetFoundationServiceUrlAISP(){
		
		Mockito.when(adapterUtility.retriveCustProfileFoundationServiceURL(anyObject())).thenReturn("test");
		String result = customerAccountsFilterFoundationServiceDelegate.getFoundationServiceURLAISP(new HashMap<>(), "test");
		assertNotNull(result);
	}
	
	@Test(expected = AdapterException.class)
	public void testGetFoundationServiceURLPISP(){
		when(adapterUtility.retriveFoundationServiceURL("test")).thenReturn("test");
		String result = customerAccountsFilterFoundationServiceDelegate.getFoundationServiceURLPISP("test", "test", "123");
		assertNotNull(result);
		/*
		 * baseURL null
		 */
		when(adapterUtility.retriveFoundationServiceURL("test")).thenReturn(null);
		customerAccountsFilterFoundationServiceDelegate.getFoundationServiceURLPISP("test", "test", "123");
		
	}
	
	@Test(expected = AdapterException.class)
	public void testGetFoundationServiceURLPISPChannelIdNull(){
		customerAccountsFilterFoundationServiceDelegate.getFoundationServiceURLPISP(null, "test", "123");
		
	}
	
	@Test(expected = AdapterException.class)
	public void testGetFoundationServiceURLPISPAccountNSCNull(){
		customerAccountsFilterFoundationServiceDelegate.getFoundationServiceURLPISP("test", null, "123");
		
	}
	
	@Test(expected = AdapterException.class)
	public void testGetFoundationServiceURLPISPAccountNumberNull(){
		customerAccountsFilterFoundationServiceDelegate.getFoundationServiceURLPISP("test", "test", null);
		
	}
	
	
	
	@Test
	public void testGetFilteredAccounts(){
		Accnts accounts = new Accnts();
		Mockito.when(CustAccntProfileSerTrans.getFilteredAccounts(anyObject(), anyObject())).thenReturn(accounts);
		Accnts result = customerAccountsFilterFoundationServiceDelegate.getFilteredAccounts(accounts, new HashMap<>());
		assertNotNull(result);
	}
	
	@Test
	public void testCreateRequestHeadersAISP(){
		
		ReflectionTestUtils.setField(customerAccountsFilterFoundationServiceDelegate, "userInReqHeader", "user");
		ReflectionTestUtils.setField(customerAccountsFilterFoundationServiceDelegate, "channelInReqHeader", "channel");
		ReflectionTestUtils.setField(customerAccountsFilterFoundationServiceDelegate, "platformInReqHeader", "platform");
		ReflectionTestUtils.setField(customerAccountsFilterFoundationServiceDelegate, "correlationReqHeader", "correlation");
		ReflectionTestUtils.setField(customerAccountsFilterFoundationServiceDelegate, "platform", "platform");
		
		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.USER_ID, "user");
		params.put(PSD2Constants.CHANNEL_ID, "channel");
		params.put(PSD2Constants.CORRELATION_ID, "correlation");
		
		HttpHeaders headers = customerAccountsFilterFoundationServiceDelegate.createRequestHeadersAISP(params);
		assertNotNull(headers);
	}
	
	@Test(expected = AdapterException.class)
	public void testCreateRequestHeadersUserIdNull(){
		
		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.USER_ID, null);
		params.put(PSD2Constants.CHANNEL_ID, "channel");
		params.put(PSD2Constants.CORRELATION_ID, "correlation");
		
		customerAccountsFilterFoundationServiceDelegate.createRequestHeadersAISP(params);
	}
	
	@Test(expected = AdapterException.class)
	public void testCreateRequestHeadersChannelIdNull(){
		
		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.USER_ID, "test");
		params.put(PSD2Constants.CHANNEL_ID, null);
		params.put(PSD2Constants.CORRELATION_ID, "correlation");
		
		customerAccountsFilterFoundationServiceDelegate.createRequestHeadersAISP(params);
	}
	@Test(expected = AdapterException.class)
	public void testCreateRequestHeadersCorrelationIdNull(){
		
		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.USER_ID, "test");
		params.put(PSD2Constants.CHANNEL_ID, "channel");
		params.put(PSD2Constants.CORRELATION_ID, null);
		
		customerAccountsFilterFoundationServiceDelegate.createRequestHeadersAISP(params);
	}
	
	@Test(expected = AdapterException.class)
	public void testCreateRequestHeadersPlatformNull(){
		
		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.USER_ID, "test");
		params.put(PSD2Constants.CHANNEL_ID, "channel");
		params.put(PSD2Constants.CORRELATION_ID, "correlation");
		ReflectionTestUtils.setField(customerAccountsFilterFoundationServiceDelegate, "platform", null);
		customerAccountsFilterFoundationServiceDelegate.createRequestHeadersAISP(params);
	}
	
	@Test
	public void testCreateRequestHeadersPISP(){
		
		ReflectionTestUtils.setField(customerAccountsFilterFoundationServiceDelegate, "userInReqHeader", "user");
		ReflectionTestUtils.setField(customerAccountsFilterFoundationServiceDelegate, "channelInReqHeader", "channel");
		ReflectionTestUtils.setField(customerAccountsFilterFoundationServiceDelegate, "platformInReqHeader", "platform");
		ReflectionTestUtils.setField(customerAccountsFilterFoundationServiceDelegate, "correlationReqHeader", "correlation");
		ReflectionTestUtils.setField(customerAccountsFilterFoundationServiceDelegate, "platform", "platform");
		
		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.USER_IN_REQ_HEADER, "user");
		params.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, "channel");
		params.put(PSD2Constants.CORRELATION_REQ_HEADER, "correlation");
		
		HttpHeaders headers = customerAccountsFilterFoundationServiceDelegate.createRequestHeadersForPISP(params);
		assertNotNull(headers);
	}
	
	@Test(expected = AdapterException.class)
	public void testCreateRequestHeadersPISPUserIdNull(){
		
		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.USER_IN_REQ_HEADER, null);
		params.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, "channel");
		params.put(PSD2Constants.CORRELATION_REQ_HEADER, "correlation");
		
		customerAccountsFilterFoundationServiceDelegate.createRequestHeadersForPISP(params);
	}
	
	@Test(expected = AdapterException.class)
	public void testCreateRequestHeadersPISPChannelIdNull(){
		
		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.USER_IN_REQ_HEADER, "test");
		params.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, null);
		params.put(PSD2Constants.CORRELATION_REQ_HEADER, "correlation");
		
		customerAccountsFilterFoundationServiceDelegate.createRequestHeadersAISP(params);
	}
	@Test(expected = AdapterException.class)
	public void testCreateRequestHeadersPISPCorrelationIdNull(){
		
		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.USER_IN_REQ_HEADER, "test");
		params.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, "channel");
		params.put(PSD2Constants.CORRELATION_REQ_HEADER, null);
		
		customerAccountsFilterFoundationServiceDelegate.createRequestHeadersAISP(params);
	}
	
	@Test(expected = AdapterException.class)
	public void testCreateRequestHeadersPISPPlatformNull(){
		
		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.USER_IN_REQ_HEADER, "test");
		params.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, "channel");
		params.put(PSD2Constants.CORRELATION_REQ_HEADER, "correlation");
		ReflectionTestUtils.setField(customerAccountsFilterFoundationServiceDelegate, "platform", null);
		customerAccountsFilterFoundationServiceDelegate.createRequestHeadersAISP(params);
	}
}
