package com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.test;

import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accounts;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion.CustomerAccountsFilter;
import com.capgemini.psd2.logger.PSD2Constants;

@RunWith(SpringJUnit4ClassRunner.class)
public class CustomerAccountsFilterTest {

	@InjectMocks
	CustomerAccountsFilter customerAccountsFilter;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testGetFilteredAccounts(){
		
		Accnts inputResObject = new Accnts();
		Map<String, String> params = new HashMap<>();
		Map<String, Map<String, List<String>>> map = new HashMap<>();
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, "test");
		customerAccountsFilter.setAccountFiltering(map);
		customerAccountsFilter.getAccountFiltering();
		Accnts accounts = customerAccountsFilter.getFilteredAccounts(inputResObject, params);
		
		assertNotNull(accounts);
	}
}
