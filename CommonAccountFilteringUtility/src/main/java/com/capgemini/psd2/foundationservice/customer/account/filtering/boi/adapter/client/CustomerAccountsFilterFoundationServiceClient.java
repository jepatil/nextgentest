package com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.client;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accounts;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.ChannelProfile;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;

@Component
public class CustomerAccountsFilterFoundationServiceClient {
	
	/** The rest client. */
	@Autowired
	@Qualifier("restClientFoundation")
	private RestClientSync restClient;

	
/**
 * Rest transport for Customer Account List 
 * @param reqInfo
 * @param responseType
 * @param headers
 * @return list of accounts 
 */
	public Accnts restTransportForCustomerAccountProfile(RequestInfo reqInfo, Class <ChannelProfile> responseType, HttpHeaders headers)
	{
		ChannelProfile channelProfile=restClient.callForGet(reqInfo, responseType, headers);
		return channelProfile.getAccounts();
	}
	
	public Accnts restTransportForSingleAccountProfile(RequestInfo reqInfo, Class <Accounts> responseType, HttpHeaders headers)
	{
		Accounts accounts=restClient.callForGet(reqInfo, responseType, headers);
		Accnts accnts=new Accnts();
		accnts.getAccount().add(accounts.getAccount());
		return accnts;
	}

}
