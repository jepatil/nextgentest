package com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.constants.CustomerAccountsFilterFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.FilteredAccounts;

public class AccountTypeFilter implements FilterationChain {
	
	private FilterationChain nextInChain;

	@Override
	public void setNext(FilterationChain next) {
		nextInChain = next;

	}

	@Override
	public FilteredAccounts process(Accnts accounts, String consentFlowType,
			Map<String, Map<String, List<String>>> accountFiltering) {
		
		FilteredAccounts filteredAccounts = new FilteredAccounts();
		
		if (accounts.getAccount().size() > 0) {
			Map<String, List<String>> accountTypeList = accountFiltering.get(CustomerAccountsFilterFoundationServiceConstants.ACCOUNTTYPE);
			if(accountTypeList != null && !accountTypeList.isEmpty()){
				List<String> validAccountTypeForFlow = (ArrayList<String>) accountTypeList.get(consentFlowType);
				List<Accnt> accountList = new ArrayList<>();
				
				validAccountTypeForFlow = validAccountTypeForFlow.stream().map(u -> u.toUpperCase()).collect(Collectors.toList());
				
				for (Accnt accnt : accounts.getAccount()) {
					if (accnt.getAccountType() != null){
						if (validAccountTypeForFlow.contains(accnt.getAccountType().toUpperCase().trim())) {   // TODO : Chances of discrepancy in account type as it is a String
							accountList.add(accnt);
						}
					}
				}
				filteredAccounts.setAccount(accountList);
				filteredAccounts.setCreditCardAccount(accounts.getCreditCardAccount());
				
				return nextInChain.process(filteredAccounts,consentFlowType,accountFiltering);
			
			}else{
				return nextInChain.process(accounts,consentFlowType,accountFiltering);
			}
			
		} 
		return filteredAccounts;
		
	}

	
	

}
