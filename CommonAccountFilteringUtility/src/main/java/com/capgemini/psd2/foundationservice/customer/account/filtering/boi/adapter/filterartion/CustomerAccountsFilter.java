package com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accounts;
import com.capgemini.psd2.logger.PSD2Constants;

/**
 * The Class CustomerAccountProfileFoundationServiceTransformer
 *
 */

@Component
@ConfigurationProperties(prefix = "foundationService")
@Configuration
@EnableAutoConfiguration
public class CustomerAccountsFilter  {
	
	/** The Account filters */
	private Map<String , Map<String,List<String>>> accountFiltering = new HashMap<>();

	public Accnts getFilteredAccounts(Accnts inputResObject, Map<String, String> params) {
		
		/** Configuring the chain of responsibility filters */
		FilterationChain jurisdictionFilter = new JurisdictionFilter();
		FilterationChain accountTypeFilter = new AccountTypeFilter();
		FilterationChain permissionFilter = new PermissionFilter();
		jurisdictionFilter.setNext(accountTypeFilter);
		accountTypeFilter.setNext(permissionFilter);
		
		Accnts accnts = (Accnts) inputResObject;
		String consentFlowType = params.get(PSD2Constants.CONSENT_FLOW_TYPE);
		
		return jurisdictionFilter.process(accnts,consentFlowType,accountFiltering);
	}

	public Map<String, Map<String, List<String>>> getAccountFiltering() {
		return accountFiltering;
	}

	public void setAccountFiltering(Map<String, Map<String, List<String>>> accountFiltering) {
		this.accountFiltering = accountFiltering;
	}


	

}