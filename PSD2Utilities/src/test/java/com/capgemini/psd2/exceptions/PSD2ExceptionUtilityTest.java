package com.capgemini.psd2.exceptions;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringJUnit4ClassRunner.class)
public class PSD2ExceptionUtilityTest {

	@Mock
	private RestTemplate restTemplate;
	
	@InjectMocks
	private PSD2ExceptionUtility exceptionUtility;
	
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testUpdateErrorMapping(){
		Map<String,String> errorMap = new HashMap<>();
		errorMap.put("test", "value");
		errorMap.put("400", "Bad Request");
		ReflectionTestUtils.setField(exceptionUtility, "errorMap", errorMap);
		ReflectionTestUtils.setField(exceptionUtility, "sentDetailErrorMessage",true );
		ReflectionTestUtils.setField(exceptionUtility, "sentErrorPayload", true);
		when(restTemplate.getForObject(anyString(),any())).thenReturn("test");
		exceptionUtility.init();
	}
	
	@SuppressWarnings("static-access")
	@Test
	public void testGetErrorMessagefromStatusCode(){
		exceptionUtility.getErrorMessagefromStatusCode("400");
	}
}
