/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.mask;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.capgemini.psd2.utilities.JSONUtilities;

/**
 * The Class PSD2DataMaskTest.
 */
public class PSD2DataMaskTest 
{
    
    /**
     * Test mask response body.
     */
    @Test
    public void testMaskResponseBody() {
        Map input = (Map) JSONUtilities.getObjectFromJSONString("{\"name\":\"Steve\",\"list\":[{\"name\":12345,\"inside\":[{\"name\":\"Steve\"}]},{\"name\":1234455,\"inside\":[{\"name\":\"Steve\"}]}],\"password\":\"secret\"}", Map.class);
        DataMaskRules dataMakRules = new DataMaskRules();
        DataMask dataMask = new DataMaskImpl(dataMakRules);
        Map<String, String> patternMap = new HashMap<String, String>();
		patternMap.put("$.list[*].name", "\\w(?=\\w{4}),X");
		patternMap.put("$.list[*].inside[*].name", "\\w(?=\\w{4}),X");
		patternMap.put("$.password", "\\w,X");
        Map output = dataMask.mask(input, patternMap, MaskOutput.UPDATE);
        assertTrue(output == input);
        assertTrue(output.get("password").equals("XXXXXX"));
        output = dataMask.mask(input, patternMap, MaskOutput.NEW);
        assertTrue(output != input);
        assertTrue(output.get("password").equals("XXXXXX"));
    }
        
    /**
     * Test data mask POJO.
     */
    @Test
    public void testDataMaskPOJO() {
    	DataMaskRules dataMakRules = new DataMaskRules();
    	
    	//Test case for Response 
    	Map<String, Map<String,String>> response = new HashMap<>();
        Map<String,String> responseInput = new HashMap<>();
        responseInput.put("key", "value");
        response.put("response", responseInput);
        dataMakRules.setResponse(response);
        assertTrue(dataMakRules.getResponse().equals(response));
        
        //Test case for ResponseLog
        response = new HashMap<>();
        response.put("responseLog", responseInput);
        dataMakRules.setResponseLog(response);
        assertTrue(dataMakRules.getResponseLog().equals(response));
        
        //Test case for RequestLog
        response = new HashMap<>();
        response.put("requestLog", responseInput);
        dataMakRules.setRequestLog(response);
        assertTrue(dataMakRules.getRequestLog().equals(response));
        
        //Test case for Mresponse
        response = new HashMap<>();
        response.put("Mresponse", responseInput);
        dataMakRules.setMresponse(response);
        assertTrue(dataMakRules.getMresponse().equals(response));
        
        //Test case for MresponseLog
        response = new HashMap<>();
        response.put("MresponseLog", responseInput);
        dataMakRules.setMresponseLog(response);
        assertTrue(dataMakRules.getMresponseLog().equals(response));
        
        //Test case for MrequestLog
        response = new HashMap<>();
        response.put("MrequestLog", responseInput);
        dataMakRules.setMrequestLog(response);
        assertTrue(dataMakRules.getMrequestLog().equals(response));
    }
}
