/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.logger;

import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.test.util.ReflectionTestUtils;



/**
 * The Class PSD2FilterTest.
 */
public class PSD2FilterTest {
	
	/** The http servlet request. */
	@Mock
	private HttpServletRequest httpServletRequest;
	
	/** The request header attributes. */
	@Mock
	private RequestHeaderAttributes requestHeaderAttributes;
	
	/** The http servlet response. */
	@Mock
	private HttpServletResponse httpServletResponse;
	
	/** The filter chain. */
	@Mock
	private FilterChain filterChain;
	
	/** The logger utils. */
	@Mock
	private LoggerUtils loggerUtils;
	
	/** The psd 2 filter. */
	@InjectMocks
	private PSD2Filter psd2Filter = new PSD2Filter();

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
		
	/**
	 * Test.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ServletException the servlet exception
	 */
	@Test 
	public void test() throws IOException, ServletException{
		Mockito.when(httpServletRequest.getHeader(PSD2Constants.CORRELATION_ID)).thenReturn("ba4f73f8-9a60-425b-aed8-2a7ef2509fea");
		Mockito.when(httpServletRequest.getHeader("Authorization")).thenReturn("Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0cHBJbmZvLWxlZ2FsRW50aXR5TmFtZSI6Ik1vbmV5d2lzZSBQdnQuIEx0ZC4iLCJhdWQiOlsiYmFua0FjY291bnRUcmFuc2FjdGlvbnMiXSwiYmFua0lkIjoiMSIsImNvbnNlbnRJZCI6IjY3MTE4MjkxIiwidHBwSW5mby1yZWdpc3RlcmVkSWQiOiIwMDciLCJ1c2VyX25hbWUiOiJkZW1vdXNlciIsInRwcEluZm8tZGlzcGxhdE5hbWUiOiJNb25leXdpc2UiLCJleHAiOjE0OTEwNDQ3MDcsImF1dGhvcml0aWVzIjpbIlJPTEVfTU9ORVlXSVNFIiwiUk9MRV9DVVNUT01FUlMiXSwianRpIjoiZWUwMzE4NTktYmIxOC00ZWNiLWJmNzAtMTJmYTMzOTYyMDJkIiwiY2xpZW50X2lkIjoibW9uZXl3aXNlIiwidHBwSW5mby10cHBUeXBlIjoiYWlzcCJ9.knD_aMYrmVJzQa8Jc5ypOWoPRU5TCkAEGSdhLOpXfqYLNyqB9QyUfIAb2PfnzZbwTRIsJ7T1rFV5l7779ktVKSv1OGw853J_8y3jUa2jfY7sFVnZxS4PLK17VnzaXUhFXT3L5R2PKSWlGYocLZkOb-BW9Go1L_nTzcx0xsCz5TaOPXh3xih_Bduzo15fu5JGMMVQKaBTd0x_YEtHB25cT57cJnbjrqYObMO0hh6WBkbA8fC4xePan3jyZsyjomVuWOGaS4xJBqhEL_TK5fVng7O1SlLHGkQYzRRrbg7pOL7W2AyJGkMV9BcUgIkKw7L5gPceB1IjL3ug4bPN9DRSXQ");
		Mockito.when(httpServletRequest.getHeader(PSD2Constants.FINANCIAL_ID)).thenReturn("finance123");
		Mockito.when(httpServletResponse.getWriter()).thenReturn(new PrintWriter("servletResponse"));
		psd2Filter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);
	}
	
	/**
	 * Testdo filter internal exceptions.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ServletException the servlet exception
	 */
	@Test
	public void testdoFilterInternalExceptions() throws IOException, ServletException {
		Mockito.when(httpServletRequest.getHeader(PSD2Constants.CORRELATION_ID)).thenReturn("ba4f73f8-9a60-425b-aed8-2a7ef2509fea");
		Mockito.when(httpServletRequest.getHeader("Authorization")).thenReturn("Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0cHBJbmZvLWxlZ2FsRW50aXR5TmFtZSI6Ik1vbmV5d2lzZSBQdnQuIEx0ZC4iLCJhdWQiOlsiYmFua0FjY291bnRUcmFuc2FjdGlvbnMiXSwiYmFua0lkIjoiMSIsImNvbnNlbnRJZCI6IjY3MTE4MjkxIiwidHBwSW5mby1yZWdpc3RlcmVkSWQiOiIwMDciLCJ1c2VyX25hbWUiOiJkZW1vdXNlciIsInRwcEluZm8tZGlzcGxhdE5hbWUiOiJNb25leXdpc2UiLCJleHAiOjE0OTEwNDQ3MDcsImF1dGhvcml0aWVzIjpbIlJPTEVfTU9ORVlXSVNFIiwiUk9MRV9DVVNUT01FUlMiXSwianRpIjoiZWUwMzE4NTktYmIxOC00ZWNiLWJmNzAtMTJmYTMzOTYyMDJkIiwiY2xpZW50X2lkIjoibW9uZXl3aXNlIiwidHBwSW5mby10cHBUeXBlIjoiYWlzcCJ9.knD_aMYrmVJzQa8Jc5ypOWoPRU5TCkAEGSdhLOpXfqYLNyqB9QyUfIAb2PfnzZbwTRIsJ7T1rFV5l7779ktVKSv1OGw853J_8y3jUa2jfY7sFVnZxS4PLK17VnzaXUhFXT3L5R2PKSWlGYocLZkOb-BW9Go1L_nTzcx0xsCz5TaOPXh3xih_Bduzo15fu5JGMMVQKaBTd0x_YEtHB25cT57cJnbjrqYObMO0hh6WBkbA8fC4xePan3jyZsyjomVuWOGaS4xJBqhEL_TK5fVng7O1SlLHGkQYzRRrbg7pOL7W2AyJGkMV9BcUgIkKw7L5gPceB1IjL3ug4bPN9DRSXQ");
		Mockito.when(httpServletRequest.getHeader(PSD2Constants.FINANCIAL_ID)).thenReturn("finance123");
		Mockito.when(httpServletRequest.getHeader(PSD2Constants.CUSTOMER_IP_ADDRESS)).thenReturn("10.1.1.2");
		Mockito.when(httpServletRequest.getHeader(PSD2Constants.CUSTOMER_LAST_LOGGED_TIME)).thenReturn("10-10-2017T10:10");
		Mockito.when(httpServletResponse.getWriter()).thenReturn(new PrintWriter("servletResponse"));
		
		
		Map tokenMap = new HashMap<>();
		tokenMap.put("jti", "ee031859-bb18-4ecb-bf70-12fa3396202d");
		
		psd2Filter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);
		
		Mockito.when(httpServletRequest.getHeader(PSD2Constants.FINANCIAL_ID)).thenReturn("");
		psd2Filter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);
				
		psd2Filter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);
	}
	
	/**
	 * Testdo filter internal exceptions 2.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ServletException the servlet exception
	 */
	@Test
	public void testdoFilterInternalExceptions2() throws IOException, ServletException {
		Mockito.when(httpServletRequest.getHeader(PSD2Constants.CORRELATION_ID)).thenReturn("ba4f73f8-9a60-425b-aed8-2a7ef2509fea");
		Mockito.when(httpServletRequest.getHeader("Authorization")).thenReturn("Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0cHBJbmZvLWxlZ2FsRW50aXR5TmFtZSI6Ik1vbmV5d2lzZSBQdnQuIEx0ZC4iLCJhdWQiOlsiYmFua0FjY291bnRUcmFuc2FjdGlvbnMiXSwiYmFua0lkIjoiMSIsImNvbnNlbnRJZCI6IjY3MTE4MjkxIiwidHBwSW5mby1yZWdpc3RlcmVkSWQiOiIwMDciLCJ1c2VyX25hbWUiOiJkZW1vdXNlciIsInRwcEluZm8tZGlzcGxhdE5hbWUiOiJNb25leXdpc2UiLCJleHAiOjE0OTEwNDQ3MDcsImF1dGhvcml0aWVzIjpbIlJPTEVfTU9ORVlXSVNFIiwiUk9MRV9DVVNUT01FUlMiXSwianRpIjoiZWUwMzE4NTktYmIxOC00ZWNiLWJmNzAtMTJmYTMzOTYyMDJkIiwiY2xpZW50X2lkIjoibW9uZXl3aXNlIiwidHBwSW5mby10cHBUeXBlIjoiYWlzcCJ9.knD_aMYrmVJzQa8Jc5ypOWoPRU5TCkAEGSdhLOpXfqYLNyqB9QyUfIAb2PfnzZbwTRIsJ7T1rFV5l7779ktVKSv1OGw853J_8y3jUa2jfY7sFVnZxS4PLK17VnzaXUhFXT3L5R2PKSWlGYocLZkOb-BW9Go1L_nTzcx0xsCz5TaOPXh3xih_Bduzo15fu5JGMMVQKaBTd0x_YEtHB25cT57cJnbjrqYObMO0hh6WBkbA8fC4xePan3jyZsyjomVuWOGaS4xJBqhEL_TK5fVng7O1SlLHGkQYzRRrbg7pOL7W2AyJGkMV9BcUgIkKw7L5gPceB1IjL3ug4bPN9DRSXQ");
		Mockito.when(httpServletRequest.getHeader(PSD2Constants.FINANCIAL_ID)).thenReturn("finance123");
		Mockito.when(httpServletRequest.getHeader(PSD2Constants.CUSTOMER_IP_ADDRESS)).thenReturn("10.1.1.2");
		Mockito.when(httpServletRequest.getHeader(PSD2Constants.CUSTOMER_LAST_LOGGED_TIME)).thenReturn("10-10-2017T10:10");
		Mockito.when(httpServletResponse.getWriter()).thenReturn(new PrintWriter("servletResponse"));
				
		Map tokenMap = new HashMap<>();
		tokenMap.put("jti", "ee031859-bb18-4ecb-bf70-12fa3396202d");		
		
		psd2Filter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);	
	}	
	
	@Test
	public void testdoFilterInternalExceptionsBearerToken() throws IOException, ServletException {
		Mockito.when(httpServletRequest.getHeader(PSD2Constants.CORRELATION_ID)).thenReturn("ba4f73f8-9a60-425b-aed8-2a7ef2509fea");
		Mockito.when(httpServletRequest.getHeader("Authorization")).thenReturn(null);
		Mockito.when(httpServletRequest.getHeader(PSD2Constants.FINANCIAL_ID)).thenReturn("finance123");
		Mockito.when(httpServletRequest.getHeader(PSD2Constants.CUSTOMER_IP_ADDRESS)).thenReturn("10.1.1.2");
		Mockito.when(httpServletRequest.getHeader(PSD2Constants.CUSTOMER_LAST_LOGGED_TIME)).thenReturn("10-10-2017T10:10");
		Mockito.when(httpServletResponse.getWriter()).thenReturn(new PrintWriter("servletResponse"));
		
		
		Map tokenMap = new HashMap<>();
		tokenMap.put("jti", "ee031859-bb18-4ecb-bf70-12fa3396202d");
		
		psd2Filter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);
		
		Mockito.when(httpServletRequest.getHeader("Authorization")).thenReturn("eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0cHBJbmZvLWxlZ2FsRW50aXR5TmFtZSI6Ik1vbmV5d2lzZSBQdnQuIEx0ZC4iLCJhdWQiOlsiYmFua0FjY291bnRUcmFuc2FjdGlvbnMiXSwiYmFua0lkIjoiMSIsImNvbnNlbnRJZCI6IjY3MTE4MjkxIiwidHBwSW5mby1yZWdpc3RlcmVkSWQiOiIwMDciLCJ1c2VyX25hbWUiOiJkZW1vdXNlciIsInRwcEluZm8tZGlzcGxhdE5hbWUiOiJNb25leXdpc2UiLCJleHAiOjE0OTEwNDQ3MDcsImF1dGhvcml0aWVzIjpbIlJPTEVfTU9ORVlXSVNFIiwiUk9MRV9DVVNUT01FUlMiXSwianRpIjoiZWUwMzE4NTktYmIxOC00ZWNiLWJmNzAtMTJmYTMzOTYyMDJkIiwiY2xpZW50X2lkIjoibW9uZXl3aXNlIiwidHBwSW5mby10cHBUeXBlIjoiYWlzcCJ9.knD_aMYrmVJzQa8Jc5ypOWoPRU5TCkAEGSdhLOpXfqYLNyqB9QyUfIAb2PfnzZbwTRIsJ7T1rFV5l7779ktVKSv1OGw853J_8y3jUa2jfY7sFVnZxS4PLK17VnzaXUhFXT3L5R2PKSWlGYocLZkOb-BW9Go1L_nTzcx0xsCz5TaOPXh3xih_Bduzo15fu5JGMMVQKaBTd0x_YEtHB25cT57cJnbjrqYObMO0hh6WBkbA8fC4xePan3jyZsyjomVuWOGaS4xJBqhEL_TK5fVng7O1SlLHGkQYzRRrbg7pOL7W2AyJGkMV9BcUgIkKw7L5gPceB1IjL3ug4bPN9DRSXQ");
		psd2Filter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);
		
		/*Mockito.when(httpServletRequest.getHeader("Authorization")).thenReturn("Bearer ");
		psd2Filter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);*/
	}
	
	@Test
	public void doFilterInternalwithCorrelationIdNullTest() throws ServletException, IOException{
		when(httpServletRequest.getHeader(PSD2Constants.TOKEN_INTROSPECTION_DATA)).thenReturn("{\"client_id\": \"TestTPP\",\"tppInformation\": {\"tppLegalEntityName\": \"Bank of Ireland TPP - PISP\",\"tppRegisteredId\": \"Q5wqFjpnTAeCtDc1Qx\",\"tppRoles\": [\"PISP\"]},\"scope\": [\"openid\",\"accounts\"],\"seviceParams\":{\"channelId\":\"B365\"},\"user_name\": \"88888888\", \"consentTokenData\":{\"consentId\": \"39032481\"},\"consentType\": \"AISP\",\"requestId\": \"d9d9c3dc-1427-4f03-92a7-7916fcf4cc0d\",\"claims\":{\"accounts\": [\"READTRANSACTIONSBASIC\",\"READBALANCES\",\"READACCOUNTSBASIC\",\"READTRANSACTIONSCREDITS\", \"READTRANSACTIONSDETAIL\",\"READTRANSACTIONSDEBITS\",\"READACCOUNTSDETAIL\"]},\"exp\": 0}");
		when(httpServletResponse.getWriter()).thenReturn(new PrintWriter("servletResponse"));
		when(httpServletRequest.getRequestURI()).thenReturn("abcd/");
		when(httpServletRequest.getHeader(PSD2Constants.FINANCIAL_ID)).thenReturn("finance123");
		when(httpServletRequest.getHeader(PSD2Constants.CUSTOMER_LAST_LOGGED_TIME)).thenReturn("2018-02-27T10:57:07+00:00");
		when(httpServletRequest.getHeader(PSD2Constants.IDEMPOTENCY_KEY)).thenReturn("abcd");
		psd2Filter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);
	}
	
	@Test
	public void doInternalFilterExceptionTest() throws ServletException, IOException{
		when(httpServletResponse.getWriter()).thenReturn(new PrintWriter("servletResponse"));
		psd2Filter.doFilterInternal(null, httpServletResponse, filterChain);
	}
	
	@Test
	public void shouldNotFilter() throws ServletException{
		when(httpServletRequest.getServletPath()).thenReturn("12345");
		psd2Filter.shouldNotFilter(httpServletRequest);
	}
	
	@Test
	public void setClaimsTest(){
		List<String> claims = new ArrayList<>();
		psd2Filter.setClaims(claims);
	}
	
	@Test
	public void testForInternalEndPoint() throws ServletException, IOException {
		ReflectionTestUtils.setField(psd2Filter, "internalEndPoint", "/internal/account-requests");
		when(httpServletRequest.getRequestURI()).thenReturn("/internal/account-requests");
		psd2Filter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);
	}
	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
		httpServletRequest = null;
		requestHeaderAttributes = null;
		httpServletResponse = null;
		filterChain = null;
		loggerUtils = null;
	}
}
