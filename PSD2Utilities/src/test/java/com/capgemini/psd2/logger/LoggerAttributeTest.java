/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.logger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.UUID;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The Class LoggerAttributeTest.
 */
public class LoggerAttributeTest {
	
	/** The logger attribute. */
	private LoggerAttribute loggerAttribute = null;
	
	/** The uuid. */
	UUID uuid = UUID.randomUUID();

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		loggerAttribute = new LoggerAttribute();
		loggerAttribute.setApiId("AccountService");
		loggerAttribute.setCorrelationId("ba4f73f8-9a60-425b-aed8-2a7ef2509fea");
		loggerAttribute.setMessage("Enter {}: Executing loggerAttributetest()");
		loggerAttribute.setTppCID("Moneywise");
		loggerAttribute.setTppLegalEntityName("Moneywise");		
		loggerAttribute.setRequestId(uuid);
		loggerAttribute.setPsuId("psuid");
		loggerAttribute.setCustomerIPAddress("10.1.1.2");
		loggerAttribute.setCustomerLastLoggedTime("10-10-2017T10:10");
		loggerAttribute.setFinancialId("finance123");
		loggerAttribute.setUpstream_api_id("upstream_api_id");
		loggerAttribute.setUpstream_end_time("upstream_end_time");
		loggerAttribute.setUpstream_request_payload("upstream_request_payload");
		loggerAttribute.setUpstream_response_payload("upstream_response_payload");
		loggerAttribute.setUpstream_start_time("upstream_start_time");
		loggerAttribute.setTimeStamp("10-10-2017T10:10");
	}

	/**
	 * Logger attributetest.
	 */
	@Test
	public void loggerAttributetest() {
		assertEquals("AccountService", loggerAttribute.getApiId());
		assertEquals("ba4f73f8-9a60-425b-aed8-2a7ef2509fea", loggerAttribute.getCorrelationId());
		assertEquals("Enter {}: Executing loggerAttributetest()", loggerAttribute.getMessage());
		assertEquals("Moneywise", loggerAttribute.getTppCID());
		assertEquals("Moneywise", loggerAttribute.getTppLegalEntityName());
		assertEquals(uuid, loggerAttribute.getRequestId());
		assertEquals("psuid", loggerAttribute.getPsuId());
		assertEquals("10.1.1.2", loggerAttribute.getCustomerIPAddress());
		assertEquals("10-10-2017T10:10", loggerAttribute.getCustomerLastLoggedTime());
		assertEquals("finance123", loggerAttribute.getFinancialId());
		assertEquals("upstream_api_id", loggerAttribute.getUpstream_api_id());
		assertEquals("upstream_end_time", loggerAttribute.getUpstream_end_time());
		assertEquals("upstream_request_payload", loggerAttribute.getUpstream_request_payload());
		assertEquals("upstream_response_payload", loggerAttribute.getUpstream_response_payload());
		assertEquals("upstream_start_time", loggerAttribute.getUpstream_start_time());
		assertEquals("10-10-2017T10:10", loggerAttribute.getTimeStamp());
	}
	
	/**
	 * Test to string.
	 */
	@Test
	public void testToString(){
		assertNotNull(loggerAttribute.toString());
	}
	
	/**
	 * Test equals.
	 */
	@Test
	public void testEquals(){
		assertTrue(loggerAttribute.equals(loggerAttribute));
	}
	
	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
		loggerAttribute = null;
	}

}
