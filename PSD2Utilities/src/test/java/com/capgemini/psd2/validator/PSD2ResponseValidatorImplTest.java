/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.validator;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.validation.Validator;

import com.capgemini.psd2.config.PSD2UtilityTestConfig;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.validator.impl.PSD2ResponseValidatorImpl;

/**
 * The Class PSD2ValidatorImplTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes=PSD2UtilityTestConfig.class)
public class PSD2ResponseValidatorImplTest {

	/** The psd 2 validator. */
	@InjectMocks
	private PSD2ResponseValidatorImpl psd2Validator;

	/** The mvc validator. */
	@Autowired
	private Validator mvcValidator;

	/**
	 * Before.
	 */
	@Before
	public void before() {
		String name = "validator";
		MockitoAnnotations.initMocks(this);
		ReflectionTestUtils.setField(psd2Validator, name, mvcValidator);
	}

	/**
	 * Test PSD 2 validator with object success.
	 */
	@Test
	public void testPSD2ValidatorWithObjectSuccess() {
		TestObject to = new TestObject();
		to.setName("test");
		psd2Validator.validate(to);
	}

	/**
	 * Test PSD 2 validator with list of object success.
	 */
	@Test
	public void testPSD2ValidatorWithListOfObjectSuccess() {
		TestObject to = new TestObject();
		to.setName("test");
		TestObject[] array = new TestObject[]{to};
		psd2Validator.validate(array);
	}

	/**
	 * Test PSD 2 validator with list of object branch check.
	 */
	@Test
	public void testPSD2ValidatorWithListOfObjectBranchCheck() {

		psd2Validator.validate(null);

		TestObject[] array = new TestObject[0];
		psd2Validator.validate(array);
	}

	/**
	 * Test PSD 2 validator with error.
	 */
	@Test(expected=PSD2Exception.class)
	public void testPSD2ValidatorWithError() {
		try{
			psd2Validator.validate(new TestObject());
		}catch (PSD2Exception e) {
			assertTrue(e.getErrorInfo().getDetailErrorMessage().equals("name : may not be null;"));
			throw e;
		}
	}

	@Test
	public void testPSD2ValidatorEnumWithSuccess() {
		psd2Validator.validateEnum(TestEnum.class, "T1");
	}

	@Test(expected=PSD2Exception.class)
	public void testPSD2ValidatorEnumWithError() {
		try{
			psd2Validator.validateEnum(TestEnum.class, "T3");
		}catch (PSD2Exception e) {
			throw e;
		}
	}
}
