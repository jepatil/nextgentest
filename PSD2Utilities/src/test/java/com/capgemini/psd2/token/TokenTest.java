/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.token;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

/**
 * The Class TokenTest.
 */
public class TokenTest {

	/**
	 * Test.
	 */
	@Test
	public void test() {
		Token token = new Token(); 
//		token.setRequestId("asdafs874-fddf852741");
		ConsentTokenData consentTokenData =new ConsentTokenData();
		consentTokenData.setConsentExpiry("1509348259877L");
		consentTokenData.setConsentId("05509648");
//		token.setConsentTokenData(consentTokenData);
		token.setExp(1509348259877L);
		token.setClient_id("moneywise");
		TppInformationTokenData TppInformationTokenData = new TppInformationTokenData();
		TppInformationTokenData.setTppLegalEntityName("Moneywise Pvt. Ltd");
		TppInformationTokenData.setTppRegisteredId("tpp123");
		Set<String> tppRoles =new HashSet<>();
		tppRoles.add("AISP");
		tppRoles.add("PISP");
		tppRoles.add("CISP");
		TppInformationTokenData.setTppRoles(tppRoles);
		token.setTppInformation(TppInformationTokenData);
		token.setUser_name("demouser");
		
		String scope = "accounts";
		Set<String> scopes = new HashSet<>();
		scopes.add(scope);
		token.setScope(scopes);
	//	token.setJti("83f4d260-baf8-4bbe-bed2-6e492a9cb89c");
		
		Object obj = new Object();
		Set<Object> objects = new HashSet<>();
		objects.add(obj);
		Map<String, Set<Object>> claims = new HashMap<>();
		claims.put("claim", objects);
		//token.setClaims(claims);
		
		//assertEquals(claims, token.getClaims());
		//assertEquals("83f4d260-baf8-4bbe-bed2-6e492a9cb89c", token.getJti());
		assertEquals(scopes, token.getScope());
		//assertEquals("asdafs874-fddf852741", token.getRequestId());
		assertEquals(1509348259877L, token.getExp());
		//assertEquals("1509348259877L", token.getConsentTokenData().getConsentExpiry());
		assertEquals("demouser", token.getUser_name());
		assertEquals("moneywise", token.getClient_id());
		assertEquals("Moneywise Pvt. Ltd", token.getTppInformation().getTppLegalEntityName());
		//assertEquals("05509648", token.getConsentTokenData().getConsentId());
	}

}
