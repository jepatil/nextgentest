/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.aspect;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.filteration.ResponseFilter;
import com.capgemini.psd2.logger.LoggerAttribute;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.mask.DataMask;


/**
 * The Class PSD2AspectTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class PSD2AspectUtilsTest {
	
	/** The data mask. */
	@Mock
	private DataMask dataMask;
	
	/** The logger attribute. */
	@Mock
	private LoggerAttribute loggerAttribute;

	/** The logger utils. */
	@Mock
	private LoggerUtils loggerUtils;

	
	/** The response filter utility. */
	@Mock
	private ResponseFilter responseFilterUtility;
	
	/** The proceeding join point. */
	@Mock
	private ProceedingJoinPoint proceedingJoinPoint;
	
	@Mock
	private JoinPoint joinPoint;
		
	/** The signature. */
	@Mock
	private MethodSignature signature;
	
	/** The aspect. */
	@InjectMocks
	private PSD2AspectUtils aspect = new PSD2AspectUtils();
	
	/** The req header attribute. */
	@Mock
	private RequestHeaderAttributes reqHeaderAttribute;
	
	/**
	 * Before.
	 */
	@Before
	public void before() {
		MockitoAnnotations.initMocks(this);
		
		when(loggerUtils.populateLoggerData(anyString())).thenReturn(loggerAttribute);
		when(proceedingJoinPoint.getSignature()).thenReturn(signature);
		when(signature.getName()).thenReturn("retrieveAccountBalance");
		when(signature.getDeclaringTypeName()).thenReturn("retrieveAccountBalance");
		when(proceedingJoinPoint.getArgs()).thenReturn(new Object[1]);
		when(joinPoint.getSignature()).thenReturn(signature);
		when(signature.getDeclaringType()).thenReturn(String.class);
	}

	/**
	 * Arround logger advice controller test.
	 */
	@Test
	public void methodPayloadAdviceTest() {
		aspect.methodPayloadAdvice(proceedingJoinPoint);
	}
	
	/**
	 * Arround logger advice controller masking test.
	 */
	@Test
	public void methodPayloadAdviceMaskingTest() {
		ReflectionTestUtils.setField(aspect, "payloadLog", true);
		ReflectionTestUtils.setField(aspect, "maskPayloadLog", true);
		ReflectionTestUtils.setField(aspect, "maskPayload", true);
		aspect.methodPayloadAdvice(proceedingJoinPoint);
		
		ReflectionTestUtils.setField(aspect, "maskPayloadLog", false);
		aspect.methodPayloadAdvice(proceedingJoinPoint);
	}
	
	/**
	 * Arround logger advice service impl test.
	 */
	@Test
	public void methodAdviceImplTest() {
		aspect.methodAdvice(proceedingJoinPoint);
	}
	
	/**
	 * Arround logger advice service impl PSD 2 exception failure test.
	 *
	 * @throws Throwable the throwable
	 */
	@Test(expected = PSD2Exception.class)
	public void methodAdviceImplPSD2ExceptionFailureTest() throws Throwable{
		when(proceedingJoinPoint.proceed()).thenThrow(PSD2Exception.populatePSD2Exception(ErrorCodeEnum.TECHNICAL_ERROR));
		aspect.methodAdvice(proceedingJoinPoint);
	}
	
	/**
	 * Arround logger advice service impl throwable failure test.
	 *
	 * @throws Throwable the throwable
	 */
	@Test(expected = PSD2Exception.class)
	public void methodAdviceImplThrowableFailureTest() throws Throwable{
		when(proceedingJoinPoint.proceed()).thenThrow(new RuntimeException("Test"));
		aspect.methodAdvice(proceedingJoinPoint);
	}
	
	
	/**
	 * Arround logger advice controller throwable failure test.
	 *
	 * @throws Throwable the throwable
	 */
	@Test(expected = PSD2Exception.class)
	public void methodPayloadAdviceThrowableFailureTest() throws Throwable{
		when(proceedingJoinPoint.proceed()).thenThrow(new RuntimeException("Test"));
		aspect.methodPayloadAdvice(proceedingJoinPoint);
	}
	
	/**
	 * Arround logger advice controller PSD 2 exception failure test.
	 *
	 * @throws Throwable the throwable
	 */
	@Test(expected = PSD2Exception.class)
	public void methodPayloadAdvicePSD2ExceptionFailureTest() throws Throwable{
		when(proceedingJoinPoint.proceed()).thenThrow(PSD2Exception.populatePSD2Exception(ErrorCodeEnum.TECHNICAL_ERROR));
		aspect.methodPayloadAdvice(proceedingJoinPoint);
	}
	
	@Test
	public void methodPayloadAdviceFalsePayloadLog() {
		ReflectionTestUtils.setField(aspect, "payloadLog", false);
		ReflectionTestUtils.setField(aspect, "maskPayloadLog", true);
		ReflectionTestUtils.setField(aspect, "maskPayload", true);
		aspect.methodPayloadAdvice(proceedingJoinPoint);
		
	}
	
	@Test(expected = PSD2Exception.class)
	public void afterThrowingAspectPSD2ExceptionTest() throws Throwable{
		aspect.throwExceptionOnJsonBinding(joinPoint, new Throwable());
	}
	
	
	
}
