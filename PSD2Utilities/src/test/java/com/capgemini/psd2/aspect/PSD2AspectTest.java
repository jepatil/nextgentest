package com.capgemini.psd2.aspect;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;

import org.aspectj.lang.ProceedingJoinPoint;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class PSD2AspectTest {
	/** The aspect. */
	@InjectMocks
	private PSD2Aspect aspect = new PSD2Aspect();
	
	/** The proceeding join point. */
	@Mock
	private ProceedingJoinPoint proceedingJoinPoint;
	
	@Mock
	private PSD2AspectUtils aspectUtils;
	
	@Before
	public void before() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testArroundLoggerAdviceService(){
		doReturn(new Object()).when(aspectUtils).methodAdvice(proceedingJoinPoint);
		aspect.arroundLoggerAdviceService(proceedingJoinPoint);
	}
	
	@Test
	public void testArroundLoggerAdviceController(){
		doReturn(new Object()).when(aspectUtils).methodPayloadAdvice(proceedingJoinPoint);
		aspect.arroundLoggerAdviceController(proceedingJoinPoint);
	}
	
	@Test
	public void testThrowExcpetionOnJsonBinding(){
		RuntimeException ex = new RuntimeException("Test");
		doNothing().when(aspectUtils).throwExceptionOnJsonBinding(proceedingJoinPoint,ex);
		aspect.throwExcpetionOnJsonBinding(proceedingJoinPoint,ex);
	}
}
