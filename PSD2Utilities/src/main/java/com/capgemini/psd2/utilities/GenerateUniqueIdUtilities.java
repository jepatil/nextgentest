/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.utilities;

import java.util.UUID;

import org.apache.commons.lang3.RandomStringUtils;

/**
 * The Class GenerateUniqueIdUtilities.
 */
public final class GenerateUniqueIdUtilities {
	
	
	/**
	 * Instantiates a new generate unique id utilities.
	 */
	private GenerateUniqueIdUtilities(){
		
	}
	
	/**
	 * Gets the unique id.
	 *
	 * @return the unique id
	 */
	public static UUID getUniqueId(){
		return UUID.randomUUID();
	}
	
	/**
	 * Generate random unique ID.
	 *
	 * @return the string
	 */
	public static String generateRandomUniqueID(){
		String uniqueNumber = RandomStringUtils.random(8,false,true);
		return uniqueNumber;
	}	
	
	public static String generateRandomClientSeceret(){
		String uniqueNumber = RandomStringUtils.random(32,true,true);
		return uniqueNumber;
	}
}
