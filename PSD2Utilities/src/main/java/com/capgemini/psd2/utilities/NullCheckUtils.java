/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.utilities;

import org.springframework.util.StringUtils;

/**
 * The Class NullCheckUtils.
 */
public final class NullCheckUtils {
	
	/**
	 * Instantiates a new null check utils.
	 */
	private NullCheckUtils(){
		
	}

	/**
	 * Checks if is null or empty.
	 *
	 * @param obj the obj
	 * @return true, if is null or empty
	 */
	public static boolean isNullOrEmpty(Object obj){
		boolean isEmpty = Boolean.FALSE;
		if(obj == null || (obj instanceof String && (!StringUtils.hasText((String)obj)))){
			isEmpty = Boolean.TRUE;
		}
		return isEmpty;
	}
}
