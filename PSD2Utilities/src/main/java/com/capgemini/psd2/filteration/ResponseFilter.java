/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.filteration;

import java.util.List;

/**
 * The Interface ResponseFilter.
 */
public interface ResponseFilter {

	/**
	 * Filter response.
	 *
	 * @param <T> the generic type
	 * @param filterObject the filter object
	 * @param methodName the method name
	 * @return the t
	 */
	public <T> T filterResponse(T filterObject, String methodName);
	
	/**
	 * Filter response.
	 *
	 * @param <T> the generic type
	 * @param filterObject the filter object
	 * @param methodName the method name
	 * @param claims the claims
	 * @return the t
	 */
	public <T> T filterResponse(T filterObject, String methodName, List<String> claims);
	
	/**
	 * Filter message.
	 *
	 * @param <T> the generic type
	 * @param filterObject the filter object
	 * @param filter the filter
	 * @return the t
	 */
	public <T> T filterMessage(T filterObject, String filter);

}
