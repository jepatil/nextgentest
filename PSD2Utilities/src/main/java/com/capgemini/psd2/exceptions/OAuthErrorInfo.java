package com.capgemini.psd2.exceptions;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class OAuthErrorInfo extends ErrorInfo {
	
	public OAuthErrorInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public OAuthErrorInfo(String statusCode) {
		super(statusCode);
		this.statusCode = statusCode;
	}
	
	public OAuthErrorInfo(String statusCode, String error, String error_description) {
		super(statusCode);
		this.statusCode = statusCode;
		this.error = error;
		this.setError_description(error_description);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The error code. */
	private String error;
	
	/** The detail error message. */
	private String error_description;
	
	@JsonIgnore
	private String statusCode;

	public String getError() {
		if(sendErrorPayload!=null && !sendErrorPayload)
			return null;
		else
			return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getError_description() {
		if(sendErrorPayload!=null && !sendErrorPayload)
			return null;
		else
		    return error_description;
	}

	public void setError_description(String error_description) {
		if(errorMap!=null && errorMap.get(statusCode)!=null){
			this.error_description =errorMap.get(statusCode).toString();
		} else{
			this.error_description = error_description;
		}
	}
    @Override
	public String getStatusCode() {
		return statusCode;
	}
    @Override
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
}
