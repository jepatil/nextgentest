/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.exceptions;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * The Class PSD2ExceptionHandler.
 */
@ControllerAdvice
public class PSD2ExceptionHandler extends ResponseEntityExceptionHandler{
	
	 /**
 	 * Handle invalid request.
 	 *
 	 * @param e the e
 	 * @param request the request
 	 * @return the response entity
 	 */
 	@ExceptionHandler({Exception.class })
	 protected ResponseEntity<Object> handleInvalidRequest(Exception e, WebRequest request){
		ErrorInfo errorInfo = null;
		HttpStatus status = null;
		String errorResponse = null;
		HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
		if(e != null && e instanceof HttpClientErrorException) {
			errorResponse = ((HttpClientErrorException)e).getResponseBodyAsString();
			status = ((HttpClientErrorException) e).getStatusCode();
			
			errorInfo = new ErrorInfo(String.valueOf(status), errorResponse, errorResponse, status.toString());
						
			/*errorInfo.setErrorCode(String.valueOf(status));
			errorInfo.setErrorMessage(errorResponse);
			errorInfo.setDetailErrorMessage(errorResponse);*/
		}
		else if(e != null && e instanceof PSD2Exception) {
			errorInfo = ((PSD2Exception)e).getErrorInfo();
			status = HttpStatus.valueOf(Integer.parseInt(errorInfo.getStatusCode()));
		}	
        if(errorInfo == null){
        	status = HttpStatus.INTERNAL_SERVER_ERROR;
        	
			errorInfo = new ErrorInfo(String.valueOf(status), errorResponse, errorResponse, status.toString());
			/*errorInfo.setErrorCode(String.valueOf(status));*/
			
		}
        return handleExceptionInternal(e, errorInfo, headers, status, request);
	 }
}
