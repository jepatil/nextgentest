/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.validator.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.Validator;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.validator.PSD2Validator;

/**
 * The Class PSD2ValidatorImpl.
 */
@Component
@Qualifier("PSD2ResponseValidator")
public class PSD2ResponseValidatorImpl implements PSD2Validator {

	/** The validator. */
	@Autowired
	@Qualifier("mvcValidator")
	private Validator validator;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.capgemini.psd2.validator.PSD2Validator#validate(java.lang.Object[])
	 */
	@Override
	public void validate(Object[] inputObjects) {
		if (inputObjects != null && inputObjects.length > 0)
			for (Object argument : inputObjects) {
				this.validate(argument);
			}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.capgemini.psd2.validator.PSD2Validator#validate(java.lang.Object)
	 */
	@Override
	public void validate(Object input) {
		Errors errors = new BeanPropertyBindingResult(input, input.getClass().getName());
		validator.validate(input, errors);
		StringBuilder errorString = new StringBuilder();
		if (errors.hasErrors()) {
			for (Object error : errors.getAllErrors()) {
				if (error instanceof FieldError) {
					FieldError fieldError = (FieldError) error;
					errorString.append(fieldError.getField());
					errorString.append(PSD2Constants.COLON);
					errorString.append(fieldError.getDefaultMessage());
					errorString.append(PSD2Constants.SEMI_COLON);
				}
			}

			throw PSD2Exception.populatePSD2Exception(errorString.toString(), ErrorCodeEnum.VALIDATION_ERROR_OUTPUT);
		}
	}

	
	@Override
	public <T extends Enum<T>> void validateEnum(Class<T> enumerator, String value)
	{ 
		boolean error = Boolean.FALSE;
	    for (T c : enumerator.getEnumConstants()) {
	        if (c.name().equals(value)) {
	            error = true;
	        }
	    }
	    if(!error)
	    	throw PSD2Exception.populatePSD2Exception(value + " Enum Not Valid",ErrorCodeEnum.VALIDATION_ERROR_OUTPUT);
	}
	
}
