package com.capgemini.psd2.security.saas.cancel.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;

import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.AccountRequestPOSTResponse;
import com.capgemini.psd2.aisp.domain.Data1;
import com.capgemini.psd2.aisp.domain.Data1.StatusEnum;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.adapter.PaymentSetupAdapterHelper;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse;
import com.capgemini.psd2.scaconsenthelper.config.PFConfig;
import com.capgemini.psd2.scaconsenthelper.config.helpers.SCAConsentHelper;
import com.capgemini.psd2.scaconsenthelper.constants.PFConstants;
import com.capgemini.psd2.scaconsenthelper.constants.SCAConsentHelperConstants;
import com.capgemini.psd2.scaconsenthelper.models.PFInstanceData;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.scaconsenthelper.services.SCAConsentHelperService;
import com.capgemini.psd2.security.constants.PSD2SecurityConstants;
import com.capgemini.psd2.security.saas.model.IntentTypeEnum;
import com.capgemini.psd2.utilities.JSONUtilities;

@RestController
public class SaaSCancelController {

	
	@Autowired
	private PaymentSetupAdapterHelper paymentSetupAdapterHelper;
	
	@Autowired
	private SCAConsentHelperService helperService;
	
	@Autowired
	private AispConsentAdapter aispConsentAdapter;

	@Autowired
	@Qualifier(value = "accountRequestMongoDbAdaptor")
	private AccountRequestAdapter accountRequestAdapter;

	@Autowired
	private PFConfig pfConfig;
	
	@Value("${spring.application.name}")
	private String applicationName;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private HttpServletResponse response;
	
	
	@RequestMapping(value = "/cancel", method = RequestMethod.PUT)
	public String cancelSetUp(ModelAndView model, @RequestParam String oAuthUrl, @RequestParam String serverErrorFlag,
			@RequestParam String channelId)
			throws ParseException {
		String redirectURI = null;
		redirectURI = pfConfig.getResumePathBaseURL().concat(oAuthUrl);
		PickupDataModel intentData = (PickupDataModel) request.getAttribute(SCAConsentHelperConstants.INTENT_DATA);
		if (serverErrorFlag != null && !serverErrorFlag.isEmpty() && Boolean.valueOf(serverErrorFlag)) {
			redirectURI = UriComponentsBuilder.fromHttpUrl(redirectURI).queryParam(PFConstants.REF, null).toUriString();
		} else{
			PFInstanceData pfInstanceData = new PFInstanceData();
			pfInstanceData.setPfInstanceId(pfConfig.getScainstanceId());
			pfInstanceData.setPfInstanceUserName(pfConfig.getScainstanceusername());
			pfInstanceData.setPfInstanceUserPwd(pfConfig.getScainstancepassword());
			redirectURI = helperService.cancelJourney(redirectURI, pfInstanceData);
		}
		if(channelId == null || channelId.trim().isEmpty()){
			throw PSD2Exception.populatePSD2Exception("Channel Id is not provided",ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
		}
		Map<String,String> paramsMap = new HashMap<String,String>();
		paramsMap.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, channelId);
		cancelSetupRequest(intentData, paramsMap);
		model.addObject(PSD2SecurityConstants.REDIRECT_URI_MODEL_ATTRIBUTE, redirectURI);
		model.addObject(PSD2Constants.APPLICATION_NAME,applicationName);
		SCAConsentHelper.invalidateCookie(response);
		return JSONUtilities.getJSONOutPutFromObject(model);
		
	}

	private void cancelSetupRequest(PickupDataModel intentData, Map<String, String> paramsMap) throws ParseException {

		if (intentData.getIntentTypeEnum().getIntentType()
				.equalsIgnoreCase(IntentTypeEnum.AISP_INTENT_TYPE.getIntentType())) {
			
		
			AispConsent consent = null;
			AccountRequestPOSTResponse accountSetupResponse = accountRequestAdapter.getAccountRequestGETResponse(intentData.getIntentId());
			
			if(accountSetupResponse.getData().getStatus().equals(Data1.StatusEnum.AUTHORISED)){
				consent = aispConsentAdapter.retrieveConsentByAccountRequestId(intentData.getIntentId(), ConsentStatusEnum.AUTHORISED);
				if(consent == null){
					throw PSD2Exception.populatePSD2Exception("Consent is not in correct status",ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
					
				}
			}
			if(consent == null) {
				accountRequestAdapter.updateAccountRequestResponse(intentData.getIntentId(), StatusEnum.REJECTED);
			}
			
		} else if (intentData.getIntentTypeEnum().getIntentType()
				.equalsIgnoreCase(IntentTypeEnum.PISP_INTENT_TYPE.getIntentType())) {
			paymentSetupAdapterHelper.updatePaymentSetupStatus(intentData.getIntentId(),
					PaymentSetupResponse.StatusEnum.REJECTED, paramsMap);
		}
	}	
}