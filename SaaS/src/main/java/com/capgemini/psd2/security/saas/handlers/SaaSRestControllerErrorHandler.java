package com.capgemini.psd2.security.saas.handlers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.scaconsenthelper.constants.PFConstants;
import com.capgemini.psd2.scaconsenthelper.constants.SCAConsentHelperConstants;
import com.capgemini.psd2.security.constants.PSD2SecurityConstants;
import com.capgemini.psd2.ui.content.utility.controller.UIStaticContentUtilityController;
import com.capgemini.psd2.utilities.DateUtilites;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.ValidationUtility;

@ControllerAdvice(basePackages = {"com.capgemini.psd2.security.saas.cancel.controller"})
@Order(Ordered.HIGHEST_PRECEDENCE+1)
public class SaaSRestControllerErrorHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(SaaSRestControllerErrorHandler.class);
	
	@Autowired
	private RequestHeaderAttributes requestHeaderAttributes;
	
	@Autowired
	private UIStaticContentUtilityController uiController;
	
	@ResponseBody
	@ExceptionHandler(PSD2Exception.class)
	public String handleCustomException(PSD2Exception ex, HttpServletRequest request,
            HttpServletResponse response) throws JSONException  {
		LOGGER.error("{\"Exception\":\"{}\",\"timestamp\":\"{}\",\"correlationId\": \"{}\",\"ErrorDetails\":{}}"," com.capgemini.psd2.security.saas.handlers.SaaSRestControllerErrorHandler.handleCustomException()",DateUtilites.generateCurrentTimeStamp(),requestHeaderAttributes.getCorrelationId(), ex.getErrorInfo());
		if(LOGGER.isDebugEnabled()){
			LOGGER.error("{\"Exception\":\"{}\",\"timestamp\":\"{}\",\"correlationId\": \"{}\",\"ErrorDetails\":\"{}\"}"," com.capgemini.psd2.security.saas.handlers.SaaSRestControllerErrorHandler.handleCustomException()",DateUtilites.generateCurrentTimeStamp(),requestHeaderAttributes.getCorrelationId(), ex.getStackTrace());
		}
		response.setStatus(Integer.parseInt(ex.getErrorInfo().getStatusCode()));
		String errorModel = errorHandler(request,ex);
		return errorModel;
	}
	
	
	private String errorHandler(HttpServletRequest request,PSD2Exception pe) throws JSONException {

		//Model model = null;
		JSONObject model = new JSONObject();
		
		String statusCode = null;

		ErrorInfo errorInfo = pe.getErrorInfo();

		String resumePath = (String) request.getAttribute(PFConstants.RESUME_PATH);

		boolean serverError = Boolean.FALSE;

		if (resumePath == null || resumePath.isEmpty()) {
			if (request.getParameter(PFConstants.RESUME_PATH) != null
					&& !request.getParameter(PFConstants.RESUME_PATH).isEmpty()) {
				resumePath = request.getParameter(PFConstants.RESUME_PATH);
			} else if (request.getParameter(SCAConsentHelperConstants.OAUTH_URL_PARAM) != null
					&& !request.getParameter(SCAConsentHelperConstants.OAUTH_URL_PARAM).isEmpty()) {
				resumePath = request.getParameter(SCAConsentHelperConstants.OAUTH_URL_PARAM);
			}
		}

		String currentCorrId =  requestHeaderAttributes.getCorrelationId();
		
		if(currentCorrId == null || currentCorrId.isEmpty()){
			currentCorrId = request.getParameter(PSD2Constants.CO_RELATION_ID);
			ValidationUtility.isValidUUID(currentCorrId);
		}

		if (errorInfo != null) {
			model.put(SCAConsentHelperConstants.EXCEPTION, errorInfo);
			errorInfo.setDetailErrorMessage(null);
			statusCode = errorInfo.getStatusCode();
		}

		if (statusCode != null && !statusCode.isEmpty()) {
			serverError = statusCode.equalsIgnoreCase(Integer.toString(HttpStatus.INTERNAL_SERVER_ERROR.value()))
					|| Integer.valueOf(statusCode).intValue() > HttpStatus.INTERNAL_SERVER_ERROR.value();
		}
		
		model.put(SCAConsentHelperConstants.EXCEPTION, new JSONObject(JSONUtilities.getJSONOutPutFromObject(pe.getErrorInfo())).toString());

		model.put(PSD2Constants.SERVER_ERROR_FLAG_ATTR, serverError);
		
		model.put(PSD2Constants.CO_RELATION_ID, currentCorrId);

		model.put(PSD2SecurityConstants.REDIRECT_URI_MODEL_ATTRIBUTE, resumePath);
		model.put(PSD2Constants.JS_MSG,
				uiController.retrieveUiParamValue(PSD2Constants.ERROR_MESSAGE, PSD2Constants.JS_MSG));
		
		return model.toString();		
	}	
}