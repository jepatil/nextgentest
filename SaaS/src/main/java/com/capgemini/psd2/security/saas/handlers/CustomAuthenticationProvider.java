/*************************************************************************
 * 
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 */

package com.capgemini.psd2.security.saas.handlers;

import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.security.AdapterAuthenticationException;
import com.capgemini.psd2.adapter.exceptions.security.SecurityAdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.security.constants.AdapterSecurityConstants;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.authentication.adapter.AuthenticationAdapter;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.rest.client.helpers.HeaderHelper;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.psd2.scaconsenthelper.config.PFConfig;
import com.capgemini.psd2.scaconsenthelper.constants.OIDCConstants;
import com.capgemini.psd2.scaconsenthelper.constants.PFConstants;
import com.capgemini.psd2.scaconsenthelper.constants.SCAConsentHelperConstants;
import com.capgemini.psd2.scaconsenthelper.models.DropOffRequest;
import com.capgemini.psd2.scaconsenthelper.models.DropOffResponse;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.security.constants.PSD2SecurityConstants;
import com.capgemini.psd2.security.exceptions.PSD2AuthenticationException;
import com.capgemini.psd2.security.exceptions.SCAConsentErrorCodeEnum;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

	
	@Autowired
	@Qualifier("authAdapter")
	private AuthenticationAdapter authenticaitonAdapter;

	@Autowired
	private AispConsentAdapter aispConsentAdapter;
	
	@Autowired
	private RestClientSync restClientSyncImpl;
	
	@Autowired
	private PFConfig pfConfig;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private RequestHeaderAttributes requestHeaderAttributes;
	
	@Value("${app.regex.username}")
	private String userNameRegex;
	
	@Value("${app.regex.password}")
	private String passwordRegex;
	
	/**
	 * Its implemented method of AuthenticationProvider to perform MFA of the
	 * bank's customer, one by normal authentication with Ping Dir.(LDAP) & Ping
	 * ID (Mobile Notification).
	 */
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		try {
			if (NullCheckUtils.isNullOrEmpty(authentication.getPrincipal())
					|| NullCheckUtils.isNullOrEmpty(authentication.getCredentials()) || !validUserAndPassword(authentication)) {
				throw PSD2AuthenticationException.populateAuthenticationFailedException(
						SCAConsentErrorCodeEnum.VALIDATION_ERROR, "UserName or Password is blank or invalid");
			}
			AispConsent consent = aispConsentAdapter.retrieveConsentByAccountRequestId(requestHeaderAttributes.getIntentId(),
					ConsentStatusEnum.AUTHORISED);
			if(consent != null && !consent.getPsuId().equalsIgnoreCase((String)authentication.getPrincipal())){
				throw PSD2AuthenticationException.populateAuthenticationFailedException(
						SCAConsentErrorCodeEnum.UN_AUTHORIZED_USER_REFRESH_TOKEN_FLOW);
			}
			authentication = authenticateUser(authentication);
			SimpleDateFormat sdf = new SimpleDateFormat(PFConstants.DATE_FORMAT);
			String currentDate = sdf.format(new Date());
			dropOff(authentication.getPrincipal().toString(),currentDate);
		} catch (PSD2AuthenticationException e) {
			throw e;
		}
		catch(PSD2Exception e){
			throw AdapterAuthenticationException.populateAuthenticationFailedException(e.getErrorInfo());
		}
		catch(Exception e){
			throw AdapterAuthenticationException.populateAuthenticationFailedException(e.getMessage(),SecurityAdapterErrorCodeEnum.TECHNICAL_ERROR);
		}
		
		return authentication;
	}

	private Authentication authenticateUser(Authentication authentication) {
		Map<String,String> headers = new HashMap<String,String>();
		headers.put(PSD2Constants.CORRELATION_ID, requestHeaderAttributes.getCorrelationId());
		headers.put(AdapterSecurityConstants.USER_HEADER, authentication.getPrincipal().toString());
		headers.put(AdapterSecurityConstants.CHANNELID_HEADER, request.getParameter(PSD2SecurityConstants.CHANNEL_ID));
		return authenticaitonAdapter.authenticate(authentication,headers);
	}

	/**
	 * 
	 */
	@Override
	public boolean supports(Class<?> authentication) {
		return true;
	}

	private void dropOff(String userId,String authenticationTime) {
		
		
		
		String correlationId = requestHeaderAttributes.getCorrelationId();
		
		String channelId = request.getParameter(PSD2SecurityConstants.CHANNEL_ID);
		
		RequestInfo requestInfo = new RequestInfo();

		requestInfo.setUrl(pfConfig.getDropOffURL());
		
		DropOffRequest dropOffDataModel = new DropOffRequest();
		dropOffDataModel.setUsername(userId);
		dropOffDataModel.setAcr(OIDCConstants.SCA_ACR);
		dropOffDataModel.setChannel_id(channelId);
		dropOffDataModel.setCorrelationId(correlationId);
		dropOffDataModel.setClient_id(requestHeaderAttributes.getTppCID());
		dropOffDataModel.setScope(requestHeaderAttributes.getScopes());
		
		dropOffDataModel.setAuthnInst(authenticationTime);

		HttpHeaders httpHeaders = HeaderHelper.populateJSONContentTypeHeader();

		String credentials = pfConfig.getScainstanceusername().concat(":").concat(pfConfig.getScainstancepassword());

		String encoderString = Base64.getEncoder().encodeToString(credentials.getBytes());

		httpHeaders.add(SCAConsentHelperConstants.AUTHORIZATION_HEADER, SCAConsentHelperConstants.BASIC+" ".concat(encoderString));

		httpHeaders.add(PFConstants.PING_INSTANCE_ID, pfConfig.getScainstanceId());

		String jsonResponse = restClientSyncImpl.callForPost(requestInfo, dropOffDataModel, String.class, httpHeaders);
		
		DropOffResponse dropOffResponse = JSONUtilities.getObjectFromJSONString(jsonResponse,DropOffResponse.class);
		
		request.setAttribute(PFConstants.REF, dropOffResponse.getRef());
	}
	
	private boolean validUserAndPassword(Authentication authentication) {
		Boolean isValid = Boolean.FALSE;
		Pattern usenamePattern = Pattern.compile(userNameRegex);
		Pattern passPattern = Pattern.compile(passwordRegex);
		Matcher m = usenamePattern.matcher((String) authentication.getPrincipal());
		Matcher m1 = passPattern.matcher((String) authentication.getCredentials());
		if(m.matches() && m1.matches()) {
			isValid = true;
		}
		return isValid;
	}
}
