package com.capgemini.psd2.security.saas.test.handlers;

import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.core.Authentication;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.capgemini.psd2.authentication.adapter.AuthenticationAdapter;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.psd2.scaconsenthelper.config.PFConfig;
import com.capgemini.psd2.scaconsenthelper.models.DropOffRequest;
import com.capgemini.psd2.security.exceptions.PSD2AuthenticationException;
import com.capgemini.psd2.security.saas.handlers.CustomAuthenticationProvider;


@RunWith(SpringJUnit4ClassRunner.class)
public class CustomAuthenticationProviderTest {

	@Mock
	private AuthenticationAdapter authenticaitonAdapter;

	@Mock
	private RequestHeaderAttributes requestHeaderAttributes;
	
	@Mock
	private PFConfig pfConfig;

	@Mock
	private Authentication mockAuthentication;
	
	@Mock
	private HttpServletRequest request;
	
	@Mock
	private RequestInfo requestInfo;
	
	@Mock
	private HttpHeaders httpHeaders;
	
	@Mock
	private DropOffRequest dropOffDataModel;
	
	@Mock
	private RestClientSync restClientSyncImpl;

	@InjectMocks
	private CustomAuthenticationProvider authenticationProvider = new CustomAuthenticationProvider();

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		MockHttpServletRequest request = new MockHttpServletRequest();
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
	}
	
	@Test(expected=PSD2AuthenticationException.class)
	public void authenticateTestForAISPFlow() {
		String jsonResponse = "{\"claims\":\"SETUP\",\"scope\":\"accounts\",\"username\":\"demo\",\"channel_id\":\"APIChannel\",\"client_id\":\"6443e15975554bce8099e35b88b40465\",\"correlationId\":\"12345\"}";
		when(requestHeaderAttributes.getCorrelationId()).thenReturn("12345");
		when(mockAuthentication.getCredentials()).thenReturn(new Object());
		when(mockAuthentication.getPrincipal()).thenReturn(new Object());
		when(authenticaitonAdapter.authenticate(anyObject(), anyMapOf(String.class, String.class)))
		.thenReturn(mockAuthentication);
		when(pfConfig.getScainstanceusername()).thenReturn("abcd");
		when(pfConfig.getScainstancepassword()).thenReturn("1234");
		when(restClientSyncImpl.callForPost(requestInfo, dropOffDataModel, String.class, httpHeaders)).thenReturn(jsonResponse);
		authenticationProvider.authenticate(mockAuthentication);
	}
	
	@Test(expected=PSD2AuthenticationException.class)
	public void authenticateTestForAISPFlowWithPrincipalNull() {

		when(requestHeaderAttributes.getCorrelationId()).thenReturn("12345");
		when(mockAuthentication.getCredentials()).thenReturn(new Object());
		when(mockAuthentication.getPrincipal()).thenReturn(null);
		authenticationProvider.authenticate(mockAuthentication);
	}
	

	/**
	 * Tear down.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@After
	public void tearDown() throws Exception {
		authenticationProvider = null;
	}

}
