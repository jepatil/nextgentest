package com.capgemini.psd2.pisp.validation;


import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Currency;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.iban4j.CountryCode;
import org.iban4j.IbanUtil;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class PispUtilities {	
	
	private static ObjectMapper objectMapper = null;
	
	private static final Pattern bbanPattern = Pattern.compile("^[A-Z]{4}\\d{14}$");
	static{
		objectMapper = new ObjectMapper();
	}
	
	private PispUtilities() {
		
	}
	
		
	public static boolean isEmpty(String data){
		boolean status = false;
		if(data == null || data.trim().length() == 0)
			status = true;
		
		return status;
	}
		
	
	public static void validateIBAN(String iban) {
		
		try{ 
			IbanUtil.validate(iban.trim());	
		}catch(Exception exception){
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_IBAN_IDENTIFICATION_NOT_VALID);
		}
		
	}
	
	public static void validateISOCountry(String countryCode){
		if(CountryCode.getByCode(countryCode) == null)
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_COUNTRY_CODE_NOT_VALID);	

	}
	
	
	public static void validateBBAN(String bban) {
	 
		try{
		      Matcher matcher = bbanPattern.matcher(bban);
		      if (! matcher.find())
		    		throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_BBAN_IDENTIFICATION_NOT_VALID);		      
		}catch(Exception exception){
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_BBAN_IDENTIFICATION_NOT_VALID);
		}
	}
		
	public static boolean isValidCurrency(String currencyCode){
		
			if(isEmpty(currencyCode))
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_INVALID_CURRENCY);
			
			try{
				Currency.getInstance(currencyCode.trim());				
				return true;
			}catch(Exception exception){
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_INVALID_CURRENCY);
			}
	}
	/*
	 * This function is used to convert Hours,Minutes,Seconds to Milliseconds
	 */
	public static long getMilliSeconds(String idempotencyDuration){
		 long duration = 0L;
		 if(isEmpty(idempotencyDuration))	
			 throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_IDEMPOTENCY_DURATION_INCORRECT_FORMAT);	
		 
		 try{
				String inputData = idempotencyDuration.toUpperCase();
				if(inputData.contains("H")){
					inputData = inputData.replaceAll("H", "");
					duration = (long)60*60*1000;					
				}else if(inputData.contains("M")){
					inputData = inputData.replaceAll("M", "");
					duration = (long)60*1000;					
				}else if(inputData.contains("S")){
					inputData = inputData.replaceAll("S", "");
					duration = 1000;					
				}
				
				return Long.parseLong(inputData) * duration; 	
				
	    }catch(NumberFormatException exception){
	    	throw PSD2Exception.populatePSD2Exception(exception.getMessage(),ErrorCodeEnum.PISP_IDEMPOTENCY_DURATION_INCORRECT_FORMAT);	
	    }
	}
	
	public static ObjectMapper getObjectMapper(){
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

		return objectMapper;
	}
	
	public static String getCurrentDateInISOFormat() {
		String isoDateTime = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssZ").format(ZonedDateTime.now());
		isoDateTime = isoDateTime.substring(0, 22).concat(":").concat(isoDateTime.substring(22, 24));
		return isoDateTime;
	}

}
