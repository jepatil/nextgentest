package com.capgemini.psd2.authentication.application.boi.fs;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.adapter.security.constants.AdapterSecurityConstants;
import com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.AuthenticationApplicationFoundationServiceAdapter;
import com.capgemini.psd2.logger.PSD2Constants;

@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
@ComponentScan("com.capgemini.psd2")

public class AuthenticationApplicationFoundationServiceTestProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(AuthenticationApplicationFoundationServiceTestProjectApplication.class, args);
	}
}

@RestController
@ResponseBody
class TestAuthenticationApplicationFSAdapter {

	@Autowired
	AuthenticationApplicationFoundationServiceAdapter adapter;

	@RequestMapping("/testAuthenticationApplicationAdapter")
	public String getResponse() {

		PrincipalImpl principal = new PrincipalImpl("boi123");
		CredentialsImpl credentials = new CredentialsImpl("1234");
		AuthenticationImpl authentication = new AuthenticationImpl(principal, credentials);

		Map<String, String> params = new HashMap<String, String>();

		params.put(AdapterSecurityConstants.USER_HEADER, "test");
		params.put(AdapterSecurityConstants.CHANNELID_HEADER, "BOL");
		params.put(PSD2Constants.CORRELATION_ID, "test");

		adapter.authenticate(authentication, params);

		return "Successfully autheticated";

	}
}