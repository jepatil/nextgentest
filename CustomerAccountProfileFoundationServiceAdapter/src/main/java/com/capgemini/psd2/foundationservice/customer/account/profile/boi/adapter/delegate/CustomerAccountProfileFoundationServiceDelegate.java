package com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.delegate;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.utility.AdapterUtility;
import com.capgemini.psd2.aisp.domain.AccountGETResponse;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.ChannelProfile;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.transformer.CustomerAccountProfileFoundationServiceTransformer;
import com.capgemini.psd2.fraudsystem.domain.PSD2CustomerInfo;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.utilities.NullCheckUtils;

/**
 * The class CustomerAccountProfileFoundationServiceDelegate
 *
 */
@Component
public class CustomerAccountProfileFoundationServiceDelegate {

	/** The user in req header. */
	@Value("${foundationService.userInReqHeader:#{X-BOI-USER}}")
	private String userInReqHeader;

	/** The channel in req header. */
	@Value("${foundationService.channelInReqHeader:#{X-BOI-CHANNEL}}")
	private String channelInReqHeader;

	/** The workstation in req header. */
	@Value("${foundationService.platformInReqHeader:#{X-BOI-WORKSTATION}}")
	private String platformInReqHeader;

	/** The correlation req header. */
	@Value("${foundationService.correlationReqHeader:#{X-BOI-WORKSTATION}}")
	private String correlationReqHeader;

	/** The platform. */
	@Value("${app.platform}")
	private String platform;

	/* Customer Account Profile Foundation Service Transformer */
	@Autowired
	private CustomerAccountProfileFoundationServiceTransformer custAccntProfileSerTrans;

	@Autowired
	private AdapterUtility adapterUtility;

	

	/*
	 * This method is calling customer account profile transformer to convert
	 * object
	 */
	public AccountGETResponse transformResponseFromFDToAPI(Accnts accounts, Map<String, String> params) {
		return custAccntProfileSerTrans.transformCustomerAccountListAdapter(accounts, params);
	}
	
	public PSD2CustomerInfo transformCustomerInfoResponseFromFDToAPI(ChannelProfile channelProfile, Map<String, String> params) {
		return custAccntProfileSerTrans.transformCustomerInfo(channelProfile, params);
	}
	/*This method is vreating headers*/
	public HttpHeaders createRequestHeaders(Map<String, String> params) {
		if (NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.USER_ID))
				|| NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.CHANNEL_ID))
				|| NullCheckUtils.isNullOrEmpty(platform)
				|| NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.CORRELATION_ID))) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(userInReqHeader, params.get(PSD2Constants.USER_ID));
		httpHeaders.add(channelInReqHeader, params.get(PSD2Constants.CHANNEL_ID));
		httpHeaders.add(platformInReqHeader, platform);
		httpHeaders.add(correlationReqHeader, params.get(PSD2Constants.CORRELATION_ID));
		return httpHeaders;
	}
	/* This method is giving URL */
	public String getFoundationServiceURLAISP(Map<String, String> params, String userId) {
		String baseURL = adapterUtility.retriveCustProfileFoundationServiceURL(params.get(PSD2Constants.CHANNEL_ID));
		return baseURL + "/" + userId + "/accounts";
	}

	
	

}