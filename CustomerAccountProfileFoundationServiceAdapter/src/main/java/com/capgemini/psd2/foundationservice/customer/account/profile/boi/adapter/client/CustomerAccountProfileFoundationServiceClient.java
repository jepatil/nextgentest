package com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.ChannelProfile;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
@Component
public class CustomerAccountProfileFoundationServiceClient {
	
	/** The rest client. */
	@Autowired
	@Qualifier("restClientFoundation")
	private RestClientSync restClient;
	
	public ChannelProfile restTransportForCustomerAccountProfile(RequestInfo reqInfo, Class <ChannelProfile> responseType, HttpHeaders headers)
	{
		return restClient.callForGet(reqInfo, responseType, headers);
	}

}
