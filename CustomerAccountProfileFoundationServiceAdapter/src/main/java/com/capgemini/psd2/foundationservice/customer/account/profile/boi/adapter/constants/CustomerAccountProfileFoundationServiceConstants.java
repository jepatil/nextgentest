package com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.constants;

public class CustomerAccountProfileFoundationServiceConstants {
	

	public static final String SCHEMENAME="SchemeName";
	public static final String IDENTIFICATION ="Identification";
	public static final String SERVICERSCHEMENAME ="ServicerSchemeName";
	public static final String  SERVICERIDENTIFICATION="ServicerIdentification";
	public static final String ACCOUNTNUMBER ="AccountNumber";
	public static final String IBAN ="IBAN";
	public static final String ACCOUNTNSCNUMBER ="AccountNSCNumber";
	public static final String BIC ="BIC";
	public static final String SORTCODEACCOUNTNUMBER ="SORTCODEACCOUNTNUMBER";
	public static final String BICFI ="BICFI";
	public static final String CONTACT_ID ="CONTACT1";
	
	
}

