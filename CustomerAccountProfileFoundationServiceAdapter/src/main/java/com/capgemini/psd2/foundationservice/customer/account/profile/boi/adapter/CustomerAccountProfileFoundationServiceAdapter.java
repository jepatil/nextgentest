package com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.adapter.CustomerAccountListAdapter;
import com.capgemini.psd2.aisp.domain.AccountGETResponse;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.CustomerAccountsFilterFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.ChannelProfile;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion.AdapterFilterUtility;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.client.CustomerAccountProfileFoundationServiceClient;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.delegate.CustomerAccountProfileFoundationServiceDelegate;
import com.capgemini.psd2.fraudsystem.domain.PSD2CustomerInfo;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;

/**
 * CustomerAccountProfileFoundationServiceAdapter This adapter is to get
 * Customer Account List
 */
@Component
public class CustomerAccountProfileFoundationServiceAdapter implements CustomerAccountListAdapter {

	/* Customer Account Profile service Delegate */
	@Autowired
	private CustomerAccountProfileFoundationServiceDelegate customerAccountProfileFoundationServiceDelegate;
	
	@Autowired
	private CustomerAccountProfileFoundationServiceClient customerAccountProfileFoundationServiceClient;

    @Autowired
    private CustomerAccountsFilterFoundationServiceAdapter commonFilterUtility;

    @Autowired
    private AdapterFilterUtility adapterFilterUtility;


	@Override
	public AccountGETResponse retrieveCustomerAccountList(String userId, Map<String, String> params) {
		
		if (params == null) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		if (NullCheckUtils.isNullOrEmpty(userId)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		} 
		
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts filteredAccounts = commonFilterUtility.retrieveCustomerAccountList(userId,params);
		
	 
		if (filteredAccounts == null || filteredAccounts.getAccount()==null || filteredAccounts.getAccount().isEmpty()) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.ACCOUNT_DETAILS_NOT_FOUND);
		}
        
		return customerAccountProfileFoundationServiceDelegate.transformResponseFromFDToAPI(filteredAccounts, params);
	}


	@Override
	public PSD2CustomerInfo retrieveCustomerInfo(String userId, Map<String, String> params) {
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders httpHeaders = null;
		ChannelProfile channelProfile=null;
		if (params == null) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		if (NullCheckUtils.isNullOrEmpty(userId)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts filteredAccounts = commonFilterUtility.retrieveCustomerAccountList(userId,params);
		if (filteredAccounts == null || filteredAccounts.getAccount()==null || filteredAccounts.getAccount().isEmpty()) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.ACCOUNT_DETAILS_NOT_FOUND);
		}
	    httpHeaders = customerAccountProfileFoundationServiceDelegate.createRequestHeaders(params);
		requestInfo.setUrl(
				customerAccountProfileFoundationServiceDelegate.getFoundationServiceURLAISP(params, userId));
		channelProfile = customerAccountProfileFoundationServiceClient.restTransportForCustomerAccountProfile(requestInfo,
				ChannelProfile.class, httpHeaders);
		channelProfile.setAccounts(filteredAccounts);
		return customerAccountProfileFoundationServiceDelegate.transformCustomerInfoResponseFromFDToAPI(channelProfile, params);
	}
}
