package com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.test;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.domain.AccountGETResponse;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.domain.Accnt;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.domain.Accounts;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.domain.Accnts;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.domain.Jurisdiction;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.transformer.CustomerAccountProfileFoundationServiceTransformer;
import com.capgemini.psd2.fraudsystem.domain.PSD2CustomerInfo;
import com.capgemini.psd2.validator.PSD2Validator;
import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl;



@RunWith(SpringJUnit4ClassRunner.class)
public class CustomerAccountProfileFoundationServiceTransformerTests {

	@InjectMocks
	CustomerAccountProfileFoundationServiceTransformer cusAccProFoundServiceTransformer;
	
	@Mock
	private PSD2Validator validator;
	/**
	 * Sets the up.
	 */
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}

	
	@Test
	public void testTransformResponseFromFDToAPI() {
	/** Test account list */
	/** Creating First Account */
		Accnt accnt = new Accnt();
		accnt.setAccountNumber("12345678");
		accnt.setAccountNSC("123456");
		accnt.setAccountName("darshan");
		accnt.setBic("SC802001");
		accnt.setIban("1022675");
		accnt.setCurrency("GBP");
		accnt.setAccountPermission("V");
		
		Accnt accnt1 = new Accnt();
		accnt1.setAccountNumber("12345678");
		accnt1.setAccountNSC("123456");
		accnt1.setAccountName("amit");
		accnt1.setBic("LOYDGB21");
		accnt1.setIban("GB19LOYD30961799709943");
		accnt1.setCurrency("EUR");
		accnt1.setAccountPermission("A");
		accnt1.setJurisdiction(Jurisdiction.NORTHERN_IRELAND);
	/**Creating Second Account */
	//	Accnt accnt2 = new Accnt();
	
	/**Creating List Of Accounts */
		Accnts accounts = new Accnts();
		accounts.getAccount().add(accnt1);
		accounts.getAccount().add(accnt);
	/**Creating Param Map */
		Map<String, String> params = new HashMap<String, String>();
    // Setting value of userInReqHeader
		ReflectionTestUtils.setField(cusAccProFoundServiceTransformer, "userInReqHeader", "X-BOI-USER");
		params.put("consentFlowType", "AISP");
		params.put("X-BOI-USER", "BOI123");
	/**Creating Permission Map*/
		Map<String, List<String>> permission = new HashMap<>();
		List<String> listPer = new ArrayList<String>();
		listPer.add("V");
		permission.put("AISP", listPer);
		
		//Creating jurisdictions map
		
	 //  cusAccProFoundServiceTransformer.setPermission(permission);
	    
		Map<String, Map<String, String>> jurisdiction = new HashMap<>();
		Map<String, String> juryMap1=new HashMap<>();
		
		juryMap1.put("SchemeName","SORTCODEACCOUNTNUMBER");
		juryMap1.put("Identification","SORTCODEACCOUNTNUMBER");
		juryMap1.put("ServicerSchemeName","");
		juryMap1.put("ServicerIdentification","");
		jurisdiction.put("NORTHERN_IRELAND",juryMap1);
		
		
		
        Map<String, Map<String, List<String>>> accountFiltering =new HashMap<>();
        
        Map<String,List<String>> map1 = new HashMap<String,List<String>>();

		List <String> accountType=new ArrayList<>();
		accountType.add("Current Account");
		map1.put("AISP", accountType);
		map1.put("CISP", accountType);
		map1.put("PISP", accountType);
		
		accountFiltering.put("accountType", map1);
		
		Map<String,List<String>> map2 = new HashMap<String,List<String>>();
		
		List <String> jurisdictionList=new ArrayList<>();
		jurisdictionList.add("NORTHERN_IRELAND.SchemeName=SORTCODEACCOUNTNUMBER");
		jurisdictionList.add("NORTHERN_IRELAND.Identification=SORTCODEACCOUNTNUMBER");
		jurisdictionList.add("NORTHERN_IRELAND.ServicerSchemeName=");
		jurisdictionList.add("NORTHERN_IRELAND.ServicerIdentification=");
		jurisdictionList.add("GREAT_BRITAIN.SchemeName=SORTCODEACCOUNTNUMBER");
		jurisdictionList.add("GREAT_BRITAIN.Identification=SORTCODEACCOUNTNUMBER");
		jurisdictionList.add("GREAT_BRITAIN.ServicerSchemeName=");
		jurisdictionList.add("GREAT_BRITAIN.ServicerIdentification=");
		
		map2.put("AISP", jurisdictionList);
		map2.put("CISP", jurisdictionList);
		map2.put("PISP", jurisdictionList);
		
		accountFiltering.put("jurisdiction", map2);      
		
		Map<String,List<String>> map3 = new HashMap<String,List<String>>();
		
		List <String> permissionList1=new ArrayList<>();
		permissionList1.add("A");
		permissionList1.add("V");
		List <String> permissionList2=new ArrayList<>();
		permissionList2.add("A");
		permissionList2.add("X");
		
		map3.put("AISP", permissionList1);
		map3.put("CISP", permissionList1);
		map3.put("PISP", permissionList2);
		
		accountFiltering.put("permission", map3);

		cusAccProFoundServiceTransformer.setAccountFiltering(accountFiltering);
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts accs= new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt accnt2 = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt();
		accnt2.setAccountNumber("12345678");
		accnt2.setAccountNSC("123456");
		accnt2.setAccountName("amit");
		accnt2.setBic("LOYDGB21");
		accnt2.setIban("GB19LOYD30961799709943");
		accnt2.setCurrency("EUR");
		accnt2.setAccountPermission("A");
		accnt2.setJurisdiction(com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Jurisdiction.NORTHERN_IRELAND);
	
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt accnt3 = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt();
		accnt2.setAccountPermission("A");
		accnt2.setJurisdiction(com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Jurisdiction.NORTHERN_IRELAND);
		accnt3.setAccountPermission("V");
		accnt3.setJurisdiction(com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Jurisdiction.NORTHERN_IRELAND);
		accs.getAccount().add(accnt2);
	
		ReflectionTestUtils.setField(cusAccProFoundServiceTransformer, "accuntNumLength", "8");
		ReflectionTestUtils.setField(cusAccProFoundServiceTransformer, "accountNSCLength", "6");
		AccountGETResponse customerlist = cusAccProFoundServiceTransformer.transformCustomerAccountListAdapter(accs, params);
		assertNotNull(customerlist);
		
		juryMap1.remove("Identification");
		juryMap1.put("Identification","IBAN");
	    customerlist = cusAccProFoundServiceTransformer.transformCustomerAccountListAdapter(accs, params);
		assertNotNull(customerlist);
	}
	
	@Test
	public void testTransformResponseFromFDToAPIIBAN() {
	/** Test account list */
	/** Creating First Account */
		Accnt accnt = new Accnt();
		accnt.setAccountNumber("12345678");
		accnt.setAccountNSC("123456");
		accnt.setAccountName("darshan");
		accnt.setBic("SC802001");
		accnt.setIban("1022675");
		accnt.setCurrency("GBP");
		accnt.setAccountPermission("V");
		accnt.setJurisdiction(Jurisdiction.REPUBLIC_OF_IRELAND);
	
	/**Creating List Of Accounts */
		Accnts accounts = new Accnts();
		accounts.getAccount().add(accnt);
	/**Creating Param Map */
		Map<String, String> params = new HashMap<String, String>();
    // Setting value of userInReqHeader
		ReflectionTestUtils.setField(cusAccProFoundServiceTransformer, "userInReqHeader", "X-BOI-USER");
		params.put("consentFlowType", "AISP");
		params.put("X-BOI-USER", "BOI123");
	/**Creating Permission Map*/
		Map<String, List<String>> permission = new HashMap<>();
		List<String> listPer = new ArrayList<String>();
		listPer.add("V");
		permission.put("AISP", listPer);
		
		//Creating jurisdictions map
		
	 //  cusAccProFoundServiceTransformer.setPermission(permission);
	    
		Map<String, Map<String, String>> jurisdiction = new HashMap<>();
		Map<String, String> juryMap1=new HashMap<>();
		
		juryMap1.put("SchemeName","IBAN");
		juryMap1.put("Identification","IBAN");
		juryMap1.put("ServicerSchemeName","BICFI");
		juryMap1.put("ServicerIdentification","BIC");
		jurisdiction.put("REPUBLIC_OF_IRELAND",juryMap1);
		
		
		
        Map<String, Map<String, List<String>>> accountFiltering =new HashMap<>();
        
        Map<String,List<String>> map1 = new HashMap<String,List<String>>();

		List <String> accountType=new ArrayList<>();
		accountType.add("Current Account");
		map1.put("AISP", accountType);
		map1.put("CISP", accountType);
		map1.put("PISP", accountType);
		
		accountFiltering.put("accountType", map1);
		
		Map<String,List<String>> map2 = new HashMap<String,List<String>>();
		
		List <String> jurisdictionList=new ArrayList<>();
		jurisdictionList.add("REPUBLIC_OF_IRELAND.SchemeName=IBAN");
		jurisdictionList.add("REPUBLIC_OF_IRELAND.Identification=IBAN");
		jurisdictionList.add("REPUBLIC_OF_IRELAND.ServicerSchemeName=BICFI");
		jurisdictionList.add("REPUBLIC_OF_IRELAND.ServicerIdentification=BIC");
	
		
		map2.put("AISP", jurisdictionList);
		map2.put("CISP", jurisdictionList);
		map2.put("PISP", jurisdictionList);
		
		accountFiltering.put("jurisdiction", map2);      
		
		Map<String,List<String>> map3 = new HashMap<String,List<String>>();
		
		List <String> permissionList1=new ArrayList<>();
		permissionList1.add("A");
		permissionList1.add("V");
		List <String> permissionList2=new ArrayList<>();
		permissionList2.add("A");
		permissionList2.add("X");
		
		map3.put("AISP", permissionList1);
		map3.put("CISP", permissionList1);
		map3.put("PISP", permissionList2);
		
		accountFiltering.put("permission", map3);

		cusAccProFoundServiceTransformer.setAccountFiltering(accountFiltering);
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts accs= new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt accnt2 = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt();
		accnt2.setAccountNumber("12345678");
		accnt2.setAccountNSC("123456");
		accnt2.setAccountName("amit");
		accnt2.setBic("LOYDGB21");
		accnt2.setIban("GB19LOYD30961799709943");
		accnt2.setCurrency("EUR");
		accnt2.setAccountPermission("A");
		accnt2.setJurisdiction(com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Jurisdiction.NORTHERN_IRELAND);
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt accnt3 = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt();
		accnt2.setAccountPermission("A");
		accnt2.setJurisdiction(com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Jurisdiction.REPUBLIC_OF_IRELAND);
		accnt3.setAccountPermission("V");
		accnt3.setJurisdiction(com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Jurisdiction.REPUBLIC_OF_IRELAND);
		accs.getAccount().add(accnt2);

		ReflectionTestUtils.setField(cusAccProFoundServiceTransformer, "accuntNumLength", "8");
		ReflectionTestUtils.setField(cusAccProFoundServiceTransformer, "accountNSCLength", "6");
		AccountGETResponse customerlist = cusAccProFoundServiceTransformer.transformCustomerAccountListAdapter(accs, params);
		assertNotNull(customerlist);
		
		juryMap1.remove("Identification");
		juryMap1.put("Identification","IBAN");
	    customerlist = cusAccProFoundServiceTransformer.transformCustomerAccountListAdapter(accs, params);
		assertNotNull(customerlist);
	}

	
	@Test(expected = AdapterException.class)
	public void testTransformResponseFromFDToAPIInvalidAccountNumberLength() {
	/** Test account list */
	/** Creating First Account */
		Accnt accnt = new Accnt();
		accnt.setAccountNumber("12345678");
		accnt.setAccountNSC("123456");
		accnt.setAccountName("darshan");
		accnt.setBic("SC802001");
		accnt.setIban("1022675");
		accnt.setCurrency("GBP");
		accnt.setAccountPermission("V");
		accnt.setJurisdiction(Jurisdiction.REPUBLIC_OF_IRELAND);
	
	/**Creating List Of Accounts */
		Accnts accounts = new Accnts();
		accounts.getAccount().add(accnt);
	/**Creating Param Map */
		Map<String, String> params = new HashMap<String, String>();
    // Setting value of userInReqHeader
		ReflectionTestUtils.setField(cusAccProFoundServiceTransformer, "userInReqHeader", "X-BOI-USER");
		params.put("consentFlowType", "AISP");
		params.put("X-BOI-USER", "BOI123");
	/**Creating Permission Map*/
		Map<String, List<String>> permission = new HashMap<>();
		List<String> listPer = new ArrayList<String>();
		listPer.add("V");
		permission.put("AISP", listPer);
	    
		Map<String, Map<String, String>> jurisdiction = new HashMap<>();
		Map<String, String> juryMap1=new HashMap<>();
		juryMap1.put("SchemeName","IBAN");
		juryMap1.put("Identification","IBAN");
		juryMap1.put("ServicerSchemeName","BICFI");
		juryMap1.put("ServicerIdentification","BIC");
		jurisdiction.put("REPUBLIC_OF_IRELAND",juryMap1);
        Map<String, Map<String, List<String>>> accountFiltering =new HashMap<>();
        Map<String,List<String>> map1 = new HashMap<String,List<String>>();
		List <String> accountType=new ArrayList<>();
		accountType.add("Current Account");
		map1.put("AISP", accountType);
		map1.put("CISP", accountType);
		map1.put("PISP", accountType);
		
		accountFiltering.put("accountType", map1);
		
		Map<String,List<String>> map2 = new HashMap<String,List<String>>();
		
		List <String> jurisdictionList=new ArrayList<>();
		jurisdictionList.add("REPUBLIC_OF_IRELAND.SchemeName=IBAN");
		jurisdictionList.add("REPUBLIC_OF_IRELAND.Identification=IBAN");
		jurisdictionList.add("REPUBLIC_OF_IRELAND.ServicerSchemeName=BICFI");
		jurisdictionList.add("REPUBLIC_OF_IRELAND.ServicerIdentification=BIC");
	
		
		map2.put("AISP", jurisdictionList);
		map2.put("CISP", jurisdictionList);
		map2.put("PISP", jurisdictionList);
		
		accountFiltering.put("jurisdiction", map2);      
		
		Map<String,List<String>> map3 = new HashMap<String,List<String>>();
		
		List <String> permissionList1=new ArrayList<>();
		permissionList1.add("A");
		permissionList1.add("V");
		map3.put("AISP", permissionList1);
		accountFiltering.put("permission", map3);
		cusAccProFoundServiceTransformer.setAccountFiltering(accountFiltering);
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts accs= new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt accnt2 = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt();
		accnt2.setAccountNumber("123456789");
		accnt2.setAccountNSC("123456");
		accnt2.setAccountName("amit");
		accnt2.setBic("LOYDGB21");
		accnt2.setIban("GB19LOYD30961799709943");
		accnt2.setCurrency("EUR");
		accnt2.setAccountPermission("A");
		accnt2.setJurisdiction(com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Jurisdiction.NORTHERN_IRELAND);
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt accnt3 = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt();
		accnt2.setAccountPermission("A");
		accnt2.setJurisdiction(com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Jurisdiction.REPUBLIC_OF_IRELAND);
		accnt3.setAccountPermission("V");
		accnt3.setJurisdiction(com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Jurisdiction.REPUBLIC_OF_IRELAND);
		accs.getAccount().add(accnt2);

		ReflectionTestUtils.setField(cusAccProFoundServiceTransformer, "accuntNumLength", "8");
		ReflectionTestUtils.setField(cusAccProFoundServiceTransformer, "accountNSCLength", "6");
		AccountGETResponse customerlist = cusAccProFoundServiceTransformer.transformCustomerAccountListAdapter(accs, params);
		assertNotNull(customerlist);
		
		juryMap1.remove("Identification");
		juryMap1.put("Identification","IBAN");
	    customerlist = cusAccProFoundServiceTransformer.transformCustomerAccountListAdapter(accs, params);
		assertNotNull(customerlist);
	}

	@Test
	public void testPSD2CustomerInfoTransformResponseFromFDToAPI() {
	/** Test account list */
	/** Creating First Account */
		Accnt accnt = new Accnt();
		accnt.setAccountNumber("12345678");
		accnt.setAccountNSC("123456");
		accnt.setAccountName("darshan");
		accnt.setBic("SC802001");
		accnt.setIban("1022675");
		accnt.setCurrency("GBP");
		accnt.setAccountPermission("V");
		
		Accnt accnt1 = new Accnt();
		accnt1.setAccountNumber("12345678");
		accnt1.setAccountNSC("123456");
		accnt1.setAccountName("amit");
		accnt1.setBic("LOYDGB21");
		accnt1.setIban("GB19LOYD30961799709943");
		accnt1.setCurrency("EUR");
		accnt1.setAccountPermission("A");
		accnt1.setJurisdiction(Jurisdiction.NORTHERN_IRELAND);
	/**Creating Second Account */
	//	Accnt accnt2 = new Accnt();
	
	/**Creating List Of Accounts */
		Accnts accounts = new Accnts();
		accounts.getAccount().add(accnt1);
		accounts.getAccount().add(accnt);
	/**Creating Param Map */
		Map<String, String> params = new HashMap<String, String>();
    // Setting value of userInReqHeader
		ReflectionTestUtils.setField(cusAccProFoundServiceTransformer, "userInReqHeader", "X-BOI-USER");
		params.put("consentFlowType", "AISP");
		params.put("X-BOI-USER", "BOI123");
	/**Creating Permission Map*/
		Map<String, List<String>> permission = new HashMap<>();
		List<String> listPer = new ArrayList<String>();
		listPer.add("V");
		permission.put("AISP", listPer);
		
		//Creating jurisdictions map
		
	 //  cusAccProFoundServiceTransformer.setPermission(permission);
	    
		Map<String, Map<String, String>> jurisdiction = new HashMap<>();
		Map<String, String> juryMap1=new HashMap<>();
		
		juryMap1.put("SchemeName","SORTCODEACCOUNTNUMBER");
		juryMap1.put("Identification","SORTCODEACCOUNTNUMBER");
		juryMap1.put("ServicerSchemeName","");
		juryMap1.put("ServicerIdentification","");
		jurisdiction.put("NORTHERN_IRELAND",juryMap1);
		
		
		
        Map<String, Map<String, List<String>>> accountFiltering =new HashMap<>();
        
        Map<String,List<String>> map1 = new HashMap<String,List<String>>();

		List <String> accountType=new ArrayList<>();
		accountType.add("Current Account");
		map1.put("AISP", accountType);
		map1.put("CISP", accountType);
		map1.put("PISP", accountType);
		
		accountFiltering.put("accountType", map1);
		
		Map<String,List<String>> map2 = new HashMap<String,List<String>>();
		
		List <String> jurisdictionList=new ArrayList<>();
		jurisdictionList.add("NORTHERN_IRELAND.SchemeName=SORTCODEACCOUNTNUMBER");
		jurisdictionList.add("NORTHERN_IRELAND.Identification=SORTCODEACCOUNTNUMBER");
		jurisdictionList.add("NORTHERN_IRELAND.ServicerSchemeName=");
		jurisdictionList.add("NORTHERN_IRELAND.ServicerIdentification=");
		jurisdictionList.add("GREAT_BRITAIN.SchemeName=SORTCODEACCOUNTNUMBER");
		jurisdictionList.add("GREAT_BRITAIN.Identification=SORTCODEACCOUNTNUMBER");
		jurisdictionList.add("GREAT_BRITAIN.ServicerSchemeName=");
		jurisdictionList.add("GREAT_BRITAIN.ServicerIdentification=");
		
		map2.put("AISP", jurisdictionList);
		map2.put("CISP", jurisdictionList);
		map2.put("PISP", jurisdictionList);
		
		accountFiltering.put("jurisdiction", map2);      
		
		Map<String,List<String>> map3 = new HashMap<String,List<String>>();
		
		List <String> permissionList1=new ArrayList<>();
		permissionList1.add("A");
		permissionList1.add("V");
		List <String> permissionList2=new ArrayList<>();
		permissionList2.add("A");
		permissionList2.add("X");
		
		map3.put("AISP", permissionList1);
		map3.put("CISP", permissionList1);
		map3.put("PISP", permissionList2);
		
		accountFiltering.put("permission", map3);

		cusAccProFoundServiceTransformer.setAccountFiltering(accountFiltering);
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts accs= new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt accnt2 = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt();
		accnt2.setAccountNumber("12345678");
		accnt2.setAccountNSC("123456");
		accnt2.setAccountName("amit");
		accnt2.setBic("LOYDGB21");
		accnt2.setIban("GB19LOYD30961799709943");
		accnt2.setCurrency("EUR");
		accnt2.setAccountPermission("A");
		accnt2.setJurisdiction(com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Jurisdiction.NORTHERN_IRELAND);
	
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt accnt3 = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt();
		accnt2.setAccountPermission("A");
		accnt2.setJurisdiction(com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Jurisdiction.NORTHERN_IRELAND);
		accnt3.setAccountPermission("V");
		accnt3.setJurisdiction(com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Jurisdiction.NORTHERN_IRELAND);
		accs.getAccount().add(accnt2);
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.ChannelProfile channelProfile=new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.ChannelProfile(); 
		channelProfile.setAccounts(accs);
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.CustomerType customerType=new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.CustomerType();
		customerType.setAddressLine1("snow House");
		customerType.setAddressLine2("Shelbourne Road");
		customerType.setAddressLine3("Dublin");
		customerType.setAddressLine4("D04 NP20");
		customerType.setClientType("INDIVIDUAL");
		customerType.setContactNumber("96998588");
		customerType.setEmail("jon@gmail.com");
		customerType.setFirstName("Jon");
		customerType.setSurName("snow");
		customerType.setTitle("Mr.");
		customerType.setTradingAsName("Capgemini");
		customerType.setDateOfBirth(new XMLGregorianCalendarImpl(new GregorianCalendar()));
		channelProfile.setCustomer(customerType);
		
		ReflectionTestUtils.setField(cusAccProFoundServiceTransformer, "accuntNumLength", "8");
		ReflectionTestUtils.setField(cusAccProFoundServiceTransformer, "accountNSCLength", "6");
	
		
		juryMap1.remove("Identification");
		juryMap1.put("Identification","IBAN");
		PSD2CustomerInfo  customerlist = cusAccProFoundServiceTransformer.transformCustomerInfo(channelProfile, params);
		assertNotNull(customerlist);
	}

	
	// Gettng List Of Permission 
	@Test
	public void testGetPermissionList() {
		Map<String, List<String>> permission = new HashMap<>();
		List<String> listPer = new ArrayList<String>();
		listPer.add("V");
		permission.put("AISP", listPer);
		
	Map<String,List<String>> map3 = new HashMap<String,List<String>>();
	 Map<String, Map<String, List<String>>> accountFiltering =new HashMap<>();
		List <String> permissionList1=new ArrayList<>();
		permissionList1.add("A");
		permissionList1.add("V");
		List <String> permissionList2=new ArrayList<>();
		permissionList2.add("A");
		permissionList2.add("X");
		
		map3.put("AISP", permissionList1);
		map3.put("CISP", permissionList1);
		map3.put("PISP", permissionList2);
		
		accountFiltering.put("permission", map3);
		assertNotNull(cusAccProFoundServiceTransformer.getAccountFiltering());
	}
	

}



