package com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.domain.AccountGETResponse;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.CustomerAccountsFilterFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.ChannelProfile;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.CustomerAccountProfileFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.client.CustomerAccountProfileFoundationServiceClient;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.delegate.CustomerAccountProfileFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.transformer.CustomerAccountProfileFoundationServiceTransformer;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;
import com.capgemini.psd2.validator.PSD2Validator;

@RunWith(SpringJUnit4ClassRunner.class)
public class CustomerAccountProfileFoundationServiceAdapterTests {

	@InjectMocks
	private CustomerAccountProfileFoundationServiceAdapter customerAccProFoundServiceAdapter;

	@Mock
	private CustomerAccountProfileFoundationServiceDelegate cusAccProFoundServiceDelegate;

	@Mock 
	private CustomerAccountsFilterFoundationServiceAdapter commonFilterUtility;
	
	@Mock
	private CustomerAccountProfileFoundationServiceTransformer custAccntProfileSerTrans;
	@Mock
	private CustomerAccountProfileFoundationServiceClient custAccntProfileFouServiceClient;
	
	@Mock
	private RestClientSyncImpl restClient;

	@Mock
	private PSD2Validator validator;
	/**
	 * Sets the up.
	 */
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}

	@Test(expected = AdapterException.class)
	public void paramsNullTest(){
		String userId = "test";
		customerAccProFoundServiceAdapter.retrieveCustomerAccountList(userId, null);
	}
	
	@Test(expected = AdapterException.class)
	public void userIdNullTest(){
		Map<String , String> params = new HashMap<>();
		customerAccProFoundServiceAdapter.retrieveCustomerAccountList(null, params);
	}
	
 	
	@Test(expected = AdapterException.class)
	public void getAccountNullTest(){
		Map<String , String> params = new HashMap<>();
		Mockito.when(commonFilterUtility.retrieveCustomerAccountList(any(), any())).thenReturn(null);
		customerAccProFoundServiceAdapter.retrieveCustomerAccountList("test", params);
	}
	
	@Test
	public void testCustomerAccountInfoList() {
	/** Test account list */
	/** Creating First Account */
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt accnt = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt();
		accnt.setAccountNumber("acct8895");
		accnt.setAccountNSC("123456");
		accnt.setAccountName("darshan");
		accnt.setBic("SC802001");
		accnt.setIban("1022675");
		accnt.setCurrency("GBP");
		accnt.setAccountPermission("V");
	/** Creating Second Account */
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt accnt1 = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt();
		accnt1.setAccountNumber("acct4568");
		accnt1.setAccountNSC("26365678");
		accnt1.setAccountName("amit");
		accnt1.setBic("LOYDGB21");
		accnt1.setIban("GB19LOYD30961799709943");
		accnt1.setCurrency("EUR");
		accnt1.setAccountPermission("A");
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts filteraccount =new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts();
		filteraccount.getAccount().add(accnt);
		filteraccount.getAccount().add(accnt1);
		Mockito.when(commonFilterUtility.retrieveCustomerAccountList(any(), any())).thenReturn(filteraccount);
		assertNotNull(filteraccount);
	}
    
	
	@Test
	public void testCustomerAccountInfoFraudnetList() {
	/** Test account list */
	/** Creating First Account */
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt accnt = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt();
		accnt.setAccountNumber("acct8895");
		accnt.setAccountNSC("123456");
		accnt.setAccountName("darshan");
		accnt.setBic("SC802001");
		accnt.setIban("1022675");
		accnt.setCurrency("GBP");
		accnt.setAccountPermission("V");
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts filteraccount =new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts();
		filteraccount.getAccount().add(accnt);
		Mockito.when(commonFilterUtility.retrieveCustomerAccountList(any(), any())).thenReturn(filteraccount);
		assertNotNull(filteraccount);
	}
    
	@Test(expected = AdapterException.class)
	public void testFilterAccountListNull() {
		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.USER_ID, "user");
		params.put(PSD2Constants.CHANNEL_ID, "channel");
		params.put(PSD2Constants.CORRELATION_ID, "correlation");
		Mockito.when(commonFilterUtility.retrieveCustomerAccountList(any(), any())).thenReturn(null);
		customerAccProFoundServiceAdapter.retrieveCustomerInfo("BOI999", params);
	
	}
	@Test
	public void testFilterAccountListwithsuccess() {
		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.USER_ID, "user");
		params.put(PSD2Constants.CHANNEL_ID, "channel");
		params.put(PSD2Constants.CORRELATION_ID, "correlation");
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt accnt = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt();
		accnt.setAccountNumber("acct8895");
		accnt.setAccountNSC("123456");
		accnt.setAccountName("darshan");
		accnt.setBic("SC802001");
		accnt.setIban("1022675");
		accnt.setCurrency("GBP");
		accnt.setAccountPermission("V");
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts filteraccount =new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts();
		filteraccount.getAccount().add(accnt);
		Mockito.when(commonFilterUtility.retrieveCustomerAccountList(any(), any())).thenReturn(filteraccount);
		Mockito.when(cusAccProFoundServiceDelegate.getFoundationServiceURLAISP(any(), any())).thenReturn("http://localhost:8082/fs-accountEnquiry-service/services/AccountEnquiry/business/nsc1234/acc1234/account");		ChannelProfile channelProfile=new ChannelProfile();
		Mockito.when(custAccntProfileFouServiceClient.restTransportForCustomerAccountProfile(any(), any(), any())).thenReturn(channelProfile);
		channelProfile.setAccounts(filteraccount);
		customerAccProFoundServiceAdapter.retrieveCustomerInfo("BOI999", params);
	
	}
	@Test(expected = AdapterException.class)
	public void testFilterAccountListwithNull() {
		customerAccProFoundServiceAdapter.retrieveCustomerInfo("BOI999", null);
	}
	public void createFilterUtilityMap()
	{
        Map<String, Map<String, List<String>>> accountFiltering =new HashMap<>();
        
		Map<String,List<String>> map1 = new HashMap<String,List<String>>();
       
	        List <String> accountType=new ArrayList<>();
	        accountType.add("Current Account");
	        map1.put("AISP", accountType);
	        map1.put("CISP", accountType);
	        map1.put("PISP", accountType);
       
   accountFiltering.put("accountType", map1);
       
       Map<String,List<String>> map2 = new HashMap<String,List<String>>();
       
	        List <String> jurisdictionList=new ArrayList<>();
	        jurisdictionList.add("NORTHERN_IRELAND.SchemeName=SORTCODEACCOUNTNUMBER");
			jurisdictionList.add("NORTHERN_IRELAND.Identification=SORTCODEACCOUNTNUMBER");
			jurisdictionList.add("NORTHERN_IRELAND.ServicerSchemeName=");
			jurisdictionList.add("NORTHERN_IRELAND.ServicerIdentification=");
			jurisdictionList.add("GREAT_BRITAIN.SchemeName=SORTCODEACCOUNTNUMBER");
			jurisdictionList.add("GREAT_BRITAIN.Identification=SORTCODEACCOUNTNUMBER");
			jurisdictionList.add("GREAT_BRITAIN.ServicerSchemeName=");
			jurisdictionList.add("GREAT_BRITAIN.ServicerIdentification=");
	        
	        map2.put("AISP", jurisdictionList);
	        map2.put("CISP", jurisdictionList);
	        map2.put("PISP", jurisdictionList);
       
    accountFiltering.put("jurisdiction", map2);	
       
       Map<String,List<String>> map3 = new HashMap<String,List<String>>();
       
	        List <String> permissionList1=new ArrayList<>();
	        permissionList1.add("A");
	        permissionList1.add("V");
	        List <String> permissionList2=new ArrayList<>();
	        permissionList2.add("A");
	        permissionList2.add("X");
	        
	        map3.put("AISP", permissionList1);
	        map3.put("CISP", permissionList1);
	        map3.put("PISP", permissionList2);
       
    accountFiltering.put("permission", map3);
       
    custAccntProfileSerTrans.setAccountFiltering(accountFiltering);
    custAccntProfileSerTrans.getAccountFiltering();
	
	}


}
