
package com.capgemini.psd2.foundationservice.validate.prestage.payment.boi.adapter.transformer;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.foundationservice.validate.prestage.payment.boi.adapter.constants.ValidatePreStagePaymentFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.validate.prestage.payment.boi.adapter.domain.DeliveryAddress;
import com.capgemini.psd2.foundationservice.validate.prestage.payment.boi.adapter.domain.PayeeInformation;
import com.capgemini.psd2.foundationservice.validate.prestage.payment.boi.adapter.domain.PayerInformation;
import com.capgemini.psd2.foundationservice.validate.prestage.payment.boi.adapter.domain.Payment;
import com.capgemini.psd2.foundationservice.validate.prestage.payment.boi.adapter.domain.PaymentInformation;
import com.capgemini.psd2.foundationservice.validate.prestage.payment.boi.adapter.domain.PaymentInstruction;
import com.capgemini.psd2.foundationservice.validate.prestage.payment.boi.adapter.domain.ValidationPassed;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.DebtorAgent.SchemeNameEnum;
import com.capgemini.psd2.pisp.domain.PaymentSetupInitiation;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.PaymentSetupValidationResponse;
import com.capgemini.psd2.pisp.domain.RiskDeliveryAddress;
import com.capgemini.psd2.utilities.NullCheckUtils;


@Component
public class ValidatePreStagePaymentFoundationServiceTransformer  {
	
	@Autowired
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;

	public PaymentInstruction transformPaymentSetupPOSTRequest(CustomPaymentSetupPOSTRequest paymentSetupPOSTRequest){
		
		PaymentSetupInitiation initiation = paymentSetupPOSTRequest.getData().getInitiation();
		
		//PaymentSetup/Data
		PaymentInformation paymentInformation = new PaymentInformation();
		paymentInformation.setPaymentDate(timeZoneDateTimeAdapter.parseNewDateTimeFS(new Date()));		
		Payment payment = new Payment();
		payment.setRequestType("ValidatePreStagePayment");

		//PaymentSetup/Data/Initiation
		paymentInformation.setInstructionIdentification(initiation.getInstructionIdentification());
		PaymentInstruction paymentInstruction = new PaymentInstruction();
		paymentInstruction.setEndToEndIdentification(initiation.getEndToEndIdentification());
		BigDecimal amount = new BigDecimal(initiation.getInstructedAmount().getAmount());
		paymentInformation.setAmount(amount);
		PayeeInformation payeeInformation = new PayeeInformation();
		payeeInformation.setBeneficiaryCurrency(initiation.getInstructedAmount().getCurrency());
		
	
		//PaymentSetup/Data/Initiation/DebtorAgent
		PayerInformation payerInformation = new PayerInformation();
		if(!NullCheckUtils.isNullOrEmpty(initiation.getDebtorAgent())){
			if(SchemeNameEnum.BICFI.toString().equals(initiation.getDebtorAgent().getSchemeName().toString())){
				payerInformation.setBic(initiation.getDebtorAgent().getIdentification());
			} 
		}
		
		//PaymentSetup/Data/Initiation/DebtorAccount
		if(!NullCheckUtils.isNullOrEmpty(initiation.getDebtorAccount())){
			if(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.SORTCODEACCOUNTNUMBER.toString().equals(initiation.getDebtorAccount().getSchemeName().toString())){	
				payerInformation.setNsc(initiation.getDebtorAccount().getIdentification().substring(0, 6));
            	payerInformation.setAccountNumber(initiation.getDebtorAccount().getIdentification().substring(6, 14));
			}
			else if(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.IBAN.toString().equals(initiation.getDebtorAccount().getSchemeName().toString())){	
				payerInformation.setIban(initiation.getDebtorAccount().getIdentification());
			}
			payerInformation.setAccountName(initiation.getDebtorAccount().getName());
			payerInformation.setAccountSecondaryID(initiation.getDebtorAccount().getSecondaryIdentification());
			
		}
		
		//PaymentSetup/Data/Initiation/CreditorAgent
		if(!NullCheckUtils.isNullOrEmpty(initiation.getCreditorAgent())){
			if(com.capgemini.psd2.pisp.domain.CreditorAgent.SchemeNameEnum.BICFI.toString().equals(initiation.getCreditorAgent().getSchemeName().toString())){
				payeeInformation.setBic(initiation.getCreditorAgent().getIdentification());
			} 
		}
		
		//PaymentSetup/Data/Initiation/CreditorAccount
		if(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.SORTCODEACCOUNTNUMBER.toString().equals(initiation.getCreditorAccount().getSchemeName().toString())){
			payeeInformation.setBeneficiaryNsc(initiation.getCreditorAccount().getIdentification().substring(0, 6));
        	payeeInformation.setBeneficiaryAccountNumber(initiation.getCreditorAccount().getIdentification().substring(6, 14));
        	payeeInformation.setBeneficiaryCountry(ValidatePreStagePaymentFoundationServiceConstants.BENEFICIARY_COUNTRY);
		} else if(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.IBAN.toString().equals(initiation.getCreditorAccount().getSchemeName().toString())){	
			payeeInformation.setIban(initiation.getCreditorAccount().getIdentification());
			payeeInformation.setBeneficiaryCountry(initiation.getCreditorAccount().getIdentification().substring(0, 2));
		}
		
		payeeInformation.setBeneficiaryName(initiation.getCreditorAccount().getName());
		payeeInformation.setAccountSecondaryID(initiation.getCreditorAccount().getSecondaryIdentification());
		
		//PaymentSetup/Data/Initiation/RemittanceInformation
		if(!NullCheckUtils.isNullOrEmpty(initiation.getRemittanceInformation())){
			paymentInformation.setUnstructured(initiation.getRemittanceInformation().getUnstructured());
			paymentInformation.setBeneficiaryReference(initiation.getRemittanceInformation().getReference());
		}

		//PaymentSetup/Risk
		if(!NullCheckUtils.isNullOrEmpty(paymentSetupPOSTRequest.getRisk().getPaymentContextCode())){
			paymentInformation.setPaymentContextCode(paymentSetupPOSTRequest.getRisk().getPaymentContextCode().toString());
		}

		payeeInformation.setMerchentCategoryCode(paymentSetupPOSTRequest.getRisk().getMerchantCategoryCode());
		payerInformation.setMerchantCustomerIdentification(paymentSetupPOSTRequest.getRisk().getMerchantCustomerIdentification());
		
		
		//PaymentSetup/Risk/DeliveryAddress
		setDeliveryAddress(paymentSetupPOSTRequest, payeeInformation);
			

        payment.setPaymentInformation(paymentInformation);
        payment.setPayerInformation(payerInformation);
        payment.setPayeeInformation(payeeInformation);
		paymentInstruction.setPayment(payment);
		
		return paymentInstruction;
		
	}	

	private void setDeliveryAddress(PaymentSetupPOSTRequest paymentSetupPOSTRequest, PayeeInformation payeeInformation) {
		
		if(!NullCheckUtils.isNullOrEmpty(paymentSetupPOSTRequest.getRisk().getDeliveryAddress())){
			DeliveryAddress deliveryAddress = new DeliveryAddress();
			RiskDeliveryAddress riskDeliveryAddress = paymentSetupPOSTRequest.getRisk().getDeliveryAddress();
			if(!NullCheckUtils.isNullOrEmpty(riskDeliveryAddress.getAddressLine())){
				int addressLineLength = riskDeliveryAddress.getAddressLine().size();
				if(addressLineLength>0)
					deliveryAddress.setAddressLine1(riskDeliveryAddress.getAddressLine().get(0));
		        if(addressLineLength > 1)
		        	deliveryAddress.setAddressLine2(riskDeliveryAddress.getAddressLine().get(1));
			}
			
			deliveryAddress.setStreetName(riskDeliveryAddress.getStreetName());
			deliveryAddress.setBuildingNumber(riskDeliveryAddress.getBuildingNumber());
			deliveryAddress.setPostCode(riskDeliveryAddress.getPostCode());
			deliveryAddress.setTownName(riskDeliveryAddress.getTownName());
			
			if (!NullCheckUtils.isNullOrEmpty(riskDeliveryAddress.getCountrySubDivision())) {
				int countrySubDivisionCount = riskDeliveryAddress.getCountrySubDivision().size();
				if (countrySubDivisionCount > 0)
					deliveryAddress.setCountrySubDivision1(riskDeliveryAddress.getCountrySubDivision().get(0));
				if (countrySubDivisionCount > 1)
					deliveryAddress.setCountrySubDivision2(riskDeliveryAddress.getCountrySubDivision().get(1));
			}
			deliveryAddress.setCountry(riskDeliveryAddress.getCountry());
			payeeInformation.setDeliveryAddress(deliveryAddress);
		}			
	}
	
	public PaymentSetupValidationResponse transformValidatePreStagePaymentResponse(ValidationPassed validationPassed){
		
		PaymentSetupValidationResponse paymentSetupPreValidationResponse = new PaymentSetupValidationResponse();
		
		if (!NullCheckUtils.isNullOrEmpty(validationPassed))
			paymentSetupPreValidationResponse.setPaymentSetupValidationStatus(ValidatePreStagePaymentFoundationServiceConstants.VALIDATION_STATUS_PENDING);
		else
			paymentSetupPreValidationResponse.setPaymentSetupValidationStatus(ValidatePreStagePaymentFoundationServiceConstants.VALIDATION_STATUS_REJECTED);

		return paymentSetupPreValidationResponse;

	}
	
}
