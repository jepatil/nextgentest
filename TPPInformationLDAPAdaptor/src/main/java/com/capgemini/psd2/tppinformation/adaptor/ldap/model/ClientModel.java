package com.capgemini.psd2.tppinformation.adaptor.ldap.model;

import java.util.List;

public class ClientModel {

	private String clientId;
	private List<String> redirectUris;
	private List<String> grantTypes;
	private String name;
	private String description;
	private String logoUrl;
	private String validateUsingAllEligibleAtms;
	private String refreshRolling;
	private String persistentGrantExpirationType;
	private String persistentGrantExpirationTime;
	private String persistentGrantExpirationTimeUnit;
	private String bypassApprovalPage;
	private String restrictScopes;
	private List<String> restrictedScopes;
	private List<String> exclusiveScopes;
	private List<String> restrictedResponseTypes;
	private String requireSignedRequests;
	

	public String getClientId() {
		return this.clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public List<String> getRedirectUris() {
		return this.redirectUris;
	}

	public void setRedirectUris(List<String> redirectUris) {
		this.redirectUris = redirectUris;
	}


	public List<String> getGrantTypes() {
		return this.grantTypes;
	}

	public void setGrantTypes(List<String> grantTypes) {
		this.grantTypes = grantTypes;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getExclusiveScopes() {
		return exclusiveScopes;
	}

	public void setExclusiveScopes(List<String> exclusiveScopes) {
		this.exclusiveScopes = exclusiveScopes;
	}

	public List<String> getRestrictedResponseTypes() {
		return restrictedResponseTypes;
	}

	public void setRestrictedResponseTypes(List<String> restrictedResponseTypes) {
		this.restrictedResponseTypes = restrictedResponseTypes;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLogoUrl() {
		return this.logoUrl;
	}

	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}



	public String getValidateUsingAllEligibleAtms() {
		return this.validateUsingAllEligibleAtms;
	}

	public void setValidateUsingAllEligibleAtms(String validateUsingAllEligibleAtms) {
		this.validateUsingAllEligibleAtms = validateUsingAllEligibleAtms;
	}

	public String getRefreshRolling() {
		return this.refreshRolling;
	}

	public void setRefreshRolling(String refreshRolling) {
		this.refreshRolling = refreshRolling;
	}

	public String getPersistentGrantExpirationType() {
		return this.persistentGrantExpirationType;
	}

	public void setPersistentGrantExpirationType(String persistentGrantExpirationType) {
		this.persistentGrantExpirationType = persistentGrantExpirationType;
	}

	public String getPersistentGrantExpirationTime() {
		return this.persistentGrantExpirationTime;
	}

	public void setPersistentGrantExpirationTime(String persistentGrantExpirationTime) {
		this.persistentGrantExpirationTime = persistentGrantExpirationTime;
	}

	public String getPersistentGrantExpirationTimeUnit() {
		return this.persistentGrantExpirationTimeUnit;
	}

	public void setPersistentGrantExpirationTimeUnit(String persistentGrantExpirationTimeUnit) {
		this.persistentGrantExpirationTimeUnit = persistentGrantExpirationTimeUnit;
	}

	public String getBypassApprovalPage() {
		return this.bypassApprovalPage;
	}

	public void setBypassApprovalPage(String bypassApprovalPage) {
		this.bypassApprovalPage = bypassApprovalPage;
	}

	public String getRestrictScopes() {
		return this.restrictScopes;
	}

	public void setRestrictScopes(String restrictScopes) {
		this.restrictScopes = restrictScopes;
	}

	public List<String> getRestrictedScopes() {
		return this.restrictedScopes;
	}

	public void setRestrictedScopes(List<String> restrictedScopes) {
		this.restrictedScopes = restrictedScopes;
	}

	public String getRequireSignedRequests() {
		return this.requireSignedRequests;
	}

	public void setRequireSignedRequests(String requireSignedRequests) {
		this.requireSignedRequests = requireSignedRequests;
	}

}