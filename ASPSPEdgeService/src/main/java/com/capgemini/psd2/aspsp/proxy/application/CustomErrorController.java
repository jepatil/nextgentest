package com.capgemini.psd2.aspsp.proxy.application;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class CustomErrorController implements ErrorController {
 
    @Value("${error.path:/error}")
    private String errorPath;
     
    @Override
    public String getErrorPath() {
        return errorPath;
    }
    
    @Value("${app.sendErrorPayload:#{false}}")
	private boolean sendErrorPayload;
	
    @RequestMapping(value = "${error.path:/error}", produces = "application/json")
    @ResponseBody
    public ResponseEntity<ErrorInfo> error(HttpServletRequest request) {
        int status = getErrorStatus(request);
        ErrorInfo ex = new ErrorInfo();
        if(sendErrorPayload){
        	ex.setErrorCode(String.valueOf(status));        	
        }
        return ResponseEntity.status(status).body(ex);
    }
 
    private int getErrorStatus(HttpServletRequest request) {
        Integer statusCode = (Integer)request.getAttribute("javax.servlet.error.status_code");
        return statusCode != null ? statusCode : HttpStatus.INTERNAL_SERVER_ERROR.value();
    }
 
}