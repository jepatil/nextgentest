/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.adapter.exceptions.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;

/**
 * The Class AdapterExceptionTest.
 */
public class AdapterExceptionTest {

	
	/**
	 * Test populate PSD 2 exception.
	 */
	@Test
	public void testPopulatePSD2Exception(){
		AdapterErrorCodeEnum errorCodeEnum= AdapterErrorCodeEnum.BAD_REQUEST;
		assertEquals("Bad request.Please check your request" ,AdapterException.populatePSD2Exception(errorCodeEnum).getMessage());
	}
	
	/**
	 * Test populate PSD 2 exception with detail message.
	 */
	@Test
	public void testPopulatePSD2ExceptionWithDetailMessage(){
		AdapterErrorCodeEnum errorCodeEnum= AdapterErrorCodeEnum.BAD_REQUEST;
		assertEquals("Error Found" ,AdapterException.populatePSD2Exception("Error Found",errorCodeEnum).getMessage());
	}
}
