package com.capgemini.psd2.integration.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.integration.service.IntentIdValidationService;

@RestController
public class IntentIdValidationController {
	
	@Autowired
	private IntentIdValidationService intentIdValidationService;
	
	@RequestMapping(value="/validateIntentId/{intentId}",method = RequestMethod.GET)
	public void validateIntentId(@PathVariable("intentId") String intentId,@RequestHeader("client_id") String clientId,@RequestHeader("scope") String scope) {
		intentIdValidationService.validateIntentId(intentId, clientId, scope);
	}
}
