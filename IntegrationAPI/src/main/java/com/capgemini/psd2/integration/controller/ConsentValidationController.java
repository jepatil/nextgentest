package com.capgemini.psd2.integration.controller;

import java.time.ZonedDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.consent.domain.PispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.adapter.PispConsentAdapter;

@RestController
public class ConsentValidationController {
	
	@Autowired
	private AispConsentAdapter aispConsentAdapter;
	
	@Autowired
	private PispConsentAdapter pispConsentAdapter;



	@RequestMapping(value = "/validateconsent/{intentId}", method = RequestMethod.GET)
	public void validateConsent(@PathVariable String intentId,
			@RequestHeader("scope") String scope,@RequestHeader("client_id") String clientId) {
		ZonedDateTime currentDate = null;
		ZonedDateTime consentExpiry =  null;
		if(clientId == null || clientId.trim().isEmpty()) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.CLIENT_ID_MISSING_IN_REQUEST);
		}
		if((PSD2Constants.OPENID_ACCOUNTS.equals(scope)) || (PSD2Constants.ACCOUNTS.equals(scope))){
			AispConsent aispConsent = aispConsentAdapter.retrieveConsentByAccountRequestId(intentId, ConsentStatusEnum.AUTHORISED);
			if(aispConsent == null){
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.CONSENT_DATA_NOT_PRESENT);
			}
			if(!clientId.equalsIgnoreCase(aispConsent.getTppCId())){
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.INVALID_TPP_USER_ACCESS);
			}
			String endDate = aispConsent.getEndDate();
			currentDate = ZonedDateTime.now();
			if(endDate!=null)
				consentExpiry = ZonedDateTime.parse(endDate);
			
		}
		else if((PSD2Constants.OPENID_PAYMENTS.equals(scope)) || (PSD2Constants.PAYMENTS.equals(scope))){
			PispConsent pispConsent = pispConsentAdapter.retrieveConsentByPaymentId(intentId, ConsentStatusEnum.AUTHORISED);
			if(pispConsent == null){
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.CONSENT_DATA_NOT_PRESENT);
			}
			if(!clientId.equalsIgnoreCase(pispConsent.getTppCId())){
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.INVALID_TPP_USER_ACCESS);				
			}
			String endDate = pispConsent.getEndDate();
			currentDate = ZonedDateTime.now();
			if(endDate!=null)
				consentExpiry = ZonedDateTime.parse(endDate);
		}
		
		if (consentExpiry!=null && consentExpiry.isBefore(currentDate)) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.CONSENT_EXPIRED);
		}
	}
}