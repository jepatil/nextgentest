package com.capgemini.psd2.integration.dtos;

import com.capgemini.psd2.token.TPPInformation;

public class TPPInformationDTO {

	private String clientId;
	private TPPInformation tppInformation;
	
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public TPPInformation getTppInformation() {
		return tppInformation;
	}
	public void setTppInformation(TPPInformation tppInformation) {
		this.tppInformation = tppInformation;
	}
	
	
	
}
