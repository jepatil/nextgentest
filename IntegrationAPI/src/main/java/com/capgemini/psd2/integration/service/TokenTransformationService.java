package com.capgemini.psd2.integration.service;

import com.capgemini.psd2.token.Token;

public interface TokenTransformationService {
	
	public void transformAISPToken(Token token,String accountRequestId);
	public void transformPISPToken(Token token,String paymentRequestId);
	

}
