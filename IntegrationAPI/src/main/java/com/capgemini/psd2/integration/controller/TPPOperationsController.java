package com.capgemini.psd2.integration.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.integration.dtos.TPPInformationDTO;
import com.capgemini.psd2.integration.service.TPPInformationService;


@RestController
public class TPPOperationsController {

	@Autowired
	private TPPInformationService tppInformationService;
	
	@RequestMapping(value="/getTppInformation/{clientId}",method = RequestMethod.GET)
	public TPPInformationDTO findTPPInformation(@PathVariable("clientId") String clientId) throws Exception{
		if(clientId == null || clientId.trim().isEmpty()){
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.CLIENT_ID_MISSING_IN_REQUEST);
		}
		return tppInformationService.findTPPInformation(clientId);
	}
	
	@RequestMapping(value="/validateTPP/{clientId}",method = RequestMethod.GET)
	public void validateTPP(@PathVariable("clientId") String clientId) throws Exception{
		boolean tppBlock = Boolean.FALSE;
		TPPInformationDTO tppInformation = tppInformationService.findTPPInformation(clientId);
		if(tppInformation.getTppInformation().getTppBlock() != null && !tppInformation.getTppInformation().getTppBlock().isEmpty()){
			tppBlock = Boolean.valueOf(tppInformation.getTppInformation().getTppBlock());
		}
		if(tppBlock) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.TPP_BLOCKED);
		}
	}
}
