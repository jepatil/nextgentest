package com.capgemini.psd2.integration.controller.test;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.account.request.mongo.db.adapter.repository.AccountRequestRepository;
import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.Data1;
import com.capgemini.psd2.aisp.domain.Data1.StatusEnum;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.consent.domain.PispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.integration.controller.UpdateIntentController;
import com.capgemini.psd2.pisp.adapter.PaymentSetupAdapterHelper;
import com.capgemini.psd2.pisp.adapter.PispConsentAdapter;

public class UpdateIntentControllerTest {
	@Mock
	private AispConsentAdapter aispConsentAdapter;

	@Mock
	private PispConsentAdapter pispConsentAdapter;

	@Mock
	private PaymentSetupAdapterHelper paymentSetupAdapterHelper;

	@Mock
	private AccountRequestAdapter accountRequestAdapter;
	
	@Mock
	private AccountRequestRepository accountRequestRepository;

	@InjectMocks
	private UpdateIntentController controller;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test(expected=PSD2Exception.class)
	public void updateIndentScopeNull(){
		controller.updateIndent("1234", null);
	}

	@Test(expected = PSD2Exception.class)
	public void updateIndentAispConsentNullTest() {
		AispConsent aispConsent = null;
		Mockito.when(aispConsentAdapter.retrieveConsentByAccountRequestId(anyObject(), anyObject()))
				.thenReturn(aispConsent);
		controller.updateIndent("1234", "OPENID ACCOUNTS");
	}

	@Test
	public void updateIndentAispConsentNotNullTest() {
		Data1 data1=new Data1();
		data1.setStatus(StatusEnum.AUTHORISED);
		when(accountRequestRepository.findByAccountRequestId(anyString())).thenReturn(data1);
		AispConsent aispConsent = new AispConsent();
		aispConsent.setConsentId("1234");
		aispConsent.setStatus(ConsentStatusEnum.EXPIRED);
		Mockito.when(aispConsentAdapter.retrieveConsentByAccountRequestId(anyObject(), anyObject()))
				.thenReturn(aispConsent);
		controller.updateIndent("1234", "OPENID ACCOUNTS");
	}

	@Test(expected = PSD2Exception.class)
	public void updateIndentPispConsentNullTest() {
		PispConsent pispConsent = null;
		Mockito.when(pispConsentAdapter.retrieveConsentByPaymentId(anyObject(), anyObject())).thenReturn(pispConsent);
		controller.updateIndent("1234", "OPENID PAYMENTS");
	}

	@Test
	public void updateIndentPispConsentNotNullTest() {
		PispConsent pispConsent = new PispConsent();
		pispConsent.setConsentId("1234");
		pispConsent.setChannelId("12345");
		pispConsent.setPsuId("1234");
		Mockito.when(pispConsentAdapter.retrieveConsentByPaymentId(anyObject(), anyObject())).thenReturn(pispConsent);
		controller.updateIndent("1234", "OPENID PAYMENTS");
	}
	
	@Test(expected = PSD2Exception.class)
	public void updateIndentPispChannelIdNullTest() {
		PispConsent pispConsent = new PispConsent();
		pispConsent.setConsentId("1234");
		Mockito.when(pispConsentAdapter.retrieveConsentByPaymentId(anyObject(), anyObject())).thenReturn(pispConsent);
		controller.updateIndent("1234", "OPENID PAYMENTS");
	}
}
