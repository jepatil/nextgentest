package com.capgemini.psd2.account.product.test.mock.data;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;

import com.capgemini.psd2.account.product.routing.adapter.impl.AccountProductRoutingAdapter;
import com.capgemini.psd2.account.product.routing.adapter.routing.AccountProductCoreSystemAdapterFactory;

import com.capgemini.psd2.aisp.adapter.AccountProductsAdapter;

public class AccountProductAdapterFactoryTest 
{
	/** The application context. */
	@Mock
	private ApplicationContext applicationContext;
	
	/** The account products core system adapter factory. */
	@InjectMocks
	private AccountProductCoreSystemAdapterFactory accountProductCoreSystemAdapterFactory;
	
	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Test account products adapter.
	 */
	@Test
	public void testAccountProductsAdapter() {
		AccountProductsAdapter accountProductsAdapter = new AccountProductRoutingAdapter();
		when(applicationContext.getBean(anyString())).thenReturn(accountProductsAdapter);
		AccountProductsAdapter accountProductsAdapterResult = (AccountProductRoutingAdapter) accountProductCoreSystemAdapterFactory.getAdapterInstance("accountProductAdapter");
		assertEquals(accountProductsAdapter, accountProductsAdapterResult);
	}
	

}
