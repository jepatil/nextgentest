package com.capgemini.psd2.account.product.routing.adapter.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;


import com.capgemini.psd2.account.product.routing.adapter.routing.AccountProductAdapterFactory;

import com.capgemini.psd2.aisp.adapter.AccountProductsAdapter;
import com.capgemini.psd2.aisp.domain.ProductGETResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;

public class AccountProductRoutingAdapter implements AccountProductsAdapter
{
	/** The account product adapter factory. */
	@Autowired
	private AccountProductAdapterFactory accountProductAdapterFactory;

	/** The default adapter. */
	@Value("${app.defaultAccountProductsAdapter}")
	private String defaultAdapter;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.capgemini.psd2.aisp.adapter.AccountInformationAdapter#
	 * retrieveAccountInformation(com.capgemini.psd2.aisp.domain.AccountMapping,
	 * java.util.Map)
	 */
	@Override
	public ProductGETResponse retrieveAccountProducts(AccountMapping accountMapping, Map<String, String> params) {
		AccountProductsAdapter accountProductsAdapter = accountProductAdapterFactory
				.getAdapterInstance(defaultAdapter);
		
		return accountProductsAdapter.retrieveAccountProducts(accountMapping, params);
	}
	

}
