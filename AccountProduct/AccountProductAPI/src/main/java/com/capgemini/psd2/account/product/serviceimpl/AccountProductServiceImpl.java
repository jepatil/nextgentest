package com.capgemini.psd2.account.product.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.account.product.service.AccountProductService;
import com.capgemini.psd2.aisp.adapter.AccountProductsAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.MetaData;
import com.capgemini.psd2.aisp.domain.ProductGETResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.logger.RequestHeaderAttributes;

@Service
public class AccountProductServiceImpl implements AccountProductService {

	/** The req header atrributes. */
	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	/** The account product adapter. */
	@Autowired
	@Qualifier("accountProductAdapterImpl")
	private AccountProductsAdapter accountProductAdapter;

	/** The aisp consent adapter. */
	@Autowired
	private AispConsentAdapter aispConsentAdapter;

	@Override
	public ProductGETResponse retrieveAccountProduct(String accountId) {

		AccountMapping accountMapping = aispConsentAdapter.retrieveAccountMappingByAccountId(
				reqHeaderAtrributes.getToken().getConsentTokenData().getConsentId(), accountId);

		ProductGETResponse productGETResponse = accountProductAdapter.retrieveAccountProducts(accountMapping,
				reqHeaderAtrributes.getToken().getSeviceParams());

		if (productGETResponse.getLinks() == null)
			productGETResponse.setLinks(new Links());

		if (productGETResponse.getMeta() == null)
			productGETResponse.setMeta(new MetaData());

		productGETResponse.getLinks().setSelf(reqHeaderAtrributes.getSelfUrl());
		productGETResponse.getMeta().setTotalPages(1);
		return productGETResponse;
	}

}
