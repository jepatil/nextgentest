package com.capgemini.psd2.account.product.service;

import com.capgemini.psd2.aisp.domain.ProductGETResponse;


/**
 * The Interface AccountProductService.
 */

@FunctionalInterface
public interface AccountProductService {

	/**
	 *
	 * Retrieve account product.
	 * @param accountId the account id
	 * @return the ProductGETResponse
	 */
	public ProductGETResponse retrieveAccountProduct(String accountId);
}
