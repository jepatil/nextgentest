package com.capgemini.psd2.account.product.test.mongo.db.impl;

import static org.junit.Assert.assertNotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.capgemini.psd2.account.product.mongo.db.adapter.impl.AccountProductMongoDbAdapterImpl;
import com.capgemini.psd2.aisp.domain.ProductGETResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountProductMongoDbAdapterImplTest 
{
	/**
	 * Sets the up.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	/** The account product mongo DB adapter impl. */
	@InjectMocks
	private AccountProductMongoDbAdapterImpl accountProductMongoDbAdapterImpl;

	/**
	 * Test retrieve blank response account product success flow.
	 */
	/*@Test
	public void testRetrieveAccountProductSuccessFlow() {
		ProductGETResponse productGETResponse = accountProductMongoDbAdapterImpl.retrieveAccountProducts(null, null);
		assertTrue(productGETResponse.getData().getProduct().isEmpty());
		
	}*/
	
	@Test
	public void testRetrieveAccountProductSuccessFlow() {
		AccountMapping accountMapping= new AccountMapping();
		ArrayList<AccountDetails> accntDetails= new ArrayList<>();
		AccountDetails accountDetails= new AccountDetails();
		Map<String, String> params = new HashMap<>();
		accntDetails.add(accountDetails);
		accountMapping.setAccountDetails(accntDetails);
		accountDetails.setAccountId("1234");
		ProductGETResponse productResponse = accountProductMongoDbAdapterImpl.retrieveAccountProducts(accountMapping,params);
		assertNotNull(productResponse);
		
	}
	}
	

