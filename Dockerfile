FROM alpine-oraclejdk8
VOLUME /tmp
RUN mkdir /classpath && chmod -R 774 /classpath
ADD target/*.jar CoreSystemAdapter-1.0.0.jar