package com.capgemini.psd2.pisp.payment.submission.retrieve.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.PaymentSubmissionPlatformAdapter;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseLinks;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseMeta;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse1;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse1.StatusEnum;
import com.capgemini.psd2.pisp.domain.PaymentSubmissionPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentSubmitPOST201Response;
import com.capgemini.psd2.pisp.domain.SubmissionRetrieveGetRequest;
import com.capgemini.psd2.pisp.payment.submission.retrieve.service.PaymentSubmissionRetrieveService;

@Service
public class PaymentSubmissionRetrieveServiceImpl implements PaymentSubmissionRetrieveService {

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Autowired
	private PaymentSubmissionPlatformAdapter submissionPlatformAdapter;

	@Override
	public PaymentSubmitPOST201Response retrievePaymentSubmissionResource(
			SubmissionRetrieveGetRequest submissionRetrieveRequest) {

		PaymentSubmissionPlatformResource submissionApiResponse = submissionPlatformAdapter
				.retrievePaymentSubmissionResource(submissionRetrieveRequest.getPaymentSubmissionId().trim());

		if (submissionApiResponse == null)
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_NO_PAYMENT_SUBMISSION_ID_FOUND_IN_SYSTEM);

		if (!submissionApiResponse.getTppCID().equalsIgnoreCase(reqHeaderAtrributes.getTppCID()))
			throw PSD2Exception
					.populatePSD2Exception(ErrorCodeEnum.PISP_NO_PAYMENT_SUBMISSION_RESOURCES_FOUND_AGAINST_TPP);

		return paymentSubmissionResponseTransformer(submissionApiResponse);
	}

	private PaymentSubmitPOST201Response paymentSubmissionResponseTransformer(
			PaymentSubmissionPlatformResource paymentSubmissionPlatformResource) {

		PaymentSubmitPOST201Response paymentSubmissionResponse = new PaymentSubmitPOST201Response();
		PaymentSetupResponse1 paymentSetupResponse = new PaymentSetupResponse1();
		paymentSetupResponse.setPaymentSubmissionId(paymentSubmissionPlatformResource.getPaymentSubmissionId());
		paymentSetupResponse.setPaymentId(paymentSubmissionPlatformResource.getPaymentId());
		paymentSetupResponse.setStatus(
				StatusEnum.fromValue(paymentSubmissionPlatformResource.getStatus()));
		paymentSetupResponse.setCreationDateTime(paymentSubmissionPlatformResource.getCreatedAt());
		paymentSubmissionResponse.setData(paymentSetupResponse);
		paymentSubmissionResponse.setLinks(populateLinks());
		paymentSubmissionResponse.setMeta(new PaymentSetupPOSTResponseMeta());
		return paymentSubmissionResponse;
	}

	private PaymentSetupPOSTResponseLinks populateLinks() {

		PaymentSetupPOSTResponseLinks responseLink = new PaymentSetupPOSTResponseLinks();
		responseLink.setSelf(reqHeaderAtrributes.getSelfUrl());
		return responseLink;

	}

}
