package com.capgemini.psd2.pisp.payment.submission.retrieve;

import javax.servlet.Filter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.capgemini.psd2.logger.PSD2Filter;

@EnableEurekaClient
@SpringBootApplication
@ComponentScan(basePackages = {"com.capgemini.psd2"})
@EnableMongoRepositories(basePackages={"com.capgemini.psd2"})
public class PaymentSubmissionRetrieveApplication {

	static ConfigurableApplicationContext context = null;
	public static void main(String[] args) {
		context = SpringApplication.run(PaymentSubmissionRetrieveApplication.class, args);
	}

	@Bean(name="psd2Filter")
	public Filter psd2Filter(){
		return new PSD2Filter(); 
	}				
	
}
