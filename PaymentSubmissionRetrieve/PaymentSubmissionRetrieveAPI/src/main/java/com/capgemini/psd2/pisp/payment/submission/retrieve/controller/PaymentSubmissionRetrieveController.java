package com.capgemini.psd2.pisp.payment.submission.retrieve.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.PaymentSubmitPOST201Response;
import com.capgemini.psd2.pisp.domain.SubmissionRetrieveGetRequest;
import com.capgemini.psd2.pisp.payment.submission.retrieve.service.PaymentSubmissionRetrieveService;
import com.capgemini.psd2.pisp.validation.adapter.PaymentValidator;

@RestController
public class PaymentSubmissionRetrieveController {

	@Autowired
	private PaymentSubmissionRetrieveService paymentSubmissionRetrieveService;

	@Autowired
	private PaymentValidator submissionValidator;

	@Value("${app.responseErrorMessageEnabled}")
	private String responseErrorMessageEnabled;

	@RequestMapping(value = "/payment-submissions/{paymentSubmissionId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)  
	@ResponseBody
	public ResponseEntity<PaymentSubmitPOST201Response> retrievePaymentSubmissionResource(
			SubmissionRetrieveGetRequest submissionRetrieveRequest) {

		try {
			submissionValidator.validateSubmissionRetrieveRequest(submissionRetrieveRequest);
			PaymentSubmitPOST201Response submissionResponse = paymentSubmissionRetrieveService
					.retrievePaymentSubmissionResource(submissionRetrieveRequest);
			return new ResponseEntity<>(submissionResponse, HttpStatus.OK);

		} catch (PSD2Exception psd2Exception) {
			if (Boolean.valueOf(responseErrorMessageEnabled))
				throw psd2Exception;
			return new ResponseEntity<>(
					HttpStatus.valueOf(Integer.parseInt(psd2Exception.getErrorInfo().getStatusCode())));
		}

	}
}
