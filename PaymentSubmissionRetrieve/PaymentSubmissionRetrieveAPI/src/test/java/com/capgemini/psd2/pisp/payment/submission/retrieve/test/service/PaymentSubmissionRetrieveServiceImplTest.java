package com.capgemini.psd2.pisp.payment.submission.retrieve.test.service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.PaymentSubmissionPlatformAdapter;
import com.capgemini.psd2.pisp.domain.PaymentSubmissionPlatformResource;
import com.capgemini.psd2.pisp.domain.SubmissionRetrieveGetRequest;
import com.capgemini.psd2.pisp.payment.submission.retrieve.service.impl.PaymentSubmissionRetrieveServiceImpl;
import com.capgemini.psd2.pisp.payment.submission.retrieve.test.mock.data.PaymentSubmissionRetrieveTestMockData;

@RunWith(SpringJUnit4ClassRunner.class)
public class PaymentSubmissionRetrieveServiceImplTest {

	@Mock
	private RequestHeaderAttributes requestHeaderAttributes;
	
	@Mock
	private PaymentSubmissionPlatformAdapter submissionPlatformAdapter;

	@InjectMocks
	private PaymentSubmissionRetrieveServiceImpl service;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testPaymentSubmissionRetrievePositive() {
		SubmissionRetrieveGetRequest submissionRetrieveRequest = new SubmissionRetrieveGetRequest();
		submissionRetrieveRequest.setPaymentSubmissionId("12345");
		PaymentSubmissionPlatformResource paymentSubmissionPlatformResource = PaymentSubmissionRetrieveTestMockData.getPaymentsubmissionPlatformResource();
		Mockito.when(submissionPlatformAdapter.retrievePaymentSubmissionResource(submissionRetrieveRequest.getPaymentSubmissionId())).thenReturn(paymentSubmissionPlatformResource);
		Mockito.when(requestHeaderAttributes.getTppCID()).thenReturn("MoneyWise.com");
		Mockito.when(requestHeaderAttributes.getSelfUrl()).thenReturn("https://localhost:8085/payment-submissions/" + submissionRetrieveRequest.getPaymentSubmissionId());
		
		service.retrievePaymentSubmissionResource(submissionRetrieveRequest);
	}
	
	@Test
	public void testPaymentSubmissionRetrievePositiveSelfLink() {
		SubmissionRetrieveGetRequest submissionRetrieveRequest = new SubmissionRetrieveGetRequest();
		submissionRetrieveRequest.setPaymentSubmissionId("12345");
		PaymentSubmissionPlatformResource paymentSubmissionPlatformResource = PaymentSubmissionRetrieveTestMockData.getPaymentsubmissionPlatformResource();
		Mockito.when(submissionPlatformAdapter.retrievePaymentSubmissionResource(submissionRetrieveRequest.getPaymentSubmissionId())).thenReturn(paymentSubmissionPlatformResource);
		Mockito.when(requestHeaderAttributes.getTppCID()).thenReturn("MoneyWise.com");
		Mockito.when(requestHeaderAttributes.getSelfUrl()).thenReturn("/");
		
		service.retrievePaymentSubmissionResource(submissionRetrieveRequest);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testPaymentSubmissionRetrieveException() {
		SubmissionRetrieveGetRequest submissionRetrieveRequest = new SubmissionRetrieveGetRequest();
		submissionRetrieveRequest.setPaymentSubmissionId("12345");
		Mockito.when(submissionPlatformAdapter.retrievePaymentSubmissionResource(submissionRetrieveRequest.getPaymentSubmissionId())).thenReturn(null);
		Mockito.when(requestHeaderAttributes.getTppCID()).thenReturn("MoneyWise.com");
		Mockito.when(requestHeaderAttributes.getSelfUrl()).thenReturn("https://localhost:8085/payment-submissions/" + submissionRetrieveRequest.getPaymentSubmissionId());
		
		service.retrievePaymentSubmissionResource(submissionRetrieveRequest);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testPaymentSubmissionRetrieveInvalidTppId() {
		SubmissionRetrieveGetRequest submissionRetrieveRequest = new SubmissionRetrieveGetRequest();
		submissionRetrieveRequest.setPaymentSubmissionId("12345");
		PaymentSubmissionPlatformResource paymentSubmissionPlatformResource = PaymentSubmissionRetrieveTestMockData.getPaymentsubmissionPlatformResource();
		Mockito.when(submissionPlatformAdapter.retrievePaymentSubmissionResource(submissionRetrieveRequest.getPaymentSubmissionId())).thenReturn(paymentSubmissionPlatformResource);
		Mockito.when(requestHeaderAttributes.getTppCID()).thenReturn("NoMoneyWise.com");
		
		service.retrievePaymentSubmissionResource(submissionRetrieveRequest);
	}

	@After
	public void tearDown() throws Exception {
		service = null;
		requestHeaderAttributes = null;
	}
}