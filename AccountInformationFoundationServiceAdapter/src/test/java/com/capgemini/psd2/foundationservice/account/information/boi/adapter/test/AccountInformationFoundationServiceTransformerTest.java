/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.information.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.domain.Account;
import com.capgemini.psd2.aisp.domain.AccountGETResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.domain.Accnt;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.domain.Accnts;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.domain.Jurisdiction;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.transformer.AccountInformationFoundationServiceTransformer;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.validator.impl.PSD2ResponseValidatorImpl;


/**
 * The Class AccountInformationFoundationServiceTransformerTest.
 */

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountInformationFoundationServiceTransformerTest {

	@Mock
	private PSD2ResponseValidatorImpl validator;
	
	
	/** The account information FS transformer. */
	@InjectMocks
	private AccountInformationFoundationServiceTransformer accountInformationFSTransformer;
	
	
	
	/**
	 * Sets the up.
	 */
	@Before
	public void setUp(){
		
		MockitoAnnotations.initMocks(this);
		
	}
	
	@Test
	public void contextLoads() {
	}
	
	/**
	 * Test transform account information.
	 */
	@Test
	public void testTransformAccountInformation() {
		Map<String, String> params = new HashMap<>();
		params.put("accountId", "12345");
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, "AISP");
		Accnts accounts = new Accnts();
		Accnt acc = new Accnt();
		acc.setAccountNumber("acct1234");
		acc.setAccountNSC("nsc1234");
		acc.setCurrency("GBP");
		acc.setAccountName("BOI_PSD2");
		accounts.getAccount().add(acc);
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
	    
		
		//setting the account filtering object
		Map<String, Map<String, List<String>>> accountFiltering =new HashMap<>();
	        
			Map<String,List<String>> map1 = new HashMap<String,List<String>>();
	        
		        List <String> accountType=new ArrayList<>();
		        accountType.add("Current Account");
		        map1.put("AISP", accountType);
		        map1.put("CISP", accountType);
		        map1.put("PISP", accountType);
	        
	    accountFiltering.put("accountType", map1);
	        
	        Map<String,List<String>> map2 = new HashMap<String,List<String>>();
	        
		        List <String> jurisdictionList=new ArrayList<>();
		        jurisdictionList.add("NORTHERN_IRELAND.SchemeName=BBAN");
		        jurisdictionList.add("NORTHERN_IRELAND.Identification=AccountNumber");
		        jurisdictionList.add("NORTHERN_IRELAND.ServicerSchemeName=UKSORTCODE");
		        jurisdictionList.add("NORTHERN_IRELAND.ServicerIdentification=AccountNSCNumber");
		        jurisdictionList.add("GREAT_BRITAIN.SchemeName=BBAN");
		        jurisdictionList.add("GREAT_BRITAIN.Identification=AccountNumber");
		        jurisdictionList.add("GREAT_BRITAIN.ServicerSchemeName=UKSORTCODE");
		        jurisdictionList.add("GREAT_BRITAIN.ServicerIdentification=AccountNSCNumber");
		        
		        map2.put("AISP", jurisdictionList);
		        map2.put("CISP", jurisdictionList);
		        map2.put("PISP", jurisdictionList);
	        
	     accountFiltering.put("jurisdiction", map2);	
	        
	        Map<String,List<String>> map3 = new HashMap<String,List<String>>();
	        
		        List <String> permissionList1=new ArrayList<>();
		        permissionList1.add("A");
		        permissionList1.add("V");
		        List <String> permissionList2=new ArrayList<>();
		        permissionList2.add("A");
		        permissionList2.add("X");
		        
		        map3.put("AISP", permissionList1);
		        map3.put("CISP", permissionList1);
		        map3.put("PISP", permissionList2);
	        
	     accountFiltering.put("permission", map3);
	        
		accountInformationFSTransformer.setAccountFiltering(accountFiltering);
		accountInformationFSTransformer.getAccountFiltering();
//		
		Map<String, Map<String, String>> jd = accountInformationFSTransformer.getJurisdiction();
		accountInformationFSTransformer.setJurisdiction(jd);
		Account responseDataObj = new Account();
		responseDataObj.setAccountId(params.get("accountId"));
		//Mockito.when(accountInformationFSTransformer.transformAccountInformation(any(), any())).thenReturn(new AccountGETResponse());
		AccountGETResponse res = accountInformationFSTransformer.transformAccountInformation(accounts, params);
		assertNotNull(res);
		

	}
	
	
	/**
	 * Test transform account information curr as null.
	 */
	@Test(expected = AdapterException.class)
	public void testTransformAccountInformationCurrAsNull() {
		Map<String, String> params = new HashMap<>();
		params.put("accountId", "12345");
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, "AISP");
		Accnts accounts = new Accnts();
		Accnt acc = new Accnt();
		acc.setAccountNumber("acct1234");
		acc.setAccountNSC("nsc1234");
		acc.setCurrency(null);
		acc.setAccountName("BOI_PSD2");
		accounts.getAccount().add(acc);
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		Map<String, Map<String, List<String>>> accountFiltering =new HashMap<>();
        
		Map<String,List<String>> map1 = new HashMap<String,List<String>>();
        
	        List <String> accountType=new ArrayList<>();
	        accountType.add("Current Account");
	        map1.put("AISP", accountType);
	        map1.put("CISP", accountType);
	        map1.put("PISP", accountType);
        
    accountFiltering.put("accountType", map1);
        
        Map<String,List<String>> map2 = new HashMap<String,List<String>>();
        
	        List <String> jurisdictionList=new ArrayList<>();
	        jurisdictionList.add("NORTHERN_IRELAND.SchemeName=BBAN");
	        jurisdictionList.add("NORTHERN_IRELAND.Identification=IBAN");
	        jurisdictionList.add("NORTHERN_IRELAND.ServicerSchemeName=UKSORTCODE");
	        jurisdictionList.add("NORTHERN_IRELAND.ServicerIdentification=AccountNSCNumber");
	        jurisdictionList.add("GREAT_BRITAIN.SchemeName=BBAN");
	        jurisdictionList.add("GREAT_BRITAIN.Identification=AccountNumber");
	        jurisdictionList.add("GREAT_BRITAIN.ServicerSchemeName=UKSORTCODE");
	        jurisdictionList.add("GREAT_BRITAIN.ServicerIdentification=AccountNSCNumber");
	        
	        map2.put("AISP", jurisdictionList);
	        map2.put("CISP", jurisdictionList);
	        map2.put("PISP", jurisdictionList);
        
     accountFiltering.put("jurisdiction", map2);	
        
        Map<String,List<String>> map3 = new HashMap<String,List<String>>();
        
	        List <String> permissionList1=new ArrayList<>();
	        permissionList1.add("A");
	        permissionList1.add("V");
	        List <String> permissionList2=new ArrayList<>();
	        permissionList2.add("A");
	        permissionList2.add("X");
	        
	        map3.put("AISP", permissionList1);
	        map3.put("CISP", permissionList1);
	        map3.put("PISP", permissionList2);
        
     accountFiltering.put("permission", map3);
        
	accountInformationFSTransformer.setAccountFiltering(accountFiltering);
		
		Account responseDataObj = new Account();
		responseDataObj.setAccountId(params.get("accountId"));
		responseDataObj.setCurrency(null);
		doThrow(AdapterException.class).when(validator).validate(any(Accnt.class));
		AccountGETResponse res = accountInformationFSTransformer.transformAccountInformation(accounts, params);
		assertNotNull(res);
	}
	
	/**
	 * Test transform account information acc number as null.
	 */
	@Test(expected = AdapterException.class)
	public void testTransformAccountInformationAccNumberAsNull() {
		Map<String, String> params = new HashMap<>();
		params.put("accountId", "12345");
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, "AISP");
		Accnts accounts = new Accnts();
		
		Accnt acc = new Accnt();
		acc.setAccountNumber(null);
		acc.setAccountNSC("nsc1234");
		acc.setCurrency("GBP");
		acc.setAccountName("BOI_PSD2");
		accounts.getAccount().add(acc);
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		
		
Map<String, Map<String, List<String>>> accountFiltering =new HashMap<>();
        
		Map<String,List<String>> map1 = new HashMap<String,List<String>>();
        
	        List <String> accountType=new ArrayList<>();
	        accountType.add("Current Account");
	        map1.put("AISP", accountType);
	        map1.put("CISP", accountType);
	        map1.put("PISP", accountType);
        
    accountFiltering.put("accountType", map1);
        
        Map<String,List<String>> map2 = new HashMap<String,List<String>>();
        
	        List <String> jurisdictionList=new ArrayList<>();
	        jurisdictionList.add("NORTHERN_IRELAND.SchemeName=BBAN");
	        jurisdictionList.add("NORTHERN_IRELAND.Identification=IBAN");
	        jurisdictionList.add("NORTHERN_IRELAND.ServicerSchemeName=UKSORTCODE");
	        jurisdictionList.add("NORTHERN_IRELAND.ServicerIdentification=AccountNSCNumber");
	        jurisdictionList.add("GREAT_BRITAIN.SchemeName=BBAN");
	        jurisdictionList.add("GREAT_BRITAIN.Identification=AccountNumber");
	        jurisdictionList.add("GREAT_BRITAIN.ServicerSchemeName=UKSORTCODE");
	        jurisdictionList.add("GREAT_BRITAIN.ServicerIdentification=AccountNSCNumber");
	        
	        map2.put("AISP", jurisdictionList);
	        map2.put("CISP", jurisdictionList);
	        map2.put("PISP", jurisdictionList);
        
     accountFiltering.put("jurisdiction", map2);	
        
        Map<String,List<String>> map3 = new HashMap<String,List<String>>();
        
	        List <String> permissionList1=new ArrayList<>();
	        permissionList1.add("A");
	        permissionList1.add("V");
	        List <String> permissionList2=new ArrayList<>();
	        permissionList2.add("A");
	        permissionList2.add("X");
	        
	        map3.put("AISP", permissionList1);
	        map3.put("CISP", permissionList1);
	        map3.put("PISP", permissionList2);
        
     accountFiltering.put("permission", map3);
        
	accountInformationFSTransformer.setAccountFiltering(accountFiltering);



		
		Account responseDataObj = new Account();
		responseDataObj.setAccountId(params.get("accountId"));
		doThrow(AdapterException.class).when(validator).validate(any(Accnt.class));
		AccountGETResponse res = accountInformationFSTransformer.transformAccountInformation(accounts, params);
		assertNotNull(res);
	}
	
	
	
	/**
	 * Test transform account information params null.
	 */
	@Test(expected = AdapterException.class)
	public void testTransformAccountInformationParamsNull() {
		Accnts accounts = new Accnts();
		Accnt acc = new Accnt();
		acc.setAccountNumber("acct1234");
		acc.setAccountNSC("nsc1234");
		acc.setCurrency("GBP");
		acc.setAccountName("1234");
		accounts.getAccount().add(acc);
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		
Map<String, Map<String, List<String>>> accountFiltering =new HashMap<>();
        
		Map<String,List<String>> map1 = new HashMap<String,List<String>>();
        
	        List <String> accountType=new ArrayList<>();
	        accountType.add("Current Account");
	        map1.put("AISP", accountType);
	        map1.put("CISP", accountType);
	        map1.put("PISP", accountType);
        
    accountFiltering.put("accountType", map1);
        
        Map<String,List<String>> map2 = new HashMap<String,List<String>>();
        
	        List <String> jurisdictionList=new ArrayList<>();
	        jurisdictionList.add("NORTHERN_IRELAND.SchemeName=BBAN");
	        jurisdictionList.add("NORTHERN_IRELAND.Identification=IBAN");
	        jurisdictionList.add("NORTHERN_IRELAND.ServicerSchemeName=UKSORTCODE");
	        jurisdictionList.add("NORTHERN_IRELAND.ServicerIdentification=AccountNSCNumber");
	        jurisdictionList.add("GREAT_BRITAIN.SchemeName=BBAN");
	        jurisdictionList.add("GREAT_BRITAIN.Identification=AccountNumber");
	        jurisdictionList.add("GREAT_BRITAIN.ServicerSchemeName=UKSORTCODE");
	        jurisdictionList.add("GREAT_BRITAIN.ServicerIdentification=AccountNSCNumber");
	        
	        map2.put("AISP", jurisdictionList);
	        map2.put("CISP", jurisdictionList);
	        map2.put("PISP", jurisdictionList);
        
     accountFiltering.put("jurisdiction", map2);	
        
        Map<String,List<String>> map3 = new HashMap<String,List<String>>();
        
	        List <String> permissionList1=new ArrayList<>();
	        permissionList1.add("A");
	        permissionList1.add("V");
	        List <String> permissionList2=new ArrayList<>();
	        permissionList2.add("A");
	        permissionList2.add("X");
	        
	        map3.put("AISP", permissionList1);
	        map3.put("CISP", permissionList1);
	        map3.put("PISP", permissionList2);
        
     accountFiltering.put("permission", map3);
        
	accountInformationFSTransformer.setAccountFiltering(accountFiltering);

		
		try{
		accountInformationFSTransformer.transformAccountInformation(accounts, null);
		}
		catch(NullPointerException e){
			throw new AdapterException("message", new ErrorInfo());
		}		
	}
	
	/**
	 * Test transform account information params account id null.
	 */
	@Test(expected = AdapterException.class)
	public void testTransformAccountInformationParamsAccountIdNull() {
		Map<String, String> params = new HashMap<>();
		params.put("accountId", null);
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, "AISP");
		Accnts accounts = new Accnts();
		Accnt acc = new Accnt();
		acc.setAccountNumber("acct1234");
		acc.setAccountNSC("nsc1234");
		acc.setCurrency("GBP");
		acc.setAccountName("1234");
		accounts.getAccount().add(acc);
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		
Map<String, Map<String, List<String>>> accountFiltering =new HashMap<>();
        
		Map<String,List<String>> map1 = new HashMap<String,List<String>>();
        
	        List <String> accountType=new ArrayList<>();
	        accountType.add("Current Account");
	        map1.put("AISP", accountType);
	        map1.put("CISP", accountType);
	        map1.put("PISP", accountType);
        
    accountFiltering.put("accountType", map1);
        
        Map<String,List<String>> map2 = new HashMap<String,List<String>>();
        
	        List <String> jurisdictionList=new ArrayList<>();
	        jurisdictionList.add("NORTHERN_IRELAND.SchemeName=BBAN");
	        jurisdictionList.add("NORTHERN_IRELAND.Identification=AccountNumber");
	        jurisdictionList.add("NORTHERN_IRELAND.ServicerSchemeName=UKSORTCODE");
	        jurisdictionList.add("NORTHERN_IRELAND.ServicerIdentification=AccountNSCNumber");
	        jurisdictionList.add("GREAT_BRITAIN.SchemeName=BBAN");
	        jurisdictionList.add("GREAT_BRITAIN.Identification=AccountNumber");
	        jurisdictionList.add("GREAT_BRITAIN.ServicerSchemeName=UKSORTCODE");
	        jurisdictionList.add("GREAT_BRITAIN.ServicerIdentification=AccountNSCNumber");
	        
	        map2.put("AISP", jurisdictionList);
	        map2.put("CISP", jurisdictionList);
	        map2.put("PISP", jurisdictionList);
        
     accountFiltering.put("jurisdiction", map2);	
        
        Map<String,List<String>> map3 = new HashMap<String,List<String>>();
        
	        List <String> permissionList1=new ArrayList<>();
	        permissionList1.add("A");
	        permissionList1.add("V");
	        List <String> permissionList2=new ArrayList<>();
	        permissionList2.add("A");
	        permissionList2.add("X");
	        
	        map3.put("AISP", permissionList1);
	        map3.put("CISP", permissionList1);
	        map3.put("PISP", permissionList2);
        
     accountFiltering.put("permission", map3);
        
	accountInformationFSTransformer.setAccountFiltering(accountFiltering);

		
		Account responseDataObj = new Account();
		responseDataObj.setAccountId(params.get("accountId"));
		doThrow(AdapterException.class).when(validator).validate(any(Account.class));
		AccountGETResponse res = accountInformationFSTransformer.transformAccountInformation(accounts, params);
		assertNotNull(res);
	}
	
	/**
	 * Test success with iban.
	 */
	@Test
	public void testSuccessWithIban() {
		Map<String, String> params = new HashMap<>();
		params.put("accountId", "12345");
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, "AISP");
		Accnts accounts = new Accnts();
		Accnt acc = new Accnt();
		acc.setIban("acct1234");
		acc.setAccountNSC("nsc1234");
		acc.setCurrency("GBP");
		acc.setJurisdiction(Jurisdiction.REPUBLIC_OF_IRELAND);
		acc.setAccountPermission("A");
		acc.setAccountName("BOI_PSD2");
		accounts.getAccount().add(acc);
		
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		
Map<String, Map<String, List<String>>> accountFiltering =new HashMap<>();
        
		Map<String,List<String>> map1 = new HashMap<String,List<String>>();
        
	        List <String> accountType=new ArrayList<>();
	        accountType.add("Current Account");
	        map1.put("AISP", accountType);
	        map1.put("CISP", accountType);
	        map1.put("PISP", accountType);
        
    accountFiltering.put("accountType", map1);
        
        Map<String,List<String>> map2 = new HashMap<String,List<String>>();
        
	        List <String> jurisdictionList=new ArrayList<>();
	        jurisdictionList.add("REPUBLIC_OF_IRELAND.SchemeName=IBAN");
	        jurisdictionList.add("REPUBLIC_OF_IRELAND.Identification=IBAN");
	        jurisdictionList.add("REPUBLIC_OF_IRELAND.ServicerSchemeName=BICFI");
	        jurisdictionList.add("REPUBLIC_OF_IRELAND.ServicerIdentification=BIC");
	       
	        
	        map2.put("AISP", jurisdictionList);
	        map2.put("CISP", jurisdictionList);
	        map2.put("PISP", jurisdictionList);
        
     accountFiltering.put("jurisdiction", map2);	
        
        Map<String,List<String>> map3 = new HashMap<String,List<String>>();
        
	        List <String> permissionList1=new ArrayList<>();
	        permissionList1.add("A");
	        permissionList1.add("V");
	        List <String> permissionList2=new ArrayList<>();
	        permissionList2.add("A");
	        permissionList2.add("X");
	        
	        map3.put("AISP", permissionList1);
	        map3.put("CISP", permissionList1);
	        map3.put("PISP", permissionList2);
        
     accountFiltering.put("permission", map3);
        
	accountInformationFSTransformer.setAccountFiltering(accountFiltering);

		
		Account responseDataObj = new Account();
		responseDataObj.setAccountId(params.get("accountId"));
	//	Mockito.when(accountInformationFSTransformer.transformAccountInformation(any(), any())).thenReturn(new AccountGETResponse());
		AccountGETResponse res = accountInformationFSTransformer.transformAccountInformation(accounts, params);
		assertNotNull(res);
	}
	
	@Test
	public void testWithIban() {
		Map<String, String> params = new HashMap<>();
		params.put("12345678_nsc123", "12345");
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, "AISP");
		Accnts accounts = new Accnts();
		Accnt acc = new Accnt();
		acc.setAccountNumber("12345678");
		acc.setAccountNSC("nsc123");
		acc.setAccountName("test");
		acc.setIban("acct123");
		acc.setCurrency("GBP");
		acc.setAccountName("BOI_PSD2");
		acc.setJurisdiction(Jurisdiction.NORTHERN_IRELAND);
		accounts.getAccount().add(acc);
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc123");
		accDet.setAccountNumber("12345678");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		
Map<String, Map<String, List<String>>> accountFiltering =new HashMap<>();
        
		Map<String,List<String>> map1 = new HashMap<String,List<String>>();
        
	        List <String> accountType=new ArrayList<>();
	        accountType.add("Current Account");
	        map1.put("AISP", accountType);
	        map1.put("CISP", accountType);
	        map1.put("PISP", accountType);
        
    accountFiltering.put("accountType", map1);
        
        Map<String,List<String>> map2 = new HashMap<String,List<String>>();
        
	        List <String> jurisdictionList=new ArrayList<>();
	        jurisdictionList.add("NORTHERN_IRELAND.SchemeName=SORTCODEACCOUNTNUMBER");
	        jurisdictionList.add("NORTHERN_IRELAND.Identification=SORTCODEACCOUNTNUMBER");
	        jurisdictionList.add("NORTHERN_IRELAND.ServicerSchemeName=");
	        jurisdictionList.add("NORTHERN_IRELAND.ServicerIdentification=");
	        jurisdictionList.add("GREAT_BRITAIN.SchemeName=SORTCODEACCOUNTNUMBER");
	        jurisdictionList.add("GREAT_BRITAIN.Identification=SORTCODEACCOUNTNUMBER");
	        jurisdictionList.add("GREAT_BRITAIN.ServicerSchemeName=");
	        jurisdictionList.add("GREAT_BRITAIN.ServicerIdentification=");
	        
	        map2.put("AISP", jurisdictionList);
	        map2.put("CISP", jurisdictionList);
	        map2.put("PISP", jurisdictionList);
        
     accountFiltering.put("jurisdiction", map2);	
        
        Map<String,List<String>> map3 = new HashMap<String,List<String>>();
        
	        List <String> permissionList1=new ArrayList<>();
	        permissionList1.add("A");
	        permissionList1.add("V");
	        List <String> permissionList2=new ArrayList<>();
	        permissionList2.add("A");
	        permissionList2.add("X");
	        
	        map3.put("AISP", permissionList1);
	        map3.put("CISP", permissionList1);
	        map3.put("PISP", permissionList2);
        
     accountFiltering.put("permission", map3);
        
	accountInformationFSTransformer.setAccountFiltering(accountFiltering);
	accountInformationFSTransformer.getAccountFiltering();
	
		Account responseDataObj = new Account();
		responseDataObj.setAccountId(params.get("accountId"));
		
		AccountGETResponse res = accountInformationFSTransformer.transformAccountInformation(accounts, params);
		assertNotNull(res);
	}
	@Test
	public void testFailurWithIban() {
		Map<String, String> params = new HashMap<>();
		params.put("12345678_nsc123", "12345");
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, "AISP");
		Accnts accounts = new Accnts();
		Accnt acc = new Accnt();
		acc.setAccountNumber("12345678");
		acc.setAccountNSC("nsc123");
		acc.setAccountName("test");
		acc.setIban("acct1234");
		acc.setCurrency("GBP");
		acc.setAccountName("BOI_PSD2");
		acc.setJurisdiction(Jurisdiction.REPUBLIC_OF_IRELAND);
		accounts.getAccount().add(acc);
		
		
		
		
        Map<String, Map<String, List<String>>> accountFiltering =new HashMap<>();
        
		Map<String,List<String>> map1 = new HashMap<String,List<String>>();
        
	        List <String> accountType=new ArrayList<>();
	        accountType.add("Current Account");
	        map1.put("AISP", accountType);
	        map1.put("CISP", accountType);
	        map1.put("PISP", accountType);
        
        accountFiltering.put("accountType", map1);
        
        Map<String,List<String>> map2 = new HashMap<String,List<String>>();
        
	        List <String> jurisdictionList=new ArrayList<>();
	        jurisdictionList.add("REPUBLIC_OF_IRELAND.SchemeName=IBAN");
	        jurisdictionList.add("REPUBLIC_OF_IRELAND.Identification=IBAN");
	        jurisdictionList.add("REPUBLIC_OF_IRELAND.ServicerSchemeName=BICFI");
	        jurisdictionList.add("REPUBLIC_OF_IRELAND.ServicerIdentification=BIC");
	        
	        map2.put("AISP", jurisdictionList);
	        map2.put("CISP", jurisdictionList);
	        map2.put("PISP", jurisdictionList);
        
     accountFiltering.put("jurisdiction", map2);	
        
        Map<String,List<String>> map3 = new HashMap<String,List<String>>();
        
	        List <String> permissionList1=new ArrayList<>();
	        permissionList1.add("A");
	        permissionList1.add("V");
	        List <String> permissionList2=new ArrayList<>();
	        permissionList2.add("A");
	        permissionList2.add("X");
	        
	        map3.put("AISP", permissionList1);
	        map3.put("CISP", permissionList1);
	        map3.put("PISP", permissionList2);
        
     accountFiltering.put("permission", map3);
        
	accountInformationFSTransformer.setAccountFiltering(accountFiltering);
	accountInformationFSTransformer.getAccountFiltering();
		AccountGETResponse res = accountInformationFSTransformer.transformAccountInformation(accounts, params);
		assertNotNull(res);
	}
	
}
	

