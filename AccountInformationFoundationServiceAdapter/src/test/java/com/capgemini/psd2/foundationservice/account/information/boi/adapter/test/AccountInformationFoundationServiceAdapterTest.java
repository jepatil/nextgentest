/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.information.boi.adapter.test;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.domain.AccountGETResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.AccountInformationFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.client.AccountInformationFoundationServiceClient;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.delegate.AccountInformationFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.domain.Accnt;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.domain.Accnts;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.domain.Accounts;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.CustomerAccountsFilterFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion.AdapterFilterUtility;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;

/**
 * The Class AccountInformationFoundationServiceAdapterTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountInformationFoundationServiceAdapterTest {

	
	/** The account information foundation service adapter. */
	@InjectMocks
	private AccountInformationFoundationServiceAdapter accountInformationFoundationServiceAdapter;
	
	/** The account information foundation service delegate. */
	@Mock
	private AccountInformationFoundationServiceDelegate accountInformationFoundationServiceDelegate;
	
	/** The account information foundation service client. */
	@Mock
	private AccountInformationFoundationServiceClient accountInformationFoundationServiceClient;
	
	/** The rest client. */
	@Mock
	private RestClientSyncImpl restClient;
	
	@Mock
	AdapterFilterUtility adapterFilterUtility;
	
	@Mock
	CustomerAccountsFilterFoundationServiceAdapter commonFilterUtility;
	
	/**
	 * Sets the up.
	 */
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	
	/**
	 * Test account information FS.
	 */
	@Test
	public void testAccountInformationFS() {
		Accnts accounts = new Accnts();
		Accnt acc = new Accnt();
		acc.setAccountNumber("acct1234");
		acc.setAccountNSC("nsc1234");
		accounts.getAccount().add(acc);
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts filteredAccounts = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt accntFilter = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt();
		accntFilter.setAccountNumber("US2345");
		accntFilter.setAccountNSC("1234");
		filteredAccounts.getAccount().add(accntFilter);
		
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		accountMapping.setCorrelationId("test");
		Mockito.when(accountInformationFoundationServiceDelegate.getFoundationServiceURL(any(), any(), any())).thenReturn("http://localhost:8082/fs-accountEnquiry-service/services/AccountEnquiry/business/nsc1234/acc1234/account");
		Mockito.when(commonFilterUtility.retrieveCustomerAccountList(any(),any())).thenReturn(filteredAccounts);
		Mockito.when(adapterFilterUtility.prevalidateAccounts(any(), any())).thenReturn(accountMapping);
		Mockito.when(accountInformationFoundationServiceClient.restTransportForSingleAccountInformation(any(), any(), any())).thenReturn(accounts);
		Mockito.when(accountInformationFoundationServiceDelegate.transformResponseFromFDToAPI(any(), any())).thenReturn(new AccountGETResponse());
		Mockito.when(restClient.callForGet(any(), any(), any())).thenReturn(accounts);
	    Mockito.when(accountInformationFoundationServiceDelegate.getAccountFromAccountList(acc.getAccountNumber(), accounts)).thenReturn(acc);
		accountInformationFoundationServiceAdapter.retrieveAccountInformation(accountMapping, new HashMap<String,String>());
		
	}
	
	/**
	 * Test multiple account information FS.
	 */
	@Test
	public void testMultipleAccountInformationFS() {
		Accnts accounts = new Accnts();
		Accnt acc = new Accnt();
		acc.setAccountNumber("acct1234");
		acc.setAccountNSC("nsc1234");
		accounts.getAccount().add(acc);
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts filteredAccounts = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt accntFilter = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt();
		accntFilter.setAccountNumber("US2345");
		accntFilter.setAccountNSC("1234");
		filteredAccounts.getAccount().add(accntFilter);
		
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		accountMapping.setCorrelationId("test");
		Mockito.when(accountInformationFoundationServiceDelegate.getFoundationServiceURL(any(), any(), any())).thenReturn("http://localhost:8082/fs-accountEnquiry-service/services/AccountEnquiry/business/nsc1234/acc1234/account");
		Mockito.when(commonFilterUtility.retrieveCustomerAccountList(any(),any())).thenReturn(filteredAccounts);
		Mockito.when(adapterFilterUtility.prevalidateAccounts(any(), any())).thenReturn(accountMapping);
		Mockito.when(accountInformationFoundationServiceClient.restTransportForMultipleAccountInformation(any(), any(), any())).thenReturn(accounts);
		Mockito.when(accountInformationFoundationServiceDelegate.transformResponseFromFDToAPI(any(), any())).thenReturn(new AccountGETResponse());
		Mockito.when(restClient.callForGet(any(), any(), any())).thenReturn(accounts);
	    Mockito.when(accountInformationFoundationServiceDelegate.getAccountFromAccountList(acc.getAccountNumber(), accounts)).thenReturn(acc);
	    Map params = new HashMap<String,String>();
	    params.put("channelId", "BOL");
		accountInformationFoundationServiceAdapter.retrieveMultipleAccountsInformation(accountMapping, params);
	}
	
	/**
	 * Test account information accnt mapping as null.
	 */
	@Test(expected = AdapterException.class)
	public void testAccountInformationAccntMappingAsNull() {
				
		Mockito.when(accountInformationFoundationServiceDelegate.createRequestHeaders(anyObject(),anyObject(),anyMap())).thenReturn(new HttpHeaders());
		accountInformationFoundationServiceAdapter.retrieveAccountInformation(null, new HashMap<String,String>());
	}
	
	@Test(expected = AdapterException.class)
	public void testParamsAsNull() {
		accountInformationFoundationServiceAdapter.retrieveAccountInformation(new AccountMapping(), null);
	}
	
	/**
	 * Test account information account as null.
	 */
	@Test(expected = AdapterException.class)
	public void testAccountInformationAccountAsNull() {
				
		Mockito.when(accountInformationFoundationServiceClient.restTransportForSingleAccountInformation(anyObject(),anyObject(),anyObject())).thenReturn(null);
		accountInformationFoundationServiceAdapter.retrieveAccountInformation(null, new HashMap<String,String>());
	}
	
	/**
	 * Test exception.
	 */
	@Test(expected = AdapterException.class)
	public void testException(){
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		Mockito.when(accountInformationFoundationServiceDelegate.getFoundationServiceURL(any(), any(), any())).thenReturn("http://localhost:8082/fs-accountEnquiry-service/services/AccountEnquiry/business/nsc1234/acc1234/account");
		Mockito.when(accountInformationFoundationServiceClient.restTransportForSingleAccountInformation(any(), any(), any())).thenReturn(null);
		Mockito.when(accountInformationFoundationServiceDelegate.transformResponseFromFDToAPI(any(), any())).thenReturn(new AccountGETResponse());
		accountInformationFoundationServiceAdapter.retrieveAccountInformation(null, new HashMap<String,String>());
	}
	
	/**
	 * Test exception.
	 */
	@Test(expected = AdapterException.class)
	public void testMultiAccountException(){
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		Mockito.when(accountInformationFoundationServiceDelegate.getFoundationServiceURL(any(), any(), any())).thenReturn("http://localhost:8082/fs-accountEnquiry-service/services/AccountEnquiry/business/nsc1234/acc1234/account");
		Mockito.when(accountInformationFoundationServiceClient.restTransportForSingleAccountInformation(any(), any(), any())).thenReturn(null);
		Mockito.when(accountInformationFoundationServiceDelegate.transformResponseFromFDToAPI(any(), any())).thenReturn(new AccountGETResponse());
		accountInformationFoundationServiceAdapter.retrieveMultipleAccountsInformation(null, new HashMap<String,String>());
	}

	/**
	 * Test null exception.
	 */
	@Test(expected = AdapterException.class)
	public void testNullException(){
		Accnts accounts = new Accnts();
		Accnt acc = new Accnt();
		acc.setAccountNumber("acct1234");
		acc.setAccountNSC("nsc1234");
		accounts.getAccount().add(acc);
		AccountMapping accountMapping =null;
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		Mockito.when(accountInformationFoundationServiceDelegate.getFoundationServiceURL(any(), any(), any())).thenReturn("http://localhost:8082/fs-accountEnquiry-service/services/AccountEnquiry/business/nsc1234/acc1234/account");
	    Mockito.when(accountInformationFoundationServiceClient.restTransportForSingleAccountInformation(any(), any(), any())).thenReturn(accounts);
        Mockito.when(accountInformationFoundationServiceDelegate.transformResponseFromFDToAPI(any(), any())).thenReturn(new AccountGETResponse());
		accountInformationFoundationServiceAdapter.retrieveAccountInformation(accountMapping, new HashMap<String,String>());
	}
	
	/**
	 * Test multiple account information account as null.
	 */
	@Test(expected = AdapterException.class)
	public void testMultipleAccountInformationAccountAsNull() {
		AccountMapping accountMapping = new AccountMapping();
		AccountDetails accDet = new AccountDetails();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		Mockito.when(accountInformationFoundationServiceClient.restTransportForSingleAccountInformation(anyObject(),anyObject(),anyObject())).thenReturn(null);
		accountInformationFoundationServiceAdapter.retrieveMultipleAccountsInformation(accountMapping, new HashMap<String,String>());
	}
	
	/**
	 * Test  account information account as null.
	 */
	@Test(expected = AdapterException.class)
	public void testFilterAccountInformationAccountAsNull() {
		AccountMapping accountMapping = new AccountMapping();
		AccountDetails accDet = new AccountDetails();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		Mockito.when(accountInformationFoundationServiceClient.restTransportForSingleAccountInformation(anyObject(),anyObject(),anyObject())).thenReturn(null);
		accountInformationFoundationServiceAdapter.retrieveAccountInformation(accountMapping, new HashMap<String,String>());
	}
	
	/**
	 * Test  account information account as null.
	 */
	@Test(expected = AdapterException.class)
	public void testAccountMappingAccountAsNull() {
		AccountMapping accountMapping = new AccountMapping();
		AccountDetails accDet = new AccountDetails();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts filteredAccounts= new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt accntFilter = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt();
		accntFilter.setAccountNumber("US2345");
		accntFilter.setAccountNSC("1234");
		filteredAccounts.getAccount().add(accntFilter);
		Mockito.when(accountInformationFoundationServiceClient.restTransportForSingleAccountInformation(anyObject(),anyObject(),anyObject())).thenReturn(null);
		Mockito.when(commonFilterUtility.retrieveCustomerAccountList(any(),any())).thenReturn(filteredAccounts);
		Mockito.when(accountInformationFoundationServiceClient.restTransportForSingleAccountInformation(anyObject(),anyObject(),anyObject())).thenReturn(null);
		accountInformationFoundationServiceAdapter.retrieveAccountInformation(accountMapping, new HashMap<String,String>());
	}
	
	
	@Test(expected=AdapterException.class)
	public void testExceptionNullParams(){
		
		AccountMapping accountMapping = new AccountMapping();
		Map<String, String> params = null;
		accountMapping.setPsuId("test");
		accountMapping.setCorrelationId("test");
		Mockito.when(accountInformationFoundationServiceClient.restTransportForSingleAccountInformation(anyObject(), anyObject(), anyObject())).thenReturn(null);
		accountInformationFoundationServiceAdapter.retrieveAccountInformation(accountMapping, params);
		
	}
	
	@Test(expected=AdapterException.class)
	public void testExceptionMultiNullParams(){
		
		AccountMapping accountMapping = new AccountMapping();
		Map<String, String> params = null;
		accountMapping.setPsuId("test");
		accountMapping.setCorrelationId("test");
		Mockito.when(accountInformationFoundationServiceClient.restTransportForSingleAccountInformation(anyObject(), anyObject(), anyObject())).thenReturn(null);
		accountInformationFoundationServiceAdapter.retrieveMultipleAccountsInformation(accountMapping, params);
		
	}
	/**
	 * Test  account  account as null.
	 */
	@Test(expected = AdapterException.class)
	public void testAccountAccountAsNull() {
		AccountMapping accountMapping = new AccountMapping();
		AccountDetails accDet = new AccountDetails();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts filteredAccounts= new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt accntFilter = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt();
		accntFilter.setAccountNumber("US2345");
		accntFilter.setAccountNSC("1234");
		filteredAccounts.getAccount().add(accntFilter);
		Mockito.when(accountInformationFoundationServiceClient.restTransportForSingleAccountInformation(anyObject(),anyObject(),anyObject())).thenReturn(null);
		Mockito.when(commonFilterUtility.retrieveCustomerAccountList(any(),any())).thenReturn(filteredAccounts);
		Mockito.when(adapterFilterUtility.prevalidateAccounts(any(), any())).thenReturn(accountMapping);
		Mockito.when(accountInformationFoundationServiceClient.restTransportForSingleAccountInformation(anyObject(),anyObject(),anyObject())).thenReturn(null);
		accountInformationFoundationServiceAdapter.retrieveAccountInformation(accountMapping, new HashMap<String,String>());
	}
	/**
	 * Test multiple account  account as null.
	 */
	@Test(expected = AdapterException.class)
	public void testMultipleAccountAccountAsNull() {
		AccountMapping accountMapping = new AccountMapping();
		AccountDetails accDet = new AccountDetails();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts filteredAccounts= new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt accntFilter = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt();
		accntFilter.setAccountNumber("US2345");
		accntFilter.setAccountNSC("1234");
		filteredAccounts.getAccount().add(accntFilter);
		Mockito.when(accountInformationFoundationServiceClient.restTransportForSingleAccountInformation(anyObject(),anyObject(),anyObject())).thenReturn(null);
		Mockito.when(commonFilterUtility.retrieveCustomerAccountList(any(),any())).thenReturn(filteredAccounts);
		Mockito.when(accountInformationFoundationServiceClient.restTransportForSingleAccountInformation(anyObject(),anyObject(),anyObject())).thenReturn(null);
		accountInformationFoundationServiceAdapter.retrieveMultipleAccountsInformation(accountMapping, new HashMap<String,String>());
	}
	/**
	 * Test null exception.
	 *//*
	@Test(expected = AdapterException.class)
	public void testMultipleAccountInformationfilteredAccountsAsNull() {
	com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accounts filteredAccounts=commonFilterUtility.retrieveCustomerAccountList(accountMapping.getPsuId(), params);
	}*/
}
