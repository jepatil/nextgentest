/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.information.boi.adapter.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.utility.AdapterUtility;
import com.capgemini.psd2.aisp.domain.AccountGETResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.client.AccountInformationFoundationServiceClient;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.delegate.AccountInformationFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.domain.Accnt;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.domain.Accnts;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.domain.Accounts;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.domain.ChannelProfile;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.transformer.AccountInformationFoundationServiceTransformer;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;

/**
 * The Class AccountInformationFoundationServiceDelegateTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountInformationFoundationServiceDelegateTransformationTest {

	/** The delegate. */
	@InjectMocks
	private AccountInformationFoundationServiceDelegate delegate;

	/** The account information foundation service client. */
	@InjectMocks
	private AccountInformationFoundationServiceClient accountInformationFoundationServiceClient;

	/** The account information FS transformer. */
	@Mock
	private AccountInformationFoundationServiceTransformer accountInformationFSTransformer;

	/** The rest client. */
	@Mock
	private RestClientSyncImpl restClient;

	/** The adapterUtility. */
	@Mock
	private AdapterUtility adapterUtility;

	/**
	 * Sets the up.
	 */
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}

	/**
	 * Test rest transport for single account information.
	 */
	@Test
	public void testRestTransportForSingleAccountInformation() {
		Map<String, String> params = new HashMap<>();
		params.put("accountId", "12345");
		Accounts accounts = new Accounts();
		Accnt acc = new Accnt();
		acc.setAccountNumber("acct1234");
		acc.setAccountNSC("nsc1234");
		accounts.setAccount(acc);
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");

		Mockito.when(restClient.callForGet(any(), any(), any())).thenReturn(accounts);

		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl("http://localhost:8082/fs-accountEnquiry-service/services/AccountEnquiry/business");

		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("X-BOI-USER", "header user");
		httpHeaders.add("X-BOI-CHANNEL", "header channel");
		httpHeaders.add("X-BOI-WORKSTATION", "header workstation");
		httpHeaders.add("X-BOI-PLATFORM", "header platform");

		Accnts res = accountInformationFoundationServiceClient.restTransportForSingleAccountInformation(requestInfo,
				Accounts.class, httpHeaders);
		assertNotNull(res);
	}
	
	@Test
	public void testRestTransportForMultipleAccountInformation() {
		Map<String, String> params = new HashMap<>();
		params.put("accountId", "12345");
		Accnts accounts = new Accnts();
		Accnt acc = new Accnt();
		acc.setAccountNumber("acct1234");
		acc.setAccountNSC("nsc1234");
		accounts.getAccount().add(acc);
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		ChannelProfile channelProfile=new ChannelProfile();
		channelProfile.setAccounts(accounts);

		Mockito.when(restClient.callForGet(any(), any(), any())).thenReturn(channelProfile);

		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl("http://localhost:8082/fs-accountEnquiry-service/services/AccountEnquiry/business");

		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("X-BOI-USER", "header user");
		httpHeaders.add("X-BOI-CHANNEL", "header channel");
		httpHeaders.add("X-BOI-WORKSTATION", "header workstation");
		httpHeaders.add("X-BOI-PLATFORM", "header platform");

		Accnts res = accountInformationFoundationServiceClient.restTransportForMultipleAccountInformation(requestInfo,
				ChannelProfile.class, httpHeaders);
		assertNotNull(res);
	}

	/**
	 * Test get foundation service URL.
	 */
	@Test
	public void testGetFoundationServiceURL() {

		String accountNSC = "number";
		String accountNumber = "accountNumber";
		String channel = "BOL";
		String baseURL = "http://localhost:8082/fs-customeraccountprofile-service/services/customerAccountProfile/channel/business/account";
		String finalURL = "http://localhost:8082/fs-customeraccountprofile-service/services/customerAccountProfile/channel/business/account/number/accountNumber";
		Mockito.when(adapterUtility.retriveFoundationServiceURL(any())).thenReturn(baseURL);
		String testURL = delegate.getFoundationServiceURL(channel, accountNSC, accountNumber);
		assertEquals("Test for building URL failed.", finalURL, testURL);
		assertNotNull(testURL);
	}

	/**
	 * Test get foundation service with invalid URL.
	 */
	@Test
	public void testGetFoundationServiceWithInvalidURL() {

		String accountNSC = "number";
		String accountNumber = "accountNumber";
		String channel = "BOL";
		String baseURL = "http://localhost:8082/fs-accountEnquiry-service/services/AccountEnquiry";
		String finalURL = "http://localhost:8082/fs-accountEnquiry-service/services/AccountEnquiry/business/number/accountNumber/account";
		Mockito.when(adapterUtility.retriveFoundationServiceURL(any())).thenReturn(baseURL);
		String testURL = delegate.getFoundationServiceURL(channel, accountNSC, accountNumber);

		assertNotEquals("Test for building URL failed.", finalURL, testURL);
		assertNotNull(testURL);
	}

	/**
	 * Test get foundation service with account NSC as null.
	 */
	@Test(expected = AdapterException.class)
	public void testGetFoundationServiceWithAccountNSCAsNull() {

		String accountNSC = null;
		String accountNumber = "number";
		String baseURL = "http://localhost:8082/fs-accountEnquiry-service/services/AccountEnquiry/business";

		delegate.getFoundationServiceURL(baseURL, accountNSC, accountNumber);

		fail("Invalid account NSC");
	}

	/**
	 * Test get foundation service with account number as null.
	 */
	@Test(expected = AdapterException.class)
	public void testGetFoundationServiceWithAccountNumberAsNull() {
		String accountNumber = null;
		String accountNSC = "nsc";
		String baseURL = "http://localhost:8082/fs-accountEnquiry-service/services/AccountEnquiry/business";

		delegate.getFoundationServiceURL(baseURL, accountNSC, accountNumber);

		fail("Invalid Account Number");
	}

	/**
	 * Test get foundation service with base URL as null.
	 */
	@Test(expected = AdapterException.class)
	public void testGetFoundationServiceWithBaseURLAsNull() {

		String accountNSC = "nsc";
		String accountNumber = "number";
		String baseURL = "test";
		
		Mockito.when(adapterUtility.retriveFoundationServiceURL(anyObject())).thenReturn(null);
		delegate.getFoundationServiceURL(baseURL, accountNSC, accountNumber);

		fail("Invalid base URL");
	}
	/**
	 * Test get foundation service with base URL as null.
	 */

	@Test(expected = AdapterException.class)
	public void testGetFoundationServiceWithChannelIdAsNull() {
		String accountNumber = null;
		String accountNSC = "nsc";
		String baseURL = "http://localhost:8082/fs-accountEnquiry-service/services/AccountEnquiry/business";
		String channelId =null;
		delegate.getFoundationServiceURL(channelId, accountNSC, accountNumber);

		fail("Invalid Account Number");
	}
	/**
	 * Test create request headers actual.
	 */
	@Test
	public void testCreateRequestHeadersActual() {

		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("userId");
		accountMapping.setCorrelationId("correlationId");
		HttpHeaders httpHeaders = new HttpHeaders();
		ReflectionTestUtils.setField(delegate, "userInReqHeader", "X-BOI-USER");
		ReflectionTestUtils.setField(delegate, "channelInReqHeader", "X-BOI-CHANNEL");
		ReflectionTestUtils.setField(delegate, "platformInReqHeader", "X-BOI-PLATFORM");
		ReflectionTestUtils.setField(delegate, "correlationReqHeader", "X-CORRELATION-ID");
		Map<String, String> params = new HashMap<>();
		params.put("channelId","channel123");
		httpHeaders = delegate.createRequestHeaders(new RequestInfo(), accountMapping,params);

		assertNotNull(httpHeaders);
	}

	/**
	 * Test get account from account list.
	 */
	@Test
	public void testGetAccountFromAccountList() {

		Accnts accounts = new Accnts();
		Accnt acc = new Accnt();
		acc.setAccountNumber("acct1234");
		acc.setAccountNSC("nsc1234");
		accounts.getAccount().add(acc);
		Accnt res = delegate.getAccountFromAccountList("acct1234", accounts);
		assertEquals("acct1234", res.getAccountNumber());

	}
	
	@Test
	public void testGetNullAccountFromAccountList() {

		Accnts accounts = new Accnts();
		Accnt acc = new Accnt();
		acc.setAccountNumber("acct1234");
		acc.setAccountNSC("nsc1234");
		accounts.getAccount().add(acc);
		Accnt res = delegate.getAccountFromAccountList("acct1", accounts);
		assertEquals(null, res);
	}

	/**
	 * Test transform response from FD to API.
	 */
	@Test
	public void testTransformResponseFromFDToAPI() {
		Accnts accounts = new Accnts();
		Accnt acc = new Accnt();
		acc.setAccountNumber("acct1234");
		acc.setAccountNSC("nsc1234");
		accounts.getAccount().add(acc);
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		Mockito.when(delegate.transformResponseFromFDToAPI(any(), any())).thenReturn(new AccountGETResponse());
		AccountGETResponse res = delegate.transformResponseFromFDToAPI(accounts, new HashMap<String, String>());
		assertNotNull(res);

	}
	
	@Test
    public void testGetFoundationServiceURL1() {
					Map<String, String> params = new HashMap<>();
					params.put("channelId", "channelId");
                   AccountMapping accountMapping = new AccountMapping();
                   accountMapping.setPsuId("123");
                    Map<String, String> foundationCustProfileUrl = new HashMap<>();
                    foundationCustProfileUrl.put("channelId", "channelId");
                   delegate.setFoundationCustProfileUrl(foundationCustProfileUrl);
                   delegate.getFoundationCustProfileUrl();
                   delegate.getFoundationServiceURL(accountMapping, params);
                   String url = delegate.getFoundationServiceURL(accountMapping, params);
                   assertEquals("channelId/123/accounts", url);
                  
    }


}
