/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.information.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.aisp.domain.AccountGETResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.delegate.AccountInformationFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.domain.Accnt;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.domain.Accnts;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.transformer.AccountInformationFoundationServiceTransformer;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;
import com.capgemini.psd2.validator.PSD2Validator;

/**
 * The Class AccountInformationFoundationServiceDelegateRestCallTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountInformationFoundationServiceDelegateRestCallTest {
	
	/** The account information FS transformer. */
	@Mock
	private AccountInformationFoundationServiceTransformer accountInformationFSTransformer = new AccountInformationFoundationServiceTransformer();
	
	@InjectMocks
	private AccountInformationFoundationServiceDelegate delegate;
	
	@Mock
	private PSD2Validator psd2Validator;
	
	/** The rest client. */
	@Mock
	private RestClientSyncImpl restClient;
	
	/**
	 * Sets the up.
	 */
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	
	/**
	 * Test transform response from FD to API.
	 */
	@Test
	public void testTransformResponseFromFDToAPI() {
		
		Map<String, String> params = new HashMap<>();
		params.put("accountId", "12345");
		Accnts accounts = new Accnts();
		Accnt acc = new Accnt();
		acc.setAccountNumber("acct1234");
		acc.setAccountNSC("nsc1234");
		acc.setCurrency("GBP");
		acc.setAccountName("BOI_PSD2");
		accounts.getAccount().add(acc);
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		accountMapping.setCorrelationId("test");
		Mockito.when(delegate.transformResponseFromFDToAPI(any(), any())).thenReturn(new AccountGETResponse());
		AccountGETResponse res = accountInformationFSTransformer.transformAccountInformation(accounts, params);
		assertNotNull(res);
		
	}
	
}
