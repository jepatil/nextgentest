/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.information.boi.adapter;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.adapter.AccountInformationAdapter;
import com.capgemini.psd2.aisp.domain.AccountGETResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.client.AccountInformationFoundationServiceClient;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.constants.AccountInformationFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.delegate.AccountInformationFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.domain.Accounts;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.domain.Accnts;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.domain.ChannelProfile;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.CustomerAccountsFilterFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion.AdapterFilterUtility;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;

/**
 * The Class AccountInformationFoundationServiceAdapter.
 */
@Component
@EnableConfigurationProperties
@ConfigurationProperties("foundationService")
public class AccountInformationFoundationServiceAdapter implements AccountInformationAdapter {

	private Map<String, String> foundationCustProfileUrl = new HashMap<>();

	/** The account information foundation service delegate. */
	@Autowired
	private AccountInformationFoundationServiceDelegate accountInformationFoundationServiceDelegate;

	/** The account information foundation service client. */
	@Autowired
	private AccountInformationFoundationServiceClient accountInformationFoundationServiceClient;

	/** The AdapterFilterUtility object */
	@Autowired
	private AdapterFilterUtility adapterFilterUtility;

	@Autowired
	private CustomerAccountsFilterFoundationServiceAdapter commonFilterUtility;

	@Value("${foundationService.consentFlowType}")
	private String consentFlowType;

	@Override
	public AccountGETResponse retrieveAccountInformation(AccountMapping accountMapping, Map<String, String> params) {

		if (accountMapping == null || accountMapping.getPsuId() == null) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		if (params == null) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		/* For prevalidation */
		params.put(AccountInformationFoundationServiceConstants.BASE_URL,
				foundationCustProfileUrl.get(params.get(PSD2Constants.CHANNEL_ID)));
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, consentFlowType);
		params.put(PSD2Constants.USER_ID, accountMapping.getPsuId());
		params.put(PSD2Constants.CHANNEL_ID, params.get("channelId"));
		params.put(PSD2Constants.CORRELATION_ID, accountMapping.getCorrelationId());

		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts filteredAccounts = commonFilterUtility
				.retrieveCustomerAccountList(accountMapping.getPsuId(), params);
		if (filteredAccounts == null || filteredAccounts.getAccount() == null
				|| filteredAccounts.getAccount().isEmpty()) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.ACCOUNT_NOT_FOUND);
		}

		accountMapping = adapterFilterUtility.prevalidateAccounts(filteredAccounts, accountMapping);

		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders httpHeaders = accountInformationFoundationServiceDelegate.createRequestHeaders(requestInfo,
				accountMapping, params);
		AccountDetails accountDetails;
		if (accountMapping != null && accountMapping.getAccountDetails() != null
				&& !accountMapping.getAccountDetails().isEmpty()) {
			accountDetails = accountMapping.getAccountDetails().get(0);		
		} else
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		String finalURL = accountInformationFoundationServiceDelegate.getFoundationServiceURL(
				params.get(AccountInformationFoundationServiceConstants.CHANNEL_ID), accountDetails.getAccountNSC(),
				accountDetails.getAccountNumber());
		requestInfo.setUrl(finalURL);
		Accnts accounts = accountInformationFoundationServiceClient
				.restTransportForSingleAccountInformation(requestInfo, Accounts.class, httpHeaders);
		if (NullCheckUtils.isNullOrEmpty(accounts)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND_FOUNDATION_SERVICE);
		}
		for (AccountDetails accdet : accountMapping.getAccountDetails()) {
			params.put(accdet.getAccountNumber() + "_" + accdet.getAccountNSC(), accdet.getAccountId());
		}
		return accountInformationFoundationServiceDelegate.transformResponseFromFDToAPI(accounts, params);
	}

	@Override
	public AccountGETResponse retrieveMultipleAccountsInformation(AccountMapping accountMapping,
			Map<String, String> params) {

		if (accountMapping == null || accountMapping.getPsuId() == null) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		if (params == null) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		/* For prevalidation */
		params.put(AccountInformationFoundationServiceConstants.BASE_URL,
				foundationCustProfileUrl.get(params.get(PSD2Constants.CHANNEL_ID)));
		params.put(PSD2Constants.USER_ID, accountMapping.getPsuId());
		params.put(PSD2Constants.CHANNEL_ID, params.get("channelId"));
		params.put(PSD2Constants.CORRELATION_ID, accountMapping.getCorrelationId());
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, consentFlowType);

		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts filteredAccounts = commonFilterUtility
				.retrieveCustomerAccountList(accountMapping.getPsuId(), params);
		if (filteredAccounts == null || filteredAccounts.getAccount() == null
				|| filteredAccounts.getAccount().isEmpty()) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.ACCOUNT_NOT_FOUND);
		}

		accountMapping = adapterFilterUtility.prevalidateAccounts(filteredAccounts, accountMapping);

		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders httpHeaders = accountInformationFoundationServiceDelegate.createRequestHeaders(requestInfo,
				accountMapping, params);
		String finalURL = accountInformationFoundationServiceDelegate.getFoundationServiceURL(accountMapping, params);
		requestInfo.setUrl(finalURL);
		Accnts accounts = accountInformationFoundationServiceClient
				.restTransportForMultipleAccountInformation(requestInfo, ChannelProfile.class, httpHeaders);
		if (NullCheckUtils.isNullOrEmpty(accounts)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND_FOUNDATION_SERVICE);
		}
		for (AccountDetails accdet : accountMapping.getAccountDetails()) {
			params.put(accdet.getAccountNumber() + "_" + accdet.getAccountNSC(), accdet.getAccountId());
		}
		return accountInformationFoundationServiceDelegate.transformResponseFromFDToAPI(accounts, params);
	}

}
