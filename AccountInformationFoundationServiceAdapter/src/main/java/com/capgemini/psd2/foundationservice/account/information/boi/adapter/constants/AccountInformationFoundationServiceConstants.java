/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.information.boi.adapter.constants;

/**
 * The Class AccountInformationFoundationServiceConstants.
 */
public class AccountInformationFoundationServiceConstants {
	
	/** The Constant FOUNDATION_SERVICE. */
	public static final String FOUNDATION_SERVICE = "foundationService";
	
	/** The Constant BASE_URL. */
	public static final String BASE_URL = "baseURL";
	
	/** The Constant ACCOUNT_ID. */
	public static final String ACCOUNT_ID = "accountId";
	
	public static final String CHANNEL_ID = "channelId";
	
	/** The Constant CURRENCY_REGEX. */
	public static final String CURRENCY_REGEX = "^[A-Z]{3}$";
	
	public static final String SCHEMENAME="SchemeName";
	public static final String IDENTIFICATION ="Identification";
	public static final String SERVICERSCHEMENAME ="ServicerSchemeName";
	public static final String  SERVICERIDENTIFICATION="ServicerIdentification";
	public static final String IBAN ="IBAN";
	public static final String ACCOUNTNSCNUMBER ="AccountNSCNumber";
	public static final String BIC ="BIC";
	public static final String SORTCODEACCOUNTNUMBER  ="SORTCODEACCOUNTNUMBER";
	public static final String BICFI ="BICFI";
	
}
