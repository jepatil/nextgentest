package com.capgemini.psd2.fraudnet.mulesoft.boi.fs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.FraudnetViaMulesoftProxyAdapter;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.constants.FraudnetViaMulesoftProxyConstants;
import com.capgemini.psd2.fraudsystem.domain.Address;
import com.capgemini.psd2.fraudsystem.domain.Emails;
import com.capgemini.psd2.fraudsystem.domain.PSD2CustomerInfo;
import com.capgemini.psd2.fraudsystem.domain.PSD2FinancialAccount;
import com.capgemini.psd2.fraudsystem.domain.PhoneNumbers;


@SpringBootApplication
@ComponentScan("com.capgemini.psd2")
public class FraudnetViaMulesoftTestProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(FraudnetViaMulesoftTestProjectApplication.class, args);
	}
}

@RestController
@RequestMapping("/testFraudnetMulesoft")
class FraudnetMulesoftController {
	
	@Autowired
	private FraudnetViaMulesoftProxyAdapter adapter;

	@RequestMapping(method = RequestMethod.GET, consumes= {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	public <T> T getResponse() {
		
		Map<String, Map<String, Object>> fraudSystemRequest = new HashMap<String, Map<String, Object>>();
		 	
		 	//eventMap
		 	Map<String, Object> eventMap = new HashMap<String, Object>();
		 	eventMap.put(FraudnetViaMulesoftProxyConstants.EVENT_TYPE, "Login");
		 	eventMap.put(FraudnetViaMulesoftProxyConstants.EVENT_TIME, "2014-02-28T15:59:23.000Z");
		 	eventMap.put(FraudnetViaMulesoftProxyConstants.EVENT_CHANNEL, "Online");
		 	eventMap.put(FraudnetViaMulesoftProxyConstants.EVENT_SOURCE_SYSTEM, "API Platform");
		 	eventMap.put(FraudnetViaMulesoftProxyConstants.EVENT_SOURCE_SUBSYSTEM, "PISP");
		 	eventMap.put(FraudnetViaMulesoftProxyConstants.TRANSFER_ID, "09a52fec30534db1abab281650da9aec");
		 	eventMap.put(FraudnetViaMulesoftProxyConstants.TRANSFER_AMNT, "240");
		 	eventMap.put(FraudnetViaMulesoftProxyConstants.EVENT_ID, "Test");
		 	eventMap.put(FraudnetViaMulesoftProxyConstants.TRANSFER_MEMO, "Test");
		 	
		 	//deviceMap
		 	Map<String, Object> deviceMap = new HashMap<String, Object>();
		 	
		 	deviceMap.put(FraudnetViaMulesoftProxyConstants.DEVICE_IP, "11.5.141.202");
		 	deviceMap.put(FraudnetViaMulesoftProxyConstants.DEVICE_JSC, "kla44j1e3NlY5BSo9z4ofjb75PaK4Vpjt5nrU8s8vWuIUgxrmTTuCUpMcUGejYO KES5jfyEwHXXTSHCRR0QOtWyFGh8cmvSuCKzIlnY6x2KlT64K3H.ppAJZ7OQuyP BB2SCXw2SCYOvYDy25adjjftckcKyAd65hz7qTvtE0EREHQxbiyInrGfyex2uCK wQ9dvcpxUlzXJJIneGfYVAQEBEm1CdC5MQjGejuTDRNzcPiAksecXF5iTmk6eAX vIdVuxISg0QWvOe9fCMGa2hUMnGWpwoNSUC56MnGW87gq1HACVcHkxI5_1.9ihy ppAIKWbZcFKV8NTghN.nkre9zH_y3ExnJpyWVEL3NvWurk51lVB4WG.CNOt96h L._Wu_0L.BwCtOMu_Ep.ziPajoMu5.VNNW5BSuxIgtaqpRxuYIdw0xO9sarwyjJ vDOhhMETcouU.Uz8464qnvvYIw5Epir6UtTvqbRyhmgiFEjsnzxK9B5qfZvQAuZ a2bU0tzrU9juBhElbElLAUugLyTUbyATf92PIiyhqOfjVrwxN_l3yoonkJgI E_X_Qs796tlnhqvnmccbguaDcujhDna2QKlNdHlAmjJvDOhhM4XM0oGN_tvfvCS nBNleW5BNlan0QkBMfs.8gC");
		 	deviceMap.put(FraudnetViaMulesoftProxyConstants.DEVICE_PAYLOAD, "rO0ABXNyACdjb20udGhlNDEuY29tbW9ucy5jcnlwdG8uQ3J5cHRvRW52ZWxvcGUAAJbgqPhc8wIAA0wABWFsaWFzdAASTGphdmEvbGFuZy9TdHJpbmc7WwAMZW5jcnlwdGVkS2V5dAACW0JbABBlbmNyeXB0ZWRQYXlsb2FkcQB-");
		 	
		 	deviceMap.put(FraudnetViaMulesoftProxyConstants.DEVICE_HEADERS, "{\"X-HTTP-Header\":\"header value\"}");
		 	
		 	Map<String, String> cookiesData = new HashMap<String, String>();
		 	cookiesData.put("user_name", "cookie value");
		 	deviceMap.put(FraudnetViaMulesoftProxyConstants.DEVICE_COOKIES, cookiesData);
		 	
		 	deviceMap.put(FraudnetViaMulesoftProxyConstants.SESSION_ID, "49C5E3479AAA14B676609186B7D2E234");
		 	deviceMap.put(FraudnetViaMulesoftProxyConstants.SESSION_LOGGEDIN, true);
		 	
		 	Map<String, Object> customerMap = new HashMap<String, Object>();
		 	
		 	PSD2CustomerInfo psd2CustomerInfo = new PSD2CustomerInfo();
		 	psd2CustomerInfo.setName("DD");
		 	psd2CustomerInfo.setCompany("KK");
		 	psd2CustomerInfo.setClientType("Individual");
		 	psd2CustomerInfo.setDob("2004-05-16");
		 	
		 	List<Emails> emailData=new ArrayList<Emails>();
		 	Emails emails = new Emails();
		 	emails.setEmail("xyz@gmail.com");
		 	emailData.add(emails);
		 	psd2CustomerInfo.setEmails(emailData);
		 	
		 	List<PhoneNumbers> phoneNumberData=new ArrayList<PhoneNumbers>();
		 	PhoneNumbers phoneNumbers = new PhoneNumbers();
		 	phoneNumbers.setNumber("1234");
		 	phoneNumberData.add(phoneNumbers);
		 	psd2CustomerInfo.setPhoneNumbers(phoneNumberData);
		 	
		 	
		 	Address address = new Address();
		 	address.setCity("London");address.setStreetLine1("street1");address.setStreetLine2("street2");address.setPostal("R40T");
		 	psd2CustomerInfo.setAddress(address);	
		 	
		 	customerMap.put(FraudnetViaMulesoftProxyConstants.CONTACT_DETAILS, psd2CustomerInfo);
		 	
		 	
		 	List<PSD2FinancialAccount> psd2FinancialAccountList = new ArrayList<PSD2FinancialAccount>();
		 	
		 	PSD2FinancialAccount psd2FinancialAccount = new PSD2FinancialAccount();
		 	psd2FinancialAccount.setType("test");
		 	psd2FinancialAccount.setHashedAccountNumber("1234");
		 	psd2FinancialAccount.setRoutingNumber("1234");
		 	
		 	psd2FinancialAccountList.add(psd2FinancialAccount);
		 	
		 	customerMap.put(FraudnetViaMulesoftProxyConstants.FIN_ACCNTS, psd2FinancialAccountList);
		 	
		 	customerMap.put(FraudnetViaMulesoftProxyConstants.ACCOUNT_ID, "1234");
		 			 	
		 	fraudSystemRequest.put(FraudnetViaMulesoftProxyConstants.FRAUDREQUEST_EVENT_MAP, eventMap);
		 	fraudSystemRequest.put(FraudnetViaMulesoftProxyConstants.FRAUDREQUEST_DEVICE_MAP, deviceMap);
		 	fraudSystemRequest.put(FraudnetViaMulesoftProxyConstants.FRAUDREQUEST_CUSTOMER_MAP, customerMap);
		 	

		   return adapter.retrieveFraudScore(fraudSystemRequest);
		   
	
	}
	
}

