/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.standingorder.mock.foundationservice.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.account.standingorder.mock.foundationservice.domain.StandingOrders;
import com.capgemini.psd2.account.standingorder.mock.foundationservice.repository.AccountStandingOrdersRepository;
import com.capgemini.psd2.account.standingorder.mock.foundationservice.service.AccountStandingOrderService;
import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;

/**
 * The Class AccountStandingOrderServiceImpl.
 */
@Service
public class AccountStandingOrderServiceImpl implements AccountStandingOrderService {

	/** The repository. */
	@Autowired
	private AccountStandingOrdersRepository repository;

	@Override
	public StandingOrders retrieveAccountStandingOrder(String payerNSC, String payerAccountNumber)

			throws Exception {

		StandingOrders standingOrders = repository.findByPayerNSCAndPayerAccountNumber(payerNSC, payerAccountNumber);

		if (standingOrders == null) {
				throw MockFoundationServiceException
						.populateMockFoundationServiceException(ErrorCodeEnum.NO_STANDINGORDER_FOUND_ESSO);
		}	
		return standingOrders;
	}
}

