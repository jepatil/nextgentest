/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.aisp.consent.adapter.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.consent.adapter.repository.AispConsentMongoRepository;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.utilities.GenerateUniqueIdUtilities;

/**
 * The Class AispConsentAdapterImpl.
 */
@Component
public class AispConsentAdapterImpl implements AispConsentAdapter {

	/** The aisp consent repository. */
	@Autowired
	private AispConsentMongoRepository aispConsentMongoRepository;

	/** The req header attributes. */
	@Autowired
	private RequestHeaderAttributes reqHeaderAttributes;



	/* (non-Javadoc)
	 * @see com.capgemini.psd2.consent.adapter.AispConsentAdapter#retrieveConsent(java.lang.String)
	 */
	@Override
	public AispConsent retrieveConsent(String consentId) {
		AispConsent consent =null;
		try{
			consent = aispConsentMongoRepository.findByConsentId(consentId);
		}catch(DataAccessResourceFailureException e){
			throw PSD2Exception.populatePSD2Exception(e.getMessage(),ErrorCodeEnum.CONNECTION_ERROR); 
		}
		if (consent == null)
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_CONSENT_FOUND);
		return consent;
	}

	/* (non-Javadoc)
	 * @see com.capgemini.psd2.consent.adapter.AispConsentMappingAdapter#retrieveAccountMappingByAccountId(java.lang.String, java.lang.String)
	 */
	@Override
	public AccountMapping retrieveAccountMappingByAccountId(String consentId, String accountId) {

	List<AispConsent> consentList = retrieveConsentsByAccountId(accountId);
	if (consentList.isEmpty()){
		throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.INVALID_ACCOUNT_ID);
	}
	

	AispConsent matchedConsent = consentList.stream().filter(consent -> consent.getConsentId().equals(consentId))
	.findFirst().orElse(null);

	if (matchedConsent == null){
		throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_ACCESS_GIVEN_TO_REQUESTED_ACCOUNT);
	}
	

	List<AccountDetails> accountDetails = new ArrayList<>();
	AccountDetails accountDetail = matchedConsent.getAccountDetails().stream()
	.filter(accDtl -> accDtl.getAccountId().equals(accountId)).findFirst().orElse(null);
	accountDetails.add(accountDetail);
	AccountMapping accountMapping = new AccountMapping();
	accountMapping.setPsuId(matchedConsent.getPsuId());
	accountMapping.setTppCID(matchedConsent.getTppCId());
	accountMapping.setCorrelationId(reqHeaderAttributes.getCorrelationId());
	accountMapping.setAccountDetails(accountDetails);
	return accountMapping;
	}

	/* (non-Javadoc)
	 * @see com.capgemini.psd2.consent.adapter.AispConsentMappingAdapter#retrieveAccountMapping(java.lang.String)
	 */
	@Override
	public AccountMapping retrieveAccountMapping(String consentId) {
		AispConsent consent = retrieveConsent(consentId);
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId(consent.getPsuId());
		accountMapping.setTppCID(consent.getTppCId());
		accountMapping.setCorrelationId(reqHeaderAttributes.getCorrelationId());
		accountMapping.setAccountDetails(consent.getAccountDetails());
		return accountMapping;
	}

	@Override
	public void createConsent(AispConsent aispConsent) {
		AispConsent consent = null;
		try{
			consent = retrieveConsentByAccountRequestId(aispConsent.getAccountRequestId(),ConsentStatusEnum.AWAITINGAUTHORISATION);
			if(consent != null){
				consent.setStatus(ConsentStatusEnum.REVOKED);
				aispConsentMongoRepository.save(consent);
			}
			else {
				consent = retrieveConsentByAccountRequestId(aispConsent.getAccountRequestId(),ConsentStatusEnum.AUTHORISED);
				if(consent != null){
					throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
				}
			}
			if(aispConsent.getAccountDetails() == null || aispConsent.getAccountDetails().isEmpty()){
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_ACCOUNT_DETAILS_FOUND);
			}
			for(AccountDetails acctDetail : aispConsent.getAccountDetails()){
				acctDetail.setAccountId(GenerateUniqueIdUtilities.getUniqueId().toString());
			}
			aispConsent.setConsentId(GenerateUniqueIdUtilities.generateRandomUniqueID());
			aispConsent.setStatus(ConsentStatusEnum.AWAITINGAUTHORISATION);
			aispConsentMongoRepository.save(aispConsent);
		}catch(DataAccessResourceFailureException e){
			throw PSD2Exception.populatePSD2Exception(e.getMessage(),ErrorCodeEnum.CONNECTION_ERROR); 
		}
	}

	@Override
	public AispConsent retrieveConsentByAccountRequestId(String accountRequestId,ConsentStatusEnum status) {
		AispConsent consent = null;
		try{
			consent = aispConsentMongoRepository.findByAccountRequestIdAndStatus(accountRequestId,status);
		}
		catch(DataAccessResourceFailureException e){
			throw PSD2Exception.populatePSD2Exception(e.getMessage(),ErrorCodeEnum.CONNECTION_ERROR); 
		}
		return consent;
	}

	@Override
	public void updateConsentStatus(String consentId,ConsentStatusEnum statusEnum) {
		AispConsent aispConsent = null;
		try{
			aispConsent = aispConsentMongoRepository.findByConsentId(consentId);
			aispConsent.setStatus(statusEnum);
			aispConsentMongoRepository.save(aispConsent);
		}
		catch(DataAccessResourceFailureException e){
			throw PSD2Exception.populatePSD2Exception(e.getMessage(),ErrorCodeEnum.CONNECTION_ERROR); 
		}
	}

	@Override
	public List<AispConsent> retrieveConsentByPsuIdAndConsentStatus(String psuId, ConsentStatusEnum statusEnum) {
		
		List<AispConsent> consentList = null;
		try {
			if (null == statusEnum) {
				consentList = aispConsentMongoRepository.findByPsuId(psuId);
			} else {
				consentList = aispConsentMongoRepository.findByPsuIdAndStatus(psuId, statusEnum);
			}
			
		} catch (DataAccessResourceFailureException e) {
			throw PSD2Exception.populatePSD2Exception(e.getMessage(),ErrorCodeEnum.CONNECTION_ERROR);
		}
		
		
		return consentList;
	}
	
	@Override
	public AispConsent updateConsentStatusWithResponse(String consentId,ConsentStatusEnum statusEnum) {
		AispConsent aispConsent = null;
		try{
			aispConsent = aispConsentMongoRepository.findByConsentId(consentId);
			aispConsent.setStatus(statusEnum);
			return aispConsentMongoRepository.save(aispConsent);
		}
		catch(DataAccessResourceFailureException e){
			throw PSD2Exception.populatePSD2Exception(e.getMessage(),ErrorCodeEnum.CONNECTION_ERROR); 
		}
	}
	
	public List<AispConsent> retrieveConsentsByAccountId(String accountId) {
		List<AispConsent> consentList = null;
		try {
		consentList = aispConsentMongoRepository.findByAccountDetailsAccountId(accountId);

		} catch (DataAccessResourceFailureException e) {
		throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.CONNECTION_ERROR);
		}
		return consentList;
		}
}
