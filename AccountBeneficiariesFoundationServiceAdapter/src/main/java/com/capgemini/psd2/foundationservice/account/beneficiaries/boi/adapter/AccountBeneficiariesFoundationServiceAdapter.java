
package com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.adapter.AccountBeneficiariesAdapter;
import com.capgemini.psd2.aisp.domain.BeneficiariesGETResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.client.AccountBeneficiariesFoundationServiceClient;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.constants.AccountBeneficiariesFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.delegate.AccountBeneficiariesFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.Beneficiaries;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.CustomerAccountsFilterFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion.AdapterFilterUtility;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class AccountBeneficiariesFoundationServiceAdapter implements AccountBeneficiariesAdapter {
	
	@Value("${foundationService.accountBeneficiaryBaseURL}")
	private String accountBeneficiaryBaseURL;
	
	@Value("${foundationService.consentFlowType}")
	private String consentFlowType;
	
	@Autowired
	private AccountBeneficiariesFoundationServiceDelegate accountBeneficiariesFoundationServiceDelegate;
	
	@Autowired
	private AccountBeneficiariesFoundationServiceClient accountBeneficiariesFoundationServiceClient;
	
	@Autowired
	private CustomerAccountsFilterFoundationServiceAdapter commonFilterUtility;
	
	@Autowired
	private AdapterFilterUtility adapterFilterUtility;

	@Override
	public BeneficiariesGETResponse retrieveAccountBeneficiaries(AccountMapping accountMapping,
			Map<String, String> params) {
		if (NullCheckUtils.isNullOrEmpty(accountMapping) || NullCheckUtils.isNullOrEmpty(accountMapping.getPsuId())) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		} 
		if(NullCheckUtils.isNullOrEmpty(params)){
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, consentFlowType);
	    params.put(PSD2Constants.CHANNEL_ID, params.get("channelId"));
		params.put(PSD2Constants.USER_ID, accountMapping.getPsuId());
		params.put(PSD2Constants.CORRELATION_ID, accountMapping.getCorrelationId());
		
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts filteredAccounts = commonFilterUtility.retrieveCustomerAccountList(accountMapping.getPsuId(), params);
		if (filteredAccounts == null || filteredAccounts.getAccount() == null || filteredAccounts.getAccount().isEmpty()) {
            throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.ACCOUNT_NOT_FOUND);
		} 
		adapterFilterUtility.prevalidateAccounts(filteredAccounts, accountMapping);
		
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders httpHeaders = accountBeneficiariesFoundationServiceDelegate.createRequestHeaders(requestInfo, accountMapping, params);
		// Assuming only one AccountDetail comes from API
		AccountDetails accountDetails=null;
		if (accountMapping.getAccountDetails() != null && !accountMapping.getAccountDetails().isEmpty()) {
			accountDetails = accountMapping.getAccountDetails().get(0);
			params.put(AccountBeneficiariesFoundationServiceConstants.ACCOUNT_ID, accountDetails.getAccountId());
		} else{
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		String finalURL = accountBeneficiariesFoundationServiceDelegate.getFoundationServiceURL(accountMapping.getPsuId(), accountBeneficiaryBaseURL);
		requestInfo.setUrl(finalURL);
		
		/**to send query params*/
		MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<>();
		queryParams.add(AccountBeneficiariesFoundationServiceConstants.NSC, accountDetails.getAccountNSC());
		queryParams.add(AccountBeneficiariesFoundationServiceConstants.ACCOUNT_NUMBER, accountDetails.getAccountNumber());
		Beneficiaries beneficiaries = null;
		try{
			beneficiaries = accountBeneficiariesFoundationServiceClient.restTransportForAccountBeneficiaryFS(requestInfo, Beneficiaries.class, queryParams, httpHeaders);
		}catch(AdapterException e){
			ErrorInfo errorInfo = e.getErrorInfo();
			if(AdapterErrorCodeEnum.NO_BENEFICIARY_FOUND.getErrorCode().equals(errorInfo.getActualErrorCode())){
				beneficiaries = new Beneficiaries();
			}else{
				throw e;
			}
		}catch(Exception e){
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.TECHNICAL_ERROR);
		}
//		if (NullCheckUtils.isNullOrEmpty(beneficiaries)) {
//			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_BENEFICIARY_FOUND);
//		}
		return accountBeneficiariesFoundationServiceDelegate.transform( beneficiaries, params);
	}



}
