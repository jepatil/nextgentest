
package com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.transformer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.domain.BeneficiariesGETResponse;
import com.capgemini.psd2.aisp.domain.Beneficiary;
import com.capgemini.psd2.aisp.domain.Data4;
import com.capgemini.psd2.aisp.domain.Data4CreditorAccount;
import com.capgemini.psd2.aisp.domain.Data4Servicer;
import com.capgemini.psd2.aisp.domain.Data4Servicer.SchemeNameEnum;
import com.capgemini.psd2.aisp.transformer.AccountBeneficiariesTransformer;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.constants.AccountBeneficiariesFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.Beneficiaries;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.BeneficiaryStatus;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;


@Component
public class AccountBeneficiariesFoundationServiceTransformer implements AccountBeneficiariesTransformer{

	@Autowired
	@Qualifier("PSD2ResponseValidator")
	private PSD2Validator validator;
	
	@Value("${foundationService.filteredBeneficiariesCountryCodes}")
	private String filteredBeneficiariesCountryCodes;
	
	@Value("${foundationService.filterBeneficiariesResponse}")
	private boolean filterBeneficiariesResponse;
	
	public <T> BeneficiariesGETResponse transformAccountBeneficiaries(T source, Map<String, String> params){
	
		BeneficiariesGETResponse beneficiariesGETResponse = new BeneficiariesGETResponse();
		Data4 data = new Data4();
		Beneficiaries beneficiaries = (Beneficiaries)source;
		
		List<com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.Beneficiary> beneficiaryList = beneficiaries.getBeneficiary();
		for(com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.Beneficiary beneficiary : beneficiaryList){
			if(NullCheckUtils.isNullOrEmpty(beneficiary.getCountryCode()) || !filterBeneficiariesResponse || 
							(filterBeneficiariesResponse && !filteredBeneficiariesCountryCodes.contains(beneficiary.getCountryCode()))){
				Data4CreditorAccount data4CreditorAccount = null;
	 			if(!NullCheckUtils.isNullOrEmpty(beneficiary.getStatus()) && beneficiary.getStatus().toString().equals(BeneficiaryStatus.Y.toString())){
					Beneficiary responseBeneficiary = new Beneficiary();
					Data4Servicer data4Servicer = new Data4Servicer();
					
		//			AccountId, beneficiaryId and Reference
					responseBeneficiary.setAccountId(params.get(AccountBeneficiariesFoundationServiceConstants.ACCOUNT_ID));
					responseBeneficiary.setBeneficiaryId(beneficiary.getId());
					responseBeneficiary.setReference(beneficiary.getReference());
					
		//			Creditor Account
					if(!NullCheckUtils.isNullOrEmpty(beneficiary.getIBAN())){
						data4CreditorAccount = new Data4CreditorAccount();
						if(!NullCheckUtils.isNullOrEmpty(beneficiary.getBIC())){
							data4Servicer.setSchemeName(SchemeNameEnum.BICFI);
							data4Servicer.setIdentification(beneficiary.getBIC());
							validator.validate(data4Servicer);
							responseBeneficiary.setServicer(data4Servicer);
						} else {
							throw AdapterException.populatePSD2Exception("No BIC is returned from FS.", AdapterErrorCodeEnum.TECHNICAL_ERROR);
						}
						data4CreditorAccount.setSchemeName(com.capgemini.psd2.aisp.domain.Data4CreditorAccount.SchemeNameEnum.IBAN);
						data4CreditorAccount.setIdentification(beneficiary.getIBAN());
						data4CreditorAccount.setName(beneficiary.getName());
						validator.validate(data4CreditorAccount);
					
					}else if(!NullCheckUtils.isNullOrEmpty(beneficiary.getNSC()) && !NullCheckUtils.isNullOrEmpty(beneficiary.getAccountNumber())){
						data4CreditorAccount = new Data4CreditorAccount();
						data4CreditorAccount.setSchemeName(com.capgemini.psd2.aisp.domain.Data4CreditorAccount.SchemeNameEnum.SORTCODEACCOUNTNUMBER);
						data4CreditorAccount.setIdentification(beneficiary.getNSC().trim() + beneficiary.getAccountNumber().trim());
						data4CreditorAccount.setName(beneficiary.getName());
						validator.validate(data4CreditorAccount);
					}
					
					responseBeneficiary.setCreditorAccount(data4CreditorAccount);
					validator.validate(responseBeneficiary);
					data.addBeneficiaryItem(responseBeneficiary);
				}
			}
		}
		if(null == data.getBeneficiary() || data.getBeneficiary().isEmpty()){
			data.setBeneficiary(new ArrayList<>());
		}
		validator.validate(data);
		beneficiariesGETResponse.setData(data);
		return beneficiariesGETResponse;
	}
}
