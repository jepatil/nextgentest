package com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.domain.BeneficiariesGETResponse;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.BeneRegisteredAsType;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.Beneficiaries;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.Beneficiary;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.BeneficiaryStatus;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.LimitBandType;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.transformer.AccountBeneficiariesFoundationServiceTransformer;
import com.capgemini.psd2.validator.PSD2Validator;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountBeneficiariesFoundationServiceTransformerTest {
	@InjectMocks
	private AccountBeneficiariesFoundationServiceTransformer accountBeneficiariesFoundationServiceTransformer;
	
	@Mock
	@Qualifier("PSD2ResponseValidator")
	private PSD2Validator validator;
	
	private Beneficiaries beneficiaries = new Beneficiaries();
	private Beneficiary beneficiary = new Beneficiary();
	private Map<String, String> params = new HashMap<String, String>();
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
		
		beneficiary.setId("12345678");
		beneficiary.setAccountNumber("12345678");
		beneficiary.setNSC("123456");
		beneficiary.setIBAN("GB29NWBK60161331926819");
		beneficiary.setABACode("abaCode");
		beneficiary.setName("Name");
		beneficiary.setReference("Reference");
		beneficiary.setCountryCode("GB");
		beneficiary.setStatus(BeneficiaryStatus.Y);
		beneficiary.setBeneRegisteredAs(BeneRegisteredAsType.B);
		beneficiary.setLimitBand(LimitBandType.P);
		beneficiary.setBIC("BOFIIE2D");
		beneficiary.setType("Type");
		beneficiaries.getBeneficiary().add(beneficiary);
		params.put("accountId", "12345678");
		Mockito.doNothing().when(validator).validate(anyObject());
	}
	@Test
	public void contextLoads() {
	}
	@Test
	public void testTransformAccountBeneficiaries(){
		
		BeneficiariesGETResponse response = accountBeneficiariesFoundationServiceTransformer.transformAccountBeneficiaries(beneficiaries, params);
		assertNotNull(response);
//		Passing sortCode and account number only.
		beneficiary.setIBAN(null);
		response = accountBeneficiariesFoundationServiceTransformer.transformAccountBeneficiaries(beneficiaries, params);
		assertNotNull(response);
//		Passing sortCode only
		beneficiary.setAccountNumber(null);
		response = accountBeneficiariesFoundationServiceTransformer.transformAccountBeneficiaries(beneficiaries, params);
		assertNotNull(response);
//		Passing accountNumber only
		beneficiary.setAccountNumber("12345678");
		beneficiary.setNSC(null);
		response = accountBeneficiariesFoundationServiceTransformer.transformAccountBeneficiaries(beneficiaries, params);
		assertNotNull(response);
//		Empty beneficiaries passed
		Beneficiaries emptyBeneficiaries = new Beneficiaries();
		response = accountBeneficiariesFoundationServiceTransformer.transformAccountBeneficiaries(emptyBeneficiaries, params);
		assertNotNull(response);
	}
	
	@Test(expected = AdapterException.class)
	public void BICNullTest(){
//		Not passing BIC
		beneficiary.setIBAN("GB29NWBK60161331926819");
		beneficiary.setBIC(null);
		accountBeneficiariesFoundationServiceTransformer.transformAccountBeneficiaries(beneficiaries, params);
	}
	
	@Test
	public void noBeneficiaryFiltered(){
//		beneficiary List is null
		beneficiary.setStatus(BeneficiaryStatus.C);
		accountBeneficiariesFoundationServiceTransformer.transformAccountBeneficiaries(beneficiaries, params);
	}
	
	@Test
	public void beneficiaryStatusNullTest(){
//		beneficiary List is null
		beneficiary.setStatus(null);
		accountBeneficiariesFoundationServiceTransformer.transformAccountBeneficiaries(beneficiaries, params);
	}
}
