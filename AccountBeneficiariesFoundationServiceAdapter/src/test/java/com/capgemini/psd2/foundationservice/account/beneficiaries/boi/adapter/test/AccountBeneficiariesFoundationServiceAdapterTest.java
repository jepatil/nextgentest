/**
 * 
 */
package com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.domain.BeneficiariesGETResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.AccountBeneficiariesFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.client.AccountBeneficiariesFoundationServiceClient;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.delegate.AccountBeneficiariesFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.BeneRegisteredAsType;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.Beneficiaries;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.Beneficiary;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.BeneficiaryStatus;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.LimitBandType;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.CustomerAccountsFilterFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion.AdapterFilterUtility;
import com.capgemini.psd2.logger.PSD2Constants;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountBeneficiariesFoundationServiceAdapterTest {


	@InjectMocks
	private AccountBeneficiariesFoundationServiceAdapter accountBeneficiariesFoundationServiceAdapter;

	@Mock
	private AccountBeneficiariesFoundationServiceDelegate accountBeneficiaryFoundationServiceDelegate;

	@Mock
	private AccountBeneficiariesFoundationServiceClient accountBeneficiaryFoundationServiceClient;
	
	@Mock
	private CustomerAccountsFilterFoundationServiceAdapter commonFilterUtility;
	
	@Mock
	private AdapterFilterUtility adapterFilterUtility;


	private Map<String, String> params = new HashMap<String, String>();
	private AccountMapping accountMapping = new AccountMapping();
	private List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
	private AccountDetails accDet = new AccountDetails();
	private HttpHeaders httpHeaders = new HttpHeaders();
	private Beneficiaries beneficiaries = new  Beneficiaries();
	private Beneficiary beneficiary = new Beneficiary();
	private BeneficiariesGETResponse beneficiariesGETResponse = new BeneficiariesGETResponse();
	private com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts filteredAccounts = new Accnts();
	private Accnt accnt = new Accnt();
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
		params.put("x-channel-id", "BOL");
		params.put("channelId", "BOL");
		params.put(PSD2Constants.CHANNEL_ID, "BOL");
		params.put(PSD2Constants.PLATFORM_IN_REQ_HEADER, "PSD2API");
		beneficiary.setId("12345678");
		beneficiary.setAccountNumber("12345678");
		beneficiary.setNSC("123456");
		beneficiary.setIBAN("GB29NWBK60161331926819");
		beneficiary.setABACode("abaCode");
		beneficiary.setName("Name");
		beneficiary.setReference("Reference");
		beneficiary.setCountryCode("GB");
		beneficiary.setStatus(BeneficiaryStatus.C);
		beneficiary.setBeneRegisteredAs(BeneRegisteredAsType.B);
		beneficiary.setLimitBand(LimitBandType.P);
		beneficiary.setBIC("BOFIIE2D");
		beneficiary.setType("Type");
		beneficiaries.getBeneficiary().add(beneficiary);
		accnt.setAccountNSC("123456");
		accnt.setAccountNumber("12345678");
		filteredAccounts.getAccount().add(accnt);
		accDet.setAccountId("1234");
		accDet.setAccountNSC("123456");
		accDet.setAccountNumber("12345678");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("12345678");
		accountMapping.setCorrelationId("12345678");
		httpHeaders.add("X-BOI-USER", "12345678");
		httpHeaders.add("X-BOI-CHANNEL", "BOL");
		httpHeaders.add("X-BOI-PLATFORM", "PSD2API");
		httpHeaders.add("X-CORRELATION-ID", "Correlation");
		ReflectionTestUtils.setField(accountBeneficiariesFoundationServiceAdapter, "consentFlowType", "AISP");
		Mockito.when(commonFilterUtility.retrieveCustomerAccountList(anyObject(), anyObject())).thenReturn(filteredAccounts);
		Mockito.when(accountBeneficiaryFoundationServiceDelegate.getFoundationServiceURL(anyString(), anyString())).thenReturn("http://localhost:9087/fs-abt-service/services/user/12345678/beneficiaries");
		Mockito.when(accountBeneficiaryFoundationServiceDelegate.createRequestHeaders(anyObject(), anyObject(), anyObject())).thenReturn(httpHeaders);
		Mockito.when(adapterFilterUtility.prevalidateAccounts(anyObject(), anyObject())).thenReturn(accountMapping);
	}
	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	@Test
	public void testRetrieveAccountBeneficiaries() {
		Mockito.when(accountBeneficiaryFoundationServiceClient.restTransportForAccountBeneficiaryFS(anyObject(), anyObject(), anyObject(), anyObject())).thenReturn(beneficiaries);
		Mockito.when(accountBeneficiaryFoundationServiceDelegate.transform(anyObject(), anyObject())).thenReturn(beneficiariesGETResponse);
		BeneficiariesGETResponse response = accountBeneficiariesFoundationServiceAdapter.retrieveAccountBeneficiaries(accountMapping, params);
		assertEquals(beneficiariesGETResponse, response);
	}
	@Test
	public void noBeneficiariesFoundExceptionTest(){
		Mockito.when(accountBeneficiaryFoundationServiceClient.restTransportForAccountBeneficiaryFS(anyObject(), anyObject(), anyObject(), anyObject())).thenReturn(null);
		accountBeneficiariesFoundationServiceAdapter.retrieveAccountBeneficiaries(accountMapping, params);
	}
	
	@Test(expected = Exception.class)
	public void testRestTransportForAccountStandingOrder(){
		Mockito.when(accountBeneficiaryFoundationServiceClient.restTransportForAccountBeneficiaryFS(anyObject(), anyObject(), anyObject(), anyObject())).thenThrow(PSD2Exception.populatePSD2Exception("",ErrorCodeEnum.TECHNICAL_ERROR));
		Mockito.when(accountBeneficiaryFoundationServiceDelegate.transform(anyObject(), anyObject())).thenReturn(beneficiariesGETResponse);;
		accountBeneficiariesFoundationServiceAdapter.retrieveAccountBeneficiaries(accountMapping, params);
		//assertEquals(standingOrderGETResponse, response);
	}
	
	
	@Test
	public void testRestTransportForAccountStandingOrders(){
		ErrorInfo errorInfo = new ErrorInfo();
		Beneficiaries standingOrders = null;
		errorInfo.setErrorCode("569");
		errorInfo.setErrorMessage("This request cannot be processed. No StandingOrder found");
		Mockito.when(accountBeneficiaryFoundationServiceClient.restTransportForAccountBeneficiaryFS(anyObject(), anyObject(), anyObject(), anyObject())).thenThrow((new AdapterException("Adapter Exception", errorInfo )));
		BeneficiariesGETResponse getresponse  = accountBeneficiariesFoundationServiceAdapter.retrieveAccountBeneficiaries(accountMapping, params);
		assertEquals(standingOrders, getresponse);
	}
	@Test(expected = AdapterException.class)
	public void accountMappingNullTest(){
		accountBeneficiariesFoundationServiceAdapter.retrieveAccountBeneficiaries(null, params);
	}
	@Test(expected = AdapterException.class)
	public void paramsNullTest(){
		accountBeneficiariesFoundationServiceAdapter.retrieveAccountBeneficiaries(accountMapping, null);
	}
	@Test(expected = AdapterException.class)
	public void accountMappingEmptyTest(){
		AccountMapping accountMapping = new AccountMapping();
		accountBeneficiariesFoundationServiceAdapter.retrieveAccountBeneficiaries(accountMapping, params);
	}
	@Test(expected = AdapterException.class)
	public void psuIdNullTest(){
		accountMapping.setPsuId(null);
		accountBeneficiariesFoundationServiceAdapter.retrieveAccountBeneficiaries(accountMapping, params);
	}
	@Test(expected = AdapterException.class)
	public void accountDetailsEmptyTest(){
		accountMapping.setAccountDetails(new ArrayList<AccountDetails>());
		accountMapping.setPsuId("12345678");
		accountBeneficiariesFoundationServiceAdapter.retrieveAccountBeneficiaries(accountMapping, params);
	}
	@Test(expected = AdapterException.class)
	public void accountDetailsNullTest(){
		accountMapping.setAccountDetails(null);
		accountMapping.setPsuId("12345678");
		accountBeneficiariesFoundationServiceAdapter.retrieveAccountBeneficiaries(accountMapping, params);
	}
	@Test(expected = AdapterException.class)
	public void filteredAccountsNullTest(){
		Mockito.when(commonFilterUtility.retrieveCustomerAccountList(anyObject(), anyObject())).thenReturn(null);
		accountBeneficiariesFoundationServiceAdapter.retrieveAccountBeneficiaries(accountMapping, params);
	}
	@Test(expected = AdapterException.class)
	public void filteredAccountsAccountEmptyTest(){
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts filteredAccounts = new Accnts();
		Mockito.when(commonFilterUtility.retrieveCustomerAccountList(anyObject(), anyObject())).thenReturn(filteredAccounts);
		accountBeneficiariesFoundationServiceAdapter.retrieveAccountBeneficiaries(accountMapping, params);
	}
}
