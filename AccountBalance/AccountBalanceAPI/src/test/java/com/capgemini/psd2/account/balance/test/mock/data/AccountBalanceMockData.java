/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.balance.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.Balance;
import com.capgemini.psd2.aisp.domain.Balance.CreditDebitIndicatorEnum;
import com.capgemini.psd2.aisp.domain.BalancesGETResponse;
import com.capgemini.psd2.aisp.domain.Data5;
import com.capgemini.psd2.aisp.domain.Data5Amount;
import com.capgemini.psd2.aisp.domain.Data5Amount1;
import com.capgemini.psd2.aisp.domain.Data5CreditLine;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.token.ConsentTokenData;
import com.capgemini.psd2.token.Token;

/**
 * The Class AccountBalanceMockData.
 */
public class AccountBalanceMockData {

	/** The mock consent. */
	private static AispConsent mockConsent = null;

	/** The mock balances GET response. */
	private static BalancesGETResponse mockBalancesGETResponse = null;

	/** The mock token. */
	public static Token mockToken;

	/** The mock balances GET response data. */
	private static Balance balance = null;

	/**
	 * Gets the token.
	 *
	 * @return the token
	 */
	public static Token getToken() {
		mockToken = new Token();
		ConsentTokenData consentTokenData = new ConsentTokenData();
		consentTokenData.setConsentExpiry("1509348259877L");
		consentTokenData.setConsentId("12345");
		mockToken.setConsentTokenData(consentTokenData);
		return mockToken;
	}

	/**
	 * Gets the consent mock data.
	 *
	 * @return the consent mock data
	 */
	public static AispConsent getConsentMockData() {
		mockConsent = new AispConsent();
		mockConsent.setTppCId("tpp123");
		mockConsent.setPsuId("user123");
		mockConsent.setConsentId("a1e590ad-73dc-453a-841e-2a2ef055e878");
		List<AccountDetails> selectedAccounts = new ArrayList<>();
		AccountDetails accountRequest = new AccountDetails();
		accountRequest.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		accountRequest.setAccountNSC("SC802001");
		accountRequest.setAccountNumber("10203345");
		selectedAccounts.add(accountRequest);
		mockConsent.setAccountDetails(selectedAccounts);
		return mockConsent;
	}

	/**
	 * Gets the mock account mapping.
	 *
	 * @return the mock account mapping
	 */
	public static AccountMapping getMockAccountMapping() {
		AccountMapping mapping = new AccountMapping();
		mapping.setTppCID("tpp123");
		mapping.setPsuId("user123");
		List<AccountDetails> selectedAccounts = new ArrayList<>();
		AccountDetails accountRequest = new AccountDetails();
		accountRequest.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		accountRequest.setAccountNSC("SC802001");
		accountRequest.setAccountNumber("10203345");
		selectedAccounts.add(accountRequest);
		mapping.setAccountDetails(selectedAccounts);
		return mapping;
	}

	/**
	 * Gets the balances GET response data.
	 *
	 * @return the balances GET response data
	 */
	public static Balance getBalancesGETResponseData() {
		balance = new Balance();
		balance.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		Data5Amount data5Amount = new Data5Amount();
		data5Amount.setAmount("1000.0");
		data5Amount.setCurrency("USD");
		balance.amount(data5Amount);
		List<Data5CreditLine> creditLineList = new ArrayList<Data5CreditLine>();
		Data5CreditLine creditLine = new Data5CreditLine();
		Data5Amount1 creditLineAmount = new Data5Amount1();
		creditLineAmount.setAmount("1000.0");
		creditLineAmount.setCurrency("USD");
		creditLine.setAmount(creditLineAmount);
		creditLine.setType(com.capgemini.psd2.aisp.domain.Data5CreditLine.TypeEnum.EMERGENCY);
		creditLineList.add(creditLine);
		balance.setCreditLine(creditLineList);
		balance.setCreditDebitIndicator(CreditDebitIndicatorEnum.CREDIT);
		return balance;
	}

	/**
	 * Gets the balances GET response.
	 *
	 * @return the balances GET response
	 */
	public static BalancesGETResponse getBalancesGETResponse() {
		mockBalancesGETResponse = new BalancesGETResponse();
		List<Balance> balanceList = new ArrayList<>();
		Balance balance = getBalancesGETResponseData();
		balanceList.add(balance);
		Data5 data5 = new Data5();
		data5.setBalance(balanceList);
		mockBalancesGETResponse.setData(data5);
		return mockBalancesGETResponse;
	}
}
