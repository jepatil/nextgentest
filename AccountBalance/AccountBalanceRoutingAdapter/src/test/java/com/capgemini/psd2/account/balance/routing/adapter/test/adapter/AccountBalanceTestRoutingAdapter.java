/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.balance.routing.adapter.test.adapter;

import java.util.Map;

import com.capgemini.psd2.account.balance.routing.adapter.test.mock.data.AccountBalanceRoutingAdapterTestMockData;
import com.capgemini.psd2.aisp.adapter.AccountBalanceAdapter;
import com.capgemini.psd2.aisp.domain.BalancesGETResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;

/**
 * The Class AccountBalanceTestRoutingAdapter.
 */
public class AccountBalanceTestRoutingAdapter implements AccountBalanceAdapter{

	/* (non-Javadoc)
	 * @see com.capgemini.psd2.aisp.adapter.AccountBalanceAdapter#retrieveAccountBalance(com.capgemini.psd2.aisp.domain.AccountMapping, java.util.Map)
	 */
	@Override
	public BalancesGETResponse retrieveAccountBalance(AccountMapping accountMapping,
			Map<String, String> params) {
		return AccountBalanceRoutingAdapterTestMockData.getBalancesGETResponse();
	}
	
}
