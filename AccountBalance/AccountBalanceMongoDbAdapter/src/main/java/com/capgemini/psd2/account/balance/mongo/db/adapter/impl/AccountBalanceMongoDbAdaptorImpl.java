/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.balance.mongo.db.adapter.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.account.balance.mongo.db.adapter.repository.AccountBalanceRepository;
import com.capgemini.psd2.aisp.adapter.AccountBalanceAdapter;
import com.capgemini.psd2.aisp.domain.Balance;
import com.capgemini.psd2.aisp.domain.BalancesGETResponse;
import com.capgemini.psd2.aisp.domain.Data5;
import com.capgemini.psd2.aisp.mock.domain.MockBalance;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;

/**
 * The Class AccountBalanceMongoDbAdaptorImpl.
 */
@Component
public class AccountBalanceMongoDbAdaptorImpl implements AccountBalanceAdapter {

	/** The repository. */
	@Autowired
	private AccountBalanceRepository repository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.capgemini.psd2.aisp.adapter.AccountBalanceAdapter#
	 * retrieveAccountBalance(com.capgemini.psd2.aisp.domain.AccountMapping,
	 * java.util.Map)
	 */
	@Override
	public BalancesGETResponse retrieveAccountBalance(AccountMapping accountMapping, Map<String, String> params) {
		List<MockBalance> mockbalanceResponseList = null;
		try {
			mockbalanceResponseList = repository.findByAccountNumberAndAccountNSC(
					accountMapping.getAccountDetails().get(0).getAccountNumber(),
					accountMapping.getAccountDetails().get(0).getAccountNSC());
		} catch (DataAccessResourceFailureException e) {
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.CONNECTION_ERROR);
		}

		if (mockbalanceResponseList == null)
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_RECORD_FOUND_FOR_REQUESTED_ACCT);

		List<Balance> balanceResponseList = new ArrayList();
		balanceResponseList.addAll(mockbalanceResponseList);
		
		BalancesGETResponse balancesGETResponse = new BalancesGETResponse();
		balanceResponseList.get(0).setAccountId(accountMapping.getAccountDetails().get(0).getAccountId());
		Data5 data = new Data5();
		data.setBalance(balanceResponseList);
		balancesGETResponse.setData(data);
		return balancesGETResponse;
	}

}
