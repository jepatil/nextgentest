package com.capgemini.psd2.account.beneficiaries.mongo.db.adapter.test.impl;

import static org.junit.Assert.assertNotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.account.beneficiaries.mongo.db.adapter.impl.AccountBeneficiariesMongoDbAdapterImpl;


import com.capgemini.psd2.aisp.domain.BeneficiariesGETResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;

/**
 * The Class AccountBeneficiariesMongoDBAdapterImplTest.
 */

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountBeneficiariesMongoDbAdapterImplTest 
{
	/**
	 * Sets the up.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	/** The account beneficiaries mongo DB adapter impl. */
	@InjectMocks
	private AccountBeneficiariesMongoDbAdapterImpl accountBeneficiariesMongoDbAdapterImpl;

	/**
	 * Test retrieve blank response account beneficiaries success flow.
	 */
	/*@Test
	public void testRetrieveAccountBeneficiariesSuccessFlow() {
		BeneficiariesGETResponse beneficiariesGETResponse = accountBeneficiariesMongoDbAdapterImpl.retrieveAccountBeneficiaries(null, null);
		assertTrue(beneficiariesGETResponse.getData().getBeneficiary().isEmpty());	
	}*/
	@Test
	public void testRetrieveAccountBeneficiaries()  {
		AccountMapping accountMapping =new AccountMapping();
		ArrayList<AccountDetails> accntDetails= new ArrayList<>();
		AccountDetails accountDetails= new AccountDetails();
		Map<String, String> params = new HashMap<>();
		accntDetails.add(accountDetails);
		accountMapping.setAccountDetails(accntDetails);
		accountDetails.setAccountId("1234");
		BeneficiariesGETResponse beneficiariesGETResponse = accountBeneficiariesMongoDbAdapterImpl.retrieveAccountBeneficiaries(accountMapping,params);
		assertNotNull(beneficiariesGETResponse);
		
		
		
		
		
	}
	

}
