package com.capgemini.psd2.account.beneficiaries.service;

import com.capgemini.psd2.aisp.domain.BeneficiariesGETResponse;


@FunctionalInterface
public interface AccountBeneficiariesService 
{
	/**
	 * Retrieve account beneficiaries.
	 *
	 * @param accountId the account id
	 * @return the beneficiaries GET response
	 */
	public BeneficiariesGETResponse retrieveAccountBeneficiaries(String accountId);
}
