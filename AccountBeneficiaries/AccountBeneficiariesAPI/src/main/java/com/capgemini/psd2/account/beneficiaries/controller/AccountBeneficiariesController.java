package com.capgemini.psd2.account.beneficiaries.controller;

import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.account.beneficiaries.service.AccountBeneficiariesService;

import com.capgemini.psd2.aisp.domain.BeneficiariesGETResponse;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.utilities.NullCheckUtils;

@RestController
public class AccountBeneficiariesController 
{
	/** The service. */
	@Autowired
	private AccountBeneficiariesService service;
	
	@RequestMapping(value ="/accounts/{accountId}/beneficiaries", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public BeneficiariesGETResponse retrieveAccountBeneficiaries(@PathVariable("accountId") String accountId) {

		if (NullCheckUtils.isNullOrEmpty(accountId))
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_ACCOUNT_ID_FOUND);
		
		if (!Pattern.matches("[a-zA-Z0-9-]{1,40}", accountId))
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.VALIDATION_ERROR);

		
		return service.retrieveAccountBeneficiaries(accountId);
	}
}
 