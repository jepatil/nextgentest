package com.capgemini.psd2.account.beneficiaries.routing.adapter.test.impl;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.account.beneficiaries.routing.adapter.impl.AccountBeneficiariesRoutingAdapter;
import com.capgemini.psd2.account.beneficiaries.routing.adapter.routing.AccountBeneficiariesAdapterFactory;
import com.capgemini.psd2.account.beneficiaries.routing.adapter.test.adapter.AccountBeneficiariesTestRoutingAdapter;
import com.capgemini.psd2.aisp.adapter.AccountBeneficiariesAdapter;

import com.capgemini.psd2.aisp.domain.BeneficiariesGETResponse;

public class AccountBeneficiariesRoutingAdapterTest 
{
	
	/** The account beneficiaries adapter factory. */
	@Mock
	private AccountBeneficiariesAdapterFactory accountBeneficiariesAdapterFactory;

	/** The account beneficiaries routing adapter. */
	@InjectMocks
	private AccountBeneficiariesAdapter accountBeneficiariesRoutingAdapter = new AccountBeneficiariesRoutingAdapter();

	/**
	 * Sets the up.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Retrieve account balancetest.
	 */
	@Test
	public void retrieveAccountDirectDebitstest() {

		AccountBeneficiariesAdapter accountBeneficiariesAdapter = new AccountBeneficiariesTestRoutingAdapter();
		Mockito.when(accountBeneficiariesAdapterFactory.getAdapterInstance(anyString()))
				.thenReturn(accountBeneficiariesAdapter);

		BeneficiariesGETResponse beneficiariesGETResponse = accountBeneficiariesRoutingAdapter.retrieveAccountBeneficiaries(null,
				null);

		assertTrue(beneficiariesGETResponse.getData().getBeneficiary().isEmpty());

	}

}
