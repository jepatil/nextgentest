/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.direct.debits.test.controller;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.capgemini.psd2.account.direct.debits.controller.AccountDirectDebitsController;
import com.capgemini.psd2.account.direct.debits.service.AccountDirectDebitsService;
import com.capgemini.psd2.aisp.domain.AccountGETResponse1;
import com.capgemini.psd2.exceptions.PSD2Exception;

/**
 * The Class AccountBalanceControllerTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountDirectDebitsControllerTest {

	/** The service. */
	@Mock
	private AccountDirectDebitsService service;

	/** The mock mvc. */
	private MockMvc mockMvc;

	/** The controller. */
	@InjectMocks
	private AccountDirectDebitsController controller;

	/** The Direct debits response. */
	AccountGETResponse1 accountGETResponse1 = null;
	
	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(controller).dispatchOptions(true).build();
	}

	/**
	 * Test null account id. Failure test
	 */
	@Test(expected = PSD2Exception.class)
	public void testNullAccountId() {
		controller.retrieveAccountDirectDebits(null);
	}
	
	/**
	 * Test invalid account id length.
	 */
	@Test(expected = PSD2Exception.class)
	public void testInvalidAccountIdLength() {
		controller.retrieveAccountDirectDebits("269c3ff5-d7f8-419b-a3b9-7136c5b4611a-qwer23454c51326fdr34-12345123smcge36dg337b");
	}

	/**
	 * Test.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void successTest() throws Exception {
		when(service.retrieveAccountDirectDebits(anyString())).thenReturn(accountGETResponse1);
		this.mockMvc.perform(get("/accounts/{accountId}/direct-debits", "269c3ff5-d7f8-419b-a3b9-7136c5b4611a")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}
	

	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
		controller = null;
	}

}
