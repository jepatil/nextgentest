package com.capgemini.psd2.account.direct.debits.boi.fs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.aisp.domain.AccountGETResponse1;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.direct.debits.boi.adapter.AccountDirectDebitsFoundationServiceAdapter;
import com.capgemini.psd2.logger.PSD2Constants;

@SpringBootApplication
@ComponentScan("com.capgemini.psd2")
public class AccountDirectDebitsFoundationServiceTestProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccountDirectDebitsFoundationServiceTestProjectApplication.class, args);
	}
}

@RestController
@ResponseBody
class TestAccountBalanceFSController {

	@Autowired
	private AccountDirectDebitsFoundationServiceAdapter adapter;

	@RequestMapping("/testAccountDirectDebits")
	public AccountGETResponse1 getResponse() {

		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("1234");
		accDet.setAccountNSC("903779");
		accDet.setAccountNumber("25369621");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("BOI999");
		accountMapping.setCorrelationId("12345678");

		Map<String, String> params = new HashMap<String, String>();

		params.put("x-channel-id", "BOL");
		params.put("channelId", "BOL");
		params.put(PSD2Constants.CHANNEL_ID, "BOL");
		params.put(PSD2Constants.PLATFORM_IN_REQ_HEADER, "PSD2API");
		return adapter.retrieveAccountDirectDebits(accountMapping, params);
	}
}