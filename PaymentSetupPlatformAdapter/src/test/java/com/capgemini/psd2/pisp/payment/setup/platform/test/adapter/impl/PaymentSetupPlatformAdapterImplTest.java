package com.capgemini.psd2.pisp.payment.setup.platform.test.adapter.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.PaymentResponseInfo;
import com.capgemini.psd2.pisp.domain.PaymentSetupPlatformResource;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.impl.PaymentSetupPlatformAdapterImpl;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.repository.PaymentSetupPlatformRepository;
import com.capgemini.psd2.pisp.payment.setup.platform.test.mock.data.PaymentSetupPlatformResourceMockData;
import com.capgemini.psd2.pisp.payment.setup.platform.test.mock.data.PaymentSetupRequestResponseMockData;
import com.capgemini.psd2.token.Token;
import com.capgemini.psd2.token.TppInformationTokenData;


@RunWith(SpringJUnit4ClassRunner.class)
public class PaymentSetupPlatformAdapterImplTest {

	@InjectMocks
	private PaymentSetupPlatformAdapterImpl adapterImpl;
	
	@Mock
	private PaymentSetupPlatformRepository paymentSetupRepository;
	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	//	this.mockMvc = MockMvcBuilders.standaloneSetup(adapterImpl).dispatchOptions(true).build();
	}
	
	@Test
	public void testUpdatePaymentSetupResourceTestPositive(){
		PaymentSetupPlatformResource platformResource = PaymentSetupPlatformResourceMockData.getPaymentSetupPlatformResourceResponse();
		Mockito.when(paymentSetupRepository.save(platformResource)).thenReturn(platformResource);
		adapterImpl.updatePaymentSetupResource(platformResource);
	}
	
	@Test(expected= PSD2Exception.class)
	public void testUpdatePaymentSetupResourceTestException(){
		PaymentSetupPlatformResource platformResource = PaymentSetupPlatformResourceMockData.getPaymentSetupPlatformResourceResponse();
		Mockito.when(paymentSetupRepository.save(platformResource)).thenThrow(new DataAccessResourceFailureException("testUpdatePaymentSetupResourceTestException"));
		adapterImpl.updatePaymentSetupResource(platformResource);
	}
	
	@Test
	public void testGetIdempotentPaymentSetupResourcePositive(){
		PaymentSetupPlatformResource platformResource = PaymentSetupPlatformResourceMockData.getPaymentSetupPlatformResourceResponse();
		Mockito.when(paymentSetupRepository.findOneByTppCIDAndIdempotencyKeyAndIdempotencyRequestAndCreatedAtGreaterThan(
				anyString(), anyString(), anyString(), any(String.class))).thenReturn(platformResource);
		PaymentSetupPlatformResource platformResource1 = adapterImpl.getIdempotentPaymentSetupResource(2000);
		assertEquals(platformResource,platformResource1);
		
	}
	
	@Test(expected= PSD2Exception.class)
	public void testGetIdempotentPaymentSetupResourceException(){
		Mockito.when(paymentSetupRepository.findOneByTppCIDAndIdempotencyKeyAndIdempotencyRequestAndCreatedAtGreaterThan(
				anyString(), anyString(), anyString(), any(String.class))).thenThrow(new DataAccessResourceFailureException("testGetIdempotentPaymentSetupResourceException"));
		adapterImpl.getIdempotentPaymentSetupResource(2000);
			
	}
	
	@Test
	public void testRetrievePaymentSetupResourcePositive(){
		PaymentSetupPlatformResource platformResource = PaymentSetupPlatformResourceMockData.getPaymentSetupPlatformResourceResponse();
		Mockito.when(paymentSetupRepository.findOneByPaymentId(anyString())).thenReturn(platformResource);		 
		PaymentSetupPlatformResource platformResource1 = adapterImpl.retrievePaymentSetupResource(platformResource.getPaymentId());
		assertEquals(platformResource,platformResource1);
		
	}
	
	@Test(expected= PSD2Exception.class)
	public void testRetrievePaymentSetupResourceException(){
		PaymentSetupPlatformResource platformResource = PaymentSetupPlatformResourceMockData.getPaymentSetupPlatformResourceResponse();
		Mockito.when(paymentSetupRepository.findOneByPaymentId(anyString())).thenThrow(new DataAccessResourceFailureException("testRetrievePaymentSetupResourceException"));
		adapterImpl.retrievePaymentSetupResource(platformResource.getPaymentId());
			
	}
	
	@Test
	public void testCreatePaymentSetupResourcePositive(){
		PaymentResponseInfo info = new PaymentResponseInfo();
		info.setIdempotencyRequest("true");
		info.setPaymentValidationStatus("Rejected");
		PaymentSetupPlatformResource mockPlatformResource = PaymentSetupPlatformResourceMockData.getPaymentSetupPlatformResourceResponse();

		CustomPaymentSetupPOSTRequest paymentSetupRequest = PaymentSetupRequestResponseMockData.getPaymentSetupPostMockRequest();
		Mockito.when(paymentSetupRepository.save(any(PaymentSetupPlatformResource.class))).thenReturn(mockPlatformResource);
		Token token = new Token();
		TppInformationTokenData tppInformation = new TppInformationTokenData();
		tppInformation.setTppLegalEntityName("MoneyWise.com");
		token.setTppInformation(tppInformation);
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(token);
		PaymentSetupPlatformResource platformResourceResponse = adapterImpl.createPaymentSetupResource(paymentSetupRequest, info);
		assertEquals(mockPlatformResource.getIdempotencyKey(),platformResourceResponse.getIdempotencyKey());
	}
	
	@Test
	public void testCreatePaymentSetupResourceOuterIfNegative(){
		PaymentResponseInfo info = new PaymentResponseInfo();
		info.setIdempotencyRequest("true");
		info.setPaymentValidationStatus("Rejected");
		PaymentSetupPlatformResource mockPlatformResource = PaymentSetupPlatformResourceMockData.getPaymentSetupPlatformResourceResponse();

		CustomPaymentSetupPOSTRequest paymentSetupRequest = PaymentSetupRequestResponseMockData.getPaymentSetupPostMockRequest();
		paymentSetupRequest.setData(null);
		Mockito.when(paymentSetupRepository.save(any(PaymentSetupPlatformResource.class))).thenReturn(mockPlatformResource);
		Token token = new Token();
		TppInformationTokenData tppInformation = new TppInformationTokenData();
		tppInformation.setTppLegalEntityName("MoneyWise.com");
		token.setTppInformation(tppInformation);
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(token);
		PaymentSetupPlatformResource platformResourceResponse = adapterImpl.createPaymentSetupResource(paymentSetupRequest, info);
		assertEquals(mockPlatformResource.getIdempotencyKey(),platformResourceResponse.getIdempotencyKey());
	}
	
	@Test
	public void testCreatePaymentSetupResourceOuterIfSecondNegative(){
		PaymentResponseInfo info = new PaymentResponseInfo();
		info.setIdempotencyRequest("true");
		info.setPaymentValidationStatus("Rejected");
		PaymentSetupPlatformResource mockPlatformResource = PaymentSetupPlatformResourceMockData.getPaymentSetupPlatformResourceResponse();

		CustomPaymentSetupPOSTRequest paymentSetupRequest = PaymentSetupRequestResponseMockData.getPaymentSetupPostMockRequest();
		paymentSetupRequest.getData().setInitiation(null);
		Mockito.when(paymentSetupRepository.save(any(PaymentSetupPlatformResource.class))).thenReturn(mockPlatformResource);
		Token token = new Token();
		TppInformationTokenData tppInformation = new TppInformationTokenData();
		tppInformation.setTppLegalEntityName("MoneyWise.com");
		token.setTppInformation(tppInformation);
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(token);
		PaymentSetupPlatformResource platformResourceResponse = adapterImpl.createPaymentSetupResource(paymentSetupRequest, info);
		assertEquals(mockPlatformResource.getIdempotencyKey(),platformResourceResponse.getIdempotencyKey());
	}
	
	@Test
	public void testCreatePaymentSetupResourceInnerIfNegative(){
		PaymentResponseInfo info = new PaymentResponseInfo();
		info.setIdempotencyRequest("true");
		info.setPaymentValidationStatus("Rejected");
		PaymentSetupPlatformResource mockPlatformResource = PaymentSetupPlatformResourceMockData.getPaymentSetupPlatformResourceResponse();

		CustomPaymentSetupPOSTRequest paymentSetupRequest = PaymentSetupRequestResponseMockData.getPaymentSetupPostMockRequest();
		paymentSetupRequest.getData().getInitiation().setDebtorAgent(null);
		Mockito.when(paymentSetupRepository.save(any(PaymentSetupPlatformResource.class))).thenReturn(mockPlatformResource);
		Token token = new Token();
		TppInformationTokenData tppInformation = new TppInformationTokenData();
		tppInformation.setTppLegalEntityName("MoneyWise.com");
		token.setTppInformation(tppInformation);
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(token);
		PaymentSetupPlatformResource platformResourceResponse = adapterImpl.createPaymentSetupResource(paymentSetupRequest, info);
		assertEquals(mockPlatformResource.getIdempotencyKey(),platformResourceResponse.getIdempotencyKey());
	}
	
	@Test(expected= PSD2Exception.class)
	public void testCreatePaymentSetupResourceException(){
		PaymentResponseInfo info = new PaymentResponseInfo();
		info.setIdempotencyRequest("true");
		info.setPaymentValidationStatus("Rejected");

		CustomPaymentSetupPOSTRequest paymentSetupRequest = PaymentSetupRequestResponseMockData.getPaymentSetupPostMockRequest();
		Mockito.when(paymentSetupRepository.save(any(PaymentSetupPlatformResource.class))).thenThrow(new DataAccessResourceFailureException("testCreatePaymentSetupResourceException"));
		Token token = new Token();
		TppInformationTokenData tppInformation = new TppInformationTokenData();
		tppInformation.setTppLegalEntityName("MoneyWise.com");
		token.setTppInformation(tppInformation);
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(token);
		adapterImpl.createPaymentSetupResource(paymentSetupRequest, info);
	}
	
	@After
	public void tearDown() throws Exception {
		adapterImpl = null;
		paymentSetupRepository = null;
		reqHeaderAtrributes = null;		
	}
}
