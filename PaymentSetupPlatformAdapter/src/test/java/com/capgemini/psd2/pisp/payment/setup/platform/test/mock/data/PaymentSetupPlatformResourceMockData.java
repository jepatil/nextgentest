package com.capgemini.psd2.pisp.payment.setup.platform.test.mock.data;

import org.joda.time.DateTime;

import com.capgemini.psd2.pisp.domain.PaymentSetupPlatformResource;
import com.capgemini.psd2.pisp.validation.PispUtilities;

public class PaymentSetupPlatformResourceMockData {
	
	public static PaymentSetupPlatformResource getPaymentSetupPlatformResourceResponse(){
		PaymentSetupPlatformResource platformResource = new PaymentSetupPlatformResource();
		
		platformResource.setId("59a6b78f8207f500610373a8");
		platformResource.setCreatedAt(PispUtilities.getCurrentDateInISOFormat());
		platformResource.setEndToEndIdentification("DEMO_USER");
		platformResource.setIdempotencyKey("FRESCO.21302.GFX.1028");
		platformResource.setIdempotencyRequest("true");
		platformResource.setInstructionIdentification("ABDDCF");
		platformResource.setPaymentId("71de8c681cdc476cb1dda68e376f2812");
		platformResource.setStatus("AcceptedTechnicalValidation	");
		platformResource.setTppCID("6443e15975554bce8099e35b88b40465");
		platformResource.setTppDebtorDetails("false");
		
		return platformResource;
		
	}
	

}
