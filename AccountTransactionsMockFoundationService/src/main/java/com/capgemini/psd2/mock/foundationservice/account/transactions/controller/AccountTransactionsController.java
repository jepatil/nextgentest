package com.capgemini.psd2.mock.foundationservice.account.transactions.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.mock.foundationservice.account.transactions.domain.Accounts;
import com.capgemini.psd2.mock.foundationservice.account.transactions.exception.InvalidParameterRequestException;
import com.capgemini.psd2.mock.foundationservice.account.transactions.exception.MissingAuthenticationHeaderException;
import com.capgemini.psd2.mock.foundationservice.account.transactions.service.AccountTransactionsService;

@RestController
@RequestMapping("/fs-abt-service/services/abt")
public class AccountTransactionsController {

	@Autowired
	private AccountTransactionsService accountTransactionsService;

	
	@RequestMapping(value = "/accounts/{accountNSC}/{accountNumber}/transactions", method = RequestMethod.GET, produces = "application/xml")
	public Accounts reteriveAccountTransaction(
			@PathVariable("accountNSC") String accountNSC,
			@PathVariable("accountNumber") String accountNumber,
			@RequestParam(required = false, value="startDate") String startDate,
			@RequestParam(required = false, value="endDate") String endDate,
			@RequestParam(required = false, value="pageNumber") String pageNumber,
			@RequestParam(required = false, value="txnType") String txnType,
			@RequestParam(required = false, value="transactionRetrievalKey") String transactionRetrievalKey,
			@RequestParam(required = false, value="pageSize") String pageSize,
			@RequestHeader(required = false, value = "X-BOI-USER") String boiUser,
			@RequestHeader(required = false, value = "X-BOI-CHANNEL") String boiChannel,
			@RequestHeader(required = false, value = "X-BOI-PLATFORM") String boiPlatform,
			@RequestHeader(required = false, value = "X-CORRELATION-ID") String correlationID) throws Exception {
		
		if (boiUser == null || boiUser.isEmpty() || boiChannel == null || boiChannel.isEmpty() || boiPlatform == null
				|| boiPlatform.isEmpty() || correlationID == null || correlationID.isEmpty()) {
			throw new MissingAuthenticationHeaderException("Header Missing");
		}

		if (accountNSC == null || accountNSC.isEmpty() || accountNumber == null || accountNumber.isEmpty()) {
			throw new InvalidParameterRequestException("Bad request");
		}

		return accountTransactionsService.retrieveAccountTransactions(accountNSC, accountNumber, startDate, endDate, pageNumber, txnType, pageSize);
		
	}


}
