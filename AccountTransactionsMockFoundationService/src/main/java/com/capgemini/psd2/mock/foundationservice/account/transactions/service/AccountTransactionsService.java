package com.capgemini.psd2.mock.foundationservice.account.transactions.service;

import com.capgemini.psd2.mock.foundationservice.account.transactions.domain.Accounts;

public interface AccountTransactionsService {

	public Accounts retrieveAccountTransactions(String accountNSC, String accountNumber, String FromBookingDateTime, String ToBookingDateTime, String RequestedPageNumber, String txnType, String pageSize) throws Exception ;

}
