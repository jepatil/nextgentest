package com.capgemini.psd2.foundationservice.account.product.boi.adapter.test;
/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/


import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.aisp.domain.ProductGETResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.product.boi.adapter.delegate.AccountProductFoundationServiceDelegate;

import com.capgemini.psd2.foundationservice.account.product.boi.adapter.domain.Product;
import com.capgemini.psd2.foundationservice.account.product.boi.adapter.transformer.AccountProductFoundationServiceTransformer;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;
import com.capgemini.psd2.validator.PSD2Validator;

/**
 * The Class AccountProductFoundationServiceDelegateRestCallTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountProductFoundationServiceDelegateRestCallTest {
	
	/** The account information FS transformer. */
	@InjectMocks
	private AccountProductFoundationServiceDelegate delegate;
	@Mock
	private AccountProductFoundationServiceTransformer accountProductFSTransformer;

	@Mock
	private PSD2Validator psd2Validator;
	
	/** The rest client. */
	@Mock
	private RestClientSyncImpl restClient;
	
	/**
	 * Sets the up.
	 */
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	
	/**
	 * Test transform response from FD to API.
	 */
	@Test
	public void testTransformResponseFromFDToAPI() {
		
		Map<String, String> params = new HashMap<>();
		params.put("accountId", "12345");
		Product product = new Product();
		
		
		product.setProductIdentifier("test");
		product.setProductType("BCA");
		
		
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		accountMapping.setCorrelationId("test");
		Mockito.when(delegate.transformResponseFromFDToAPI(any(), any())).thenReturn(new ProductGETResponse());
		ProductGETResponse res = accountProductFSTransformer.transformAccountProducts(product, params);
		assertNotNull(res);
	}
	
}
