package com.capgemini.psd2.foundationservice.account.product.boi.adapter.test;
/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 *******************************************************************************/


import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.domain.ProductGETResponse;
import com.capgemini.psd2.aisp.domain.StandingOrdersGETResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.foundationservice.account.product.boi.adapter.AccountProductFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.account.product.boi.adapter.client.AccountProductFoundationServiceClient;
import com.capgemini.psd2.foundationservice.account.product.boi.adapter.delegate.AccountProductFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.product.boi.adapter.domain.Product;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.CustomerAccountsFilterFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion.AdapterFilterUtility;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;



/*
 * The Class AccountProductFoundationServiceAdapterTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountProductFoundationServiceAdapterTest {

	
	/** The account information foundation service adapter. */
	@InjectMocks
	private AccountProductFoundationServiceAdapter accountProductFoundationServiceAdapter;
	
	/** The account information foundation service delegate. */
	@Mock
	private AccountProductFoundationServiceDelegate accountProductFoundationServiceDelegate;
	
	/** The account information foundation service client. */
	@Mock
	private AccountProductFoundationServiceClient accountProductFoundationServiceClient;
	
	/** The rest client. */
	@Mock
	private RestClientSyncImpl restClient;
	
	@Mock
	AdapterFilterUtility adapterFilterUtility;
	
	@Mock
	CustomerAccountsFilterFoundationServiceAdapter commonFilterUtility;
	
	/**
	 * Sets the up.
	 */
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	
	/**
	 * Test account information FS.
	 */
	@Test
	public void testAccountProductFS() {
		Product product = new Product();
		
		product.setProductIdentifier("test");
		product.setProductType("BCA");
		
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		
		
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts filteredAccounts = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt accntFilter = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt();
		accntFilter.setAccountNumber("US2345");
		accntFilter.setAccountNSC("1234");
		filteredAccounts.getAccount().add(accntFilter);
		
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		accountMapping.setCorrelationId("test");
		Mockito.when(commonFilterUtility.retrieveCustomerAccountList(any(),any())).thenReturn(filteredAccounts);
		Mockito.when(adapterFilterUtility.prevalidateAccounts(any(), any())).thenReturn(accountMapping);
		Mockito.when(accountProductFoundationServiceClient.restTransportForAccountProduct(any(), any(), any())).thenReturn(product);
		Mockito.when(accountProductFoundationServiceDelegate.transformResponseFromFDToAPI(any(), any())).thenReturn(new ProductGETResponse());
		Mockito.when(restClient.callForGet(any(), any(), any())).thenReturn(product);
	    accountProductFoundationServiceAdapter.retrieveAccountProducts(accountMapping, new HashMap<String,String>());
		
	}

	/**
	 * Test account Product accnt mapping as null.
	 */
	@Test(expected = AdapterException.class)
	public void testAccountProductAccntMappingAsNull() {
				
		Mockito.when(accountProductFoundationServiceDelegate.createRequestHeaders(anyObject(),anyObject(),anyObject())).thenReturn(new HttpHeaders());
		accountProductFoundationServiceAdapter.retrieveAccountProducts(null, new HashMap<String,String>());
	}
	
	/**
	 * Test account Product accnt mapping psu ID as null.
	 */
	@Test(expected = AdapterException.class)
	public void testAccountProductAccntMappingPsuIdAsNull() {
		AccountMapping accountMapping = new AccountMapping();		
		Mockito.when(accountProductFoundationServiceDelegate.createRequestHeaders(anyObject(),anyObject(),anyObject())).thenReturn(new HttpHeaders());
		accountProductFoundationServiceAdapter.retrieveAccountProducts(accountMapping, new HashMap<String,String>());
	}
	
	@Test(expected = AdapterException.class)
	public void testParamsAsNull() {
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("test");
		accountProductFoundationServiceAdapter.retrieveAccountProducts(accountMapping, null);
	}
	
	/**
	 * Test account Product as null.
	 */
	@Test
	public void testAccountProductAsNull() {
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("test");
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		Product po=null;
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts filteredAccounts = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt accntFilter = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt();
		accntFilter.setAccountNumber("US2345");
		accntFilter.setAccountNSC("1234");
		filteredAccounts.getAccount().add(accntFilter);
		accountMapping.setAccountDetails(accDetList);
		Mockito.when(commonFilterUtility.retrieveCustomerAccountList(any(),any())).thenReturn(filteredAccounts);
		Mockito.when(adapterFilterUtility.prevalidateAccounts(any(), any())).thenReturn(accountMapping);
		
		Mockito.when(accountProductFoundationServiceClient.restTransportForAccountProduct(anyObject(),anyObject(),anyObject())).thenReturn(null);
		ProductGETResponse productGETResponse=accountProductFoundationServiceAdapter.retrieveAccountProducts(accountMapping, new HashMap<String,String>());
		assertEquals(productGETResponse, po);
	}
	
	/**
	 * Test account accnt details as null.
	 */
	@Test(expected = AdapterException.class)
	public void testAccountDetailsAsNull() {
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("test");
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts filteredAccounts = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt accntFilter = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt();
		accntFilter.setAccountNumber("US2345");
		accntFilter.setAccountNSC("1234");
		filteredAccounts.getAccount().add(accntFilter);
		
		Mockito.when(commonFilterUtility.retrieveCustomerAccountList(any(),any())).thenReturn(filteredAccounts);
		Mockito.when(adapterFilterUtility.prevalidateAccounts(any(), any())).thenReturn(accountMapping);
		accountProductFoundationServiceAdapter.retrieveAccountProducts(accountMapping, new HashMap<String,String>());
	}
		
		@Test(expected = Exception.class)
		public void testRestTransportForProduct(){
			ProductGETResponse productGETResponse=null;
			AccountMapping accountMapping = new AccountMapping();
			accountMapping.setPsuId("test");
			com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts filteredAccounts = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts();
			com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt accntFilter = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt();
			accntFilter.setAccountNumber("US2345");
			accntFilter.setAccountNSC("1234");
			filteredAccounts.getAccount().add(accntFilter);
			Mockito.when(accountProductFoundationServiceClient.restTransportForAccountProduct(anyObject(),anyObject(),anyObject())).thenThrow(PSD2Exception.populatePSD2Exception("",ErrorCodeEnum.TECHNICAL_ERROR));
			Mockito.when(accountProductFoundationServiceDelegate.transformResponseFromFDToAPI(any(),any())).thenReturn(productGETResponse);
			accountProductFoundationServiceAdapter.retrieveAccountProducts(accountMapping, new HashMap<String,String>());
			//assertEquals(standingOrderGETResponse, response);
		}
		
		
		@Test
		public void testRestTransportForAccountProducts(){
			
			AccountMapping accountMapping = new AccountMapping();
			List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
			AccountDetails accDet = new AccountDetails();
			accDet.setAccountId("12345");
			accDet.setAccountNSC("nsc1234");
			accDet.setAccountNumber("acct1234");
			accDetList.add(accDet);
			ProductGETResponse productGETResponse =null;
			
			com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts filteredAccounts = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts();
			com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt accntFilter = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt();
			accntFilter.setAccountNumber("US2345");
			accntFilter.setAccountNSC("1234");
			filteredAccounts.getAccount().add(accntFilter);
			
			accountMapping.setAccountDetails(accDetList);
			accountMapping.setTppCID("test");
			accountMapping.setPsuId("test");
			accountMapping.setCorrelationId("test");
			Mockito.when(commonFilterUtility.retrieveCustomerAccountList(any(),any())).thenReturn(filteredAccounts);
			Mockito.when(adapterFilterUtility.prevalidateAccounts(any(), any())).thenReturn(accountMapping);
			ErrorInfo errorInfo = new ErrorInfo();
			errorInfo.setErrorCode("571");
			errorInfo.setErrorMessage("This request cannot be processed. No StandingOrder found");
			Mockito.when(accountProductFoundationServiceClient.restTransportForAccountProduct(anyObject(),anyObject(),anyObject())).thenThrow((new AdapterException("Adapter Exception", errorInfo )));
			Mockito.when(commonFilterUtility.retrieveCustomerAccountList(any(),any())).thenReturn(filteredAccounts);
			ProductGETResponse getresponse  = accountProductFoundationServiceAdapter.retrieveAccountProducts(accountMapping, new HashMap<String,String>());
			assertEquals(productGETResponse, getresponse);
			
			
		}

	
	
}
