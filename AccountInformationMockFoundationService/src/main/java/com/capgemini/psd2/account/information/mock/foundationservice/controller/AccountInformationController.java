/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.information.mock.foundationservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.account.information.mock.foundationservice.domain.Accounts;
import com.capgemini.psd2.account.information.mock.foundationservice.exception.handler.InvalidParameterRequestException;
import com.capgemini.psd2.account.information.mock.foundationservice.exception.handler.MissingAuthenticationHeaderException;
import com.capgemini.psd2.account.information.mock.foundationservice.service.AccountInformationService;

/**
 * The Class AccountInformationController.
 */
@RestController
@RequestMapping("/fs-customeraccountprofile-service/services/customerAccountProfile/channel")
public class AccountInformationController {

	/** The account information service. */
	@Autowired
	private AccountInformationService accountInformationService;

	/**
	 * Channel A reterive account information.
	 *
	 * @param accountNsc the account nsc
	 * @param accountNumber the account number
	 * @param boiUser the boi user
	 * @param boiChannel the boi channel
	 * @param boiPlatform the boi platform
	 * @param correlationID the correlation ID
	 * @return the accounts
	 * @throws Exception the exception
	 */
	@RequestMapping(value = "/business/account/{nsc}/{accountNumber}", method = RequestMethod.GET, produces = "application/xml")
	@ResponseBody
	public Accounts channelAReteriveAccountInformation(
			@PathVariable("nsc") String accountNSC,
			@PathVariable("accountNumber") String accountNumber,
			@RequestHeader(required = false,value="X-BOI-USER") String boiUser,
			@RequestHeader(required = false,value="X-BOI-CHANNEL") String boiChannel,
			@RequestHeader(required = false,value="X-BOI-PLATFORM") String boiPlatform,
			@RequestHeader(required = false,value="X-CORRELATION-ID") String correlationID) throws Exception{
		
		if(boiUser==null || boiChannel==null || boiPlatform==null || correlationID==null ){
			throw new MissingAuthenticationHeaderException("Header Missing");
		}
		
		if(accountNSC==null || accountNumber==null )
		{
			throw new InvalidParameterRequestException("Bad request");
		}

		return accountInformationService.retrieveAccountInformation(accountNSC, accountNumber);
		
	}	
	/**
	 * Channel B reterive account information.
	 *
	 * @param accountNsc the account nsc
	 * @param accountNumber the account number
	 * @param boiUser the boi user
	 * @param boiChannel the boi channel
	 * @param boiPlatform the boi platform
	 * @param correlationID the correlation ID
	 * @return the accounts
	 * @throws Exception the exception
	 */
	@RequestMapping(value = "/personal/account/{nsc}/{accountNumber}", method = RequestMethod.GET, produces = "application/xml")
	@ResponseBody
	public Accounts channelBReteriveAccountInformation(
			@PathVariable("nsc") String accountNSC,
			@PathVariable("accountNumber") String accountNumber,
			@RequestHeader(required = false,value="X-BOI-USER") String boiUser,
			@RequestHeader(required = false,value="X-BOI-CHANNEL") String boiChannel,
			@RequestHeader(required = false,value="X-BOI-PLATFORM") String boiPlatform,
			@RequestHeader(required = false,value="X-CORRELATION-ID") String correlationID) throws Exception {
		
		if(boiUser==null || boiChannel==null || boiPlatform==null || correlationID==null ){
			throw new MissingAuthenticationHeaderException("Header Missing");
		}
		
		if(accountNSC==null || accountNumber==null )
		{
			throw new InvalidParameterRequestException("Bad request");
		}
		return accountInformationService.retrieveAccountInformation(accountNSC, accountNumber);
		

	}



}
