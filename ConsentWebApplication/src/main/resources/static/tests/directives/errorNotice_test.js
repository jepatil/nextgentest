"use strict";
describe("ConsentApp.errorNotice_test", function() {
    beforeEach(module("consentTemplates", "consentApp"));
    var compile, scope, element, allAccounts;

    beforeEach(inject(function($rootScope, $compile) {
        scope = $rootScope.$new();
        element = angular.element("<error-notice error-data='errorData'></error-notice>");
        $compile(element)(scope);
        scope.$digest();
    }));

    describe("test template", function() {
        it("should bind the errorData to template", function() {
            element.isolateScope().errorData = { errorCode: 999, correlationId: null };
            scope.$digest();
            expect(element.text()).toContain(999);
        });
    });
});