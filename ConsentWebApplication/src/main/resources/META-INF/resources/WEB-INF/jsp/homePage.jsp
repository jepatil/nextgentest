<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<%@ page session="false"%>
<html lang="en">

<head>
<script type="text/javascript">
	setTimeout(function() {
		var resumePath = document.getElementById("resumePath").value;
		window.location.href = resumePath;
	}, 100);
</script>
</head>

<body>
	<input type="hidden" name="resumePath" id="resumePath"
		value="${redirectUrl}" />
</body>
</html>