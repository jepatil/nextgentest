package com.capgemini.psd2.payment.validation.mock.foundationservice.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;
import com.capgemini.psd2.foundationservice.utilities.NullCheckUtils;
import com.capgemini.psd2.foundationservice.validator.SuccesCodeEnum;
import com.capgemini.psd2.foundationservice.validator.ValidationPassed;
import com.capgemini.psd2.foundationservice.validator.ValidationUtility;
import com.capgemini.psd2.payment.validation.mock.foundationservice.domain.PaymentInstruction;
import com.capgemini.psd2.payment.validation.mock.foundationservice.repository.PaymentValidationRepository;
import com.capgemini.psd2.payment.validation.mock.foundationservice.service.PaymentValidationService;

@Service
public class PaymentValidationServiceImpl implements PaymentValidationService{

	@Autowired
	private ValidationUtility validationUtility;
	
	@Autowired
	private PaymentValidationRepository paymentValidationRepository;
	
	@Override
	public ValidationPassed validatePaymentInstruction(PaymentInstruction paymentInstruction) {
	
		String endToEndIdentification = paymentInstruction.getEndToEndIdentification();
		ValidationPassed validationPassed =null;
		
		PaymentInstruction savedPaymentInstruction = paymentValidationRepository.findByPaymentPaymentId(paymentInstruction.getPayment().getPaymentId());
		
		if (NullCheckUtils.isNullOrEmpty(savedPaymentInstruction)) {
			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.NOT_FOUND_PMV);
		}
		
		try{
			validationPassed = validationUtility.validateMockBusinessValidations(endToEndIdentification, SuccesCodeEnum.SUCCESS_VALIDATE_PAYMENT.getSuccessCode(), SuccesCodeEnum.SUCCESS_PRESTAGE_VALIDATE_PAYMENT.getSuccessMessage());
		} catch (MockFoundationServiceException e) {		
			e.getErrorInfo().setErrorText(savedPaymentInstruction.getPayment().getPaymentInformation().getSubmissionID()+ "^" + e.getErrorInfo().getErrorText());
			throw e;
		}
				 
		validationPassed.setSuccessMessage(savedPaymentInstruction.getPayment().getPaymentInformation().getSubmissionID());				
		return validationPassed;
	
	}

}
