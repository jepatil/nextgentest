package com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.adapter.datetime.utility.TransactionDateRange;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.utility.AdapterUtility;
import com.capgemini.psd2.aisp.domain.AccountTransactionsGETResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.client.AccountTransactionsFoundationServiceClient;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.constants.AccountTransactionsFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.delegate.AccountTransactionsFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.domain.Accnt;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.domain.Accounts;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.transformer.AccountTransactionsFoundationServiceTransformer;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountTransactionsFoundationServiceDelegateTest {
	
	/** The delegate. */
	@InjectMocks
	private AccountTransactionsFoundationServiceDelegate delegate;

	/** The account transactions foundation service client. */
	@InjectMocks
	private AccountTransactionsFoundationServiceClient accountTransactionsFoundationServiceClient;

	/** The account transactions FS transformer. */
	@Mock
	private AccountTransactionsFoundationServiceTransformer accountTransactionsFSTransformer;

	/** The rest client. */
	@Mock
	private RestClientSyncImpl restClient;
	
	@Mock
	private AdapterUtility adapterUtility;
	
	@Mock
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;

	/**
	 * Sets the up.
	 */
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}

	/**
	 * Test rest transport for single account transactions.
	 */
	@Test
	public void testRestTransportForSingleTransactions() {
		Map<String, String> params = new HashMap<>();
		params.put("accountId", "12345");
		params.put("channelId", "BOL");
		Accounts accounts = new Accounts();
		Accnt accnt = new Accnt();
		accounts.getAccount().add(accnt);	
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("1234");
		accDet.setAccountNSC("US2345");
		accDet.setAccountNumber("1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		accountMapping.setCorrelationId("test");
		
		MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
		queryParams.add("FromBookingDateTime", "2016-12-31T23:59:59");
		queryParams.add("ToBookingDateTime", "2015-01-01T00:00:00");
		queryParams.add("RequestedPageNumber", "1");
		queryParams.add("TransactionFilter", "CREDIT");
		queryParams.add("transactionRetrievalKey", "trnxKey");

		Mockito.when(restClient.callForGet(any(), any(), any())).thenReturn(accounts);

		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl("http://localhost:8081/fs-abt-service/services/abt/accounts");

		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("X-BOI-USER", "header user");
		httpHeaders.add("X-BOI-CHANNEL", "header channel");
		httpHeaders.add("X-CORRELATION-ID", "header correlation id");
		httpHeaders.add("X-BOI-PLATFORM", "header platform");

		Accounts res = accountTransactionsFoundationServiceClient.restTransportForSingleAccountTransactions(requestInfo, Accounts.class, queryParams, httpHeaders);
		assertNotNull(res);
	}

	@Test
	public void testCreateTransactionDateRangeElseCases(){
		Map<String, String> params = new HashMap<>();
		params.put(AccountTransactionsFoundationServiceConstants.CONSENT_EXPIRATION_DATETIME, "2015-01-01T00:00:00");
		params.put(AccountTransactionsFoundationServiceConstants.END_DATE, "2015-01-01T00:00:00");
		params.put(AccountTransactionsFoundationServiceConstants.FROM_BOOKING_DATE_TIME, "2015-01-01T00:00:00");
		params.put(AccountTransactionsFoundationServiceConstants.REQUESTED_FROM_CONSENT_DATETIME, "2015-01-01T00:00:00");
		params.put(AccountTransactionsFoundationServiceConstants.REQUESTED_FROM_DATETIME, "2015-01-01T00:00:00");
		params.put(AccountTransactionsFoundationServiceConstants.REQUESTED_TO_CONSENT_DATETIME, "2017-01-01T00:00:00");
		params.put(AccountTransactionsFoundationServiceConstants.REQUESTED_TO_DATETIME, "2017-01-01T00:00:00");
		Mockito.when(timeZoneDateTimeAdapter.parseDateTimeFS(anyObject())).thenReturn(new Date());
		Mockito.when(timeZoneDateTimeAdapter.parseDateFS(anyObject())).thenReturn(new Date());
		ReflectionTestUtils.setField(delegate, "defaultTransactionDateRange", 1);
		TransactionDateRange res = delegate.createTransactionDateRange(params);
		assertNotNull(res);
	}
	
	@Test
	public void testCreateTransactionDateRangeIfCases(){
		Map<String, String> params = new HashMap<>();
//		params.put(AccountTransactionsFoundationServiceConstants.CONSENT_EXPIRATION_DATETIME, "2015-01-01T00:00:00");
//		params.put(AccountTransactionsFoundationServiceConstants.END_DATE, "2015-01-01T00:00:00");
//		params.put(AccountTransactionsFoundationServiceConstants.FROM_BOOKING_DATE_TIME, "2015-01-01T00:00:00");
//		params.put(AccountTransactionsFoundationServiceConstants.REQUESTED_FROM_CONSENT_DATETIME, "2015-01-01T00:00:00");
//		params.put(AccountTransactionsFoundationServiceConstants.REQUESTED_FROM_DATETIME, "2015-01-01T00:00:00");
//		params.put(AccountTransactionsFoundationServiceConstants.REQUESTED_TO_CONSENT_DATETIME, "2015-01-01T00:00:00");
//		params.put(AccountTransactionsFoundationServiceConstants.REQUESTED_TO_DATETIME, "2015-01-01T00:00:00");
		Mockito.when(timeZoneDateTimeAdapter.parseDateTimeFS(anyObject())).thenReturn(new Date());
		Mockito.when(timeZoneDateTimeAdapter.parseDateFS(anyObject())).thenReturn(new Date());
		ReflectionTestUtils.setField(delegate, "defaultTransactionDateRange", 1);
		TransactionDateRange res = delegate.createTransactionDateRange(params);
		assertNotNull(res);
	}
	
	
	@Test(expected = AdapterException.class)
	public void testFsCallFilterException(){
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		TransactionDateRange transactionDateRange = new TransactionDateRange();
		try {
			transactionDateRange.setRequestDateTime(dateFormat.parse("2017-01-01T00:00:00"));
			transactionDateRange.setConsentExpiryDateTime(dateFormat.parse("2015-01-01T00:00:00"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		delegate.fsCallFilter(transactionDateRange);
	}
	
	@Test(expected = AdapterException.class)
	public void testFsCallFilterException1(){
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		TransactionDateRange transactionDateRange = new TransactionDateRange();
		try {
			transactionDateRange.setRequestDateTime(dateFormat.parse("2017-01-01T00:00:00"));
			transactionDateRange.setConsentExpiryDateTime(dateFormat.parse("2018-01-01T00:00:00"));
			transactionDateRange.setFilterFromDate(dateFormat.parse("2017-01-01T00:00:00"));
			transactionDateRange.setFilterToDate(dateFormat.parse("2016-01-01T00:00:00"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		delegate.fsCallFilter(transactionDateRange);
	}
	
	@Test
	public void testFsCallFilter1(){
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		TransactionDateRange transactionDateRange = new TransactionDateRange();
		try {
			transactionDateRange.setRequestDateTime(dateFormat.parse("2014-01-01T00:00:00"));
			transactionDateRange.setConsentExpiryDateTime(dateFormat.parse("2015-01-01T00:00:00"));
			transactionDateRange.setFilterFromDate(dateFormat.parse("2015-01-01T00:00:00"));
			transactionDateRange.setFilterToDate(dateFormat.parse("2017-01-01T00:00:00"));
			transactionDateRange.setTransactionFromDateTime(dateFormat.parse("2015-01-02T00:00:00"));
			transactionDateRange.setTransactionToDateTime(dateFormat.parse("2017-01-01T00:00:00"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		TransactionDateRange res = delegate.fsCallFilter(transactionDateRange);
		assertEquals(true, res.isEmptyResponse());
	}
	
	@Test
	public void testFsCallFilter2(){
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		TransactionDateRange transactionDateRange = new TransactionDateRange();
		try {
			transactionDateRange.setRequestDateTime(dateFormat.parse("2014-01-01T00:00:00"));
			transactionDateRange.setConsentExpiryDateTime(dateFormat.parse("2015-01-01T00:00:00"));
			transactionDateRange.setFilterFromDate(dateFormat.parse("2015-01-01T00:00:00"));
			transactionDateRange.setFilterToDate(dateFormat.parse("2017-01-01T00:00:00"));
			transactionDateRange.setTransactionFromDateTime(dateFormat.parse("2014-01-01T00:00:00"));
			transactionDateRange.setTransactionToDateTime(dateFormat.parse("2017-01-01T00:00:00"));
			transactionDateRange.setFilterToDate(dateFormat.parse("2015-01-01T00:00:00"));
			transactionDateRange.setFilterFromDate(dateFormat.parse("2015-01-01T00:00:00"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		delegate.fsCallFilter(transactionDateRange);
	}
	
	@Test
	public void testFsCallFilter3(){
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		TransactionDateRange transactionDateRange = new TransactionDateRange();
		try {
			transactionDateRange.setRequestDateTime(dateFormat.parse("2014-01-01T00:00:00"));
			transactionDateRange.setConsentExpiryDateTime(dateFormat.parse("2015-01-01T00:00:00"));
			transactionDateRange.setFilterFromDate(dateFormat.parse("2015-01-01T00:00:00"));
			transactionDateRange.setFilterToDate(dateFormat.parse("2017-01-02T00:00:00"));
			transactionDateRange.setTransactionFromDateTime(dateFormat.parse("2014-01-01T00:00:00"));
			transactionDateRange.setTransactionToDateTime(dateFormat.parse("2017-01-01T00:00:00"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		delegate.fsCallFilter(transactionDateRange);
	}
	
	@Test
	public void testFsCallFilter4(){
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		TransactionDateRange transactionDateRange = new TransactionDateRange();
		try {
			transactionDateRange.setRequestDateTime(dateFormat.parse("2015-01-01T00:00:00"));
			transactionDateRange.setConsentExpiryDateTime(dateFormat.parse("2015-01-02T00:00:00"));
			transactionDateRange.setFilterFromDate(dateFormat.parse("2015-01-01T00:00:00"));
			transactionDateRange.setFilterToDate(dateFormat.parse("2015-01-02T00:00:00"));
			transactionDateRange.setTransactionFromDateTime(dateFormat.parse("2015-01-01T02:00:00"));
			transactionDateRange.setTransactionToDateTime(dateFormat.parse("2015-01-02T01:00:00"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		delegate.fsCallFilter(transactionDateRange);
		
		try {
			transactionDateRange.setFilterToDate(dateFormat.parse("2015-01-02T02:00:00"));
			transactionDateRange.setTransactionToDateTime(dateFormat.parse("2015-01-02T01:00:00"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		delegate.fsCallFilter(transactionDateRange);
	}
	/**
	 * Test get foundation service URL.
	 */
	@Test
	public void testGetFoundationServiceURL() {

		String accountNSC = "number";
		String accountNumber = "accountNumber";
		String channelId = "BOL";
		String baseURL = "http://localhost:8081/fs-abt-service/services/abt/accounts";
		Mockito.when(adapterUtility.retriveFoundationServiceURL(any())).thenReturn(baseURL);
		String finalURL = "http://localhost:8081/fs-abt-service/services/abt/accounts/number/accountNumber/transactions";
		String testURL = delegate.getFoundationServiceURL(accountNSC, accountNumber, channelId);
		assertEquals("Test for building URL failed.", finalURL, testURL);
		assertNotNull(testURL);
	}

	/**
	 * Test get foundation service with invalid URL.
	 */
	@Test
	public void testGetFoundationServiceWithInvalidURL() {

		String accountNSC = "number";
		String accountNumber = "accountNumber";
		String channelId = "BOL";
		String baseURL = "http://localhost:8081/fs-abt-service/services/abt/accounts/invalid";
		Mockito.when(adapterUtility.retriveFoundationServiceURL(any())).thenReturn(baseURL);
		String finalURL = "http://localhost:8081/fs-abt-service/services/abt/accounts/number/accountNumber/transactions";
		String testURL = delegate.getFoundationServiceURL(accountNSC, accountNumber, channelId);
		assertNotEquals("Test for building URL failed.", finalURL, testURL);
		assertNotNull(testURL);
	}

	/**
	 * Test get foundation service with account NSC as null.
	 */
	@Test(expected = AdapterException.class)
	public void testGetFoundationServiceWithAccountNSCAsNull() {

		String accountNSC = null;
		String accountNumber = "number";
		String channelId = "BOL";
		String baseURL = "http://localhost:8081/fs-abt-service/services/abt/accounts";
		Mockito.when(adapterUtility.retriveFoundationServiceURL(any())).thenReturn(baseURL);
		delegate.getFoundationServiceURL(accountNSC, accountNumber, channelId);

		fail("Invalid account NSC");
	}

	/**
	 * Test get foundation service with account number as null.
	 */
	@Test(expected = AdapterException.class)
	public void testGetFoundationServiceWithAccountNumberAsNull() {

		String accountNSC = "nsc";
		String accountNumber = null;
		String baseURL = "http://localhost:8081/fs-abt-service/services/abt/accounts";

		delegate.getFoundationServiceURL(accountNSC, accountNumber, baseURL);

		fail("Invalid Account Number");
	}

	/**
	 * Test get foundation service with base URL as null.
	 */
	@Test(expected = AdapterException.class)
	public void testGetFoundationServiceWithBaseURLAsNull() {

		String accountNSC = "nsc";
		String accountNumber = "number";
		String baseURL = null;

		delegate.getFoundationServiceURL(accountNSC, accountNumber, baseURL);

		fail("Invalid base URL");
	}

	/**
	 * Test create request headers actual.
	 */
	@Test
	public void testCreateRequestHeadersActual() {

		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("userId");
		accountMapping.setCorrelationId("correlationId");

		HttpHeaders httpHeaders = new HttpHeaders();
		ReflectionTestUtils.setField(delegate, "userInReqHeader", "X-BOI-USER");
		ReflectionTestUtils.setField(delegate, "channelInReqHeader", "X-BOI-CHANNEL");
		ReflectionTestUtils.setField(delegate, "platformInReqHeader", "X-BOI-PLATFORM");
		ReflectionTestUtils.setField(delegate, "correlationReqHeader", "X-CORRELATION-ID");
		
		Map<String, String> params = new HashMap<String, String>();
		params.put("channelId", "BOL");

		httpHeaders = delegate.createRequestHeaders(new RequestInfo(), accountMapping, params);

		assertNotNull(httpHeaders);
	}

	/**
	 * Test transform response from FD to API.
	 */
	@Test
	public void testTransformResponseFromFDToAPI() {
		Map<String, String> params = new HashMap<>();
		params.put("accountId", "12345");
		params.put("channelId", "BOL");
		Accounts accounts = new Accounts();
		Accnt accnt = new Accnt();
		accounts.getAccount().add(accnt);	
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("1234");
		accDet.setAccountNSC("US2345");
		accDet.setAccountNumber("1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		accountMapping.setCorrelationId("test");

		Mockito.when(delegate.transformResponseFromFDToAPI(any(), any())).thenReturn(new AccountTransactionsGETResponse());
		AccountTransactionsGETResponse res = delegate.transformResponseFromFDToAPI(accounts, new HashMap<String, String>());
		assertNotNull(res);

	}
	
	@Test(expected = AdapterException.class)
	public void testGetFoundationServiceURLBaseURLEmpty() {
		when(adapterUtility.retriveFoundationServiceURL("test")).thenReturn(null);
		delegate.getFoundationServiceURL("test", "test", "test");
	}

}
