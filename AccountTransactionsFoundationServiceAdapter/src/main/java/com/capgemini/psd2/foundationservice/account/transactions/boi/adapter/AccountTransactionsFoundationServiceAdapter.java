/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.transactions.boi.adapter;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.capgemini.psd2.adapter.datetime.utility.TransactionDateRange;
import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.adapter.AccountTransactionAdapter;
import com.capgemini.psd2.aisp.domain.AccountTransactionsGETResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.client.AccountTransactionsFoundationServiceClient;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.constants.AccountTransactionsFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.delegate.AccountTransactionsFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.domain.Accounts;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.CustomerAccountsFilterFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion.AdapterFilterUtility;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;

/**
 * The Class AccountTransactionsFoundationServiceAdapter.
 */
@EnableConfigurationProperties
@ConfigurationProperties("foundationService")
@Component
public class AccountTransactionsFoundationServiceAdapter implements AccountTransactionAdapter {

	private Map<String, String> foundationCustProfileUrl = new HashMap<>();
	
	@Value("${foundationService.consentFlowType}")
	private String consentFlowType; 

	/** The account transactions foundation service delegate. */
	@Autowired
	private AccountTransactionsFoundationServiceDelegate accountTransactionsFoundationServiceDelegate;
	
	@Autowired
	private CustomerAccountsFilterFoundationServiceAdapter commonFilterUtility;
	
	@Autowired
	private AdapterFilterUtility adapterFilterUtility;

	/** The account transactions foundation service client. */
	@Autowired
	private AccountTransactionsFoundationServiceClient accountTransactionsFoundationServiceClient;
	
	@Override
	public AccountTransactionsGETResponse retrieveAccountTransaction(AccountMapping accountMapping, Map<String, String> params) {
	
		AccountDetails accountDetails;
		if (accountMapping.getAccountDetails() != null && !accountMapping.getAccountDetails().isEmpty()) {
			accountDetails = accountMapping.getAccountDetails().get(0);
			if (NullCheckUtils.isNullOrEmpty(accountDetails.getAccountId())) {
				throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
			}
			params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_ID, accountDetails.getAccountId());
		} else
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
	    params.put(PSD2Constants.CHANNEL_ID, params.get("channelId"));
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, consentFlowType);
		params.put(PSD2Constants.USER_ID, accountMapping.getPsuId());
		params.put(PSD2Constants.CORRELATION_ID, accountMapping.getCorrelationId());
		
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts filteredAccounts = commonFilterUtility.retrieveCustomerAccountList(accountMapping.getPsuId(), params);
		if (filteredAccounts == null || filteredAccounts.getAccount() == null || filteredAccounts.getAccount().isEmpty()) {
            throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.ACCOUNT_NOT_FOUND);
		}
		accountMapping = adapterFilterUtility.prevalidateAccounts(filteredAccounts, accountMapping);
		
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders httpHeaders = accountTransactionsFoundationServiceDelegate.createRequestHeaders(requestInfo, accountMapping, params);

		String finalURL = accountTransactionsFoundationServiceDelegate.getFoundationServiceURL(accountDetails.getAccountNSC(), accountDetails.getAccountNumber(), params.get(PSD2Constants.CHANNEL_ID));
		requestInfo.setUrl(finalURL);
		
		/*TransactionDateRange Check*/
		String fromBookingDateTimeInString = null;
		String toBookingDateTimeInString = null;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");		
		TransactionDateRange transactionDateRange = accountTransactionsFoundationServiceDelegate.createTransactionDateRange(params);
		TransactionDateRange decision =accountTransactionsFoundationServiceDelegate.fsCallFilter(transactionDateRange);
		if (decision.isEmptyResponse()) {
			return new AccountTransactionsGETResponse();
		} else {
			fromBookingDateTimeInString = formatter.format(decision.getNewFilterFromDate());
			toBookingDateTimeInString = formatter.format(decision.getNewFilterToDate());
		}
						
		/**to send query params*/
		MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<>();
		queryParams.add(AccountTransactionsFoundationServiceConstants.START_DATE, fromBookingDateTimeInString);
		queryParams.add(AccountTransactionsFoundationServiceConstants.END_DATE, toBookingDateTimeInString);
		queryParams.add(AccountTransactionsFoundationServiceConstants.PAGE_NUMBER, params.get(AccountTransactionsFoundationServiceConstants.REQUESTED_PAGE_NUMBER));
		queryParams.add(AccountTransactionsFoundationServiceConstants.TRANSACTION_RETRIEVAL_KEY, params.get(AccountTransactionsFoundationServiceConstants.REQUESTED_TXN_KEY));
		queryParams.add(AccountTransactionsFoundationServiceConstants.TRANSACTION_TYPE, params.get(AccountTransactionsFoundationServiceConstants.REQUESTED_TXN_FILTER));
		queryParams.add(AccountTransactionsFoundationServiceConstants.PAGE_SIZE, params.get(AccountTransactionsFoundationServiceConstants.REQUESTED_PAGE_SIZE));
		Accounts accounts = null;
		try{
			accounts = accountTransactionsFoundationServiceClient.restTransportForSingleAccountTransactions(requestInfo, Accounts.class, queryParams, httpHeaders);
			int minPageSize = Integer.parseInt(params.get(AccountTransactionsFoundationServiceConstants.REQUESTED_MIN_PAGE_SIZE));
			int maxPageSize = Integer.parseInt(params.get(AccountTransactionsFoundationServiceConstants.REQUESTED_MAX_PAGE_SIZE));
			int actualFSPageSize = accounts.getAccount().get(0).getTransactions().getPageSize();
			if(accounts.getAccount().get(0).getTransactions().isHasMoreTxns()) {
				if(actualFSPageSize < minPageSize || actualFSPageSize > maxPageSize)
					throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.TECHNICAL_ERROR);
			} else {
				if(actualFSPageSize > maxPageSize) 
					throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.TECHNICAL_ERROR);
			}
		} catch(PSD2Exception ex) {
			if(AccountTransactionsFoundationServiceConstants.ERROR_CODE_404.equals(ex.getErrorInfo().getStatusCode())) {
				return new AccountTransactionsGETResponse();
			}else if(AccountTransactionsFoundationServiceConstants.ERROR_CODE_500.equals(ex.getErrorInfo().getStatusCode())) {
				throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.TECHNICAL_ERROR);
			}
		}
		
		if(accounts == null) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND_FOUNDATION_SERVICE);
		}
		return accountTransactionsFoundationServiceDelegate.transformResponseFromFDToAPI(accounts, params);
	}

	public Map<String, String> getFoundationCustProfileUrl() {
		return foundationCustProfileUrl;
	}

	public void setFoundationCustProfileUrl(Map<String, String> foundationCustProfileUrl) {
		this.foundationCustProfileUrl = foundationCustProfileUrl;
	}

}
