/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.delegate;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.adapter.datetime.utility.TransactionDateRange;
import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.utility.AdapterUtility;
import com.capgemini.psd2.aisp.domain.AccountTransactionsGETResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.constants.AccountTransactionsFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.domain.Accounts;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.transformer.AccountTransactionsFoundationServiceTransformer;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;

/**
 * The Class AccountTransactionsFoundationServiceDelegate.
 */
@Component
public class AccountTransactionsFoundationServiceDelegate {
	
	/** The account transactions FS transformer. */
	@Autowired
	private AccountTransactionsFoundationServiceTransformer accountTransactionsFSTransformer;
	
	@Autowired
	private AdapterUtility adapterUtility; 
	
	@Autowired
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;
	
	/** The user in req header. */
	@Value("${foundationService.userInReqHeader:#{X-BOI-USER}}")
	private String userInReqHeader;

	/** The channel in req header. */
	@Value("${foundationService.channelInReqHeader:#{X-BOI-CHANNEL}}")
	private String channelInReqHeader;

	/** The platform in req header. */
	@Value("${foundationService.platformInReqHeader:#{X-BOI-PLATFORM}}")
	private String platformInReqHeader;

	/** The correlation req header. */
	@Value("${foundationService.correlationReqHeader:#{X-CORRELATION-ID}}")
	private String correlationReqHeader;
	
	/** The platform. */
	@Value("${app.platform}")
	private String platform;
	
	@Value("${foundationService.defaultTransactionDateRange}")
	private int defaultTransactionDateRange;

	
	/**
	 * Gets the foundation service URL.
	 *
	 * @param accountNSC the account NSC
	 * @param accountNumber the account number
	 * @param baseURL the base URL
	 * @return the foundation service URL
	 */
	public String getFoundationServiceURL(String accountNSC, String accountNumber, String channelId){
		if(NullCheckUtils.isNullOrEmpty(accountNSC)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_NSC_IN_REQUEST);
		}
		if(NullCheckUtils.isNullOrEmpty(accountNumber)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}
		if(NullCheckUtils.isNullOrEmpty(channelId)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		String baseURL = adapterUtility.retriveFoundationServiceURL(channelId);
		if(NullCheckUtils.isNullOrEmpty(baseURL)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		return baseURL + "/" + accountNSC + "/" +accountNumber + "/" + "transactions";
	}
	
	/**
	 * Transform response from FD to API.
	 *
	 * @param transactions the transactions
	 * @param params the params
	 * @return the account transactions GET response
	 */
	public AccountTransactionsGETResponse transformResponseFromFDToAPI(Accounts accounts, Map<String, String> params){
		return accountTransactionsFSTransformer.transformAccountTransaction(accounts, params);
	}
	
	/**
	 * Creates the request headers.
	 *
	 * @param requestInfo the request info
	 * @param accountMapping the account mapping
	 * @return the http headers
	 */
	public HttpHeaders createRequestHeaders(RequestInfo requestInfo, AccountMapping accountMapping, Map<String, String> params) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(userInReqHeader, accountMapping.getPsuId());
		httpHeaders.add(channelInReqHeader, params.get(PSD2Constants.CHANNEL_ID));
		httpHeaders.add(platformInReqHeader, platform);
		httpHeaders.add(correlationReqHeader, accountMapping.getCorrelationId());
		return httpHeaders;
	}
	
	
	public TransactionDateRange createTransactionDateRange(Map<String, String> params) {
		
		TransactionDateRange transactionDateRange = new TransactionDateRange();		
		//Consent Expiry 
		if (NullCheckUtils.isNullOrEmpty(params.get(AccountTransactionsFoundationServiceConstants.CONSENT_EXPIRATION_DATETIME))) {
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.DATE, 1);
			transactionDateRange.setConsentExpiryDateTime(calendar.getTime());
		} else {
			transactionDateRange.setConsentExpiryDateTime(timeZoneDateTimeAdapter.parseDateTimeFS(params.get(AccountTransactionsFoundationServiceConstants.CONSENT_EXPIRATION_DATETIME)));
		}			
		//TransactionFromDateTime
		if (NullCheckUtils.isNullOrEmpty(params.get(AccountTransactionsFoundationServiceConstants.REQUESTED_FROM_CONSENT_DATETIME))) {
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.MONTH, defaultTransactionDateRange);
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MINUTE, 0);
			transactionDateRange.setTransactionFromDateTime(calendar.getTime());
		} else {
			transactionDateRange.setTransactionFromDateTime(timeZoneDateTimeAdapter.parseDateFS(params.get(AccountTransactionsFoundationServiceConstants.REQUESTED_FROM_CONSENT_DATETIME)));
		}
		//TransactionToDateTime
		if (NullCheckUtils.isNullOrEmpty(params.get(AccountTransactionsFoundationServiceConstants.REQUESTED_TO_CONSENT_DATETIME))) {
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MINUTE, 0);
			transactionDateRange.setTransactionToDateTime(calendar.getTime());
		} else {
			transactionDateRange.setTransactionToDateTime(timeZoneDateTimeAdapter.parseDateFS(params.get(AccountTransactionsFoundationServiceConstants.REQUESTED_TO_CONSENT_DATETIME)));
		}
		//Request DateTime
		transactionDateRange.setRequestDateTime(new Date());
		//FilterFromDate
		if (NullCheckUtils.isNullOrEmpty(params.get(AccountTransactionsFoundationServiceConstants.REQUESTED_FROM_DATETIME))) {		
			transactionDateRange.setFilterFromDate(transactionDateRange.getTransactionFromDateTime());
		} else {
			transactionDateRange.setFilterFromDate(timeZoneDateTimeAdapter.parseDateFS(params.get(AccountTransactionsFoundationServiceConstants.REQUESTED_FROM_DATETIME)));
		}
		//FilterToDate
		if (NullCheckUtils.isNullOrEmpty(params.get(AccountTransactionsFoundationServiceConstants.REQUESTED_TO_DATETIME))) {
			transactionDateRange.setFilterToDate(transactionDateRange.getTransactionToDateTime());
		} else {
			transactionDateRange.setFilterToDate(timeZoneDateTimeAdapter.parseDateFS(params.get(AccountTransactionsFoundationServiceConstants.REQUESTED_TO_DATETIME)));
		}
		//To > ConExp
		if (transactionDateRange.getTransactionToDateTime().getTime() > transactionDateRange.getConsentExpiryDateTime().getTime()) {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			transactionDateRange.setTransactionToDateTime(timeZoneDateTimeAdapter.parseDateFS(formatter.format(transactionDateRange.getConsentExpiryDateTime())));
		}
		return transactionDateRange;
	}
	
	
	//TransactionDateRange Validation
	public TransactionDateRange fsCallFilter(TransactionDateRange transactionDateRange) {
	
		if (transactionDateRange.getRequestDateTime().getTime() > transactionDateRange.getConsentExpiryDateTime().getTime()) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.CONSENT_EXPIRED);
		}
		else if (transactionDateRange.getFilterFromDate().getTime() > transactionDateRange.getFilterToDate().getTime()) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.INVALID_FILTER);
		}
		else if (transactionDateRange.getTransactionFromDateTime().getTime() > transactionDateRange.getConsentExpiryDateTime().getTime()
				|| transactionDateRange.getTransactionFromDateTime().getTime() > transactionDateRange.getFilterToDate().getTime()
				|| transactionDateRange.getFilterFromDate().getTime() > transactionDateRange.getTransactionToDateTime().getTime()) {
			transactionDateRange.setEmptyResponse(true);
		}
		else if (transactionDateRange.getTransactionFromDateTime().getTime() <= transactionDateRange.getFilterFromDate().getTime()) {
			if (transactionDateRange.getFilterToDate().getTime() <= transactionDateRange.getTransactionToDateTime().getTime()) {
				transactionDateRange.setNewFilterFromDate(transactionDateRange.getFilterFromDate());
				transactionDateRange.setNewFilterToDate(transactionDateRange.getFilterToDate());

			} else {
				transactionDateRange.setNewFilterFromDate(transactionDateRange.getFilterFromDate());
				transactionDateRange.setNewFilterToDate(transactionDateRange.getTransactionToDateTime());
			}
		}
		else if (transactionDateRange.getFilterToDate().getTime() <= transactionDateRange.getTransactionToDateTime().getTime()) {
			transactionDateRange.setNewFilterFromDate(transactionDateRange.getTransactionFromDateTime());
			transactionDateRange.setNewFilterToDate(transactionDateRange.getFilterToDate());
		}
		else {
			transactionDateRange.setNewFilterFromDate(transactionDateRange.getTransactionFromDateTime());
			transactionDateRange.setNewFilterToDate(transactionDateRange.getTransactionToDateTime());
		}		
		return transactionDateRange;		
	}
	
}
