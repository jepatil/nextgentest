package com.capgemini.psd2.test.mock.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.capgemini.psd2.aisp.domain.AccountRequestPOSTResponse;
import com.capgemini.psd2.aisp.domain.AccountTransactionsGETResponse;
import com.capgemini.psd2.aisp.domain.Data1;
import com.capgemini.psd2.aisp.domain.Data3;
import com.capgemini.psd2.aisp.domain.Data3Amount;
import com.capgemini.psd2.aisp.domain.Data3Balance;
import com.capgemini.psd2.aisp.domain.Data3BalanceAmount;
import com.capgemini.psd2.aisp.domain.Data3BankTransactionCode;
import com.capgemini.psd2.aisp.domain.Data3MerchantDetails;
import com.capgemini.psd2.aisp.domain.Data3ProprietaryBankTransactionCode;
import com.capgemini.psd2.aisp.domain.Data3Transaction;
import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.MetaData;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.token.ConsentTokenData;
import com.capgemini.psd2.token.Token;

public class AccountTransactionApiTestData {

	static Data3Transaction data = new Data3Transaction();
	static Data3Amount amount = new Data3Amount();
	static Data3Balance balance = new Data3Balance();
	static Data3BalanceAmount amount2 = new Data3BalanceAmount();
	static Data3BankTransactionCode bankTransactionCode = new Data3BankTransactionCode();
	static Data3ProprietaryBankTransactionCode proprietaryBankTransactionCode = new Data3ProprietaryBankTransactionCode();
	static Data3MerchantDetails merchantDetails = new Data3MerchantDetails();

	public static String getTestAccountId() {
		return "269c3ff5-d7f8-419b-a3b9-7136c5b4611a";
	}

	public static String getTestBankID() {
		return "TestBankID";
	}

	public static AccountRequestPOSTResponse getAccountRequestPOSTResponse() {
		AccountRequestPOSTResponse accountRequestPOSTResponse = new AccountRequestPOSTResponse();
		Data1 respData = new Data1();
		respData.setAccountRequestId("123434321");
		respData.setTransactionFromDateTime("2017-05-03T00:00:00+00:00");
		respData.setTransactionToDateTime("2017-12-03T00:00:00+00:00");
		accountRequestPOSTResponse.setData(respData);
		accountRequestPOSTResponse.setRisk(new Object());
		return accountRequestPOSTResponse;
	}

	public static AccountRequestPOSTResponse getAccountRequestPOSTResponse2() {
		AccountRequestPOSTResponse accountRequestPOSTResponse = new AccountRequestPOSTResponse();
		Data1 respData = new Data1();
		respData.setAccountRequestId("123434321");
		accountRequestPOSTResponse.setData(respData);
		accountRequestPOSTResponse.setRisk(new Object());
		return accountRequestPOSTResponse;
	}

	public static AccountRequestPOSTResponse getAccountRequestPOSTResponse3() {
		AccountRequestPOSTResponse accountRequestPOSTResponse = new AccountRequestPOSTResponse();
		Data1 respData = new Data1();
		respData.setAccountRequestId("123434321");
		respData.setTransactionFromDateTime("2015-01-02T00:00:00+00:00");
		respData.setTransactionToDateTime("2017-01-02T00:00:00+00:00");
		accountRequestPOSTResponse.setData(respData);
		accountRequestPOSTResponse.setRisk(new Object());
		return accountRequestPOSTResponse;
	}

	public static AccountRequestPOSTResponse getAccountRequestPOSTResponseWithNoDates() {
		AccountRequestPOSTResponse accountRequestPOSTResponse = new AccountRequestPOSTResponse();
		Data1 respData = new Data1();
		respData.setAccountRequestId("123434321");
		accountRequestPOSTResponse.setData(respData);
		accountRequestPOSTResponse.setRisk(new Object());
		return accountRequestPOSTResponse;
	}

	public static List<Object> getMockClaimsList() {
		List<Object> claimList = new ArrayList<Object>();
		claimList.add("ReadTransactionsCredits");
		claimList.add("ReadTransactionsDebits");
		return claimList;
	}

	public static AccountTransactionsGETResponse getAccountTransactionsGETResponse() {
		AccountTransactionsGETResponse resp = new AccountTransactionsGETResponse();
		List<Data3Transaction> dataList = new ArrayList<Data3Transaction>();
		data.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		data.setTransactionId("123");
		data.setTransactionReference("Ref123");
		amount.setAmount("10.00");
		amount.setCurrency("GBP");
		data.setAmount(amount);
		data.setCreditDebitIndicator(com.capgemini.psd2.aisp.domain.Data3Transaction.CreditDebitIndicatorEnum.CREDIT);
		data.setStatus(com.capgemini.psd2.aisp.domain.Data3Transaction.StatusEnum.BOOKED);
		data.setBookingDateTime("2017-04-05T10:43:07+00:00");
		data.setValueDateTime("2017-04-05T10:45:22+00:00");
		data.setTransactionInformation("Cash from Aubrey");
		data.setAddressLine("XYZ address Line");
		bankTransactionCode.setCode("ReceivedCreditTransfer");
		bankTransactionCode.setSubCode("DomesticCreditTransfer");
		data.setBankTransactionCode(bankTransactionCode);
		proprietaryBankTransactionCode.setCode("Transfer");
		proprietaryBankTransactionCode.setIssuer("AlphaBank");
		data.setProprietaryBankTransactionCode(proprietaryBankTransactionCode);
		balance.setAmount(amount2);
		amount2.setAmount("230.00");
		amount2.setCurrency("GBP");
		balance.setCreditDebitIndicator(
				com.capgemini.psd2.aisp.domain.Data3Balance.CreditDebitIndicatorEnum.CREDIT);
		balance.setType(com.capgemini.psd2.aisp.domain.Data3Balance.TypeEnum.INTERIMBOOKED);
		data.setBalance(balance);
		data.setMerchantDetails(merchantDetails);
		merchantDetails.setMerchantCategoryCode("MerchantXYZcode");
		merchantDetails.setMerchantName("MerchantXYZ");
		dataList.add(data);
		Data3 data3 = new Data3();
		data3.setTransaction(dataList);
		resp.setData(data3);
		Links links = new Links();
		links.setFirst("1");
		links.setLast("10");
		links.setNext("5");
		links.setPrev("3");
		links.setSelf("4");
		resp.setLinks(links);
		MetaData meta = new MetaData();
		resp.setMeta(meta);
		return resp;
	}

	public static Token getMockTokenWithALLFilter() {
		Token x = new Token();
		x.setRequestId("123434321");
		Map<String, Set<Object>> claims = new HashMap<String, Set<Object>>();
		Set<Object> value = new HashSet<Object>();
		value.add("ReadTransactionsCredits");
		value.add("ReadTransactionsDebits");
		claims.put("accounts", value);
		x.setClaims(claims);
		ConsentTokenData consentTokenData = new ConsentTokenData();
		x.setConsentTokenData(consentTokenData);
		Map<String, String> seviceParams = new HashMap<>();
		seviceParams.put("channelId", "demoChannelID");
		x.setSeviceParams(seviceParams);
		return x;
	}

	public static Token getMockTokenWithCreditFilter() {
		Token x = new Token();
		x.setRequestId("123434321");
		Map<String, Set<Object>> claims = new HashMap<String, Set<Object>>();
		Set<Object> value = new HashSet<Object>();
		value.add("ReadTransactionsCredits");
		claims.put("accounts", value);
		x.setClaims(claims);
		ConsentTokenData consentTokenData = new ConsentTokenData();
		x.setConsentTokenData(consentTokenData);
		Map<String, String> seviceParams = new HashMap<>();
		seviceParams.put("channelId", "demoChannelID");
		x.setSeviceParams(seviceParams);
		return x;
	}

	public static Token getMockTokenWithDebitFilter() {
		Token x = new Token();
		x.setRequestId("123434321");
		Map<String, Set<Object>> claims = new HashMap<String, Set<Object>>();
		Set<Object> value = new HashSet<Object>();
		value.add("ReadTransactionsDebits");
		claims.put("accounts", value);
		x.setClaims(claims);
		ConsentTokenData consentTokenData = new ConsentTokenData();
		x.setConsentTokenData(consentTokenData);
		Map<String, String> seviceParams = new HashMap<>();
		seviceParams.put("channelId", "demoChannelID");
		x.setSeviceParams(seviceParams);
		return x;
	}

	public static AccountTransactionsGETResponse getAccountTransactionsGETResponseWithNullLinks() {
		AccountTransactionsGETResponse resp = new AccountTransactionsGETResponse();
		List<Data3Transaction> dataList = new ArrayList<Data3Transaction>();
		data.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		data.setTransactionId("123");
		data.setTransactionReference("Ref123");
		amount.setAmount("10.00");
		amount.setCurrency("GBP");
		data.setAmount(amount);
		data.setCreditDebitIndicator(com.capgemini.psd2.aisp.domain.Data3Transaction.CreditDebitIndicatorEnum.CREDIT);
		data.setStatus(com.capgemini.psd2.aisp.domain.Data3Transaction.StatusEnum.BOOKED);
		data.setBookingDateTime("2017-04-05T10:43:07+00:00");
		data.setValueDateTime("2017-04-05T10:45:22+00:00");
		data.setTransactionInformation("Cash from Aubrey");
		data.setAddressLine("XYZ address Line");
		bankTransactionCode.setCode("ReceivedCreditTransfer");
		bankTransactionCode.setSubCode("DomesticCreditTransfer");
		data.setBankTransactionCode(bankTransactionCode);
		proprietaryBankTransactionCode.setCode("Transfer");
		proprietaryBankTransactionCode.setIssuer("AlphaBank");
		data.setProprietaryBankTransactionCode(proprietaryBankTransactionCode);
		balance.setAmount(amount2);
		amount2.setAmount("230.00");
		amount2.setCurrency("GBP");
		balance.setCreditDebitIndicator(
				com.capgemini.psd2.aisp.domain.Data3Balance.CreditDebitIndicatorEnum.CREDIT);
		balance.setType(com.capgemini.psd2.aisp.domain.Data3Balance.TypeEnum.INTERIMBOOKED);
		data.setBalance(balance);
		data.setMerchantDetails(merchantDetails);
		merchantDetails.setMerchantCategoryCode("MerchantXYZcode");
		merchantDetails.setMerchantName("MerchantXYZ");
		dataList.add(data);
		Data3 data3 = new Data3();
		data3.setTransaction(dataList);
		resp.setData(data3);
		Links links = new Links();
		resp.setLinks(links);
		return resp;
	}

	public static AccountTransactionsGETResponse getAccountTransactionsGETResponseWithNullLinksObject() {
		AccountTransactionsGETResponse resp = new AccountTransactionsGETResponse();
		List<Data3Transaction> dataList = new ArrayList<Data3Transaction>();
		data.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		data.setTransactionId("123");
		data.setTransactionReference("Ref123");
		amount.setAmount("10.00");
		amount.setCurrency("GBP");
		data.setAmount(amount);
		data.setCreditDebitIndicator(com.capgemini.psd2.aisp.domain.Data3Transaction.CreditDebitIndicatorEnum.CREDIT);
		data.setStatus(com.capgemini.psd2.aisp.domain.Data3Transaction.StatusEnum.BOOKED);
		data.setBookingDateTime("2017-04-05T10:43:07+00:00");
		data.setValueDateTime("2017-04-05T10:45:22+00:00");
		data.setTransactionInformation("Cash from Aubrey");
		data.setAddressLine("XYZ address Line");
		bankTransactionCode.setCode("ReceivedCreditTransfer");
		bankTransactionCode.setSubCode("DomesticCreditTransfer");
		data.setBankTransactionCode(bankTransactionCode);
		proprietaryBankTransactionCode.setCode("Transfer");
		proprietaryBankTransactionCode.setIssuer("AlphaBank");
		data.setProprietaryBankTransactionCode(proprietaryBankTransactionCode);
		balance.setAmount(amount2);
		amount2.setAmount("230.00");
		amount2.setCurrency("GBP");
		balance.setCreditDebitIndicator(
				com.capgemini.psd2.aisp.domain.Data3Balance.CreditDebitIndicatorEnum.CREDIT);
		balance.setType(com.capgemini.psd2.aisp.domain.Data3Balance.TypeEnum.INTERIMBOOKED);
		data.setBalance(balance);
		data.setMerchantDetails(merchantDetails);
		merchantDetails.setMerchantCategoryCode("MerchantXYZcode");
		merchantDetails.setMerchantName("MerchantXYZ");
		dataList.add(data);
		Data3 data3 = new Data3();
		data3.setTransaction(dataList);
		resp.setData(data3);
		return resp;
	}

	public static AispConsent getAispConsent() {
		AispConsent aispConsent = new AispConsent();
		aispConsent.setTransactionFromDateTime("2017-05-03T00:00:00");
		aispConsent.setTransactionToDateTime("2017-12-03T00:00:00");
		return aispConsent;
	}
	
	public static AispConsent getAispConsent2() {
		AispConsent aispConsent = new AispConsent();
		aispConsent.setTransactionFromDateTime("2015-01-02T00:00:00");
		aispConsent.setTransactionToDateTime("2017-01-02T00:00:00");
		return aispConsent;
	}
}