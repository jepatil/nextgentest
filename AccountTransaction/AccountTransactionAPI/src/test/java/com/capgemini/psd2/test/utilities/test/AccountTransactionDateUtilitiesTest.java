package com.capgemini.psd2.test.utilities.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.account.transaction.utilities.AccountTransactionDateUtilities;
import com.capgemini.psd2.exceptions.PSD2Exception;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountTransactionDateUtilitiesTest {

	@InjectMocks
	static AccountTransactionDateUtilities utility = new AccountTransactionDateUtilities();

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void convertDateStringToLocalDateTimeTest() {
		assertEquals("2017-08-17T05:05:05.123",
				utility.convertDateStringToLocalDateTime("2017-08-17T05:05:05.123").toString());
	}

	@Test(expected = PSD2Exception.class)
	public void AccountTransactionDateUtilitiesException() {
		utility.convertDateStringToLocalDateTime("2017/08/17");
	}

	@Test
	public void convertZonedStringToLocalDateTimeTest() {
		assertNull(utility.convertZonedStringToLocalDateTime(null));
	}

	@Test(expected = PSD2Exception.class)
	public void convertZonedStringToLocalDateTimeException() {
		utility.convertZonedStringToLocalDateTime("2017/08/17");
	}

	@Test
	public void convertZonedDateTimeToStringTest() {
        String dt = "2019-01-19T00:00:00";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
        LocalDateTime formatDateTime = LocalDateTime.parse(dt, formatter);
			assertEquals("2019-01-19T00:00:00.000", utility.convertZonedDateTimeToString(formatDateTime));
	}

	@After
	public void tearDown() throws Exception {
		utility = null;
	}
}
