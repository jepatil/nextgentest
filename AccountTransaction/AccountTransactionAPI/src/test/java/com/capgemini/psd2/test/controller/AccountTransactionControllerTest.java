/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.test.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.capgemini.psd2.account.transaction.controller.AccountTransactionController;
import com.capgemini.psd2.account.transaction.service.impl.AccountTransactionServiceImpl;
import com.capgemini.psd2.account.transaction.utilities.AccountTransactionValidationUtilities;
import com.capgemini.psd2.test.mock.data.AccountTransactionApiTestData;

/**
 * The Class AccountInformationControllerTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountTransactionControllerTest {

	/** The service. */
	@Mock
	private AccountTransactionServiceImpl service;
	
	@Mock
	AccountTransactionValidationUtilities accountTransactionValidationUtilities;

	/** The mock mvc. */
	private MockMvc mockMvc;

	/** The controller. */
	@InjectMocks
	private AccountTransactionController controller;

	/**
	 * Sets the up.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(controller).dispatchOptions(true).build();
	}

	/**
	 * Test retrieve account information success flow.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Test
	public void testRetrieveAccountTransactionSuccessFlow() throws Exception {
		
		Mockito.when(service.retrieveAccountTransaction(anyString(), anyString(), anyString(), anyInt(), anyInt(), anyString()))
				.thenReturn(AccountTransactionApiTestData.getAccountTransactionsGETResponse());

		this.mockMvc.perform(get("/accounts/{accountId}/transactions", AccountTransactionApiTestData.getTestAccountId())
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}

	/**
	 * Test retrieve account information success data.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Test
	public void testRetrieveAccountTransactionSuccessData() throws Exception {

		Mockito.when(service.retrieveAccountTransaction(anyString(), anyString(), anyString(), anyInt(), anyInt(), anyString()))
				.thenReturn(AccountTransactionApiTestData.getAccountTransactionsGETResponse());

		assertEquals(AccountTransactionApiTestData.getAccountTransactionsGETResponse(), controller
				.retrieveAccountTransaction(AccountTransactionApiTestData.getTestAccountId(), "", "", 1, 1, ""));

		String expectedJson = "{\"Data\": {\"Transaction\": [{\"AccountId\": \"269c3ff5-d7f8-419b-a3b9-7136c5b4611a\",\"TransactionId\": \"123\",\"TransactionReference\": \"Ref123\",\"Amount\": {\"Amount\": \"10.00\",\"Currency\": \"GBP\"},\"CreditDebitIndicator\": \"Credit\",\"Status\": \"Booked\",\"BookingDateTime\": \"2017-04-05T10:43:07+00:00\",\"ValueDateTime\": \"2017-04-05T10:45:22+00:00\",\"TransactionInformation\": \"Cash from Aubrey\",\"AddressLine\": \"XYZ address Line\",\"BankTransactionCode\": {\"Code\": \"ReceivedCreditTransfer\",\"SubCode\": \"DomesticCreditTransfer\"},\"ProprietaryBankTransactionCode\": {\"Code\": \"Transfer\",\"Issuer\": \"AlphaBank\"},\"Balance\": {\"Amount\": {\"Amount\": \"230.00\",\"Currency\": \"GBP\"},\"CreditDebitIndicator\": \"Credit\",\"Type\": \"InterimBooked\"},\"MerchantDetails\": {\"MerchantName\": \"MerchantXYZ\",\"MerchantCategoryCode\": \"MerchantXYZcode\"}}]}}";

		this.mockMvc.perform(get("/accounts/{accountId}/transactions", AccountTransactionApiTestData.getTestAccountId())
				.contentType(MediaType.APPLICATION_JSON)).andExpect(content().json(expectedJson));

	}

	/**
	 * Tear down.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@After
	public void tearDown() throws Exception {
		controller = null;
	}
}
