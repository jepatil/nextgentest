package com.capgemini.psd2.account.transaction.utilities;

import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class AccountTransactionValidationUtilities {

	@Value("${app.minPageSize}")
	private String minPageSize;

	@Value("${app.maxPageSize}")
	private String maxPageSize;

	public void validateParams(String accountId, Integer pageNumber, Integer pageSize) {

		if (NullCheckUtils.isNullOrEmpty(accountId))
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_ACCOUNT_ID_FOUND);
		if (!Pattern.matches("[a-zA-Z0-9-]{1,40}", accountId))
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.VALIDATION_ERROR);
		if ((pageNumber != null) && (pageNumber.intValue() < 0))
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.VALIDATION_ERROR);
		if ((pageSize != null) && ((pageSize.intValue() < Integer.valueOf(minPageSize))
				|| (pageSize.intValue() > Integer.valueOf(maxPageSize))))
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.VALIDATION_ERROR);
	}
}
