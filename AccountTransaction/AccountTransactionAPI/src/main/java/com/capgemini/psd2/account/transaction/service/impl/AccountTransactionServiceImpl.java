package com.capgemini.psd2.account.transaction.service.impl;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.account.transaction.constants.AccountTransactionConstants;
import com.capgemini.psd2.account.transaction.service.AccountTransactionService;
import com.capgemini.psd2.account.transaction.utilities.AccountTransactionDateUtilities;
import com.capgemini.psd2.aisp.adapter.AccountTransactionAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.AccountTransactionsGETResponse;
import com.capgemini.psd2.aisp.domain.Data3;
import com.capgemini.psd2.aisp.domain.Data3Transaction;
import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.MetaData;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.netflix.discovery.shared.Pair;

@Service
public class AccountTransactionServiceImpl implements AccountTransactionService {
	@Value("${app.creditFilter}")
	private String creditFilter;
	@Value("${app.debitFilter}")
	private String debitFilter;
	@Value("${app.minPageSize}")
	private String minPageSize;
	@Value("${app.maxPageSize}")
	private String maxPageSize;
	@Value("${app.defaultPageSize}")
	private String defaultPageSize;

	@Autowired
	@Qualifier("transactionRoutingAdapter")
	private AccountTransactionAdapter accountTransactionAdapter;

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Autowired
	private AispConsentAdapter aispConsentAdapter;

	@Autowired
	AccountTransactionDateUtilities apiDateUtil;

	@Override
	public AccountTransactionsGETResponse retrieveAccountTransaction(String accountId, String paramFromDate,
			String paramToDate, Integer paramPageNumber, Integer paramPageSize, String txnKey) {

		// consent dates
		AispConsent aispConsent = aispConsentAdapter
				.retrieveConsent(reqHeaderAtrributes.getToken().getConsentTokenData().getConsentId());
			
		Pair<String, String> consentDates = populateSetupDates(aispConsent);
		
		String expirationDate = getExpirationDate(aispConsent);

		AccountMapping accountMapping = aispConsentAdapter.retrieveAccountMappingByAccountId(
				reqHeaderAtrributes.getToken().getConsentTokenData().getConsentId(), accountId);
		
		Map<String, String> params = new HashMap<>();
		params.put(AccountTransactionConstants.REQUESTED_FROM_DATETIME, paramFromDate);
		params.put(AccountTransactionConstants.REQUESTED_TO_DATETIME, paramToDate);
		
		//new addition to params
		params.put(AccountTransactionConstants.REQUESTED_FROM_CONSENT_DATETIME, consentDates.first());
		params.put(AccountTransactionConstants.REQUESTED_TO_CONSENT_DATETIME,consentDates.second());
		params.put(AccountTransactionConstants.CONSENT_EXPIRATION_DATETIME, expirationDate);
		
		params.put(AccountTransactionConstants.REQUESTED_PAGE_NUMBER,
				paramPageNumber == null ? AccountTransactionConstants.DEFAULT_PAGE : paramPageNumber.toString());
		params.put(AccountTransactionConstants.REQUESTED_TXN_FILTER, populateFilter());
		params.put(AccountTransactionConstants.REQUESTED_TXN_KEY, txnKey);
		params.put(AccountTransactionConstants.REQUESTED_PAGE_SIZE,
				paramPageSize == null ? defaultPageSize : paramPageSize.toString());
		params.put(AccountTransactionConstants.REQUESTED_MIN_PAGE_SIZE, String.valueOf(minPageSize));
		params.put(AccountTransactionConstants.REQUESTED_MAX_PAGE_SIZE, String.valueOf(maxPageSize));
		params.putAll(reqHeaderAtrributes.getToken().getSeviceParams());
		AccountTransactionsGETResponse transactionResponse = null;
		try {
			transactionResponse = accountTransactionAdapter
				.retrieveAccountTransaction(accountMapping, params);
		}catch(PSD2Exception e ) {
			throw e;
		}

		if (transactionResponse == null)
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_RECORD_FOUND_FOR_REQUESTED_ACCT);

		params.put("linksFromDate", paramFromDate);
		params.put("linksToDate", paramToDate);
		if (paramPageNumber != null)
			params.put("linksPageNumber", String.valueOf(paramPageNumber));
		if (paramPageSize != null)
			params.put("linksPageSize", String.valueOf(paramPageSize));
		transactionResponse = populateLinksAndMeta(transactionResponse, params);
		return transactionResponse;
	}
	
	//CR20
	private Pair<String, String> populateSetupDates(AispConsent aispConsent){
		
		String consentFromDateTimeString = aispConsent.getTransactionFromDateTime();
		String consentToDateTimeString = aispConsent.getTransactionToDateTime();
		
		String requestedFromString = null;
		String requestedToString = null;
		
		if(consentFromDateTimeString!=null) {
		
			LocalDateTime consentFrmDtTm = apiDateUtil.convertZonedStringToLocalDateTime(consentFromDateTimeString);
			requestedFromString = apiDateUtil.convertZonedDateTimeToString(consentFrmDtTm);
		}
		
		if(consentToDateTimeString!=null) {
			LocalDateTime consentToDtTm = apiDateUtil.convertZonedStringToLocalDateTime(consentToDateTimeString);
			requestedToString = apiDateUtil.convertZonedDateTimeToString(consentToDtTm);
		}

		return new Pair<>(requestedFromString, requestedToString);
	}
	
	//CR20
	private String getExpirationDate(AispConsent aispConsent) {
		String expirationDateTimeString = aispConsent.getEndDate();
		
		String expirationDateTimeToString = null;
		if(expirationDateTimeString!=null) {
			LocalDateTime expirationDateTime = apiDateUtil.convertZonedStringToLocalDateTime(expirationDateTimeString);
		
			expirationDateTimeToString = apiDateUtil.convertZonedDateTimeToString(expirationDateTime);
		}
		return expirationDateTimeToString;
	}


	private String populateFilter() {
		String transactionFilter;
		Set<Object> claims = reqHeaderAtrributes.getToken().getClaims().get(AccountTransactionConstants.ACCOUNT_SCOPE_NAME);
		if ((claims.contains(creditFilter)) && (claims.contains(debitFilter)))
			transactionFilter = AccountTransactionConstants.FilterEnum.ALL.toString();
		else if (claims.contains(creditFilter))
			transactionFilter = AccountTransactionConstants.FilterEnum.CREDIT.toString();
		else
			transactionFilter = AccountTransactionConstants.FilterEnum.DEBIT.toString();
		return transactionFilter;
	}

	private AccountTransactionsGETResponse populateLinksAndMeta(AccountTransactionsGETResponse response,
			Map<String, String> params) {

		String paramTxnKey = params.get(AccountTransactionConstants.REQUESTED_TXN_KEY);
		String paramFromDate = params.get("linksFromDate");
		String paramToDate = params.get("linksToDate");
		String paramPageNumber = params.get("linksPageNumber");
		String paramPageSize = params.get("linksPageSize");

		StringBuilder baseUrl = new StringBuilder(reqHeaderAtrributes.getSelfUrl());

		createURL(baseUrl, AccountTransactionConstants.FROM_DATE_PARAM_NAME, paramFromDate);
		createURL(baseUrl, AccountTransactionConstants.TO_DATE_PARAM_NAME, paramToDate);
		createURL(baseUrl, AccountTransactionConstants.PAGE_SIZE_PARAM_NAME, paramPageSize);

		StringBuilder selfUrl = new StringBuilder(baseUrl.toString());
		createURL(selfUrl, AccountTransactionConstants.PAGE_NUMBER_PARAM_NAME, paramPageNumber);
		createURL(selfUrl, AccountTransactionConstants.TXN_KEY_PARAM_NAME, paramTxnKey);

		if (response.getMeta() == null)
			response.setMeta(new MetaData());
		
		if (response.getData() == null){
			Data3 data = new Data3();
			List<Data3Transaction> transaction = new ArrayList<>();
			data.setTransaction(transaction);
			response.setData(data);
		}
		if (response.getLinks() == null) {
			response.setLinks(new Links());
			response.getLinks().setSelf(selfUrl.toString());
			return response;
		}

		String txnKey = null;
		if (response.getLinks().getSelf() != null && response.getLinks().getSelf().contains(":"))
			txnKey = response.getLinks().getSelf().split(":")[1];

		response.getLinks().setSelf(selfUrl.toString());
		response.getLinks().setFirst(null);
		response.getLinks().setNext(appendUrl(response.getLinks().getNext(), baseUrl.toString(), txnKey));
		response.getLinks().setPrev(appendUrl(response.getLinks().getPrev(), baseUrl.toString(), txnKey));
		response.getLinks().setLast(appendUrl(response.getLinks().getLast(), baseUrl.toString(), txnKey));
		return response;
	}

	private String appendUrl(String fsPageNum, String baseUrl, String txnKey) {
		if (fsPageNum == null)
			return null;
		StringBuilder lastUrl = new StringBuilder(baseUrl);
		createURL(lastUrl, AccountTransactionConstants.PAGE_NUMBER_PARAM_NAME, fsPageNum);
		createURL(lastUrl, AccountTransactionConstants.TXN_KEY_PARAM_NAME, txnKey);
		return lastUrl.toString();
	}

	private void createURL(StringBuilder url, String key, String value) {
		if (value != null) {
			if (url.indexOf("?") == -1)
				url.append("?").append(key).append("=").append(value);
			else
				url.append("&").append(key).append("=").append(value);
		}
	}

	@Override
	public AccountTransactionsGETResponse retrieveAccountTransactions(String paramString1, String paramString2,
			String paramString3, Integer paramInteger, Integer pageSize, String txnKey) {
		return null;
	}
}