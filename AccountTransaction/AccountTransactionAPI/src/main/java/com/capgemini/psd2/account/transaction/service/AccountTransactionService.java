package com.capgemini.psd2.account.transaction.service;

import com.capgemini.psd2.aisp.domain.AccountTransactionsGETResponse;

public interface AccountTransactionService {
	public AccountTransactionsGETResponse retrieveAccountTransaction(String paramString1, String paramString2,
			String paramString3, Integer paramInteger, Integer pageSize, String txnKey);

	public AccountTransactionsGETResponse retrieveAccountTransactions(String paramString1, String paramString2,
			String paramString3, Integer paramInteger, Integer pageSize, String txnKey);
}