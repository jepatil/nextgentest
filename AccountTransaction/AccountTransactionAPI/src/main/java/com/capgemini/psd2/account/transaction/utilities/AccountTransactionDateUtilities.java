package com.capgemini.psd2.account.transaction.utilities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;

import org.springframework.stereotype.Component;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;

@Component
public class AccountTransactionDateUtilities {
	public LocalDateTime convertDateStringToLocalDateTime(String inputDateString) {
		if (inputDateString != null) {
			try {
				return LocalDateTime.parse(inputDateString, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
			} catch (DateTimeParseException e) {
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.VALIDATION_ERROR);
			}
		}
		return null;
	}
	
	public LocalDateTime convertZonedStringToLocalDateTime(String inputDateString) {
		if (inputDateString != null) {
			try {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				Date date = sdf.parse(inputDateString);
				return convertDateStringToLocalDateTime(sdf.format(date));
			} catch (DateTimeParseException | ParseException e) {
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.VALIDATION_ERROR);
			}
		}
		return null;
	}

	public String convertZonedDateTimeToString(LocalDateTime inputDateTime) {
		return inputDateTime.plusNanos(1).toString().substring(0, 23);
	}
}
