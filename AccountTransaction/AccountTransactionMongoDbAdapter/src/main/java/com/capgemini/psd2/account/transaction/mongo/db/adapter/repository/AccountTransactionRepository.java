package com.capgemini.psd2.account.transaction.mongo.db.adapter.repository;

import java.time.LocalDateTime;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.capgemini.psd2.aisp.mock.domain.MockAccountTransactionsGETResponseData;

/**
 * The Interface AccountTransactionRepository.
 */
public interface AccountTransactionRepository
		extends PagingAndSortingRepository<MockAccountTransactionsGETResponseData, String> {

	Page<MockAccountTransactionsGETResponseData> findByAccountNumberAndAccountNSCAndBookingDateTimeCopyBetween(String accountNumber, String accountNsc,
			LocalDateTime fromBookingDateTime, LocalDateTime toBookingDateTime, Pageable pageable);

	Page<MockAccountTransactionsGETResponseData> findByAccountNumberAndAccountNSCAndCreditDebitIndicatorAndBookingDateTimeCopyBetween(
			String accountNumber, String accountNsc, String indicator, LocalDateTime fromBookingDateTime,
			LocalDateTime toBookingDateTime, Pageable pageable);
	
}