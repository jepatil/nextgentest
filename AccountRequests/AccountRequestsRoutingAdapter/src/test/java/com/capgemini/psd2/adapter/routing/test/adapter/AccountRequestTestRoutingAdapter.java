/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.adapter.routing.test.adapter;

import com.capgemini.psd2.adapter.routing.test.data.AccountRequestRoutingAdapterTestMockData;
import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.aisp.domain.AccountRequestPOSTResponse;
import com.capgemini.psd2.aisp.domain.Data1;
import com.capgemini.psd2.aisp.domain.Data1.StatusEnum;

public class AccountRequestTestRoutingAdapter implements AccountRequestAdapter {

	@Override
	public AccountRequestPOSTResponse createAccountRequestPOSTResponse(Data1 data) {

		return AccountRequestRoutingAdapterTestMockData.postAccountRequestPOSTResponse(data);
	}

	@Override
	public AccountRequestPOSTResponse getAccountRequestGETResponse(String accountRequestId) {
		return AccountRequestRoutingAdapterTestMockData.getAccountRequestGETResponse(accountRequestId);
	}

	@Override
	public AccountRequestPOSTResponse updateAccountRequestResponse(String accountRequestId, StatusEnum statusEnum) {
		return AccountRequestRoutingAdapterTestMockData.updateAccountRequestResponse(accountRequestId, statusEnum);
	}

	@Override
	public void removeAccountRequest(String accountRequestId, String cid) {

	}

}
