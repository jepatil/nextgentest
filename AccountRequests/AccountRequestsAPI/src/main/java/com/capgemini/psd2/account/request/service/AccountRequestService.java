package com.capgemini.psd2.account.request.service;

import com.capgemini.psd2.aisp.domain.AccountRequestPOSTRequest;
import com.capgemini.psd2.aisp.domain.AccountRequestPOSTResponse;

public interface AccountRequestService {
	

	public AccountRequestPOSTResponse createAccountRequest(AccountRequestPOSTRequest accountRequestPOSTRequest);

	public AccountRequestPOSTResponse retrieveAccountRequest(String accountRequestId);

	public void removeAccountRequest(String accountRequestId);

}
