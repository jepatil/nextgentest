package com.capgemini.psd2.account.request.validation;

import java.util.ArrayList;
import java.util.List;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Component
@Configuration
@EnableConfigurationProperties
@ConfigurationProperties("app")
public class AccountReqeustConfig {

	private List<String> internalAPIInvocationRoles = new ArrayList<>();
	
	public List<String> getInternalAPIInvocationRoles() {
		return internalAPIInvocationRoles;
	}

	public void setInternalAPIInvocationRoles(List<String> internalAPIInvocationRoles) {
		this.internalAPIInvocationRoles = internalAPIInvocationRoles;
	}
}
