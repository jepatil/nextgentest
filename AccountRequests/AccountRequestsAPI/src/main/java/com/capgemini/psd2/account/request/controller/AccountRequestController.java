package com.capgemini.psd2.account.request.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.account.request.service.AccountRequestService;
import com.capgemini.psd2.account.request.validation.AccountRequestValidationUtilities;
import com.capgemini.psd2.aisp.domain.AccountRequestPOSTRequest;
import com.capgemini.psd2.aisp.domain.AccountRequestPOSTResponse;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.utilities.NullCheckUtils;

@RestController
public class AccountRequestController {

	@Autowired
	private AccountRequestService accountRequestService;
	
	@Autowired
	private AccountRequestValidationUtilities accountRequestValidationUtilities;

	@RequestMapping(value = "/account-requests", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_UTF8_VALUE }, produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	@ResponseStatus(HttpStatus.CREATED)
	public AccountRequestPOSTResponse createAccountRequest(
			@RequestBody AccountRequestPOSTRequest accountRequestPOSTRequest) {

		AccountRequestPOSTRequest validatedRequest = accountRequestValidationUtilities
				.validateAccountRequestInputs(accountRequestPOSTRequest);
		return accountRequestService.createAccountRequest(validatedRequest);
	}

	@RequestMapping(value = "/account-requests/{AccountRequestId}", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public AccountRequestPOSTResponse getAccountRequest(@PathVariable("AccountRequestId") String accountRequestId) {

		if (NullCheckUtils.isNullOrEmpty(accountRequestId)) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.VALIDATION_ERROR);
		}
		accountRequestValidationUtilities.validateAccountRequestId(accountRequestId);
		return accountRequestService.retrieveAccountRequest(accountRequestId);
	}

	@RequestMapping(value = {"/account-requests/{AccountRequestId}" , "/internal/account-requests/{AccountRequestId}"}, method = RequestMethod.DELETE, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteAccountRequest(@PathVariable("AccountRequestId") String accountRequestId) {
		if (NullCheckUtils.isNullOrEmpty(accountRequestId)) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.VALIDATION_ERROR);
		}
		accountRequestValidationUtilities.validateAccountRequestId(accountRequestId);
		accountRequestService.removeAccountRequest(accountRequestId);
		
	}

}
