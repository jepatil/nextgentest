package com.capgemini.psd2.account.request.test.validations;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.account.request.test.mock.data.AccountRequestMockData;
import com.capgemini.psd2.account.request.validation.AccountRequestValidationUtilities;
import com.capgemini.psd2.account.request.validation.PermissionsValidationRules;
import com.capgemini.psd2.aisp.domain.AccountRequestPOSTRequest;
import com.capgemini.psd2.aisp.domain.Data;
import com.capgemini.psd2.aisp.domain.Data.PermissionsEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountRequestPermissionValidationTest {

	@Mock
	private PermissionsValidationRules permissionValidationRules;
	
	@InjectMocks
	private AccountRequestValidationUtilities accountRequestValidationUtilities;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void accountRequestConfigtest() {
		PermissionsValidationRules permissionsValidationRules = new PermissionsValidationRules();
		Map<String, List<PermissionsEnum>> mandatoryRules = new HashMap<>();
		List<PermissionsEnum> rules = new ArrayList<>();
		rules.add(PermissionsEnum.READACCOUNTSDETAIL);
		rules.add(PermissionsEnum.READACCOUNTSBASIC);
		
		mandatoryRules.put("READACCOUNTSDETAIL", rules);
		permissionsValidationRules.setMandatoryRules(mandatoryRules);
		assertTrue(permissionsValidationRules.getMandatoryRules().equals(mandatoryRules));
		
		Map<String, List<PermissionsEnum>> orValidationRules = new HashMap<>();
		rules = new ArrayList<>();
		rules.add(PermissionsEnum.READACCOUNTSBASIC);
		
		orValidationRules.put("READACCOUNTSBASIC", rules);
		permissionsValidationRules.setOrConditonRules(orValidationRules);
		assertTrue(permissionsValidationRules.getOrConditonRules().equals(orValidationRules));
	}
	
	@Test
	public void testMandatoryRules() {
		Map<String, List<PermissionsEnum>> mandatoryRules = new HashMap<>();
		List<PermissionsEnum> rules = new ArrayList<>();
		rules.add(PermissionsEnum.READACCOUNTSDETAIL);
		rules.add(PermissionsEnum.READACCOUNTSBASIC);
		mandatoryRules.put("READACCOUNTSDETAIL", rules);
		Mockito.when(permissionValidationRules.getMandatoryRules()).thenReturn(mandatoryRules);
		AccountRequestPOSTRequest request = accountRequestValidationUtilities.validateAccountRequestInputs(AccountRequestMockData
				.getAccountRequestsParamatersMockData());
		assertTrue(request.getData().getPermissions().contains(PermissionsEnum.valueOf("READACCOUNTSBASIC")));
	}
	
	@Test
	public void testOrConditionRules() {
		List<PermissionsEnum> rules = new ArrayList<>();
		Map<String, List<PermissionsEnum>> orValidationRules = new HashMap<>();
		rules.add(PermissionsEnum.READTRANSACTIONSDEBITS);
		rules.add(PermissionsEnum.READTRANSACTIONSCREDITS);
		orValidationRules.put("READTRANSACTIONSBASIC", rules);
		rules = new ArrayList<>();
		rules.add(PermissionsEnum.READTRANSACTIONSBASIC);
		orValidationRules.put("READTRANSACTIONSCREDITS", rules);
		rules = new ArrayList<>();
		rules.add(PermissionsEnum.READTRANSACTIONSBASIC);
		orValidationRules.put("READTRANSACTIONSDEBITS", rules);
		
		Mockito.when(permissionValidationRules.getOrConditonRules()).thenReturn(orValidationRules);
		
		accountRequestValidationUtilities.validateAccountRequestInputs(AccountRequestMockData
				.getAccountRequestsParamatersMockDataOrConditions());
	}
	
	
	@Test(expected = PSD2Exception.class)
	public void testOrConditionRulesException() {
		Map<String, List<PermissionsEnum>> mandatoryRules = new HashMap<>();
		List<PermissionsEnum> rules = new ArrayList<>();
		rules.add(PermissionsEnum.READACCOUNTSDETAIL);
		rules.add(PermissionsEnum.READACCOUNTSBASIC);
		mandatoryRules.put("READACCOUNTSDETAIL", rules);
		Mockito.when(permissionValidationRules.getMandatoryRules()).thenReturn(mandatoryRules);
		
		rules = new ArrayList<>();
		Map<String, List<PermissionsEnum>> orValidationRules = new HashMap<>();
		rules.add(PermissionsEnum.READTRANSACTIONSCREDITS);
		rules.add(PermissionsEnum.READTRANSACTIONSDEBITS);
		orValidationRules.put("READACCOUNTSBASIC", rules);
		
		Mockito.when(permissionValidationRules.getOrConditonRules()).thenReturn(orValidationRules);
		
		accountRequestValidationUtilities.validateAccountRequestInputs(AccountRequestMockData
				.getAccountRequestsParamatersExceptionMockData());
	}
	
	@Test(expected = PSD2Exception.class)
	public void testValidateEmptyRequestExceptions() {
		AccountRequestPOSTRequest request = new AccountRequestPOSTRequest();
		accountRequestValidationUtilities.validateAccountRequestInputs(request);	
	}
	
	@Test(expected = PSD2Exception.class)
	public void testValidateNullRequestExceptions() {
		AccountRequestPOSTRequest request = null;
		accountRequestValidationUtilities.validateAccountRequestInputs(request);	
	}
	
	@Test(expected = PSD2Exception.class)
	public void testValidateDateRequestExceptions() {
		AccountRequestPOSTRequest request = new AccountRequestPOSTRequest();
		
		Data data = new Data();
		Long longDate = 20170502000000L;
		data.setExpirationDateTime(longDate.toString());
			
		request.setData(data);
		request.setRisk(null);
		
		accountRequestValidationUtilities.validateAccountRequestInputs(request);
	}

	@Test(expected = PSD2Exception.class)
	public void testValidateFromToDateRequestExceptions() {
		AccountRequestPOSTRequest request = new AccountRequestPOSTRequest();
		
		Data data = new Data();
		data.setExpirationDateTime("2029-05-08T00:00:00.050");			
		data.setTransactionFromDateTime("2016-05-05T00:00:00.050");
		data.setTransactionToDateTime("2017-05-03T00:00:00.060");
		
		request.setData(data);
		request.setRisk(null);
		
		accountRequestValidationUtilities.validateAccountRequestInputs(request);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testValidateFromDateRequestExceptions() {
		AccountRequestPOSTRequest request = new AccountRequestPOSTRequest();
		
		Data data = new Data();
		data.setExpirationDateTime("2029-05-08T00:00:00.050");			
		data.setTransactionFromDateTime("2016-05-05T00:00:00.050");
				
		request.setData(data);
		request.setRisk(null);
		
		accountRequestValidationUtilities.validateAccountRequestInputs(request);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testValidateToDateRequestExceptions() {
		AccountRequestPOSTRequest request = new AccountRequestPOSTRequest();
		
		Data data = new Data();
		data.setExpirationDateTime("2029-05-08T00:00:00.050");			
		data.setTransactionToDateTime("2019-05-05T00:00:00.050");
				
		request.setData(data);
		request.setRisk(null);
		
		accountRequestValidationUtilities.validateAccountRequestInputs(request);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testValidateNullExpirationDateRequestExceptions() {
		AccountRequestPOSTRequest request = new AccountRequestPOSTRequest();
		
		Data data = new Data();
		data.setTransactionFromDateTime("2016-05-05T00:00:00.050");
		data.setTransactionToDateTime("2017-05-03T00:00:00.060");
		
		request.setData(data);
		request.setRisk(null);
		
		accountRequestValidationUtilities.validateAccountRequestInputs(request);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testValidateExpirationDateRequestExceptions() {
		AccountRequestPOSTRequest request = new AccountRequestPOSTRequest();
		
		Data data = new Data();
		data.setExpirationDateTime("2016-05-08T00:00:00.050");			
		data.setTransactionFromDateTime("2016-05-05T00:00:00.050");
		data.setTransactionToDateTime("2019-05-03T00:00:00.060");
		
		request.setData(data);
		request.setRisk(null);
		
		accountRequestValidationUtilities.validateAccountRequestInputs(request);
	}
		
	@Test(expected = PSD2Exception.class)
	public void testValidateEmptyPermissionsRequestExceptions() {
		AccountRequestPOSTRequest request = new AccountRequestPOSTRequest();
		
		Data data = new Data();
		data.setExpirationDateTime("2019-05-08T00:00:00.050");			
		data.setTransactionFromDateTime("2017-05-02T00:00:00.050");
		data.setTransactionToDateTime("2017-05-03T00:00:00.050");
		
		List<PermissionsEnum> permissions = new ArrayList<>();
		data.setPermissions(permissions);
		
		request.setData(data);
		request.setRisk(null);
		
		accountRequestValidationUtilities.validateAccountRequestInputs(request);

	}
	
	@Test(expected = PSD2Exception.class)
	public void testValidateNullPermissionsRequestExceptions() {
		AccountRequestPOSTRequest request = new AccountRequestPOSTRequest();
		
		Data data = new Data();
		data.setExpirationDateTime("2019-05-08T00:00:00.050");			
		data.setTransactionFromDateTime("2017-05-02T00:00:00.050");
		data.setTransactionToDateTime("2017-05-03T00:00:00.050");
		
		data.setPermissions(null);
		request.setData(data);
		request.setRisk(null);
		accountRequestValidationUtilities.validateAccountRequestInputs(request);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testValidatePermissionIncludesNullRequestExceptions() {
		AccountRequestPOSTRequest request = new AccountRequestPOSTRequest();
		
		Data data = new Data();
		data.setExpirationDateTime("2019-05-08T00:00:00.050");			
		data.setTransactionFromDateTime("2017-05-02T00:00:00.050");
		data.setTransactionToDateTime("2017-05-03T00:00:00.050");
		
		List<PermissionsEnum> permissions = new ArrayList<>();
		permissions.add(null);
		data.setPermissions(permissions);
		
		request.setData(data);
		request.setRisk(null);
		accountRequestValidationUtilities.validateAccountRequestInputs(request);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testValidateAccountRequestIdException() {
		String accountRequestId = "56981c81-a529-45be-9ea8-88addc01858$";
		ReflectionTestUtils.setField(accountRequestValidationUtilities, "validAccountRequestIdChars", "[^A-Za-z0-9-]+");
		accountRequestValidationUtilities.validateAccountRequestId(accountRequestId);
	}
	
	@Test
	public void testValidateAccountRequestId() {
		String accountRequestId = "56981c81-a529-45be-9ea8-88addc018585";
		ReflectionTestUtils.setField(accountRequestValidationUtilities, "validAccountRequestIdChars", "[^A-Za-z0-9-]+");
		accountRequestValidationUtilities.validateAccountRequestId(accountRequestId);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testValidateAccountRequestIdInvalidLengthException() {
		String accountRequestId = "56981c81-a529-45be-9ea8-88addc0185856981c81-a529-45be-9ea8-88addc0185856981c81-a529-45be-9ea8-88addc0185856981c81-a529-45be-9ea8-88addc01858";
		accountRequestValidationUtilities.validateAccountRequestId(accountRequestId);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testValidateAccountRequestIdInvalidLengthZeroException() {
		String accountRequestId = "";
		accountRequestValidationUtilities.validateAccountRequestId(accountRequestId);
	}
	
	@After
	public void tearDown() throws Exception {
		permissionValidationRules = null;
	}
	
	
}