package com.capgemini.psd2.account.request.test.service;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.account.request.service.impl.AccountRequestServiceImpl;
import com.capgemini.psd2.account.request.test.mock.data.AccountRequestMockData;
import com.capgemini.psd2.account.request.validation.AccountReqeustConfig;
import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.aisp.domain.AccountRequestPOSTResponse;
import com.capgemini.psd2.aisp.domain.Data1;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.token.Token;
import com.capgemini.psd2.token.TppInformationTokenData;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountRequestServiceImplTest {

	@Mock
	private AccountRequestAdapter adapter;
	
	@Mock
	private RequestHeaderAttributes requestHeaderAttributes;
	
	@Mock
	private AccountReqeustConfig bypassTPPRoles;
	
	
	@InjectMocks
	private Data1 data;
	
	@InjectMocks
	AccountRequestPOSTResponse accountRequestPOSTResponse;

	@InjectMocks
	private AccountRequestServiceImpl service;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void accountRequestPOSTtest() {
		Mockito.when(adapter.createAccountRequestPOSTResponse(any(Data1.class))).thenReturn(AccountRequestMockData.getAccountRequestsMockResponseData());
		Mockito.when(requestHeaderAttributes.getSelfUrl()).thenReturn("http://localhost:8989/account-requests");
		Token token = new Token();
		TppInformationTokenData tppInformation = new TppInformationTokenData();
		tppInformation.setTppLegalEntityName("Moneywise.com");
		token.setTppInformation(tppInformation);
		Mockito.when(requestHeaderAttributes.getToken()).thenReturn(token);
		service.createAccountRequest(AccountRequestMockData.getAccountRequestsParamatersMockData());
	}
	
	@Test
	public void accountRequestPOSTSuccesstest() {
		Mockito.when(adapter.createAccountRequestPOSTResponse(any(Data1.class))).thenReturn(AccountRequestMockData.getAccountRequestsBranchCoverageMockResponseData());
		Mockito.when(requestHeaderAttributes.getSelfUrl()).thenReturn("http://localhost:8989/account-requests");
		Token token = new Token();
		TppInformationTokenData tppInformation = new TppInformationTokenData();
		tppInformation.setTppLegalEntityName("Moneywise.com");
		token.setTppInformation(tppInformation);
		Mockito.when(requestHeaderAttributes.getToken()).thenReturn(token);
		service.createAccountRequest(AccountRequestMockData.getAccountRequestsParamatersMockData());
	}
	
	@Test
	public void accountRequestGETtest() {
		String accountRequestId = "af5b90c1-64b5-4a52-ba55-5eed68b2a269";
		List<String> bypassRoles = new ArrayList<>();
		bypassRoles.add("PISP");
		bypassRoles.add("INTERNAL_ALL");
		Token token = new Token();
		TppInformationTokenData tppInformation = new TppInformationTokenData();
		Set<String> tppRoles = new HashSet<>();
		tppRoles.add("AISP");
		tppInformation.setTppRoles(tppRoles);
		token.setTppInformation(tppInformation);
		data.setTppCID("6443e15975554bce8099e35b88b40465");
		accountRequestPOSTResponse.setData(data);
		Mockito.when(adapter.getAccountRequestGETResponse(accountRequestId)).thenReturn(accountRequestPOSTResponse);
		Mockito.when(requestHeaderAttributes.getTppCID()).thenReturn("6443e15975554bce8099e35b88b40465");
		Mockito.when(requestHeaderAttributes.getToken()).thenReturn(token);
		//Mockito.when(adapter.getAccountRequestGETResponse(accountRequestId, null)).thenReturn(AccountRequestMockData.getAccountRequestsMockResponseData());
		Mockito.when(bypassTPPRoles.getInternalAPIInvocationRoles()).thenReturn(bypassRoles);
		service.retrieveAccountRequest(accountRequestId);
	}
	
	@Test
	public void accountRequestGETTPPtest() {
		String accountRequestId = "af5b90c1-64b5-4a52-ba55-5eed68b2a269";
		List<String> bypassRoles = new ArrayList<>();
		bypassRoles.add("PISP");
		bypassRoles.add("INTERNAL_ALL");
		Token token = new Token();
		TppInformationTokenData tppInformation = new TppInformationTokenData();
		Set<String> tppRoles = new HashSet<>();
		tppRoles.add("INTERNAL_ALL");
		tppInformation.setTppRoles(tppRoles);
		token.setTppInformation(tppInformation);
		data.setTppCID("6443e15975554bce8099e35b88b40465");
		accountRequestPOSTResponse.setData(data);
		Mockito.when(adapter.getAccountRequestGETResponse(accountRequestId)).thenReturn(accountRequestPOSTResponse);
		Mockito.when(requestHeaderAttributes.getTppCID()).thenReturn("6443e15975554bce8099e35b88b40465");
		Mockito.when(requestHeaderAttributes.getToken()).thenReturn(token);
		//Mockito.when(adapter.getAccountRequestGETResponse(accountRequestId, null)).thenReturn(AccountRequestMockData.getAccountRequestsMockResponseData());
		Mockito.when(bypassTPPRoles.getInternalAPIInvocationRoles()).thenReturn(bypassRoles);
		service.retrieveAccountRequest(accountRequestId);
	}
	
	@Test
	public void accountRequestGETSuccesstest() {
		String accountRequestId = "af5b90c1-64b5-4a52-ba55-5eed68b2a269";
		List<String> bypassRoles = new ArrayList<>();
		bypassRoles.add("PISP");
		bypassRoles.add("INTERNAL_ALL");
		Token token = new Token();
		TppInformationTokenData tppInformation = new TppInformationTokenData();
		Set<String> tppRoles = new HashSet<>();
		tppRoles.add("AISP");
		tppInformation.setTppRoles(tppRoles);
		token.setTppInformation(tppInformation);
		data.setTppCID("6443e15975554bce8099e35b88b40465");
		accountRequestPOSTResponse.setData(data);
		Mockito.when(adapter.getAccountRequestGETResponse(accountRequestId)).thenReturn(accountRequestPOSTResponse);
		Mockito.when(requestHeaderAttributes.getTppCID()).thenReturn("6443e15975554bce8099e35b88b40465");
		Mockito.when(requestHeaderAttributes.getToken()).thenReturn(token);
		//Mockito.when(adapter.getAccountRequestGETResponse(accountRequestId, null)).thenReturn(AccountRequestMockData.getAccountRequestsBranchCoverageMockResponseData());
		Mockito.when(bypassTPPRoles.getInternalAPIInvocationRoles()).thenReturn(bypassRoles);
		service.retrieveAccountRequest(accountRequestId);
	}
	 
	/*@Test(expected = PSD2Exception.class)
	public void accountRequestGETExceptiontest() {		
		String accountRequestId = "af5b90c1-64b5-4a52-ba55-5eed68b2a269";
		List<String> bypassRoles = new ArrayList<>();
		bypassRoles.add("PISP");
		bypassRoles.add("INTERNAL_ALL");
		Token token = new Token();
		TppInformationTokenData tppInformation = new TppInformationTokenData();
		Set<String> tppRoles = new HashSet<>();
		tppRoles.add("AISP");
		tppInformation.setTppRoles(tppRoles);
		token.setTppInformation(tppInformation);
		data.setTppCID("6443e15975554bce8099e35b88b40465");
		accountRequestPOSTResponse.setData(data);
		Mockito.when(adapter.getAccountRequestGETResponse(accountRequestId)).thenReturn(accountRequestPOSTResponse);
		Mockito.when(requestHeaderAttributes.getToken()).thenReturn(token);
		Mockito.when(requestHeaderAttributes.getTppCID()).thenReturn("6443e15975554bce8099e35b88b40465a");
		//Mockito.when(adapter.getAccountRequestGETResponse(accountRequestId, null)).thenReturn(AccountRequestMockData.getAccountRequestsMockResponseData());
		service.retrieveAccountRequest(accountRequestId);
	}*/
	
	@Test
	public void accountRequestDELETEtest() {
		String accountRequestId = "af5b90c1-64b5-4a52-ba55-5eed68b2a269";
		List<String> bypassRoles = new ArrayList<>();
		bypassRoles.add("PISP");
		bypassRoles.add("INTERNAL_ALL");
		Token token = new Token();
		TppInformationTokenData tppInformation = new TppInformationTokenData();
		Set<String> tppRoles = new HashSet<>();
		tppRoles.add("AISP");
		tppInformation.setTppRoles(tppRoles);
		token.setTppInformation(tppInformation);
		data.setTppCID("6443e15975554bce8099e35b88b40465");
		accountRequestPOSTResponse.setData(data);
		Mockito.when(adapter.getAccountRequestGETResponse(accountRequestId)).thenReturn(accountRequestPOSTResponse);
		Mockito.when(requestHeaderAttributes.getTppCID()).thenReturn("6443e15975554bce8099e35b88b40465");
		Mockito.when(requestHeaderAttributes.getToken()).thenReturn(token);
		Mockito.doNothing().when(adapter).removeAccountRequest(accountRequestId, null);
		Mockito.when(bypassTPPRoles.getInternalAPIInvocationRoles()).thenReturn(bypassRoles);
		service.removeAccountRequest(accountRequestId);
	}
	
	@After
	public void tearDown() throws Exception {
		service = null;
		adapter = null;
		requestHeaderAttributes = null;
	}
}