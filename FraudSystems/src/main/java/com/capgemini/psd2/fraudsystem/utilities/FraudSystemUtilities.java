/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.fraudsystem.utilities;

import java.util.Enumeration;
import javax.servlet.http.HttpServletRequest;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;

public final class FraudSystemUtilities {

	private static final Logger LOGGER = LoggerFactory.getLogger(FraudSystemUtilities.class);

	private FraudSystemUtilities(){
		
	}
	
	public static String populateFraudSystemHeaders(HttpServletRequest request) {
		Enumeration<String> fsHeader = (Enumeration<String>) request.getHeaderNames();
		JSONObject fsHeaderJSON = new JSONObject();
		String header = null;
		while (fsHeader.hasMoreElements()) {
			header = fsHeader.nextElement();
			try {
				fsHeaderJSON.put(header, request.getHeader(header));

			} catch (JSONException e) {
				PSD2Exception pe = PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.TECHNICAL_ERROR);
				LOGGER.error("{\"Exception\":\"{}\",\"{}\",\"ErrorDetails\":{}}",
						"com.capgemini.psd2.fraudsystem.utilities.populateFraudSystemHeaders()", pe.getErrorInfo());
				if (LOGGER.isDebugEnabled()) {
					LOGGER.error("{\"Exception\":\"{}\",\"{}\",\"ErrorDetails\":\"{}\"}",
							"com.capgemini.psd2.fraudsystem.utilities.populateFraudSystemHeaders()", pe.getStackTrace(),
							pe);
				}
			}
		}
		return fsHeaderJSON.toString();
	}

}
