package com.capgemini.psd2.foundationservice.account.direct.debits.boi.adapter.test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.utility.AdapterUtility;
import com.capgemini.psd2.aisp.domain.AccountGETResponse1;
import com.capgemini.psd2.aisp.domain.DirectDebit;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.direct.debits.boi.adapter.client.AccountDirectDebitsFoundationServiceClientImpl;
import com.capgemini.psd2.foundationservice.account.direct.debits.boi.adapter.delegate.AccountDirectDebitsFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.direct.debits.boi.adapter.transformer.AccountDirectDebitsFoundationServiceTransformer;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;

public class AccountDirectDebitsFoundationServiceDelegateTest {

	@InjectMocks
	AccountDirectDebitsFoundationServiceDelegate accountDirectDebitsFoundationServiceDelegate;
	
	@Mock
	AccountDirectDebitsFoundationServiceClientImpl accountDirectDebitsFoundationServiceClientImpl;
	
	@Mock
	AccountDirectDebitsFoundationServiceTransformer accountDirectDebitsFoundationServiceTransformer;
	
	/** The rest client. */
	@Mock
	private RestClientSyncImpl restClient;

	/** The adapterUtility. */
	@Mock
	private AdapterUtility adapterUtility;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads(){
		
	}

		@Test(expected = AdapterException.class)
		public void testGetFoundationServiceWithAccountNumberAsNull() {
			String accountNumber = null;
			String accountNSC = "nsc";
			String baseURL =  "http://localhost:9089/fs-entity-service/services/account";
			
			accountDirectDebitsFoundationServiceDelegate.getFoundationServiceURL(accountNSC, accountNumber, baseURL);

			fail("Invalid accountNumber");
		}
		
		@Test(expected = AdapterException.class)
		public void testGetFoundationServiceWithNscAsNull() {
			String accountNumber = "number";
			String accountNSC = null;
			String baseURL =  "http://localhost:9089/fs-entity-service/services/account";
			
			accountDirectDebitsFoundationServiceDelegate.getFoundationServiceURL(accountNSC, accountNumber, baseURL);

			fail("Invalid nsc");
		}
	
		@Test
		public void testGetFoundationServiceURL(){
			String finalURL = "http://localhost:9089/fs-entity-service/services/account/12345678/654321/directdebits";
			String response = accountDirectDebitsFoundationServiceDelegate.getFoundationServiceURL("12345678","654321","http://localhost:9089/fs-entity-service/services/account");
			assertThat(response).isEqualTo(finalURL);
		}
		
		@Test(expected = AdapterException.class)
		public void testGetFoundationServiceNscandAccountNumberNull(){
			accountDirectDebitsFoundationServiceDelegate.getFoundationServiceURL(null,null, "http://localhost:9089/fs-entity-service/services/account");
		}
		
		/**
		 * Test create request headers actual.
		 */
		@Test
		public void testCreateRequestHeadersActual() {

			ReflectionTestUtils.setField(accountDirectDebitsFoundationServiceDelegate, "userInReqHeader", "X-BOI-USER");
			ReflectionTestUtils.setField(accountDirectDebitsFoundationServiceDelegate, "channelInReqHeader", "X-BOI-CHANNEL");
			ReflectionTestUtils.setField(accountDirectDebitsFoundationServiceDelegate, "platformInReqHeader", "X-BOI-PLATFORM");
			ReflectionTestUtils.setField(accountDirectDebitsFoundationServiceDelegate, "correlationReqHeader", "X-CORRELATION-ID");
			ReflectionTestUtils.setField(accountDirectDebitsFoundationServiceDelegate, "platform", "PSD2API");
			AccountMapping accountMapping = new AccountMapping();
			accountMapping.setPsuId("12345678");
			accountMapping.setCorrelationId("123");
			Map<String, String> params = new HashMap<>();
			params.put(PSD2Constants.CHANNEL_NAME, "Channel");
			params.put(PSD2Constants.PLATFORM_IN_REQ_HEADER, "PSD2API");
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.add("X-BOI-USER", "12345678");
			httpHeaders.add("X-BOI-CHANNEL", "Channel");
			httpHeaders.add("X-BOI-PLATFORM", "PSD2API");
			httpHeaders.add("X-CORRELATION-ID", "123");
			HttpHeaders response = accountDirectDebitsFoundationServiceDelegate.createRequestHeaders(new RequestInfo(), accountMapping, params);
			assertThat(response).isEqualTo(httpHeaders);
			params.put(PSD2Constants.PLATFORM_IN_REQ_HEADER, null);
			response = accountDirectDebitsFoundationServiceDelegate.createRequestHeaders(new RequestInfo(), accountMapping, params);
			assertThat(response).isEqualTo(httpHeaders);
		}

		/**
		 * Test transform response from FD to API.
		 */
		@Test
		public void testTransformResponseFromFDToAPI() {
			DirectDebit directDebits = new DirectDebit();
			
			AccountMapping accountMapping = new AccountMapping();
			List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
			AccountDetails accDet = new AccountDetails();
			accDet.setAccountId("12345");
			accDet.setAccountNSC("nsc1234");
			accDet.setAccountNumber("acct1234");
			accDetList.add(accDet);
			accountMapping.setAccountDetails(accDetList);
			accountMapping.setTppCID("test");
			accountMapping.setPsuId("test");
			Mockito.when(accountDirectDebitsFoundationServiceDelegate.transform(anyObject(),anyObject())).thenReturn(new AccountGETResponse1());
			AccountGETResponse1 res = accountDirectDebitsFoundationServiceDelegate.transform(directDebits, new HashMap<String, String>());
			assertNotNull(res);

		}
}
