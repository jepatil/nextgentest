
package com.capgemini.psd2.foundationservice.account.direct.debits.boi.adapter.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.aisp.domain.DirectDebit;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;

@Service
public class AccountDirectDebitsFoundationServiceClientImpl implements AccountDirectDebitsFoundationServiceClient {

	/** The rest client. */
	@Autowired
	@Qualifier("restClientFoundation")
	private RestClientSync restClient;

	@Override
	public DirectDebit restTransportForDirectDebitsFS(RequestInfo requestInfo, Class<DirectDebit> response,
			HttpHeaders httpHeaders) {
		return new DirectDebit();
	}
}
