
package com.capgemini.psd2.foundationservice.account.direct.debits.boi.adapter;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.adapter.AccountDirectDebitsAdapter;
import com.capgemini.psd2.aisp.domain.AccountGETResponse1;
import com.capgemini.psd2.aisp.domain.DirectDebit;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.direct.debits.boi.adapter.client.AccountDirectDebitsFoundationServiceClient;
import com.capgemini.psd2.foundationservice.account.direct.debits.boi.adapter.delegate.AccountDirectDebitsFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.CustomerAccountsFilterFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion.AdapterFilterUtility;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class AccountDirectDebitsFoundationServiceAdapter implements AccountDirectDebitsAdapter {

	@Value("${foundationService.accountDirectDebitsBaseURL}")
	private String accountDirectDebitsBaseURL;

	@Value("${foundationService.consentFlowType}")
	private String consentFlowType;

	@Autowired
	private AccountDirectDebitsFoundationServiceDelegate accountDirectDebitsFoundationServiceDelegate;

	@Autowired
	private AccountDirectDebitsFoundationServiceClient accountDirectDebitsFoundationServiceClient;

	@Autowired
	private AdapterFilterUtility adapterFilterUtility;

	@Autowired
	private CustomerAccountsFilterFoundationServiceAdapter commonFilterUtility;

	@Override
	public AccountGETResponse1 retrieveAccountDirectDebits(AccountMapping accountMapping, Map<String, String> params) {
		if (NullCheckUtils.isNullOrEmpty(accountMapping) || NullCheckUtils.isNullOrEmpty(accountMapping.getPsuId())) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		if (NullCheckUtils.isNullOrEmpty(params)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, consentFlowType);
		params.put(PSD2Constants.CHANNEL_ID, params.get("channelId"));
		params.put(PSD2Constants.USER_ID, accountMapping.getPsuId());
		params.put(PSD2Constants.CORRELATION_ID, accountMapping.getCorrelationId());

		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts filteredAccounts = commonFilterUtility
				.retrieveCustomerAccountList(accountMapping.getPsuId(), params);
		if (filteredAccounts == null || filteredAccounts.getAccount() == null
				|| filteredAccounts.getAccount().isEmpty()) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.ACCOUNT_NOT_FOUND);
		}
		adapterFilterUtility.prevalidateAccounts(filteredAccounts, accountMapping);

		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders httpHeaders = accountDirectDebitsFoundationServiceDelegate.createRequestHeaders(requestInfo,
				accountMapping, params);
		AccountDetails accountDetails = null;
		if (accountMapping.getAccountDetails() != null && !accountMapping.getAccountDetails().isEmpty()) {
			accountDetails = accountMapping.getAccountDetails().get(0);
		} else {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		String finalURL = accountDirectDebitsFoundationServiceDelegate.getFoundationServiceURL(
				accountDetails.getAccountNSC(), accountDetails.getAccountNumber(), accountDirectDebitsBaseURL);
		requestInfo.setUrl(finalURL);
		DirectDebit directDebit = accountDirectDebitsFoundationServiceClient.restTransportForDirectDebitsFS(requestInfo,
				DirectDebit.class, httpHeaders);
		return accountDirectDebitsFoundationServiceDelegate.transform(directDebit, params);
	}

}
