package com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.impl;

import java.util.Map;

import org.springframework.dao.DataAccessResourceFailureException;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.PaymentSetupValidationResponse;
import com.capgemini.psd2.pisp.adapter.PaymentSetupValidationAdapter;
import com.capgemini.psd2.pisp.status.PaymentStatusEnum;

public class PaymentSetupValidationMongoDbAdapterImpl implements PaymentSetupValidationAdapter{
	@Override
	public PaymentSetupValidationResponse validatePreStagePaymentSetup(CustomPaymentSetupPOSTRequest paymentSetupRequest,
			Map<String, String> paramsMap) {
		
		PaymentSetupValidationResponse validationResponse = new PaymentSetupValidationResponse();
		try{
			/*
			 * Following conditions are for demo and these shows the conditions for rejection of payment setup.
			 */
			if("INR".equalsIgnoreCase(paymentSetupRequest.getData().getInitiation().getInstructedAmount().getCurrency())){
				/*In case 400 , 401, 403, 404 or 500 other error code from FS*/
			
				validationResponse.setPaymentSetupValidationStatus(PaymentStatusEnum.REJECTED.getStatusCode());
				return validationResponse;
			}
			
			if("FS_PMPSV_003".equalsIgnoreCase(paymentSetupRequest.getData().getInitiation().getEndToEndIdentification())) {
				/*In case 400 , 401, 403, 404 or 500 other error code from FS*/
				
				validationResponse.setPaymentSetupValidationStatus(PaymentStatusEnum.REJECTED.getStatusCode());
				return validationResponse;
			}
			
			if("1.11".equalsIgnoreCase(paymentSetupRequest.getData().getInitiation().getInstructedAmount().getAmount())){
				/*In case 400 , 401, 403, 404 or 500 other error code from FS*/
			
				validationResponse.setPaymentSetupValidationStatus(PaymentStatusEnum.REJECTED.getStatusCode());
				return validationResponse;
			}
			
			if("CAC_NAME_DEMO_FAIL".equalsIgnoreCase(paymentSetupRequest.getData().getInitiation().getCreditorAccount().getName())){
				/*In case 400 , 401, 403, 404 or 500 other error code from FS*/
			
				validationResponse.setPaymentSetupValidationStatus(PaymentStatusEnum.REJECTED.getStatusCode());
				return validationResponse;
			}
			
			//In case http status 200 and return value from bank is 'SUCCESS'
			validationResponse.setPaymentSetupValidationStatus(PaymentStatusEnum.PENDING.getStatusCode());
			return validationResponse;
				
		}catch(DataAccessResourceFailureException exception){
			//In case 500 error code
			throw PSD2Exception.populatePSD2Exception(exception.getMessage(), ErrorCodeEnum.PISP_TECHNICAL_ERROR_FS_PRE_STAGE_VALIDATION);
		}		
		
	}


}
