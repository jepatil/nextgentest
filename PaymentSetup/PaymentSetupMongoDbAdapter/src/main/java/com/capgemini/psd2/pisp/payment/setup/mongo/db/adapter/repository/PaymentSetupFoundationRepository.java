package com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.capgemini.psd2.pisp.domain.PaymentSetupFoundationResource;

public interface PaymentSetupFoundationRepository extends MongoRepository<PaymentSetupFoundationResource, String>{

	  public PaymentSetupFoundationResource findOneByDataPaymentId(String paymentId );	
}
