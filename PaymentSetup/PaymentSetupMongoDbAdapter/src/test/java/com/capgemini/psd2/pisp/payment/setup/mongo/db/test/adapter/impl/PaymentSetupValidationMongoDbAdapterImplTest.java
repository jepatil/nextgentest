package com.capgemini.psd2.pisp.payment.setup.mongo.db.test.adapter.impl;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.pisp.domain.CreditorAccount;
import com.capgemini.psd2.pisp.domain.CreditorAgent;
import com.capgemini.psd2.pisp.domain.CreditorAgent.SchemeNameEnum;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.DebtorAccount;
import com.capgemini.psd2.pisp.domain.DebtorAgent;
import com.capgemini.psd2.pisp.domain.PaymentSetup;
import com.capgemini.psd2.pisp.domain.PaymentSetupInitiation;
import com.capgemini.psd2.pisp.domain.PaymentSetupInitiationInstructedAmount;
import com.capgemini.psd2.pisp.domain.PaymentSetupValidationResponse;
import com.capgemini.psd2.pisp.domain.RemittanceInformation;
import com.capgemini.psd2.pisp.domain.Risk;
import com.capgemini.psd2.pisp.domain.Risk.PaymentContextCodeEnum;
import com.capgemini.psd2.pisp.domain.RiskDeliveryAddress;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.impl.PaymentSetupValidationMongoDbAdapterImpl;


@RunWith(SpringJUnit4ClassRunner.class)
public class PaymentSetupValidationMongoDbAdapterImplTest {
	
	@InjectMocks
	private PaymentSetupValidationMongoDbAdapterImpl adapter;
	
	/**
	 * Sets the up.
	 */
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}

	@Test
	public void paymentSetupValidateResponseTest(){
		
		CustomPaymentSetupPOSTRequest request = new CustomPaymentSetupPOSTRequest();
		PaymentSetup data = new PaymentSetup();
		request.setData(data);
		Risk risk = new Risk();
		request.setRisk(risk);
		
		PaymentSetupInitiation initiation = new PaymentSetupInitiation();
		data.setInitiation(initiation);
		
		initiation.setInstructionIdentification("ABDDC");
		initiation.setEndToEndIdentification("FRESCO.21302.GFX.28");
		
		PaymentSetupInitiationInstructedAmount instructedAmount = new PaymentSetupInitiationInstructedAmount();
		instructedAmount.setAmount("1.11");
		instructedAmount.setCurrency("INR");
		initiation.setInstructedAmount(instructedAmount);
		
		CreditorAgent creditorAgent = new CreditorAgent();
		creditorAgent.setSchemeName(SchemeNameEnum.BICFI);
		creditorAgent.setIdentification("SC080801");
		initiation.setCreditorAgent(creditorAgent);
		
		CreditorAccount creditorAccount = new CreditorAccount();
		creditorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.IBAN);
		creditorAccount.setIdentification("NWBK60161331926819");
		creditorAccount.setName("CAC_NAME_DEMO_FAIL");
		creditorAccount.setSecondaryIdentification("0002");
		initiation.setCreditorAccount(creditorAccount);
		
		DebtorAgent debtorAgent = new DebtorAgent();
		debtorAgent.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAgent.SchemeNameEnum.BICFI);
		debtorAgent.setIdentification("SC112800");
		initiation.setDebtorAgent(debtorAgent);
		
		DebtorAccount debtorAccount = new DebtorAccount();
		debtorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.IBAN);
		debtorAccount.setIdentification("GB29NWBK60161331926819");
		debtorAccount.setName("Andrea Smith");
		debtorAccount.setSecondaryIdentification("0002");
		initiation.setDebtorAccount(debtorAccount);
		
		RemittanceInformation remittanceInformation = new RemittanceInformation();
		remittanceInformation.setReference("FRESCO-101");
		remittanceInformation.setUnstructured("Internal ops code 5120101");
		initiation.setRemittanceInformation(remittanceInformation);
		
		risk.setMerchantCategoryCode("5967");
		risk.setMerchantCustomerIdentification("053598653254");
		risk.setPaymentContextCode(PaymentContextCodeEnum.ECOMMERCEGOODS);
		
		RiskDeliveryAddress deliveryAddress = new RiskDeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		
		List<String> addressLine = new ArrayList<>();
		addressLine.add("Flat 7");
		addressLine.add("Acacia Lodge");		
		deliveryAddress.setAddressLine(addressLine);
		
		deliveryAddress.setBuildingNumber("27");
		deliveryAddress.setCountry("UK");
		
		List<String> countrySubDivision = new ArrayList<>();
		countrySubDivision.add("ABCDABCDABCDABCDAB");
		countrySubDivision.add("DEFG");
		deliveryAddress.setCountrySubDivision(countrySubDivision);
		
		deliveryAddress.setPostCode("GU31 2ZZ");
		deliveryAddress.setStreetName("AcaciaAvenue");
		deliveryAddress.setTownName("Sparsholt");
		
		CustomPaymentSetupPOSTResponse paymentSetupPOSTResponse = new CustomPaymentSetupPOSTResponse();
		HashMap<String, String> params = new HashMap<>();
		PaymentSetupValidationResponse validationResponse = new PaymentSetupValidationResponse();
		 adapter.validatePreStagePaymentSetup(request, params);
		//assertNotNull(validationResponse);
		instructedAmount.setCurrency("EUR");
		adapter.validatePreStagePaymentSetup(request, params);
		//assertNotNull(validationResponse);
		instructedAmount.setAmount("2436546");
		adapter.validatePreStagePaymentSetup(request, params);
		creditorAccount.setName("FAIL");
		adapter.validatePreStagePaymentSetup(request, params);
	}
	
		
}
