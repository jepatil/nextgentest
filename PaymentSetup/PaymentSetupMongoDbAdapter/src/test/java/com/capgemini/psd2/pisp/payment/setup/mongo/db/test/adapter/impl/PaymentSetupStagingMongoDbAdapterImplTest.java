package com.capgemini.psd2.pisp.payment.setup.mongo.db.test.adapter.impl;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.CreditorAccount;
import com.capgemini.psd2.pisp.domain.CreditorAgent;
import com.capgemini.psd2.pisp.domain.CreditorAgent.SchemeNameEnum;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.DebtorAccount;
import com.capgemini.psd2.pisp.domain.DebtorAgent;
import com.capgemini.psd2.pisp.domain.PaymentSetup;
import com.capgemini.psd2.pisp.domain.PaymentSetupFoundationResource;
import com.capgemini.psd2.pisp.domain.PaymentSetupInitiation;
import com.capgemini.psd2.pisp.domain.PaymentSetupInitiationInstructedAmount;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseLinks;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseMeta;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse.StatusEnum;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponseInitiation;
import com.capgemini.psd2.pisp.domain.PaymentSetupStagingResponse;
import com.capgemini.psd2.pisp.domain.RemittanceInformation;
import com.capgemini.psd2.pisp.domain.Risk;
import com.capgemini.psd2.pisp.domain.Risk.PaymentContextCodeEnum;
import com.capgemini.psd2.pisp.domain.RiskDeliveryAddress;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.impl.PaymentSetupStagingMongoDbAdapterImpl;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.repository.PaymentSetupFoundationRepository;
import com.capgemini.psd2.pisp.sequence.adapter.SequenceGenerator;
import com.capgemini.psd2.pisp.validation.PispUtilities;

@RunWith(SpringJUnit4ClassRunner.class)
public class PaymentSetupStagingMongoDbAdapterImplTest {

	@InjectMocks
	PaymentSetupStagingMongoDbAdapterImpl paymentSetupStagingMongoDbAdapterImpl;
	@Mock
	private PaymentSetupFoundationRepository paymentSetupBankRepository;

	@Mock
	private SequenceGenerator sequenceGenerator;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void createStagingPaymentSetupTest(){
		
		CustomPaymentSetupPOSTRequest request = new CustomPaymentSetupPOSTRequest();
		PaymentSetup data = new PaymentSetup();
		request.setData(data);
		Risk risk = new Risk();
		request.setRisk(risk);
		
		PaymentSetupInitiation initiation = new PaymentSetupInitiation();
		data.setInitiation(initiation);
		
		initiation.setInstructionIdentification("ABDDC");
		initiation.setEndToEndIdentification("FRESCO.21302.GFX.28");
		
		PaymentSetupInitiationInstructedAmount instructedAmount = new PaymentSetupInitiationInstructedAmount();
		instructedAmount.setAmount("777777777.00");
		instructedAmount.setCurrency("EUR");
		initiation.setInstructedAmount(instructedAmount);
		
		CreditorAgent creditorAgent = new CreditorAgent();
		creditorAgent.setSchemeName(SchemeNameEnum.BICFI);
		creditorAgent.setIdentification("SC080801");
		initiation.setCreditorAgent(creditorAgent);
		
		CreditorAccount creditorAccount = new CreditorAccount();
		creditorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.IBAN);
		creditorAccount.setIdentification("NWBK60161331926819");
		creditorAccount.setName("Test user");
		creditorAccount.setSecondaryIdentification("0002");
		initiation.setCreditorAccount(creditorAccount);
		
		DebtorAgent debtorAgent = new DebtorAgent();
		debtorAgent.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAgent.SchemeNameEnum.BICFI);
		debtorAgent.setIdentification("SC112800");
		initiation.setDebtorAgent(debtorAgent);
		
		DebtorAccount debtorAccount = new DebtorAccount();
		debtorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.IBAN);
		debtorAccount.setIdentification("GB29NWBK60161331926819");
		debtorAccount.setName("Andrea Smith");
		debtorAccount.setSecondaryIdentification("0002");
		initiation.setDebtorAccount(debtorAccount);
		
		RemittanceInformation remittanceInformation = new RemittanceInformation();
		remittanceInformation.setReference("FRESCO-101");
		remittanceInformation.setUnstructured("Internal ops code 5120101");
		initiation.setRemittanceInformation(remittanceInformation);
		
		risk.setMerchantCategoryCode("5967");
		risk.setMerchantCustomerIdentification("053598653254");
		risk.setPaymentContextCode(PaymentContextCodeEnum.ECOMMERCEGOODS);
		
		RiskDeliveryAddress deliveryAddress = new RiskDeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		
		List<String> addressLine = new ArrayList<>();
		addressLine.add("Flat 7");
		addressLine.add("Acacia Lodge");		
		deliveryAddress.setAddressLine(addressLine);
		
		deliveryAddress.setBuildingNumber("27");
		deliveryAddress.setCountry("UK");
		
		List<String> countrySubDivision = new ArrayList<>();
		countrySubDivision.add("ABCDABCDABCDABCDAB");
		countrySubDivision.add("DEFG");
		deliveryAddress.setCountrySubDivision(countrySubDivision);
		
		deliveryAddress.setPostCode("GU31 2ZZ");
		deliveryAddress.setStreetName("AcaciaAvenue");
		deliveryAddress.setTownName("Sparsholt");
		
		PaymentSetupFoundationResource paymentSetupFoundationResource = new PaymentSetupFoundationResource();
		
		Mockito.when(sequenceGenerator.getNextSequenceId(anyString())).thenReturn(2123L);
		Mockito.when(paymentSetupBankRepository.save(any(PaymentSetupFoundationResource.class))).thenReturn(paymentSetupFoundationResource);
		
		PaymentSetupStagingResponse paymentSetupStagingResponse = paymentSetupStagingMongoDbAdapterImpl.createStagingPaymentSetup(request, new HashMap<>());
		assertNotNull(paymentSetupStagingResponse);
		
		
		deliveryAddress.setCountry(null);
		paymentSetupStagingResponse = paymentSetupStagingMongoDbAdapterImpl.createStagingPaymentSetup(request, new HashMap<>());
		assertNotNull(paymentSetupStagingResponse);
		
		deliveryAddress.setCountry("UK");
		risk.setDeliveryAddress(null);
		paymentSetupStagingResponse = paymentSetupStagingMongoDbAdapterImpl.createStagingPaymentSetup(request, new HashMap<>());
		assertNotNull(paymentSetupStagingResponse);
	}
	
	@Test(expected = PSD2Exception.class)
	public void createStagingPaymentSetupExceptionTest(){
		
		CustomPaymentSetupPOSTRequest request = new CustomPaymentSetupPOSTRequest();
		PaymentSetup data = new PaymentSetup();
		request.setData(data);
		Risk risk = new Risk();
		request.setRisk(risk);
		
		PaymentSetupInitiation initiation = new PaymentSetupInitiation();
		data.setInitiation(initiation);
		
		initiation.setInstructionIdentification("ABDDC");
		initiation.setEndToEndIdentification("FRESCO.21302.GFX.28");
		
		PaymentSetupInitiationInstructedAmount instructedAmount = new PaymentSetupInitiationInstructedAmount();
		instructedAmount.setAmount("777777777.00");
		instructedAmount.setCurrency("EUR");
		initiation.setInstructedAmount(instructedAmount);
		
		CreditorAgent creditorAgent = new CreditorAgent();
		creditorAgent.setSchemeName(SchemeNameEnum.BICFI);
		creditorAgent.setIdentification("SC080801");
		initiation.setCreditorAgent(creditorAgent);
		
		CreditorAccount creditorAccount = new CreditorAccount();
		creditorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.IBAN);
		creditorAccount.setIdentification("NWBK60161331926819");
		creditorAccount.setName("Test user");
		creditorAccount.setSecondaryIdentification("0002");
		initiation.setCreditorAccount(creditorAccount);
		
		DebtorAgent debtorAgent = new DebtorAgent();
		debtorAgent.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAgent.SchemeNameEnum.BICFI);
		debtorAgent.setIdentification("SC112800");
		initiation.setDebtorAgent(debtorAgent);
		
		DebtorAccount debtorAccount = new DebtorAccount();
		debtorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.IBAN);
		debtorAccount.setIdentification("GB29NWBK60161331926819");
		debtorAccount.setName("Andrea Smith");
		debtorAccount.setSecondaryIdentification("0002");
		initiation.setDebtorAccount(debtorAccount);
		
		RemittanceInformation remittanceInformation = new RemittanceInformation();
		remittanceInformation.setReference("FRESCO-101");
		remittanceInformation.setUnstructured("Internal ops code 5120101");
		initiation.setRemittanceInformation(remittanceInformation);
		
		risk.setMerchantCategoryCode("5967");
		risk.setMerchantCustomerIdentification("053598653254");
		risk.setPaymentContextCode(PaymentContextCodeEnum.ECOMMERCEGOODS);
		
		RiskDeliveryAddress deliveryAddress = new RiskDeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		
		List<String> addressLine = new ArrayList<>();
		addressLine.add("Flat 7");
		addressLine.add("Acacia Lodge");		
		deliveryAddress.setAddressLine(addressLine);
		
		deliveryAddress.setBuildingNumber("27");
		deliveryAddress.setCountry("UK");
		
		List<String> countrySubDivision = new ArrayList<>();
		countrySubDivision.add("ABCDABCDABCDABCDAB");
		countrySubDivision.add("DEFG");
		deliveryAddress.setCountrySubDivision(countrySubDivision);
		
		deliveryAddress.setPostCode("GU31 2ZZ");
		deliveryAddress.setStreetName("AcaciaAvenue");
		deliveryAddress.setTownName("Sparsholt");
		
		Mockito.when(sequenceGenerator.getNextSequenceId(anyString())).thenReturn(2123L);
		Mockito.when(paymentSetupBankRepository.save(any(PaymentSetupFoundationResource.class))).thenThrow(new DataAccessResourceFailureException("Hey"));
		paymentSetupStagingMongoDbAdapterImpl.createStagingPaymentSetup(request, new HashMap<>());
		
		
	}
	
	@Test
	public void updateStagedPaymentSetupTest(){
		
		CustomPaymentSetupPOSTResponse response = new CustomPaymentSetupPOSTResponse();
		PaymentSetupResponse data = new PaymentSetupResponse();
		response.setData(data);
		data.setPaymentId("12345");
		data.setCreationDateTime(PispUtilities.getCurrentDateInISOFormat());
		data.setStatus(StatusEnum.ACCEPTEDTECHNICALVALIDATION);
		
		Risk risk = new Risk();
		response.setRisk(risk);
		
		PaymentSetupResponseInitiation initiation = new PaymentSetupResponseInitiation();
		data.setInitiation(initiation);
		
		initiation.setInstructionIdentification("ABDDC");
		initiation.setEndToEndIdentification("FRESCO.21302.GFX.28");
		
		PaymentSetupInitiationInstructedAmount instructedAmount = new PaymentSetupInitiationInstructedAmount();
		instructedAmount.setAmount("777777777.00");
		instructedAmount.setCurrency("EUR");
		initiation.setInstructedAmount(instructedAmount);
		
		CreditorAgent creditorAgent = new CreditorAgent();
		creditorAgent.setSchemeName(SchemeNameEnum.BICFI);
		creditorAgent.setIdentification("SC080801");
		initiation.setCreditorAgent(creditorAgent);
		
		CreditorAccount creditorAccount = new CreditorAccount();
		creditorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.IBAN);
		creditorAccount.setIdentification("NWBK60161331926819");
		creditorAccount.setName("Test user");
		creditorAccount.setSecondaryIdentification("0002");
		initiation.setCreditorAccount(creditorAccount);
		
		DebtorAgent debtorAgent = new DebtorAgent();
		debtorAgent.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAgent.SchemeNameEnum.BICFI);
		debtorAgent.setIdentification("SC112800");
		initiation.setDebtorAgent(debtorAgent);
		
		DebtorAccount debtorAccount = new DebtorAccount();
		debtorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.IBAN);
		debtorAccount.setIdentification("GB29NWBK60161331926819");
		debtorAccount.setName("Andrea Smith");
		debtorAccount.setSecondaryIdentification("0002");
		initiation.setDebtorAccount(debtorAccount);
		
		RemittanceInformation remittanceInformation = new RemittanceInformation();
		remittanceInformation.setReference("FRESCO-101");
		remittanceInformation.setUnstructured("Internal ops code 5120101");
		initiation.setRemittanceInformation(remittanceInformation);
		
		risk.setMerchantCategoryCode("5967");
		risk.setMerchantCustomerIdentification("053598653254");
		risk.setPaymentContextCode(PaymentContextCodeEnum.ECOMMERCEGOODS);
		
		RiskDeliveryAddress deliveryAddress = new RiskDeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		
		List<String> addressLine = new ArrayList<>();
		addressLine.add("Flat 7");
		addressLine.add("Acacia Lodge");		
		deliveryAddress.setAddressLine(addressLine);
		
		deliveryAddress.setBuildingNumber("27");
		deliveryAddress.setCountry("UK");
		
		List<String> countrySubDivision = new ArrayList<>();
		countrySubDivision.add("ABCDABCDABCDABCDAB");
		countrySubDivision.add("DEFG");
		deliveryAddress.setCountrySubDivision(countrySubDivision);
		
		deliveryAddress.setPostCode("GU31 2ZZ");
		deliveryAddress.setStreetName("AcaciaAvenue");
		deliveryAddress.setTownName("Sparsholt");
		
		response.setLinks(new PaymentSetupPOSTResponseLinks());
		response.setMeta(new PaymentSetupPOSTResponseMeta());
		
		PaymentSetupFoundationResource paymentSetupFoundationResource = new PaymentSetupFoundationResource();
		paymentSetupFoundationResource.setData(data);
		Mockito.when(paymentSetupBankRepository.findOneByDataPaymentId(anyString())).thenReturn(paymentSetupFoundationResource);
		Mockito.when(paymentSetupBankRepository.save(any(PaymentSetupFoundationResource.class))).thenReturn(paymentSetupFoundationResource);
		/*
		 * Normal Flow
		 */
		PaymentSetupStagingResponse paymentSetupStagingResponse = paymentSetupStagingMongoDbAdapterImpl.updateStagedPaymentSetup(response, new HashMap<>());
		assertNotNull(paymentSetupStagingResponse);
		
		/*
		 * CreationDateTime Null
		 */
		data.setCreationDateTime(null);
		paymentSetupStagingResponse = paymentSetupStagingMongoDbAdapterImpl.updateStagedPaymentSetup(response, new HashMap<>());
		assertNotNull(paymentSetupStagingResponse);
		
	}
	
	@Test(expected = PSD2Exception.class)
	public void updateStagedPaymentSetupExcetionTest(){
		CustomPaymentSetupPOSTResponse response = new CustomPaymentSetupPOSTResponse();
		PaymentSetupResponse data = new PaymentSetupResponse();
		response.setData(data);
		data.setPaymentId("12345");
		Mockito.when(paymentSetupBankRepository.findOneByDataPaymentId(anyString())).thenThrow(new DataAccessResourceFailureException("Hey"));
		paymentSetupStagingMongoDbAdapterImpl.updateStagedPaymentSetup(response, new HashMap<>());
	}
	
	@Test(expected = PSD2Exception.class)
	public void retrieveStagedPaymentSetupTest(){
		
		PaymentSetupFoundationResource paymentSetupFoundationResource = new PaymentSetupFoundationResource();
		PaymentSetupResponse data = new PaymentSetupResponse();
		paymentSetupFoundationResource.setData(data);
		data.setPaymentId("12345");
		data.setCreationDateTime(PispUtilities.getCurrentDateInISOFormat());
		data.setStatus(StatusEnum.ACCEPTEDTECHNICALVALIDATION);
		
		Risk risk = new Risk();
		paymentSetupFoundationResource.setRisk(risk);
		
		PaymentSetupResponseInitiation initiation = new PaymentSetupResponseInitiation();
		data.setInitiation(initiation);
		
		initiation.setInstructionIdentification("ABDDC");
		initiation.setEndToEndIdentification("FRESCO.21302.GFX.28");
		
		PaymentSetupInitiationInstructedAmount instructedAmount = new PaymentSetupInitiationInstructedAmount();
		instructedAmount.setAmount("777777777.00");
		instructedAmount.setCurrency("EUR");
		initiation.setInstructedAmount(instructedAmount);
		
		CreditorAgent creditorAgent = new CreditorAgent();
		creditorAgent.setSchemeName(SchemeNameEnum.BICFI);
		creditorAgent.setIdentification("SC080801");
		initiation.setCreditorAgent(creditorAgent);
		
		CreditorAccount creditorAccount = new CreditorAccount();
		creditorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.IBAN);
		creditorAccount.setIdentification("NWBK60161331926819");
		creditorAccount.setName("Test user");
		creditorAccount.setSecondaryIdentification("0002");
		initiation.setCreditorAccount(creditorAccount);
		
		DebtorAgent debtorAgent = new DebtorAgent();
		debtorAgent.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAgent.SchemeNameEnum.BICFI);
		debtorAgent.setIdentification("SC112800");
		initiation.setDebtorAgent(debtorAgent);
		
		DebtorAccount debtorAccount = new DebtorAccount();
		debtorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.IBAN);
		debtorAccount.setIdentification("GB29NWBK60161331926819");
		debtorAccount.setName("Andrea Smith");
		debtorAccount.setSecondaryIdentification("0002");
		initiation.setDebtorAccount(debtorAccount);
		
		RemittanceInformation remittanceInformation = new RemittanceInformation();
		remittanceInformation.setReference("FRESCO-101");
		remittanceInformation.setUnstructured("Internal ops code 5120101");
		initiation.setRemittanceInformation(remittanceInformation);
		
		risk.setMerchantCategoryCode("5967");
		risk.setMerchantCustomerIdentification("053598653254");
		risk.setPaymentContextCode(PaymentContextCodeEnum.ECOMMERCEGOODS);
		
		RiskDeliveryAddress deliveryAddress = new RiskDeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		
		List<String> addressLine = new ArrayList<>();
		addressLine.add("Flat 7");
		addressLine.add("Acacia Lodge");		
		deliveryAddress.setAddressLine(addressLine);
		
		deliveryAddress.setBuildingNumber("27");
		deliveryAddress.setCountry("UK");
		
		List<String> countrySubDivision = new ArrayList<>();
		countrySubDivision.add("ABCDABCDABCDABCDAB");
		countrySubDivision.add("DEFG");
		deliveryAddress.setCountrySubDivision(countrySubDivision);
		
		deliveryAddress.setPostCode("GU31 2ZZ");
		deliveryAddress.setStreetName("AcaciaAvenue");
		deliveryAddress.setTownName("Sparsholt");
		
		paymentSetupFoundationResource.setLinks(new PaymentSetupPOSTResponseLinks());
		paymentSetupFoundationResource.setMeta(new PaymentSetupPOSTResponseMeta());
		
		Mockito.when(paymentSetupBankRepository.findOneByDataPaymentId(anyString())).thenReturn(paymentSetupFoundationResource);
		CustomPaymentSetupPOSTResponse paymentSetupPOSTResponse = paymentSetupStagingMongoDbAdapterImpl.retrieveStagedPaymentSetup("test", new HashMap<>());
		assertNotNull(paymentSetupPOSTResponse);
		/*
		 * Exception
		 */
		Mockito.when(paymentSetupBankRepository.findOneByDataPaymentId(anyString())).thenReturn(null);
		paymentSetupStagingMongoDbAdapterImpl.retrieveStagedPaymentSetup("test", new HashMap<>());
		
	}
	
	@Test(expected = PSD2Exception.class)
	public void retrieveStagedPaymentSetupException1Test(){
		PaymentSetupFoundationResource paymentSetupFoundationResource = new PaymentSetupFoundationResource();
		paymentSetupFoundationResource.setData(null);
		Mockito.when(paymentSetupBankRepository.findOneByDataPaymentId(anyString())).thenReturn(paymentSetupFoundationResource);
		paymentSetupStagingMongoDbAdapterImpl.retrieveStagedPaymentSetup("test", new HashMap<>());
	}
	
	@Test(expected = PSD2Exception.class)
	public void retrieveStagedPaymentSetupException2Test(){
		PaymentSetupFoundationResource paymentSetupFoundationResource = new PaymentSetupFoundationResource();
		PaymentSetupResponse data = new PaymentSetupResponse();
		paymentSetupFoundationResource.setData(data);
		data.setPaymentId("12345");
		
		data.setCreationDateTime(PispUtilities.getCurrentDateInISOFormat());
		data.setStatus(StatusEnum.ACCEPTEDTECHNICALVALIDATION);
		paymentSetupFoundationResource.setData(data);
		
		paymentSetupFoundationResource.getData().setInitiation(null);
		Mockito.when(paymentSetupBankRepository.findOneByDataPaymentId(anyString())).thenReturn(paymentSetupFoundationResource);
		paymentSetupStagingMongoDbAdapterImpl.retrieveStagedPaymentSetup("test", new HashMap<>());
	}
	
	@Test(expected = PSD2Exception.class)
	public void retrieveStagedPaymentSetupException3Test(){
		Mockito.when(paymentSetupBankRepository.findOneByDataPaymentId(anyString())).thenThrow(new DataAccessResourceFailureException("Hey"));
		paymentSetupStagingMongoDbAdapterImpl.retrieveStagedPaymentSetup("test", new HashMap<>());
	}
		
	
}
