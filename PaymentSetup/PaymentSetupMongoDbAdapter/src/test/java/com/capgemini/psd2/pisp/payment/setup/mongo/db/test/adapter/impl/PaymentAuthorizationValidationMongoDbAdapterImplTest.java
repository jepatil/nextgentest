package com.capgemini.psd2.pisp.payment.setup.mongo.db.test.adapter.impl;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.DebtorAccount;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponseInitiation;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.impl.PaymentAuthorizationValidationMongoDbAdapterImpl;

@RunWith(SpringJUnit4ClassRunner.class)
public class PaymentAuthorizationValidationMongoDbAdapterImplTest {

	@InjectMocks
	PaymentAuthorizationValidationMongoDbAdapterImpl paymentAuthorizationValidationMongoDbAdapterImpl;
	/**
	 * Sets the up.
	 */
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testPreAuthorizationPaymentValidation(){
		
		CustomPaymentSetupPOSTResponse paymentSetupResponse = new CustomPaymentSetupPOSTResponse();
		PaymentSetupResponse data = new PaymentSetupResponse();
		PaymentSetupResponseInitiation initiation = new PaymentSetupResponseInitiation();
		DebtorAccount debtorAccount = new DebtorAccount();
		Map<String, String> params = new HashMap<>();
		
		paymentSetupResponse.setData(data);
		data.setInitiation(initiation);
		initiation.setDebtorAccount(debtorAccount);
		initiation.setEndToEndIdentification("FS_PMV_0");
		paymentAuthorizationValidationMongoDbAdapterImpl.preAuthorizationPaymentValidation(paymentSetupResponse, params);
		
		initiation.setEndToEndIdentification("FS_PMV_014");
		paymentAuthorizationValidationMongoDbAdapterImpl.preAuthorizationPaymentValidation(paymentSetupResponse, params);
	}
	
	@Test
	public void testExceptionForPreAuthorizationPaymentValidation(){
		
		CustomPaymentSetupPOSTResponse paymentSetupResponse = new CustomPaymentSetupPOSTResponse();
		PaymentSetupResponse data = new PaymentSetupResponse();
		PaymentSetupResponseInitiation initiation = new PaymentSetupResponseInitiation();
		DebtorAccount debtorAccount = new DebtorAccount();
		Map<String, String> params = new HashMap<>();
		
		paymentSetupResponse.setData(data);
		data.setInitiation(initiation);
		initiation.setDebtorAccount(null);
		
		
		initiation.setEndToEndIdentification("FS_PMV_0");
		paymentAuthorizationValidationMongoDbAdapterImpl.preAuthorizationPaymentValidation(paymentSetupResponse, params);
	}
}
