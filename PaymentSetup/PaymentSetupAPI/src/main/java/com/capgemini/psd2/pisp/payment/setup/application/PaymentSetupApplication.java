package com.capgemini.psd2.pisp.payment.setup.application;

import javax.servlet.Filter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.capgemini.psd2.logger.PSD2Filter;
import com.capgemini.psd2.pisp.validation.JsonStringTrimmer;


@SpringBootApplication
@ComponentScan(basePackages = {"com.capgemini.psd2"})
@EnableMongoRepositories(basePackages={"com.capgemini.psd2"})
@EnableEurekaClient
public class PaymentSetupApplication {

	static ConfigurableApplicationContext context = null;
	public static void main(String[] args) {
		context=SpringApplication.run(PaymentSetupApplication.class, args);
	}	
	/*
	 * To trim the white space from the fields of JSON request pay load
	 */
	@Bean
	public JsonStringTrimmer jsonStringTrimmer(){
		return new JsonStringTrimmer();
	}
	
	@Bean(name="psd2Filter")
	public Filter psd2Filter(){
		return new PSD2Filter(); 
	}	
}
