package com.capgemini.psd2.pisp.payment.setup.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.payment.setup.service.PaymentSetupService;
import com.capgemini.psd2.pisp.validation.adapter.PaymentValidator;

@RestController
public class PaymentSetupController {

	@Autowired
	private PaymentSetupService paymentSetupService; 

	@Autowired
	private PaymentValidator paymentSetupValidator;
		
	@Value("${app.responseErrorMessageEnabled}")
	private String responseErrorMessageEnabled;
	
	@RequestMapping(value="/payments",method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_UTF8_VALUE }, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE })	
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<PaymentSetupPOSTResponse> createPaymentSetupResource(@RequestBody PaymentSetupPOSTRequest paymentSetupRequest){
		
		CustomPaymentSetupPOSTRequest customPaymentSetupRequest = new CustomPaymentSetupPOSTRequest();
		customPaymentSetupRequest.setData(paymentSetupRequest.getData());
		customPaymentSetupRequest.setRisk(paymentSetupRequest.getRisk());
		boolean  isValidPaymentSetupRequest = Boolean.FALSE;	
		try{			
			 isValidPaymentSetupRequest = paymentSetupValidator.validatePaymentSetupRequest(customPaymentSetupRequest);			
			PaymentSetupPOSTResponse paymentSetupResponse = paymentSetupService.createPaymentSetupResource(customPaymentSetupRequest, isValidPaymentSetupRequest);				
			return new ResponseEntity<>(paymentSetupResponse, HttpStatus.CREATED);		
			
		}catch(PSD2Exception psd2Exception){	
			if(! isValidPaymentSetupRequest)
				paymentSetupService.createPaymentSetupResource(customPaymentSetupRequest, isValidPaymentSetupRequest);
			
			if(Boolean.valueOf(responseErrorMessageEnabled))
				throw psd2Exception;
			
			return new ResponseEntity<>(HttpStatus.valueOf(Integer.parseInt(psd2Exception.getErrorInfo().getStatusCode())));			
		}					
	}

	@RequestMapping(value="/payments/{paymentId}",method = RequestMethod.GET,produces = {MediaType.APPLICATION_JSON_UTF8_VALUE })	
	@ResponseBody
	public ResponseEntity<PaymentSetupPOSTResponse> retrievePaymentSetupResource(PaymentRetrieveGetRequest paymentRetrieveRequest){
		
		try{
			paymentSetupValidator.validatePaymentRetrieveRequest(paymentRetrieveRequest);
			PaymentSetupPOSTResponse paymentSetupResponse = paymentSetupService.retrievePaymentSetupResource(paymentRetrieveRequest);			
			return new ResponseEntity<>(paymentSetupResponse, HttpStatus.OK);		
		}catch(PSD2Exception psd2Exception){			
			if(Boolean.valueOf(responseErrorMessageEnabled))
				throw psd2Exception;			
			return new ResponseEntity<>(HttpStatus.valueOf(Integer.parseInt(psd2Exception.getErrorInfo().getStatusCode())));			
		}	
	}

}
