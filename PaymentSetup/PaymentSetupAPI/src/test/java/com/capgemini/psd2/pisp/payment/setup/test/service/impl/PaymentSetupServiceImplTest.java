package com.capgemini.psd2.pisp.payment.setup.test.service.impl;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyObject;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.PaymentSetupAdapterHelper;
import com.capgemini.psd2.pisp.adapter.PaymentSetupPlatformAdapter;
import com.capgemini.psd2.pisp.domain.CreditorAccount;
import com.capgemini.psd2.pisp.domain.CreditorAgent;
import com.capgemini.psd2.pisp.domain.CreditorAgent.SchemeNameEnum;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.DebtorAccount;
import com.capgemini.psd2.pisp.domain.DebtorAgent;
import com.capgemini.psd2.pisp.domain.PaymentResponseInfo;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;
import com.capgemini.psd2.pisp.domain.PaymentSetup;
import com.capgemini.psd2.pisp.domain.PaymentSetupInitiation;
import com.capgemini.psd2.pisp.domain.PaymentSetupInitiationInstructedAmount;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupStagingResponse;
import com.capgemini.psd2.pisp.domain.RemittanceInformation;
import com.capgemini.psd2.pisp.domain.Risk;
import com.capgemini.psd2.pisp.domain.Risk.PaymentContextCodeEnum;
import com.capgemini.psd2.pisp.domain.RiskDeliveryAddress;
import com.capgemini.psd2.pisp.payment.setup.comparator.PaymentSetupPayloadComparator;
import com.capgemini.psd2.pisp.payment.setup.service.impl.PaymentSetupServiceImpl;
import com.capgemini.psd2.pisp.payment.setup.transformer.PaymentSetupResponseTransformer;
import com.capgemini.psd2.pisp.validation.PispUtilities;

@RunWith(SpringJUnit4ClassRunner.class)
public class PaymentSetupServiceImplTest {
		
	@InjectMocks
	private PaymentSetupServiceImpl service = new PaymentSetupServiceImpl("5");
	
	@Mock
	private PaymentSetupResponseTransformer responseTransformer;
	
	@Mock
	private PaymentSetupPayloadComparator paymentSetupComparator;	
	
	@Mock
	private PaymentSetupPlatformAdapter paymentSetupPlatformAdapter;
	
	@Mock
	private PaymentSetupAdapterHelper paymentSetupAdapterHelper;
	
	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void contextLoads() {
	}
	
	@Test(expected = PSD2Exception.class)
	public void createPaymentSetupResourceInvalidSetupRequestTest() {
		
		CustomPaymentSetupPOSTRequest request = new CustomPaymentSetupPOSTRequest();
		PaymentSetup data = new PaymentSetup();
		request.setData(data);
		Risk risk = new Risk();
		request.setRisk(risk);
		
		PaymentSetupInitiation initiation = new PaymentSetupInitiation();
		data.setInitiation(initiation);
		
		initiation.setInstructionIdentification("ABDDC");
		initiation.setEndToEndIdentification("FRESCO.21302.GFX.28");
		
		PaymentSetupInitiationInstructedAmount instructedAmount = new PaymentSetupInitiationInstructedAmount();
		instructedAmount.setAmount("777777777.00");
		instructedAmount.setCurrency("EUR");
		initiation.setInstructedAmount(instructedAmount);
		
		CreditorAgent creditorAgent = new CreditorAgent();
		creditorAgent.setSchemeName(SchemeNameEnum.BICFI);
		creditorAgent.setIdentification("SC080801");
		initiation.setCreditorAgent(creditorAgent);
		
		CreditorAccount creditorAccount = new CreditorAccount();
		creditorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.IBAN);
		creditorAccount.setIdentification("NWBK60161331926819");
		creditorAccount.setName("Test user");
		creditorAccount.setSecondaryIdentification("0002");
		initiation.setCreditorAccount(creditorAccount);
		
		DebtorAgent debtorAgent = new DebtorAgent();
		debtorAgent.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAgent.SchemeNameEnum.BICFI);
		debtorAgent.setIdentification("SC112800");
		initiation.setDebtorAgent(debtorAgent);
		
		DebtorAccount debtorAccount = new DebtorAccount();
		debtorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.IBAN);
		debtorAccount.setIdentification("GB29NWBK60161331926819");
		debtorAccount.setName("Andrea Smith");
		debtorAccount.setSecondaryIdentification("0002");
		initiation.setDebtorAccount(debtorAccount);
		
		RemittanceInformation remittanceInformation = new RemittanceInformation();
		remittanceInformation.setReference("FRESCO-101");
		remittanceInformation.setUnstructured("Internal ops code 5120101");
		initiation.setRemittanceInformation(remittanceInformation);
		
		risk.setMerchantCategoryCode("5967");
		risk.setMerchantCustomerIdentification("053598653254");
		risk.setPaymentContextCode(PaymentContextCodeEnum.ECOMMERCEGOODS);
		
		RiskDeliveryAddress deliveryAddress = new RiskDeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		
		List<String> addressLine = new ArrayList<>();
		addressLine.add("Flat 7");
		addressLine.add("Acacia Lodge");		
		deliveryAddress.setAddressLine(addressLine);
		
		deliveryAddress.setBuildingNumber("27");
		deliveryAddress.setCountry("UK");
		
		List<String> countrySubDivision = new ArrayList<>();
		countrySubDivision.add("ABCDABCDABCDABCDAB");
		countrySubDivision.add("DEFG");
		deliveryAddress.setCountrySubDivision(countrySubDivision);
		
		deliveryAddress.setPostCode("GU31 2ZZ");
		deliveryAddress.setStreetName("AcaciaAvenue");
		deliveryAddress.setTownName("Sparsholt");
		
		PaymentResponseInfo paymentResponseInfo = new PaymentResponseInfo();
		PaymentSetupPlatformResource paymentSetupPlatformResource = new PaymentSetupPlatformResource();
		paymentSetupPlatformResource.setId("59a6b78f8207f500610373a8");
		paymentSetupPlatformResource.setCreatedAt(PispUtilities.getCurrentDateInISOFormat());
		paymentSetupPlatformResource.setEndToEndIdentification("DEMO_USER");
		paymentSetupPlatformResource.setIdempotencyKey("FRESCO.21302.GFX.1028");
		paymentSetupPlatformResource.setIdempotencyRequest("true");
		paymentSetupPlatformResource.setInstructionIdentification("ABDDCF");
		paymentSetupPlatformResource.setPaymentId("71de8c681cdc476cb1dda68e376f2812");
		paymentSetupPlatformResource.setStatus("AcceptedTechnicalValidation");
		paymentSetupPlatformResource.setTppCID("6443e15975554bce8099e35b88b40465");
		paymentSetupPlatformResource.setTppDebtorDetails("false");
		
		CustomPaymentSetupPOSTResponse paymentSetupPOSTResponse = new CustomPaymentSetupPOSTResponse();
		PaymentSetupStagingResponse setupStagingResponse = new PaymentSetupStagingResponse();
		setupStagingResponse.setPaymentId("71de8c681cdc476cb1dda68e376f2812");
		
		Mockito.when(paymentSetupAdapterHelper.validatePreStagePaymentSetup(anyObject())).thenReturn(paymentResponseInfo);
		Mockito.when(paymentSetupPlatformAdapter.createPaymentSetupResource(anyObject(), anyObject())).thenReturn(paymentSetupPlatformResource);
		Mockito.when(responseTransformer.paymentSetupResponseTransformer(anyObject(), anyObject(), anyObject())).thenReturn(paymentSetupPOSTResponse);
		
//		Mockito.when(paymentSetupPlatformAdapter.createPaymentSetupResource(anyObject(), anyObject())).thenReturn(paymentSetupPlatformResource);
		
		Mockito.when(paymentSetupPlatformAdapter.getIdempotentPaymentSetupResource(anyLong())).thenReturn(paymentSetupPlatformResource);
		Mockito.when(paymentSetupAdapterHelper.retrieveStagedPaymentSetup(anyObject())).thenReturn(paymentSetupPOSTResponse);
		Mockito.when(paymentSetupComparator.comparePaymentDetails(anyObject(), anyObject(), anyObject())).thenReturn(0);
//		Mockito.when(responseTransformer.paymentSetupResponseTransformer(anyObject(), anyObject(), anyObject())).thenReturn(paymentSetupPOSTResponse);
		
		Mockito.when(paymentSetupAdapterHelper.createStagingPaymentSetup(anyObject())).thenReturn(setupStagingResponse);
		Mockito.doNothing().when(paymentSetupPlatformAdapter).updatePaymentSetupResource(anyObject());
		
		Mockito.when(paymentSetupPlatformAdapter.retrievePaymentSetupResource(anyObject())).thenReturn(null);
		Mockito.doNothing().when(paymentSetupPlatformAdapter).updatePaymentSetupResource(anyObject());
		
//		Mockito.when(paymentSetupAdapterHelper.createStagingPaymentSetup(anyObject(), anyObject())).thenReturn(setupStagingResponse);
		Mockito.doNothing().when(paymentSetupPlatformAdapter).updatePaymentSetupResource(anyObject());
		/*
		 * 
		 */
		service.createPaymentSetupResource(request, true);
		/*
		 * 
		 */
		service.createPaymentSetupResource(request, false);
		
		/*
		 * 
		 */
		PaymentSetupResponse paymentSetupResponse = new PaymentSetupResponse();
		paymentSetupResponse.setPaymentId("123");
		paymentSetupPOSTResponse.setData(paymentSetupResponse);
		Mockito.when(responseTransformer.paymentSetupResponseTransformer(anyObject(), anyObject(), anyObject())).thenReturn(paymentSetupPOSTResponse);
		PaymentSetupPOSTResponse paymentSetupPOSTResponse1 = service.createPaymentSetupResource(request, true);
//		assertNotNull(paymentSetupPOSTResponse);
		/*
		 * 
		 */
		PaymentSetupStagingResponse stagingResponse = new PaymentSetupStagingResponse();
		stagingResponse.setPaymentId("123");
		Mockito.when(paymentSetupAdapterHelper.createStagingPaymentSetup(anyObject())).thenReturn(stagingResponse);
		Mockito.doNothing().when(paymentSetupPlatformAdapter).updatePaymentSetupResource(anyObject());
		PaymentSetupPOSTResponse paymentSetupPOSTResponse3 = new PaymentSetupPOSTResponse();
		paymentSetupPOSTResponse3.setData(null);
		paymentSetupPlatformResource.setStatus("Pending");
		Mockito.when(responseTransformer.paymentSetupResponseTransformer(anyObject(), anyObject(), anyObject())).thenReturn(paymentSetupPOSTResponse3);
		service.createPaymentSetupResource(request, true);
		/*
		 *	Throws PISP_PAYMENT_SETUP_CREATION_FAILED in createStagingPaymentSetup
		 */
		stagingResponse.setPaymentId(null);
		Mockito.when(paymentSetupAdapterHelper.createStagingPaymentSetup(anyObject())).thenReturn(stagingResponse);
		Mockito.doNothing().when(paymentSetupPlatformAdapter).updatePaymentSetupResource(anyObject());
		paymentSetupPOSTResponse3.setData(null);
		Mockito.when(responseTransformer.paymentSetupResponseTransformer(anyObject(), anyObject(), anyObject())).thenReturn(paymentSetupPOSTResponse3);
		service.createPaymentSetupResource(request, true);
	}
	

	
	@Test
	public void retrievePaymentSetupResourceTest(){
		
		PaymentSetupPlatformResource paymentSetupPlatformResource = new PaymentSetupPlatformResource();
		paymentSetupPlatformResource.setId("59a6b78f8207f500610373a8");
		paymentSetupPlatformResource.setCreatedAt(PispUtilities.getCurrentDateInISOFormat());
		paymentSetupPlatformResource.setEndToEndIdentification("DEMO_USER");
		paymentSetupPlatformResource.setIdempotencyKey("FRESCO.21302.GFX.1028");
		paymentSetupPlatformResource.setIdempotencyRequest("true");
		paymentSetupPlatformResource.setInstructionIdentification("ABDDCF");
		paymentSetupPlatformResource.setPaymentId("71de8c681cdc476cb1dda68e376f2812");
		paymentSetupPlatformResource.setStatus("AcceptedTechnicalValidation");
		paymentSetupPlatformResource.setTppCID("6443e15975554bce8099e35b88b40465");
		paymentSetupPlatformResource.setTppDebtorDetails("false");
		
		CustomPaymentSetupPOSTResponse paymentSetupPOSTResponse = new CustomPaymentSetupPOSTResponse();
		PaymentRetrieveGetRequest paymentRetrieveGetRequest = new PaymentRetrieveGetRequest();
		paymentRetrieveGetRequest.setPaymentId("71de8c681cdc476cb1dda68e376f2812");
		
		Mockito.when(paymentSetupPlatformAdapter.retrievePaymentSetupResource(anyObject())).thenReturn(paymentSetupPlatformResource);
		Mockito.when(reqHeaderAtrributes.getTppCID()).thenReturn("6443e15975554bce8099e35b88b40465");
		Mockito.when(paymentSetupAdapterHelper.retrieveStagedPaymentSetup(anyObject())).thenReturn(paymentSetupPOSTResponse);
		Mockito.when(responseTransformer.paymentSetupResponseTransformer(anyObject(), anyObject(), anyObject())).thenReturn(paymentSetupPOSTResponse);
		service.retrievePaymentSetupResource(paymentRetrieveGetRequest);
		
	}
	
	@Test(expected = PSD2Exception.class)
	public void retrievePaymentSetupResourcePlatformResponseNull(){
		
		PaymentSetupPlatformResource paymentSetupPlatformResource = new PaymentSetupPlatformResource();
		paymentSetupPlatformResource.setId("59a6b78f8207f500610373a8");
		paymentSetupPlatformResource.setCreatedAt(PispUtilities.getCurrentDateInISOFormat());
		paymentSetupPlatformResource.setEndToEndIdentification("DEMO_USER");
		paymentSetupPlatformResource.setIdempotencyKey("FRESCO.21302.GFX.1028");
		paymentSetupPlatformResource.setIdempotencyRequest("true");
		paymentSetupPlatformResource.setInstructionIdentification("ABDDCF");
		paymentSetupPlatformResource.setPaymentId("71de8c681cdc476cb1dda68e376f2812");
		paymentSetupPlatformResource.setStatus("AcceptedTechnicalValidation");
		paymentSetupPlatformResource.setTppCID("6443e15975554bce8099e35b88b40465");
		paymentSetupPlatformResource.setTppDebtorDetails("false");
		
		PaymentRetrieveGetRequest paymentRetrieveGetRequest = new PaymentRetrieveGetRequest();
		paymentRetrieveGetRequest.setPaymentId("71de8c681cdc476cb1dda68e376f2812");
		
		Mockito.when(paymentSetupPlatformAdapter.retrievePaymentSetupResource(anyObject())).thenReturn(null);
//		Mockito.when(reqHeaderAtrributes.getTppCID()).thenReturn("6443e15975554bce8099e35b88b40465");
//		Mockito.when(paymentSetupAdapterHelper.retrieveStagedPaymentSetup(anyObject())).thenReturn(paymentSetupPOSTResponse);
//		Mockito.when(responseTransformer.paymentSetupResponseTransformer(anyObject(), anyObject(), anyObject())).thenReturn(paymentSetupPOSTResponse);
		service.retrievePaymentSetupResource(paymentRetrieveGetRequest);
		
	}
	
	@Test(expected = PSD2Exception.class)
	public void retrievePaymentSetupResourceTPPIDNnotEqual(){
		
		PaymentSetupPlatformResource paymentSetupPlatformResource = new PaymentSetupPlatformResource();
		paymentSetupPlatformResource.setId("59a6b78f8207f500610373a8");
		paymentSetupPlatformResource.setCreatedAt(PispUtilities.getCurrentDateInISOFormat());
		paymentSetupPlatformResource.setEndToEndIdentification("DEMO_USER");
		paymentSetupPlatformResource.setIdempotencyKey("FRESCO.21302.GFX.1028");
		paymentSetupPlatformResource.setIdempotencyRequest("true");
		paymentSetupPlatformResource.setInstructionIdentification("ABDDCF");
		paymentSetupPlatformResource.setPaymentId("71de8c681cdc476cb1dda68e376f2812");
		paymentSetupPlatformResource.setStatus("AcceptedTechnicalValidation");
		paymentSetupPlatformResource.setTppCID("6443e15975554bce8099e35b");
		paymentSetupPlatformResource.setTppDebtorDetails("false");
		
		CustomPaymentSetupPOSTResponse paymentSetupPOSTResponse = new CustomPaymentSetupPOSTResponse();
		PaymentRetrieveGetRequest paymentRetrieveGetRequest = new PaymentRetrieveGetRequest();
		paymentRetrieveGetRequest.setPaymentId("71de8c681cdc476cb1dda68e376f2812");
		
		Mockito.when(paymentSetupPlatformAdapter.retrievePaymentSetupResource(anyObject())).thenReturn(paymentSetupPlatformResource);
		Mockito.when(reqHeaderAtrributes.getTppCID()).thenReturn("6443e15975554bce8099e35b88b40465");
		Mockito.when(paymentSetupAdapterHelper.retrieveStagedPaymentSetup(anyObject())).thenReturn(paymentSetupPOSTResponse);
		Mockito.when(responseTransformer.paymentSetupResponseTransformer(anyObject(), anyObject(), anyObject())).thenReturn(paymentSetupPOSTResponse);
		service.retrievePaymentSetupResource(paymentRetrieveGetRequest);
		
	}


}
