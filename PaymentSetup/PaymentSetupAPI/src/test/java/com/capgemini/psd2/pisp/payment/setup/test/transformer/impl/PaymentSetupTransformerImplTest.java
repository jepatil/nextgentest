package com.capgemini.psd2.pisp.payment.setup.test.transformer.impl;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CreditorAccount;
import com.capgemini.psd2.pisp.domain.CreditorAgent;
import com.capgemini.psd2.pisp.domain.CreditorAgent.SchemeNameEnum;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.DebtorAccount;
import com.capgemini.psd2.pisp.domain.DebtorAgent;
import com.capgemini.psd2.pisp.domain.PaymentSetupInitiationInstructedAmount;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseLinks;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseMeta;
import com.capgemini.psd2.pisp.domain.PaymentSetupPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse.StatusEnum;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponseInitiation;
import com.capgemini.psd2.pisp.domain.RemittanceInformation;
import com.capgemini.psd2.pisp.domain.Risk;
import com.capgemini.psd2.pisp.domain.Risk.PaymentContextCodeEnum;
import com.capgemini.psd2.pisp.domain.RiskDeliveryAddress;
import com.capgemini.psd2.pisp.payment.setup.transformer.impl.PaymentSetupResponseTransformerImpl;
import com.capgemini.psd2.pisp.validation.PispUtilities;

@RunWith(SpringJUnit4ClassRunner.class)
public class PaymentSetupTransformerImplTest {

	@InjectMocks
	PaymentSetupResponseTransformerImpl transformer;
	
	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;
	
	
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void paymentSetupResponseTransformerTestFirst(){
		
		CustomPaymentSetupPOSTResponse response = new CustomPaymentSetupPOSTResponse();
		PaymentSetupResponse data = new PaymentSetupResponse();
		response.setData(data);
		data.setPaymentId("12345");
		data.setCreationDateTime(PispUtilities.getCurrentDateInISOFormat());
		data.setStatus(StatusEnum.ACCEPTEDTECHNICALVALIDATION);
		
		Risk risk = new Risk();
		response.setRisk(risk);
		
		PaymentSetupResponseInitiation initiation = new PaymentSetupResponseInitiation();
		data.setInitiation(initiation);
		
		initiation.setInstructionIdentification("ABDDC");
		initiation.setEndToEndIdentification("FRESCO.21302.GFX.28");
		
		PaymentSetupInitiationInstructedAmount instructedAmount = new PaymentSetupInitiationInstructedAmount();
		instructedAmount.setAmount("777777777.00");
		instructedAmount.setCurrency("EUR");
		initiation.setInstructedAmount(instructedAmount);
		
		CreditorAgent creditorAgent = new CreditorAgent();
		creditorAgent.setSchemeName(SchemeNameEnum.BICFI);
		creditorAgent.setIdentification("SC080801");
		initiation.setCreditorAgent(creditorAgent);
		
		CreditorAccount creditorAccount = new CreditorAccount();
		creditorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.IBAN);
		creditorAccount.setIdentification("NWBK60161331926819");
		creditorAccount.setName("Test user");
		creditorAccount.setSecondaryIdentification("0002");
		initiation.setCreditorAccount(creditorAccount);
		
		DebtorAgent debtorAgent = new DebtorAgent();
		debtorAgent.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAgent.SchemeNameEnum.BICFI);
		debtorAgent.setIdentification("SC112800");
		initiation.setDebtorAgent(debtorAgent);
		
		DebtorAccount debtorAccount = new DebtorAccount();
		debtorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.IBAN);
		debtorAccount.setIdentification("GB29NWBK60161331926819");
		debtorAccount.setName("Andrea Smith");
		debtorAccount.setSecondaryIdentification("0002");
		initiation.setDebtorAccount(debtorAccount);
		
		RemittanceInformation remittanceInformation = new RemittanceInformation();
		remittanceInformation.setReference("FRESCO-101");
		remittanceInformation.setUnstructured("Internal ops code 5120101");
		initiation.setRemittanceInformation(remittanceInformation);
		
		risk.setMerchantCategoryCode("5967");
		risk.setMerchantCustomerIdentification("053598653254");
		risk.setPaymentContextCode(PaymentContextCodeEnum.ECOMMERCEGOODS);
		
		RiskDeliveryAddress deliveryAddress = new RiskDeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		
		List<String> addressLine = new ArrayList<>();
		addressLine.add("Flat 7");
		addressLine.add("Acacia Lodge");		
		deliveryAddress.setAddressLine(addressLine);
		
		deliveryAddress.setBuildingNumber("27");
		deliveryAddress.setCountry("UK");
		
		List<String> countrySubDivision = new ArrayList<>();
		countrySubDivision.add("ABCDABCDABCDABCDAB");
		countrySubDivision.add("DEFG");
		deliveryAddress.setCountrySubDivision(countrySubDivision);
		
		deliveryAddress.setPostCode("GU31 2ZZ");
		deliveryAddress.setStreetName("AcaciaAvenue");
		deliveryAddress.setTownName("Sparsholt");
		
		response.setLinks(new PaymentSetupPOSTResponseLinks());
		response.setMeta(new PaymentSetupPOSTResponseMeta());
		
		PaymentSetupPlatformResource paymentSetupPlatformResource = new PaymentSetupPlatformResource();
		paymentSetupPlatformResource.setId("59a6b78f8207f500610373a8");
		paymentSetupPlatformResource.setCreatedAt(PispUtilities.getCurrentDateInISOFormat());
		paymentSetupPlatformResource.setEndToEndIdentification("DEMO_USER");
		paymentSetupPlatformResource.setIdempotencyKey("FRESCO.21302.GFX.1028");
		paymentSetupPlatformResource.setIdempotencyRequest("true");
		paymentSetupPlatformResource.setInstructionIdentification("ABDDCF");
		paymentSetupPlatformResource.setPaymentId("71de8c681cdc476cb1dda68e376f2812");
		paymentSetupPlatformResource.setStatus("AcceptedTechnicalValidation");
		paymentSetupPlatformResource.setTppCID("6443e15975554bce8099e35b88b40465");
		paymentSetupPlatformResource.setTppDebtorDetails("false");
		transformer.paymentSetupResponseTransformer(response, paymentSetupPlatformResource, "POST");
		
//		
		paymentSetupPlatformResource.setTppDebtorDetails("true");
		paymentSetupPlatformResource.setTppDebtorNameDetails("false");
		response.getData().getInitiation().setDebtorAccount(debtorAccount);
		transformer.paymentSetupResponseTransformer(response, paymentSetupPlatformResource, "POST");
		
	}
	
	@Test
	public void paymentSetupResponseTransformerTestSecond(){
		
		CustomPaymentSetupPOSTResponse response = new CustomPaymentSetupPOSTResponse();
		PaymentSetupResponse data = new PaymentSetupResponse();
		response.setData(data);
		data.setPaymentId("12345");
		data.setCreationDateTime(PispUtilities.getCurrentDateInISOFormat());
		data.setStatus(StatusEnum.ACCEPTEDTECHNICALVALIDATION);
		
		Risk risk = new Risk();
		response.setRisk(risk);
		
		PaymentSetupResponseInitiation initiation = new PaymentSetupResponseInitiation();
		data.setInitiation(initiation);
		
		initiation.setInstructionIdentification("ABDDC");
		initiation.setEndToEndIdentification("FRESCO.21302.GFX.28");
		
		PaymentSetupInitiationInstructedAmount instructedAmount = new PaymentSetupInitiationInstructedAmount();
		instructedAmount.setAmount("777777777.00");
		instructedAmount.setCurrency("EUR");
		initiation.setInstructedAmount(instructedAmount);
		
		CreditorAgent creditorAgent = new CreditorAgent();
		creditorAgent.setSchemeName(SchemeNameEnum.BICFI);
		creditorAgent.setIdentification("SC080801");
		initiation.setCreditorAgent(creditorAgent);
		
		CreditorAccount creditorAccount = new CreditorAccount();
		creditorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.IBAN);
		creditorAccount.setIdentification("NWBK60161331926819");
		creditorAccount.setName("Test user");
		creditorAccount.setSecondaryIdentification("0002");
		initiation.setCreditorAccount(creditorAccount);
		
		DebtorAgent debtorAgent = new DebtorAgent();
		debtorAgent.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAgent.SchemeNameEnum.BICFI);
		debtorAgent.setIdentification("SC112800");
		initiation.setDebtorAgent(debtorAgent);
		
		DebtorAccount debtorAccount = new DebtorAccount();
		debtorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.IBAN);
		debtorAccount.setIdentification("GB29NWBK60161331926819");
		debtorAccount.setName("Andrea Smith");
		debtorAccount.setSecondaryIdentification("0002");
		initiation.setDebtorAccount(debtorAccount);
		
		RemittanceInformation remittanceInformation = new RemittanceInformation();
		remittanceInformation.setReference("FRESCO-101");
		remittanceInformation.setUnstructured("Internal ops code 5120101");
		initiation.setRemittanceInformation(remittanceInformation);
		
		risk.setMerchantCategoryCode("5967");
		risk.setMerchantCustomerIdentification("053598653254");
		risk.setPaymentContextCode(PaymentContextCodeEnum.ECOMMERCEGOODS);
		
		RiskDeliveryAddress deliveryAddress = new RiskDeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		
		List<String> addressLine = new ArrayList<>();
		addressLine.add("Flat 7");
		addressLine.add("Acacia Lodge");		
		deliveryAddress.setAddressLine(addressLine);
		
		deliveryAddress.setBuildingNumber("27");
		deliveryAddress.setCountry("UK");
		
		List<String> countrySubDivision = new ArrayList<>();
		countrySubDivision.add("ABCDABCDABCDABCDAB");
		countrySubDivision.add("DEFG");
		deliveryAddress.setCountrySubDivision(countrySubDivision);
		
		deliveryAddress.setPostCode("GU31 2ZZ");
		deliveryAddress.setStreetName("AcaciaAvenue");
		deliveryAddress.setTownName("Sparsholt");
		
//		response.setLinks(new PaymentSetupPOSTResponseLinks());
//		response.setMeta(new PaymentSetupPOSTResponseMeta());
		
		PaymentSetupPlatformResource paymentSetupPlatformResource = new PaymentSetupPlatformResource();
		paymentSetupPlatformResource.setId("59a6b78f8207f500610373a8");
		paymentSetupPlatformResource.setCreatedAt(PispUtilities.getCurrentDateInISOFormat());
		paymentSetupPlatformResource.setEndToEndIdentification("DEMO_USER");
		paymentSetupPlatformResource.setIdempotencyKey("FRESCO.21302.GFX.1028");
		paymentSetupPlatformResource.setIdempotencyRequest("true");
		paymentSetupPlatformResource.setInstructionIdentification("ABDDCF");
		paymentSetupPlatformResource.setPaymentId("71de8c681cdc476cb1dda68e376f2812");
		paymentSetupPlatformResource.setStatus("AcceptedTechnicalValidation");
		paymentSetupPlatformResource.setTppCID("6443e15975554bce8099e35b88b40465");
		paymentSetupPlatformResource.setTppDebtorDetails("false");
		
		Mockito.when(reqHeaderAtrributes.getSelfUrl()).thenReturn("test");
		transformer.paymentSetupResponseTransformer(response, paymentSetupPlatformResource, "POST");
		response.setLinks(null);
		response.setMeta(null);
		Mockito.when(reqHeaderAtrributes.getSelfUrl()).thenReturn("/test");
		transformer.paymentSetupResponseTransformer(response, paymentSetupPlatformResource, "POST");
		
	}
}
