package com.capgemini.psd2.pisp.payment.setup.validation.routing.test.adapter.impl;

import com.capgemini.psd2.pisp.domain.PaymentSetupValidationResponse;

public class PaymentSetupValidationRoutingAdapterTestMockData {

	
	public static PaymentSetupValidationResponse paymentSetupValidationResponse;
	
	public static PaymentSetupValidationResponse getPaymentSetupValidationResponse(){
		
		paymentSetupValidationResponse = new PaymentSetupValidationResponse();
		return paymentSetupValidationResponse;
	}
}
