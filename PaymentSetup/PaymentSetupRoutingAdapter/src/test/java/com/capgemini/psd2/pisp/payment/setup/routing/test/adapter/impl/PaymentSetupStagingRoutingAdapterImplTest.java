package com.capgemini.psd2.pisp.payment.setup.routing.test.adapter.impl;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyString;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.pisp.adapter.PaymentSetupStagingAdapter;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.payment.setup.routing.adapter.impl.PaymentSetupStagingRoutingAdapterImpl;
import com.capgemini.psd2.pisp.payment.setup.routing.adapter.routing.PaymentSetupAdapterFactory;

@RunWith(SpringJUnit4ClassRunner.class)
public class PaymentSetupStagingRoutingAdapterImplTest {

	@InjectMocks
	PaymentSetupStagingRoutingAdapterImpl adapter;
	
	@Mock
	private PaymentSetupAdapterFactory factory;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void retrieveStagedPaymentSetupTest() {
		PaymentSetupStagingAdapter paymentSetupStagingAdapter = new PaymentSetupStagingTestRoutingAdapterImpl();
		Mockito.when(factory.getPaymentSetupStagingInstance(anyString())).thenReturn(paymentSetupStagingAdapter);
		CustomPaymentSetupPOSTResponse response = adapter.retrieveStagedPaymentSetup("test", new HashMap<>());
		//assertNotNull(response);
	}
	
	@Test
	public void createStagingPaymentSetupTest(){
		PaymentSetupStagingAdapter paymentSetupStagingAdapter = new PaymentSetupStagingTestRoutingAdapterImpl();
		CustomPaymentSetupPOSTRequest paymentSetupPOSTRequest = new CustomPaymentSetupPOSTRequest();
		Mockito.when(factory.getPaymentSetupStagingInstance(anyString())).thenReturn(paymentSetupStagingAdapter);
		adapter.createStagingPaymentSetup(paymentSetupPOSTRequest, new HashMap<>());
	}
	
	@Test
	public void updateStagedPaymentSetupTest(){
		PaymentSetupStagingAdapter paymentSetupStagingAdapter = new PaymentSetupStagingTestRoutingAdapterImpl();
		CustomPaymentSetupPOSTResponse paymentSetupPOSTResponse = new CustomPaymentSetupPOSTResponse();
		Mockito.when(factory.getPaymentSetupStagingInstance(anyString())).thenReturn(paymentSetupStagingAdapter);
		adapter.updateStagedPaymentSetup(paymentSetupPOSTResponse, new HashMap<>());
	}
}
