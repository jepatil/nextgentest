package com.capgemini.psd2.pisp.payment.setup.routing.test.adapter.helper;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.aisp.domain.Data2Account;
import com.capgemini.psd2.aisp.domain.Data2Servicer;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.PaymentAuthorizationValidationAdapter;
import com.capgemini.psd2.pisp.adapter.PaymentSetupPlatformAdapter;
import com.capgemini.psd2.pisp.adapter.PaymentSetupStagingAdapter;
import com.capgemini.psd2.pisp.adapter.PaymentSetupValidationAdapter;
import com.capgemini.psd2.pisp.domain.CreditorAccount;
import com.capgemini.psd2.pisp.domain.CreditorAgent;
import com.capgemini.psd2.pisp.domain.CreditorAgent.SchemeNameEnum;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.DebtorAccount;
import com.capgemini.psd2.pisp.domain.DebtorAgent;
import com.capgemini.psd2.pisp.domain.PaymentResponseInfo;
import com.capgemini.psd2.pisp.domain.PaymentSetupInitiationInstructedAmount;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseLinks;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseMeta;
import com.capgemini.psd2.pisp.domain.PaymentSetupPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse.StatusEnum;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponseInitiation;
import com.capgemini.psd2.pisp.domain.PaymentSetupStagingResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupValidationResponse;
import com.capgemini.psd2.pisp.domain.RemittanceInformation;
import com.capgemini.psd2.pisp.domain.Risk;
import com.capgemini.psd2.pisp.domain.Risk.PaymentContextCodeEnum;
import com.capgemini.psd2.pisp.domain.RiskDeliveryAddress;
import com.capgemini.psd2.pisp.payment.setup.routing.adapter.helper.impl.PaymentSetupAdapterHelperImpl;
import com.capgemini.psd2.pisp.validation.PispUtilities;
import com.capgemini.psd2.token.Token;

@RunWith(SpringJUnit4ClassRunner.class)
public class PaymentSetupRoutingAdapterHelperTest {

	@InjectMocks
	private PaymentSetupAdapterHelperImpl adapter;
	
	@Mock
	@Qualifier("paymentSetupStagingRoutingAdapter")
	private PaymentSetupStagingAdapter paymentSetupStagingRoutingAdapter;
	
	@Mock
	@Qualifier("paymentSetupValidationRoutingAdapter")
	private PaymentSetupValidationAdapter paymentSetupValidationRoutingAdapter;
	
	@Mock
	@Qualifier("paymentSetupValidationRoutingAdapter")
	private PaymentAuthorizationValidationAdapter authorizationValidationRoutingAdapter;
	

	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;
	
	@Mock
	private PaymentSetupPlatformAdapter paymentPlatformAdapter;	
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void retrieveStagedPaymentSetupTest() {
		Token token = new Token();
		CustomPaymentSetupPOSTResponse setupPOSTResponse = new CustomPaymentSetupPOSTResponse();
		Map<String, String> seviceParams = new HashMap<>();
		seviceParams.put(PSD2Constants.CHANNEL_NAME, "test");
		token.setSeviceParams(seviceParams);
		Mockito.when(paymentSetupStagingRoutingAdapter.retrieveStagedPaymentSetup(anyObject(), anyObject())).thenReturn(setupPOSTResponse);
		//Mockito.when(reqHeaderAtrributes.getByValueToken()).thenReturn(token);
		Mockito.when(reqHeaderAtrributes.getPsuId()).thenReturn("test");
		Mockito.when(reqHeaderAtrributes.getCorrelationId()).thenReturn("test");
		setupPOSTResponse = adapter.retrieveStagedPaymentSetup("test");
		assertNotNull(setupPOSTResponse);
	}
	
	@Test
	public void validatePreStagePaymentSetupTest() {
		
		PaymentSetupValidationResponse paymentSetupValidationResponse =new PaymentSetupValidationResponse();
		
		
		CustomPaymentSetupPOSTRequest paymentSetupPOSTRequest = new CustomPaymentSetupPOSTRequest();
		
		Mockito.when(paymentSetupValidationRoutingAdapter.validatePreStagePaymentSetup(anyObject(), anyObject())).thenReturn(paymentSetupValidationResponse);
		PaymentResponseInfo paymentResponseInfo = adapter.validatePreStagePaymentSetup(paymentSetupPOSTRequest);
		assertNotNull(paymentResponseInfo);
		
		paymentSetupValidationResponse.setPaymentSetupValidationStatus("Rejected");
		paymentResponseInfo = adapter.validatePreStagePaymentSetup(paymentSetupPOSTRequest);
	}
	
	@Test
	public void createStagingPaymentSetupTest() {
		
		PaymentSetupStagingResponse setupStagingResponse = new PaymentSetupStagingResponse();
		CustomPaymentSetupPOSTRequest paymentSetupPOSTRequest = new CustomPaymentSetupPOSTRequest();
		Mockito.when(paymentSetupStagingRoutingAdapter.createStagingPaymentSetup(anyObject(), anyObject())).thenReturn(setupStagingResponse);
		setupStagingResponse = adapter.createStagingPaymentSetup(paymentSetupPOSTRequest);
		assertNotNull(setupStagingResponse);
	}
	
	@Test
	public void updateStagedPaymentSetupTest() {
		
		PaymentSetupStagingResponse setupStagingResponse = new PaymentSetupStagingResponse();
		CustomPaymentSetupPOSTResponse response = new CustomPaymentSetupPOSTResponse();
		PaymentSetupResponse data = new PaymentSetupResponse();
		response.setData(data);
		data.setPaymentId("12345");
		data.setCreationDateTime(PispUtilities.getCurrentDateInISOFormat());
		data.setStatus(StatusEnum.ACCEPTEDTECHNICALVALIDATION);
		
		Risk risk = new Risk();
		response.setRisk(risk);
		
		PaymentSetupResponseInitiation initiation = new PaymentSetupResponseInitiation();
		data.setInitiation(initiation);
		
		initiation.setInstructionIdentification("ABDDC");
		initiation.setEndToEndIdentification("FRESCO.21302.GFX.28");
		
		PaymentSetupInitiationInstructedAmount instructedAmount = new PaymentSetupInitiationInstructedAmount();
		instructedAmount.setAmount("777777777.00");
		instructedAmount.setCurrency("EUR");
		initiation.setInstructedAmount(instructedAmount);
		
		CreditorAgent creditorAgent = new CreditorAgent();
		creditorAgent.setSchemeName(SchemeNameEnum.BICFI);
		creditorAgent.setIdentification("SC080801");
		initiation.setCreditorAgent(creditorAgent);
		
		CreditorAccount creditorAccount = new CreditorAccount();
		creditorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.IBAN);
		creditorAccount.setIdentification("NWBK60161331926819");
		creditorAccount.setName("Test user");
		creditorAccount.setSecondaryIdentification("0002");
		initiation.setCreditorAccount(creditorAccount);
		
		DebtorAgent debtorAgent = new DebtorAgent();
		debtorAgent.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAgent.SchemeNameEnum.BICFI);
		debtorAgent.setIdentification("SC112800");
		initiation.setDebtorAgent(debtorAgent);
		
		DebtorAccount debtorAccount = new DebtorAccount();
		debtorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.IBAN);
		debtorAccount.setIdentification("GB29NWBK60161331926819");
		debtorAccount.setName("Andrea Smith");
		debtorAccount.setSecondaryIdentification("0002");
		initiation.setDebtorAccount(debtorAccount);
		
		RemittanceInformation remittanceInformation = new RemittanceInformation();
		remittanceInformation.setReference("FRESCO-101");
		remittanceInformation.setUnstructured("Internal ops code 5120101");
		initiation.setRemittanceInformation(remittanceInformation);
		
		risk.setMerchantCategoryCode("5967");
		risk.setMerchantCustomerIdentification("053598653254");
		risk.setPaymentContextCode(PaymentContextCodeEnum.ECOMMERCEGOODS);
		
		RiskDeliveryAddress deliveryAddress = new RiskDeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		
		List<String> addressLine = new ArrayList<>();
		addressLine.add("Flat 7");
		addressLine.add("Acacia Lodge");		
		deliveryAddress.setAddressLine(addressLine);
		
		deliveryAddress.setBuildingNumber("27");
		deliveryAddress.setCountry("UK");
		
		List<String> countrySubDivision = new ArrayList<>();
		countrySubDivision.add("ABCDABCDABCDABCDAB");
		countrySubDivision.add("DEFG");
		deliveryAddress.setCountrySubDivision(countrySubDivision);
		
		deliveryAddress.setPostCode("GU31 2ZZ");
		deliveryAddress.setStreetName("AcaciaAvenue");
		deliveryAddress.setTownName("Sparsholt");
		
		response.setLinks(new PaymentSetupPOSTResponseLinks());
		response.setMeta(new PaymentSetupPOSTResponseMeta());
		Mockito.when(paymentSetupStagingRoutingAdapter.updateStagedPaymentSetup(anyObject(), anyObject())).thenReturn(setupStagingResponse);
		setupStagingResponse = adapter.updateStagedPaymentSetup(response);
		assertNotNull(setupStagingResponse);
	}
	
	@Test
	public void updatePaymentSetupStatusTest() {
		PaymentSetupPlatformResource paymentSetupPlatformResource = new PaymentSetupPlatformResource();
//		PaymentSetupPOSTResponse paymentSetupPOSTResponse = new PaymentSetupPOSTResponse();
		CustomPaymentSetupPOSTResponse response = new CustomPaymentSetupPOSTResponse();
		PaymentSetupResponse data = new PaymentSetupResponse();
		response.setData(data);
		data.setPaymentId("12345");
		data.setCreationDateTime(PispUtilities.getCurrentDateInISOFormat());
		data.setStatus(StatusEnum.ACCEPTEDTECHNICALVALIDATION);
		
		
		
		response.setLinks(new PaymentSetupPOSTResponseLinks());
		response.setMeta(new PaymentSetupPOSTResponseMeta());
		Mockito.when(paymentPlatformAdapter.retrievePaymentSetupResource(anyObject())).thenReturn(paymentSetupPlatformResource);
		Mockito.doNothing().when(paymentPlatformAdapter).updatePaymentSetupResource(anyObject());
		Mockito.when(paymentSetupStagingRoutingAdapter.retrieveStagedPaymentSetup(anyObject(), anyObject())).thenReturn(response);
		
		adapter.updatePaymentSetupStatus("123", StatusEnum.PENDING, new HashMap<>());
		/*
		 *  throws 
		 */
	}
	
	@Test(expected = PSD2Exception.class)
	public void updatePaymentSetupDebtorDetailsTest() {
		
		CustomPaymentSetupPOSTResponse response = new CustomPaymentSetupPOSTResponse();
		PaymentSetupResponse data = new PaymentSetupResponse();
		response.setData(data);
		data.setPaymentId("12345");
		data.setCreationDateTime(PispUtilities.getCurrentDateInISOFormat());
		data.setStatus(StatusEnum.ACCEPTEDTECHNICALVALIDATION);
		
		Risk risk = new Risk();
		response.setRisk(risk);
		
		PaymentSetupResponseInitiation initiation = new PaymentSetupResponseInitiation();
		data.setInitiation(initiation);
		
		initiation.setInstructionIdentification("ABDDC");
		initiation.setEndToEndIdentification("FRESCO.21302.GFX.28");
		
		PaymentSetupInitiationInstructedAmount instructedAmount = new PaymentSetupInitiationInstructedAmount();
		instructedAmount.setAmount("777777777.00");
		instructedAmount.setCurrency("EUR");
		initiation.setInstructedAmount(instructedAmount);
		
		CreditorAgent creditorAgent = new CreditorAgent();
		creditorAgent.setSchemeName(SchemeNameEnum.BICFI);
		creditorAgent.setIdentification("SC080801");
		initiation.setCreditorAgent(creditorAgent);
		
		CreditorAccount creditorAccount = new CreditorAccount();
		creditorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.IBAN);
		creditorAccount.setIdentification("NWBK60161331926819");
		creditorAccount.setName("Test user");
		creditorAccount.setSecondaryIdentification("0002");
		initiation.setCreditorAccount(creditorAccount);
		
		DebtorAgent debtorAgent = new DebtorAgent();
		debtorAgent.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAgent.SchemeNameEnum.BICFI);
		debtorAgent.setIdentification("SC112800");
		initiation.setDebtorAgent(debtorAgent);
		
		DebtorAccount debtorAccount = new DebtorAccount();
		debtorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.IBAN);
		debtorAccount.setIdentification("GB29NWBK60161331926819");
		debtorAccount.setName("Andrea Smith");
		debtorAccount.setSecondaryIdentification("0002");
		initiation.setDebtorAccount(debtorAccount);
		
		RemittanceInformation remittanceInformation = new RemittanceInformation();
		remittanceInformation.setReference("FRESCO-101");
		remittanceInformation.setUnstructured("Internal ops code 5120101");
		initiation.setRemittanceInformation(remittanceInformation);
		
		risk.setMerchantCategoryCode("5967");
		risk.setMerchantCustomerIdentification("053598653254");
		risk.setPaymentContextCode(PaymentContextCodeEnum.ECOMMERCEGOODS);
		
		RiskDeliveryAddress deliveryAddress = new RiskDeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		
		List<String> addressLine = new ArrayList<>();
		addressLine.add("Flat 7");
		addressLine.add("Acacia Lodge");		
		deliveryAddress.setAddressLine(addressLine);
		
		deliveryAddress.setBuildingNumber("27");
		deliveryAddress.setCountry("UK");
		
		List<String> countrySubDivision = new ArrayList<>();
		countrySubDivision.add("ABCDABCDABCDABCDAB");
		countrySubDivision.add("DEFG");
		deliveryAddress.setCountrySubDivision(countrySubDivision);
		
		deliveryAddress.setPostCode("GU31 2ZZ");
		deliveryAddress.setStreetName("AcaciaAvenue");
		deliveryAddress.setTownName("Sparsholt");
		
		response.setLinks(new PaymentSetupPOSTResponseLinks());
		response.setMeta(new PaymentSetupPOSTResponseMeta());
		
		
		
		PaymentSetupStagingResponse setupStagingResponse = new PaymentSetupStagingResponse();
		Data2Servicer accountGETResponseServicer = new Data2Servicer();
		accountGETResponseServicer.setIdentification("test");
		accountGETResponseServicer.setSchemeName(com.capgemini.psd2.aisp.domain.Data2Servicer.SchemeNameEnum.BICFI);
		Data2Account accountGETResponseAccount = new Data2Account();
		accountGETResponseAccount.setIdentification("test");
		accountGETResponseAccount.setSchemeName(com.capgemini.psd2.aisp.domain.Data2Account.SchemeNameEnum.IBAN);
		accountGETResponseAccount.setName("test");
		accountGETResponseAccount.setSecondaryIdentification("test1");
		PSD2Account account = new PSD2Account();
		account.setAccount(accountGETResponseAccount);
		account.setServicer(accountGETResponseServicer);
		account.setAdditionalInformation(new HashMap<String,String>());
		
		PaymentSetupPlatformResource paymentSetupPlatformResource = new PaymentSetupPlatformResource();
		
		Mockito.when(paymentPlatformAdapter.retrievePaymentSetupResource(anyObject())).thenReturn(paymentSetupPlatformResource);
		Mockito.doNothing().when(paymentPlatformAdapter).updatePaymentSetupResource(anyObject());
		Mockito.when(paymentSetupStagingRoutingAdapter.retrieveStagedPaymentSetup(anyObject(), anyObject())).thenReturn(response);
		Mockito.when(paymentSetupStagingRoutingAdapter.updateStagedPaymentSetup(anyObject(), anyObject())).thenReturn(setupStagingResponse);
		
		adapter.updatePaymentSetupDebtorDetails(account, "123");
		/*
		 * Throws INVALID_ACCOUNT_DATA from populateAdditionalPaymentDetails()
		 */
		account.setServicer(null);
		adapter.updatePaymentSetupDebtorDetails(account, "123");
	}
	
	@Test(expected = PSD2Exception.class)
	public void updatePaymentSetupDebtorDetailsTest2(){
		CustomPaymentSetupPOSTResponse response = new CustomPaymentSetupPOSTResponse();
		PaymentSetupResponse data = new PaymentSetupResponse();
		response.setData(data);
		data.setPaymentId("12345");
		data.setCreationDateTime(PispUtilities.getCurrentDateInISOFormat());
		data.setStatus(StatusEnum.ACCEPTEDTECHNICALVALIDATION);
		
		Risk risk = new Risk();
		response.setRisk(risk);
		
		PaymentSetupResponseInitiation initiation = new PaymentSetupResponseInitiation();
		data.setInitiation(initiation);
		
		initiation.setInstructionIdentification("ABDDC");
		initiation.setEndToEndIdentification("FRESCO.21302.GFX.28");
		
		PaymentSetupInitiationInstructedAmount instructedAmount = new PaymentSetupInitiationInstructedAmount();
		instructedAmount.setAmount("777777777.00");
		instructedAmount.setCurrency("EUR");
		initiation.setInstructedAmount(instructedAmount);
		
		CreditorAgent creditorAgent = new CreditorAgent();
		creditorAgent.setSchemeName(SchemeNameEnum.BICFI);
		creditorAgent.setIdentification("SC080801");
		initiation.setCreditorAgent(creditorAgent);
		
		CreditorAccount creditorAccount = new CreditorAccount();
		creditorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.IBAN);
		creditorAccount.setIdentification("NWBK60161331926819");
		creditorAccount.setName("Test user");
		creditorAccount.setSecondaryIdentification("0002");
		initiation.setCreditorAccount(creditorAccount);
		
		DebtorAgent debtorAgent = new DebtorAgent();
		debtorAgent.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAgent.SchemeNameEnum.BICFI);
		debtorAgent.setIdentification("SC112800");
		initiation.setDebtorAgent(debtorAgent);
		
		DebtorAccount debtorAccount = new DebtorAccount();
		debtorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.IBAN);
		debtorAccount.setIdentification("GB29NWBK60161331926819");
		debtorAccount.setName("Andrea Smith");
		debtorAccount.setSecondaryIdentification("0002");
		initiation.setDebtorAccount(debtorAccount);
		
		RemittanceInformation remittanceInformation = new RemittanceInformation();
		remittanceInformation.setReference("FRESCO-101");
		remittanceInformation.setUnstructured("Internal ops code 5120101");
		initiation.setRemittanceInformation(remittanceInformation);
		
		risk.setMerchantCategoryCode("5967");
		risk.setMerchantCustomerIdentification("053598653254");
		risk.setPaymentContextCode(PaymentContextCodeEnum.ECOMMERCEGOODS);
		
		RiskDeliveryAddress deliveryAddress = new RiskDeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		
		List<String> addressLine = new ArrayList<>();
		addressLine.add("Flat 7");
		addressLine.add("Acacia Lodge");		
		deliveryAddress.setAddressLine(addressLine);
		
		deliveryAddress.setBuildingNumber("27");
		deliveryAddress.setCountry("UK");
		
		List<String> countrySubDivision = new ArrayList<>();
		countrySubDivision.add("ABCDABCDABCDABCDAB");
		countrySubDivision.add("DEFG");
		deliveryAddress.setCountrySubDivision(countrySubDivision);
		
		deliveryAddress.setPostCode("GU31 2ZZ");
		deliveryAddress.setStreetName("AcaciaAvenue");
		deliveryAddress.setTownName("Sparsholt");
		
		response.setLinks(new PaymentSetupPOSTResponseLinks());
		response.setMeta(new PaymentSetupPOSTResponseMeta());
		
		
		
		PaymentSetupStagingResponse setupStagingResponse = new PaymentSetupStagingResponse();
		Data2Servicer accountGETResponseServicer = new Data2Servicer();
		accountGETResponseServicer.setIdentification("test");
		accountGETResponseServicer.setSchemeName(com.capgemini.psd2.aisp.domain.Data2Servicer.SchemeNameEnum.BICFI);
		Data2Account accountGETResponseAccount = new Data2Account();
		accountGETResponseAccount.setIdentification("abcdefabcrftgy");
		accountGETResponseAccount.setSchemeName(com.capgemini.psd2.aisp.domain.Data2Account.SchemeNameEnum.SORTCODEACCOUNTNUMBER);
		accountGETResponseAccount.setName("test");
		accountGETResponseAccount.setSecondaryIdentification("test1");
		PSD2Account account = new PSD2Account();
		account.setAccount(accountGETResponseAccount);
		account.setServicer(accountGETResponseServicer);
		account.setAdditionalInformation(new HashMap<String,String>());
		
		PaymentSetupPlatformResource paymentSetupPlatformResource = new PaymentSetupPlatformResource();
		
		Mockito.when(paymentPlatformAdapter.retrievePaymentSetupResource(anyObject())).thenReturn(paymentSetupPlatformResource);
		Mockito.doNothing().when(paymentPlatformAdapter).updatePaymentSetupResource(anyObject());
		Mockito.when(paymentSetupStagingRoutingAdapter.retrieveStagedPaymentSetup(anyObject(), anyObject())).thenReturn(response);
		Mockito.when(paymentSetupStagingRoutingAdapter.updateStagedPaymentSetup(anyObject(), anyObject())).thenReturn(setupStagingResponse);
		
		adapter.updatePaymentSetupDebtorDetails(account, "123");
		/*
		 * throws PSD2Exception from populateAdditionalPaymentDetails
		 */
		accountGETResponseAccount.setIdentification("test");
		adapter.updatePaymentSetupDebtorDetails(account, "123");
	}
	
	@Test
	public void preAuthorizationValidationTest() {
		CustomPaymentSetupPOSTResponse response = new CustomPaymentSetupPOSTResponse();
		PaymentSetupResponse data = new PaymentSetupResponse();
		response.setData(data);
		data.setPaymentId("12345");
		data.setCreationDateTime(PispUtilities.getCurrentDateInISOFormat());
		data.setStatus(StatusEnum.ACCEPTEDTECHNICALVALIDATION);
		
		Risk risk = new Risk();
		response.setRisk(risk);
		
		PaymentSetupResponseInitiation initiation = new PaymentSetupResponseInitiation();
		data.setInitiation(initiation);
		
		initiation.setInstructionIdentification("ABDDC");
		initiation.setEndToEndIdentification("FRESCO.21302.GFX.28");
		
		PaymentSetupInitiationInstructedAmount instructedAmount = new PaymentSetupInitiationInstructedAmount();
		instructedAmount.setAmount("777777777.00");
		instructedAmount.setCurrency("EUR");
		initiation.setInstructedAmount(instructedAmount);
		
		CreditorAgent creditorAgent = new CreditorAgent();
		creditorAgent.setSchemeName(SchemeNameEnum.BICFI);
		creditorAgent.setIdentification("SC080801");
		initiation.setCreditorAgent(creditorAgent);
		
		CreditorAccount creditorAccount = new CreditorAccount();
		creditorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.IBAN);
		creditorAccount.setIdentification("NWBK60161331926819");
		creditorAccount.setName("Test user");
		creditorAccount.setSecondaryIdentification("0002");
		initiation.setCreditorAccount(creditorAccount);
		
		DebtorAgent debtorAgent = new DebtorAgent();
		debtorAgent.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAgent.SchemeNameEnum.BICFI);
		debtorAgent.setIdentification("SC112800");
		initiation.setDebtorAgent(debtorAgent);
		
		DebtorAccount debtorAccount = new DebtorAccount();
		debtorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.IBAN);
		debtorAccount.setIdentification("GB29NWBK60161331926819");
		debtorAccount.setName("Andrea Smith");
		debtorAccount.setSecondaryIdentification("0002");
		initiation.setDebtorAccount(debtorAccount);
		
		RemittanceInformation remittanceInformation = new RemittanceInformation();
		remittanceInformation.setReference("FRESCO-101");
		remittanceInformation.setUnstructured("Internal ops code 5120101");
		initiation.setRemittanceInformation(remittanceInformation);
		
		risk.setMerchantCategoryCode("5967");
		risk.setMerchantCustomerIdentification("053598653254");
		risk.setPaymentContextCode(PaymentContextCodeEnum.ECOMMERCEGOODS);
		
		RiskDeliveryAddress deliveryAddress = new RiskDeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		
		List<String> addressLine = new ArrayList<>();
		addressLine.add("Flat 7");
		addressLine.add("Acacia Lodge");		
		deliveryAddress.setAddressLine(addressLine);
		
		deliveryAddress.setBuildingNumber("27");
		deliveryAddress.setCountry("UK");
		
		List<String> countrySubDivision = new ArrayList<>();
		countrySubDivision.add("ABCDABCDABCDABCDAB");
		countrySubDivision.add("DEFG");
		deliveryAddress.setCountrySubDivision(countrySubDivision);
		
		deliveryAddress.setPostCode("GU31 2ZZ");
		deliveryAddress.setStreetName("AcaciaAvenue");
		deliveryAddress.setTownName("Sparsholt");
		
		response.setLinks(new PaymentSetupPOSTResponseLinks());
		response.setMeta(new PaymentSetupPOSTResponseMeta());
		
		
		
		PaymentSetupStagingResponse setupStagingResponse = new PaymentSetupStagingResponse();
		Data2Servicer accountGETResponseServicer = new Data2Servicer();
		accountGETResponseServicer.setIdentification("test");
		accountGETResponseServicer.setSchemeName(com.capgemini.psd2.aisp.domain.Data2Servicer.SchemeNameEnum.BICFI);
		Data2Account accountGETResponseAccount = new Data2Account();
		accountGETResponseAccount.setIdentification("test");
		accountGETResponseAccount.setSchemeName(com.capgemini.psd2.aisp.domain.Data2Account.SchemeNameEnum.IBAN);
		accountGETResponseAccount.setName("test");
		accountGETResponseAccount.setSecondaryIdentification("test1");
		PSD2Account account = new PSD2Account();
		account.setAccount(accountGETResponseAccount);
		account.setServicer(accountGETResponseServicer);
		account.setAdditionalInformation(new HashMap<String,String>());
		PaymentSetupValidationResponse paymentSetupValidationResponse = new PaymentSetupValidationResponse();
		paymentSetupValidationResponse.setPaymentSetupValidationStatus("REjected");
		
		Mockito.when(paymentSetupStagingRoutingAdapter.retrieveStagedPaymentSetup(anyObject(), anyObject())).thenReturn(response);
		Mockito.when(reqHeaderAtrributes.getCorrelationId()).thenReturn("test");
		Mockito.when(authorizationValidationRoutingAdapter.preAuthorizationPaymentValidation(anyObject(), anyObject())).thenReturn(paymentSetupValidationResponse);
		
		paymentSetupValidationResponse = adapter.preAuthorizationValidation(account, "123", new HashMap<>());
		assertNotNull(paymentSetupValidationResponse);
	}
	
	@Test
	public void populateFraudResponseTest(){
		CustomPaymentSetupPOSTResponse paymentSetupResponse = new CustomPaymentSetupPOSTResponse();
		PSD2Account account = new PSD2Account();
		Object fraudResponse = new Object();
		account.setFraudResponse(fraudResponse);
		adapter.populateFraudResponse(paymentSetupResponse, account);
	}
}
