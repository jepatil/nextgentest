package com.capgemini.psd2.pisp.payment.setup.routing.adapter.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupStagingResponse;
import com.capgemini.psd2.pisp.adapter.PaymentSetupStagingAdapter;
import com.capgemini.psd2.pisp.payment.setup.routing.adapter.routing.PaymentSetupAdapterFactory;

public class PaymentSetupStagingRoutingAdapterImpl implements PaymentSetupStagingAdapter{
	
	/** The factory. */
	@Autowired
	private PaymentSetupAdapterFactory factory;

	/** The default adapter. */
	@Value("${app.paymentSetupStagingAdapter}")
	private String paymentSetupStagingAdapter;

	@Override
	public CustomPaymentSetupPOSTResponse retrieveStagedPaymentSetup(String paymentId, Map<String, String> params) {
		return getRoutingAdapter().retrieveStagedPaymentSetup(paymentId,params);
	}

	@Override
	public PaymentSetupStagingResponse createStagingPaymentSetup(CustomPaymentSetupPOSTRequest paymentSetupRequest,
			Map<String, String> params) {
		return getRoutingAdapter().createStagingPaymentSetup(paymentSetupRequest, params);
	}

	@Override
	public PaymentSetupStagingResponse updateStagedPaymentSetup(CustomPaymentSetupPOSTResponse paymentSetupBankResource,
			Map<String, String> params) {
		return getRoutingAdapter().updateStagedPaymentSetup(paymentSetupBankResource, params);	
	}

	private PaymentSetupStagingAdapter getRoutingAdapter(){
		return factory.getPaymentSetupStagingInstance(paymentSetupStagingAdapter);
	}
}
