
package com.capgemini.psd2.pisp.payment.setup.routing.adapter.routing;

import com.capgemini.psd2.pisp.adapter.PaymentSetupValidationAdapter;
import com.capgemini.psd2.pisp.adapter.PaymentAuthorizationValidationAdapter;
import com.capgemini.psd2.pisp.adapter.PaymentSetupStagingAdapter;


public interface PaymentSetupAdapterFactory {
	

	
	public PaymentSetupStagingAdapter getPaymentSetupStagingInstance(String coreSystemName);
	
	public PaymentSetupValidationAdapter getPaymentSetupValidationInstance(String coreSystemName);
	
	public PaymentAuthorizationValidationAdapter getPaymentAuthorizationValidationInstance(String coreSystemName);

}
