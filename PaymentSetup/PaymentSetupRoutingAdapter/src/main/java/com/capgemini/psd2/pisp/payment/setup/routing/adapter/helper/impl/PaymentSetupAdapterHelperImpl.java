package com.capgemini.psd2.pisp.payment.setup.routing.adapter.helper.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.domain.Account;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.PaymentAuthorizationValidationAdapter;
import com.capgemini.psd2.pisp.adapter.PaymentSetupAdapterHelper;
import com.capgemini.psd2.pisp.adapter.PaymentSetupPlatformAdapter;
import com.capgemini.psd2.pisp.adapter.PaymentSetupStagingAdapter;
import com.capgemini.psd2.pisp.adapter.PaymentSetupValidationAdapter;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.DebtorAccount;
import com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum;
import com.capgemini.psd2.pisp.domain.DebtorAgent;
import com.capgemini.psd2.pisp.domain.PaymentResponseInfo;
import com.capgemini.psd2.pisp.domain.PaymentSetupPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse.StatusEnum;
import com.capgemini.psd2.pisp.domain.PaymentSetupStagingResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupValidationResponse;
import com.capgemini.psd2.pisp.status.PaymentStatusEnum;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class PaymentSetupAdapterHelperImpl implements PaymentSetupAdapterHelper {
	/** The adapter. */
	@Autowired
	@Qualifier("paymentSetupStagingRoutingAdapter")
	private PaymentSetupStagingAdapter paymentSetupStagingRoutingAdapter;

	@Autowired
	@Qualifier("paymentSetupValidationRoutingAdapter")
	private PaymentSetupValidationAdapter paymentSetupValidationRoutingAdapter;

	@Autowired
	@Qualifier("paymentSetupValidationRoutingAdapter")
	private PaymentAuthorizationValidationAdapter authorizationValidationRoutingAdapter;

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Autowired
	private PaymentSetupPlatformAdapter paymentPlatformAdapter;

	@Override
	public CustomPaymentSetupPOSTResponse retrieveStagedPaymentSetup(String paymentId) {

		Map<String, String> mapParam = getHeaderParams();
		return paymentSetupStagingRoutingAdapter.retrieveStagedPaymentSetup(paymentId, mapParam);
	}

	@Override
	public PaymentResponseInfo validatePreStagePaymentSetup(CustomPaymentSetupPOSTRequest paymentSetupRequest) {
		Map<String, String> mapParam = getHeaderParams();
		PaymentSetupValidationResponse paymentSetupValidationStatus = paymentSetupValidationRoutingAdapter
				.validatePreStagePaymentSetup(paymentSetupRequest, mapParam);

		String idempotencyRequest = String.valueOf(Boolean.TRUE);
		String preStagePaymentStatus = paymentSetupValidationStatus.getPaymentSetupValidationStatus();
		PaymentResponseInfo params = new PaymentResponseInfo();
		if (PaymentStatusEnum.REJECTED.getStatusCode().equalsIgnoreCase(preStagePaymentStatus)) {
			idempotencyRequest = String.valueOf(Boolean.FALSE);
		}
		params.setPaymentValidationStatus(preStagePaymentStatus);
		params.setIdempotencyRequest(idempotencyRequest);
		return params;
	}

	@Override
	public PaymentSetupStagingResponse createStagingPaymentSetup(CustomPaymentSetupPOSTRequest paymentSetupRequest) {
		Map<String, String> mapParam = getHeaderParams();
		return paymentSetupStagingRoutingAdapter.createStagingPaymentSetup(paymentSetupRequest, mapParam);
	}

	@Override
	public PaymentSetupStagingResponse updateStagedPaymentSetup(
			CustomPaymentSetupPOSTResponse paymentSetupBankResource) {
		Map<String, String> mapParam = getHeaderParams();
		if (paymentSetupBankResource.getData().getStatus() != null && paymentSetupBankResource.getData().getStatus().toString() != null
				&& paymentSetupBankResource.getPaymentStatus() == null)
			paymentSetupBankResource.setPaymentStatus(paymentSetupBankResource.getData().getStatus().toString());

		return paymentSetupStagingRoutingAdapter.updateStagedPaymentSetup(paymentSetupBankResource, mapParam);
	}

	/*
	 * Method will be called by Consent application
	 */
	@Override
	public void updatePaymentSetupStatus(String paymentId, StatusEnum paymentSetupStatus,
			Map<String, String> paramsMap) {

		PaymentSetupPlatformResource paymentLocalResource = paymentPlatformAdapter
				.retrievePaymentSetupResource(paymentId);
		paymentLocalResource.setStatus(paymentSetupStatus.toString());
		paymentPlatformAdapter.updatePaymentSetupResource(paymentLocalResource);

		CustomPaymentSetupPOSTResponse paymentSetupResponse = retrieveStagedPaymentSetup(paymentId);
		paymentSetupResponse.getData().setStatus(paymentSetupStatus);
		paymentSetupResponse.setPaymentStatus(paymentSetupStatus.toString());
		updateStagedPaymentSetup(paymentSetupResponse);
	}

	/*
	 * Method will be called by Consent application
	 */
	@Override
	public void updatePaymentSetupDebtorDetails(Account account, String paymentId) {
		CustomPaymentSetupPOSTResponse paymentSetupResponse = retrieveStagedPaymentSetup(paymentId);
		populateAdditionalPaymentDetails(paymentSetupResponse, account);
		populateFraudResponse(paymentSetupResponse, account);
		/**
		 * While retrieving staging record Api Platform recieved PaymentStatus as "Null" .
		 * Hence platform has set last status 'AcceptedTechincalValidation' to avoid 
		 * any discrepancy.
		 */
		paymentSetupResponse.setPaymentStatus(PaymentStatusEnum.ACCEPTEDTECHNICALVALIDATION.getStatusCode());
		updateStagedPaymentSetup(paymentSetupResponse);
	}

	public void populateFraudResponse(CustomPaymentSetupPOSTResponse paymentSetupResponse, Account account) {
			if (!NullCheckUtils.isNullOrEmpty(((PSD2Account) account).getFraudResponse())) {
				paymentSetupResponse.setFraudnetResponse(((PSD2Account) account).getFraudResponse());
		 }
	}

	/*
	 * Method will be called by Consent application
	 */
	@Override
	public PaymentSetupValidationResponse preAuthorizationValidation(Account account, String paymentId,
			Map<String, String> mapParam) {

		CustomPaymentSetupPOSTResponse paymentSetupResponse = retrieveStagedPaymentSetup(paymentId);
		populateAdditionalPaymentDetails(paymentSetupResponse, account);

		mapParam.put(PSD2Constants.CORRELATION_REQ_HEADER, reqHeaderAtrributes.getCorrelationId());
		PaymentSetupValidationResponse validationResponse = authorizationValidationRoutingAdapter
				.preAuthorizationPaymentValidation(paymentSetupResponse, mapParam);
		validationResponse
				.setPaymentSetupValidationStatus(validationResponse.getPaymentSetupValidationStatus().toUpperCase());
		return validationResponse;
	}

	private CustomPaymentSetupPOSTResponse populateAdditionalPaymentDetails(
			CustomPaymentSetupPOSTResponse paymentSetupResponse, Account account) {

		PSD2Account psd2Account = (PSD2Account) account;

		DebtorAccount debtorAccount = null;
		DebtorAgent debtorAgent = null;
		if (com.capgemini.psd2.aisp.domain.Data2Account.SchemeNameEnum.IBAN
				.equals(psd2Account.getAccount().getSchemeName())) {
			if (psd2Account.getServicer() != null && psd2Account.getServicer().getIdentification() != null) {
				debtorAccount = new DebtorAccount();
				debtorAgent = new DebtorAgent();
				debtorAccount.setIdentification(psd2Account.getAccount().getIdentification());
				debtorAccount
						.setSchemeName(SchemeNameEnum.fromValue(psd2Account.getAccount().getSchemeName().toString()));
				debtorAgent.setIdentification(psd2Account.getServicer().getIdentification());
				debtorAgent.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAgent.SchemeNameEnum
						.fromValue(psd2Account.getServicer().getSchemeName().toString()));
				paymentSetupResponse.getData().getInitiation().setDebtorAgent(debtorAgent);
			} else {
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.INVALID_ACCOUNT_DATA);
			}
		} else {
			if (psd2Account.getAccount().getIdentification().length() == 14) {
				debtorAccount = new DebtorAccount();
				debtorAccount.setIdentification(psd2Account.getAccount().getIdentification());
				debtorAccount
						.setSchemeName(SchemeNameEnum.fromValue(psd2Account.getAccount().getSchemeName().toString()));
			} else {
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.INVALID_ACCOUNT_DATA);
			}
		}

		/*
		 * If DebtorAccount.Name is not sent by TPP then product will set the
		 * name from PSD2Account.NickName else will set from PSD2.Account.Name
		 * to adapter and it would have same value as sent by TPP during payment
		 * setup. Name is mandatory parameter for FS call hence set it.
		 */
		String debtorAccountName = psd2Account.getNickname();
		if (!NullCheckUtils.isNullOrEmpty(psd2Account.getAccount().getName()))
			debtorAccountName = psd2Account.getAccount().getName();

		debtorAccount.setName(debtorAccountName);
		debtorAccount.setSecondaryIdentification(psd2Account.getAccount().getSecondaryIdentification());
		paymentSetupResponse.getData().getInitiation().setDebtorAccount(debtorAccount);
		paymentSetupResponse.setPayerCurrency(psd2Account.getCurrency());
		paymentSetupResponse
				.setPayerJurisdiction(psd2Account.getAdditionalInformation().get(PSD2Constants.PAYER_JURISDICTION));
		paymentSetupResponse.setAccountNumber(psd2Account.getAdditionalInformation().get(PSD2Constants.ACCOUNT_NUMBER));
		paymentSetupResponse.setAccountNSC(psd2Account.getAdditionalInformation().get(PSD2Constants.ACCOUNT_NSC));
		paymentSetupResponse.setAccountIban(psd2Account.getAdditionalInformation().get(PSD2Constants.IBAN));
		paymentSetupResponse.setAccountBic(psd2Account.getAdditionalInformation().get(PSD2Constants.BIC));
		return paymentSetupResponse;
	}

	private Map<String, String> getHeaderParams() {

		Map<String, String> mapParam = new HashMap<>();

		/*
		 * Below code will be used when update service will be called from
		 * payment submission API. For submission flow -> update, PSUId and
		 * ChannelName are mandatory fields for FS
		 */
		if (null != reqHeaderAtrributes.getToken()
				&& null != reqHeaderAtrributes.getToken().getSeviceParams()) {
			mapParam.put(PSD2Constants.CHANNEL_IN_REQ_HEADER,
					reqHeaderAtrributes.getToken().getSeviceParams().get(PSD2Constants.CHANNEL_NAME));
		}
		mapParam.put(PSD2Constants.USER_IN_REQ_HEADER, reqHeaderAtrributes.getPsuId());

		// End of code change

		mapParam.put(PSD2Constants.CORRELATION_REQ_HEADER, reqHeaderAtrributes.getCorrelationId());

		return mapParam;
	}

}
