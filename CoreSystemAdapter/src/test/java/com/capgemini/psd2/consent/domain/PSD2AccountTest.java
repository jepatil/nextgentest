package com.capgemini.psd2.consent.domain;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class PSD2AccountTest {

	@Test
	public void testGetter(){
		PSD2Account account = new PSD2Account();
		account.setAccountType("Savings");
		account.setAccountId("1022675");
		Map<String, String> additionalInformation = new HashMap<>();
		additionalInformation.put("consentId", "123456");
		account.setAdditionalInformation(additionalInformation );
		assertEquals("Savings", account.getAccountType());
		assertEquals("1022675", account.getAccountId());
		assertEquals("123456", account.getAdditionalInformation().get("consentId"));
	}
}
