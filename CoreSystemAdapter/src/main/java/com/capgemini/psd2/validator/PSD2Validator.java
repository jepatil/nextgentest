/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.validator;

/**
 * The Interface PSD2Validator.
 */
public interface PSD2Validator {
	
	/**
	 * Validate.
	 *
	 * @param input the input
	 */
	public void validate(Object[] input);
	
	/**
	 * Validate.
	 *
	 * @param input the input
	 */
	public void validate(Object input);
	
	/**
	 * Validate.
	 *
	 * @param input the input
	 */
	public <T extends Enum<T>> void validateEnum(Class<T> enumerator, String value);
}
