/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.consent.domain;

import java.util.List;

import org.springframework.data.annotation.Id;

import com.capgemini.psd2.enums.ConsentStatusEnum;

/**
 * The Class Consent.
 */
public class AispConsent {

	/** The consent id. */
	@Id
	private String consentId;
	
	/** The psu id. */
	private String psuId;
	
	/** The tpp CID. */
	private String tppCId;
	
	/** The account details. */
	private List<AccountDetails> accountDetails;
	
	private String startDate;
	
	private String endDate;
	
	private ConsentStatusEnum status;
	
	private String accountRequestId;
	
    private String transactionFromDateTime;

	private String transactionToDateTime;
	
	private String tppApplicationName;
	
	private String tppLegalEntityName;
	
	private String channelId;

	/**
	 * Gets the consent id.
	 *
	 * @return the consent id
	 */
	public String getConsentId() {
		return consentId;
	}

	/**
	 * Sets the consent id.
	 *
	 * @param consentId the new consent id
	 */
	public void setConsentId(String consentId) {
		this.consentId = consentId;
	}

	/**
	 * Gets the psu id.
	 *
	 * @return the psu id
	 */
	public String getPsuId() {
		return psuId;
	}

	/**
	 * Sets the psu id.
	 *
	 * @param psuId the new psu id
	 */
	public void setPsuId(String psuId) {
		this.psuId = psuId;
	}

	/**
	 * Gets the account details.
	 *
	 * @return the account details
	 */
	public List<AccountDetails> getAccountDetails() {
		return accountDetails;
	}

	/**
	 * Sets the account details.
	 *
	 * @param accountDetails the new account details
	 */
	public void setAccountDetails(List<AccountDetails> accountDetails) {
		this.accountDetails = accountDetails;
	}
	
	

	public String getTppCId() {
		return tppCId;
	}

	public void setTppCId(String tppCId) {
		this.tppCId = tppCId;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	
	public ConsentStatusEnum getStatus() {
		return status;
	}

	public void setStatus(ConsentStatusEnum status) {
		this.status = status;
	}

	public String getAccountRequestId() {
		return accountRequestId;
	}

	public void setAccountRequestId(String accountRequestId) {
		this.accountRequestId = accountRequestId;
	}

	public String getTransactionFromDateTime() {
		return transactionFromDateTime;
	}

	public void setTransactionFromDateTime(String transactionFromDateTime) {
		this.transactionFromDateTime = transactionFromDateTime;
	}

	public String getTransactionToDateTime() {
		return transactionToDateTime;
	}

	public void setTransactionToDateTime(String transactionToDateTime) {
		this.transactionToDateTime = transactionToDateTime;
	}
	

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public String getTppApplicationName() {
		return tppApplicationName;
	}

	public void setTppApplicationName(String tppApplicationName) {
		this.tppApplicationName = tppApplicationName;
	}

	public String getTppLegalEntityName() {
		return tppLegalEntityName;
	}

	public void setTppLegalEntityName(String tppLegalEntityName) {
		this.tppLegalEntityName = tppLegalEntityName;
	}
	
}
