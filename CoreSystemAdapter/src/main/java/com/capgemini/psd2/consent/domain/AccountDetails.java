/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.consent.domain;

import com.capgemini.psd2.aisp.domain.Data2Account;
import com.capgemini.psd2.aisp.domain.Data2Servicer;

/**
 * The Class AccountDetails.
 */
public class AccountDetails {
	
	/** The account number. */
	private String accountNumber;
	
	/** The account NSC. */
	private String accountNSC;
	
	/** The account id. */
	private String accountId;
	
	/** The nickname. */
	private String nickname;
	
	/** The currency. */
	private String currency;
	
	/** The account type. */
	private String accountType;
	
	private String hashValue;

	 private Data2Account account;
	 
	 private Data2Servicer servicer;

	

	/**
	 * Gets the account number.
	 *
	 * @return the account number
	 */
	public String getAccountNumber() {
		return accountNumber;
	}

	/**
	 * Sets the account number.
	 *
	 * @param accountNumber the new account number
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * Gets the account NSC.
	 *
	 * @return the account NSC
	 */
	public String getAccountNSC() {
		return accountNSC;
	}

	/**
	 * Sets the account NSC.
	 *
	 * @param accountNSC the new account NSC
	 */
	public void setAccountNSC(String accountNSC) {
		this.accountNSC = accountNSC;
	}

	/**
	 * Gets the account id.
	 *
	 * @return the account id
	 */
	public String getAccountId() {
		return accountId;
	}

	/**
	 * Sets the account id.
	 *
	 * @param accountId the new account id
	 */
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getHashValue() {
		return hashValue;
	}

	public void setHashValue(String hashValue) {
		this.hashValue = hashValue;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public Data2Account getAccount() {
		return account;
	}

	public void setAccount(Data2Account account) {
		this.account = account;
	}
	
	public Data2Servicer getServicer() {
		return servicer;
	}

	public void setServicer(Data2Servicer servicer) {
		this.servicer = servicer;
	}	
	
}
