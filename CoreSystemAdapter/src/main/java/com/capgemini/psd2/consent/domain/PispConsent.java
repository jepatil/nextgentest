package com.capgemini.psd2.consent.domain;

import org.springframework.data.annotation.Id;

import com.capgemini.psd2.enums.ConsentStatusEnum;

public class PispConsent {

	/** The consent id. */
	@Id
	private String consentId;
	
	/** The psu id. */
	private String psuId;
	
	/** The tpp CID. */
	private String tppCId;
	
	/** The account details. */
	private AccountDetails accountDetails;
	
	private String startDate;
	
	private ConsentStatusEnum status;
	
	private String paymentId;
	
	private String endDate;

	private String channelId;
	
	private String tppApplicationName;
	
	private String tppLegalEntityName;

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	} 

	public String getConsentId() {
		return consentId;
	}

	public void setConsentId(String consentId) {
		this.consentId = consentId;
	}

	public String getPsuId() {
		return psuId;
	}

	public void setPsuId(String psuId) {
		this.psuId = psuId;
	}

	public String getTppCId() {
		return tppCId;
	}

	public void setTppCId(String tppCId) {
		this.tppCId = tppCId;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public ConsentStatusEnum getStatus() {
		return status;
	}

	public void setStatus(ConsentStatusEnum status) {
		this.status = status;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public AccountDetails getAccountDetails() {
		return accountDetails;
	}

	public void setAccountDetails(AccountDetails accountDetails) {
		this.accountDetails = accountDetails;
	}
	
	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public String getTppApplicationName() {
		return tppApplicationName;
	}

	public void setTppApplicationName(String tppApplicationName) {
		this.tppApplicationName = tppApplicationName;
	}

	public String getTppLegalEntityName() {
		return tppLegalEntityName;
	}

	public void setTppLegalEntityName(String tppLegalEntityName) {
		this.tppLegalEntityName = tppLegalEntityName;
	}
	
}