package com.capgemini.psd2.consent.domain;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

import org.springframework.util.StringUtils;

import com.capgemini.psd2.aisp.domain.Account;
import com.fasterxml.jackson.annotation.JsonProperty;

public class PSD2Account extends Account {

	@JsonProperty("AccountType")
	private String accountType;
	
	@JsonProperty("AdditionalInformation")
	private Map<String,String> additionalInformation;
	
	@JsonProperty("HashedValue")
	private String hashedValue;
	
	private Object fraudResponse;
	
	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	
	public Map<String, String> getAdditionalInformation() {
		return additionalInformation;
	}

	public void setAdditionalInformation(Map<String, String> additionalInformation) {
		this.additionalInformation = additionalInformation;
	}

	public String getHashedValue() {
		return hashedValue;
	}
	
	public void setHashedValue() {
		hashedValue = generateHashedValue(getAccount().getIdentification());
	}

	private String generateHashedValue(String strTobeHashed) {
		if(!StringUtils.hasText(strTobeHashed)){
			return null;
		}
		try {
			MessageDigest md;
			md = MessageDigest.getInstance("SHA-256");
			md.update(strTobeHashed.getBytes());

	        byte byteData[] = md.digest();
	        
	        StringBuffer sb = new StringBuffer();
	        
	        for (byte byteElement : byteData) {
	         sb.append(Integer.toString((byteElement & 0xff) + 0x100, 16).substring(1));
	        }
	        
	        return sb.toString();

		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
        
	}

	public Object getFraudResponse() {
		return fraudResponse;
	}

	public void setFraudResponse(Object fraudResponse) {
		this.fraudResponse = fraudResponse;
	}
}
