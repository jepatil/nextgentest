package com.capgemini.psd2.pisp.domain;


public class PaymentResponseInfo {
  
	
  	private String paymentValidationStatus;
	private String idempotencyRequest;
	private String paymentSubmissionId;
	
	public String getPaymentSubmissionId() {
		return paymentSubmissionId;
	}
	public void setPaymentSubmissionId(String paymentSubmissionId) {
		this.paymentSubmissionId = paymentSubmissionId;
	}
	public String getPaymentValidationStatus() {
		return paymentValidationStatus;
	}
	public void setPaymentValidationStatus(String paymentValidationStatus) {
		this.paymentValidationStatus = paymentValidationStatus;
	}
	public String getIdempotencyRequest() {
		return idempotencyRequest;
	}
	public void setIdempotencyRequest(String idempotencyRequest) {
		this.idempotencyRequest = idempotencyRequest;
	}
	
	
	
	
}
