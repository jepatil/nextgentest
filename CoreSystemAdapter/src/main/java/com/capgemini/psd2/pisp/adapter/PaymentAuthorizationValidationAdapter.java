package com.capgemini.psd2.pisp.adapter;

import java.util.Map;

import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupValidationResponse;

public interface PaymentAuthorizationValidationAdapter {
	public PaymentSetupValidationResponse preAuthorizationPaymentValidation(CustomPaymentSetupPOSTResponse paymentSetupResponse, Map<String, String> params);

}