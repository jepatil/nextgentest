package com.capgemini.psd2.pisp.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import com.capgemini.psd2.pisp.domain.PaymentSubmitPOST201Response;

@Document(collection = "paymentSubmissionFoundationResources")
public class PaymentSubmissionFoundationResource extends PaymentSubmitPOST201Response{
	@Id
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	
}
