package com.capgemini.psd2.pisp.adapter;

import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.PaymentResponseInfo;
import com.capgemini.psd2.pisp.domain.PaymentSetupPlatformResource;

public interface PaymentSetupPlatformAdapter {
	public PaymentSetupPlatformResource createPaymentSetupResource(CustomPaymentSetupPOSTRequest paymentSetupRequest, PaymentResponseInfo params);
	public void updatePaymentSetupResource(PaymentSetupPlatformResource paymentSetupResource);
	public PaymentSetupPlatformResource retrievePaymentSetupResource(String paymentId);	
	public PaymentSetupPlatformResource getIdempotentPaymentSetupResource(long idempotencyDuration);	
}
