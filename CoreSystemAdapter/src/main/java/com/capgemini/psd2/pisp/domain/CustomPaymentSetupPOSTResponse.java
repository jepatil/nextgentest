package com.capgemini.psd2.pisp.domain;

import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponse;

public class CustomPaymentSetupPOSTResponse extends PaymentSetupPOSTResponse{
	
	private String paymentStatus;
	private String payerCurrency;
	private String payerJurisdiction;
	private String accountNumber;
	private String accountNSC;
	private String accountBic;
	private String accountIban;
	private Object fraudnetResponse;

	public String getPayerCurrency() {
		return payerCurrency;
	}

	public void setPayerCurrency(String payerCurrency) {
		this.payerCurrency = payerCurrency;
	}

	public String getPayerJurisdiction() {
		return payerJurisdiction;
	}

	public void setPayerJurisdiction(String payerJurisdiction) {
		this.payerJurisdiction = payerJurisdiction;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountNSC() {
		return accountNSC;
	}

	public void setAccountNSC(String accountNSC) {
		this.accountNSC = accountNSC;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	@Override
	public String toString() {
		return "CustomPaymentSetupPOSTResponse [paymentStatus=" + paymentStatus + ", payerCurrency=" + payerCurrency
				+ ", payerJurisdiction=" + payerJurisdiction + ", accountNumber=" + accountNumber + ", accountNSC="
				+ accountNSC + ", toString()=" + super.toString() +"]";
	}

	public String getAccountBic() {
		return accountBic;
	}

	public void setAccountBic(String accountBic) {
		this.accountBic = accountBic;
	}

	public String getAccountIban() {
		return accountIban;
	}

	public void setAccountIban(String accountIban) {
		this.accountIban = accountIban;
	}

	public Object getFraudnetResponse() {
		return fraudnetResponse;
	}

	public void setFraudnetResponse(Object fraudnetResponse) {
		this.fraudnetResponse = fraudnetResponse;
	}
	
	

}
