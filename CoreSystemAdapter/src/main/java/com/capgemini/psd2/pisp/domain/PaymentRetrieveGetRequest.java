package com.capgemini.psd2.pisp.domain;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class PaymentRetrieveGetRequest {

	private String paymentId;
		
	@NotNull
	@Size(min=1,max=128)
	public String getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}
	
	
}
