package com.capgemini.psd2.integration.adapter;

public interface TPPInformationAdaptor{
	
	public <T> T fetchTPPInformation(String clientId);

	public String  fetchApplicationName(String clientId);

}
