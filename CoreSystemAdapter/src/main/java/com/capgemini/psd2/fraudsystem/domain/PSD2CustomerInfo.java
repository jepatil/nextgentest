package com.capgemini.psd2.fraudsystem.domain;

import java.util.List;
import java.util.Map;

import com.capgemini.psd2.aisp.domain.AccountGETResponse;

public class PSD2CustomerInfo extends AccountGETResponse { 

	private String id;
	private String name;
	private String company;

	private List<Emails> emails;
	private List<PhoneNumbers> phoneNumbers;

	private Address address;
	private String dob;
	private String hashedTaxId;
	private String mothersMaidenName;
	private String clientType;

	private Map<String, Object> extension;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public List<Emails> getEmails() {
		return emails;
	}

	public void setEmails(List<Emails> emails) {
		this.emails = emails;
	}

	public List<PhoneNumbers> getPhoneNumbers() {
		return phoneNumbers;
	}

	public void setPhoneNumbers(List<PhoneNumbers> phoneNumbers) {
		this.phoneNumbers = phoneNumbers;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getHashedTaxId() {
		return hashedTaxId;
	}

	public void setHashedTaxId(String hashedTaxId) {
		this.hashedTaxId = hashedTaxId;
	}

	public String getMothersMaidenName() {
		return mothersMaidenName;
	}

	public void setMothersMaidenName(String mothersMaidenName) {
		this.mothersMaidenName = mothersMaidenName;
	}

	public Map<String, Object> getExtension() {
		return extension;
	}

	public void setExtension(Map<String, Object> extension) {
		this.extension = extension;
	}

	public String getClientType() {
		return clientType;
	}

	public void setClientType(String clientType) {
		this.clientType = clientType;
	}

}
