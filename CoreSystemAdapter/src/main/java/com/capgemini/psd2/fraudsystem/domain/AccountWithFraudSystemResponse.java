package com.capgemini.psd2.fraudsystem.domain;

import com.capgemini.psd2.aisp.domain.Account;

public class AccountWithFraudSystemResponse extends Account {
	Object fraudResponse;

	public Object getFraudResponse() {
		return fraudResponse;
	}

	public void setFraudResponse(Object fraudResponse) {
		this.fraudResponse = fraudResponse;
	}

}
