package com.capgemini.psd2.aisp.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Data
 */
@ApiModel(description = "Data")
public class Data4   {
  @JsonProperty("Beneficiary")
  private List<Beneficiary> beneficiary = null;

  public Data4 beneficiary(List<Beneficiary> beneficiary) {
    this.beneficiary = beneficiary;
    return this;
  }

  public Data4 addBeneficiaryItem(Beneficiary beneficiaryItem) {
    if (this.beneficiary == null) {
      this.beneficiary = new ArrayList<Beneficiary>();
    }
    this.beneficiary.add(beneficiaryItem);
    return this;
  }

   /**
   * Beneficiary
   * @return beneficiary
  **/
  @ApiModelProperty(value = "Beneficiary")

  @Valid

  public List<Beneficiary> getBeneficiary() {
    return beneficiary;
  }

  public void setBeneficiary(List<Beneficiary> beneficiary) {
    this.beneficiary = beneficiary;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Data4 data4 = (Data4) o;
    return Objects.equals(this.beneficiary, data4.beneficiary);
  }

  @Override
  public int hashCode() {
    return Objects.hash(beneficiary);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Data4 {\n");
    
    sb.append("    beneficiary: ").append(toIndentedString(beneficiary)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

