package com.capgemini.psd2.aisp.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Data
 */
@ApiModel(description = "Data")
public class Data7   {
  @JsonProperty("StandingOrder")
  private List<StandingOrder> standingOrder = null;

  public Data7 standingOrder(List<StandingOrder> standingOrder) {
    this.standingOrder = standingOrder;
    return this;
  }

  public Data7 addStandingOrderItem(StandingOrder standingOrderItem) {
    if (this.standingOrder == null) {
      this.standingOrder = new ArrayList<StandingOrder>();
    }
    this.standingOrder.add(standingOrderItem);
    return this;
  }

   /**
   * StandingOrder
   * @return standingOrder
  **/
  @ApiModelProperty(value = "StandingOrder")

  @Valid

  public List<StandingOrder> getStandingOrder() {
    return standingOrder;
  }

  public void setStandingOrder(List<StandingOrder> standingOrder) {
    this.standingOrder = standingOrder;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Data7 data7 = (Data7) o;
    return Objects.equals(this.standingOrder, data7.standingOrder);
  }

  @Override
  public int hashCode() {
    return Objects.hash(standingOrder);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Data7 {\n");
    
    sb.append("    standingOrder: ").append(toIndentedString(standingOrder)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

