/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.aisp.transformer;

import java.util.Map;

import com.capgemini.psd2.aisp.domain.AccountRequestPOSTResponse;

/**
 * The Interface AccountRequestTransformer.
 */
public interface AccountRequestTransformer {
	
	/**
	 * Transform account information.
	 *
	 * @param <T> the generic type
	 * @param source the source
	 * @param destination the destination
	 * @param params the params
	 * @return the account request POST response
	 */
	public <T> AccountRequestPOSTResponse transformAccountInformation(T source, AccountRequestPOSTResponse destination,
			Map<String, String> params);

}
