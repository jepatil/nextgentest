/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.aisp.adapter;

import java.util.List;

import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;

/**
 * The Interface AispConsentAdapter.
 */
public interface AispConsentAdapter {
	
	public void createConsent(AispConsent aispConsent);
	
	public AispConsent retrieveConsent(String consentId);
	
	public AispConsent retrieveConsentByAccountRequestId(String accountRequestId,ConsentStatusEnum status);
	
	/**
	 * Retrieve account mapping by account id.
	 *
	 * @param consentId the consent id
	 * @param accountId the account id
	 * @return the account mapping
	 */
	public AccountMapping retrieveAccountMappingByAccountId(String consentId, String accountId);
	
	/**
	 * Retrieve account mapping.
	 *
	 * @param consentId the consent id
	 * @return the account mapping
	 */
	public AccountMapping retrieveAccountMapping(String consentId);
	
	public void updateConsentStatus(String consentId,ConsentStatusEnum statusEnum);
	
	public AispConsent updateConsentStatusWithResponse(String consentId,ConsentStatusEnum statusEnum);
	
	public List<AispConsent> retrieveConsentByPsuIdAndConsentStatus(String psuId, ConsentStatusEnum statusEnum);
}
