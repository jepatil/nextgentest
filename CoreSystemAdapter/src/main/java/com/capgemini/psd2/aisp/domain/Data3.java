package com.capgemini.psd2.aisp.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Data
 */
@ApiModel(description = "Data")
public class Data3   {
  @JsonProperty("Transaction")
  private List<Data3Transaction> transaction = null;

  public Data3 transaction(List<Data3Transaction> transaction) {
    this.transaction = transaction;
    return this;
  }

  public Data3 addTransactionItem(Data3Transaction transactionItem) {
    if (this.transaction == null) {
      this.transaction = new ArrayList<Data3Transaction>();
    }
    this.transaction.add(transactionItem);
    return this;
  }

   /**
   * Transaction
   * @return transaction
  **/
  @ApiModelProperty(value = "Transaction")

  @Valid
 @Size(min=1)
  public List<Data3Transaction> getTransaction() {
    return transaction;
  }

  public void setTransaction(List<Data3Transaction> transaction) {
    this.transaction = transaction;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Data3 data3 = (Data3) o;
    return Objects.equals(this.transaction, data3.transaction);
  }

  @Override
  public int hashCode() {
    return Objects.hash(transaction);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Data3 {\n");
    
    sb.append("    transaction: ").append(toIndentedString(transaction)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

