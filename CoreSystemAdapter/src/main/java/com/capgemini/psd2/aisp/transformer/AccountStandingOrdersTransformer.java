package com.capgemini.psd2.aisp.transformer;

import java.util.Map;

import com.capgemini.psd2.aisp.domain.StandingOrdersGETResponse;

public interface AccountStandingOrdersTransformer {
	public <T> StandingOrdersGETResponse transformAccountStandingOrders(T source, Map<String, String> params);

}
