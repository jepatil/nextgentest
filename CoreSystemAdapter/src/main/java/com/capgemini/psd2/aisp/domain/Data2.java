package com.capgemini.psd2.aisp.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Data
 */
@ApiModel(description = "Data")
public class Data2   {
  @JsonProperty("Account")
  private List<Account> account = null;

  public Data2 account(List<Account> account) {
    this.account = account;
    return this;
  }

  public Data2 addAccountItem(Account accountItem) {
    if (this.account == null) {
      this.account = new ArrayList<Account>();
    }
    this.account.add(accountItem);
    return this;
  }

   /**
   * Account
   * @return account
  **/
  @ApiModelProperty(value = "Account")

  @Valid

  public List<Account> getAccount() {
    return account;
  }

  public void setAccount(List<Account> account) {
    this.account = account;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Data2 data2 = (Data2) o;
    return Objects.equals(this.account, data2.account);
  }

  @Override
  public int hashCode() {
    return Objects.hash(account);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Data2 {\n");
    
    sb.append("    account: ").append(toIndentedString(account)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

