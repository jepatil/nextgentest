package com.capgemini.psd2.aisp.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Data
 */
@ApiModel(description = "Data")
public class Data5   {
  @JsonProperty("Balance")
  private List<Balance> balance = null;

  public Data5 balance(List<Balance> balance) {
    this.balance = balance;
    return this;
  }

  public Data5 addBalanceItem(Balance balanceItem) {
    if (this.balance == null) {
      this.balance = new ArrayList<Balance>();
    }
    this.balance.add(balanceItem);
    return this;
  }

   /**
   * Balance
   * @return balance
  **/
  @ApiModelProperty(value = "Balance")

  @Valid
 @Size(min=1)
  public List<Balance> getBalance() {
    return balance;
  }

  public void setBalance(List<Balance> balance) {
    this.balance = balance;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Data5 data5 = (Data5) o;
    return Objects.equals(this.balance, data5.balance);
  }

  @Override
  public int hashCode() {
    return Objects.hash(balance);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Data5 {\n");
    
    sb.append("    balance: ").append(toIndentedString(balance)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

