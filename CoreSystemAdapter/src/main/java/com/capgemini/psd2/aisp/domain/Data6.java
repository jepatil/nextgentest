package com.capgemini.psd2.aisp.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Data
 */
@ApiModel(description = "Data")
public class Data6   {
  @JsonProperty("DirectDebit")
  private List<DirectDebit> directDebit = null;

  public Data6 directDebit(List<DirectDebit> directDebit) {
    this.directDebit = directDebit;
    return this;
  }

  public Data6 addDirectDebitItem(DirectDebit directDebitItem) {
    if (this.directDebit == null) {
      this.directDebit = new ArrayList<DirectDebit>();
    }
    this.directDebit.add(directDebitItem);
    return this;
  }

   /**
   * DirectDebit
   * @return directDebit
  **/
  @ApiModelProperty(value = "DirectDebit")

  @Valid

  public List<DirectDebit> getDirectDebit() {
    return directDebit;
  }

  public void setDirectDebit(List<DirectDebit> directDebit) {
    this.directDebit = directDebit;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Data6 data6 = (Data6) o;
    return Objects.equals(this.directDebit, data6.directDebit);
  }

  @Override
  public int hashCode() {
    return Objects.hash(directDebit);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Data6 {\n");
    
    sb.append("    directDebit: ").append(toIndentedString(directDebit)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

