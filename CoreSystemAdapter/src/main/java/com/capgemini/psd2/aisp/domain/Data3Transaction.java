package com.capgemini.psd2.aisp.domain;

import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

import io.swagger.annotations.ApiModelProperty;

/**
 * Data3Transaction
 */
public class Data3Transaction   {
  @JsonProperty("AccountId")
  private String accountId = null;

  @JsonProperty("TransactionId")
  private String transactionId = null;

  @JsonProperty("TransactionReference")
  private String transactionReference = null;

  @JsonProperty("Amount")
  private Data3Amount amount = null;

  /**
   * Indicates whether the transaction is a credit or a debit entry.
   */
  public enum CreditDebitIndicatorEnum {
    CREDIT("Credit"),
    
    DEBIT("Debit");

    private String value;

    CreditDebitIndicatorEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static CreditDebitIndicatorEnum fromValue(String text) {
      for (CreditDebitIndicatorEnum b : CreditDebitIndicatorEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("CreditDebitIndicator")
  private CreditDebitIndicatorEnum creditDebitIndicator = null;

  /**
   * Status of a transaction entry on the books of the account servicer.
   */
  public enum StatusEnum {
    BOOKED("Booked"),
    
    PENDING("Pending");

    private String value;

    StatusEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static StatusEnum fromValue(String text) {
      for (StatusEnum b : StatusEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("Status")
  private StatusEnum status = null;

  @JsonProperty("BookingDateTime")
  private String bookingDateTime = null;

  @JsonProperty("ValueDateTime")
  private String valueDateTime = null;

  @JsonProperty("TransactionInformation")
  private String transactionInformation = null;

  @JsonProperty("AddressLine")
  private String addressLine = null;

  @JsonProperty("BankTransactionCode")
  private Data3BankTransactionCode bankTransactionCode = null;

  @JsonProperty("ProprietaryBankTransactionCode")
  private Data3ProprietaryBankTransactionCode proprietaryBankTransactionCode = null;

  @JsonProperty("Balance")
  private Data3Balance balance = null;

  @JsonProperty("MerchantDetails")
  private Data3MerchantDetails merchantDetails = null;

  public Data3Transaction accountId(String accountId) {
    this.accountId = accountId;
    return this;
  }

   /**
   * A unique and immutable identifier used to identify the account resource. This identifier has no meaning to the account owner.
   * @return accountId
  **/
  @ApiModelProperty(required = true, value = "A unique and immutable identifier used to identify the account resource. This identifier has no meaning to the account owner.")
  @NotNull

 @Size(min=1,max=40)
  public String getAccountId() {
    return accountId;
  }

  public void setAccountId(String accountId) {
    this.accountId = accountId;
  }

  public Data3Transaction transactionId(String transactionId) {
    this.transactionId = transactionId;
    return this;
  }

   /**
   * Unique identifier for the transaction within an servicing institution. This identifier is both unique and immutable.
   * @return transactionId
  **/
  @ApiModelProperty(value = "Unique identifier for the transaction within an servicing institution. This identifier is both unique and immutable.")

 @Size(min=1,max=40)
  public String getTransactionId() {
    return transactionId;
  }

  public void setTransactionId(String transactionId) {
    this.transactionId = transactionId;
  }

  public Data3Transaction transactionReference(String transactionReference) {
    this.transactionReference = transactionReference;
    return this;
  }

   /**
   * Unique reference for the transaction. This reference is optionally populated, and may as an example be the FPID in the Faster Payments context.
   * @return transactionReference
  **/
  @ApiModelProperty(value = "Unique reference for the transaction. This reference is optionally populated, and may as an example be the FPID in the Faster Payments context.")

 @Size(min=1,max=35)
  public String getTransactionReference() {
    return transactionReference;
  }

  public void setTransactionReference(String transactionReference) {
    this.transactionReference = transactionReference;
  }

  public Data3Transaction amount(Data3Amount amount) {
    this.amount = amount;
    return this;
  }

   /**
   * Get amount
   * @return amount
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Data3Amount getAmount() {
    return amount;
  }

  public void setAmount(Data3Amount amount) {
    this.amount = amount;
  }

  public Data3Transaction creditDebitIndicator(CreditDebitIndicatorEnum creditDebitIndicator) {
    this.creditDebitIndicator = creditDebitIndicator;
    return this;
  }

   /**
   * Indicates whether the transaction is a credit or a debit entry.
   * @return creditDebitIndicator
  **/
  @ApiModelProperty(required = true, value = "Indicates whether the transaction is a credit or a debit entry.")
  @NotNull


  public CreditDebitIndicatorEnum getCreditDebitIndicator() {
    return creditDebitIndicator;
  }

  public void setCreditDebitIndicator(CreditDebitIndicatorEnum creditDebitIndicator) {
    this.creditDebitIndicator = creditDebitIndicator;
  }

  public Data3Transaction status(StatusEnum status) {
    this.status = status;
    return this;
  }

   /**
   * Status of a transaction entry on the books of the account servicer.
   * @return status
  **/
  @ApiModelProperty(required = true, value = "Status of a transaction entry on the books of the account servicer.")
  @NotNull


  public StatusEnum getStatus() {
    return status;
  }

  public void setStatus(StatusEnum status) {
    this.status = status;
  }

  public Data3Transaction bookingDateTime(String bookingDateTime) {
    this.bookingDateTime = bookingDateTime;
    return this;
  }

   /**
   * Date and time when a transaction entry is posted to an account on the account servicer's books. Usage: Booking date is the expected booking date, unless the status is booked, in which case it is the actual booking date.  All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00
   * @return bookingDateTime
  **/
  @ApiModelProperty(required = true, value = "Date and time when a transaction entry is posted to an account on the account servicer's books. Usage: Booking date is the expected booking date, unless the status is booked, in which case it is the actual booking date.  All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00")
  @NotNull

  @Valid

  public String getBookingDateTime() {
    return bookingDateTime;
  }

  public void setBookingDateTime(String bookingDateTime) {
    this.bookingDateTime = bookingDateTime;
  }

  public Data3Transaction valueDateTime(String valueDateTime) {
    this.valueDateTime = valueDateTime;
    return this;
  }

   /**
   * Date and time at which assets become available to the account owner in case of a credit entry, or cease to be available to the account owner in case of a debit entry.  Usage: If entry status is pending and value date is present, then the value date refers to an expected/requested value date. For entries subject to availability/float and for which availability information is provided, the value date must not be used. In this case the availability component identifies the  number of availability days.  All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00
   * @return valueDateTime
  **/
  @ApiModelProperty(value = "Date and time at which assets become available to the account owner in case of a credit entry, or cease to be available to the account owner in case of a debit entry.  Usage: If entry status is pending and value date is present, then the value date refers to an expected/requested value date. For entries subject to availability/float and for which availability information is provided, the value date must not be used. In this case the availability component identifies the  number of availability days.  All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00")

  @Valid

  public String getValueDateTime() {
    return valueDateTime;
  }

  public void setValueDateTime(String valueDateTime) {
    this.valueDateTime = valueDateTime;
  }

  public Data3Transaction transactionInformation(String transactionInformation) {
    this.transactionInformation = transactionInformation;
    return this;
  }

   /**
   * Further details of the transaction. This is the transaction narrative, which is unstructured text.
   * @return transactionInformation
  **/
  @ApiModelProperty(value = "Further details of the transaction. This is the transaction narrative, which is unstructured text.")

 @Size(min=1,max=500)
  public String getTransactionInformation() {
    return transactionInformation;
  }

  public void setTransactionInformation(String transactionInformation) {
    this.transactionInformation = transactionInformation;
  }

  public Data3Transaction addressLine(String addressLine) {
    this.addressLine = addressLine;
    return this;
  }

   /**
   * Information that locates and identifies a specific address, as defined by postal services, that is presented in free format text.
   * @return addressLine
  **/
  @ApiModelProperty(value = "Information that locates and identifies a specific address, as defined by postal services, that is presented in free format text.")

 @Size(min=1,max=70)
  public String getAddressLine() {
    return addressLine;
  }

  public void setAddressLine(String addressLine) {
    this.addressLine = addressLine;
  }

  public Data3Transaction bankTransactionCode(Data3BankTransactionCode bankTransactionCode) {
    this.bankTransactionCode = bankTransactionCode;
    return this;
  }

   /**
   * Get bankTransactionCode
   * @return bankTransactionCode
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Data3BankTransactionCode getBankTransactionCode() {
    return bankTransactionCode;
  }

  public void setBankTransactionCode(Data3BankTransactionCode bankTransactionCode) {
    this.bankTransactionCode = bankTransactionCode;
  }

  public Data3Transaction proprietaryBankTransactionCode(Data3ProprietaryBankTransactionCode proprietaryBankTransactionCode) {
    this.proprietaryBankTransactionCode = proprietaryBankTransactionCode;
    return this;
  }

   /**
   * Get proprietaryBankTransactionCode
   * @return proprietaryBankTransactionCode
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Data3ProprietaryBankTransactionCode getProprietaryBankTransactionCode() {
    return proprietaryBankTransactionCode;
  }

  public void setProprietaryBankTransactionCode(Data3ProprietaryBankTransactionCode proprietaryBankTransactionCode) {
    this.proprietaryBankTransactionCode = proprietaryBankTransactionCode;
  }

  public Data3Transaction balance(Data3Balance balance) {
    this.balance = balance;
    return this;
  }

   /**
   * Get balance
   * @return balance
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Data3Balance getBalance() {
    return balance;
  }

  public void setBalance(Data3Balance balance) {
    this.balance = balance;
  }

  public Data3Transaction merchantDetails(Data3MerchantDetails merchantDetails) {
    this.merchantDetails = merchantDetails;
    return this;
  }

   /**
   * Get merchantDetails
   * @return merchantDetails
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Data3MerchantDetails getMerchantDetails() {
    return merchantDetails;
  }

  public void setMerchantDetails(Data3MerchantDetails merchantDetails) {
    this.merchantDetails = merchantDetails;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Data3Transaction data3Transaction = (Data3Transaction) o;
    return Objects.equals(this.accountId, data3Transaction.accountId) &&
        Objects.equals(this.transactionId, data3Transaction.transactionId) &&
        Objects.equals(this.transactionReference, data3Transaction.transactionReference) &&
        Objects.equals(this.amount, data3Transaction.amount) &&
        Objects.equals(this.creditDebitIndicator, data3Transaction.creditDebitIndicator) &&
        Objects.equals(this.status, data3Transaction.status) &&
        Objects.equals(this.bookingDateTime, data3Transaction.bookingDateTime) &&
        Objects.equals(this.valueDateTime, data3Transaction.valueDateTime) &&
        Objects.equals(this.transactionInformation, data3Transaction.transactionInformation) &&
        Objects.equals(this.addressLine, data3Transaction.addressLine) &&
        Objects.equals(this.bankTransactionCode, data3Transaction.bankTransactionCode) &&
        Objects.equals(this.proprietaryBankTransactionCode, data3Transaction.proprietaryBankTransactionCode) &&
        Objects.equals(this.balance, data3Transaction.balance) &&
        Objects.equals(this.merchantDetails, data3Transaction.merchantDetails);
  }

  @Override
  public int hashCode() {
    return Objects.hash(accountId, transactionId, transactionReference, amount, creditDebitIndicator, status, bookingDateTime, valueDateTime, transactionInformation, addressLine, bankTransactionCode, proprietaryBankTransactionCode, balance, merchantDetails);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Data3Transaction {\n");
    
    sb.append("    accountId: ").append(toIndentedString(accountId)).append("\n");
    sb.append("    transactionId: ").append(toIndentedString(transactionId)).append("\n");
    sb.append("    transactionReference: ").append(toIndentedString(transactionReference)).append("\n");
    sb.append("    amount: ").append(toIndentedString(amount)).append("\n");
    sb.append("    creditDebitIndicator: ").append(toIndentedString(creditDebitIndicator)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    bookingDateTime: ").append(toIndentedString(bookingDateTime)).append("\n");
    sb.append("    valueDateTime: ").append(toIndentedString(valueDateTime)).append("\n");
    sb.append("    transactionInformation: ").append(toIndentedString(transactionInformation)).append("\n");
    sb.append("    addressLine: ").append(toIndentedString(addressLine)).append("\n");
    sb.append("    bankTransactionCode: ").append(toIndentedString(bankTransactionCode)).append("\n");
    sb.append("    proprietaryBankTransactionCode: ").append(toIndentedString(proprietaryBankTransactionCode)).append("\n");
    sb.append("    balance: ").append(toIndentedString(balance)).append("\n");
    sb.append("    merchantDetails: ").append(toIndentedString(merchantDetails)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

