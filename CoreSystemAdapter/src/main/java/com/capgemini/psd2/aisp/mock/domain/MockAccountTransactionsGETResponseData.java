package com.capgemini.psd2.aisp.mock.domain;


import java.time.LocalDateTime;

import com.capgemini.psd2.aisp.domain.Data3Transaction;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class MockAccountTransactionsGETResponseData extends Data3Transaction {

	private String accountNumber;

	private String accountNSC;

	private String psuId;

	private LocalDateTime bookingDateTimeCopy;
	
	private String txnRetrievalKey;

	@JsonIgnore
	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	@JsonIgnore
	public String getAccountNSC() {
		return accountNSC;
	}

	public void setAccountNSC(String accountNSC) {
		this.accountNSC = accountNSC;
	}

	@JsonIgnore
	public String getPsuId() {
		return psuId;
	}

	public void setPsuId(String psuId) {
		this.psuId = psuId;
	}

	@JsonIgnore
	public LocalDateTime getBookingDateTimeCopy() {
		return bookingDateTimeCopy;
	}

	public void setBookingDateTimeCopy(LocalDateTime bookingDateTimeCopy) {
		this.bookingDateTimeCopy = bookingDateTimeCopy;
	}

	@JsonIgnore
	public String getTxnRetrievalKey() {
		return txnRetrievalKey;
	}

	public void setTxnRetrievalKey(String txnRetrievalKey) {
		this.txnRetrievalKey = txnRetrievalKey;
	}
}
