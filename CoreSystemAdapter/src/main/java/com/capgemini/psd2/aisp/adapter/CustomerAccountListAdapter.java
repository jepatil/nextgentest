package com.capgemini.psd2.aisp.adapter;

import java.util.Map;

import com.capgemini.psd2.aisp.domain.AccountGETResponse;
import com.capgemini.psd2.fraudsystem.domain.PSD2CustomerInfo;

public interface CustomerAccountListAdapter {

	public AccountGETResponse retrieveCustomerAccountList(String userId, Map<String, String> params);
	
	public PSD2CustomerInfo retrieveCustomerInfo(String userId, Map<String, String> params);
}