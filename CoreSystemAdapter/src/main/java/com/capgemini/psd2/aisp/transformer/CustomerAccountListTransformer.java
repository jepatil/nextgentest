package com.capgemini.psd2.aisp.transformer;

import java.util.Map;

import com.capgemini.psd2.aisp.domain.AccountGETResponse;
import com.capgemini.psd2.fraudsystem.domain.PSD2CustomerInfo;

public interface CustomerAccountListTransformer {
	public <T> AccountGETResponse transformCustomerAccountListAdapter(T source, Map<String, String> params);
	
	public <T> PSD2CustomerInfo transformCustomerInfo(T source, Map<String, String> params);
}
