package com.capgemini.psd2.aisp.transformer;

import java.util.Map;

import com.capgemini.psd2.aisp.domain.ProductGETResponse;

public interface AccountProductsTransformer {
	public <T> ProductGETResponse transformAccountProducts(T source, Map<String, String> params);
}
