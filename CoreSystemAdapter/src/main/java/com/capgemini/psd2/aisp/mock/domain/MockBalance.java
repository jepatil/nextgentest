package com.capgemini.psd2.aisp.mock.domain;

import com.capgemini.psd2.aisp.domain.Balance;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class MockBalance extends Balance{

	@JsonIgnore
	private String accountNumber;
	@JsonIgnore
	private String accountNSC;
	@JsonIgnore
	private String psuId;
	
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getAccountNSC() {
		return accountNSC;
	}
	public void setAccountNSC(String accountNSC) {
		this.accountNSC = accountNSC;
	}
	public String getPsuId() {
		return psuId;
	}
	public void setPsuId(String psuId) {
		this.psuId = psuId;
	}

	
}
