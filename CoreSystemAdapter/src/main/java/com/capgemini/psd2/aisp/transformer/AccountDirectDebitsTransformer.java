package com.capgemini.psd2.aisp.transformer;

import java.util.Map;

import com.capgemini.psd2.aisp.domain.AccountGETResponse1;

public interface AccountDirectDebitsTransformer {
	public <T> AccountGETResponse1 transformAccountDirectDebits(T source, Map<String, String> params);
}
