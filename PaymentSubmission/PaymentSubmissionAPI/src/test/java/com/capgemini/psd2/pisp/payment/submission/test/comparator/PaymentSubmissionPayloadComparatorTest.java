package com.capgemini.psd2.pisp.payment.submission.test.comparator;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.pisp.domain.CreditorAccount;
import com.capgemini.psd2.pisp.domain.CreditorAgent;
import com.capgemini.psd2.pisp.domain.CreditorAgent.SchemeNameEnum;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomPaymentSubmissionPOSTRequest;
import com.capgemini.psd2.pisp.domain.DebtorAccount;
import com.capgemini.psd2.pisp.domain.DebtorAgent;
import com.capgemini.psd2.pisp.domain.PaymentSetup;
import com.capgemini.psd2.pisp.domain.PaymentSetupInitiation;
import com.capgemini.psd2.pisp.domain.PaymentSetupInitiationInstructedAmount;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseLinks;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseMeta;
import com.capgemini.psd2.pisp.domain.PaymentSetupPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse.StatusEnum;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponseInitiation;
import com.capgemini.psd2.pisp.domain.PaymentSubmission;
import com.capgemini.psd2.pisp.domain.PaymentSubmissionPOSTRequest;
import com.capgemini.psd2.pisp.domain.RemittanceInformation;
import com.capgemini.psd2.pisp.domain.Risk;
import com.capgemini.psd2.pisp.domain.Risk.PaymentContextCodeEnum;
import com.capgemini.psd2.pisp.domain.RiskDeliveryAddress;
import com.capgemini.psd2.pisp.payment.submission.comparator.PaymentSubmissionPayloadComparator;
import com.capgemini.psd2.pisp.validation.PispUtilities;

public class PaymentSubmissionPayloadComparatorTest {
	
	@InjectMocks
	PaymentSubmissionPayloadComparator comparator;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void contextLoads() {
	}

	@Test
		public void compareTest(){
			
			CustomPaymentSetupPOSTResponse response = new CustomPaymentSetupPOSTResponse();
			PaymentSetupResponse data = new PaymentSetupResponse();
			response.setData(data);
			data.setPaymentId("12345");
			data.setCreationDateTime(PispUtilities.getCurrentDateInISOFormat());
			data.setStatus(StatusEnum.ACCEPTEDTECHNICALVALIDATION);
			
			Risk risk = new Risk();
			response.setRisk(risk);
			
			PaymentSetupResponseInitiation initiation = new PaymentSetupResponseInitiation();
			data.setInitiation(initiation);
			
			initiation.setInstructionIdentification("ABDDC");
			initiation.setEndToEndIdentification("FRESCO.21302.GFX.28");
			
			PaymentSetupInitiationInstructedAmount instructedAmount = new PaymentSetupInitiationInstructedAmount();
			instructedAmount.setAmount("777777777.00");
			instructedAmount.setCurrency("EUR");
			initiation.setInstructedAmount(instructedAmount);
			
			CreditorAgent creditorAgent = new CreditorAgent();
			creditorAgent.setSchemeName(SchemeNameEnum.BICFI);
			creditorAgent.setIdentification("SC080801");
			initiation.setCreditorAgent(creditorAgent);
			
			CreditorAccount creditorAccount = new CreditorAccount();
			creditorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.IBAN);
			creditorAccount.setIdentification("NWBK60161331926819");
			creditorAccount.setName("Test user");
			creditorAccount.setSecondaryIdentification("0002");
			initiation.setCreditorAccount(creditorAccount);
			
			DebtorAgent debtorAgent = new DebtorAgent();
			debtorAgent.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAgent.SchemeNameEnum.BICFI);
			debtorAgent.setIdentification("SC112800");
			initiation.setDebtorAgent(debtorAgent);
			
			DebtorAccount debtorAccount = new DebtorAccount();
			debtorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.IBAN);
			debtorAccount.setIdentification("GB29NWBK60161331926819");
			debtorAccount.setName("Andrea Smith");
			debtorAccount.setSecondaryIdentification("0002");
			initiation.setDebtorAccount(debtorAccount);
			
			RemittanceInformation remittanceInformation = new RemittanceInformation();
			remittanceInformation.setReference("FRESCO-101");
			remittanceInformation.setUnstructured("Internal ops code 5120101");
			initiation.setRemittanceInformation(remittanceInformation);
			
			risk.setMerchantCategoryCode("5967");
			risk.setMerchantCustomerIdentification("053598653254");
			risk.setPaymentContextCode(PaymentContextCodeEnum.ECOMMERCEGOODS);
			
			RiskDeliveryAddress deliveryAddress = new RiskDeliveryAddress();
			risk.setDeliveryAddress(deliveryAddress);
			
			List<String> addressLine = new ArrayList<>();
			addressLine.add("Flat 7");
			addressLine.add("Acacia Lodge");		
			deliveryAddress.setAddressLine(addressLine);
			
			deliveryAddress.setBuildingNumber("27");
			deliveryAddress.setCountry("UK");
			
			List<String> countrySubDivision = new ArrayList<>();
			countrySubDivision.add("ABCDABCDABCDABCDAB");
			countrySubDivision.add("DEFG");
			deliveryAddress.setCountrySubDivision(countrySubDivision);
			
			deliveryAddress.setPostCode("GU31 2ZZ");
			deliveryAddress.setStreetName("AcaciaAvenue");
			deliveryAddress.setTownName("Sparsholt");
			
			response.setLinks(new PaymentSetupPOSTResponseLinks());
			response.setMeta(new PaymentSetupPOSTResponseMeta());
			
			
			CustomPaymentSetupPOSTResponse paymentSetupResponse = new CustomPaymentSetupPOSTResponse();
			paymentSetupResponse.setData(data);
			paymentSetupResponse.setRisk(risk);
		
			paymentSetupResponse.setLinks(new PaymentSetupPOSTResponseLinks());
			paymentSetupResponse.setMeta(new PaymentSetupPOSTResponseMeta());
			
			int result = comparator.compare(response, paymentSetupResponse);
			assertEquals(0, result);
	
			
			
			//PaymentSetupPOSTResponse paymentSetupResponse = new PaymentSetupPOSTResponse();
			paymentSetupResponse = new CustomPaymentSetupPOSTResponse();
			//PaymentSetupResponse data = new PaymentSetupResponse();
			PaymentSetupResponse data1 = new PaymentSetupResponse();
			paymentSetupResponse.setData(data1);
			data.setPaymentId("12345");
			data.setCreationDateTime(PispUtilities.getCurrentDateInISOFormat());
			data.setStatus(StatusEnum.ACCEPTEDTECHNICALVALIDATION);
			
			Risk risk1 = new Risk();
			paymentSetupResponse.setRisk(risk1);
			
			PaymentSetupResponseInitiation initiation1 = new PaymentSetupResponseInitiation();
			data.setInitiation(initiation1);
			
			initiation1.setInstructionIdentification("ABDDC");
			initiation1.setEndToEndIdentification("FRESCO.21302.GFX.28");
			
			PaymentSetupInitiationInstructedAmount instructedAmount1 = new PaymentSetupInitiationInstructedAmount();
			instructedAmount1.setAmount("777777777.00");
			instructedAmount1.setCurrency("EUR");
			initiation1.setInstructedAmount(instructedAmount1);
			
			CreditorAgent creditorAgent1 = new CreditorAgent();
			creditorAgent1.setSchemeName(SchemeNameEnum.BICFI);
			creditorAgent1.setIdentification("SC080801");
			initiation1.setCreditorAgent(creditorAgent1);
			
			CreditorAccount creditorAccount1 = new CreditorAccount();
			creditorAccount1.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.IBAN);
			creditorAccount1.setIdentification("NWBK60161331926819");
			creditorAccount1.setName("Test user");
			creditorAccount1.setSecondaryIdentification("0002");
			initiation1.setCreditorAccount(creditorAccount1);
			
			DebtorAgent debtorAgent1 = new DebtorAgent();
			debtorAgent1.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAgent.SchemeNameEnum.BICFI);
			debtorAgent1.setIdentification("SC112800");
			initiation1.setDebtorAgent(debtorAgent1);
			
			DebtorAccount debtorAccount1 = new DebtorAccount();
			debtorAccount1.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.IBAN);
			debtorAccount1.setIdentification("GB29NWBK60161331926819");
			debtorAccount1.setName("Andrea Smith");
			debtorAccount1.setSecondaryIdentification("0002");
			initiation1.setDebtorAccount(debtorAccount1);
			
			RemittanceInformation remittanceInformation1 = new RemittanceInformation();
			remittanceInformation1.setReference("FRESCO-101");
			remittanceInformation1.setUnstructured("Internal ops code 5120101");
			initiation1.setRemittanceInformation(remittanceInformation1);
			
			risk1.setMerchantCategoryCode("5967");
			risk1.setMerchantCustomerIdentification("053598653254");
			risk1.setPaymentContextCode(PaymentContextCodeEnum.ECOMMERCEGOODS);
			
			RiskDeliveryAddress deliveryAddress1 = new RiskDeliveryAddress();
			risk1.setDeliveryAddress(deliveryAddress1);
			
			List<String> addressLine1 = new ArrayList<>();
			addressLine1.add("Flat 7");
			addressLine1.add("Acacia Lodge");		
			deliveryAddress1.setAddressLine(addressLine1);
			
			deliveryAddress1.setBuildingNumber("27");
			deliveryAddress1.setCountry("UK");
			
			List<String> countrySubDivision1 = new ArrayList<>();
			countrySubDivision1.add("ABCDABCDABCDABCDAB");
			countrySubDivision1.add("DEFG");
			deliveryAddress1.setCountrySubDivision(countrySubDivision1);
			
			deliveryAddress1.setPostCode("GU31 2ZZ");
			deliveryAddress1.setStreetName("AcaciaAvenue");
			deliveryAddress1.setTownName("Sparsholt");
			
			paymentSetupResponse.setLinks(new PaymentSetupPOSTResponseLinks());
			paymentSetupResponse.setMeta(new PaymentSetupPOSTResponseMeta());
			
			
			result = comparator.compare(response, paymentSetupResponse);
			assertEquals(1, result);

	}
			
			/**
			 * 
			 */
			@Test
			public void comparePaymentDetailsTests(){
			CustomPaymentSetupPOSTResponse paymentSetupResponse = new CustomPaymentSetupPOSTResponse();
			PaymentSetupInitiation initiation = new PaymentSetupInitiation();
			CustomPaymentSetupPOSTRequest request = new CustomPaymentSetupPOSTRequest();
			CustomPaymentSetupPOSTResponse adaptedPaymentResponse = new CustomPaymentSetupPOSTResponse();
			CustomPaymentSubmissionPOSTRequest paymentSubmissionRequest = new CustomPaymentSubmissionPOSTRequest();
			
			PaymentSetupInitiationInstructedAmount instructedAmount = new PaymentSetupInitiationInstructedAmount();
			instructedAmount.setAmount(".5");
			initiation.setInstructedAmount(instructedAmount);
			
			
			PaymentSetupInitiationInstructedAmount instructedAmount1 = new PaymentSetupInitiationInstructedAmount();
			instructedAmount1.setAmount("00.50");

	
			
			PaymentSubmission data = new PaymentSubmission();
			Risk risk = new Risk();
			
			PaymentSetupResponse data1 = new PaymentSetupResponse();
		
			paymentSetupResponse.setLinks(new PaymentSetupPOSTResponseLinks());
			paymentSetupResponse.setMeta(new PaymentSetupPOSTResponseMeta());
			paymentSetupResponse.setData(data1);
			paymentSetupResponse.setRisk(risk);
			
			PaymentSetupResponse data2 = new PaymentSetupResponse();
			adaptedPaymentResponse.setData(data2);
			adaptedPaymentResponse.setRisk(risk);
			PaymentSetupResponseInitiation initiation2 = new PaymentSetupResponseInitiation();
			data2.setInitiation(initiation2);
			PaymentSetupInitiationInstructedAmount instructedAmount2 = new PaymentSetupInitiationInstructedAmount();
			instructedAmount2.setAmount(".5");
			initiation2.setInstructedAmount(instructedAmount2);
			
			paymentSubmissionRequest.setData(data);
			
			PaymentSetup test = new PaymentSetup();
			request.setData(test);
			request.setRisk(risk);
			test.setInitiation(initiation);
			
			paymentSubmissionRequest.setRisk(risk);
			
			data.setInitiation(initiation2);
			PaymentSetupResponseInitiation initiation1 = new PaymentSetupResponseInitiation();
			data1.setInitiation(initiation1);
			initiation1.setInstructedAmount(instructedAmount1);
		
			DebtorAgent debtorAgent1 = new DebtorAgent();
			debtorAgent1.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAgent.SchemeNameEnum.BICFI);
			debtorAgent1.setIdentification("SC112800");
			initiation1.setDebtorAgent(debtorAgent1);
			
			DebtorAccount debtorAccount1 = new DebtorAccount();
			debtorAccount1.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.IBAN);
			debtorAccount1.setIdentification("GB29NWBK60161331926819");
			debtorAccount1.setName("Andrea Smith");
			debtorAccount1.setSecondaryIdentification("0002");
			initiation1.setDebtorAccount(debtorAccount1); 
			PaymentSetupPlatformResource resource = new PaymentSetupPlatformResource();
			resource.setTppDebtorDetails("true");
			resource.setTppDebtorNameDetails("true");
			int result = comparator.comparePaymentDetails(paymentSetupResponse, paymentSubmissionRequest, resource);
			assertEquals(1, result);
			resource.setTppDebtorNameDetails("false");
			debtorAccount1.setName(null);
			result = comparator.comparePaymentDetails(paymentSetupResponse, paymentSubmissionRequest, resource);
			assertEquals(1, result);
			resource.setTppDebtorDetails("false");
			debtorAccount1.setName("Andrea Smith");
			 result = comparator.comparePaymentDetails(paymentSetupResponse, request, resource);
			assertEquals(0, result);
			}
			
			@Test
			public void DebtorTest() {
			PaymentSetupInitiation initiation = new PaymentSetupInitiation();
			CustomPaymentSetupPOSTResponse paymentSetupResponse = new CustomPaymentSetupPOSTResponse();
			CustomPaymentSetupPOSTResponse adaptedPaymentResponse = new CustomPaymentSetupPOSTResponse();
			CustomPaymentSetupPOSTRequest request = new CustomPaymentSetupPOSTRequest();
		
			
			PaymentSubmissionPOSTRequest paymentSubmissionRequest = new PaymentSubmissionPOSTRequest();
			PaymentSubmission data = new PaymentSubmission();
			Risk risk = new Risk();
			PaymentSetupResponse data2 = new PaymentSetupResponse();
			adaptedPaymentResponse.setData(data2);
			adaptedPaymentResponse.setRisk(risk);
			PaymentSetupResponseInitiation initiation2 = new PaymentSetupResponseInitiation();
			data2.setInitiation(initiation2);
			PaymentSetupInitiationInstructedAmount instructedAmount2 = new PaymentSetupInitiationInstructedAmount();
			instructedAmount2.setAmount("00.50");
			initiation2.setInstructedAmount(instructedAmount2);
			
			paymentSubmissionRequest.setData(data);
			
			PaymentSetup test = new PaymentSetup();
			request.setData(test);
			request.setRisk(risk);
			test.setInitiation(initiation);
			PaymentSetupInitiationInstructedAmount instructedAmount = new PaymentSetupInitiationInstructedAmount();
			instructedAmount.setAmount("00.50");
			initiation.setInstructedAmount(instructedAmount);
			
			paymentSubmissionRequest.setRisk(risk);
			
			data.setInitiation(initiation2);
			PaymentSetupResponseInitiation initiation1 = new PaymentSetupResponseInitiation();
			PaymentSetupResponse data1 = new PaymentSetupResponse();
			data1.setInitiation(initiation1);
			PaymentSetupInitiationInstructedAmount instructedAmount1 = new PaymentSetupInitiationInstructedAmount();
			instructedAmount1.setAmount("00.50");
			initiation1.setInstructedAmount(instructedAmount1);
			
			paymentSetupResponse.setLinks(new PaymentSetupPOSTResponseLinks());
			paymentSetupResponse.setMeta(new PaymentSetupPOSTResponseMeta());
			paymentSetupResponse.setData(data1);
			paymentSetupResponse.setRisk(risk);
			
			Risk risk2 = new Risk();
			adaptedPaymentResponse.setRisk(risk2);
			
			initiation.setDebtorAgent(null);
			int result = comparator.compare(paymentSetupResponse,adaptedPaymentResponse);
			
			assertEquals(0, result);
			
			//DebtorAccount null
			initiation.setDebtorAgent(null);
			
			initiation2.setDebtorAccount(null);
			PaymentSetupPlatformResource resource = new PaymentSetupPlatformResource();
			resource.setTppDebtorDetails("true");
			resource.setTppDebtorNameDetails("true");
			result = comparator.comparePaymentDetails(paymentSetupResponse, request, resource);
			assertEquals(0, result);
			}
			
			@Test
			public void validateDebtorDetailsTest(){
				DebtorAccount debtorAccount = new DebtorAccount();
				PaymentSetupResponseInitiation responseInitiation = new PaymentSetupResponseInitiation();
				PaymentSetupResponseInitiation requestInitiation = new PaymentSetupResponseInitiation();
				PaymentSetupPlatformResource paymentSetupPlatformResource = new PaymentSetupPlatformResource();
				paymentSetupPlatformResource.setTppDebtorDetails("false");
				requestInitiation.setDebtorAccount(debtorAccount);
				boolean result = comparator.validateDebtorDetails(responseInitiation, requestInitiation, paymentSetupPlatformResource);
				assertThat(result).isEqualTo(false);
				requestInitiation.setDebtorAccount(null);
				responseInitiation.setDebtorAccount(null);
				result = comparator.validateDebtorDetails(responseInitiation, requestInitiation, paymentSetupPlatformResource);
				assertThat(result).isEqualTo(true);
			}
			
			
			@Test
			public void comparePaymentDetailsTests2(){
			CustomPaymentSetupPOSTResponse paymentSetupResponse = new CustomPaymentSetupPOSTResponse();
			PaymentSetupInitiation initiation = new PaymentSetupInitiation();
			CustomPaymentSetupPOSTRequest request = new CustomPaymentSetupPOSTRequest();
			CustomPaymentSetupPOSTResponse adaptedPaymentResponse = new CustomPaymentSetupPOSTResponse();
			CustomPaymentSubmissionPOSTRequest paymentSubmissionRequest = new CustomPaymentSubmissionPOSTRequest();
			
			PaymentSetupInitiationInstructedAmount instructedAmount = new PaymentSetupInitiationInstructedAmount();
			instructedAmount.setAmount(".5");
			initiation.setInstructedAmount(instructedAmount);
			
			
			PaymentSetupInitiationInstructedAmount instructedAmount1 = new PaymentSetupInitiationInstructedAmount();
			instructedAmount1.setAmount("00.50");

	
			
			PaymentSubmission data = new PaymentSubmission();
			Risk risk = new Risk();
			RiskDeliveryAddress riskDeliveryAddress=new RiskDeliveryAddress();
			List<String> addressLineList=new ArrayList<>();
			addressLineList.add("address1");
			
			
			riskDeliveryAddress.setAddressLine(addressLineList);
			riskDeliveryAddress.setBuildingNumber("123");
			riskDeliveryAddress.setCountry("Ireland");
			List<String> countrySubDivision=new ArrayList<>();
			countrySubDivision.add("countrysubdivision1");
			
			riskDeliveryAddress.setCountrySubDivision(countrySubDivision);
			riskDeliveryAddress.setPostCode("520012");
			riskDeliveryAddress.setStreetName("Jamesstreet");
			riskDeliveryAddress.setTownName("sothampton");
			
			risk.setDeliveryAddress(riskDeliveryAddress);
			
			PaymentSetupResponse data1 = new PaymentSetupResponse();
		
			paymentSetupResponse.setLinks(new PaymentSetupPOSTResponseLinks());
			paymentSetupResponse.setMeta(new PaymentSetupPOSTResponseMeta());
			paymentSetupResponse.setData(data1);
			paymentSetupResponse.setRisk(risk);
			
			PaymentSetupResponse data2 = new PaymentSetupResponse();
			adaptedPaymentResponse.setData(data2);
			adaptedPaymentResponse.setRisk(risk);
			PaymentSetupResponseInitiation initiation2 = new PaymentSetupResponseInitiation();
			data2.setInitiation(initiation2);
			PaymentSetupInitiationInstructedAmount instructedAmount2 = new PaymentSetupInitiationInstructedAmount();
			instructedAmount2.setAmount(".5");
			initiation2.setInstructedAmount(instructedAmount2);
			
			paymentSubmissionRequest.setData(data);
			
			PaymentSetup test = new PaymentSetup();
			request.setData(test);
			request.setRisk(risk);
			test.setInitiation(initiation);
			
			paymentSubmissionRequest.setRisk(risk);
			
			data.setInitiation(initiation2);
			PaymentSetupResponseInitiation initiation1 = new PaymentSetupResponseInitiation();
			data1.setInitiation(initiation1);
			initiation1.setInstructedAmount(instructedAmount1);
		
			DebtorAgent debtorAgent1 = new DebtorAgent();
			debtorAgent1.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAgent.SchemeNameEnum.BICFI);
			debtorAgent1.setIdentification("SC112800");
			initiation1.setDebtorAgent(debtorAgent1);
			
			DebtorAccount debtorAccount1 = new DebtorAccount();
			debtorAccount1.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.IBAN);
			debtorAccount1.setIdentification("GB29NWBK60161331926819");
			debtorAccount1.setName("Andrea Smith");
			debtorAccount1.setSecondaryIdentification("0002");
			initiation1.setDebtorAccount(debtorAccount1); 
			PaymentSetupPlatformResource resource = new PaymentSetupPlatformResource();
			resource.setTppDebtorDetails("true");
			resource.setTppDebtorNameDetails("true");
			int result = comparator.comparePaymentDetails(paymentSetupResponse, paymentSubmissionRequest, resource);
			assertEquals(1, result);
			resource.setTppDebtorNameDetails("false");
			debtorAccount1.setName(null);
			result = comparator.comparePaymentDetails(paymentSetupResponse, paymentSubmissionRequest, resource);
			assertEquals(1, result);
			resource.setTppDebtorDetails("false");
			debtorAccount1.setName("Andrea Smith");
			 result = comparator.comparePaymentDetails(paymentSetupResponse, request, resource);
			assertEquals(0, result);
			}
			
			
			@Test
			public void comparePaymentDetailsTests3(){
			CustomPaymentSetupPOSTResponse paymentSetupResponse = new CustomPaymentSetupPOSTResponse();
			PaymentSetupInitiation initiation = new PaymentSetupInitiation();
			CustomPaymentSetupPOSTRequest request = new CustomPaymentSetupPOSTRequest();
			CustomPaymentSetupPOSTResponse adaptedPaymentResponse = new CustomPaymentSetupPOSTResponse();
			CustomPaymentSubmissionPOSTRequest paymentSubmissionRequest = new CustomPaymentSubmissionPOSTRequest();
			
			PaymentSetupInitiationInstructedAmount instructedAmount = new PaymentSetupInitiationInstructedAmount();
			instructedAmount.setAmount(".5");
			initiation.setInstructedAmount(instructedAmount);
			
			
			PaymentSetupInitiationInstructedAmount instructedAmount1 = new PaymentSetupInitiationInstructedAmount();
			instructedAmount1.setAmount("00.50");

	
			
			PaymentSubmission data = new PaymentSubmission();
			Risk risk = new Risk();
			RiskDeliveryAddress riskDeliveryAddress=new RiskDeliveryAddress();
			List<String> addressLineList=new ArrayList<>();
			addressLineList.add(null);
			
			riskDeliveryAddress.setAddressLine(addressLineList);
			riskDeliveryAddress.setBuildingNumber("123");
			riskDeliveryAddress.setCountry("Ireland");
			List<String> countrySubDivision=new ArrayList<>();	
			countrySubDivision.add(null);
			
			
			riskDeliveryAddress.setCountrySubDivision(countrySubDivision);
			riskDeliveryAddress.setPostCode("520012");
			riskDeliveryAddress.setStreetName("Jamesstreet");
			riskDeliveryAddress.setTownName("sothampton");
			
			risk.setDeliveryAddress(riskDeliveryAddress);
			
			
			PaymentSetupResponse data1 = new PaymentSetupResponse();
		
			paymentSetupResponse.setLinks(new PaymentSetupPOSTResponseLinks());
			paymentSetupResponse.setMeta(new PaymentSetupPOSTResponseMeta());
			paymentSetupResponse.setData(data1);
			paymentSetupResponse.setRisk(risk);
			
			PaymentSetupResponse data2 = new PaymentSetupResponse();
			adaptedPaymentResponse.setData(data2);
			adaptedPaymentResponse.setRisk(risk);
			PaymentSetupResponseInitiation initiation2 = new PaymentSetupResponseInitiation();
			data2.setInitiation(initiation2);
			PaymentSetupInitiationInstructedAmount instructedAmount2 = new PaymentSetupInitiationInstructedAmount();
			instructedAmount2.setAmount(".5");
			initiation2.setInstructedAmount(instructedAmount2);
			
			paymentSubmissionRequest.setData(data);
			
			PaymentSetup test = new PaymentSetup();
			request.setData(test);
			request.setRisk(risk);
			test.setInitiation(initiation);
			
			paymentSubmissionRequest.setRisk(risk);
			
			data.setInitiation(initiation2);
			PaymentSetupResponseInitiation initiation1 = new PaymentSetupResponseInitiation();
			data1.setInitiation(initiation1);
			initiation1.setInstructedAmount(instructedAmount1);
		
			DebtorAgent debtorAgent1 = new DebtorAgent();
			debtorAgent1.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAgent.SchemeNameEnum.BICFI);
			debtorAgent1.setIdentification("SC112800");
			initiation1.setDebtorAgent(debtorAgent1);
			
			DebtorAccount debtorAccount1 = new DebtorAccount();
			debtorAccount1.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.IBAN);
			debtorAccount1.setIdentification("GB29NWBK60161331926819");
			debtorAccount1.setName("Andrea Smith");
			debtorAccount1.setSecondaryIdentification("0002");
			initiation1.setDebtorAccount(debtorAccount1); 
			PaymentSetupPlatformResource resource = new PaymentSetupPlatformResource();
			resource.setTppDebtorDetails("true");
			resource.setTppDebtorNameDetails("true");
			int result = comparator.comparePaymentDetails(paymentSetupResponse, paymentSubmissionRequest, resource);
			assertEquals(1, result);
			resource.setTppDebtorNameDetails("false");
			debtorAccount1.setName(null);
			result = comparator.comparePaymentDetails(paymentSetupResponse, paymentSubmissionRequest, resource);
			assertEquals(1, result);
			resource.setTppDebtorDetails("false");
			debtorAccount1.setName("Andrea Smith");
			 result = comparator.comparePaymentDetails(paymentSetupResponse, request, resource);
			assertEquals(1, result);
			}
			
		
			
		}

			
	


