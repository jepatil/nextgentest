package com.capgemini.psd2.pisp.payment.submission.test.mock.data;

import com.capgemini.psd2.pisp.domain.CustomPaymentSubmissionPOSTRequest;
import com.fasterxml.jackson.databind.ObjectMapper;

public class PaymentSubmissionPOSTRequestResponseMockData {
	
	public static CustomPaymentSubmissionPOSTRequest getPaymentSubmissionPOSTRequest() {
		return new CustomPaymentSubmissionPOSTRequest();
		
	}
	
	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
