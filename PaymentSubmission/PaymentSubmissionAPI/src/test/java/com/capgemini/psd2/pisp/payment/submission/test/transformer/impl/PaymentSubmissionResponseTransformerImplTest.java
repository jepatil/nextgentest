package com.capgemini.psd2.pisp.payment.submission.test.transformer.impl;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.PaymentSubmissionPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentSubmitPOST201Response;
import com.capgemini.psd2.pisp.payment.submission.transformer.impl.PaymentSubmissionResponseTransformerImpl;
import com.capgemini.psd2.pisp.validation.PispUtilities;

@RunWith(SpringJUnit4ClassRunner.class)
public class PaymentSubmissionResponseTransformerImplTest {
	
	@InjectMocks
	private PaymentSubmissionResponseTransformerImpl paymentSubmissionResponseTransformerImpl;
	
	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testPaymentSubmissionResponseTransformer() {
		
		PaymentSubmissionPlatformResource paymentSubmissionPlatformResource = new PaymentSubmissionPlatformResource();
		paymentSubmissionPlatformResource.setCreatedAt(PispUtilities.getCurrentDateInISOFormat());
		paymentSubmissionPlatformResource.setPaymentId("123");
		paymentSubmissionPlatformResource.setPaymentSubmissionId("1234");
		paymentSubmissionPlatformResource.setStatus("200");
		when(reqHeaderAtrributes.getSelfUrl()).thenReturn("test");
		PaymentSubmitPOST201Response post201Response = paymentSubmissionResponseTransformerImpl.paymentSubmissionResponseTransformer(paymentSubmissionPlatformResource);
		assertNotNull(post201Response);
		/*
		 * populateLinks() 2nd branch
		 */
		when(reqHeaderAtrributes.getSelfUrl()).thenReturn("/test");
		post201Response = paymentSubmissionResponseTransformerImpl.paymentSubmissionResponseTransformer(paymentSubmissionPlatformResource);
		assertNotNull(post201Response);
		
	}
}
