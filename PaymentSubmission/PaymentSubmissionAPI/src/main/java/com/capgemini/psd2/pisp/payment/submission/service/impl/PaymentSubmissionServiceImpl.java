package com.capgemini.psd2.pisp.payment.submission.service.impl;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.consent.domain.PispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.PaymentSetupAdapterHelper;
import com.capgemini.psd2.pisp.adapter.PaymentSetupPlatformAdapter;
import com.capgemini.psd2.pisp.adapter.PaymentSubmissionAdapterHelper;
import com.capgemini.psd2.pisp.adapter.PaymentSubmissionPlatformAdapter;
import com.capgemini.psd2.pisp.adapter.PispConsentAdapter;
import com.capgemini.psd2.pisp.consent.adapter.repository.PispConsentMongoRepository;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomPaymentSubmissionPOSTRequest;
import com.capgemini.psd2.pisp.domain.PaymentResponseInfo;
import com.capgemini.psd2.pisp.domain.PaymentSetupPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse.StatusEnum;
import com.capgemini.psd2.pisp.domain.PaymentSubmissionExecutionResponse;
import com.capgemini.psd2.pisp.domain.PaymentSubmissionPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentSubmitPOST201Response;
import com.capgemini.psd2.pisp.payment.submission.comparator.PaymentSubmissionPayloadComparator;
import com.capgemini.psd2.pisp.payment.submission.service.PaymentSubmissionService;
import com.capgemini.psd2.pisp.payment.submission.transformer.PaymentSubmissionResponseTransformer;
import com.capgemini.psd2.pisp.status.PaymentStatusEnum;
import com.capgemini.psd2.pisp.validation.PispUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class PaymentSubmissionServiceImpl implements PaymentSubmissionService {

	@Autowired
	private PaymentSubmissionPayloadComparator paymentComparator;
	@Autowired
	private PaymentSubmissionResponseTransformer responseTransformer;
	@Autowired
	private PaymentSetupPlatformAdapter paymentPlatformAdapter;
	@Autowired
	private PaymentSubmissionPlatformAdapter submissionPlatformAdapter;		
	@Autowired
	private PaymentSetupAdapterHelper paymentSetupAdapterHelper;
	@Autowired
	private PaymentSubmissionAdapterHelper submissionAdapterHelper;
	
	@Autowired
	private PispConsentAdapter pispConsentAdapter;
	@Autowired
	private PispConsentMongoRepository pispConsentMongoRepository;
	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;
	
	private long paymentSetupResourceExpiry;
	private long idempotencyDuration;
	
	@Autowired
	public PaymentSubmissionServiceImpl(@Value("${app.resource.paymentSetupResourceExpiry}") String paymentSetupResourceExpiry,
										 @Value("${app.idempotency.durationForPaymentSubmission}") String idempotencyDuration){
		this.paymentSetupResourceExpiry = PispUtilities.getMilliSeconds(paymentSetupResourceExpiry);
		this.idempotencyDuration = PispUtilities.getMilliSeconds(idempotencyDuration);
	}
	
	@Override
	public PaymentSubmitPOST201Response createPaymentSubmissionResource(CustomPaymentSubmissionPOSTRequest submissionRequest) {
		
		/*
		 * If requestId of Token,it is a paymentId for which TPP got the consent, does not match with paymentId of submissionRequest
		 * then throw an exception as 'Bad Request'
		 */

		if(! submissionRequest.getData().getPaymentId().equalsIgnoreCase(reqHeaderAtrributes.getToken().getRequestId()))
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_TOKEN_PAYMENT_ID_NOT_MATCHED_WITH_SUBMISSION_PAYMENT_ID);
		
				
		PaymentSubmitPOST201Response submissionResponse = performIdempotencyCheck(submissionRequest);
		if(submissionResponse != null && submissionResponse.getData().getPaymentSubmissionId() != null)
			return submissionResponse;	
		
		/*
		 * Checking paymentId exists or not.
		 * If it is not then return an error to PISP
		 */
		PaymentSetupPlatformResource paymentPlatformResource = paymentPlatformAdapter.retrievePaymentSetupResource(submissionRequest.getData().getPaymentId());
		if(paymentPlatformResource == null)
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_NO_PAYMENT_SETUP_ID_FOUND_IN_SYSTEM);	
		
		/*
		 * Checking whether paymentId is already used for other payment initiation or not.
		 * If it is already used then return an error to PISP. 
		 */
		PaymentSubmissionPlatformResource submissionPlatformResource = submissionPlatformAdapter.retrievePaymentSubmissionResourceByPaymentId(submissionRequest.getData().getPaymentId().trim());
		if(submissionPlatformResource != null && submissionPlatformResource.getPaymentSubmissionId() != null)
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_PAYMENT_ID_ALREADY_USED_FOR_SUBMISSION);	
		
		/*
		 * Checking whether staged payment setup resource's status is 'AccedptedCustomerProfile' or not.
		 * If status is differ then return an error to PISP.
		 */		
		if(! PaymentStatusEnum.ACCEPTEDCUSTOMERPROFILE.getStatusCode().equalsIgnoreCase(paymentPlatformResource.getStatus()))
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_INVALID_PAYMENT_SETUP_STATUS);	
		
		/*
		 * Retrieving staged payment setup details from bank.
		 * If no details are found at bank end then adapter will return an error to PISP.
		 */
		CustomPaymentSetupPOSTResponse paymentSetupFoundationResponse = paymentSetupAdapterHelper.retrieveStagedPaymentSetup(submissionRequest.getData().getPaymentId());
		
		/*
		 * Validating submission request details against the staged payment setup details.
		 * If details are not matched then return an error to PISP.
		 */
		if (paymentComparator.comparePaymentDetails(paymentSetupFoundationResponse, submissionRequest, paymentPlatformResource) > 0)
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_SUBMISSION_PAYLOAD_COMPARISON_FAILED);
		
		/*
		 * Checking whether staged payment setup resource is expired or not.
		 * If it is expired then return an error to PISP.
		 */
		if(isStagedPaymentResourceExpired(paymentPlatformResource)){		
						
			/*
			 * Revoke Consent and Access token
			 */
			revokeConsentAndAccessToken(submissionRequest.getData().getPaymentId());	
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_PAYMENT_SETUP_RESOURCE_EXPIRED);
		}		
		
		/*
		 * Initiated pre-submission validation.
		 *   
		 */
		PaymentResponseInfo params = submissionAdapterHelper.prePaymentSubmissionValidation(paymentSetupFoundationResponse);
		
		/*
		 * Checking paymentSubmissionId. It can not be blank.
		 */
		
		if(NullCheckUtils.isNullOrEmpty(params.getPaymentSubmissionId()))
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_SUBMISSION_ID_NOT_FOUND_PAYMENT_VALIDATION);
		
		/*
		 * If submissionId is already used for any other resource 
		 * then platform will throw the exception for duplicate paymentSubmissionId
		 */
		if(submissionPlatformAdapter.retrievePaymentSubmissionResource(params.getPaymentSubmissionId()) != null)
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_DUPLICATE_SUBMISSION_ID);
		
		/*
		 * Checking again whether paymentId is already used for other payment initiation or not.
		 * If it is already used then return an error to PISP.
		 *  
		 */
		submissionPlatformResource = submissionPlatformAdapter.retrievePaymentSubmissionResourceByPaymentId(submissionRequest.getData().getPaymentId());
		if(submissionPlatformResource != null && ! NullCheckUtils.isNullOrEmpty(submissionPlatformResource.getPaymentSubmissionId()))
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_PAYMENT_ID_ALREADY_USED_FOR_SUBMISSION);	
		
				
		/*
		 * Created payment submission resource in platform environment with status 'Pending' or 'Rejected'
		 * based on validation status from adapter -> bank.
		 *   
		 */
		submissionPlatformResource = submissionPlatformAdapter.createPaymentSubmissionResource(paymentPlatformResource, params);
		
		/*
		 * Call StageUpdatePayment service to update the payment submission status.
		 */
		paymentSetupFoundationResponse.getData().setStatus(StatusEnum.fromValue(submissionPlatformResource.getStatus()));
		paymentSetupFoundationResponse.setPaymentStatus(submissionPlatformResource.getStatus());
		paymentSetupAdapterHelper.updateStagedPaymentSetup(paymentSetupFoundationResponse);
		/*
		 * If pre-submission validation is completed successfully then status might be Pending or Rejected.
		 * If status is 'Rejected' then API platform will send proper response with status 'Rejected' to TPP
		 * Its as per CR010. 
		 */
		if(PaymentStatusEnum.REJECTED.getStatusCode().equalsIgnoreCase(submissionPlatformResource.getStatus())){
			return responseTransformer.paymentSubmissionResponseTransformer(submissionPlatformResource);
		}
		
		paymentSetupFoundationResponse.getData().setCreationDateTime(paymentPlatformResource.getCreatedAt());
		initiatePaymentSubmission(submissionRequest, submissionPlatformResource, paymentSetupFoundationResponse);	
		
		return responseTransformer.paymentSubmissionResponseTransformer(submissionPlatformResource);
				
	}
	
	
	
	
	private void initiatePaymentSubmission(CustomPaymentSubmissionPOSTRequest submissionRequest, PaymentSubmissionPlatformResource submissionPlatformResource, CustomPaymentSetupPOSTResponse paymentSetupFoundationResponse){
		
		
		/*
		 * If TPP has not sent DebtorAccount.Name then product will set the name details from staging record which is sent by FS
		 */
		if(submissionRequest.getData().getInitiation().getDebtorAccount() != null && 
				NullCheckUtils.isNullOrEmpty(submissionRequest.getData().getInitiation().getDebtorAccount().getName())){
			
			submissionRequest.getData().getInitiation().getDebtorAccount().setName(paymentSetupFoundationResponse.getData().getInitiation().getDebtorAccount().getName());
		}
		/*
		 * If debtor details are not provided by TPP then Platform will set debtor details received from FS.
		 * Same will be sent to FS for Pre-SubmissionValidation and Account Permission
		 */
		
		if(submissionRequest.getData().getInitiation().getDebtorAccount() == null){
			submissionRequest.getData().getInitiation().setDebtorAgent(paymentSetupFoundationResponse.getData().getInitiation().getDebtorAgent());
			submissionRequest.getData().getInitiation().setDebtorAccount(paymentSetupFoundationResponse.getData().getInitiation().getDebtorAccount());
		}
		submissionRequest.setPayerCurrency(paymentSetupFoundationResponse.getPayerCurrency());
		submissionRequest.setPayerJurisdiction(paymentSetupFoundationResponse.getPayerJurisdiction());
		submissionRequest.setCreatedOn(paymentSetupFoundationResponse.getData().getCreationDateTime());
		submissionRequest.setPaymentStatus(submissionPlatformResource.getStatus());
		submissionRequest.setFraudSystemResponse(paymentSetupFoundationResponse.getFraudnetResponse());
		PaymentSubmissionExecutionResponse executionResponse = submissionAdapterHelper.executePaymentSubmission(submissionRequest);
		
		if(NullCheckUtils.isNullOrEmpty(executionResponse.getPaymentSubmissionId())){			
			
			submissionPlatformResource.setStatus(PaymentStatusEnum.REJECTED.getStatusCode());
			submissionPlatformResource.setIdempotencyRequest(String.valueOf(Boolean.FALSE));
			submissionPlatformAdapter.updatePaymentSubmissionResource(submissionPlatformResource);
		
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_PAYMENT_SUBMISSION_CREATION_FAILED);
		}
		
		/*
		 * PaymentSubmissionId, generated through pre payment validation and payment execution must be same.
		 */
		
		if( !submissionPlatformResource.getPaymentSubmissionId().equalsIgnoreCase(executionResponse.getPaymentSubmissionId()))
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_SUBMISSION_ID_NOT_EQUAL_VALIDATION_EXECUTION);
			
		String paymentExecutionStatus = executionResponse.getPaymentSubmissionStatus();
		if(PaymentStatusEnum.PASSED.getStatusCode().equalsIgnoreCase(executionResponse.getPaymentSubmissionStatus()))			
			paymentExecutionStatus = PaymentStatusEnum.ACCEPTEDSETTLEMENTINPROCESS.getStatusCode();			
		
		submissionPlatformResource.setStatus(paymentExecutionStatus);
		submissionPlatformResource.setPaymentSubmissionId(executionResponse.getPaymentSubmissionId());
		submissionPlatformResource.setIdempotencyRequest(String.valueOf(Boolean.TRUE));
		
		/*
		 * Update platform submission resource with appropriate status
		 */
		verifyAndUpdatePaymentSubmissionResource(submissionPlatformResource);
		
		/*
		 * Call again StageUpdatePayment service to update final payment submission status 'ACCEPTEDSETTLEMENTINPROCESS' or 'REJECTED'.
		 */
		
		paymentSetupFoundationResponse.setPaymentStatus(paymentExecutionStatus);
		paymentSetupAdapterHelper.updateStagedPaymentSetup(paymentSetupFoundationResponse);
		
				
	}
	
	/*
	 * Update platform submission resource status to 'ACCEPTEDSETTLEMENTINPROCESS' or 'REJECTED'.
	 */
	private void verifyAndUpdatePaymentSubmissionResource(PaymentSubmissionPlatformResource submissionPlatformResource){
					
		submissionPlatformAdapter.updatePaymentSubmissionResource(submissionPlatformResource);
	}

	/*
	 * Checking the validity of paymentId. 
	 * If its expired then throw the exception
	 */
	private boolean isStagedPaymentResourceExpired(PaymentSetupPlatformResource paymentSetupPlatformResource){
				
		String paymentResourceCreatedAt = paymentSetupPlatformResource.getCreatedAt();
		DateTime paymentResourceCreatedAtDate = new DateTime(paymentResourceCreatedAt);
		DateTime currentTimeStamp = new DateTime();
		
		
		boolean expiryStatus = Boolean.FALSE;
		if(currentTimeStamp.getMillis() - paymentResourceCreatedAtDate.getMillis() > paymentSetupResourceExpiry)					
			expiryStatus = Boolean.TRUE;
		
		return expiryStatus;				
	}
	
	private PaymentSubmitPOST201Response performIdempotencyCheck(CustomPaymentSubmissionPOSTRequest submissionRequest){
		PaymentSubmissionPlatformResource submissionPlatformResource = submissionPlatformAdapter.getIdempotentPaymentSubmissionResource(idempotencyDuration);
		
		if(submissionPlatformResource == null)
			return null;
		
		/*
		 * Checking paymentId exists or not.
		 * If it is not then return an error to PISP
		 */
		PaymentSetupPlatformResource paymentPlatformResource = paymentPlatformAdapter.retrievePaymentSetupResource(submissionRequest.getData().getPaymentId());
		
		if(paymentPlatformResource == null)
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_NO_PAYMENT_SETUP_ID_FOUND_IN_SYSTEM);	
		
		if(! submissionRequest.getData().getPaymentId().equalsIgnoreCase(submissionPlatformResource.getPaymentId()))
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_INVALID_PAYMENT_ID_AGAINST_SUBMISSION_REQUEST);
		
		CustomPaymentSetupPOSTResponse paymentSetupFoundationResponse = paymentSetupAdapterHelper.retrieveStagedPaymentSetup(submissionPlatformResource.getPaymentId());		
		
		if (paymentComparator.comparePaymentDetails(paymentSetupFoundationResponse, submissionRequest, paymentPlatformResource) > 0)
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_SUBMISSION_PAYLOAD_COMPARISON_FAILED);
		
		if(PaymentStatusEnum.PENDING.getStatusCode().equalsIgnoreCase(submissionPlatformResource.getStatus())){
			
			if(isStagedPaymentResourceExpired(paymentPlatformResource))	{		
				/*
				 * Revoke Consent and Access token
				 */
				revokeConsentAndAccessToken(submissionRequest.getData().getPaymentId());	
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_PAYMENT_SETUP_RESOURCE_EXPIRED);
			}
			
			paymentSetupFoundationResponse.getData().setCreationDateTime(paymentPlatformResource.getCreatedAt());
			initiatePaymentSubmission(submissionRequest, submissionPlatformResource, paymentSetupFoundationResponse);
			
		}
		return responseTransformer.paymentSubmissionResponseTransformer(submissionPlatformResource);
	}
	
	private void revokeConsentAndAccessToken(String paymentId) {

        PispConsent pispConsent = pispConsentAdapter.retrieveConsentByPaymentId(paymentId, ConsentStatusEnum.AUTHORISED);
        if (pispConsent != null) {
        	
           pispConsent.setStatus(ConsentStatusEnum.EXPIRED);
           pispConsentMongoRepository.save(pispConsent);       
        }
	}
	
			
}
