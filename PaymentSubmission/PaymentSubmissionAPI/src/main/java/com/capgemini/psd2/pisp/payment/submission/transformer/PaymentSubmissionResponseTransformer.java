package com.capgemini.psd2.pisp.payment.submission.transformer;

import com.capgemini.psd2.pisp.domain.PaymentSubmissionPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentSubmitPOST201Response;

public interface PaymentSubmissionResponseTransformer {
	public PaymentSubmitPOST201Response paymentSubmissionResponseTransformer(PaymentSubmissionPlatformResource paymentSubmissionPlatformResource);
}
