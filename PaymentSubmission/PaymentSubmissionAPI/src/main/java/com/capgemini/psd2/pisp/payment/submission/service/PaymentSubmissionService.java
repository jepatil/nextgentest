package com.capgemini.psd2.pisp.payment.submission.service;

import com.capgemini.psd2.pisp.domain.CustomPaymentSubmissionPOSTRequest;
import com.capgemini.psd2.pisp.domain.PaymentSubmitPOST201Response;

public interface PaymentSubmissionService {

	public PaymentSubmitPOST201Response createPaymentSubmissionResource(CustomPaymentSubmissionPOSTRequest paymentSetupRequest);
}
