package com.capgemini.psd2.pisp.payment.submission.mongo.db.test.adapter.impl;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.PaymentSubmission;
import com.capgemini.psd2.pisp.domain.PaymentSubmissionExecutionResponse;
import com.capgemini.psd2.pisp.domain.PaymentSubmissionFoundationResource;
import com.capgemini.psd2.pisp.domain.CustomPaymentSubmissionPOSTRequest;
import com.capgemini.psd2.pisp.domain.PaymentSetupFoundationResource;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponseInitiation;
import com.capgemini.psd2.pisp.payment.submission.mongo.db.adapter.impl.PaymentSubmissionExecutionMongoDbAdapterImpl;
import com.capgemini.psd2.pisp.payment.submission.mongo.db.adapter.repository.PaymentSetupRetrieveFoundationRepository;
import com.capgemini.psd2.pisp.payment.submission.mongo.db.adapter.repository.PaymentSubmissionFoundationRepository;
import com.capgemini.psd2.pisp.sequence.adapter.SequenceGenerator;


@RunWith(SpringJUnit4ClassRunner.class)
//@SpringApplicationConfiguration(PaymentSubmissionExecutionMongoDbAdapterImplTest.class)
public class PaymentSubmissionExecutionMongoDbAdapterImplTest {


	@InjectMocks
	PaymentSubmissionExecutionMongoDbAdapterImpl paymentSubmissionExecutionMongoDbAdapterImpl;
	
	@Mock
	private PaymentSetupRetrieveFoundationRepository paymentSetupFoundationRepository;
	
	@Mock
	PaymentSubmissionFoundationRepository paymentSubmissionFoundationRepository;
	
	@Mock
	SequenceGenerator sequenceGenerator;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void contextLoads() {
	}


	@Test
	public void executePaymentSubmission() {
		CustomPaymentSubmissionPOSTRequest paymentSubmissionPOSTRequest = new CustomPaymentSubmissionPOSTRequest();
		PaymentSubmission paymentSubmission = new PaymentSubmission();
		PaymentSetupResponseInitiation initiation=new PaymentSetupResponseInitiation();
		paymentSubmission.setInitiation(initiation);
		paymentSubmission.setPaymentId("rfd");
		Map<String, String> params = new HashMap<String, String>();
		paymentSubmissionPOSTRequest.setData(paymentSubmission);
		when(paymentSetupFoundationRepository.findOneByDataPaymentId(anyString())).thenReturn(new PaymentSetupFoundationResource());
		PaymentSubmissionExecutionResponse executionResponse = new PaymentSubmissionExecutionResponse();
		PaymentSubmissionFoundationResource paymentSubmissionBankResource = new PaymentSubmissionFoundationResource();
		Mockito.when(paymentSubmissionFoundationRepository.save(paymentSubmissionBankResource)).thenReturn(paymentSubmissionBankResource);
		Mockito.when(sequenceGenerator.getNextSequenceId(anyString())).thenReturn((long)3.0);
		executionResponse = paymentSubmissionExecutionMongoDbAdapterImpl.executePaymentSubmission(paymentSubmissionPOSTRequest, params);
		assertNotNull(executionResponse);

		
	
	}
	@Test(expected= PSD2Exception.class)
	public void executePaymentSubmissionFailure() {
		CustomPaymentSubmissionPOSTRequest paymentSubmissionPOSTRequest = new CustomPaymentSubmissionPOSTRequest();
		PaymentSubmission paymentSubmission = new PaymentSubmission();
		PaymentSetupResponseInitiation initiation=new PaymentSetupResponseInitiation();
		paymentSubmission.setInitiation(initiation);
		paymentSubmission.setPaymentId("rfd");
		Map<String, String> params = new HashMap<String, String>();
		paymentSubmissionPOSTRequest.setData(paymentSubmission);
		when(paymentSetupFoundationRepository.findOneByDataPaymentId(anyString())).thenThrow(DataAccessResourceFailureException.class);
		PaymentSubmissionExecutionResponse executionResponse = new PaymentSubmissionExecutionResponse();
		PaymentSubmissionFoundationResource paymentSubmissionBankResource = new PaymentSubmissionFoundationResource();
		Mockito.when(paymentSubmissionFoundationRepository.save(paymentSubmissionBankResource)).thenReturn(paymentSubmissionBankResource);
		Mockito.when(sequenceGenerator.getNextSequenceId(anyString())).thenThrow(new DataAccessResourceFailureException("Hey"));
		paymentSubmissionExecutionMongoDbAdapterImpl.executePaymentSubmission(paymentSubmissionPOSTRequest, params);
		assertNotNull(executionResponse);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testForException() {
		CustomPaymentSubmissionPOSTRequest obj=new CustomPaymentSubmissionPOSTRequest();
		PaymentSubmission paymentsub=new PaymentSubmission();
		paymentsub.setPaymentId("12345");
		obj.setData(paymentsub);
		
		when(paymentSubmissionFoundationRepository.findOneByDataPaymentId(anyString())).thenThrow(DataAccessResourceFailureException.class);
		paymentSubmissionExecutionMongoDbAdapterImpl.executePaymentSubmission(obj, null);
	}
	
}
	    
	  

