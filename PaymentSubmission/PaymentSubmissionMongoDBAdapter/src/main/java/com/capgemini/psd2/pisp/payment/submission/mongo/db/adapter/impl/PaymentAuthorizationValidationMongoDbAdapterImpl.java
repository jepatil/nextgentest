package com.capgemini.psd2.pisp.payment.submission.mongo.db.adapter.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.adapter.PaymentAuthorizationValidationAdapter;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupFoundationResource;
import com.capgemini.psd2.pisp.domain.PaymentSetupValidationResponse;
import com.capgemini.psd2.pisp.payment.submission.mongo.db.adapter.repository.PaymentSetupRetrieveFoundationRepository;
import com.capgemini.psd2.pisp.status.PaymentStatusEnum;

public class PaymentAuthorizationValidationMongoDbAdapterImpl implements PaymentAuthorizationValidationAdapter{

	
	@Autowired
	private PaymentSetupRetrieveFoundationRepository paymentSetupFoundationRepository;
	

	@Override
	public PaymentSetupValidationResponse preAuthorizationPaymentValidation(
			CustomPaymentSetupPOSTResponse paymentSetupResponse, Map<String, String> params) {
		
		if(paymentSetupResponse.getData().getInitiation().getDebtorAccount() != null && "FS_PMV_011".equalsIgnoreCase(paymentSetupResponse.getData().getInitiation().getEndToEndIdentification())){
			/*
			 * It is checking payment permission.
			 */			
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_INVALID_ACCOUNT_PERMISSION);
		}
		
		String paymentValidationStatus = PaymentStatusEnum.PASSED.getStatusCode();
		if("FS_PMV_013".equalsIgnoreCase(paymentSetupResponse.getData().getInitiation().getEndToEndIdentification())){
				
			paymentValidationStatus = PaymentStatusEnum.REJECTED.getStatusCode();
		}
		
		PaymentSetupValidationResponse response = new PaymentSetupValidationResponse();
		response.setPaymentSetupValidationStatus(paymentValidationStatus);

		response.setPaymentSubmissionId(fetchPaymentSubmissionId(paymentSetupResponse.getData().getPaymentId()));
		return response;
	}
	
	private String fetchPaymentSubmissionId(String  paymentId){
		
		try{
			PaymentSetupFoundationResource paymentSetupFoundationResource = paymentSetupFoundationRepository.findOneByDataPaymentId(paymentId);
			
			return paymentSetupFoundationResource.getPaymentSubmissionId();
		}catch(DataAccessResourceFailureException exception){
			throw PSD2Exception.populatePSD2Exception(exception.getMessage(), ErrorCodeEnum.PISP_TECHNICAL_ERROR_FS_PAYMENT_RETRIEVE);
		}
		
	}

}
