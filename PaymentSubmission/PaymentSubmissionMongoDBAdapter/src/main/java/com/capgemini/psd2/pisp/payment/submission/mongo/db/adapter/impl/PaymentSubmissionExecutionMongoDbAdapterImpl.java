package com.capgemini.psd2.pisp.payment.submission.mongo.db.adapter.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.adapter.PaymentSubmissionExecutionAdapter;
import com.capgemini.psd2.pisp.domain.CustomPaymentSubmissionPOSTRequest;
import com.capgemini.psd2.pisp.domain.PaymentSetupFoundationResource;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse1;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse1.StatusEnum;
import com.capgemini.psd2.pisp.domain.PaymentSubmissionExecutionResponse;
import com.capgemini.psd2.pisp.domain.PaymentSubmissionFoundationResource;
import com.capgemini.psd2.pisp.payment.submission.mongo.db.adapter.repository.PaymentSetupRetrieveFoundationRepository;
import com.capgemini.psd2.pisp.payment.submission.mongo.db.adapter.repository.PaymentSubmissionFoundationRepository;
import com.capgemini.psd2.pisp.status.PaymentStatusEnum;
import com.capgemini.psd2.pisp.validation.PispUtilities;

public class PaymentSubmissionExecutionMongoDbAdapterImpl implements PaymentSubmissionExecutionAdapter{
	
	@Autowired
	private PaymentSubmissionFoundationRepository paymentSubmissionFoundationRepository;
	
	@Autowired
	private PaymentSetupRetrieveFoundationRepository paymentSetupFoundationRepository;

	@Override
	public PaymentSubmissionExecutionResponse executePaymentSubmission(CustomPaymentSubmissionPOSTRequest paymentSubmissionRequest,
			Map<String, String> params) {
		try{
			
			PaymentSubmissionFoundationResource paymentSubmissionBankResource  = paymentSubmissionFoundationRepository.findOneByDataPaymentId(paymentSubmissionRequest.getData().getPaymentId());
			
			if(paymentSubmissionBankResource == null){
				
				paymentSubmissionBankResource = new PaymentSubmissionFoundationResource();				
				paymentSubmissionBankResource.setData(new PaymentSetupResponse1());
				paymentSubmissionBankResource.getData().setPaymentSubmissionId(fetchPaymentSubmissionId(paymentSubmissionRequest.getData().getPaymentId()));
				paymentSubmissionBankResource.getData().setPaymentId(paymentSubmissionRequest.getData().getPaymentId());
				paymentSubmissionBankResource.getData().setCreationDateTime(PispUtilities.getCurrentDateInISOFormat());
				paymentSubmissionBankResource.getData().setStatus(StatusEnum.ACCEPTEDSETTLEMENTINPROCESS);				
				if("FS_PMV_017".equalsIgnoreCase(paymentSubmissionRequest.getData().getInitiation().getEndToEndIdentification())){
						
					paymentSubmissionBankResource.getData().setStatus(StatusEnum.REJECTED);
				}
				paymentSubmissionFoundationRepository.save(paymentSubmissionBankResource);	
			}
		
			String paymentValidationStatus = PaymentStatusEnum.REJECTED.getStatusCode();
			
			PaymentSubmissionExecutionResponse executionResponse = new PaymentSubmissionExecutionResponse();
			if(StatusEnum.ACCEPTEDSETTLEMENTINPROCESS.toString().equalsIgnoreCase(paymentSubmissionBankResource.getData().getStatus().toString()))
				paymentValidationStatus = PaymentStatusEnum.PASSED.getStatusCode();
			
			executionResponse.setPaymentSubmissionStatus(paymentValidationStatus);
			executionResponse.setPaymentSubmissionId(paymentSubmissionBankResource.getData().getPaymentSubmissionId());
			
			return executionResponse;
			
		}catch(DataAccessResourceFailureException exception){
			throw PSD2Exception.populatePSD2Exception(exception.getMessage(), ErrorCodeEnum.PISP_TECHNICAL_ERROR_FS_PAYMENT_INITIATION);
		}
		  
	}
	
	private String fetchPaymentSubmissionId(String paymentId){
		
		try{
			PaymentSetupFoundationResource paymentSetupFoundationResource = paymentSetupFoundationRepository.findOneByDataPaymentId(paymentId);
			
			return paymentSetupFoundationResource.getPaymentSubmissionId();
		}catch(DataAccessResourceFailureException exception){
			throw PSD2Exception.populatePSD2Exception(exception.getMessage(), ErrorCodeEnum.PISP_TECHNICAL_ERROR_FS_PAYMENT_RETRIEVE);
		}
		
	}

}
