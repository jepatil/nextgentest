
package com.capgemini.psd2.pisp.payment.submission.routing.adapter.routing;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.pisp.adapter.PaymentAuthorizationValidationAdapter;
import com.capgemini.psd2.pisp.adapter.PaymentSubmissionExecutionAdapter;

@Component
public class PaymentSubmissionCoreSystemAdapterFactory implements ApplicationContextAware, PaymentSubmissionAdapterFactory {

	/** The application context. */
	private ApplicationContext applicationContext;
		
	@Override
	public PaymentSubmissionExecutionAdapter getPaymentSubmissionExecutionAdapterInstance(String adapterName) {
		return (PaymentSubmissionExecutionAdapter) applicationContext.getBean(adapterName);		
	}
	
	@Override
	public PaymentAuthorizationValidationAdapter getPaymentAuthorizationValidationAdapterInstance(
			String adapterName) {
		return (PaymentAuthorizationValidationAdapter) applicationContext.getBean(adapterName);
	}
		
	@Override
	public void setApplicationContext(ApplicationContext context) {
		  this.applicationContext = context;
	}
	
}
