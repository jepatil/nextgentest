package com.capgemini.psd2.pisp.payment.submission.routing.adapter.impl;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import com.capgemini.psd2.pisp.adapter.PaymentSubmissionExecutionAdapter;
import com.capgemini.psd2.pisp.domain.CustomPaymentSubmissionPOSTRequest;
import com.capgemini.psd2.pisp.domain.PaymentSubmissionExecutionResponse;
import com.capgemini.psd2.pisp.payment.submission.routing.adapter.routing.PaymentSubmissionAdapterFactory;

public class PaymentSubmissionExecutionRoutingAdapterImpl implements PaymentSubmissionExecutionAdapter{
	
	@Autowired
	private PaymentSubmissionAdapterFactory factory;

	@Value("${app.paymentSubmissionExecutionAdapter}")
	private String paymentSubmissionExecutionAdapter;
	
	@Override
	public PaymentSubmissionExecutionResponse executePaymentSubmission(CustomPaymentSubmissionPOSTRequest paymentSubmissionRequest,
			Map<String, String> params) {
		return getRoutingAdapter().executePaymentSubmission(paymentSubmissionRequest, params);
	}
	

	private PaymentSubmissionExecutionAdapter getRoutingAdapter(){
		return factory.getPaymentSubmissionExecutionAdapterInstance(paymentSubmissionExecutionAdapter);
	}
}
