
package com.capgemini.psd2.pisp.payment.submission.routing.adapter.routing;

import com.capgemini.psd2.pisp.adapter.PaymentAuthorizationValidationAdapter;
import com.capgemini.psd2.pisp.adapter.PaymentSubmissionExecutionAdapter;

public interface PaymentSubmissionAdapterFactory {
	

	public PaymentSubmissionExecutionAdapter getPaymentSubmissionExecutionAdapterInstance(String coreSystemName);
	public PaymentAuthorizationValidationAdapter getPaymentAuthorizationValidationAdapterInstance(String coreSystemName);
}
