package com.capgemini.psd2.pisp.payment.submission.routing.adapter.helper.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.PaymentAuthorizationValidationAdapter;
import com.capgemini.psd2.pisp.adapter.PaymentSubmissionAdapterHelper;
import com.capgemini.psd2.pisp.adapter.PaymentSubmissionExecutionAdapter;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomPaymentSubmissionPOSTRequest;
import com.capgemini.psd2.pisp.domain.PaymentResponseInfo;
import com.capgemini.psd2.pisp.domain.PaymentSetupValidationResponse;
import com.capgemini.psd2.pisp.domain.PaymentSubmissionExecutionResponse;
import com.capgemini.psd2.pisp.status.PaymentStatusEnum;

@Component
public class PaymentSubmissionAdapterHelperImpl implements PaymentSubmissionAdapterHelper{

	@Autowired
	@Qualifier("paymentSubmissionExecutionAdapter")
	private PaymentSubmissionExecutionAdapter submissionExecutionRoutingAdapter;
	
	@Autowired
	@Qualifier("paymentAuthorizationValidationAdapter")
	private PaymentAuthorizationValidationAdapter authorizationValidationRoutingAdapter;
	
	@Value("${app.consentFlowType}")
	private String consentFlowType;
	
	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;
	
	@Override
	public PaymentResponseInfo prePaymentSubmissionValidation(CustomPaymentSetupPOSTResponse paymentSetupResponse) {
		
		Map<String,String> mapParam = getHeaderParams(); 
		mapParam.put(PSD2Constants.CONSENT_FLOW_TYPE, PSD2Constants.PISP_SUBMISSION_AUTHORIZATION_FLOW);
		PaymentSetupValidationResponse validationResponse = authorizationValidationRoutingAdapter.preAuthorizationPaymentValidation(paymentSetupResponse, mapParam);
		
		String validationStatus = PaymentStatusEnum.PENDING.getStatusCode();
		if(PaymentStatusEnum.REJECTED.getStatusCode().equalsIgnoreCase(validationResponse.getPaymentSetupValidationStatus())){
			validationStatus = validationResponse.getPaymentSetupValidationStatus();
		} 
		PaymentResponseInfo params = new PaymentResponseInfo();
		params.setPaymentValidationStatus(validationStatus);
		params.setIdempotencyRequest(String.valueOf(Boolean.TRUE));	
		params.setPaymentSubmissionId(validationResponse.getPaymentSubmissionId());
		return params;
	}
	
	@Override
	public PaymentSubmissionExecutionResponse executePaymentSubmission(CustomPaymentSubmissionPOSTRequest paymentSubmissionRequest){
		Map<String,String> mapParam = getHeaderParams(); 	
		
		return submissionExecutionRoutingAdapter.executePaymentSubmission(paymentSubmissionRequest, mapParam);
	}
		
	private Map<String,String> getHeaderParams(){
		Map<String,String> mapParam = new HashMap<>();	
		
		mapParam.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, reqHeaderAtrributes.getToken().getSeviceParams().get(PSD2Constants.CHANNEL_NAME)); 	
		mapParam.put(PSD2Constants.USER_IN_REQ_HEADER, reqHeaderAtrributes.getPsuId());
		mapParam.put(PSD2Constants.CORRELATION_REQ_HEADER, reqHeaderAtrributes.getCorrelationId());
		
		return mapParam;
	}

	

}
