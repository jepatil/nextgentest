package com.capgemini.psd2.pisp.payment.submission.routing.test.adapter.impl;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.pisp.domain.PaymentSubmissionExecutionResponse;
import com.capgemini.psd2.pisp.domain.CustomPaymentSubmissionPOSTRequest;
import com.capgemini.psd2.pisp.payment.submission.routing.adapter.impl.PaymentSubmissionExecutionRoutingAdapterImpl;
import com.capgemini.psd2.pisp.payment.submission.routing.adapter.routing.PaymentSubmissionAdapterFactory;

@RunWith(SpringJUnit4ClassRunner.class)
public class PaymentSubmissionExecutionRoutingAdapterImplTest {

	@InjectMocks
	PaymentSubmissionExecutionRoutingAdapterImpl paymentSubmissionExecutionRoutingAdapterImpl;
	
	@Mock
	PaymentSubmissionAdapterFactory factory;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testExecutePaymentSubmission() {
		PaymentSubmissionExecutionAdapterMock mock = new PaymentSubmissionExecutionAdapterMock();
		CustomPaymentSubmissionPOSTRequest postRequest = new CustomPaymentSubmissionPOSTRequest();
		Map<String, String> params = new HashMap<>();
		when(factory.getPaymentSubmissionExecutionAdapterInstance(anyObject())).thenReturn(mock);
		PaymentSubmissionExecutionResponse response = paymentSubmissionExecutionRoutingAdapterImpl.executePaymentSubmission(postRequest, params);
		assertNotNull(response);
	}
}
