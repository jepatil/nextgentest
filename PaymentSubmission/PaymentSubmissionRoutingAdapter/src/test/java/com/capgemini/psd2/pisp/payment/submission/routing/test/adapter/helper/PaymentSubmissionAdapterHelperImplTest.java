package com.capgemini.psd2.pisp.payment.submission.routing.test.adapter.helper;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.PaymentAuthorizationValidationAdapter;
import com.capgemini.psd2.pisp.adapter.PaymentSubmissionExecutionAdapter;
import com.capgemini.psd2.pisp.domain.PaymentResponseInfo;
import com.capgemini.psd2.pisp.domain.PaymentSetupValidationResponse;
import com.capgemini.psd2.pisp.domain.PaymentSubmissionExecutionResponse;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomPaymentSubmissionPOSTRequest;
import com.capgemini.psd2.pisp.payment.submission.routing.adapter.helper.impl.PaymentSubmissionAdapterHelperImpl;
import com.capgemini.psd2.token.Token;

@RunWith(SpringJUnit4ClassRunner.class)
public class PaymentSubmissionAdapterHelperImplTest {
	
	@InjectMocks
	PaymentSubmissionAdapterHelperImpl paymentSubmissionAdapterHelperImpl;
	
	@Mock
	@Qualifier("paymentSubmissionExecutionAdapter")
	private PaymentSubmissionExecutionAdapter submissionExecutionRoutingAdapter;
	
	@Mock
	@Qualifier("paymentAuthorizationValidationAdapter")
	private PaymentAuthorizationValidationAdapter authorizationValidationRoutingAdapter;
	
	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testPrePaymentSubmissionValidation(){
		
		Token token = new Token();
		Map<String, String> map = new HashMap<>();
		map.put("channelId", "test");
		token.setSeviceParams(map);
		reqHeaderAtrributes.setToken(token);
		when(reqHeaderAtrributes.getToken()).thenReturn(token);
		
		
		CustomPaymentSetupPOSTResponse paymentSubmissionPOSTResponse = new CustomPaymentSetupPOSTResponse();
		PaymentSetupValidationResponse paymentSetupValidationResponse = new PaymentSetupValidationResponse();
		paymentSetupValidationResponse.setPaymentSetupValidationStatus("Rejected");
		when(reqHeaderAtrributes.getPsuId()).thenReturn("test");
		when(reqHeaderAtrributes.getCorrelationId()).thenReturn("test");
		when(authorizationValidationRoutingAdapter.preAuthorizationPaymentValidation(anyObject(), anyObject())).thenReturn(paymentSetupValidationResponse);
		PaymentResponseInfo params = paymentSubmissionAdapterHelperImpl.prePaymentSubmissionValidation(paymentSubmissionPOSTResponse);
		assertNotNull(params);
		/*
		 * 
		 */
		paymentSetupValidationResponse.setPaymentSetupValidationStatus("test");
		params = paymentSubmissionAdapterHelperImpl.prePaymentSubmissionValidation(paymentSubmissionPOSTResponse);
		assertNotNull(params);
		
	}
	
	@Test
	public void testExecutePaymentSubmission() {
		
		Token token = new Token();
		Map<String, String> map = new HashMap<>();
		map.put("channelId", "test");
		token.setSeviceParams(map);
		reqHeaderAtrributes.setToken(token);
		when(reqHeaderAtrributes.getToken()).thenReturn(token);
		
		CustomPaymentSubmissionPOSTRequest paymentSubmissionPOSTRequest = new CustomPaymentSubmissionPOSTRequest();
		DateTime createdOn = new DateTime();
		PaymentSubmissionExecutionResponse response = new PaymentSubmissionExecutionResponse();
		when(submissionExecutionRoutingAdapter.executePaymentSubmission(anyObject(), anyObject())).thenReturn(response);
		PaymentSubmissionExecutionResponse executionResponse = paymentSubmissionAdapterHelperImpl.executePaymentSubmission(paymentSubmissionPOSTRequest);
		assertNotNull(executionResponse);
	}
}
