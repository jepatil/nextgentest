package com.capgemini.psd2.pisp.payment.submission.routing.test.adapter.impl;

import java.util.Map;

import com.capgemini.psd2.pisp.adapter.PaymentSubmissionExecutionAdapter;
import com.capgemini.psd2.pisp.domain.PaymentSubmissionExecutionResponse;
import com.capgemini.psd2.pisp.domain.CustomPaymentSubmissionPOSTRequest;

public class PaymentSubmissionExecutionAdapterMock implements PaymentSubmissionExecutionAdapter{

	@Override
	public PaymentSubmissionExecutionResponse executePaymentSubmission(
			CustomPaymentSubmissionPOSTRequest paymentSubmissionRequest, Map<String, String> params) {
		return new PaymentSubmissionExecutionResponse();
	}

}
