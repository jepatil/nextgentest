package com.capgemini.psd2.pisp.validation.adapter;

import org.springframework.stereotype.Component;

import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomPaymentSubmissionPOSTRequest;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;
import com.capgemini.psd2.pisp.domain.SubmissionRetrieveGetRequest;

@Component
public interface PaymentValidator {
	public boolean validatePaymentSetupRequest(CustomPaymentSetupPOSTRequest paymentSetupRequest);
	public boolean validatePaymentRetrieveRequest(PaymentRetrieveGetRequest paymentRetrieveRequest);
	public boolean validatePaymentSubmissionRequest(CustomPaymentSubmissionPOSTRequest paymentSubmissionRequest);	
	public boolean validateSubmissionRetrieveRequest(SubmissionRetrieveGetRequest submissionRetrieveRequest);
}
