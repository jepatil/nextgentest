package com.capgemini.psd2.pisp.validation.adapter.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.constraints.EnumPaymentConstraint;
import com.capgemini.psd2.pisp.domain.CreditorAccount;
import com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum;
import com.capgemini.psd2.pisp.domain.CreditorAgent;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomPaymentSubmissionPOSTRequest;
import com.capgemini.psd2.pisp.domain.DebtorAccount;
import com.capgemini.psd2.pisp.domain.DebtorAgent;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;
import com.capgemini.psd2.pisp.domain.PaymentSetupInitiation;
import com.capgemini.psd2.pisp.domain.PaymentSetupInitiationInstructedAmount;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponseInitiation;
import com.capgemini.psd2.pisp.domain.PaymentSubmission;
import com.capgemini.psd2.pisp.domain.RemittanceInformation;
import com.capgemini.psd2.pisp.domain.Risk;
import com.capgemini.psd2.pisp.domain.Risk.PaymentContextCodeEnum;
import com.capgemini.psd2.pisp.domain.RiskDeliveryAddress;
import com.capgemini.psd2.pisp.domain.SubmissionRetrieveGetRequest;
import com.capgemini.psd2.pisp.validation.PispUtilities;
import com.capgemini.psd2.pisp.validation.adapter.PaymentValidator;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;

@Component
public class PaymentValidatorImpl implements PaymentValidator {

	@Autowired
	@Qualifier("PSD2RequestValidator")
	private PSD2Validator psd2Validator;

	@Value("${app.regex.paymentId:#{null}}")
	private String paymentIdRegexValidator;
	
	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Override
	public boolean validatePaymentSetupRequest(CustomPaymentSetupPOSTRequest paymentSetupRequest) {

		validateHeaders();
		if (NullCheckUtils.isNullOrEmpty(paymentSetupRequest))
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_INVALID_PAYMENT_SETUP_REQUEST_PAYLOAD);

		if (NullCheckUtils.isNullOrEmpty(paymentSetupRequest.getData()))
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_PAYMENT_DATA_FIELD_MISSING);

		validatePaymentInitiation(paymentSetupRequest.getData().getInitiation());
		validateInstructedAmount(paymentSetupRequest.getData().getInitiation().getInstructedAmount());
		validateCreditorAccountDetails(paymentSetupRequest.getData().getInitiation().getCreditorAccount(),
				paymentSetupRequest.getData().getInitiation().getCreditorAgent());
		validateDebtorDetails(paymentSetupRequest.getData().getInitiation().getDebtorAgent(),
				paymentSetupRequest.getData().getInitiation().getDebtorAccount());
		validateRemittanceInformation(paymentSetupRequest.getData().getInitiation().getRemittanceInformation());
		validateRisk(paymentSetupRequest.getRisk());
		validateRiskDeliveryAddress(paymentSetupRequest.getRisk().getDeliveryAddress());

		return Boolean.TRUE;

	}

	@Override
	public boolean validatePaymentRetrieveRequest(PaymentRetrieveGetRequest paymentRetrieveRequest) {
		psd2Validator.validate(paymentRetrieveRequest);
		return Boolean.TRUE;
	}

	@Override
	public boolean validatePaymentSubmissionRequest(CustomPaymentSubmissionPOSTRequest paymentSubmissionRequest) {

		validateHeaders();

		if (NullCheckUtils.isNullOrEmpty(paymentSubmissionRequest))
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_INVALID_PAYMENT_SUBMISSION_REQUEST_PAYLOAD);

		
		validatePaymentSubmissionData(paymentSubmissionRequest.getData());
		validatePaymentId(paymentSubmissionRequest.getData().getPaymentId());
		validateSubmissionInitiation(paymentSubmissionRequest.getData().getInitiation());
		validateInstructedAmount(paymentSubmissionRequest.getData().getInitiation().getInstructedAmount());
		validateCreditorAccountDetails(paymentSubmissionRequest.getData().getInitiation().getCreditorAccount(),
				paymentSubmissionRequest.getData().getInitiation().getCreditorAgent());
		validateDebtorDetails(paymentSubmissionRequest.getData().getInitiation().getDebtorAgent(),
				paymentSubmissionRequest.getData().getInitiation().getDebtorAccount());
		validateRemittanceInformation(paymentSubmissionRequest.getData().getInitiation().getRemittanceInformation());
		validateRisk(paymentSubmissionRequest.getRisk());
		validateRiskDeliveryAddress(paymentSubmissionRequest.getRisk().getDeliveryAddress());

		return Boolean.TRUE;
	}

	@Override
	public boolean validateSubmissionRetrieveRequest(SubmissionRetrieveGetRequest submissionRetrieveRequest) {
		psd2Validator.validate(submissionRetrieveRequest);
		return Boolean.TRUE;
	}

	/*
	 * Added on 14-July-2017
	 */

	private void validatePaymentSubmissionData(PaymentSubmission data) {
		if (NullCheckUtils.isNullOrEmpty(data))
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_SUBMISSION_DATA_FIELD_MISSING);
		psd2Validator.validate(data);
	}

	private void validateSubmissionInitiation(PaymentSetupResponseInitiation intiation) {

		if (NullCheckUtils.isNullOrEmpty(intiation))
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_SUBMISSION_INITIATION_FIELD_MISSING);
		psd2Validator.validate(intiation);
	}

	private void validatePaymentInitiation(PaymentSetupInitiation intiation) {

		if (NullCheckUtils.isNullOrEmpty(intiation))
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_PAYMENT_INITIATION_FIELD_MISSING);
		psd2Validator.validate(intiation);
	}

	private void validateInstructedAmount(PaymentSetupInitiationInstructedAmount instructedAmount) {
		if (NullCheckUtils.isNullOrEmpty(instructedAmount))
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_INSTRUCTED_AMOUNT_FIELD_MISSING);
		psd2Validator.validate(instructedAmount);
		PispUtilities.isValidCurrency(instructedAmount.getCurrency());

	}

	private void validateCreditorAccountDetails(CreditorAccount creditorAccount, CreditorAgent creditorAgent) {
		if (NullCheckUtils.isNullOrEmpty(creditorAccount))
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_CREDITOR_ACCOUNT_FIELD_MISSING);

		psd2Validator.validate(creditorAccount);
		
		if (SchemeNameEnum.SORTCODEACCOUNTNUMBER.equals(creditorAccount.getSchemeName())) {
			if (!NullCheckUtils.isNullOrEmpty(creditorAgent))
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_CREDITOR_AGENT_FIELD_NOT_REQUIRED);

			if (! creditorAccount.getIdentification().matches("^[0-9]{6}[0-9]{8}$"))
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_CREDITOR_AGENT_IDENTIFICATION_INVALID);
		} else if (SchemeNameEnum.IBAN.equals(creditorAccount.getSchemeName())) {
				if (NullCheckUtils.isNullOrEmpty(creditorAgent))
					throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_CREDITOR_AGENT_FIELD_MISSING);
				PispUtilities.validateIBAN(creditorAccount.getIdentification());
				psd2Validator.validate(creditorAgent);
				
		}
				

	}

	private void validateDebtorDetails(DebtorAgent debtorAgent, DebtorAccount debtorAccount) {
		if (!NullCheckUtils.isNullOrEmpty(debtorAccount)){
			psd2Validator.validate(debtorAccount);
			
			if(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.SORTCODEACCOUNTNUMBER.equals(debtorAccount.getSchemeName())){
				if (!NullCheckUtils.isNullOrEmpty(debtorAgent))
					throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_DEBITOR_AGENT_FIELD_NOT_REQUIRED);

				if (! debtorAccount.getIdentification().matches("^[0-9]{6}[0-9]{8}$"))
					throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_DEBITOR_AGENT_IDENTIFICATION_INVALID);
			}else if(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.IBAN.equals(debtorAccount.getSchemeName())){
				if (NullCheckUtils.isNullOrEmpty(debtorAgent))
					throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_DEBITOR_AGENT_FIELD_MISSING);
				PispUtilities.validateIBAN(debtorAccount.getIdentification());
				psd2Validator.validate(debtorAgent);
			}
			
		}

	}

	private void validateRemittanceInformation(RemittanceInformation remittanceInformation) {

		if (!NullCheckUtils.isNullOrEmpty(remittanceInformation))
			psd2Validator.validate(remittanceInformation);

	}

	private void validateRisk(Risk risk) {
		if (NullCheckUtils.isNullOrEmpty(risk))
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_RISK_FIELD_MISSING);
		psd2Validator.validate(risk);
		validatePaymentContext(risk);
	}

	private void validatePaymentContext(Risk risk) {
		if (risk.getPaymentContextCode() != null) {
			if ((PaymentContextCodeEnum.ECOMMERCEGOODS.equals(risk.getPaymentContextCode())
					|| PaymentContextCodeEnum.ECOMMERCESERVICES.equals(risk.getPaymentContextCode()))
					&& (NullCheckUtils.isNullOrEmpty(risk.getMerchantCategoryCode())
							|| NullCheckUtils.isNullOrEmpty(risk.getMerchantCustomerIdentification()))) {

				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_PAYMENT_CONTEXT_VALIDATION);
			}

			if (PaymentContextCodeEnum.ECOMMERCEGOODS.equals(risk.getPaymentContextCode())
					&& NullCheckUtils.isNullOrEmpty(risk.getDeliveryAddress())) {

				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_PAYMENT_CONTEXT_VALIDATION_RISK_ADDRESS);
			}

		}
	}

	private void validateRiskDeliveryAddress(RiskDeliveryAddress deliveryAddress) {
		if (!NullCheckUtils.isNullOrEmpty(deliveryAddress)) {

			psd2Validator.validate(deliveryAddress);
			validateAddressLine(deliveryAddress.getAddressLine());
			validateCountrySubDivision(deliveryAddress.getCountrySubDivision());
			PispUtilities.validateISOCountry(deliveryAddress.getCountry());
		}

	}

	private void validateAddressLine(List<String> addressLine) {
		if (!NullCheckUtils.isNullOrEmpty(addressLine)) {
			for (String addressLineValue : addressLine) {
				if (!NullCheckUtils.isNullOrEmpty(addressLineValue)
						&& addressLineValue.length() > EnumPaymentConstraint.ADDRESSLINE_LENGTH.getSize())
					throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_ADDRESS_LINE_LENGTH_NOT_VALID);
			}
		}
	}

	private void validateCountrySubDivision(List<String> countrySubDivision) {
		if (!NullCheckUtils.isNullOrEmpty(countrySubDivision)) {
			for (String countrySubDivisionValue : countrySubDivision) {
				if (!NullCheckUtils.isNullOrEmpty(countrySubDivisionValue)
						&& countrySubDivisionValue.length() > EnumPaymentConstraint.COUNTRYSUBDIVISION_LENGTH.getSize())
					throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_COUNTRY_SUBDIVISION_LENGTH_NOT_VALID);
			}
		}
	}

	public void validateHeaders() {
		if (NullCheckUtils.isNullOrEmpty(reqHeaderAtrributes.getIdempotencyKey()))
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_IDEMPOTENCY_KEY_NOT_FOUND);
		if (!reqHeaderAtrributes.getIdempotencyKey().matches("^(?!\\s)(.*)(\\S)$"))
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_INVALID_IDEMPOTENCY_KEY_STRUCTURE);
		if (reqHeaderAtrributes.getIdempotencyKey().trim().length() > EnumPaymentConstraint.IDEMPOTENCY_KEY_LENGTH
				.getSize())
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_INVALID_IDEMPOTENCY_KEY_LENGTH);
	}
	
	private void validatePaymentId(String paymentId) {
		if(!paymentId.matches(paymentIdRegexValidator)) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_SUBMISSION_INVALID_PAYMENT_ID);
		}
	}

}
