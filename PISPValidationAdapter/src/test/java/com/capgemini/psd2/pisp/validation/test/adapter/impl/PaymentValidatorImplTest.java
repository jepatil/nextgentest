package com.capgemini.psd2.pisp.validation.test.adapter.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomPaymentSubmissionPOSTRequest;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;
import com.capgemini.psd2.pisp.domain.SubmissionRetrieveGetRequest;
import com.capgemini.psd2.pisp.validation.adapter.impl.PaymentValidatorImpl;
import com.capgemini.psd2.pisp.validation.test.mock.data.PaymentSetupRequestResponseMockData;
import com.capgemini.psd2.validator.PSD2Validator;

@RunWith(SpringJUnit4ClassRunner.class)
public class PaymentValidatorImplTest {

	@Mock
	private PSD2Validator psd2Validator;
	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	@InjectMocks
	private PaymentValidatorImpl validatorImpl = new PaymentValidatorImpl();

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		// this.mockMvc =
		// MockMvcBuilders.standaloneSetup(adapterImpl).dispatchOptions(true).build();
	}

	@Test
	public void testValidatePaymentRetrieveRequestPositive() {

		PaymentRetrieveGetRequest paymentRetrieveRequest = new PaymentRetrieveGetRequest();
		paymentRetrieveRequest.setPaymentId("12345");
		Mockito.doNothing().when(psd2Validator).validate(any(PaymentRetrieveGetRequest.class));
		boolean returnValue = validatorImpl.validatePaymentRetrieveRequest(paymentRetrieveRequest);
		assertEquals(returnValue, true);

	}

	@Test
	public void testValidateSubmissionRetrieveRequestPositive() {
		ReflectionTestUtils.setField(validatorImpl, "paymentIdRegexValidator", "[a-zA-Z0-9-_]{1,128}");
		SubmissionRetrieveGetRequest paymentRetrieveRequest = new SubmissionRetrieveGetRequest();
		paymentRetrieveRequest.setPaymentSubmissionId("12345");
		Mockito.doNothing().when(psd2Validator).validate(any(SubmissionRetrieveGetRequest.class));
		boolean returnValue = validatorImpl.validateSubmissionRetrieveRequest(paymentRetrieveRequest);
		assertEquals(returnValue, true);
	}

	/*
	 * Idempotency key null check
	 */
	@Test(expected = PSD2Exception.class)
	public void testValidatePaymentSetupRequestIdempotencyNullException() {
		CustomPaymentSetupPOSTRequest paymentSetupRequestMockData = PaymentSetupRequestResponseMockData
				.getPaymentSetupPostMockRequest();
		validatorImpl.validatePaymentSetupRequest(paymentSetupRequestMockData);
	}

	/*
	 * Idempotency key length validation
	 */
	@Test(expected = PSD2Exception.class)
	public void testValidatePaymentSetupRequestIdempotencyLengthException() {
		CustomPaymentSetupPOSTRequest paymentSetupRequestMockData = PaymentSetupRequestResponseMockData
				.getPaymentSetupPostMockRequest();
		when(reqHeaderAtrributes.getIdempotencyKey())
				.thenReturn("fkshhfksfshhfsfsnfnfsfslfksjfjsflsfsfjjsfsfsflskfjsflsfsfhjkslfsfkslfsfflkfslkfslkk");
		validatorImpl.validatePaymentSetupRequest(paymentSetupRequestMockData);
	}

	/*
	 * If PaymentSubmissionPOSTRequest is null
	 */
	@Test(expected = PSD2Exception.class)
	public void testValidatePaymentSubmissionRequestNullException() {
		when(reqHeaderAtrributes.getIdempotencyKey()).thenReturn("fkshhfksfshhfsfsn");
		validatorImpl.validatePaymentSubmissionRequest(null);
	}

	/*
	 * If PaymentSetupPOSTRequest is null
	 */
	@Test(expected = PSD2Exception.class)
	public void testValidatePaymentSetupRequestNullException() {
		when(reqHeaderAtrributes.getIdempotencyKey()).thenReturn("fkshhfksfshhfsfsn");
		validatorImpl.validatePaymentSetupRequest(null);
	}

	/*
	 * If PaymentSetupPOSTRequest.data is null
	 */
	@Test(expected = PSD2Exception.class)
	public void testValidatePaymentSetupRequestDataNullException() {
		CustomPaymentSetupPOSTRequest paymentSetupRequestMockData = PaymentSetupRequestResponseMockData
				.getPaymentSetupPostMockRequest();
		paymentSetupRequestMockData.setData(null);
		when(reqHeaderAtrributes.getIdempotencyKey()).thenReturn("fkshhfksfshhfsfsn");
		validatorImpl.validatePaymentSetupRequest(paymentSetupRequestMockData);
	}

	/*
	 * If PaymentSubmissionPOSTRequest.data is null
	 */
	@Test(expected = PSD2Exception.class)
	public void testValidatePaymentSubmissionRequestDataNullException() {
		CustomPaymentSubmissionPOSTRequest paymentSubmissionRequestMockData = PaymentSetupRequestResponseMockData
				.getPaymentSubmissionPostMockRequest();
		paymentSubmissionRequestMockData.setData(null);
		when(reqHeaderAtrributes.getIdempotencyKey()).thenReturn("fkshhfksfshhfsfsn");
		validatorImpl.validatePaymentSubmissionRequest(paymentSubmissionRequestMockData);
	}

	/*
	 * If PaymentSetupPOSTRequest.data.initiation is null
	 */
	@Test(expected = PSD2Exception.class)
	public void testValidatePaymentSetupRequestDataInitiationNullException() {
		CustomPaymentSetupPOSTRequest paymentSetupRequestMockData = PaymentSetupRequestResponseMockData
				.getPaymentSetupPostMockRequest();
		paymentSetupRequestMockData.getData().setInitiation(null);
		when(reqHeaderAtrributes.getIdempotencyKey()).thenReturn("fkshhfksfshhfsfsn");
		validatorImpl.validatePaymentSetupRequest(paymentSetupRequestMockData);
	}

	/*
	 * If PaymentSubmissionPOSTRequest.data.initiation is null
	 */
	@Test(expected = PSD2Exception.class)
	public void testValidatePaymentSubmissionRequestDataInitiationNullException() {
		ReflectionTestUtils.setField(validatorImpl, "paymentIdRegexValidator", "[a-zA-Z0-9-_]{1,128}");
		CustomPaymentSubmissionPOSTRequest paymentSubmissionRequestMockData = PaymentSetupRequestResponseMockData
				.getPaymentSubmissionPostMockRequest();
		paymentSubmissionRequestMockData.getData().setInitiation(null);
		when(reqHeaderAtrributes.getIdempotencyKey()).thenReturn("fkshhfksfshhfsfsn");
		validatorImpl.validatePaymentSubmissionRequest(paymentSubmissionRequestMockData);
	}

	/*
	 * If PaymentSetupPOSTRequest.data.initiation.InstructedAmount is null
	 */
	@Test(expected = PSD2Exception.class)
	public void testValidatePaymentSetupRequestInstructedAmountNullException() {
		CustomPaymentSetupPOSTRequest paymentSetupRequestMockData = PaymentSetupRequestResponseMockData
				.getPaymentSetupPostMockRequest();
		paymentSetupRequestMockData.getData().getInitiation().setInstructedAmount(null);
		when(reqHeaderAtrributes.getIdempotencyKey()).thenReturn("fkshhfksfshhfsfsn");
		validatorImpl.validatePaymentSetupRequest(paymentSetupRequestMockData);
	}

	/*
	 * If PaymentSetupPOSTRequest.data.initiation.CreditorAgent is null
	 */
	@Test(expected = PSD2Exception.class)
	public void testValidatePaymentSetupRequestCreditorAgentNullException() {
		CustomPaymentSetupPOSTRequest paymentSetupRequestMockData = PaymentSetupRequestResponseMockData
				.getPaymentSetupPostMockRequest();
		paymentSetupRequestMockData.getData().getInitiation().setCreditorAgent(null);
		when(reqHeaderAtrributes.getIdempotencyKey()).thenReturn("fkshhfksfshhfsfsn");
		validatorImpl.validatePaymentSetupRequest(paymentSetupRequestMockData);
	}

	/*
	 * If PaymentSetupPOSTRequest.data.initiation.CreditorAccount is null
	 */
	@Test(expected = PSD2Exception.class)
	public void testValidatePaymentSetupRequestCreditorAccountNullException() {
		CustomPaymentSetupPOSTRequest paymentSetupRequestMockData = PaymentSetupRequestResponseMockData
				.getPaymentSetupPostMockRequest();
		paymentSetupRequestMockData.getData().getInitiation().setCreditorAccount(null);
		when(reqHeaderAtrributes.getIdempotencyKey()).thenReturn("fkshhfksfshhfsfsn");
		validatorImpl.validatePaymentSetupRequest(paymentSetupRequestMockData);
	}

	/*
	 * If PaymentSetupPOSTRequest.data.initiation.DebtorAccount is null &
	 * DebtorAgent is not null
	 */
	@Test(expected = PSD2Exception.class)
	public void testValidatePaymentSetupRequestDebtorAccountNullException() {
		CustomPaymentSetupPOSTRequest paymentSetupRequestMockData = PaymentSetupRequestResponseMockData
				.getPaymentSetupPostMockRequest();
		paymentSetupRequestMockData.getData().getInitiation().setDebtorAgent(null);
		when(reqHeaderAtrributes.getIdempotencyKey()).thenReturn("fkshhfksfshhfsfsn");
		validatorImpl.validatePaymentSetupRequest(paymentSetupRequestMockData);
	}

	/*
	 * If PaymentSetupPOSTRequest.data.initiation.DebtorAccount is not null &
	 * DebtorAgent is null
	 */
	@Test(expected = PSD2Exception.class)
	public void testValidatePaymentSetupRequestDebtorAgentNullException() {
		CustomPaymentSetupPOSTRequest paymentSetupRequestMockData = PaymentSetupRequestResponseMockData
				.getPaymentSetupPostMockRequest();
		paymentSetupRequestMockData.getData().getInitiation().setDebtorAgent(null);
		when(reqHeaderAtrributes.getIdempotencyKey()).thenReturn("fkshhfksfshhfsfsn");
		validatorImpl.validatePaymentSetupRequest(paymentSetupRequestMockData);
	}

	/*
	 * If PaymentSetupPOSTRequest.Risk.DeliveryAddress.AddressLine.list is
	 * having values greater than defined size
	 */
	@Test(expected = PSD2Exception.class)
	public void testValidatePaymentSetupRequestDeliveryAddressAddressLineValueLengthException() {
		CustomPaymentSetupPOSTRequest paymentSetupRequestMockData = PaymentSetupRequestResponseMockData
				.getPaymentSetupPostMockRequest();
		paymentSetupRequestMockData.getRisk().getDeliveryAddress().getAddressLine().set(0,
				"kjasdadasdasddlkdadadnadada;ldaddjkldasdadkadadadmadadaddlakdadadadadldaddadajdlddlddk");
		paymentSetupRequestMockData.getRisk().getDeliveryAddress().getAddressLine().set(1,
				"kjasdadasdasddlkdadadnadada;ldaddjkldasdadkadadadmadadaddlakdadadadadldaddadajdlddlddk");
		when(reqHeaderAtrributes.getIdempotencyKey()).thenReturn("fkshhfksfshhfsfsn");
		validatorImpl.validatePaymentSetupRequest(paymentSetupRequestMockData);
	}

	/*
	 * If PaymentSetupPOSTRequest.Risk.DeliveryAddress.CountrySubDivision.list
	 * is having values greater than defined size
	 */
	@Test(expected = PSD2Exception.class)
	public void testValidatePaymentSetupRequestDeliveryAddressCountrySubDivisionValueLengthException() {
		CustomPaymentSetupPOSTRequest paymentSetupRequestMockData = PaymentSetupRequestResponseMockData
				.getPaymentSetupPostMockRequest();
		paymentSetupRequestMockData.getRisk().getDeliveryAddress().getCountrySubDivision().set(0,
				"kjasdadasdasddlkdadadnadada;ldaddjkldasdadkadadadmadadaddlakdadadadadldaddadajdlddlddk");
		paymentSetupRequestMockData.getRisk().getDeliveryAddress().getCountrySubDivision().set(1,
				"kjasdadasdasddlkdadadnadada;ldaddjkldasdadkadadadmadadaddlakdadadadadldaddadajdlddlddk");
		when(reqHeaderAtrributes.getIdempotencyKey()).thenReturn("fkshhfksfshhfsfsn");
		validatorImpl.validatePaymentSetupRequest(paymentSetupRequestMockData);
	}

	/*
	 * If PaymentSetupPOSTRequest.Risk.DeliveryAddress.CountryCode is invalid
	 */
	@Test(expected = PSD2Exception.class)
	public void testValidatePaymentSetupRequestDeliveryAddressCountryCodeInvalidException() {
		CustomPaymentSetupPOSTRequest paymentSetupRequestMockData = PaymentSetupRequestResponseMockData
				.getPaymentSetupPostMockRequest();
		paymentSetupRequestMockData.getRisk().getDeliveryAddress().setCountry("ABC");
		when(reqHeaderAtrributes.getIdempotencyKey()).thenReturn("fkshhfksfshhfsfsn");
		validatorImpl.validatePaymentSetupRequest(paymentSetupRequestMockData);
	}

	/*
	 * If PaymentSetupPOSTRequest.Risk is null
	 */
	@Test(expected = PSD2Exception.class)
	public void testValidatePaymentSetupRequestRiskNullException() {
		CustomPaymentSetupPOSTRequest paymentSetupRequestMockData = PaymentSetupRequestResponseMockData
				.getPaymentSetupPostMockRequest();
		paymentSetupRequestMockData.setRisk(null);
		when(reqHeaderAtrributes.getIdempotencyKey()).thenReturn("fkshhfksfshhfsfsn");
		validatorImpl.validatePaymentSetupRequest(paymentSetupRequestMockData);
	}

	/*
	 * If PaymentSetupPOSTRequest.data.initiation.RemittanceInformation is null
	 */
	@Test
	public void testValidatePaymentSubmissionRequestPositive() {
		ReflectionTestUtils.setField(validatorImpl, "paymentIdRegexValidator", "[a-zA-Z0-9-_]{1,128}");
		CustomPaymentSubmissionPOSTRequest paymentSubmissionRequestMockData = PaymentSetupRequestResponseMockData
				.getPaymentSubmissionPostMockRequest();
		when(reqHeaderAtrributes.getIdempotencyKey()).thenReturn("fkshhfksfshhfsfsn");
		boolean returnValue = validatorImpl.validatePaymentSubmissionRequest(paymentSubmissionRequestMockData);
		assertEquals(true, returnValue);
	}

	/*
	 * If PaymentSetupPOSTRequest.data.initiation.RemittanceInformation is null
	 */
	@Test
	public void testValidatePaymentSetupRequestRemittanceInformationNullPositive() {
		CustomPaymentSetupPOSTRequest paymentSetupRequestMockData = PaymentSetupRequestResponseMockData
				.getPaymentSetupPostMockRequest();
		paymentSetupRequestMockData.getData().getInitiation().setRemittanceInformation(null);
		when(reqHeaderAtrributes.getIdempotencyKey()).thenReturn("fkshhfksfshhfsfsn");
		boolean returnValue = validatorImpl.validatePaymentSetupRequest(paymentSetupRequestMockData);
		assertEquals(true, returnValue);
	}

	/*
	 * If PaymentContextCodeEnum is ECOMMERCEGOODS and
	 * PaymentSetupPOSTRequest.Risk.DeliveryAddress is not null
	 */
	@Test
	public void testValidatePaymentSetupRequestDeliveryAddressPositiveWithECOMMERCEGOODS() {
		CustomPaymentSetupPOSTRequest paymentSetupRequestMockData = PaymentSetupRequestResponseMockData
				.getPaymentSetupPostMockRequest();
		when(reqHeaderAtrributes.getIdempotencyKey()).thenReturn("fkshhfksfshhfsfsn");
		boolean returnValue = validatorImpl.validatePaymentSetupRequest(paymentSetupRequestMockData);
		assertEquals(true, returnValue);
	}

	/*
	 * If PaymentSetupPOSTRequest.Risk.DeliveryAddress is null
	 */
	@Test
	public void testValidatePaymentSetupRequestDeliveryAddressNullPositive() {
		CustomPaymentSetupPOSTRequest paymentSetupRequestMockData = PaymentSetupRequestResponseMockData
				.getPaymentSetupPostMockRequest();
		paymentSetupRequestMockData.getRisk()
				.setPaymentContextCode(paymentSetupRequestMockData.getRisk().getPaymentContextCode().OTHER);
		paymentSetupRequestMockData.getRisk().setDeliveryAddress(null);
		when(reqHeaderAtrributes.getIdempotencyKey()).thenReturn("fkshhfksfshhfsfsn");
		boolean returnValue = validatorImpl.validatePaymentSetupRequest(paymentSetupRequestMockData);
		assertEquals(true, returnValue);
	}

	/*
	 * If PaymentSetupPOSTRequest.Risk.DeliveryAddress.AddressLine is null
	 */
	@Test
	public void testValidatePaymentSetupRequestDeliveryAddressAddressLineNullPositive() {
		CustomPaymentSetupPOSTRequest paymentSetupRequestMockData = PaymentSetupRequestResponseMockData
				.getPaymentSetupPostMockRequest();
		paymentSetupRequestMockData.getRisk().getDeliveryAddress().setAddressLine(null);
		when(reqHeaderAtrributes.getIdempotencyKey()).thenReturn("fkshhfksfshhfsfsn");
		boolean returnValue = validatorImpl.validatePaymentSetupRequest(paymentSetupRequestMockData);
		assertEquals(true, returnValue);
	}

	/*
	 * If PaymentSetupPOSTRequest.Risk.DeliveryAddress.AddressLine.list is blank
	 */
	@Test
	public void testValidatePaymentSetupRequestDeliveryAddressAddressLineListBlankPositive() {
		CustomPaymentSetupPOSTRequest paymentSetupRequestMockData = PaymentSetupRequestResponseMockData
				.getPaymentSetupPostMockRequest();
		paymentSetupRequestMockData.getRisk().getDeliveryAddress().getAddressLine().set(0, "");
		paymentSetupRequestMockData.getRisk().getDeliveryAddress().getAddressLine().set(1, "");
		when(reqHeaderAtrributes.getIdempotencyKey()).thenReturn("fkshhfksfshhfsfsn");
		boolean returnValue = validatorImpl.validatePaymentSetupRequest(paymentSetupRequestMockData);
		assertEquals(true, returnValue);
	}

	/*
	 * If PaymentSetupPOSTRequest.Risk.DeliveryAddress.CountrySubDivision is
	 * null
	 */
	@Test
	public void testValidatePaymentSetupRequestDeliveryAddressCountrySubDivisionNullPositive() {
		CustomPaymentSetupPOSTRequest paymentSetupRequestMockData = PaymentSetupRequestResponseMockData
				.getPaymentSetupPostMockRequest();
		paymentSetupRequestMockData.getRisk().getDeliveryAddress().setCountrySubDivision(null);
		when(reqHeaderAtrributes.getIdempotencyKey()).thenReturn("fkshhfksfshhfsfsn");
		boolean returnValue = validatorImpl.validatePaymentSetupRequest(paymentSetupRequestMockData);
		assertEquals(true, returnValue);
	}

	/*
	 * If PaymentSetupPOSTRequest.Risk.DeliveryAddress.CountrySubDivision.list
	 * is blank
	 */
	@Test
	public void testValidatePaymentSetupRequestDeliveryAddressCountrySubDivisionListBlankPositive() {
		CustomPaymentSetupPOSTRequest paymentSetupRequestMockData = PaymentSetupRequestResponseMockData
				.getPaymentSetupPostMockRequest();
		paymentSetupRequestMockData.getRisk().getDeliveryAddress().getCountrySubDivision().set(0, "");
		paymentSetupRequestMockData.getRisk().getDeliveryAddress().getCountrySubDivision().set(1, "");
		when(reqHeaderAtrributes.getIdempotencyKey()).thenReturn("fkshhfksfshhfsfsn");
		boolean returnValue = validatorImpl.validatePaymentSetupRequest(paymentSetupRequestMockData);
		assertEquals(true, returnValue);
	}

	/*
	 * Execute request with BBAN for CreditorAccount & DebtorAccount
	 */
	@Test
	public void testValidatePaymentSetupRequestBBANPositive() {
		CustomPaymentSetupPOSTRequest paymentSetupRequestMockData = PaymentSetupRequestResponseMockData
				.getPaymentSetupPostMockRequest();
		when(reqHeaderAtrributes.getIdempotencyKey()).thenReturn("fkshhfksfshhfsfsn");
		boolean returnValue = validatorImpl.validatePaymentSetupRequest(paymentSetupRequestMockData);
		assertEquals(true, returnValue);
	}

	/*
	 * Execute request with IBAN for CreditorAccount
	 */
	@Test
	public void testValidatePaymentSetupRequestCreditorIBANPositive() {
		CustomPaymentSetupPOSTRequest paymentSetupRequestMockData = PaymentSetupRequestResponseMockData
				.getPaymentSetupPostMockRequest();
		paymentSetupRequestMockData.getData().getInitiation().getCreditorAccount().setSchemeName(SchemeNameEnum.IBAN);
		paymentSetupRequestMockData.getData().getInitiation().getCreditorAccount()
				.setIdentification("IE29AIBK93115212345678");
		when(reqHeaderAtrributes.getIdempotencyKey()).thenReturn("fkshhfksfshhfsfsn");
		boolean returnValue = validatorImpl.validatePaymentSetupRequest(paymentSetupRequestMockData);
		assertEquals(true, returnValue);
	}

	/*
	 * Execute request with IBAN for DebtorAccount
	 */
	@Test
	public void testValidatePaymentSetupRequestDebtorIBANPositive() {
		CustomPaymentSetupPOSTRequest paymentSetupRequestMockData = PaymentSetupRequestResponseMockData
				.getPaymentSetupPostMockRequest();
		paymentSetupRequestMockData.getData().getInitiation().getDebtorAccount()
				.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.IBAN);
		paymentSetupRequestMockData.getData().getInitiation().getDebtorAccount()
				.setIdentification("IE29AIBK93115212345678");
		when(reqHeaderAtrributes.getIdempotencyKey()).thenReturn("fkshhfksfshhfsfsn");
		boolean returnValue = validatorImpl.validatePaymentSetupRequest(paymentSetupRequestMockData);
		assertEquals(true, returnValue);
	}

	/*
	 * If PaymentSetupPOSTRequest.data.initiation.DebtorAccount & DebtorAgent
	 * are null
	 */
	@Test
	public void testValidatePaymentSetupRequestDebtorAgentAndAccountNullPositive() {
		CustomPaymentSetupPOSTRequest paymentSetupRequestMockData = PaymentSetupRequestResponseMockData
				.getPaymentSetupPostMockRequest();
		paymentSetupRequestMockData.getData().getInitiation().setDebtorAgent(null);
		paymentSetupRequestMockData.getData().getInitiation().setDebtorAccount(null);
		when(reqHeaderAtrributes.getIdempotencyKey()).thenReturn("fkshhfksfshhfsfsn");
		boolean returnValue = validatorImpl.validatePaymentSetupRequest(paymentSetupRequestMockData);
		assertEquals(true, returnValue);
	}

	@After
	public void tearDown() throws Exception {
		psd2Validator = null;
		reqHeaderAtrributes = null;
		validatorImpl = null;
	}

}
