package com.capgemini.psd2.pisp.validation.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.pisp.domain.CreditorAccount;
import com.capgemini.psd2.pisp.domain.CreditorAgent;
import com.capgemini.psd2.pisp.domain.CreditorAgent.SchemeNameEnum;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomPaymentSubmissionPOSTRequest;
import com.capgemini.psd2.pisp.domain.DebtorAccount;
import com.capgemini.psd2.pisp.domain.DebtorAgent;
import com.capgemini.psd2.pisp.domain.PaymentSetup;
import com.capgemini.psd2.pisp.domain.PaymentSetupInitiation;
import com.capgemini.psd2.pisp.domain.PaymentSetupInitiationInstructedAmount;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseLinks;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseMeta;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse.StatusEnum;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponseInitiation;
import com.capgemini.psd2.pisp.domain.PaymentSubmission;
import com.capgemini.psd2.pisp.domain.RemittanceInformation;
import com.capgemini.psd2.pisp.domain.Risk;
import com.capgemini.psd2.pisp.domain.Risk.PaymentContextCodeEnum;
import com.capgemini.psd2.pisp.domain.RiskDeliveryAddress;
import com.capgemini.psd2.pisp.validation.PispUtilities;
import com.fasterxml.jackson.databind.ObjectMapper;

public class PaymentSetupRequestResponseMockData {
	public static CustomPaymentSetupPOSTRequest getPaymentSetupPostMockRequest() {
		CustomPaymentSetupPOSTRequest request = new CustomPaymentSetupPOSTRequest();
		PaymentSetup data = new PaymentSetup();
		request.setData(data);
		Risk risk = new Risk();
		request.setRisk(risk);
		
		PaymentSetupInitiation initiation = new PaymentSetupInitiation();
		data.setInitiation(initiation);
		
		initiation.setInstructionIdentification("ABDDC");
		initiation.setEndToEndIdentification("FRESCO.21302.GFX.28");
		
		PaymentSetupInitiationInstructedAmount instructedAmount = new PaymentSetupInitiationInstructedAmount();
		instructedAmount.setAmount("777777777.00");
		instructedAmount.setCurrency("EUR");
		initiation.setInstructedAmount(instructedAmount);
		
		CreditorAgent creditorAgent = new CreditorAgent();
		creditorAgent.setSchemeName(SchemeNameEnum.BICFI);
		creditorAgent.setIdentification("SC080801");
		initiation.setCreditorAgent(creditorAgent);
		
		CreditorAccount creditorAccount = new CreditorAccount();
		creditorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.IBAN);
		creditorAccount.setIdentification("FR1420041010050500013M02606");
		creditorAccount.setName("Test user");
		creditorAccount.setSecondaryIdentification("0002");
		initiation.setCreditorAccount(creditorAccount);
		
		DebtorAgent debtorAgent = new DebtorAgent();
		debtorAgent.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAgent.SchemeNameEnum.BICFI);
		debtorAgent.setIdentification("SC112800");
		initiation.setDebtorAgent(debtorAgent);
		
		DebtorAccount debtorAccount = new DebtorAccount();
		debtorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.IBAN);
		debtorAccount.setIdentification("GB29NWBK60161331926819");
		debtorAccount.setName("Andrea Smith");
		debtorAccount.setSecondaryIdentification("0002");
		initiation.setDebtorAccount(debtorAccount);
		
		RemittanceInformation remittanceInformation = new RemittanceInformation();
		remittanceInformation.setReference("FRESCO-101");
		remittanceInformation.setUnstructured("Internal ops code 5120101");
		initiation.setRemittanceInformation(remittanceInformation);
		
		risk.setMerchantCategoryCode("5967");
		risk.setMerchantCustomerIdentification("053598653254");
		risk.setPaymentContextCode(PaymentContextCodeEnum.ECOMMERCEGOODS);
		
		RiskDeliveryAddress deliveryAddress = new RiskDeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		
		List<String> addressLine = new ArrayList<>();
		addressLine.add("Flat 7");
		addressLine.add("Acacia Lodge");		
		deliveryAddress.setAddressLine(addressLine);
		
		deliveryAddress.setBuildingNumber("27");
		deliveryAddress.setCountry("GB");
		
		List<String> countrySubDivision = new ArrayList<>();
		countrySubDivision.add("ABCDABCDABCDABCDAB");
		countrySubDivision.add("DEFG");
		deliveryAddress.setCountrySubDivision(countrySubDivision);
		
		deliveryAddress.setPostCode("GU31 2ZZ");
		deliveryAddress.setStreetName("AcaciaAvenue");
		deliveryAddress.setTownName("Sparsholt");
		
				
		return request;
		
	
	}
	
	public static CustomPaymentSetupPOSTResponse getPaymentSetupPostMockResponse(){
		CustomPaymentSetupPOSTResponse response = new CustomPaymentSetupPOSTResponse();
		PaymentSetupResponse data = new PaymentSetupResponse();
		response.setData(data);
		data.setPaymentId("12345");
		data.setCreationDateTime(PispUtilities.getCurrentDateInISOFormat());
		data.setStatus(StatusEnum.ACCEPTEDTECHNICALVALIDATION);
		
		Risk risk = new Risk();
		response.setRisk(risk);
		
		PaymentSetupResponseInitiation initiation = new PaymentSetupResponseInitiation();
		data.setInitiation(initiation);
		
		initiation.setInstructionIdentification("ABDDC");
		initiation.setEndToEndIdentification("FRESCO.21302.GFX.28");
		
		PaymentSetupInitiationInstructedAmount instructedAmount = new PaymentSetupInitiationInstructedAmount();
		instructedAmount.setAmount("777777777.00");
		instructedAmount.setCurrency("EUR");
		initiation.setInstructedAmount(instructedAmount);
		
		CreditorAgent creditorAgent = new CreditorAgent();
		creditorAgent.setSchemeName(SchemeNameEnum.BICFI);
		creditorAgent.setIdentification("SC080801");
		initiation.setCreditorAgent(creditorAgent);
		
		CreditorAccount creditorAccount = new CreditorAccount();
		creditorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.IBAN);
		creditorAccount.setIdentification("NWBK60161331926819");
		creditorAccount.setName("Test user");
		creditorAccount.setSecondaryIdentification("0002");
		initiation.setCreditorAccount(creditorAccount);
		
		DebtorAgent debtorAgent = new DebtorAgent();
		debtorAgent.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAgent.SchemeNameEnum.BICFI);
		debtorAgent.setIdentification("SC112800");
		initiation.setDebtorAgent(debtorAgent);
		
		DebtorAccount debtorAccount = new DebtorAccount();
		debtorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.IBAN);
		debtorAccount.setIdentification("GB29NWBK60161331926819");
		debtorAccount.setName("Andrea Smith");
		debtorAccount.setSecondaryIdentification("0002");
		initiation.setDebtorAccount(debtorAccount);
		
		RemittanceInformation remittanceInformation = new RemittanceInformation();
		remittanceInformation.setReference("FRESCO-101");
		remittanceInformation.setUnstructured("Internal ops code 5120101");
		initiation.setRemittanceInformation(remittanceInformation);
		
		risk.setMerchantCategoryCode("5967");
		risk.setMerchantCustomerIdentification("053598653254");
		risk.setPaymentContextCode(PaymentContextCodeEnum.ECOMMERCEGOODS);
		
		RiskDeliveryAddress deliveryAddress = new RiskDeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		
		List<String> addressLine = new ArrayList<>();
		addressLine.add("Flat 7");
		addressLine.add("Acacia Lodge");		
		deliveryAddress.setAddressLine(addressLine);
		
		deliveryAddress.setBuildingNumber("27");
		deliveryAddress.setCountry("UK");
		
		List<String> countrySubDivision = new ArrayList<>();
		countrySubDivision.add("ABCDABCDABCDABCDAB");
		countrySubDivision.add("DEFG");
		deliveryAddress.setCountrySubDivision(countrySubDivision);
		
		deliveryAddress.setPostCode("GU31 2ZZ");
		deliveryAddress.setStreetName("AcaciaAvenue");
		deliveryAddress.setTownName("Sparsholt");
		
		response.setLinks(new PaymentSetupPOSTResponseLinks());
		response.setMeta(new PaymentSetupPOSTResponseMeta());
				
		return response;
	}
	
	public static CustomPaymentSubmissionPOSTRequest getPaymentSubmissionPostMockRequest(){
		CustomPaymentSubmissionPOSTRequest response = new CustomPaymentSubmissionPOSTRequest();
		PaymentSubmission data = new PaymentSubmission();
		response.setData(data);
		data.setPaymentId("12345");
		
		Risk risk = new Risk();
		response.setRisk(risk);
		
		PaymentSetupResponseInitiation initiation = new PaymentSetupResponseInitiation();
		data.setInitiation(initiation);
		
		initiation.setInstructionIdentification("ABDDC");
		initiation.setEndToEndIdentification("FRESCO.21302.GFX.28");
		
		PaymentSetupInitiationInstructedAmount instructedAmount = new PaymentSetupInitiationInstructedAmount();
		instructedAmount.setAmount("777777777.00");
		instructedAmount.setCurrency("EUR");
		initiation.setInstructedAmount(instructedAmount);
		
		CreditorAgent creditorAgent = new CreditorAgent();
		creditorAgent.setSchemeName(SchemeNameEnum.BICFI);
		creditorAgent.setIdentification("SC080801");
		initiation.setCreditorAgent(creditorAgent);
		
		CreditorAccount creditorAccount = new CreditorAccount();
		creditorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.IBAN);
		creditorAccount.setIdentification("FR1420041010050500013M02606");
		creditorAccount.setName("Test user");
		creditorAccount.setSecondaryIdentification("0002");
		initiation.setCreditorAccount(creditorAccount);
		
		DebtorAgent debtorAgent = new DebtorAgent();
		debtorAgent.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAgent.SchemeNameEnum.BICFI);
		debtorAgent.setIdentification("SC112800");
		initiation.setDebtorAgent(debtorAgent);
		
		DebtorAccount debtorAccount = new DebtorAccount();
		debtorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.IBAN);
		debtorAccount.setIdentification("GB29NWBK60161331926819");
		debtorAccount.setName("Andrea Smith");
		debtorAccount.setSecondaryIdentification("0002");
		initiation.setDebtorAccount(debtorAccount);
		
		RemittanceInformation remittanceInformation = new RemittanceInformation();
		remittanceInformation.setReference("FRESCO-101");
		remittanceInformation.setUnstructured("Internal ops code 5120101");
		initiation.setRemittanceInformation(remittanceInformation);
		
		risk.setMerchantCategoryCode("5967");
		risk.setMerchantCustomerIdentification("053598653254");
		risk.setPaymentContextCode(PaymentContextCodeEnum.ECOMMERCEGOODS);
		
		RiskDeliveryAddress deliveryAddress = new RiskDeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		
		List<String> addressLine = new ArrayList<>();
		addressLine.add("Flat 7");
		addressLine.add("Acacia Lodge");		
		deliveryAddress.setAddressLine(addressLine);
		
		deliveryAddress.setBuildingNumber("27");
		deliveryAddress.setCountry("GB");
		
		List<String> countrySubDivision = new ArrayList<>();
		countrySubDivision.add("ABCDABCDABCDABCDAB");
		countrySubDivision.add("DEFG");
		deliveryAddress.setCountrySubDivision(countrySubDivision);
		
		deliveryAddress.setPostCode("GU31 2ZZ");
		deliveryAddress.setStreetName("AcaciaAvenue");
		deliveryAddress.setTownName("Sparsholt");
		
				
		return response;
	}
	
	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
