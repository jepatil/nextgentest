package com.capgemini.psd2.consent.revocation.utilities;

import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.utilities.NullCheckUtils;

public class ConsentRevocationUtilities {

	private ConsentRevocationUtilities() {
		throw new IllegalAccessError("Utility class");
	}

	public static void validateInputs(String userId, String status) {

		if (NullCheckUtils.isNullOrEmpty(userId)) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.VALIDATION_ERROR);
		}

		if (status != null) {
			boolean flag = Boolean.FALSE;
			for (ConsentStatusEnum consentEnum : ConsentStatusEnum.values()) {
				if (consentEnum.getStatusCode().equalsIgnoreCase(status)) {
					flag = Boolean.TRUE;
					break;
				}
			}
			if (!flag) {
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.VALIDATION_ERROR);
			}
		}
	}
}