
package com.capgemini.psd2.consent.revocation.data;

import java.util.List;

import org.springframework.data.annotation.Id;

import com.capgemini.psd2.aisp.domain.Data1.PermissionsEnum;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Consent extends AispConsent {

	/** The consent id. */
	@Id
	private String consentId;

	/** The psu id. */
	private String psuId;

	/** The tpp CID. */
	private String tppCId;

	private String tppLegalEntityName;

	/** The account details. */
	private List<AccountDetails> accountDetails;

	private String consentCreationDate;

	private List<PermissionsEnum> permissions;

	private String paymentId;
	
	@JsonProperty("Status")
	private String statusName;

	public enum ConsentType {
		AISP, PISP
	}

	private ConsentType consentType = null;

	public ConsentType getConsentType() {
		return consentType;
	}

	public void setConsentType(ConsentType consentType) {
		this.consentType = consentType;
	}

	public List<PermissionsEnum> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<PermissionsEnum> permissions) {
		this.permissions = permissions;
	}

	/**
	 * Gets the consent id.
	 *
	 * @return the consent id
	 */
	@Override
	public String getConsentId() {
		return consentId;
	}

	/**
	 * Sets the consent id.
	 *
	 * @param consentId
	 *            the new consent id
	 */
	@Override
	public void setConsentId(String consentId) {
		this.consentId = consentId;
	}

	/**
	 * Gets the psu id.
	 *
	 * @return the psu id
	 */
	@Override
	public String getPsuId() {
		return psuId;
	}

	/**
	 * Sets the psu id.
	 *
	 * @param psuId
	 *            the new psu id
	 */
	@Override
	public void setPsuId(String psuId) {
		this.psuId = psuId;
	}

	/**
	 * Gets the account details.
	 *
	 * @return the account details
	 */
	@Override
	public List<AccountDetails> getAccountDetails() {
		return accountDetails;
	}

	/**
	 * Sets the account details.
	 *
	 * @param accountDetails
	 *            the new account details
	 */
	@Override
	public void setAccountDetails(List<AccountDetails> accountDetails) {
		this.accountDetails = accountDetails;
	}

	@Override
	public String getTppCId() {
		return tppCId;
	}

	@Override
	public void setTppCId(String tppCId) {
		this.tppCId = tppCId;
	}

	public String getConsentCreationDate() {
		return consentCreationDate;
	}

	public void setConsentCreationDate(String consentCreationDate) {
		this.consentCreationDate = consentCreationDate;
	}

	public String getTppLegalEntityName() {
		return tppLegalEntityName;
	}

	public void setTppLegalEntityName(String tppLegalEntityName) {
		this.tppLegalEntityName = tppLegalEntityName;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

}
