package com.capgemini.psd2.consent.revocation.data;

import java.util.List;
import java.util.Objects;

import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.MetaData;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ConsentListResponse {

	@JsonProperty("Data")
	private List<Consent> data = null;

	@JsonProperty("Links")
	private Links links = null;

	@JsonProperty("Meta")
	private MetaData meta = null;

	public List<Consent> getData() {
		return data;
	}

	public void setData(List<Consent> data) {
		this.data = data;
	}

	public Links getLinks() {
		return links;
	}

	public void setLinks(Links links) {
		this.links = links;
	}

	public MetaData getMeta() {
		return meta;
	}

	public void setMeta(MetaData meta) {
		this.meta = meta;
	}

	public ConsentListResponse data(List<Consent> data) {
		this.data = data;
		return this;
	}

	public ConsentListResponse links(Links links) {
		this.links = links;
		return this;
	}

	public ConsentListResponse meta(MetaData meta) {
		this.meta = meta;
		return this;
	}

	@Override
	public int hashCode() {
		return Objects.hash(data, links, meta);
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		ConsentListResponse consentListResponse = (ConsentListResponse) o;
		return Objects.equals(this.data, consentListResponse.data)
				&& Objects.equals(this.links, consentListResponse.links)
				&& Objects.equals(this.meta, consentListResponse.meta);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class ConsentListResponse {\n");

		sb.append("    data: ").append(toIndentedString(data)).append("\n");
		sb.append("    links: ").append(toIndentedString(links)).append("\n");
		sb.append("    meta: ").append(toIndentedString(meta)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

}
