package com.capgemini.psd2.consent.revocation.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.aisp.consent.adapter.impl.AispConsentAdapterImpl;
import com.capgemini.psd2.aisp.domain.AccountRequestPOSTResponse;
import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.MetaData;
import com.capgemini.psd2.aisp.domain.Data1.PermissionsEnum;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.consent.domain.PispConsent;
import com.capgemini.psd2.consent.revocation.data.Consent;
import com.capgemini.psd2.consent.revocation.data.Consent.ConsentType;
import com.capgemini.psd2.consent.revocation.data.ConsentListResponse;
import com.capgemini.psd2.consent.revocation.service.ConsentRevocationService;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.consent.adapter.impl.PispConsentAdapterImpl;
import com.capgemini.psd2.pisp.domain.PaymentSetupPlatformResource;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.repository.PaymentSetupPlatformRepository;
import com.capgemini.psd2.utilities.JSONUtilities;

@Service
public class ConsentRevocationServiceImpl implements ConsentRevocationService {

	@Autowired
	private AispConsentAdapterImpl aispConsentAdapter;

	@Autowired
	private PispConsentAdapterImpl pispConsentAdapter;

	@Autowired
	@Qualifier("AccountRequestAdapter")
	private AccountRequestAdapter accountRequestAdapter;
	
	@Autowired
	private PaymentSetupPlatformRepository paymentSetupPlatformRepository;

	@Autowired
	private HttpServletRequest httpServletRequest;

	@Override
	public ConsentListResponse getConsentList(String userId, String status) {

		ConsentListResponse consentListResponse = new ConsentListResponse();
		List<Consent> consents = new ArrayList<>();

		List<AispConsent> aispConsentList;
		
		if (null == status) {
			aispConsentList = aispConsentAdapter.retrieveConsentByPsuIdAndConsentStatus(userId, null);
		} else {
			aispConsentList = aispConsentAdapter.retrieveConsentByPsuIdAndConsentStatus(userId,
					ConsentStatusEnum.valueOf(status.toUpperCase()));
		}

		List<PispConsent> pispConsentList;
		if (null == status) {
			pispConsentList = pispConsentAdapter.retrieveConsentByPsuIdAndConsentStatus(userId, null);
		} else {
			pispConsentList = pispConsentAdapter.retrieveConsentByPsuIdAndConsentStatus(userId,
					ConsentStatusEnum.valueOf(status.toUpperCase()));
		}
		
		consents = formCommonConsentList(aispConsentList, pispConsentList, consents);

		consentListResponse.setData(consents);
		if (consentListResponse.getLinks() == null)
			consentListResponse.setLinks(new Links());
		if (consentListResponse.getMeta() == null)
			consentListResponse.setMeta(new MetaData());
		if (httpServletRequest.getQueryString() != null) {
			consentListResponse.getLinks().setSelf(httpServletRequest.getRequestURI() + PSD2Constants.QUESTIONMARK
					+ httpServletRequest.getQueryString());
		} else {
			consentListResponse.getLinks().setSelf(httpServletRequest.getRequestURI());
		}
		consentListResponse.getMeta().setTotalPages(1);
		return consentListResponse;
	}

	private List<Consent> formCommonConsentList(List<AispConsent> aispConsentList, List<PispConsent> pispConsentList,
			List<Consent> consents) {
		
		if (aispConsentList.isEmpty() && pispConsentList.isEmpty()) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_CONSENT_LIST_FOUND);
		} else {
			if (!aispConsentList.isEmpty()) {
				for (AispConsent aispConsent : aispConsentList) {
					Consent data = JSONUtilities
							.getObjectFromJSONString(JSONUtilities.getJSONOutPutFromObject(aispConsent), Consent.class);
					ConsentStatusEnum statusEnum = aispConsent.getStatus();
					//Updating
					data.setStatusName(statusEnum.getStatusCode());
					data.setConsentType(ConsentType.AISP);
					
					AccountRequestPOSTResponse accountRequestPOSTResponse = accountRequestAdapter
							.getAccountRequestGETResponse(aispConsent.getAccountRequestId());

					if (null != accountRequestPOSTResponse) {
						List<PermissionsEnum> permissions = accountRequestPOSTResponse.getData().getPermissions();
						data.setPermissions(permissions);
						data.setTppLegalEntityName(accountRequestPOSTResponse.getData().getTppLegalEntityName());
						data.setConsentCreationDate(accountRequestPOSTResponse.getData().getCreationDateTime());
					}
					consents.add(data);
				}
			}

			if (!pispConsentList.isEmpty()) {
				for (PispConsent pispConsent : pispConsentList) {
					Consent data = new Consent();
					data.setConsentId(pispConsent.getConsentId());
					data.setPsuId(pispConsent.getPsuId());
					data.setTppCId(pispConsent.getTppCId());
					List<AccountDetails> accountDetails = new ArrayList<>();
					accountDetails.add(pispConsent.getAccountDetails());
					data.setAccountDetails(accountDetails);
					data.setStartDate(pispConsent.getStartDate());
					data.setEndDate(pispConsent.getEndDate());
					ConsentStatusEnum statusEnum = pispConsent.getStatus();
					//Updating
					data.setStatusName(statusEnum.getStatusCode());					
					data.setPaymentId(pispConsent.getPaymentId());
					
					data.setConsentType(ConsentType.PISP);
					PaymentSetupPlatformResource paymentSetupPlatformResource = paymentSetupPlatformRepository.findOneByPaymentId(pispConsent.getPaymentId());
					if (null !=paymentSetupPlatformResource) {
						data.setTppLegalEntityName(paymentSetupPlatformResource.getTppLegalEntityName());
						data.setConsentCreationDate(paymentSetupPlatformResource.getCreatedAt());
					}					
					consents.add(data);
				}
			}
		}
		
		return consents;
	}

}
