package com.capgemini.psd2.consent.revocation.service;

import com.capgemini.psd2.consent.revocation.data.ConsentListResponse;

@FunctionalInterface
public interface ConsentRevocationService {

	ConsentListResponse getConsentList(String userId, String status);
	
}
