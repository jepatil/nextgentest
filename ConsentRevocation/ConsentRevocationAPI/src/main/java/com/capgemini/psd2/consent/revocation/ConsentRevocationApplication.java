package com.capgemini.psd2.consent.revocation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.web.client.RestTemplate;

import com.capgemini.psd2.account.request.routing.adapter.impl.AccountRequestRoutingAdapter;
import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;

@EnableEurekaClient
@SpringBootApplication
@ComponentScan(basePackages = { "com.capgemini.psd2" })
@EnableMongoRepositories(basePackages = { "com.capgemini.psd2" })
public class ConsentRevocationApplication {

	static ConfigurableApplicationContext context = null;

	public static void main(String[] args) {
		context = SpringApplication.run(ConsentRevocationApplication.class, args);
	}

	@Bean(name = "AccountRequestAdapter")
	public AccountRequestAdapter accountRequestRoutingAdapter() {
		return new AccountRequestRoutingAdapter();
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

}
