package com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.aisp.domain.AccountTransactionsGETResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.AccountTransactionsFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.domain.Accnt;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.domain.Accounts;

@SpringBootApplication
@ComponentScan("com.capgemini.psd2")
public class AccountTransactionsFoundationServiceTestProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccountTransactionsFoundationServiceTestProjectApplication.class, args);

	}
}

@RestController
@ResponseBody
class TestAccountTransactionsFSController{
	
	@Autowired
	private AccountTransactionsFoundationServiceAdapter adapter;
	
	@RequestMapping("/testAccountTransactionsWithSeconds")
	public AccountTransactionsGETResponse getResponseForDateWithSeconds(){
		Accounts accounts = new Accounts();
		Accnt accnt = new Accnt();
		accounts.getAccount().add(accnt);	
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("1234");
		accDet.setAccountNSC("903779");
		accDet.setAccountNumber("76528776");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("BOI999");
		accountMapping.setCorrelationId("12345678");
		
		Map<String, String> params = new HashMap<String, String>();
		params.put("ConsentExpirationDateTime", "2018-12-01T23:59:59");
		params.put("RequestedFromConsentDateTime", "2017-01-01T23:59:59");
		params.put("RequestedToConsentDateTime", "2017-12-01T23:59:59");
		params.put("RequestedFromDateTime", "2017-02-01T23:59:59");
		params.put("RequestedToDateTime", "2017-03-31T00:00:00");
		params.put("RequestedPageNumber", "1");
		params.put("RequestedTxnFilter", "CREDIT");
		params.put("RequestedTxnKey", "trnxKey");	
	    params.put("RequestedMinPageSize", "25");
	    params.put("RequestedMaxPageSize", "100");
	    params.put("RequestedPageSize", "5");
	    params.put("x-channel-id", "BOL");
	    params.put("channelId", "BOL");
	    
		AccountTransactionsGETResponse res = adapter.retrieveAccountTransaction(accountMapping, params);
		return res;
	}
	
	@RequestMapping("/testAccountTransactionsWithoutSeconds")
	public AccountTransactionsGETResponse getResponseForDateWithoutSeconds(){
		Accounts accounts = new Accounts();
		Accnt accnt = new Accnt();
		accounts.getAccount().add(accnt);	
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("1234");
		accDet.setAccountNSC("901538");
		accDet.setAccountNumber("23682876");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("BOI123");
		accountMapping.setCorrelationId("test");
		
		Map<String, String> params = new HashMap<String, String>();
		params.put("RequestedFromDateTime", "2017-01-01T00:00");
		params.put("RequestedToDateTime", "2018-12-31T23:59");
		params.put("RequestedPageNumber", "1");
		params.put("RequestedTxnFilter", "CREDIT");
		params.put("RequestedTxnKey", "trnxKey");	
	    params.put("RequestedMinPageSize", "25");
	    params.put("RequestedMaxPageSize", "100");
	    params.put("RequestedPageSize", "25");
	    params.put("x-channel-id", "BOL");
	    params.put("channelId", "BOL");
	    params.put("minPageSize", "25");
	    params.put("maxPageSize", "100");
	    params.put("pageSize", "5");
		
		AccountTransactionsGETResponse res = adapter.retrieveAccountTransaction(accountMapping, params);
		return res;
	}
}