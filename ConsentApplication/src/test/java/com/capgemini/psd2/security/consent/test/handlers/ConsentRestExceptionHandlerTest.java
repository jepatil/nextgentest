package com.capgemini.psd2.security.consent.test.handlers;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.fraudsystem.helper.FraudSystemHelper;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.scaconsenthelper.constants.PFConstants;
import com.capgemini.psd2.scaconsenthelper.constants.SCAConsentHelperConstants;
import com.capgemini.psd2.scaconsenthelper.models.IntentTypeEnum;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.security.consent.handlers.ConsentRestExceptionHandler;
import com.capgemini.psd2.ui.content.utility.controller.UIStaticContentUtilityController;

public class ConsentRestExceptionHandlerTest {
	
	@Mock
	private HttpServletRequest request;
	
	@Mock
	private HttpServletResponse response;
	
	@Mock
	private RequestHeaderAttributes requestHeaderAttributes;
	
	@Mock
	private UIStaticContentUtilityController uiController;
	
	@Mock
	private FraudSystemHelper fraudSystemHelper;
	
	@InjectMocks
	private ConsentRestExceptionHandler consentRestExceptionHandler;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		requestHeaderAttributes = new RequestHeaderAttributes();
		requestHeaderAttributes.setCorrelationId("ba4f73f8-9a60-425b-aed8-2a7ef2509fea");
		consentRestExceptionHandler = new ConsentRestExceptionHandler();
		ReflectionTestUtils.setField(consentRestExceptionHandler, "requestHeaderAttributes", requestHeaderAttributes);
		ReflectionTestUtils.setField(consentRestExceptionHandler, "uiController", uiController);
	}
	
	@Test
	public void testHandleCustomException(){
		request = mock(HttpServletRequest.class);
		response = mock(HttpServletResponse.class);
		ErrorInfo errorInfo = new ErrorInfo("100", "Error message", "400");
		PSD2Exception ex = new PSD2Exception("ErrorFound", errorInfo);
		PickupDataModel pickUpDataModel = new PickupDataModel();
		pickUpDataModel.setIntentTypeEnum(IntentTypeEnum.AISP_INTENT_TYPE);
		when(request.getAttribute(SCAConsentHelperConstants.INTENT_DATA)).thenReturn(pickUpDataModel);
		when(request.getParameter(PFConstants.RESUME_PATH)).thenReturn("https://pfcluster.apiboidev.com");
		assertNotNull(consentRestExceptionHandler.handleCustomException(ex, request, response));
		when(request.getParameter(PFConstants.RESUME_PATH)).thenReturn(null);
		when(request.getAttribute(SCAConsentHelperConstants.INTENT_DATA)).thenReturn(null);
		when(request.getParameter(SCAConsentHelperConstants.OAUTH_URL_PARAM)).thenReturn("https://pfcluster.apiboidev.com");
		assertNotNull(consentRestExceptionHandler.handleCustomException(ex, request, response));
	}
	
	
	

}
