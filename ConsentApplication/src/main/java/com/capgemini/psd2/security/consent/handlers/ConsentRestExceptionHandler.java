package com.capgemini.psd2.security.consent.handlers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.exceptions.FraudSystemException;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.fraudsystem.helper.FraudSystemHelper;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.scaconsenthelper.constants.PFConstants;
import com.capgemini.psd2.scaconsenthelper.constants.SCAConsentHelperConstants;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.security.constants.PSD2SecurityConstants;
import com.capgemini.psd2.ui.content.utility.controller.UIStaticContentUtilityController;
import com.capgemini.psd2.utilities.DateUtilites;
import com.capgemini.psd2.utilities.JSONUtilities;

@ControllerAdvice(basePackages = {"com.capgemini.psd2.security.consent.pisp.rest.controllers","com.capgemini.psd2.security.consent.aisp.rest.controllers","com.capgemini.psd2.security.consent.rest.controllers"})
@Order(Ordered.HIGHEST_PRECEDENCE+1)
public class ConsentRestExceptionHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(ConsentRestExceptionHandler.class);
		
	@Autowired
	private RequestHeaderAttributes requestHeaderAttributes;
	
	@Autowired
	private UIStaticContentUtilityController uiController;
	
	@Autowired
	private FraudSystemHelper fraudSystemHelper;
	
	@ResponseBody
	@ExceptionHandler(PSD2Exception.class)
	public String handleCustomException(PSD2Exception ex, HttpServletRequest request,
            HttpServletResponse response) throws JSONException  {
		LOGGER.error("{\"Exception\":\"{}\",\"timestamp\":\"{}\",\"correlationId\": \"{}\",\"ErrorDetails\":{}}"," com.capgemini.psd2.security.consent.handlers.ConsentRestExceptionHandler.handleCustomException()",DateUtilites.generateCurrentTimeStamp(),requestHeaderAttributes.getCorrelationId(), ex.getErrorInfo());
		if(LOGGER.isDebugEnabled()){
			LOGGER.error("{\"Exception\":\"{}\",\"timestamp\":\"{}\",\"correlationId\": \"{}\",\"ErrorDetails\":\"{}\"}"," com.capgemini.psd2.security.consent.handlers.ConsentRestExceptionHandler.handleCustomException()",DateUtilites.generateCurrentTimeStamp(),requestHeaderAttributes.getCorrelationId(), ex.getStackTrace());
		}
		//Model model = null;
		JSONObject model = new JSONObject();
		PickupDataModel intentData = null;
		ErrorInfo errorInfo = null;
		String resumePath = null;
		String statusCode = null;
		boolean serverError = Boolean.FALSE;

		ex.getErrorInfo().setDetailErrorMessage(null);

		errorInfo = ex.getErrorInfo();
		
		statusCode = errorInfo.getStatusCode();
		
		response.setStatus(Integer.parseInt(statusCode));
		
		intentData = (PickupDataModel)request.getAttribute(SCAConsentHelperConstants.INTENT_DATA);
		
		model.put(SCAConsentHelperConstants.EXCEPTION, new JSONObject(JSONUtilities.getJSONOutPutFromObject(ex.getErrorInfo())));
		
		if (request.getParameter(PFConstants.RESUME_PATH) != null && !request.getParameter(PFConstants.RESUME_PATH).isEmpty()) {
			resumePath = request.getParameter(PFConstants.RESUME_PATH);
		} else if (request.getParameter(SCAConsentHelperConstants.OAUTH_URL_PARAM) != null && !request.getParameter(SCAConsentHelperConstants.OAUTH_URL_PARAM).isEmpty()) {
			resumePath = request.getParameter(SCAConsentHelperConstants.OAUTH_URL_PARAM);
		}
		
		if (statusCode != null && !statusCode.isEmpty()) {
			serverError = statusCode.equalsIgnoreCase(Integer.toString(HttpStatus.INTERNAL_SERVER_ERROR.value()))
					|| Integer.valueOf(statusCode).intValue() > HttpStatus.INTERNAL_SERVER_ERROR.value();
		}

		model.put(PSD2Constants.SERVER_ERROR_FLAG_ATTR, serverError);
		if(intentData != null) {
			model.put(PSD2Constants.CONSENT_FLOW_TYPE, intentData.getIntentTypeEnum().getIntentType());	
		}

		model.put(PSD2SecurityConstants.REDIRECT_URI_MODEL_ATTRIBUTE, resumePath);
		model.put(PSD2Constants.JS_MSG,
				uiController.retrieveUiParamValue(PSD2Constants.ERROR_MESSAGE, PSD2Constants.JS_MSG));
				
		model.put(PSD2Constants.CO_RELATION_ID, requestHeaderAttributes.getCorrelationId());
		
		
		if (ex instanceof FraudSystemException){
			fraudSystemHelper.captureFraudEvent(null, null, ((FraudSystemException) ex).getParams(), null);
		}
		
		return model.toString();
	}	
}