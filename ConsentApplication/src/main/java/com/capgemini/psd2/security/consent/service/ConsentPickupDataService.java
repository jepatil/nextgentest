package com.capgemini.psd2.security.consent.service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.scaconsenthelper.config.helpers.MainPickupDataService;
import com.capgemini.psd2.scaconsenthelper.constants.OIDCConstants;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;

@Service
public class ConsentPickupDataService extends MainPickupDataService {

	@Override
	public PickupDataModel populateIntentData(String jsonResponse) throws ParseException {

		String intentId = null;
		
		PickupDataModel intentData = new PickupDataModel();

		JSONParser parser = new JSONParser();
		JSONObject jsonObject = (JSONObject) parser.parse(jsonResponse);

		String claims = (String) jsonObject.get(OIDCConstants.CLAIMS);
		String scopes = (String) jsonObject.get(OIDCConstants.SCOPE);
		String userName = (String) jsonObject.get(OIDCConstants.USERNAME);
		String channelId = (String) jsonObject.get(OIDCConstants.CHANNEL_ID);
		String clientId = (String) jsonObject.get(OIDCConstants.CLIENT_ID);
		String correlationId =  (String) jsonObject.get(PSD2Constants.CO_RELATION_ID);

		if (scopes != null && !scopes.isEmpty() && scopes.contains(OIDCConstants.OPENID)) {
			scopes = scopes.replace(OIDCConstants.OPENID, StringUtils.EMPTY).trim();
		}
		
		//Pattern openBankingPattern = Pattern.compile("userinfo=\\{openbanking_intent_id=\\{value=(.*?),");
		Pattern idTokenPattern = Pattern.compile("id_token=\\{.*openbanking_intent_id=\\{.*value=(.*?)}");
		
		//Matcher openBankingMatcher = openBankingPattern.matcher(claims);
		Matcher idTokenMatcher = idTokenPattern.matcher(claims);
		/*if (openBankingMatcher.find()){
			intentId = openBankingMatcher.group(1);
		}else*/
		
		if(idTokenMatcher.find()){
			if(idTokenMatcher.group(1).contains(",")){
				String[] values = idTokenMatcher.group(1).split(",");
				intentId = values[0];
			}else{
				intentId = idTokenMatcher.group(1);
			}
		}
		intentData.setChannelId(channelId);
		intentData.setIntentId(intentId);
		intentData.setScope(scopes);
		intentData.setUserId(userName);
		intentData.setClientId(clientId);
		intentData.setCorrelationId(correlationId);
		return intentData;
	}
}