package com.capgemini.psd2.security.consent.aisp.mvc.test.controllers;

import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.Account;
import com.capgemini.psd2.aisp.domain.AccountGETResponse;
import com.capgemini.psd2.aisp.domain.Data2;
import com.capgemini.psd2.aisp.domain.Data2Account;
import com.capgemini.psd2.aisp.domain.Data2Servicer;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.consent.domain.PSD2AccountsAdditionalInfo;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.fraudsystem.helper.FraudSystemHelper;
import com.capgemini.psd2.integration.adapter.TPPInformationAdaptor;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.scaconsenthelper.config.PFConfig;
import com.capgemini.psd2.scaconsenthelper.config.helpers.ConsentAuthorizationHelper;
import com.capgemini.psd2.scaconsenthelper.constants.PFConstants;
import com.capgemini.psd2.scaconsenthelper.models.DropOffResponse;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.scaconsenthelper.services.SCAConsentHelperService;
import com.capgemini.psd2.security.consent.aisp.helpers.AispConsentCreationDataHelper;
import com.capgemini.psd2.security.consent.aisp.rest.controllers.AispConsentAuthorizeController;
import com.capgemini.psd2.security.constants.PSD2SecurityConstants;


@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class AispConsentAuthorizeControllerTest {

	@Value("${app.saas.security.tokenSigningKey}")
	private String tokenSigningKey;

	@Value("${app.signincallbackurl}")
	private String signincallbackurl;

	@Mock
	private AispConsentCreationDataHelper consentCreationDataHelper;

	@InjectMocks
	private AispConsentAuthorizeController consentAuthorizeController;

	@Mock
	private RequestHeaderAttributes requestHeaderAttributes;

	@Mock
	private HttpServletRequest httpServletRequest;

	@Mock
	private HttpServletResponse httpServletResponse;
	
	@Mock
	private ConsentAuthorizationHelper consentAuthHelper;
	
	@Mock
	private SCAConsentHelperService helperService;
	
	@Mock
	private ConsentAuthorizationHelper consentAuthorizationHelper;
	
	@Mock
	private AispConsentAdapter aispConsentAdapter;
	
	@Mock
	private TPPInformationAdaptor tppInformationAdaptor;
	
	@Mock
	private FraudSystemHelper fraudSystemHelper;
	
	@Mock
	private PFConfig pfConfig;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setPrefix("/");
		viewResolver.setSuffix(".jsp");
	}
 
	@Test
	public void testCreateConsent() throws Exception {
		ModelAndView model = new ModelAndView();
		PSD2AccountsAdditionalInfo obj=new PSD2AccountsAdditionalInfo();
		obj.setAccountdetails(getCustomerAccountsNew());
		String resumePath="0";
		when(consentCreationDataHelper.retrieveCustomerAccountListInfo(anyString(), anyString(), anyString(), anyString(), anyString())).thenReturn(getCustomerAccountInfo());
		when(consentAuthHelper.matchAccountFromList(anyObject(), anyObject())).thenReturn(new Account());
		doNothing().when(consentCreationDataHelper).createConsent(anyObject(), anyString(), anyString(), anyString(), anyString(), anyString(),anyString(),anyObject());
		DropOffResponse dropresponse=new DropOffResponse();
		dropresponse.setRef("abcd");
		when(helperService.dropOffOnConsentSubmission(anyObject(), anyString(), anyString(), anyString())).thenReturn(dropresponse);
		when(pfConfig.getResumePathBaseURL()).thenReturn("http://localhost:8");
		when(tppInformationAdaptor.fetchApplicationName(anyString())).thenReturn("Moneywise");
		when(tppInformationAdaptor.fetchTPPInformation(anyString())).thenReturn(new Object());
		when(httpServletRequest.getAttribute(anyString())).thenReturn(new PickupDataModel());
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put("redirect_uri", "/test");
		consentAuthorizeController.createConsent(model, obj, resumePath,Boolean.FALSE.toString());
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testCreateConsentRefreshTokenRenewalFlow() throws NamingException{
		ModelAndView model = new ModelAndView();
		PSD2AccountsAdditionalInfo obj=new PSD2AccountsAdditionalInfo();
		obj.setAccountdetails(getCustomerAccountsNew());
		Map<String, String> paramMap = new HashMap<>();
		List<Account> accountList = new ArrayList<>();
		accountList.add(new Account());
		String resumePath="0";
		DropOffResponse dropresponse=new DropOffResponse();
		dropresponse.setRef("abcd");
		when(consentAuthorizationHelper.populateAccountListFromAccountDetails(anyObject())).thenReturn(accountList);
		when(helperService.dropOffOnConsentSubmission(anyObject(), anyString(), anyString(), anyString())).thenReturn(dropresponse);
		when(pfConfig.getResumePathBaseURL()).thenReturn("http://localhost:8");
		when(httpServletRequest.getAttribute(anyString())).thenReturn(new PickupDataModel());
		doNothing().when(fraudSystemHelper).captureFraudEvent(anyString(),anyList(), anyMap());
		doNothing().when(helperService).revokeAllPreviousGrants(anyString(),anyString(),anyString());
		AispConsent aispConsent = new AispConsent();
		aispConsent.setAccountRequestId("123456");
		when(aispConsentAdapter.retrieveConsentByAccountRequestId(anyString(),anyObject())).thenReturn(aispConsent);
		paramMap.put("redirect_uri", "/test");
		consentAuthorizeController.createConsent(model, obj, resumePath,Boolean.TRUE.toString());
	}

	@Test
	public void testCancelConsent() {
		ModelAndView model=new ModelAndView();
		Map<String, String> paramsMap=new HashMap<>();
		paramsMap.put(PSD2Constants.SERVER_ERROR_FLAG_ATTR, "true");
		when(httpServletRequest.getAttribute(anyString())).thenReturn(new PickupDataModel());
		doNothing().when(consentCreationDataHelper).cancelAccountRequest(anyString());
		paramsMap.put(PFConstants.RESUME_PATH,"80");
		paramsMap.put(PSD2SecurityConstants.REFRESH_TOKEN_RENEWAL_FLOW, Boolean.FALSE.toString());
		when(pfConfig.getResumePathBaseURL()).thenReturn("http://localhost:80");
		
		consentAuthorizeController.cancelConsent(model, paramsMap);
	}
	
	@Test
	public void testCancelConsentElseBranch() {
		ModelAndView model=new ModelAndView();
		Map<String, String> paramsMap=new HashMap<>();
		paramsMap.put(PSD2Constants.SERVER_ERROR_FLAG_ATTR, null);
		when(httpServletRequest.getAttribute(anyString())).thenReturn(new PickupDataModel());
		doNothing().when(consentCreationDataHelper).cancelAccountRequest(anyString());
		paramsMap.put(PFConstants.RESUME_PATH,"");
		when(pfConfig.getResumePathBaseURL()).thenReturn("");
		when(helperService.cancelJourney(anyString(), anyObject())).thenReturn("http://localhost:80");

		consentAuthorizeController.cancelConsent(model, paramsMap);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testCancelConsentAispConsentNotNull() {
		ModelAndView model=new ModelAndView();
		Map<String, String> paramsMap=new HashMap<>();
		paramsMap.put(PSD2Constants.SERVER_ERROR_FLAG_ATTR, null);
		when(httpServletRequest.getAttribute(anyString())).thenReturn(new PickupDataModel());
		doNothing().when(consentCreationDataHelper).cancelAccountRequest(anyString());
		paramsMap.put(PFConstants.RESUME_PATH,"");
		AispConsent aispConsent = new AispConsent();
		aispConsent.setAccountRequestId("123456");
		when(aispConsentAdapter.retrieveConsentByAccountRequestId(anyString(),anyObject())).thenReturn(aispConsent);
		when(pfConfig.getResumePathBaseURL()).thenReturn("");
		when(helperService.cancelJourney(anyString(), anyObject())).thenReturn("http://localhost:80");
		consentAuthorizeController.cancelConsent(model, paramsMap);
		paramsMap.put(PSD2SecurityConstants.REFRESH_TOKEN_RENEWAL_FLOW, Boolean.TRUE.toString());
		when(aispConsentAdapter.retrieveConsentByAccountRequestId(anyString(),anyObject())).thenReturn(null);
		consentAuthorizeController.cancelConsent(model, paramsMap);
	}
	@Test(expected = PSD2Exception.class)
	public void testCancelConsentRefeshTokenFlowAispConsentNull() {
		ModelAndView model=new ModelAndView();
		Map<String, String> paramsMap=new HashMap<>();
		paramsMap.put(PSD2Constants.SERVER_ERROR_FLAG_ATTR, null);
		when(httpServletRequest.getAttribute(anyString())).thenReturn(new PickupDataModel());
		doNothing().when(consentCreationDataHelper).cancelAccountRequest(anyString());
		paramsMap.put(PFConstants.RESUME_PATH,"");
		AispConsent aispConsent = new AispConsent();
		aispConsent.setAccountRequestId("123456");;;
		//when(aispConsentAdapter.retrieveConsentByAccountRequestId(anyString(),anyObject())).thenReturn(aispConsent);
		when(pfConfig.getResumePathBaseURL()).thenReturn("");
		when(helperService.cancelJourney(anyString(), anyObject())).thenReturn("http://localhost:80");
		paramsMap.put(PSD2SecurityConstants.REFRESH_TOKEN_RENEWAL_FLOW, Boolean.TRUE.toString());
		when(aispConsentAdapter.retrieveConsentByAccountRequestId(anyString(),anyObject())).thenReturn(null);
		consentAuthorizeController.cancelConsent(model, paramsMap);
	}
	
	public static AccountGETResponse getCustomerAccountInfo() {
		AccountGETResponse mockAccountGETResponse = new AccountGETResponse();
		List<Account> accountData = new ArrayList<>();
		PSD2Account acct = new PSD2Account();
		acct.setAccountId("14556236");
		acct.setCurrency("EUR");
		acct.setNickname("John");
		// acct.setHashedValue();
		acct.setAccountType("savings");
		Data2Servicer servicer = new Data2Servicer();
		servicer.setIdentification("12345");
		Data2Account account = new Data2Account();
		account.setIdentification("12345");
		account.setSchemeName(Data2Account.SchemeNameEnum.IBAN);
		acct.setAccount(account);
		acct.setServicer(servicer);

		PSD2Account accnt = new PSD2Account();
		accnt.setAccountId("14556236");
		accnt.setCurrency("EUR");
		accnt.setNickname("John");
		// accnt.setHashedValue();
		accnt.setServicer(servicer);
		accnt.setAccount(account);
		accnt.setAccountType("savings");
		accountData.add(acct);
		accountData.add(accnt);
		Data2 data2 = new Data2();
		data2.setAccount(accountData);
		account.setSchemeName(Data2Account.SchemeNameEnum.IBAN);
		mockAccountGETResponse.setData(data2);

		return mockAccountGETResponse;
	}
	
	public static List<PSD2Account> getCustomerAccountsNew() {
		List<PSD2Account> mockCustomerAccountList = new ArrayList<>();
		PSD2Account customerAccountInfo1 = new PSD2Account();
		Data2Account acct = new Data2Account();
		acct.setIdentification("12345");
		acct.setSchemeName(com.capgemini.psd2.aisp.domain.Data2Account.SchemeNameEnum.IBAN);
		Data2Servicer servicer = new Data2Servicer();
		servicer.setIdentification("12345");
		customerAccountInfo1.setServicer(servicer);
		customerAccountInfo1.setAccount(acct);
		customerAccountInfo1.setCurrency("EUR");
		customerAccountInfo1.setNickname("John");
		customerAccountInfo1.setHashedValue();
		PSD2Account customerAccountInfo2 = new PSD2Account();

		customerAccountInfo2.setCurrency("EUR");
		customerAccountInfo2.setNickname("John");
		customerAccountInfo2.setAccount(acct);
		customerAccountInfo2.setHashedValue();
		customerAccountInfo2.setServicer(servicer);

		mockCustomerAccountList.add(customerAccountInfo1);
		mockCustomerAccountList.add(customerAccountInfo2);
		return mockCustomerAccountList;
	}
}
