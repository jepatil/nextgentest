package com.capgemini.psd2.security.consent.aisp.test.helpers;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.adapter.CustomerAccountListAdapter;
import com.capgemini.psd2.aisp.domain.Account;
import com.capgemini.psd2.aisp.domain.AccountGETResponse;
import com.capgemini.psd2.aisp.domain.AccountRequestPOSTResponse;
import com.capgemini.psd2.aisp.domain.Data1;
import com.capgemini.psd2.aisp.domain.Data1.StatusEnum;
import com.capgemini.psd2.aisp.domain.Data2Account;
import com.capgemini.psd2.aisp.domain.Data2Servicer;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.fraudsystem.helper.FraudSystemHelper;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.security.consent.aisp.helpers.AispConsentCreationDataHelperImpl;

@RunWith(SpringJUnit4ClassRunner.class)
public class AispConsentCreationDataHelperImplTest {

	@Mock
	private AccountRequestAdapter accountRequestAdapter;

	@Mock
	private AispConsentAdapter aispConsentAdapter;

	@Mock
	private CustomerAccountListAdapter customerAccountListAdapter;

	@Mock
	private RequestHeaderAttributes reqAttributes;
	
	@Mock
	private FraudSystemHelper fraudSystemHelper;

	
	// @Mock
	// private DatatypeConverter dateConverter;

	@InjectMocks
	private AispConsentCreationDataHelperImpl consentCreationDataHelperImpl = new AispConsentCreationDataHelperImpl();

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put("AISP", "AISP");
		paramMap.put("client_id", "client123");
		paramMap.put("AccountRequestId", "accReqId123");
	}

	@Test
	public void testRetrieveAccountRequestSetupDataAISPCrationDateBeforeException() {
		AccountRequestPOSTResponse accountRequestPOSTResponse = new AccountRequestPOSTResponse();
		Data1 data = new Data1();
		data.setCreationDateTime("2017-02-25T00:00:00-00:00");
		accountRequestPOSTResponse.data(data);
		when(accountRequestAdapter.getAccountRequestGETResponse(anyString()))
				.thenReturn(accountRequestPOSTResponse);
		consentCreationDataHelperImpl.retrieveAccountRequestSetupData("dummy");
	}
	
	
	@Test(expected = PSD2Exception.class)
	public void testRetrieveAccountRequestSetupForException() {
		AccountRequestPOSTResponse accountRequestPOSTResponse = new AccountRequestPOSTResponse();
		Data1 data = new Data1();
		data.setCreationDateTime("2017-02-25T00:00:00-00:00");
		accountRequestPOSTResponse.data(data);
		when(accountRequestAdapter.getAccountRequestGETResponse(anyString()))
				.thenReturn(null);
	consentCreationDataHelperImpl.retrieveAccountRequestSetupData("dummy");
	}

	@Test
	public void testCreateConsent() throws NamingException {
		doNothing().when(aispConsentAdapter).createConsent(any(AispConsent.class));
		when(accountRequestAdapter.getAccountRequestGETResponse(anyString()))
				.thenReturn(getAccountRequestPOSTResponseInvalidStatus());
		
		doNothing().when(fraudSystemHelper).captureFraudEvent(anyObject(), anyObject(),anyObject());
		
		List<Account> acctList=new ArrayList<>();
		
		PSD2Account customerAccountInfo1 = new PSD2Account();
		Data2Account acct = new Data2Account();
		acct.setIdentification("12345");
		acct.setSchemeName(com.capgemini.psd2.aisp.domain.Data2Account.SchemeNameEnum.IBAN);
		Data2Servicer servicer = new Data2Servicer();
		servicer.setIdentification("12345");
		customerAccountInfo1.setServicer(servicer);
		customerAccountInfo1.setAccount(acct);
		customerAccountInfo1.setCurrency("EUR");
		customerAccountInfo1.setNickname("John");
		customerAccountInfo1.setHashedValue();
		PSD2Account customerAccountInfo2 = new PSD2Account();

		customerAccountInfo2.setCurrency("EUR");
		customerAccountInfo2.setNickname("John");
		customerAccountInfo2.setAccount(acct);
		customerAccountInfo2.setHashedValue();
		customerAccountInfo2.setServicer(servicer);
		acctList.add(customerAccountInfo1);
		acctList.add(customerAccountInfo2);
		
		consentCreationDataHelperImpl.createConsent(acctList,null,null,null,null,null,null,null);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testCreateConsent4() throws NamingException {
		doNothing().when(aispConsentAdapter).createConsent(any(AispConsent.class));
		when(accountRequestAdapter.getAccountRequestGETResponse(anyString()))
				.thenReturn(getAccountRequestPOSTResponseInvalidStatus());
		
		doNothing().when(fraudSystemHelper).captureFraudEvent(anyObject(), anyObject(),anyObject());
		
		List<Account> acctList=new ArrayList<>();
		
		PSD2Account customerAccountInfo1 = new PSD2Account();
		Data2Account acct = new Data2Account();
		acct.setIdentification("12345");
		acct.setSchemeName(com.capgemini.psd2.aisp.domain.Data2Account.SchemeNameEnum.IBAN);
		Data2Servicer servicer = new Data2Servicer();
		servicer.setIdentification(null);
		customerAccountInfo1.setServicer(servicer);
		customerAccountInfo1.setAccount(acct);
		customerAccountInfo1.setCurrency("EUR");
		customerAccountInfo1.setNickname("John");
		customerAccountInfo1.setHashedValue();
		PSD2Account customerAccountInfo2 = new PSD2Account();

		customerAccountInfo2.setCurrency("EUR");
		customerAccountInfo2.setNickname("John");
		customerAccountInfo2.setAccount(acct);
		customerAccountInfo2.setHashedValue();
		customerAccountInfo2.setServicer(servicer);
		acctList.add(customerAccountInfo1);
		acctList.add(customerAccountInfo2);
		
		consentCreationDataHelperImpl.createConsent(acctList,null,null,null,null,null,null,null);
	}
	
	
	@Test(expected=Exception.class)
	public void testCreateConsentBranches() throws NamingException {
		doNothing().when(aispConsentAdapter).createConsent(any(AispConsent.class));
		when(accountRequestAdapter.getAccountRequestGETResponse(anyString()))
				.thenReturn(getAccountRequestPOSTResponseInvalidStatus());
		
		doNothing().when(fraudSystemHelper).captureFraudEvent(anyObject(), anyObject(),anyObject());
		
		List<Account> acctList=new ArrayList<>();
		
		PSD2Account customerAccountInfo1 = new PSD2Account();
		Data2Account acct = new Data2Account();
		acct.setIdentification("12345");
		acct.setSchemeName(com.capgemini.psd2.aisp.domain.Data2Account.SchemeNameEnum.SORTCODEACCOUNTNUMBER);
		Data2Servicer servicer = new Data2Servicer();
		servicer.setIdentification("12345678901234");
		customerAccountInfo1.setServicer(servicer);
		customerAccountInfo1.setAccount(acct);
		customerAccountInfo1.setCurrency("EUR");
		customerAccountInfo1.setNickname("John");
		customerAccountInfo1.setHashedValue();
		PSD2Account customerAccountInfo2 = new PSD2Account();

		customerAccountInfo2.setCurrency("EUR");
		customerAccountInfo2.setNickname("John");
		customerAccountInfo2.setAccount(acct);
		customerAccountInfo2.setHashedValue();
		customerAccountInfo2.setServicer(servicer);
		acctList.add(customerAccountInfo1);
		acctList.add(customerAccountInfo2);
		
		consentCreationDataHelperImpl.createConsent(acctList,null,null,null,null,null,null,null);
	}
	
	
	@Test
	public void testCreateConsentBranches3() throws NamingException {
		doNothing().when(aispConsentAdapter).createConsent(any(AispConsent.class));
		when(accountRequestAdapter.getAccountRequestGETResponse(anyString()))
				.thenReturn(getAccountRequestPOSTResponseInvalidStatus());
		
		doNothing().when(fraudSystemHelper).captureFraudEvent(anyObject(), anyObject(),anyObject());
		
		List<Account> acctList=new ArrayList<>();
		
		PSD2Account customerAccountInfo1 = new PSD2Account();
		Data2Account acct = new Data2Account();
		acct.setIdentification("12345678901234");
		acct.setSchemeName(com.capgemini.psd2.aisp.domain.Data2Account.SchemeNameEnum.SORTCODEACCOUNTNUMBER);
		Data2Servicer servicer = new Data2Servicer();
		servicer.setIdentification("12345678901234");
		customerAccountInfo1.setServicer(servicer);
		customerAccountInfo1.setAccount(acct);
		customerAccountInfo1.setCurrency("EUR");
		customerAccountInfo1.setNickname("John");
		customerAccountInfo1.setHashedValue();
		PSD2Account customerAccountInfo2 = new PSD2Account();

		customerAccountInfo2.setCurrency("EUR");
		customerAccountInfo2.setNickname("John");
		customerAccountInfo2.setAccount(acct);
		customerAccountInfo2.setHashedValue();
		customerAccountInfo2.setServicer(servicer);
		acctList.add(customerAccountInfo1);
		acctList.add(customerAccountInfo2);
		
		consentCreationDataHelperImpl.createConsent(acctList,null,null,null,null,null,null,null);
	}

	/*@Test(expected = PSD2Exception.class)
	public void testCreateConsentIfelseBranches() {
		Map<String, String> paramsMap = new HashMap<String, String>();
		List<Account> acctList = new ArrayList<>();
		Account acct1 = new Account();
		acct1.setAccountId("14556236");
		acct1.setCurrency("EUR");
		acct1.setNickname("John");
		Data2Servicer servicer = new Data2Servicer();
		servicer.setIdentification("12345");
		// acct1.setServicer(servicer);
		Data2Account data2account = new Data2Account();
		data2account.setSchemeName(Data2Account.SchemeNameEnum.IBAN);
		data2account.setIdentification("12345");
		acct1.setAccount(data2account);
		acctList.add(acct1);
		//consentCreationDataHelperImpl.createConsent(acctList, "12345", paramsMap);
	}

	@Test(expected = Exception.class)
	public void testCreateConsentBranches() {
		Map<String, String> paramsMap = new HashMap<String, String>();
		List<Account> acctList = new ArrayList<>();
		Account acct1 = new Account();
		acct1.setAccountId("14556236");
		acct1.setCurrency("EUR");
		acct1.setNickname("John");
		Data2Servicer servicer = new Data2Servicer();
		servicer.setIdentification("12345");
		// acct1.setServicer(servicer);
		Data2Account data2account = new Data2Account();
		data2account.setSchemeName(SchemeNameEnum.SORTCODEACCOUNTNUMBER);
		data2account.setIdentification("01234567891234");
		acct1.setAccount(data2account);
		acctList.add(acct1);
		//consentCreationDataHelperImpl.createConsent(acctList, "12345", paramsMap);
	}

	@Test(expected = PSD2Exception.class)
	public void testCreateConsentForElseBranch() {
		Map<String, String> paramsMap = new HashMap<String, String>();
		List<Account> acctList = new ArrayList<>();
		Account acct1 = new Account();
		acct1.setAccountId("14556236");
		acct1.setCurrency("EUR");
		acct1.setNickname("John");
		Data2Servicer servicer = new Data2Servicer();
		servicer.setIdentification("12345");
		// acct1.setServicer(servicer);
		Data2Account data2account = new Data2Account();
		data2account.setSchemeName(SchemeNameEnum.SORTCODEACCOUNTNUMBER);
		data2account.setIdentification("0123456789123");
		acct1.setAccount(data2account);
		acctList.add(acct1);
		//consentCreationDataHelperImpl.createConsent(acctList, "12345", paramsMap);
	}*/

	/*@Test
	public void testCancelAccountRequestForNullConsent() {
		when(aispConsentAdapter.retrieveConsentByAccountRequestId(anyString(),
				eq(ConsentStatusEnum.AWAITINGAUTHORISATION))).thenReturn(new AispConsent());
		Map<String, String> paramsMap = new HashMap<>();
		consentCreationDataHelperImpl.cancelAccountRequest(paramsMap);
	}

	@Test
	public void testRetrieveAccountRequestSetupDataForReturning() {
		// AccountRequestPOSTResponse accountReq=new AccountRequestPOSTResponse();
		// Data1 data1=new Data1();
		// List<PermissionsEnum> permissionList=new ArrayList<>();
		// permissionList.add(PermissionsEnum.READACCOUNTSBASIC);
		// data1.setPermissions(permissionList);
		// accountReq.setData(data1);
		AccountRequestPOSTResponse accountReq = AispConsentApplicationMockdata.getAccountRequestPOSTResponse();
		when(accountRequestAdapter.getAccountRequestGETResponse(anyString(), anyMap())).thenReturn(accountReq);
		ReflectionTestUtils.setField(consentCreationDataHelperImpl, "accountRequestExpiryTime", Integer.parseInt("1"));
		assertEquals(JSONUtilities.getJSONOutPutFromObject(accountReq),
				consentCreationDataHelperImpl.retrieveAccountRequestSetupData());
	}*/

	@SuppressWarnings("unchecked")
	@Test
	public void testRetrieveCustomerAccountListInfoSuccessFlow() {
		//when(securityRequestAttributes.getFlowType()).thenReturn("AISP");
		when(reqAttributes.getCorrelationId()).thenReturn("123445");
		when(customerAccountListAdapter.retrieveCustomerAccountList(anyString(), anyMap()))
				.thenReturn(new AccountGETResponse());
				consentCreationDataHelperImpl.retrieveCustomerAccountListInfo(null,null,null,null,null);
	}

	@Test
	public void testCancelAccountRequest() {
		when(aispConsentAdapter.retrieveConsentByAccountRequestId(anyString(), any(ConsentStatusEnum.class))).thenReturn(new AispConsent());
		doNothing().when(aispConsentAdapter).updateConsentStatus(anyString(), any(ConsentStatusEnum.class));
		when(accountRequestAdapter.updateAccountRequestResponse(anyString(), any(StatusEnum.class))).thenReturn(new AccountRequestPOSTResponse());
		
		consentCreationDataHelperImpl.cancelAccountRequest("");
	}
	
	public static List<PSD2Account> getCustomerAccountsNew() {
		List<PSD2Account> mockCustomerAccountList = new ArrayList<>();
		PSD2Account customerAccountInfo1 = new PSD2Account();
		Data2Account acct = new Data2Account();
		acct.setIdentification("12345");
		acct.setSchemeName(com.capgemini.psd2.aisp.domain.Data2Account.SchemeNameEnum.IBAN);
		Data2Servicer servicer = new Data2Servicer();
		servicer.setIdentification("12345");
		customerAccountInfo1.setServicer(servicer);
		customerAccountInfo1.setAccount(acct);
		customerAccountInfo1.setCurrency("EUR");
		customerAccountInfo1.setNickname("John");
		customerAccountInfo1.setHashedValue();
		PSD2Account customerAccountInfo2 = new PSD2Account();

		customerAccountInfo2.setCurrency("EUR");
		customerAccountInfo2.setNickname("John");
		customerAccountInfo2.setAccount(acct);
		customerAccountInfo2.setHashedValue();
		customerAccountInfo2.setServicer(servicer);

		mockCustomerAccountList.add(customerAccountInfo1);
		mockCustomerAccountList.add(customerAccountInfo2);
		return mockCustomerAccountList;
	}
	
	public static AccountRequestPOSTResponse getAccountRequestPOSTResponseInvalidStatus() {
		AccountRequestPOSTResponse mockAccountRequestPOSTResponse = new AccountRequestPOSTResponse();

		Data1 data = new Data1();
		List<com.capgemini.psd2.aisp.domain.Data1.PermissionsEnum> permissions = new ArrayList<>();
		permissions.add(com.capgemini.psd2.aisp.domain.Data1.PermissionsEnum.READACCOUNTSBASIC);
		permissions.add(com.capgemini.psd2.aisp.domain.Data1.PermissionsEnum.READACCOUNTSDETAIL);
		permissions.add(com.capgemini.psd2.aisp.domain.Data1.PermissionsEnum.READBALANCES);
		data.setPermissions(permissions);
		data.setCreationDateTime("2017-12-25T00:00:00-00:00");
		data.setExpirationDateTime("2017-05-02T00:00:00-00:00");
		data.setTransactionFromDateTime("2017-05-03T00:00:00-00:00");
		data.setTransactionToDateTime("2017-12-03T00:00:00-00:00");
		data.setAccountRequestId("af5b90c1-64b5-4a52-ba55-5eed68b2a269");
		data.setStatus(StatusEnum.AUTHORISED);
		mockAccountRequestPOSTResponse.setData(data);
		mockAccountRequestPOSTResponse.setRisk(null);
		return mockAccountRequestPOSTResponse;
	}
}
