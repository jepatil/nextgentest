/*************************************************************************
 * 
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 */
package com.capgemini.psd2.security.consent.aisp.rest.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;

import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.Account;
import com.capgemini.psd2.aisp.domain.AccountGETResponse;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.consent.domain.PSD2AccountsAdditionalInfo;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.fraudsystem.helper.FraudSystemHelper;
import com.capgemini.psd2.integration.adapter.TPPInformationAdaptor;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.scaconsenthelper.config.PFConfig;
import com.capgemini.psd2.scaconsenthelper.config.helpers.ConsentAuthorizationHelper;
import com.capgemini.psd2.scaconsenthelper.config.helpers.SCAConsentHelper;
import com.capgemini.psd2.scaconsenthelper.constants.PFConstants;
import com.capgemini.psd2.scaconsenthelper.models.DropOffResponse;
import com.capgemini.psd2.scaconsenthelper.models.IntentTypeEnum;
import com.capgemini.psd2.scaconsenthelper.models.PFInstanceData;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.scaconsenthelper.services.SCAConsentHelperService;
import com.capgemini.psd2.security.consent.aisp.helpers.AispConsentCreationDataHelper;
import com.capgemini.psd2.security.consent.aisp.helpers.AispConsentCreationDataHelperImpl;
import com.capgemini.psd2.security.constants.PSD2SecurityConstants;
import com.capgemini.psd2.utilities.JSONUtilities;

/**
 * ConsentAuthorize Controller
 * 
 * @author Capgemini
 */

@RestController
public class AispConsentAuthorizeController {

	@Autowired
	private AispConsentCreationDataHelper consentCreationDataHelper;

	@Autowired
	private RequestHeaderAttributes requestHeaderAttributes;

	@Autowired
	private SCAConsentHelperService helperService;

	@Autowired
	private PFConfig pfConfig;

	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private HttpServletResponse response;

	@Autowired
	@Qualifier("aispConsentAuthorizationHelper")
	private ConsentAuthorizationHelper consentAuthorizationHelper;
	
	@Autowired
	private FraudSystemHelper fraudSystemHelper;
	
	
	@Autowired
	private AispConsentAdapter aispConsentAdapter;

	@Autowired
	private TPPInformationAdaptor tppInformationAdaptor;
	
	/**
	 * This method is used for creating the consent
	 * 
	 * @param contract-
	 *            ConsentTemplate
	 * @return ConsentResponse - Response after creating consent
	 * @throws NamingException 
	 */
	@RequestMapping(value = "/aisp/consent", method = RequestMethod.POST)
	public String createConsent(ModelAndView model, @RequestBody PSD2AccountsAdditionalInfo accountListAdditionalInfo,@RequestParam String resumePath,@RequestParam String refreshTokenRenewalFlow) throws NamingException {

		AispConsent aispConsent = null;
		
		String headers = accountListAdditionalInfo.getHeaders();

		PickupDataModel pickupDataModel = SCAConsentHelper.populatePickupDataModel(request);
		
		if(refreshTokenRenewalFlow != null && Boolean.valueOf(refreshTokenRenewalFlow)){
			aispConsent = aispConsentAdapter.retrieveConsentByAccountRequestId(pickupDataModel.getIntentId(),ConsentStatusEnum.AUTHORISED);
			if(aispConsent == null){
				throw PSD2Exception.populatePSD2Exception("Account setup is not having authorized status.",
						ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
			}
		}
		if(aispConsent != null){
			Map<String, String> params = new HashMap<>();
			params.putAll(AispConsentCreationDataHelperImpl.captureFraudParam(headers));
			params.put(PSD2Constants.CHANNEL_NAME, pickupDataModel.getChannelId());
			
			List<Account> accountList = consentAuthorizationHelper.populateAccountListFromAccountDetails(aispConsent);
			
			fraudSystemHelper.captureFraudEvent(pickupDataModel.getUserId(), (List<PSD2Account>) (List<?>) accountList, params);
			
			helperService.revokeAllPreviousGrants(pickupDataModel.getIntentId(), this.pfConfig.getGrantsrevocationapiuser(),
					this.pfConfig.getGrantsrevocationapipwd());
		}
		
		else {
			
			AccountGETResponse custAccountList = consentCreationDataHelper.retrieveCustomerAccountListInfo(
					pickupDataModel.getUserId(), pickupDataModel.getClientId(),
					IntentTypeEnum.AISP_INTENT_TYPE.getIntentType(), requestHeaderAttributes.getCorrelationId(),
					pickupDataModel.getChannelId());
			
			List<PSD2Account> customerAccountList = null;
			customerAccountList = accountListAdditionalInfo.getAccountdetails();

			Account matchedAccount = null;
			List<Account> consentCustAcctList = new ArrayList<Account>();

			for (PSD2Account selectedAccount : customerAccountList) { // Submitted
																		// Accounts
				matchedAccount = consentAuthorizationHelper.matchAccountFromList(custAccountList, selectedAccount);
				consentCustAcctList.add(matchedAccount);
			}
			
			String tppApplicationName = tppInformationAdaptor.fetchApplicationName(pickupDataModel.getClientId());
			Object tppInformationObj = tppInformationAdaptor.fetchTPPInformation(pickupDataModel.getClientId());			
			
			consentCreationDataHelper.createConsent(consentCustAcctList, pickupDataModel.getUserId(),
					pickupDataModel.getClientId(), pickupDataModel.getIntentId(), pickupDataModel.getChannelId(), headers, tppApplicationName, tppInformationObj);
		}
		

		DropOffResponse dropOffResponse = helperService.dropOffOnConsentSubmission(pickupDataModel,
				this.pfConfig.getAispinstanceId(), this.pfConfig.getAispinstanceusername(),
				this.pfConfig.getAispinstancepassword());

		resumePath = pfConfig.getResumePathBaseURL().concat(resumePath);

		String redirectURI = UriComponentsBuilder.fromHttpUrl(resumePath)
				.queryParam(PFConstants.REF, dropOffResponse.getRef()).toUriString();

		model.addObject(PSD2SecurityConstants.REDIRECT_URI_MODEL_ATTRIBUTE, redirectURI);
		SCAConsentHelper.invalidateCookie(response);
		return JSONUtilities.getJSONOutPutFromObject(model);
	}


	@RequestMapping(value = "/aisp/cancelConsent", method = RequestMethod.PUT)
	public String cancelConsent(ModelAndView model, @RequestParam Map<String, String> paramsMap) {
		
		String serverErrorFlag = paramsMap.get(PSD2Constants.SERVER_ERROR_FLAG_ATTR);

		PickupDataModel pickupDataModel = SCAConsentHelper.populatePickupDataModel(request);
		
		String refreshTokenRenewalFlow = paramsMap.get(PSD2SecurityConstants.REFRESH_TOKEN_RENEWAL_FLOW);

		AispConsent aispConsent  = aispConsentAdapter.retrieveConsentByAccountRequestId(pickupDataModel.getIntentId(),ConsentStatusEnum.AUTHORISED);
		
		if(refreshTokenRenewalFlow != null && Boolean.valueOf(refreshTokenRenewalFlow)){
			if(aispConsent == null){
				throw PSD2Exception.populatePSD2Exception("Account setup is not having authorized status.",
						ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
			}
		}
		else if(aispConsent != null){
			throw PSD2Exception.populatePSD2Exception("Account setup is not having authorized status.",
					ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
		}
		if(aispConsent == null){
			consentCreationDataHelper.cancelAccountRequest(pickupDataModel.getIntentId());
		}

		String resumePath = paramsMap.get(PFConstants.RESUME_PATH);

		resumePath = pfConfig.getResumePathBaseURL().concat(resumePath);

		if (serverErrorFlag != null && !serverErrorFlag.isEmpty() && Boolean.valueOf(serverErrorFlag)) {
			resumePath = UriComponentsBuilder.fromHttpUrl(resumePath).queryParam(PFConstants.REF, null).toUriString();
		} else if ((serverErrorFlag == null || serverErrorFlag.isEmpty())
				|| (serverErrorFlag != null && !serverErrorFlag.isEmpty() && !Boolean.valueOf(serverErrorFlag))) {
			PFInstanceData pfInstanceData = new PFInstanceData();
			pfInstanceData.setPfInstanceId(pfConfig.getAispinstanceId());
			pfInstanceData.setPfInstanceUserName(pfConfig.getAispinstanceusername());
			pfInstanceData.setPfInstanceUserPwd(pfConfig.getAispinstancepassword());
			resumePath = helperService.cancelJourney(resumePath, pfInstanceData);
		}
		model.addObject(PSD2SecurityConstants.REDIRECT_URI_MODEL_ATTRIBUTE, resumePath);
		SCAConsentHelper.invalidateCookie(response);		
		return JSONUtilities.getJSONOutPutFromObject(model);
	}

}