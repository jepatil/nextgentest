package com.capgemini.psd2.security.consent.aisp.helpers;

import java.util.Map;

import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.scaconsenthelper.config.helpers.ConsentAuthorizationHelper;

public class AispConsentAuthorizationHelper extends ConsentAuthorizationHelper {

	@Override
	public PSD2Account populateAccountwithUnmaskedValues(PSD2Account accountwithMask, PSD2Account accountwithoutMask) {

		if(accountwithoutMask.getAccount() != null)
			accountwithMask.getAccount().setIdentification(accountwithoutMask.getAccount().getIdentification());
		
		Map<String,String> additionalInfo = accountwithoutMask.getAdditionalInformation();
		Map<String,String> additionalInfotobeUnmasked = accountwithMask.getAdditionalInformation();
		
		if(additionalInfo != null && additionalInfotobeUnmasked != null){
			additionalInfotobeUnmasked.put(PSD2Constants.ACCOUNT_NUMBER, additionalInfo.get(PSD2Constants.ACCOUNT_NUMBER));
			additionalInfotobeUnmasked.put(PSD2Constants.IBAN, additionalInfo.get(PSD2Constants.IBAN));
		}
		
		return accountwithMask;
	}

}
